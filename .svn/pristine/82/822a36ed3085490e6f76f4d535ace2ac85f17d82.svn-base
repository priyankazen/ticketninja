/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONObject;

import java.security.MessageDigest;

import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.NotificationChannelUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.AppVersion;
import in.ticketninja.objects.MyEvent;
import in.ticketninja.objects.UserInfo;
import in.ticketninja.service.TimerService;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestBasicInfo;
import in.ticketninja.ws.request.RequestForceUpdate;
import in.ticketninja.ws.response.CallbackBasicInfo;
import in.ticketninja.ws.response.CallbackForceUpdate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private LoginUtils mLogin;
    private PreferencesUtils mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        mLogin = new LoginUtils(this);
        mPref = new PreferencesUtils(this);

        if (Utility.isOnline(this) && mLogin.isLoggedIn()) {
            wsBasicInfo(mLogin.getUserId());
        }
        Log.e(TAG, "Token= >" + PreferencesUtils.getFCMPushKey(this));
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "in.ticketninja.test",
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        try {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                NotificationChannelUtils channelUtils = new NotificationChannelUtils(this);
                channelUtils.createChannels();
            }
            if (!PreferencesUtils.isFCMTopicRegister(this) && Validate.isNotNull(PreferencesUtils.getFCMPushKey(this))) {
                FirebaseMessaging.getInstance().subscribeToTopic("Event_Detail");
                PreferencesUtils.setFCMTopicRegister(this, true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Utility.isOnline(this)) {
            wsCheckLatestVersion();
        } else {
            MessageUtils.showToast(getBaseContext(), R.string.msg_no_internet);
            if (Validate.isNotNull(PreferencesUtils.getAPP_VERSION(getBaseContext())) && Validate.isNotNull(PreferencesUtils.getAppMessage(getBaseContext()))) {
                if (Integer.parseInt(PreferencesUtils.getAPP_VERSION(getBaseContext())) > Utility.getAppVersionCode(SplashActivity.this)
                        && PreferencesUtils.isIS_FORCEFULLY(getBaseContext())) {

                    Log.e(TAG, " and " + PreferencesUtils.getAPP_VERSION(getBaseContext()));
                    Log.e(TAG, " and " + Utility.getAppVersionCode(SplashActivity.this));

                    Log.e(TAG, " and " + PreferencesUtils.isIS_FORCEFULLY(getBaseContext()));

                    CommonClass.showForceUpdateDialogOffline(SplashActivity.this);
                } else {
                    onSplashComplete();
                }
            } else {
                onSplashComplete();
            }
        }
    }

    private void onSplashComplete() {

        new CountDownTimer(3000, 1000) {
            public void onTick(long millisUntilFinished) {
            }

            public void onFinish() {
                if (!isFinishing()) {
                    startNextActivity();
                }
            }
        }.start();
    }

    private void startLandingActivity() {
        //Intent intent = new Intent(getApplicationContext(), NotificationListActivity.class);
        Intent intent = new Intent(getApplicationContext(), LandingActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        //finish();
    }

    private void startNextActivity() {
        try {

            if (Utility.isMyServiceRunning(this, TimerService.class)) {
                stopService(new Intent(this, TimerService.class));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Log.e(TAG, Utility.isAppOpen(SplashActivity.this) + "");
            if (getIntent().getExtras() != null && getIntent() != null) {
                Bundle bundle = getIntent().getExtras();
                JSONObject msgObject = new JSONObject(bundle.getString("message"));
                JSONObject apsObject = new JSONObject(msgObject.getString("aps"));
                String msgData = apsObject.optString("data");
                if (Validate.isNotNull(msgData)) {
                    JSONObject data = new JSONObject(apsObject.getString("data"));

                    long LoginMasterID = data.optLong("LoginMasterID", 0);
                    int nType = data.optInt("N_Type", 0);

                    if (nType == Constant.NotificationType.BULK_EVENT_DETAIL) {
                        long EventId = data.optLong("Event_Id", 0);
                        MyEvent myEvent = new MyEvent();
                        myEvent.Event_Id = EventId;
                        Intent noteIntent = new Intent(this, EventDetailActivity.class);
                        noteIntent.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                        noteIntent.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_NOTIFICATION);
                        noteIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                        startActivity(noteIntent);

                    } else if (mLogin.isLoggedIn() && LoginMasterID == mLogin.getUserId()) {
                        long EventId = data.optLong("Event_Id", 0);
                        //long ReferenceId = data.optLong("Reference_Id", 0);
                        //long NotificationID = data.optLong("NotificationID", 0);

                        if (nType == Constant.NotificationType.EVENT) {
                            MyEvent myEvent = new MyEvent();
                            myEvent.Event_Id = EventId;
                            Intent noteIntent = new Intent(this, EventDetailActivity.class);
                            noteIntent.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                            noteIntent.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_NOTIFICATION);
                            noteIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(noteIntent);

                        } else if (nType == Constant.NotificationType.TICKET) {
                            Intent noteIntent = new Intent(this, MyTicketsActivity.class);
                            noteIntent.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_NOTIFICATION);
                            noteIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                            startActivity(noteIntent);
                        }
                    }else {
                        startLandingActivity();
                    }
                }else {
                    startLandingActivity();
                }
            } else {
                startLandingActivity();
            }
        } catch (Exception e) {
            e.printStackTrace();
            startLandingActivity();
        }
    }

    // WS - Basic Info
    private void wsBasicInfo(long userId) {
        RequestBasicInfo request = new RequestBasicInfo(userId);

        TicketNinjaAPI tnAPI = RestApi.createAPI();
        Call<CallbackBasicInfo> call = tnAPI.basicInfo(request);
        call.enqueue(new Callback<CallbackBasicInfo>() {
            @Override
            public void onResponse(Call<CallbackBasicInfo> call, @NonNull Response<CallbackBasicInfo> response) {
                if (response.isSuccessful()) {
                    if (response.body().data().isSuccess()) {

                        // REMOVE CACHED DASHBOARD DATA
                        // CacheHelper mCacheHelper = new CacheHelper(SplashActivity.this);
                        // mCacheHelper.remove(CacheHelper.KEY_DASHBOARD);

                        UserInfo userInfo = response.body().data();
                        LoginUtils mLogin = new LoginUtils(SplashActivity.this);
                        mLogin.setUserId(userInfo.User_Id);
                        mLogin.setFirstName(userInfo.Firstname);
                        mLogin.setLastName(userInfo.Lastname);
                        mLogin.setEmail(userInfo.Email);
                        mLogin.setCountryCode(userInfo.Country_Code);
                        mLogin.setMobile(userInfo.MobileNo);
                        mLogin.setGender(userInfo.Gender);
                        mLogin.setPicture(userInfo.User_Image_URL);
                        mLogin.setUserDOBWithParse(userInfo.Birthdate);
                        mLogin.setNotifications(userInfo.notification_unread_count);

                        if (Validate.isNotNull(mPref.getUserEmailId())){
                            mPref.setUserEmailId(userInfo.Email);
                        }
                        if (Validate.isNotNull(mPref.getUserPhoneNumber())){
                            mPref.setUserPhoneNumber(userInfo.MobileNo);
                        }
                        if (Validate.isNotNull(mPref.getCountryCode())){
                            mPref.setCountryCode(userInfo.Country_Code);
                        }


                        try {
                            // FirebaseAnalytics
                            FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
                            mFirebaseAnalytics.setUserProperty("tn_user_id", mLogin.getUserId() + "");
                            mFirebaseAnalytics.setUserProperty("tn_user_name", mLogin.getName() + "");
                            mFirebaseAnalytics.setUserId(mLogin.getUserId() + "");
                            // Crashlytics
                            Crashlytics.setUserIdentifier("" + mLogin.getUserId());
                            Crashlytics.setUserEmail("" + mLogin.getEmail());
                            Crashlytics.setUserName("" + mLogin.getName());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<CallbackBasicInfo> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    // WS - CHECK LATEST APP VERSION
    private void wsCheckLatestVersion() {

        RequestForceUpdate request = new RequestForceUpdate(getPackageName());

        TicketNinjaAPI tnAPI = RestApi.createAPI();
        Call<CallbackForceUpdate> call = tnAPI.forceUpdate(request);
        call.enqueue(new Callback<CallbackForceUpdate>() {
            @Override
            public void onResponse(Call<CallbackForceUpdate> call, Response<CallbackForceUpdate> response) {
                try {
                    if (response.isSuccessful()) {
                        AppVersion appVersion = response.body().data();
                        if (appVersion != null && appVersion.isSuccess()) {

                            PreferencesUtils.setVersionCode(getBaseContext(), String.valueOf(appVersion.getAppVersion()));
                            Log.e(TAG, appVersion.getAppVersion() + " and " + PreferencesUtils.getAPP_VERSION(getBaseContext()));

                            PreferencesUtils.setIS_FORCEFULLY(getBaseContext(), appVersion.isForcefully());
                            PreferencesUtils.setVersionMessage(getBaseContext(), appVersion.getMessage());
                            Log.e(TAG, appVersion.isForcefully() + " and " + PreferencesUtils.isIS_FORCEFULLY(getBaseContext()) + " and " + PreferencesUtils.getAppMessage(getBaseContext()));

                            if (appVersion.getAppVersion() > Utility.getAppVersionCode(SplashActivity.this)
                                    && appVersion.isForcefully()) {


                                CommonClass.showForceUpdateDialog(SplashActivity.this, appVersion);
                            } else {
                                onSplashComplete();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    onSplashComplete();
                }
            }

            @Override
            public void onFailure(Call<CallbackForceUpdate> call, Throwable t) {
                t.printStackTrace();
                onSplashComplete();
            }

        });
    }

}
