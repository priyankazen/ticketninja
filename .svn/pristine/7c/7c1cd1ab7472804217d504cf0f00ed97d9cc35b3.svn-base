/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.MyEventTimeList;

public class EventShowInfoAdapter extends RecyclerView.Adapter {

    private List<MyEventTimeList> dataList;
    private Context context;
    private boolean isArtistFirst;
    private String eventType = "";

    private final int VIEW_TYPE_ARTIST_FIRST = 0;
    private final int VIEW_TYPE_DETAIL_FIRST = 1;

    private int currentExpand = -1;

    public EventShowInfoAdapter(Context context, List<MyEventTimeList> dataList, boolean isArtistFirst, String eventType) {
        this.context = context;
        this.dataList = dataList;
        this.isArtistFirst = isArtistFirst;
        this.eventType = eventType;
    }

    @Override
    public int getItemViewType(int position) {
        return isArtistFirst ? VIEW_TYPE_ARTIST_FIRST : VIEW_TYPE_DETAIL_FIRST;
        //return super.getItemViewType(position);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v;
        if (viewType == VIEW_TYPE_DETAIL_FIRST) {
            v = inflater.inflate(R.layout.item_event_detail_show_info_reverse, parent, false);
        } else {
            v = inflater.inflate(R.layout.item_event_detail_show_info, parent, false);
        }
        return new ShowViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof ShowViewHolder) {
            ShowViewHolder h = (ShowViewHolder) viewHolder;
            MyEventTimeList timeList = dataList.get(position);
            //h.tvShowDate.setText(timeList.toString());
            h.tvShowDate.setText(DateTimeUtils.changeDateTimeFormat(timeList.date(), DateTimeUtils.SERVER_FORMAT_DATE, "dd MMM"));
            //h.tvShowYear.setText(timeList.toString());
            //h.tvShowYear.setText(DateTimeUtils.changeDateTimeFormat(timeList.date(), DateTimeUtils.SERVER_FORMAT_DATE, "yyyy"));
            h.tvShowYear.setText(String.format("%s", timeList.time()));
            h.tvShowTitle.setText(timeList.title());
            h.tvShowSmallDetail.setText(timeList.smallDetail());

            //h.ivExpandArrow.animate().scaleY(currentExpand == h.getAdapterPosition() ? -1f : 1f).start();
            //h.ivExpandArrow.setRotation(currentExpand == h.getAdapterPosition() ? 1800 : 0);
            h.ivExpandArrow.setScaleY(currentExpand == h.getAdapterPosition() ? -1f : 1f);
            h.llShowDetail.setVisibility(currentExpand == h.getAdapterPosition() ? View.VISIBLE : View.GONE);
            h.llShowHeader.setOnClickListener(v -> {
                currentExpand = currentExpand == h.getAdapterPosition() ? -1 : h.getAdapterPosition();
                /*MyEventTimeList currentTimeList = currentExpand == h.getAdapterPosition() ? dataList.get(currentExpand) : null;
                if (clickListener != null)
                    clickListener.onItemClick(currentExpand, currentTicket, category1);*/

                notifyDataSetChanged();
            });

            if (timeList.getArtists() != null && timeList.getArtists().size() > 0) {
                h.tvLabelArtists.setText(timeList.getArtists().size() > 1
                        ? R.string.event_detail_organizer_artists : R.string.event_detail_organizer_artist);
                EventArtistAdapter eventArtistAdapter = new EventArtistAdapter(context, timeList.getArtists());
                eventArtistAdapter.setOnItemClickListener((artist_position, isChecked, artist) -> {
                    // expandCollapseArtist(isChecked, artist);
                    if (isChecked && artist != null) {
                        h.llEventArtistDetail.setVisibility(View.VISIBLE);
                        h.tvArtistName.setText(String.format("About %s", artist.name()));
                        h.tvArtistDetail.setText(artist.detail());
                        /*handler.postDelayed(() -> {
                            if (isAppbarExpanded) collapseToolbar();
                            tvArtistDetail.getParent().requestChildFocus(tvArtistDetail, tvArtistDetail);
                        }, 200);*/
                        h.rvEventArtists.smoothScrollToPosition(artist_position);

                    } else {
                        h.llEventArtistDetail.setVisibility(View.GONE);
                        h.tvArtistName.setText("");
                        h.tvArtistDetail.setText("");
                    }

                });
                h.rvEventArtists.setAdapter(eventArtistAdapter);
                h.llEventArtistDetail.setVisibility(View.GONE);
                h.tvArtistName.setText("");
                h.tvArtistDetail.setText("");

                h.llEventArtists.setVisibility(View.VISIBLE);
            } else {
                h.llEventArtists.setVisibility(View.GONE);
            }
            if (Validate.isNotNull(eventType)) {
                h.tvLabelAbout.setText(String.format("About the %s", eventType));
            }
            if (Validate.isNotNull(timeList.fullDetail())) {
                String eventDetail = timeList.fullDetail();
                h.tvEventDetail.setText(Html.fromHtml(eventDetail));
                h.llEventAbout.setVisibility(View.VISIBLE);
            } else {
                h.llEventAbout.setVisibility(View.GONE);
            }

        }
    }

    private static class ShowViewHolder extends RecyclerView.ViewHolder {

        TextView tvShowDate;
        TextView tvShowYear;
        TextView tvShowTitle;
        TextView tvShowSmallDetail;
        View ivExpandArrow;

        View llShowHeader;
        View llShowDetail;

        LinearLayout llEventArtists;
        TextView tvLabelArtists;
        RecyclerView rvEventArtists;
        LinearLayout llEventArtistDetail;
        TextView tvArtistName;
        TextView tvArtistDetail;
        TextView tvLabelAbout;
        LinearLayout llEventAbout;
        TextView tvEventDetail;

        ShowViewHolder(@NonNull View itemView) {
            super(itemView);
            tvShowDate = itemView.findViewById(R.id.tvShowDate);
            tvShowYear = itemView.findViewById(R.id.tvShowYear);
            tvShowTitle = itemView.findViewById(R.id.tvShowTitle);
            tvShowSmallDetail = itemView.findViewById(R.id.tvShowSmallDetail);
            ivExpandArrow = itemView.findViewById(R.id.ivExpandArrow);

            llShowHeader = itemView.findViewById(R.id.ll_show_header);
            llShowDetail = itemView.findViewById(R.id.ll_show_detail);

            llEventArtists = itemView.findViewById(R.id.llEventArtists);
            tvLabelArtists = itemView.findViewById(R.id.tvLabelArtists);
            rvEventArtists = itemView.findViewById(R.id.rvEventArtists);
            rvEventArtists.setHasFixedSize(true);
            rvEventArtists.setNestedScrollingEnabled(false);
            llEventArtistDetail = itemView.findViewById(R.id.llEventArtistDetail);
            tvArtistName = itemView.findViewById(R.id.tvLabelArtistName);
            tvArtistDetail = itemView.findViewById(R.id.tvEventArtistDetail);

            llEventAbout = itemView.findViewById(R.id.llEventAbout);
            tvLabelAbout = itemView.findViewById(R.id.tvLabelAbout);
            tvEventDetail = itemView.findViewById(R.id.tvEventDetail);

        }
    }

}
