<?xml version="1.0" encoding="utf-8"?><!--
  ~ Copyright (c) 2017. Suthar Rohit
  ~ Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
  ~ <a href="http://RohitSuthar.com/">Suthar Rohit</a>
  ~
  ~ @author Suthar Rohit
  -->

<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="@color/themeGray"
    android:orientation="vertical"
    tools:context=".BookingHistoryDetailActivity">

    <include layout="@layout/layout_toolbar" />

    <View
        android:id="@+id/toolbarBottomDivider"
        style="@style/toolbarBottomDivider" />

    <LinearLayout
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:orientation="vertical"
        app:layout_behavior="@string/appbar_scrolling_view_behavior">

        <RelativeLayout
            android:id="@+id/llMainLayout"
            android:layout_width="match_parent"
            android:layout_height="wrap_content">

            <include layout="@layout/layout_ticket_detail_view_download" />

            <android.support.v4.widget.NestedScrollView
                android:id="@+id/nestedScrollView"
                android:layout_width="match_parent"
                android:layout_height="match_parent"
                android:layout_above="@id/llFooter">

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="vertical"
                    android:padding="@dimen/booking_layout_margin">

                    <android.support.v7.widget.CardView
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        app:cardBackgroundColor="@color/cardview_light_background"
                        app:cardCornerRadius="@dimen/card_corner_radius"
                        app:cardElevation="@dimen/card_elevation">

                        <LinearLayout
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:background="@android:color/white"
                            android:orientation="vertical">

                            <ImageView
                                android:id="@+id/ivEventPoster"
                                android:layout_width="match_parent"
                                android:layout_height="@dimen/parallax_height"
                                android:adjustViewBounds="true"
                                android:contentDescription="@string/title_activity_main"
                                android:scaleType="fitXY"
                                android:transitionName="eventPoster"
                                tools:background="@color/themeBlack"
                                tools:src="@drawable/ic_placeholder"
                                tools:visibility="gone" />

                            <LinearLayout
                                android:layout_width="match_parent"
                                android:layout_height="wrap_content"
                                android:orientation="vertical">

                                <TextView
                                    android:id="@+id/tvEventTitle"
                                    android:layout_width="match_parent"
                                    android:layout_height="wrap_content"
                                    android:layout_marginEnd="@dimen/booking_layout_inner_margin"
                                    android:layout_marginLeft="@dimen/booking_layout_inner_margin"
                                    android:layout_marginRight="@dimen/booking_layout_inner_margin"
                                    android:layout_marginStart="@dimen/booking_layout_inner_margin"
                                    android:layout_marginTop="@dimen/booking_layout_inner_margin"
                                    android:textColor="@color/event_pricing_title_textColor"
                                    android:textSize="@dimen/order_summary_title_textSize"
                                    android:transitionName="eventTitle"
                                    tools:text="Farhan Akhtar-Live In Concert" />

                                <View
                                    style="@style/account_divider_section"
                                    android:layout_marginEnd="@dimen/booking_layout_inner_margin"
                                    android:layout_marginLeft="@dimen/booking_layout_inner_margin"
                                    android:layout_marginRight="@dimen/booking_layout_inner_margin"
                                    android:layout_marginStart="@dimen/booking_layout_inner_margin"
                                    android:layout_marginTop="@dimen/booking_layout_inner_margin" />

                                <include layout="@layout/layout_ticket_detail_booking_id_barcode" />

                                <include layout="@layout/layout_booking_history_info" />

                                <include
                                    layout="@layout/layout_booking_history_detail"
                                    tools:visibility="visible" />

                            </LinearLayout>
                        </LinearLayout>
                    </android.support.v7.widget.CardView>
                </LinearLayout>
            </android.support.v4.widget.NestedScrollView>
        </RelativeLayout>

        <include
            layout="@layout/layout_no_internet"
            android:visibility="gone" />

    </LinearLayout>
</LinearLayout>
