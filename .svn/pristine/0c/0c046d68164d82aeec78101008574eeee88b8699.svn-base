/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.ticketninja.adapters.EventPricingAdapter;
import in.ticketninja.adapters.EventPricingDateChooserAdapter;
import in.ticketninja.adapters.EventPricingTimeChooserAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.EventPriceCategory;
import in.ticketninja.objects.MultipleDay;
import in.ticketninja.objects.MyEventDetail;
import in.ticketninja.objects.MyEventTimeList;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestInsertTicketSession;
import in.ticketninja.ws.response.CallbackInsertTicketSession;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventPricingMultidayActivity extends AppCompatActivity {

    private final String TAG = EventPricingMultidayActivity.class.getSimpleName();

    private CommonClass CC;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;

    private RecyclerView rvDateSelector;
    private RecyclerView rvTimeSelector;
    private RecyclerView recyclerView;
    private LinearLayout llErrorLayout;
    private View llMainLayout;
    private Button btnContinue;
    private ImageView ivErrorImage;
    private TextView tvErrorMessage, tvErrorButton;

    private MyEventDetail myEvent;

    private TextView tvTitle;
    private TextView tvDateTime;
    private TextView tvShowTitle;
    private LinearLayout llEventVenue;
    private TextView tvVenueName;
    private TextView tvVenueAddress;
    private LinearLayout llSeasonPass, llTimeView;
    private TextView tvBtnSingleDayPass, tvBtnSeasonPass;

    private EventPriceCategory selectedCategory;
    private boolean isSeasonPass = false;
    private int selectedDatePos = 0;
    private int selectedTimePos = 0;
    private MultipleDay seasonPassDay = new MultipleDay();
    private List<MultipleDay> displayDayList = new ArrayList<>();
    private List<EventPriceCategory> pricingCategoryList = new ArrayList<>();
    private EventPricingDateChooserAdapter mDateAdapter;
    private EventPricingAdapter mEventPricingAdapter;
    private TextView tvDiscount;
    private RelativeLayout rvDiscount;
    private PreferencesUtils mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_pricing_multiple_day);

        Log.e(TAG,"is calledddd..............");

        mPref=new PreferencesUtils(this);
        CC = new CommonClass(this);
        mLogin = new LoginUtils(this);
        mLoader = new SRKLoaderDialog(this);
        if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_DATA)) {
            myEvent = (MyEventDetail) getIntent().getSerializableExtra(Constant.ScreenExtras.EVENT_DATA);
        }

        // ACTIONBAR
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            ViewCompat.setElevation(toolbar, 10);

            ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
            ivActionBack.setOnClickListener(v -> onBackPressed());
            TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
            tvTitle.setText(getTitle());
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
        }

        rvDateSelector = findViewById(R.id.rvDateSelector);
        rvDateSelector.setHasFixedSize(true);
        rvDateSelector.setNestedScrollingEnabled(false);

        rvTimeSelector = findViewById(R.id.rvTimeSelector);
        rvTimeSelector.setHasFixedSize(true);
        rvTimeSelector.setNestedScrollingEnabled(false);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        tvTitle = findViewById(R.id.tvEventTitle);
        tvDiscount = findViewById(R.id.tv_discount);
        rvDiscount = findViewById(R.id.rv_discount);
        tvDateTime = findViewById(R.id.tvEventDateTime);
        btnContinue = findViewById(R.id.btnContinue);
        llEventVenue = findViewById(R.id.llEventVenue);
        tvVenueName = findViewById(R.id.tvEventVenue);
        tvVenueAddress = findViewById(R.id.tvEventAddress);
        tvShowTitle = findViewById(R.id.tv_show_title);

        llTimeView = findViewById(R.id.llTimeView);
        llSeasonPass = findViewById(R.id.llSeasonPass);
        tvBtnSingleDayPass = findViewById(R.id.tvBtnSingleDayPass);
        tvBtnSeasonPass = findViewById(R.id.tvBtnSeasonPass);

        tvBtnSingleDayPass.setOnClickListener(view -> {
            if (isSeasonPass) {
                isSeasonPass = false;
                changePassType();
            }

        });
        tvBtnSeasonPass.setOnClickListener(view -> {
            if (!isSeasonPass) {
                isSeasonPass = true;
                changePassType();
            }
        });

        llMainLayout = findViewById(R.id.nestedScrollView);
        llErrorLayout = findViewById(R.id.llErrorLayout);
        ivErrorImage = findViewById(R.id.ivErrorImage);
        tvErrorMessage = findViewById(R.id.tvErrorMessage);
        tvErrorButton = findViewById(R.id.tvErrorButton);

        if (CC.isOnline()) {
            if (myEvent != null) {
                invalidateData();
            } else {
                tvErrorMessage.setText(R.string.msg_something_wrong);
                showError();
            }
        } else {
            showNoInternet();
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == RESULT_OK && requestCode == Constant.REQUEST_CODE_CONFIRM_CONTACT_DETAIL) {
//            if (data != null && data.hasExtra(Constant.ScreenExtras.EVENT_SESSION_ID)) {
//                mPref = new PreferencesUtils(this);
//                if (Validate.isNotNull(mPref.getUserEmailId()) && Validate.isNotNull(mPref.getUserPhoneNumber()) &&
//                        Validate.isNotNull(mPref.getCountryCode())) {
//                    OrderSummaryActivity.start(EventPricingMultidayActivity.this, data.getStringExtra(Constant.ScreenExtras.EVENT_SESSION_ID));
//                }
//            }
//        }
//    }

    private void changePassType() {
        if (isSeasonPass) {
            tvBtnSingleDayPass.setSelected(false);
            tvBtnSeasonPass.setSelected(true);
            mDateAdapter.setSeasonPass(isSeasonPass && myEvent.isSeasonPassAllowed(), 0);
            mDateAdapter.notifyDataSetChanged();

            if (seasonPassDay != null && Validate.isNotNull(seasonPassDay.eventDate()))
                setupTime(seasonPassDay);

        } else {
            tvBtnSingleDayPass.setSelected(true);
            tvBtnSeasonPass.setSelected(false);
            mDateAdapter.setSeasonPass(isSeasonPass && myEvent.isSeasonPassAllowed(), selectedDatePos);
            mDateAdapter.notifyDataSetChanged();

            if (displayDayList.size() > 0) {
                setupTime(displayDayList.get(selectedDatePos));
                //filterAndDisplayCategory(multipleDayList.get(selectedDatePos).eventDate(), "");
            }
        }
    }

    public void setupTime(MultipleDay day) {
        selectedTimePos = 0;
        if (day.timeList() != null && day.timeList().size() > 0) {
            List<MyEventTimeList> timeList = day.timeList();
            EventPricingTimeChooserAdapter mTimeAdapter = new EventPricingTimeChooserAdapter(timeList);
            mTimeAdapter.setOnTimeSelectedListener((position, time) -> {
                selectedTimePos = position;

                filterAndDisplayCategory(day.eventDate(), time.time(), time.title());
            });
            rvTimeSelector.setAdapter(mTimeAdapter);
            llTimeView.setVisibility(View.VISIBLE);

            filterAndDisplayCategory(day.eventDate(), timeList.get(selectedTimePos).time(), timeList.get(selectedTimePos).title());

        } else {
            llTimeView.setVisibility(View.GONE);

            if (displayDayList.size() > 0)
                filterAndDisplayCategory(displayDayList.get(selectedDatePos).eventDate(), "", "");
        }
    }

    private void filterAndDisplayCategory(String date, String time, String title) {
        //Log.e(TAG, "date=" + date + "  time=" + time + "  title=" + title);
        tvShowTitle.setText(Validate.isNotNull(title) ? title : "");
        tvShowTitle.setVisibility(Validate.isNotNull(title) ? View.VISIBLE : View.GONE);
        List<EventPriceCategory> pricingList = new ArrayList<>();
        for (EventPriceCategory eventPricing : pricingCategoryList) {
            if (isSeasonPass && eventPricing.isSeasonPass()
                    && eventPricing.time().equalsIgnoreCase(time)) {
                pricingList.add(eventPricing);
            } else if (!isSeasonPass
                    && eventPricing.eventDate().equalsIgnoreCase(date)
                    && eventPricing.time().equalsIgnoreCase(time)) {
                pricingList.add(eventPricing);
            }
        }

        if (mEventPricingAdapter == null) {
            mEventPricingAdapter = new EventPricingAdapter(pricingList);
            mEventPricingAdapter.setOnItemClickListener((position, count, priceCategory) -> {
                if (position >= 0 && count > 0 && priceCategory != null) {
                    selectedCategory = priceCategory;
                    selectedCategory.noOfTicket(count);
                    btnContinue.setEnabled(true);
                    //Log.e(TAG, "selectedCategory==  date=" + selectedCategory.eventDate() + "  time=" + selectedCategory.time());

                } else {
                    selectedCategory = null;
                    btnContinue.setEnabled(false);
                }
            });
            recyclerView.setAdapter(mEventPricingAdapter);
        } else {
            selectedCategory = null;
            btnContinue.setEnabled(false);
            mEventPricingAdapter.setDataList(pricingList);
            mEventPricingAdapter.notifyDataSetChanged();
        }
    }

    private void invalidateData() {

        tvTitle.setText(Validate.isNotNull(myEvent.getName()) ? String.format("%s", myEvent.getName()) : "");

        if (myEvent.getDayType().equalsIgnoreCase(Constant.EventDayType.SINGLE_DAY)) {
            tvDateTime.setText(String.format("%s, %s (%s %s)", myEvent.getDay(), myEvent.getMDDate(), myEvent.getTime(), Constant.ONWARDS));

        } else if (myEvent.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY)
                || myEvent.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW)) {
            tvDateTime.setText(String.format("%s, %s (%s %s)", myEvent.getMDDate(), myEvent.getYear(), myEvent.getTime(), Constant.ONWARDS));

        } else if (myEvent.getDayType().equalsIgnoreCase(Constant.EventDayType.PERPETUAL)) {
            tvDateTime.setText(String.format("%s, %s (%s)", myEvent.getDay(), myEvent.getMDDate(), myEvent.getTime()));

        } else {
            tvDateTime.setText("");
        }

        tvVenueName.setText(Validate.isNotNull(myEvent.getVenueName()) ? String.format("%s", myEvent.getVenueName()) : "");
        tvVenueAddress.setText(Validate.isNotNull(myEvent.getVenueFullAddress()) ? String.format("%s", myEvent.getVenueFullAddress()) : "");


        if (Validate.isNotNull(myEvent.getDiscountLabel())) {
            rvDiscount.setVisibility(View.VISIBLE);
            tvDiscount.setText(String.format(Locale.getDefault(), "%s %d%% off", myEvent.getDiscountLabel(), myEvent.getDiscountPer()));
        } else {
            rvDiscount.setVisibility(View.GONE);
        }

        if (Validate.isNotNull(myEvent.getLatitude()) && Validate.isNotNull(myEvent.getLongitude())) {
            llEventVenue.setOnClickListener(v -> CommonClass.openMap(this, myEvent.getVenueName(), myEvent.getLatitude(), myEvent.getLongitude()));
        }

        llSeasonPass.setVisibility(myEvent.isSeasonPassAllowed() && myEvent.isSingleDayPassAllowed() ? View.VISIBLE : View.GONE);

        if (myEvent.getMultipleDayList() != null && myEvent.getMultipleDayList().size() > 0) {
            List<MultipleDay> multipleDayList = myEvent.getMultipleDayList();
            for (MultipleDay day : multipleDayList) {
                if (day.isSeasonPass()) {
                    seasonPassDay = day;
                } else {
                    displayDayList.add(day);
                }
            }
            mDateAdapter = new EventPricingDateChooserAdapter(displayDayList, isSeasonPass && myEvent.isSeasonPassAllowed());
            mDateAdapter.setOnDateSelectedListener((position, day) -> {
                selectedDatePos = position;
                //filterAndDisplayCategory(day.eventDate());
                setupTime(day);
            });
            rvDateSelector.setAdapter(mDateAdapter);
            rvDateSelector.setVisibility(View.VISIBLE);
        } else {
            rvDateSelector.setVisibility(View.GONE);
        }

        if (myEvent.getPriceCategory() != null && myEvent.getPriceCategory().size() > 0) {
            pricingCategoryList = myEvent.getPriceCategory();
            btnContinue.setEnabled(false);

            recyclerView.setVisibility(View.VISIBLE);
        } else {
            recyclerView.setVisibility(View.GONE);
        }

        btnContinue.setOnClickListener(v -> {
            Utility.hideKeyboard(this);
            if (CC.isOnline() && selectedCategory != null) {
                if (Validate.isNotNull(selectedCategory.seatType()) && selectedCategory.seatType().equalsIgnoreCase(Constant.SEAT_TYPE_ARRANGE_SEATING)) {
                    Intent intent = new Intent(getApplicationContext(), SeatSelectionActivity.class);
                    intent.putExtra(Constant.ScreenExtras.EVENT_ID, myEvent.getId());
                    intent.putExtra(Constant.ScreenExtras.SHOW_ID, selectedCategory.showId());
                    intent.putExtra(Constant.ScreenExtras.PLAN_CATEGORY_ID, selectedCategory.id());
                    intent.putExtra(Constant.ScreenExtras.SEAT_COUNT, selectedCategory.noOfTicket());
                    intent.putExtra(Constant.ScreenExtras.EVENT_DATE, selectedCategory.eventDate());
                    intent.putExtra(Constant.ScreenExtras.EVENT_TIME, selectedCategory.time());
                    startActivity(intent);
                    //finish();
                } else {
                    wsInsertTicketSession(myEvent.getId(), selectedCategory.eventDate(), selectedCategory.time(), mLogin.getUserId(), selectedCategory);
                }
            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        });

        //Log.e(TAG, "myEvent.isSeasonPassAllowed()=>" + myEvent.isSeasonPassAllowed());
        //Log.e(TAG, "myEvent.isSingleDayPassAllowed()=>" + myEvent.isSingleDayPassAllowed());

        isSeasonPass = myEvent.isSeasonPassAllowed() && !myEvent.isSingleDayPassAllowed();

        changePassType();

    }

    public void showNoInternet() {
        Glide.with(this)
                .load(R.drawable.ic_connection_lost)
                .placeholder(R.drawable.ic_connection_lost_static)
                .into(ivErrorImage);
        tvErrorMessage.setText(R.string.msg_no_internet);
        llMainLayout.setVisibility(View.GONE);
        llErrorLayout.setVisibility(View.VISIBLE);

        tvErrorButton.setVisibility(View.VISIBLE);
        tvErrorButton.setText(R.string.btn_try_again);
        tvErrorButton.setOnClickListener(v -> {

        });
    }

    public void showError() {
        Glide.with(this)
                .load(R.drawable.ic_no_result_found)
                .placeholder(R.drawable.ic_no_result_found)
                .into(ivErrorImage);
        llMainLayout.setVisibility(View.GONE);
        llErrorLayout.setVisibility(View.VISIBLE);

        tvErrorButton.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboard(this);
        super.onBackPressed();
    }

    // WS - Insert Ticket Session
    private void wsInsertTicketSession(long eventId, String eventDate, String timeSlot, long userId, EventPriceCategory categoryList) {
        if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

        RequestInsertTicketSession request = new RequestInsertTicketSession(eventId, eventDate, timeSlot, userId, categoryList);

        TicketNinjaAPI tnAPI = RestApi.createAPI();
        Call<CallbackInsertTicketSession> call = tnAPI.insertTicketSession(request);
        call.enqueue(new Callback<CallbackInsertTicketSession>() {
            @SuppressWarnings("ConstantConditions")
            @Override
            public void onResponse(Call<CallbackInsertTicketSession> call, @NonNull Response<CallbackInsertTicketSession> response) {
                if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                try {
                    if (response.isSuccessful()) {
                        if (response.body().data().isSuccess()) {
                            String sessionID = response.body().data().sessionID();

                            if (Validate.isNull(mPref.getUserEmailId()) || Validate.isNull(mPref.getUserPhoneNumber()) ||
                                    Validate.isNull(mPref.getCountryCode())) {

                                // TODO: 29/3/18 SHREYA 1 : Contact Detail Flow
//                                startActivity(new Intent(EventPricingMultidayActivity.this, ContactConfirmationActivity.class));

                                ContactConfirmationActivity.Companion.start(EventPricingMultidayActivity.this,sessionID, Constant.ContactActions.EDIT);

                            }else {
                                OrderSummaryActivity.start(EventPricingMultidayActivity.this,sessionID);
                            }
                        } else if (Validate.isNotNull(response.body().data().message())) {
                            CC.showAlert(response.body().data().message());
                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CC.showToast(R.string.msg_something_wrong);
                }
            }

            @Override
            public void onFailure(Call<CallbackInsertTicketSession> call, @NonNull Throwable t) {
                if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                CC.showToast(R.string.msg_something_wrong);
                t.printStackTrace();
            }
        });

    }

}
