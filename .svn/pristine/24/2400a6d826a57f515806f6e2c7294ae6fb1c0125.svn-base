/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import in.ticketninja.EventDetailActivity;
import in.ticketninja.R;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.objects.MyEvent;
import in.ticketninja.objects.MySlider;

/**
 * Created by Shabbir on 26/10/16.
 */

public class DashboardPagerAdapter extends PagerAdapter {

    private CommonClass CC;
    private Context mContext;
    private List<MySlider> dataList = new ArrayList<>();

    public DashboardPagerAdapter(Context mContext, List<MySlider> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
        this.CC = new CommonClass(mContext);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.custom_pager_image, container, false);

        ImageView imageView = itemView.findViewById(R.id.img_pager_item);

        Glide.with(mContext)
                .load(dataList.get(position).getMainImageURL())
                //.centerCrop()
                .skipMemoryCache(false)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imageView);

        imageView.setOnClickListener(v -> {
            if (CC.isOnline()) {
                MyEvent myEvent = new MyEvent();
                myEvent.Event_Id = dataList.get(position).Event_Id;
                myEvent.Poster_Image_Id = dataList.get(position).getMainImageId();
                myEvent.Poster_Image_URL = dataList.get(position).getMainImageURL();
                myEvent.Main_Image_Id = dataList.get(position).getMainImageId();
                myEvent.Main_Image_URL = dataList.get(position).getMainImageURL();

                Intent intDetail = new Intent(v.getContext(), EventDetailActivity.class);
                intDetail.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    Pair<View, String> p1 = Pair.create(imageView, "eventPoster");
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) mContext, p1);
                    mContext.startActivity(intDetail, options.toBundle());
                } else {
                    mContext.startActivity(intDetail);
                }
            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        });

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }


}
