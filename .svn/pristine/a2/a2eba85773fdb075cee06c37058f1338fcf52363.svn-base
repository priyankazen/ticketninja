/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.common;

import android.os.Environment;
import android.support.annotation.StringDef;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * TicketNinja(in.ticketninja) <br />
 * Developed by <b><a href="http://RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 13/01/2016.
 *
 * @author Suthar Rohit
 */
public class Constant {

    public static final String ONWARDS = "Onwards";
    public static final String DEFAULT_COUNTRY_REGION = "IN";
    public static final String DEVICE_TYPE = "android";
    public static final String ENGINE_TYPE = "App";
    public static final String TIMER_FORMAT = "%02d:%02d";

    public static final int MAXIMUM_UPLOAD_FILE_SIZE = 1024 * 4;
    public static final int PAYMENT_SESSION_TIMEOUT = 12;

    public static final String EVENT_TYPE_ATTRACTION = "Attraction";
    public static final String SEAT_TYPE_FREE_SEATING = "Free Seating";
    public static final String SEAT_TYPE_ARRANGE_SEATING = "Reserved Seating";
    public static final String IMAGE_DOWNLOAD_PATH = Environment.getExternalStorageDirectory() + "/.TicketNinja/";

    public static final String CONTACT_REPLACE_EXPRESSION = "[^+0-9]";
    public static final int REQUEST_CODE_GOOGLE_SIGN_IN = 1001;
    public static final int PERMISSION_FOR_FACEBOOK_LOGIN = 2001;

    public static class Gender {
        public static final String MALE = "Male";
        public static final String FEMALE = "Female";
    }

    public static class ActivityForResult {
        public static final int CAMERA = 1010;
        public static final int GALLERY = 1011;
        public static final int CROP_IMAGE = 1012;

        public static final int LOGIN = 1013;
        public static final int REGISTER = 1014;
        public static final int EDIT_PROFILE = 1015;
        public static final int FILE_SELECT = 1016;
        public static final int PICK_CONTACT = 1017;
        public static final int NOTIFICATION = 1018;

        public static final int FROM_MY_TICKETS = 1019;
        public static final int SHARE_TICKETS = 1020;
        public static final int UPDATE_CONTACT_DETAIL = 1021;
    }

    public static class RequestPermissions {
        public static final int CAMERA = 1111;
        public static final int GALLERY = 1112;
        public static final int WRITE_EXTERNAL_STORAGE = 1113;
        public static final int WRITE_EXTERNAL_STORAGE_FOR_BGDWNLD = 1115;
        public static final int READ_EXTERNAL_STORAGE = 1114;
        public static final int READ_CONTACTS = 1115;
    }

    public static class ScreenExtras {
        public static final String PAGE = "page";
        public static final String MESSAGE = "message";
        public static final String FROM_SCREEN = "extraFromScreen";
        public static final String REDIRECT_TO = "extraRedirectTo";
        public static final String PAGE_TYPE = "page_type";
        public static final String DATA = "extraData";
        public static final String EMAIL = "extraEmail";

        public static final int FROM_SPLASH = 101;
        public static final int FROM_ACTIVITY_MAIN = 102;
        public static final int FROM_ACTIVITY_MY_PROFILE = 103;
        public static final int FROM_ACTIVITY_NOTIFICATION = 104;
        public static final int FROM_ACTIVITY_PAYMENT = 105;
        public static final int FROM_ACTIVITY_SERVICE = 106;
        public static final int FROM_ACTIVITY_BOOKING_HISTORY = 107;
        public static final int FROM_ACTIVITY_MY_TICKETS = 108;


        public static final int PRESS_LOGIN = 104;
        public static final int PRESS_SIGNUP = 105;

        public static final String EVENT_TYPE_NAME = "extraEventTypeName";
        public static final String EVENT_TYPE_KEY = "extraEventTypeKey";
        public static final String EVENT_DATA = "extraEventData";
        public static final String EVENT_ID = "extraEventId";
        public static final String SHOW_ID = "extraShowId";
        public static final String EVENT_NAME = "extraEventName";
        public static final String EVENT_DISCOUNT_PER = "extraDiscountPer";
        public static final String EVENT_DISCOUNT_LABEL = "extraDiscountLabel";
        public static final String PLAN_CATEGORY_ID = "extraPlanCategoryId";
        public static final String SEAT_COUNT = "extraSeatCount";
        public static final String EVENT_TIME = "extraEventTime";
        public static final String EVENT_DATE = "extraEventDate";
        public static final String BOOKING_DATA = "extraBookingData";
        public static final String EVENT_SESSION_ID = "extraSessionId";
        public static final String EVENT_HISTORY_TYPE = "extraHistoryType";
        public static final String EVENT_TRANSACTION_ID = "extraTransactionId";
        public static final String EVENT_TRANSACTION_TYPE = "extraTransactionType";
        public static final String EVENT_SESSION_DATA = "extraSessionData";
        public static final String EVENT_NOTE_LIST = "extraNoteList";
        public static final String KEY_SECTION = "extraSection";
        public static final String KEY_IMAGE_TITLE = "extraImageTitle";
        public static final String KEY_IMAGE_LINK = "extraImageLink";
        public static final String CONTACT_ACTION = "extraContactActionEdit";

        public static final String PRIVACY = "privacy";
        public static final String SHARE_VIA = "extraShareVia";
        public static final String TRAN_ID = "tranid";
        public static final String BOOKING = "Booking";
        public static final String TICKET = "Ticket";


        public static final String TYPE = "type";
        public static final String IMAGE_URI = "imageUri";
        public static final String YOU_TUBE_CODE = "VideoCode";
    }

    // HISTORY TYPE
    public static class HistoryType {
        public static final String FUTURE = "future";
        public static final String PAST = "old";
    }

    // REGISTRATION MODE
    public static class RegisterMode {
        public static final String INSERT = "IN";
        public static final String UPDATE = "UP";
    }

    // EVENT DAY_TYPE
    public static class EventDayType {
        public static final String SINGLE_DAY = "Singleday";
        public static final String MULTIPLE_DAY = "Multipleday";
        public static final String MULTI_DAY_MULTI_SHOW = "Multipledayevent";
        public static final String PERPETUAL = "Perpetual";
        public static final String OTHER = "";
    }

    // NOTIFICATION TYPE
    public static class NotificationType {
        public static final int EVENT = 1;
        public static final int TICKET = 2;
        public static final int BULK_EVENT_DETAIL = 3;
    }

    //Define the list of accepted constants
    @StringDef({EventDayType.SINGLE_DAY, EventDayType.MULTIPLE_DAY, EventDayType.MULTI_DAY_MULTI_SHOW, EventDayType.PERPETUAL, EventDayType.OTHER})
    @Retention(RetentionPolicy.SOURCE)
    public @interface EventDayTypeAnt {
    }

    //Define the list of accepted constants
    @StringDef({RegisterMode.INSERT, RegisterMode.UPDATE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface RegisterWSMode {
    }

    // PAYMENT TYPE
    public static class TicketStatus {
        public static final String OPEN = "Open";
        public static final String RECEIVED = "Received";
        public static final String USED = "Used";
        public static final String REVOKE = "Revoke";
        public static final String SHARED = "Shared";
    }

    // PAYMENT TYPE
    public static class PaymentType {
        public static final String COD = "COD";
        public static final String ONLINE = "ONLINE";
        public static final String RSVP = "RSVP";
        public static final String COMPLEMENTARY = "Complementary";
    }

    // MERCHANT TYPE
    public static class MerchantType {
        public static final String RAZORPAY = "Razorpay";
        public static final String PAYTM = "Paytm";
    }

    //Define the list of accepted constants
    @StringDef({PaymentType.COD, PaymentType.ONLINE, PaymentType.RSVP, PaymentType.COMPLEMENTARY})
    @Retention(RetentionPolicy.SOURCE)
    public @interface PaymentWSType {
    }

    //Define the list of accepted constants
    @StringDef({MerchantType.RAZORPAY, MerchantType.PAYTM})
    @Retention(RetentionPolicy.SOURCE)
    public @interface MerchantWSType {
    }

    // PAYMENT
    public static final int PAYMENT_SUCCESS = 1;
    public static final int PAYMENT_FAILED = 2;
    public static final int PAYMENT_PENDING = 3;
    public static final String PAYMENT_SUCCESS_TITLE = "success";
    public static final String PAYMENT_FAILED_TITLE = "fail";
    public static final String PAYMENT_PENDING_TITLE = "pending";
    public static final String PAYMENT_CANCELED_TITLE = "cancel";

    public static class SelectTicket {

        public static final String SELECTED_DATE = "date";
        public static final String IMAGE_ID = "imageid";
        public static final String COUNTRYNAME = "cname";
        public static final String TIME = "time";
        public static final String TIME_LIST = "timelist";
        public static final String CATEGORYLIST = "categorylist";
        public static final String COUNTRY_CODE = "code";


    }

    public static class Register {
        public static final String EMAIL = "email";
    }

    public static String generateQRCodeURL(String value) {
        return "https://api.qrserver.com/v1/create-qr-code/?data=" + value + "&size=100x100.png";
    }

    // SHARE VIA
    public static class ShareVia {
        public static final int MOBILE_NO = 1;
        public static final int EMAIL = 2;
        public static final int CONTACT = 3;
    }

    public class ContactActions {
        public static final String EDIT ="mActionEdit" ;
        public static final String CREATE ="mActionCreate" ;
    }
}
