<?xml version="1.0" encoding="utf-8"?><!--
  ~ Copyright (c) 2017. Suthar Rohit
  ~ Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
  ~ <a href="http://RohitSuthar.com/">Suthar Rohit</a>
  ~
  ~ @author Suthar Rohit
  -->

<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:orientation="vertical"
    tools:background="@color/themeGray">

    <RelativeLayout
        android:id="@+id/rl_main"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:layout_gravity="center_vertical"
        android:layout_marginBottom="@dimen/booking_history_item_margin_vertical"
        android:layout_marginEnd="@dimen/booking_history_item_margin_horizontal"
        android:layout_marginLeft="@dimen/booking_history_item_margin_horizontal"
        android:layout_marginRight="@dimen/booking_history_item_margin_horizontal"
        android:layout_marginStart="@dimen/booking_history_item_margin_horizontal"
        android:layout_marginTop="@dimen/booking_history_item_margin_vertical"
        android:background="@color/themeGray"
        android:divider="@android:color/transparent"
        android:orientation="vertical">

        <RelativeLayout
            android:id="@+id/rlTicketTop"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:background="@drawable/shadow_demo">

            <LinearLayout
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_marginLeft="1dp"
                android:layout_marginRight="1dp"
                android:layout_marginTop="1dp"
                android:background="@drawable/bg_white"
                android:orientation="vertical"
                android:padding="8dp">

                <RelativeLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content">

                    <TextView
                        android:id="@+id/tvEventDate"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_alignParentEnd="true"
                        android:layout_alignParentRight="true"
                        android:layout_marginTop="10dp"
                        android:background="@drawable/bg_event_list_data_line"
                        android:gravity="center"
                        android:paddingEnd="0dp"
                        android:paddingLeft="@dimen/event_list_content_padding"
                        android:paddingRight="0dp"
                        android:paddingStart="@dimen/event_list_content_padding"
                        android:textColor="@color/date_color"
                        android:textSize="@dimen/event_list_date_textSize"
                        tools:text="Jan\n11\nSUN" />

                    <LinearLayout
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_toLeftOf="@id/tvEventDate"
                        android:layout_toStartOf="@id/tvEventDate"
                        android:orientation="vertical">

                        <TextView
                            android:id="@+id/tvEventTitle"
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:textColor="@color/themeRed"
                            android:textSize="15sp"
                            android:transitionName="eventTitle"
                            tools:text="Raavan ki ramayan" />

                        <TextView
                            android:id="@+id/tvEventLocation"
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:layout_marginTop="2dp"
                            android:drawablePadding="5dp"
                            android:gravity="start"
                            android:textColor="@color/date_color"
                            android:textSize="13sp"
                            tools:text="Pundit Dindayal Upadhyay Auditorium" />

                        <TextView
                            android:id="@+id/tv_seatno"
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:layout_marginTop="3dp"
                            android:textColor="@android:color/black"
                            tools:text="Seat No : Free Seating" />

                        <TextView
                            android:id="@+id/tv_time"
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:drawableLeft="@drawable/ic_access_time_red"
                            android:drawablePadding="5dp"
                            android:drawableStart="@drawable/ic_access_time_red"
                            android:textColor="@color/themeRed"
                            tools:text="06:00 PM" />

                    </LinearLayout>
                </RelativeLayout>

                <org.apmem.tools.layouts.FlowLayout
                    android:id="@+id/ll_main"
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:layout_marginBottom="10dp"
                    android:layout_marginTop="5dp"
                    android:gravity="center_vertical"
                    android:orientation="horizontal">

                    <TextView
                        android:id="@+id/tvEventType"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginEnd="@dimen/event_tag_padding_horizontal"
                        android:layout_marginLeft="@dimen/event_tag_padding_horizontal"
                        android:layout_marginRight="@dimen/event_tag_padding_horizontal"
                        android:layout_marginStart="@dimen/event_tag_padding_horizontal"
                        android:layout_marginTop="@dimen/event_tag_padding_vertical"
                        android:background="@drawable/event_list_tag_bg"
                        android:fontFamily="@font/roboto"
                        android:textAllCaps="true"
                        android:textColor="@color/date_color"
                        android:textSize="@dimen/event_list_tag_textSize"
                        tools:text="DRAMA DRAMA DRAMA " />

                    <TextView
                        android:id="@+id/tvEventCategory"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginEnd="@dimen/event_tag_padding_horizontal"
                        android:layout_marginLeft="@dimen/event_tag_padding_horizontal"
                        android:layout_marginRight="@dimen/event_tag_padding_horizontal"
                        android:layout_marginStart="@dimen/event_tag_padding_horizontal"
                        android:layout_marginTop="@dimen/event_tag_padding_vertical"
                        android:background="@drawable/event_list_tag_bg"
                        android:fontFamily="@font/roboto"
                        android:textAllCaps="true"
                        android:textColor="@color/date_color"
                        android:textSize="@dimen/event_list_tag_textSize"
                        tools:text="Bollywood" />

                    <TextView
                        android:id="@+id/tvEventLanguage"
                        android:layout_width="wrap_content"
                        android:layout_height="wrap_content"
                        android:layout_marginLeft="@dimen/event_tag_padding_horizontal"
                        android:layout_marginStart="@dimen/event_tag_padding_horizontal"
                        android:layout_marginTop="@dimen/event_tag_padding_vertical"
                        android:background="@drawable/event_list_tag_bg"
                        android:fontFamily="@font/roboto"
                        android:textAllCaps="true"
                        android:textColor="@color/date_color"
                        android:textSize="@dimen/event_list_tag_textSize"
                        tools:text="Hindi" />
                </org.apmem.tools.layouts.FlowLayout>
            </LinearLayout>
        </RelativeLayout>

        <RelativeLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_below="@id/rlTicketTop"
            android:background="@drawable/demo_bg"
            android:padding="10dp">

            <TextView
                android:id="@+id/tv_bid"
                android:layout_width="wrap_content"
                android:layout_height="wrap_content"
                android:layout_centerVertical="true"
                android:layout_marginLeft="10dp"
                android:layout_marginStart="10dp"
                android:fontFamily="@font/helvetica_neue_medium"
                android:text="Booking ID - "
                android:textSize="13sp" />

            <TextView
                android:id="@+id/tvBookingId"
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                android:layout_centerVertical="true"
                android:layout_marginEnd="10dp"
                android:layout_marginLeft="10dp"
                android:layout_marginRight="10dp"
                android:layout_marginStart="10dp"
                android:layout_toEndOf="@id/tv_bid"
                android:layout_toRightOf="@id/tv_bid"
                android:drawableEnd="@drawable/ic_ticket_list_logo"
                android:drawableRight="@drawable/ic_ticket_list_logo"
                android:fontFamily="@font/helvetica_neue_medium"
                android:textColor="@color/themeRed"
                android:textSize="15sp"
                tools:text="AKG95011" />
        </RelativeLayout>

        <RelativeLayout
            android:id="@+id/rl_line"
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:layout_below="@id/rlTicketTop"
            android:layout_gravity="bottom"
            android:layout_marginTop="-10dp">

            <ImageView
                android:id="@+id/iv_mail_left"
                android:layout_width="20dp"
                android:layout_height="20dp"
                android:layout_alignParentTop="true"
                android:src="@drawable/half_round" />

            <ImageView
                android:id="@+id/iv_dot"
                android:layout_width="match_parent"
                android:layout_height="2dp"
                android:layout_centerVertical="true"
                android:layout_toEndOf="@id/iv_mail_left"
                android:layout_toLeftOf="@id/iv_mail_right"
                android:layout_toRightOf="@id/iv_mail_left"
                android:layout_toStartOf="@id/iv_mail_right"
                android:layerType="software"
                android:src="@drawable/doted" />

            <ImageView
                android:id="@+id/iv_mail_right"
                android:layout_width="20dp"
                android:layout_height="20dp"
                android:layout_alignParentEnd="true"
                android:layout_alignParentRight="true"
                android:layout_alignParentTop="true"
                android:src="@drawable/half_round_right" />
        </RelativeLayout>

    </RelativeLayout>
</LinearLayout>