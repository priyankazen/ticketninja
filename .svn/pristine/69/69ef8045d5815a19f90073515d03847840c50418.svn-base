<?xml version="1.0" encoding="utf-8"?><!--
  ~ Copyright (c) 2017. Suthar Rohit
  ~ Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
  ~ <a href="http://RohitSuthar.com/">Suthar Rohit</a>
  ~
  ~ @author Suthar Rohit
  -->

<resources>

    <dimen name="toolbar_title_textSize">18sp</dimen>
    <dimen name="toolbar_divider_height">1dp</dimen>
    <dimen name="divider_height">0.5dp</dimen>
    <dimen name="controlDistance">25dp</dimen>
    <dimen name="dialog_minWidth">300dp</dimen>
    <dimen name="form_view_distance">10dp</dimen>
    <dimen name="form_view_distance_horizontal">10dp</dimen>

    <!-- SPLASH -->
    <dimen name="splash_logo_padding">50dp</dimen>

    <!-- HOME -->
    <dimen name="home_site_menu_stroke_size">0.5dp</dimen>

    <dimen name="event_tag_padding_vertical">5dp</dimen>
    <dimen name="event_tag_padding_horizontal">4dp</dimen>
    <dimen name="event_list_content_padding">8dp</dimen>
    <dimen name="event_list_title_textSize">14sp</dimen>
    <dimen name="event_list_date_textSize">12sp</dimen>
    <dimen name="event_list_tag_textSize">11sp</dimen>
    <dimen name="event_list_tag_stroke_size">0.5dp</dimen>
    <dimen name="event_list_location_textSize">11sp</dimen>
    <dimen name="event_list_buy_ticket_textSize">11sp</dimen>
    <dimen name="event_list_buy_ticket_minHeight">28dp</dimen>
    <dimen name="event_list_recycler_view_margin_horizontal">5dp</dimen>
    <dimen name="city_list_minHeight">50dp</dimen>

    <!-- SEARCH -->
    <dimen name="search_history_textSize">16sp</dimen>
    <dimen name="search_title_padding_horizontal">10dp</dimen>

    <!-- MY ACCOUNT -->
    <dimen name="account_username_textSize">20sp</dimen>
    <dimen name="account_subHeader_textSize">16sp</dimen>
    <dimen name="account_header_paddingTop">25dp</dimen>

    <!-- MORE MENU -->
    <dimen name="city_icon_selector_size">10dp</dimen>
    <dimen name="city_icon_selector_padding">5dp</dimen>
    <dimen name="more_menu_minHeight">60dp</dimen>
    <dimen name="more_menu_padding">5dp</dimen>
    <dimen name="more_menu_item_textSize">16sp</dimen>

    <!-- LOGIN / REGISTER / FORGOT PASSWORD -->
    <dimen name="login_logo_height">150dp</dimen>
    <dimen name="login_logo_width">180dp</dimen>
    <dimen name="login_input_textSize">15sp</dimen>
    <dimen name="login_button_textSize">15sp</dimen>
    <dimen name="event_detail_button_textSize">17sp</dimen>

    <!-- CARD -->
    <dimen name="card_corner_radius">4dp</dimen>
    <dimen name="card_elevation">3dp</dimen>
    <dimen name="card_horizontal_margin">15dp</dimen>
    <dimen name="card_inner_padding">5dp</dimen>
    <dimen name="card_margin">10dp</dimen>
    <dimen name="card_padding">5dp</dimen>
    <dimen name="card_paddingLeft">5dp</dimen>
    <dimen name="card_paddingRight">5dp</dimen>
    <dimen name="card_paddingTop">20dp</dimen>
    <dimen name="card_paddingBottom">20dp</dimen>

    <!-- BUTTON -->
    <dimen name="button_minWidth">120dip</dimen>
    <dimen name="button_minHeight">40dip</dimen>
    <dimen name="button_minHeightSmall">36dip</dimen>
    <!-- Default insets (outer padding) around buttons -->
    <dimen name="button_inset_vertical_material">6dp</dimen>
    <dimen name="button_inset_horizontal_material">@dimen/control_inset_material</dimen>
    <!-- Default inner padding within buttons -->
    <dimen name="button_padding_vertical_material">@dimen/control_padding_material</dimen>
    <dimen name="button_padding_horizontal_material">8dp</dimen>
    <!-- Default insets (outer padding) around controls -->
    <dimen name="control_inset_material">4dp</dimen>
    <!-- Default inner padding within controls -->
    <dimen name="control_padding_material">4dp</dimen>
    <!-- Default rounded corner for controls -->
    <dimen name="control_corner_material">5dp</dimen>
    <dimen name="control_corner_large">2dp</dimen>
    <dimen name="control_corner_extra_large">5dp</dimen>

    <dimen name="view_pager_height">180dp</dimen>

    <!-- EVENT DETAIL -->
    <dimen name="parallax_height">150dp</dimen>
    <dimen name="event_list_invite_only_textSize">11sp</dimen>
    <dimen name="event_list_invite_only_padding_horizontal">15dp</dimen>
    <dimen name="event_list_invite_only_padding_vertical">3dp</dimen>
    <dimen name="event_list_invite_only_corner_radius">3dp</dimen>
    <dimen name="event_detail_header_title_textSize">15sp</dimen>
    <dimen name="event_detail_header_sub_title_textSize">13sp</dimen>
    <dimen name="event_detail_side_margin">10dp</dimen>
    <dimen name="event_detail_section_margin">5dp</dimen>
    <dimen name="event_detail_section_title_textSize">16sp</dimen>
    <dimen name="event_detail_plan_label_textSize">11sp</dimen>
    <dimen name="event_detail_plan_image_size">70dp</dimen>
    <dimen name="event_detail_timing_icon_size">40dp</dimen>
    <dimen name="event_detail_timing_icon_margin">20dp</dimen>
    <dimen name="event_detail_timing_textSize">12sp</dimen>
    <dimen name="event_detail_price_range_textSize">14sp</dimen>
    <dimen name="event_detail_organizer_label_textSize">10sp</dimen>
    <dimen name="event_detail_organizer_photo_size">40dp</dimen>
    <dimen name="event_detail_artist_photo_size">80dp</dimen>
    <dimen name="event_detail_artist_title_textSize">15sp</dimen>
    <dimen name="event_detail_artist_detail_textSize">13sp</dimen>
    <dimen name="event_detail_detail_textSize">12sp</dimen>
    <dimen name="event_detail_terms_item_textSize">14sp</dimen>
    <dimen name="event_detail_terms_item_bullet_textSize">12sp</dimen>
    <dimen name="event_detail_show_header_date_textSize">13sp</dimen>
    <dimen name="event_detail_show_header_date_stroke_width">1dp</dimen>
    <dimen name="event_detail_show_header_year_textSize">12sp</dimen>
    <dimen name="event_detail_show_header_title_textSize">17sp</dimen>
    <dimen name="event_detail_show_header_small_detail_textSize">10sp</dimen>
    <dimen name="event_detail_show_header_divider_height">1dp</dimen>
    <dimen name="event_detail_show_header_main_padding">5dp</dimen>

    <!-- EVENT PRICING -->
    <dimen name="event_pricing_side_margin">10dp</dimen>
    <dimen name="event_pricing_title_textSize">18sp</dimen>
    <dimen name="event_pricing_ticket_count_textSize">18sp</dimen>
    <dimen name="event_pricing_ticket_count_size">40dp</dimen>
    <dimen name="event_pricing_date_chooser_stroke_width">0.5dp</dimen>
    <dimen name="event_pricing_date_chooser_dayName_textSize">14sp</dimen>
    <dimen name="event_pricing_date_chooser_day_textSize">22sp</dimen>
    <dimen name="event_pricing_date_chooser_time_textSize">14sp</dimen>
    <dimen name="event_pricing_date_chooser_time_padding_vertical">8dp</dimen>
    <dimen name="event_pricing_date_chooser_time_padding_horizontal">6dp</dimen>
    <dimen name="event_pricing_date_corner_radius">8dp</dimen>
    <dimen name="event_pricing_date_inner_padding_horizontal">2dp</dimen>
    <dimen name="event_pricing_date_inner_padding_vertical">2dp</dimen>
    <dimen name="event_pricing_show_discount_textSize">18sp</dimen>
    <dimen name="event_pricing_show_title_textSize">18sp</dimen>

    <!-- ATTRACTION - SELECT DATE & TIME -->
    <dimen name="date_time_inner_padding_horizontal">15dp</dimen>
    <dimen name="date_time_inner_padding_vertical">10dp</dimen>
    <dimen name="date_time_inner_header_horizontal">5dp</dimen>
    <dimen name="date_time_header_drawable_padding">6dp</dimen>
    <dimen name="date_time_inner_view_distance_small">10dp</dimen>
    <dimen name="date_time_inner_view_distance">20dp</dimen>
    <dimen name="date_time_detail_drawable_padding">8dp</dimen>
    <dimen name="date_time_detail_textSize">15sp</dimen>

    <!-- ORDER SUMMARY -->
    <dimen name="order_summary_inner_padding">10dp</dimen>
    <dimen name="order_summary_title_textSize">16sp</dimen>
    <dimen name="order_summary_venue_textSize">14sp</dimen>
    <dimen name="order_summary_seat_no_label_textSize">14sp</dimen>
    <dimen name="order_summary_seat_no_value_textSize">16sp</dimen>
    <dimen name="order_summary_amount_label_textSize">14sp</dimen>
    <dimen name="order_summary_amount_label_tax_textSize">11sp</dimen>
    <dimen name="order_summary_amount_value_textSize">16sp</dimen>
    <dimen name="order_summary_category_item_name_textSize">13sp</dimen>
    <dimen name="order_summary_category_item_price_textSize">13sp</dimen>
    <dimen name="order_summary_category_item_seat_no_label_textSize">11sp</dimen>
    <dimen name="order_summary_category_item_seat_no_textSize">11sp</dimen>
    <dimen name="order_summary_book_date_label_textSize">12sp</dimen>
    <dimen name="order_summary_book_date_value_textSize">12sp</dimen>

    <!-- PURCHASE TICKET -->
    <dimen name="purchase_ticket_main_padding">10dp</dimen>
    <dimen name="purchase_ticket_header_title_padding_horizontal">10dp</dimen>
    <dimen name="purchase_ticket_header_title_textSize">15sp</dimen>
    <dimen name="purchase_ticket_section_title_textSize">15sp</dimen>
    <dimen name="purchase_ticket_edittext_textSize">15sp</dimen>
    <dimen name="purchase_ticket_bottom_hint_textSize">11sp</dimen>
    <dimen name="purchase_ticket_terms_title_textSize">15sp</dimen>
    <dimen name="purchase_ticket_terms_desc_textSize">13sp</dimen>

    <!-- EDIT PROFILE -->
    <dimen name="edit_profile_floating_hint_textSize">11sp</dimen>
    <dimen name="edit_profile_edittextsize">18sp</dimen>
    <dimen name="edit_profile_edittext_textSize">15sp</dimen>
    <dimen name="btn_customercare">10dp</dimen>


    <!--CUSTOMER CARE-->
    <dimen name="btn_customercare_height">45dp</dimen>

    <!-- BOOKING CONFIRMATION -->
    <dimen name="booking_id_label_textSize">12sp</dimen>
    <dimen name="booking_id_value_textSize">14sp</dimen>
    <dimen name="booking_layout_margin">10dp</dimen>
    <dimen name="booking_layout_inner_margin">8dp</dimen>
    <dimen name="booking_table_row_margin_horizontal">8dp</dimen>
    <dimen name="booking_table_row_margin_vertical">8dp</dimen>
    <dimen name="booking_barcode_dialog_corner_radius">5dp</dimen>

    <!-- MORE MENUS -->
    <dimen name="more_about_us_textSize">16sp</dimen>
    <dimen name="more_terms_header_textSize">15sp</dimen>
    <dimen name="more_terms_content_textSize">13sp</dimen>
    <dimen name="more_terms_content_padding">5dp</dimen>
    <dimen name="more_contact_info_drawable_padding">5dp</dimen>

    <!-- OTP VERIFICATION -->
    <dimen name="otp_verification_label_textSize">18sp</dimen>
    <dimen name="otp_verification_pin_textSize">26sp</dimen>


    <!-- BOOKING HISTORY-->
    <dimen name="booking_history_main_height">200dp</dimen>
    <dimen name="booking_history_height_info">160dp</dimen>
    <dimen name="margin_from_top_line">150dp</dimen>
    <dimen name="booking_history_margin_left">5dp</dimen>
    <dimen name="booking_history_item_margin_vertical">5dp</dimen>
    <dimen name="booking_history_item_margin_horizontal">12dp</dimen>

    <dimen name="detail_barcode_size">70dp</dimen>

    <!-- M-TICKET -->
    <dimen name="m_ticket_title_textSize">16sp</dimen>
    <dimen name="m_ticket_location_textSize">14sp</dimen>
    <dimen name="m_ticket_booking_id_label_textSize">12sp</dimen>
    <dimen name="m_ticket_booking_id_value_textSize">14sp</dimen>
    <dimen name="m_ticket_detail_textSize">14sp</dimen>
    <dimen name="m_ticket_detail_textSize_mini">12sp</dimen>
    <dimen name="m_ticket_detail_mini_textSize">13sp</dimen>
    <dimen name="m_ticket_detail_info_textSize">11sp</dimen>
    <dimen name="m_ticket_detail_vertical_margin">3dp</dimen>
    <dimen name="m_ticket_detail_vertical_margin2">5dp</dimen>

    <!--VIEW TICKET-->
    <dimen name="mticket_title_textsize">20sp</dimen>
    <dimen name="mticket_subtitle_textsize">17sp</dimen>
    <dimen name="mticket_id_val_textsize">18sp</dimen>
    <dimen name="mticket_tag_textsize">13sp</dimen>
    <dimen name="mticket_secondary_textsize">14sp</dimen>
    <dimen name="mticket_poster_height">130dp</dimen>
    <dimen name="mticket_layout_margin">10dp</dimen>
    <dimen name="mticket_view_margin">15dp</dimen>
    <dimen name="mticket_view_topbottom_margin">10dp</dimen>
    <dimen name="mticket_sub_view_height">70dp</dimen>

    <!-- NOTIFICATION LIST -->
    <dimen name="notification_list_side_padding">8dp</dimen>

    <!-- SHARE TICKET -->
    <dimen name="share_dialog_item_textSize">15sp</dimen>
    <dimen name="share_dialog_item_padding_vertical">15sp</dimen>
    <dimen name="share_dialog_item_padding_horizontal">15sp</dimen>
    <dimen name="share_dialog_item_layout_padding_vertical">13sp</dimen>
    <dimen name="share_dialog_item_layout_padding_horizontal">15sp</dimen>
    <dimen name="share_via_contact_bg_corner_radius">10dp</dimen>
    <dimen name="share_via_contact_bg_padding_horizontal">10dp</dimen>
    <dimen name="share_via_contact_bg_padding_vertical">10dp</dimen>
    <dimen name="textsize_small">14sp</dimen>
    <dimen name="textsize_very_small">12sp</dimen>

</resources>