/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.database;

import android.database.Cursor;
import android.support.annotation.NonNull;

import in.ticketninja.objects.BookingTicketList;

/**
 * Created by Rohit on 07-11-2017.
 */

public class DB_MyTicketList {

    public static final String TABLE_NAME = "tbl_my_ticket_list";

    public static final String BOOKING_ID = "Booking_Id";
    public static final String EVENT_ID = "Event_Id";
    public static final String NAME = "Name";
    public static final String CATEGORY = "Category_Name";
    public static final String LANGUAGE = "Language";
    public static final String IMAGE_ID = "Detail_Image_Id";
    public static final String IMAGE_URL = "Detail_Image_URL";
    public static final String DATE = "Date";
    public static final String DATE_TO = "Date_to";
    public static final String DAY_NAME = "Day_Name";
    public static final String DAY_TYPE = "Day_Type";
    public static final String D_M_DATE = "D_M_Date";
    public static final String TIME = "Time";
    public static final String EVENT_TYPE = "E_Type";
    public static final String LOCATION_NAME = "Location_Name";
    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
    public static final String NO_OF_TICKETS = "NoofTicket";
    public static final String QR_CODE = "QR_Code";
    public static final String RATE = "Rate";
    public static final String SEASON_PASS = "Seasonpass";
    public static final String SEAT_NO = "Seat_No";
    public static final String SEAT_TYPE = "Seat_Type";
    public static final String SHARE_TO = "Share_To";
    public static final String SHARE_FROM = "Share_From";
    public static final String STATUS = "Status";
    public static final String TICKET_DESC = "Ticket_desc";
    public static final String TRANS_ID = "Trans_Id";
    public static final String TRANS_NO = "Trans_No";
    public static final String Y_DATE = "Y_Date";
    public static final String USER_ID = "user_id";

    public static final String CREATE_TABLE_SYNTAX = "CREATE TABLE IF NOT EXISTS " + DB_MyTicketList.TABLE_NAME
            + "(" +
            DB_MyTicketList.BOOKING_ID + " TEXT," +
            DB_MyTicketList.EVENT_ID + " TEXT," +
            DB_MyTicketList.NAME + " TEXT," +
            DB_MyTicketList.CATEGORY + " TEXT," +
            DB_MyTicketList.LANGUAGE + " TEXT," +
            DB_MyTicketList.IMAGE_ID + " TEXT," +
            DB_MyTicketList.IMAGE_URL + " TEXT," +
            DB_MyTicketList.DATE + " TEXT," +
            DB_MyTicketList.DATE_TO + " TEXT," +
            DB_MyTicketList.DAY_NAME + " TEXT," +
            DB_MyTicketList.DAY_TYPE + " TEXT," +
            DB_MyTicketList.D_M_DATE + " TEXT," +
            DB_MyTicketList.TIME + " TEXT," +
            DB_MyTicketList.EVENT_TYPE + " TEXT," +
            DB_MyTicketList.LOCATION_NAME + " TEXT," +
            DB_MyTicketList.LATITUDE + " TEXT," +
            DB_MyTicketList.LONGITUDE + " TEXT," +
            DB_MyTicketList.NO_OF_TICKETS + " TEXT," +
            DB_MyTicketList.QR_CODE + " TEXT," +
            DB_MyTicketList.RATE + " TEXT," +
            DB_MyTicketList.SEASON_PASS + " TEXT," +
            DB_MyTicketList.SEAT_NO + " TEXT," +
            DB_MyTicketList.SEAT_TYPE + " TEXT," +
            DB_MyTicketList.SHARE_FROM + " TEXT," +
            DB_MyTicketList.SHARE_TO + " TEXT," +
            DB_MyTicketList.STATUS + " TEXT," +
            DB_MyTicketList.TICKET_DESC + " TEXT," +
            DB_MyTicketList.TRANS_ID + " TEXT," +
            DB_MyTicketList.TRANS_NO + " TEXT," +
            DB_MyTicketList.Y_DATE + " TEXT," +
            DB_MyTicketList.USER_ID + " TEXT" +
            ")";


    public static final BookingTicketList getDataFields(@NonNull Cursor c) {
        BookingTicketList history = new BookingTicketList();

        history.Booking_Id = c.getString(c.getColumnIndex(DB_MyTicketList.BOOKING_ID));
        history.Event_Id = c.getInt(c.getColumnIndex(DB_MyTicketList.EVENT_ID));
        history.Name = c.getString(c.getColumnIndex(DB_MyTicketList.NAME));
        history.Category_Name = c.getString(c.getColumnIndex(DB_MyTicketList.CATEGORY));
        history.Language = c.getString(c.getColumnIndex(DB_MyTicketList.LANGUAGE));
        history.Detail_Image_Id = c.getString(c.getColumnIndex(DB_MyTicketList.IMAGE_ID));
        history.Detail_Image_URL = c.getString(c.getColumnIndex(DB_MyTicketList.IMAGE_URL));
        history.Date = c.getString(c.getColumnIndex(DB_MyTicketList.DATE));
        history.Date_to = c.getString(c.getColumnIndex(DB_MyTicketList.DATE_TO));
        history.Day_Name = c.getString(c.getColumnIndex(DB_MyTicketList.DAY_NAME));
        history.Day_Type = c.getString(c.getColumnIndex(DB_MyTicketList.DAY_TYPE));
        history.D_M_Date = c.getString(c.getColumnIndex(DB_MyTicketList.D_M_DATE));
        history.Time = c.getString(c.getColumnIndex(DB_MyTicketList.TIME));
        history.E_Type = c.getString(c.getColumnIndex(DB_MyTicketList.EVENT_TYPE));
        history.Latitude = c.getString(c.getColumnIndex(DB_MyTicketList.LATITUDE));
        history.Longitude = c.getString(c.getColumnIndex(DB_MyTicketList.LONGITUDE));
        history.Location_Name = c.getString(c.getColumnIndex(DB_MyTicketList.LOCATION_NAME));
        history.QR_Code = c.getString(c.getColumnIndex(DB_MyTicketList.QR_CODE));
        history.NoofTicket = c.getInt(c.getColumnIndex(DB_MyTicketList.NO_OF_TICKETS));
        history.Rate = c.getDouble(c.getColumnIndex(DB_MyTicketList.RATE));
        history.Seat_No = c.getString(c.getColumnIndex(DB_MyTicketList.SEAT_NO));
        history.Seat_Type = c.getString(c.getColumnIndex(DB_MyTicketList.SEAT_TYPE));
        history.Status = c.getString(c.getColumnIndex(DB_MyTicketList.STATUS));
        history.Share_To = c.getString(c.getColumnIndex(DB_MyTicketList.SHARE_TO));
        history.Share_From = c.getString(c.getColumnIndex(DB_MyTicketList.SHARE_FROM));
        history.Seasonpass = Boolean.parseBoolean(c.getString(c.getColumnIndex(DB_MyTicketList.SEASON_PASS)));
        history.Ticket_desc = c.getString(c.getColumnIndex(DB_MyTicketList.TICKET_DESC));
        history.Trans_Id = c.getInt(c.getColumnIndex(DB_MyTicketList.TRANS_ID));
        history.Trans_No = c.getString(c.getColumnIndex(DB_MyTicketList.TRANS_NO));
        history.Y_Date = c.getString(c.getColumnIndex(DB_MyTicketList.Y_DATE));
        history.USER_ID = c.getInt(c.getColumnIndex(DB_MyTicketList.USER_ID));

        return history;
    }


    /*{
        "Booking_Id":3901,
            "Category_Name":"Mazza Club",
            "Date":"2017-11-19",
            "Date_to":"2017-11-19",
            "Day_Type":"Singleday",
            "Detail_Image_Id":"524984cb-2921-46b1-be33-f42d74127ec4.jpg",
            "Detail_Image_URL":
        "https:\/\/demo.ticketninja.in:4430\/Admin\/Document\/524984cb-2921-46b1-be33-f42d74127ec4.jpg",
                "E_Type":"Event",
            "Latitude":"23.03430230953805",
            "Location_Name":"Rajpath Club-Ahmedabad",
            "Longitude":"72.50887602111811",
            "Name":"Papon Live",
            "NoofTicket":"1",
            "QR_Code":"T-NEF3904197",
            "Rate":2000.00,
            "Seasonpass":false,
            "Seat_No":"",
            "Seat_Type":"Free Seating",
            "Share_From":"",
            "Status":"Open",
            "Ticket_desc":"ADMIT ONE PERSON",
            "Time":"7:00 AM",
            "Time_Type":null,
            "Trans_Id":3904,
            "Trans_No":"NEF3904197"
    }*/

}
