/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.fragments;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import in.ticketninja.LandingActivity;
import in.ticketninja.MyMTicketsActivity;
import in.ticketninja.MyTicketsActivity;
import in.ticketninja.PastTicketActivity;
import in.ticketninja.R;
import in.ticketninja.adapters.MyTicketsAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.database.DatabaseHelper;
import in.ticketninja.objects.User_Ticket_Event;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestUserTicket;
import in.ticketninja.ws.response.CallbackUserTicket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MyTicketFragment#newInstance} factory method to
 * newInstance an instance of this fragment.
 */
public class MyTicketFragment extends Fragment {

    private CommonClass CC;
    private DatabaseHelper DB;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;

    private List<User_Ticket_Event> myTicketsList = new ArrayList<>();

    private RecyclerView recyclerView;
    private LinearLayout llErrorLayout;
    private ImageView ivErrorImage;
    private TextView tvErrorMessage, tvErrorButton;
    private Button btn_past_ticket;

    public MyTicketFragment() {
    }

    public static MyTicketFragment newInstance() {
        MyTicketFragment fragment = new MyTicketFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if (getArguments() != null) {
        }*/
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_my_ticket2, container, false);
        CC = new CommonClass(getActivity());
        DB = new DatabaseHelper(getActivity());
        mLogin = new LoginUtils(getActivity());
        mLoader = new SRKLoaderDialog(getActivity());

        // ACTIONBAR
        AppCompatActivity activity;
        if (getActivity() instanceof LandingActivity) {
            activity = (LandingActivity) getActivity();
        } else if (getActivity() instanceof MyTicketsActivity) {
            activity = (MyTicketsActivity) getActivity();
        } else {
            activity = null;
        }
        Toolbar toolbar = v.findViewById(R.id.toolbar);
        if (activity != null) {
            activity.setSupportActionBar(toolbar);
            if (activity.getSupportActionBar() != null) {
                ActionBar supportActionBar = activity.getSupportActionBar();
                supportActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                supportActionBar.setDisplayHomeAsUpEnabled(false);
                supportActionBar.setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);
                if (activity instanceof MyTicketsActivity) {
                    ImageView ivActionBack = v.findViewById(R.id.ivActionBack);
                    ivActionBack.setOnClickListener(view -> getActivity().onBackPressed());
                } else {
                    v.findViewById(R.id.ivActionBack).setVisibility(View.GONE);
                }
                TextView tvTitle = v.findViewById(R.id.tvTitle);
                tvTitle.setText(R.string.str_myticket);

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    v.findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        }

        recyclerView = v.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);

        llErrorLayout = v.findViewById(R.id.llErrorLayout);
        ivErrorImage = v.findViewById(R.id.ivErrorImage);
        tvErrorMessage = v.findViewById(R.id.tvErrorMessage);
        tvErrorButton = v.findViewById(R.id.tvErrorButton);
        btn_past_ticket = v.findViewById(R.id.btn_past_ticket);

        if (CC.isOnline()) {
            wsUserTicket();
        } else {
            myTicketsList = DB.getMyTickets(mLogin.getUserId());
            if (myTicketsList != null && myTicketsList.size() > 0) {
                invalidateData();
            } else {
                showNoInternet();
            }
        }

        // Inflate the layout for this fragment
        return v;
    }

    public void showNoInternet() {
        Glide.with(this)
                .load(R.drawable.ic_connection_lost)
                .placeholder(R.drawable.ic_connection_lost_static)
                .into(ivErrorImage);
        tvErrorMessage.setText(R.string.msg_no_internet);
        recyclerView.setVisibility(View.GONE);
        llErrorLayout.setVisibility(View.VISIBLE);

        tvErrorButton.setVisibility(View.VISIBLE);
        tvErrorButton.setText(R.string.btn_try_again);
        tvErrorButton.setOnClickListener(v -> {
            if (CC.isOnline()) {
                wsUserTicket();
            } else {
                showNoInternet();
            }
        });
    }

    public void showError(boolean showErrorButton) {
        Glide.with(this)
                .load(R.drawable.ic_no_result_found)
                .placeholder(R.drawable.ic_no_result_found)
                .into(ivErrorImage);
        recyclerView.setVisibility(View.GONE);
        llErrorLayout.setVisibility(View.VISIBLE);

        if (showErrorButton) {
            tvErrorButton.setVisibility(View.VISIBLE);
            tvErrorButton.setText(R.string.btn_book_now);
            tvErrorButton.setOnClickListener(v -> {
                Utility.hideKeyboard(getActivity());
                Intent i1 = new Intent(getActivity(), LandingActivity.class);
                i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i1);
            });
        } else {
            tvErrorButton.setVisibility(View.GONE);
        }

        btn_past_ticket.setVisibility(View.GONE);
        btn_past_ticket.setOnClickListener(v -> {
            Utility.hideKeyboard(getActivity());
            Intent i = new Intent(getActivity(), PastTicketActivity.class);
            startActivity(i);

        });
    }

    private void invalidateData() {
        Collections.reverse(myTicketsList);

        MyTicketsAdapter adapter = new MyTicketsAdapter(getContext(), myTicketsList);
        adapter.setOnItemClickListener((position, history) -> {
            if (history.getTicketList().size() > 0) {
                Intent intViewTicket = new Intent(getContext(), MyMTicketsActivity.class);
                intViewTicket.putExtra(Constant.ScreenExtras.BOOKING_DATA, history);
                startActivityForResult(intViewTicket, Constant.ActivityForResult.FROM_MY_TICKETS);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    private void wsUserTicket() {
        if (!mLoader.isShowing() && !getActivity().isFinishing()) mLoader.show();

        RequestUserTicket request = new RequestUserTicket(mLogin.getUserId());

        TicketNinjaAPI tnAPI = RestApi.createAPI();
        Call<CallbackUserTicket> call = tnAPI.UserTicket(request);
        call.enqueue(new Callback<CallbackUserTicket>() {
            @Override
            public void onResponse(Call<CallbackUserTicket> call, @NonNull Response<CallbackUserTicket> response) {
                try {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (response.body().data().isSuccess()) {

                            DB.clearMyTicketTable(mLogin.getUserId());
                            DB.clearMyTicketListTable(mLogin.getUserId());

                            myTicketsList = response.body().data().userTicketEvents();
                            if (myTicketsList != null && myTicketsList.size() > 0) {

                                for (int i = 0; i < myTicketsList.size(); i++) {
                                    DB.addMyTickets(myTicketsList.get(i), mLogin.getUserId());
                                }

                                if (getActivity() != null && !getActivity().isFinishing()) {
                                    if (ContextCompat.checkSelfPermission(getActivity(),
                                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                                            || ContextCompat.checkSelfPermission(getActivity(),
                                            android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                        ActivityCompat.requestPermissions(getActivity(),
                                                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                                                Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE);
                                    } else {
                                        DB.startDownloadingMyTicketsInBackground();
                                    }
                                }
                                myTicketsList = DB.getMyTickets(mLogin.getUserId());
                                invalidateData();

                            } else {
                                tvErrorMessage.setText(R.string.msg_no_history_records_found);
                                showError(true);
                            }

                        } else if (Validate.isNotNull(response.body().data().message())) {
                            tvErrorMessage.setText(response.body().data().message());
                            showError(false);

                        } else {
                            tvErrorMessage.setText(R.string.msg_something_wrong);
                            showError(false);
                        }
                    } else {
                        tvErrorMessage.setText(R.string.msg_something_wrong);
                        showError(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    if (!getActivity().isFinishing()) {
                        tvErrorMessage.setText(R.string.msg_something_wrong);
                        showError(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<CallbackUserTicket> call, @NonNull Throwable t) {
                t.printStackTrace();
                if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                tvErrorMessage.setText(R.string.msg_something_wrong);
                showError(false);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //DB.startDownloadingBookingHistoryInBackground();
                    DB.startDownloadingMyTicketsInBackground();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.ActivityForResult.FROM_MY_TICKETS) {
            try {
                myTicketsList = DB.getMyTickets(mLogin.getUserId());
                invalidateData();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
