/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import in.ticketninja.CitySelectionActivity;
import in.ticketninja.R;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.objects.CityList;

public class CityAdapter extends RecyclerView.Adapter implements Filterable {

    private List<CityList> dataList;
    private OnItemClickListener clickListener;
    private int mCheckedPosition = -1;
    private PreferencesUtils pref;

    private String Cityname;

    private CitySelectionActivity.CustomFilter mFilter;

    public CityAdapter(List<CityList> dataList, String cityName, Context context) {
        this.dataList = dataList;
        this.Cityname = cityName;

        mFilter = new CitySelectionActivity.CustomFilter(CityAdapter.this);
        pref = new PreferencesUtils(context);

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_city_adapter, parent, false);
        return new MenuHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof MenuHolder) {
            MenuHolder h = (MenuHolder) viewHolder;
            int p = h.getAdapterPosition();
            h.tvItemText.setText(dataList.get(p).name());

            if (dataList.get(p).name().equalsIgnoreCase(Cityname)) {
                dataList.get(p).setChecked(true);
                h.tvItemIcon.setSelected(true);
            } else {
                dataList.get(p).setChecked(false);
                h.tvItemIcon.setSelected(false);
            }

            h.itemView.setOnClickListener(v -> {
                if (clickListener != null)
                    clickListener.onItemClick(h.getAdapterPosition(), dataList.get(p));
            });
        }
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    // LOAD MORE LISTENER
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.clickListener = listener;
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    private static class MenuHolder extends RecyclerView.ViewHolder {

        TextView tvItemText;
        ImageView tvItemIcon;

        MenuHolder(@NonNull View itemView) {
            super(itemView);
            tvItemIcon = itemView.findViewById(R.id.tvItemIcon);
            tvItemText = itemView.findViewById(R.id.tvItemText);
            // tvItemName.setEnabled(false);
            /*Typeface tfRegular = TypefaceUtils.HelveticaRegular(itemView.getContext());
            tvItemText.setTypeface(tfRegular);*/
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int position, CityList menu);
    }

}
