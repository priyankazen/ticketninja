<?xml version="1.0" encoding="utf-8"?><!--
  ~ Copyright (c) 2017. Aditi PArikh
  ~ Developed by Aditi Parikhfor NicheTech Computer Solutions Pvt. Ltd. use only.
  -->

<android.support.design.widget.CoordinatorLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="@color/themeGray"
    android:orientation="vertical"
    tools:context="in.ticketninja.fragments.MTicketPageFragment">

    <android.support.v4.widget.NestedScrollView
        android:layout_width="match_parent"
        android:layout_height="match_parent">

        <LinearLayout
            android:layout_width="match_parent"
            android:layout_height="wrap_content"
            android:orientation="vertical"
            android:padding="@dimen/booking_layout_margin">

            <android.support.v7.widget.CardView
                android:layout_width="match_parent"
                android:layout_height="wrap_content"
                app:cardBackgroundColor="@color/cardview_light_background"
                app:cardCornerRadius="@dimen/card_corner_radius"
                app:cardElevation="@dimen/card_elevation">

                <LinearLayout
                    android:layout_width="match_parent"
                    android:layout_height="wrap_content"
                    android:orientation="vertical">

                    <ImageView
                        android:id="@+id/ivEventPoster"
                        android:layout_width="match_parent"
                        android:layout_height="@dimen/parallax_height"
                        android:adjustViewBounds="true"
                        android:contentDescription="@string/title_activity_main"
                        android:scaleType="fitXY"
                        android:transitionName="eventPoster"
                        tools:background="@color/themeBlack"
                        tools:src="@drawable/ic_placeholder"
                        tools:visibility="gone" />

                    <LinearLayout
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:layout_margin="@dimen/mticket_view_margin"
                        android:orientation="vertical">

                        <TextView
                            android:id="@+id/tv_event_title"
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:fontFamily="@font/roboto_medium"
                            android:textColor="@color/event_pricing_title_textColor"
                            android:textSize="@dimen/m_ticket_title_textSize"
                            android:transitionName="eventTitle"
                            tools:text="Farhan Akhtar-Live In Concert" />

                        <TextView
                            android:id="@+id/tv_event_location"
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:layout_marginTop="5dp"
                            android:drawableLeft="@drawable/ic_event_location"
                            android:drawablePadding="5dp"
                            android:drawableStart="@drawable/ic_event_location"
                            android:fontFamily="@font/roboto"
                            android:gravity="center_vertical"
                            android:textColor="@color/m_ticket_label_red_title_color"
                            android:textSize="@dimen/m_ticket_location_textSize"
                            tools:text="AES Ground, Ahmedabad" />

                    </LinearLayout>

                    <RelativeLayout
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:background="@color/event_detail_timing_background">

                        <FrameLayout
                            android:id="@+id/fl_barcode"
                            android:layout_width="wrap_content"
                            android:layout_height="wrap_content"
                            android:layout_alignParentEnd="true"
                            android:layout_alignParentRight="true"
                            android:layout_centerVertical="true"
                            android:background="@drawable/bg_my_ticket_barcode_line">

                            <ImageView
                                android:id="@+id/iv_barcode"
                                android:layout_width="@dimen/detail_barcode_size"
                                android:layout_height="@dimen/detail_barcode_size"
                                android:layout_gravity="center"
                                android:layout_marginEnd="10dp"
                                android:layout_marginLeft="10dp"
                                android:layout_marginRight="10dp"
                                android:layout_marginStart="10dp"
                                android:padding="10dp"
                                tools:src="@mipmap/ic_launcher" />

                            <TextView
                                android:id="@+id/tv_ticketShared"
                                android:layout_width="wrap_content"
                                android:layout_height="wrap_content"
                                android:layout_gravity="center"
                                android:background="@drawable/btn_theme_bg_buy_ticket"
                                android:gravity="center"
                                android:text="Ticket\nShared"
                                android:textAllCaps="true"
                                android:textColor="@color/themeWhite"
                                android:textSize="10sp"
                                android:visibility="gone"
                                tools:visibility="visible" />

                        </FrameLayout>

                        <LinearLayout
                            android:layout_width="match_parent"
                            android:layout_height="@dimen/mticket_sub_view_height"
                            android:layout_centerVertical="true"
                            android:layout_toLeftOf="@id/fl_barcode"
                            android:layout_toStartOf="@id/fl_barcode"
                            android:gravity="center"
                            android:orientation="horizontal"
                            android:weightSum="2">

                            <LinearLayout
                                android:id="@+id/ll_my_ticket_ticket_id"
                                android:layout_width="0dp"
                                android:layout_height="match_parent"
                                android:layout_gravity="center"
                                android:layout_weight="1"
                                android:gravity="center"
                                android:orientation="vertical">

                                <TextView
                                    android:id="@+id/tv_label_ticket_id"
                                    android:layout_width="match_parent"
                                    android:layout_height="wrap_content"
                                    android:fontFamily="@font/roboto"
                                    android:gravity="center"
                                    android:text="@string/mticket_ticket_id_label"
                                    android:textColor="@color/m_ticket_label_gray_title_color"
                                    android:textSize="@dimen/m_ticket_booking_id_label_textSize" />

                                <TextView
                                    android:id="@+id/tv_ticket_id"
                                    android:layout_width="match_parent"
                                    android:layout_height="wrap_content"
                                    android:layout_marginTop="3dp"
                                    android:fontFamily="@font/roboto_medium"
                                    android:gravity="center"
                                    android:textColor="@color/m_ticket_label_red_title_color"
                                    android:textSize="@dimen/m_ticket_booking_id_value_textSize"
                                    tools:text="EOH1065" />
                            </LinearLayout>

                            <LinearLayout
                                android:id="@+id/ll_my_ticket_booking_id"
                                android:layout_width="0dp"
                                android:layout_height="match_parent"
                                android:layout_gravity="center"
                                android:layout_weight="1"
                                android:background="@drawable/bg_my_ticket_barcode_line"
                                android:gravity="center"
                                android:orientation="vertical"
                                android:visibility="visible">

                                <TextView
                                    android:id="@+id/tv_label_booking_id"
                                    android:layout_width="match_parent"
                                    android:layout_height="wrap_content"
                                    android:fontFamily="@font/roboto"
                                    android:gravity="center"
                                    android:text="@string/mticket_booking_id_label"
                                    android:textColor="@color/m_ticket_label_gray_title_color"
                                    android:textSize="@dimen/m_ticket_booking_id_label_textSize" />

                                <TextView
                                    android:id="@+id/tv_booking_id"
                                    android:layout_width="match_parent"
                                    android:layout_height="wrap_content"
                                    android:layout_marginTop="3dp"
                                    android:fontFamily="@font/roboto_medium"
                                    android:gravity="center"
                                    android:textColor="@color/m_ticket_label_red_title_color"
                                    android:textSize="@dimen/m_ticket_booking_id_value_textSize"
                                    tools:text="EOH1065" />

                            </LinearLayout>

                        </LinearLayout>
                    </RelativeLayout>

                    <LinearLayout
                        android:layout_width="match_parent"
                        android:layout_height="wrap_content"
                        android:orientation="vertical"
                        android:padding="@dimen/mticket_view_margin">

                        <include
                            layout="@layout/layout_my_ticket"
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:layout_marginBottom="@dimen/mticket_view_topbottom_margin" />

                        <View
                            style="@style/booking_divider_section"
                            android:layout_marginTop="@dimen/mticket_view_topbottom_margin" />

                        <LinearLayout
                            android:id="@+id/llShared"
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:orientation="vertical">

                            <RelativeLayout
                                android:layout_width="match_parent"
                                android:layout_height="wrap_content"
                                android:layout_marginBottom="@dimen/m_ticket_detail_vertical_margin2"
                                android:layout_marginTop="@dimen/m_ticket_detail_vertical_margin2">

                                <Button
                                    android:id="@+id/btnShareRevoke"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:layout_alignParentEnd="true"
                                    android:layout_alignParentRight="true"
                                    android:layout_centerVertical="true"
                                    android:fontFamily="@font/roboto_medium"
                                    android:minWidth="100dp"
                                    android:text="@string/m_ticket_revoke"
                                    android:textSize="12sp" />

                                <TextView
                                    android:id="@+id/tvSharedLabel"
                                    android:layout_width="match_parent"
                                    android:layout_height="wrap_content"
                                    android:layout_centerVertical="true"
                                    android:layout_toLeftOf="@id/btnShareRevoke"
                                    android:layout_toStartOf="@id/btnShareRevoke"
                                    android:fontFamily="@font/roboto_medium"
                                    android:gravity="start|center_vertical"
                                    android:padding="5dp"
                                    android:textColor="@color/m_ticket_sub_label_gray_color"
                                    android:textSize="@dimen/m_ticket_detail_textSize_mini"
                                    tools:text="@string/m_ticket_shared_from" />

                            </RelativeLayout>

                            <View style="@style/booking_divider_section" />
                        </LinearLayout>

                        <TextView
                            android:id="@+id/tv_ticket_category"
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:layout_marginBottom="@dimen/m_ticket_detail_vertical_margin"
                            android:layout_marginTop="@dimen/m_ticket_detail_vertical_margin"
                            android:drawableLeft="@drawable/ic_red_m_ticket_ticket"
                            android:drawablePadding="5dp"
                            android:drawableStart="@drawable/ic_red_m_ticket_ticket"
                            android:fontFamily="@font/roboto_medium"
                            android:gravity="center_vertical"
                            android:padding="5dp"
                            android:textColor="@color/m_ticket_label_red_title_color"
                            android:textSize="@dimen/m_ticket_detail_textSize"
                            tools:text="The Manek Chawk entry" />

                        <View style="@style/booking_divider_section" />

                        <LinearLayout
                            android:id="@+id/llSeatNo"
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:orientation="vertical">

                            <RelativeLayout
                                android:layout_width="match_parent"
                                android:layout_height="wrap_content"
                                android:layout_marginBottom="@dimen/m_ticket_detail_vertical_margin2"
                                android:layout_marginTop="@dimen/m_ticket_detail_vertical_margin2">

                                <TextView
                                    android:id="@+id/tvSeatNoLabel"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:layout_centerVertical="true"
                                    android:drawableLeft="@drawable/ic_order_seatno"
                                    android:drawablePadding="5dp"
                                    android:drawableStart="@drawable/ic_order_seatno"
                                    android:fontFamily="@font/roboto_medium"
                                    android:gravity="start|center_vertical"
                                    android:padding="5dp"
                                    android:text="@string/label_seat_no"
                                    android:textColor="@color/m_ticket_sub_label_gray_color"
                                    android:textSize="@dimen/m_ticket_detail_textSize" />

                                <TextView
                                    android:id="@+id/tvSeatNo"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:layout_alignParentEnd="true"
                                    android:layout_alignParentRight="true"
                                    android:layout_centerVertical="true"
                                    android:layout_toEndOf="@id/tvSeatNoLabel"
                                    android:layout_toRightOf="@id/tvSeatNoLabel"
                                    android:fontFamily="@font/roboto_medium"
                                    android:gravity="end|center_vertical"
                                    android:padding="5dp"
                                    android:textColor="@color/themeRed"
                                    android:textSize="@dimen/m_ticket_detail_textSize"
                                    tools:text="G-14" />
                            </RelativeLayout>

                            <View style="@style/booking_divider_section" />
                        </LinearLayout>

                        <LinearLayout
                            android:id="@+id/llSeatType"
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:orientation="vertical">

                            <TextView
                                android:id="@+id/tvSeatType"
                                android:layout_width="wrap_content"
                                android:layout_height="wrap_content"
                                android:layout_marginBottom="@dimen/m_ticket_detail_vertical_margin2"
                                android:layout_marginTop="@dimen/m_ticket_detail_vertical_margin2"
                                android:drawableLeft="@drawable/ic_order_seatno"
                                android:drawablePadding="5dp"
                                android:drawableStart="@drawable/ic_order_seatno"
                                android:fontFamily="@font/roboto_medium"
                                android:gravity="start|center_vertical"
                                android:padding="5dp"
                                android:text="@string/label_seat_no"
                                android:textColor="@color/themeRed"
                                android:textSize="@dimen/m_ticket_detail_textSize" />

                            <View style="@style/booking_divider_section" />
                        </LinearLayout>

                        <TextView
                            android:id="@+id/tv_add_person"
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:layout_marginBottom="@dimen/m_ticket_detail_vertical_margin"
                            android:layout_marginTop="@dimen/m_ticket_detail_vertical_margin"
                            android:drawableEnd="@drawable/ic_red_m_ticket_person"
                            android:drawablePadding="5dp"
                            android:drawableRight="@drawable/ic_red_m_ticket_person"
                            android:fontFamily="@font/roboto_medium"
                            android:gravity="center_vertical"
                            android:padding="5dp"
                            android:textColor="@color/m_ticket_sub_label_gray_color"
                            android:textSize="@dimen/m_ticket_detail_textSize"
                            tools:text="Admit One Person" />

                        <View style="@style/booking_divider_section" />

                        <LinearLayout
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:layout_marginBottom="@dimen/m_ticket_detail_vertical_margin2"
                            android:layout_marginTop="@dimen/m_ticket_detail_vertical_margin2"
                            android:orientation="horizontal"
                            android:weightSum="2">

                            <RelativeLayout
                                android:layout_width="0dp"
                                android:layout_height="wrap_content"
                                android:layout_gravity="start|center_vertical"
                                android:layout_weight="1.1">

                                <TextView
                                    android:id="@+id/tv_email"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:drawableLeft="@drawable/ic_red_m_ticket_mail"
                                    android:drawablePadding="5dp"
                                    android:drawableStart="@drawable/ic_red_m_ticket_mail"
                                    android:fontFamily="@font/roboto_medium"
                                    android:gravity="start|center_vertical"
                                    android:text="@string/mticket_contact_email"
                                    android:textColor="@color/m_ticket_sub_label_gray_color"
                                    android:textSize="@dimen/m_ticket_detail_mini_textSize" />
                            </RelativeLayout>

                            <RelativeLayout
                                android:layout_width="0dp"
                                android:layout_height="wrap_content"
                                android:layout_weight="0.9"
                                android:gravity="end|center_vertical">

                                <TextView
                                    android:id="@+id/tv_phone"
                                    android:layout_width="wrap_content"
                                    android:layout_height="wrap_content"
                                    android:drawableLeft="@drawable/ic_red_m_ticket_call"
                                    android:drawablePadding="3dp"
                                    android:drawableStart="@drawable/ic_red_m_ticket_call"
                                    android:fontFamily="@font/roboto_medium"
                                    android:gravity="end|center_vertical"
                                    android:visibility="gone"
                                    android:text="@string/mticket_contact_phone"
                                    android:textColorLink="@color/m_ticket_sub_label_gray_color"
                                    android:textSize="@dimen/m_ticket_detail_mini_textSize" />
                            </RelativeLayout>
                        </LinearLayout>

                        <View style="@style/booking_divider_section" />

                        <TextView
                            android:id="@+id/tv_terms_and_condition"
                            android:layout_width="match_parent"
                            android:layout_height="wrap_content"
                            android:layout_marginTop="@dimen/mticket_view_topbottom_margin"
                            android:fontFamily="@font/roboto_light"
                            android:text="@string/mticket_terms_cnditions"
                            android:textColor="@color/m_ticket_sub_label_gray_color"
                            android:textSize="@dimen/m_ticket_detail_info_textSize" />
                    </LinearLayout>
                </LinearLayout>
            </android.support.v7.widget.CardView>
        </LinearLayout>
    </android.support.v4.widget.NestedScrollView>
</android.support.design.widget.CoordinatorLayout>
