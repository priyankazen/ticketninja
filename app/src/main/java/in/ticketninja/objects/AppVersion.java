package in.ticketninja.objects;

import in.ticketninja.common.Validate;
import in.ticketninja.ws.RestApi;

/**
 * Created by Rohit on 02-08-2017.
 */

public class AppVersion {

    private long ver_id = 0;
    private String package_id = "";
    private int app_version = 0;
    private String app_application_name = "";
    private String device_type = "";
    private String forcefully = "";
    private String message = "";
    private String createddate = "";
    private String updateddate = "";
    private boolean login_required = false;

    public long getId() {
        return ver_id;
    }

    public String getPackageId() {
        return package_id;
    }

    public int getAppVersion() {
        return app_version;
    }

    public String getDeviceType() {
        return device_type;
    }

    public boolean isForcefully() {
        return Validate.isNotNull(forcefully) && forcefully.equalsIgnoreCase("yes");
    }

    public String getMessage() {
        return message;
    }

    public int ErrorCode = 0;
    public String ErrorMessage = "";
    public String ServiceName = "";

    public boolean isSuccess() {
        return ErrorCode == RestApi.ErrorCode.SUCCESS;
    }

    public String message() {
        return ErrorMessage;
    }

    public boolean isLogin_required() {
        return login_required;
    }
}