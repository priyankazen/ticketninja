/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import in.ticketninja.common.Utility;
import in.ticketninja.ws.RestApi;

/**
 * Created by Rohit on 18-08-2017.
 */

public class BookingHistory implements Serializable {

    private static final long serialVersionUID = 6663408501416574232L;

    public long trans_id = 0;
    public long event_id = 0;
    public double amount = 0.0;
    private double charge = 0.0;
    public double processingfee = 0.0;
    public double tax = 0.0;
    private double coupon_per =  0.0;
    public Float total_processingfee = 0F;
    public double discount = 0.0;
    private double discount_per = 0.0;
    public double netamount = 0.0;
    public int ismticket = 0;
    public int coupon_id = 0;
    public int seasonpass = 0;
    public int is_live_ticket = 0;
    public int is_app_ticket = 0;
    private int total_ticket = 0;
    public int noofticket = 0;

    public String trans_no = "";
    public String qr_code = "";
    public String qr_code_url;
    public String trans_date = "";
    public String event_name = "";
    public String event_date = "";
    public String event_date_to = "";
    public String event_time = "";
    public String location_name = "";
    public String type = "";
    public String day_type = "";
    public String email = "";
    public String mobileno = "";
    public String trans_type = "";
    public String payment_type = "";
    public String payment_status = "";
    public String p_d_address = "";
    public String payment_id = "";
    public String language = "";
    public String category = "";
    public String detail_image_id = "";
    public String detail_image_url = "";
    public String latitude = "";
    public String longitude = "";
    public String coupon_code = "";
    public String tax_type = "";
    public String d_time_slot = "";
    public String e_type_icon = "";

    public List<BookingTicketList> ticket_list = new ArrayList<>();

    private String GSTIN_No = "";
    private String GST_Inv_No = "";
    public String Day_Name = "";
    public String Y_Date = "";
    public String M_D_Date = "";
    public String Seat_No = "";
    public String Seat_Type = "";
    public String USER_ID = "";


    //amount ,charge,processingfee, tax,total_processingfee,discount_per,discount,netamount,coupon_per


    public int isIs_live_ticket() {
        return is_live_ticket;
    }

    public int isIs_app_ticket() {
        return is_app_ticket;
    }

    public String getQr_code_url() {
        return qr_code_url;
    }

    public List<BookingTicketList> getTicketList() {
        return ticket_list;
    }

    public long getEventId() {
        return event_id;
    }

    public void setEventId(long event_Id) {
        event_id = event_Id;
    }

    public String getName() {
        return event_name;
    }

    public void setName(String name) {
        event_name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    private double getCharge() {
        return charge;
    }

    public String getDeliveryTimeSlot() {
        return d_time_slot;
    }

    public String getEvent_date() {
        return event_date;
    }

    public String getDateTo() {
        return event_date_to;
    }

    public String getDayType() {
        return day_type;
    }

    public String getImageId() {
        return detail_image_id;
    }

    public String getImageUrl() {
        return detail_image_url;
    }

    public double getDiscount() {
        return discount;
    }

    public String getType() {
        return type;
    }

    public String getTypeIcon() {
        return e_type_icon;
    }

    public String getEmail() {
        return email;
    }

    private String getGSTIN_No() {
        return GSTIN_No;
    }

    private String getGST_Inv_No() {
        return GST_Inv_No;
    }

    public int getIsmticket() {
        return ismticket;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getVenueName() {
        return location_name;
    }

   /* public String getMDDate() {
        return M_D_Date;
    }

    public String getDay() {
        return Day_Name;
    }
    public String getYear() {
        return Y_Date;
    }*/

    public String getMDDate() {
        return Utility.getDate(getEvent_date(), getDateTo());
    }

    public String getDay() {
        return Utility.getDayName(getEvent_date());
    }

    public String getYear() {
        return Utility.getYear(getEvent_date());
    }

    public String getMobileno() {
        return mobileno;
    }

    public double getNetamount() {
        return netamount;
    }

    public String getP_d_address() {
        return p_d_address;
    }

    private String getPayment_id() {
        return payment_id;
    }

    public String getPaymentStatus() {
        return payment_status;
    }

    public String getPaymentType() {
        return payment_type;
    }

    public double getProcessingfee() {
        return processingfee;
    }

    public String getQRCode() {
        return qr_code;
    }

    public double getTax() {
        return tax;
    }

    public String getTax_type() {
        return tax_type;
    }

    public String getEvent_time() {
        return event_time;
    }

    /*public int getTotalTicket() {
        return total_ticket;
    }*/

    public int getTotalTicket() {
        return noofticket;
    }

    public String getTransDate() {
        return trans_date;
    }

    public long getTrans_id() {
        return trans_id;
    }

    public String getBookingId() {
        return trans_no;
    }

   /* public String getTrans_no() {
        return trans_no;
    }*/

    private String getTransType() {
        return trans_type;
    }


    public String getCategory() {
        return category;
    }

    public String getLanguage() {
        return language;
    }

    public String getSeat_No() {
        return Seat_No;
    }

    public String getSeatType() {
        return Seat_Type;
    }

    public int isSeasonPass() {
        return seasonpass;
    }

    public String getCouponCode() {
        return coupon_code;
    }

    public int getCoupon_id() {
        return coupon_id;
    }

    public Float getTota_ProcessingFee() {
        return total_processingfee;
    }

    private int ErrorCode = 0;
    private String ErrorMessage = "";
    private String ServiceName = "";

    public boolean isSuccess() {
        return ErrorCode == RestApi.ErrorCode.SUCCESS;
    }

    public String message() {
        return ErrorMessage;
    }

    @Override
    public String toString() {
        return "BookingHistory{" +
                "trans_id=" + trans_id +
                ", trans_no='" + trans_no + '\'' +
                ", ticket_list=" + ticket_list +
                ", qr_code='" + qr_code + '\'' +
                ", qr_code_url='" + qr_code_url + '\'' +
                ", trans_date='" + trans_date + '\'' +
                ", event_id=" + event_id +
                ", event_name='" + event_name + '\'' +
                ", event_date='" + event_date + '\'' +
                ", event_date_to='" + event_date_to + '\'' +
                ", event_time='" + event_time + '\'' +
                ", location_name='" + location_name + '\'' +
                ", type='" + type + '\'' +
                ", day_type='" + day_type + '\'' +
                ", email='" + email + '\'' +
                ", mobileno='" + mobileno + '\'' +
                ", trans_type='" + trans_type + '\'' +
                ", payment_type='" + payment_type + '\'' +
                ", payment_status='" + payment_status + '\'' +
                ", p_d_address='" + p_d_address + '\'' +
                ", payment_id='" + payment_id + '\'' +
                ", language='" + language + '\'' +
                ", category='" + category + '\'' +
                ", detail_image_id='" + detail_image_id + '\'' +
                ", detail_image_url='" + detail_image_url + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", total_ticket=" + total_ticket +
                ", amount=" + amount +
                ", charge=" + charge +
                ", processingfee=" + processingfee +
                ", tax=" + tax +
                ", total_processingfee=" + total_processingfee +
                ", discount=" + discount +
                ", netamount=" + netamount +
                ", ismticket=" + ismticket +
                ", coupon_id=" + coupon_id +
                ", coupon_code='" + coupon_code + '\'' +
                ", seasonpass=" + seasonpass +
                ", is_live_ticket=" + is_live_ticket +
                ", is_app_ticket=" + is_app_ticket +
                ", tax_type='" + tax_type + '\'' +
                ", d_time_slot='" + d_time_slot + '\'' +
                ", e_type_icon='" + e_type_icon + '\'' +
                ", GSTIN_No='" + GSTIN_No + '\'' +
                ", GST_Inv_No='" + GST_Inv_No + '\'' +
                ", Day_Name='" + Day_Name + '\'' +
                ", Y_Date='" + Y_Date + '\'' +
                ", M_D_Date='" + M_D_Date + '\'' +
                ", Seat_No='" + Seat_No + '\'' +
                ", Seat_Type='" + Seat_Type + '\'' +
                ", USER_ID=" + USER_ID +
                ", ErrorCode=" + ErrorCode +
                ", ErrorMessage='" + ErrorMessage + '\'' +
                ", ServiceName='" + ServiceName + '\'' +
                ", noofticket='" + noofticket + '\'' +
                '}';
    }
}
