/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

/**
 * TicketNinja_Working(in.ticketninja.objects) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 13/6/17.
 *
 * @author Suthar Rohit
 */
public class Search_db {

    public int user_id;
    public String keyword;

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Search_db(int user_id, String keyword) {
        this.user_id = user_id;
        this.keyword = keyword;
    }

    @Override
    public String toString() {
        return "Search_db{" +
                "user_id=" + user_id +
                ", keyword='" + keyword + '\'' +
                '}';
    }
}
