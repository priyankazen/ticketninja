/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import java.io.Serializable;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 22-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class EventPriceCategory implements Serializable {

    private static final long serialVersionUID = 6663408501416574225L;

    private long category_id = 0;
    private long show_id = 0;
    private double mrp = 0;
    private double rate = 0;
    private int noofticket = 0;
    private double amount = 0;
    private int addons = 0;
    private int seasonpass = 0;
    private int identification = 0;
    private int total_seat = 0;//total_seat
    private double totalMRP = 0;
    private double totalRate = 0;

    private String category_name = "";
    private String show_name = "";
    private String event_date = "";
    private String time = "";
    private String seat_type = "";
    private String c_desc = "";
    private String seat_no = "";

    public long getCategory_id() {
        return category_id;
    }

    private double getTotalMRP() {
        return totalMRP;
    }

    public void setTotalMRP(double totalMRP) {
        this.totalMRP = totalMRP;
    }

    public void setMrp(double mrp) {
        this.mrp = mrp;
    }

    public double getMrp() {
        return mrp;
    }

    public EventPriceCategory() {
    }

    public long id() {
        return category_id;
    }

    public String name() {
        return category_name;
    }

    public String description() {
        return c_desc;
    }

    public int noOfTicket() {
        return noofticket;
    }

    public void noOfTicket(int noOfTicket) {
        noofticket = noOfTicket;
    }

    public double amount() {
        return amount;
    }

    public void amount(double amount) {
        this.amount = amount;
    }

    public double rate() {
        return rate;
    }

    public String seatNo() {
        return seat_no;
    }

    public String seatType() {
        return seat_type;
    }

    public String eventDate() {
        return event_date;
    }

    public long showId() {
        return show_id;
    }

    public int isAddons() {
        return addons;
    }

    public int isSeasonPass() {
        return seasonpass;
    }

    public int stock() {
        return total_seat;
    }

    public String time() {
        return time;
    }

    public int isIdentification() {
        return identification;
    }

    public double getTotalRate() {
        return totalRate;
    }

    public void setTotalRate(double totalRate) {
        this.totalRate = totalRate;
    }

    @Override
    public String toString() {
        return "EventPriceCategory{" +
                "c_desc='" + c_desc + '\'' +
                ", category_id=" + category_id +
                ", category_name='" + category_name + '\'' +
                ", noofticket=" + noofticket +
                ", seat_no='" + seat_no + '\'' +
                ", seat_type='" + seat_type + '\'' +
                ", identification=" + identification +

                '}';
    }
}
