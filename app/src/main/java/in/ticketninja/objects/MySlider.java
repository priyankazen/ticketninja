/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 14-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class MySlider {

    //src,src

    public int event_id;
    public String event_name;
    public String src;
    public String type = "";//promo,event
    private String url = "";
    private String event_type = "";
    private String collection = "";
    private String Main_Image_Id;

    public MySlider() {

    }

    public int eventId() {
        return event_id;
    }
/*
    public String imageName() {
        return Main_Image_Id;
    }*/

    public MySlider(int event_Id, String main_Image_Id) {
        event_id = event_Id;
        Main_Image_Id = main_Image_Id;
    }

    public String getMainImageId() {
        return Main_Image_Id;
    }

    public String getMainImageURL() {
        return src;
    }

    public String getPosterImageURL() {
        return src;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setEvent_id(int event_id) {
        this.event_id = event_id;
    }

    public void setMain_Image_Id(String main_Image_Id) {
        Main_Image_Id = main_Image_Id;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEvent_type() {
        return event_type;
    }

    public void setEvent_type(String event_type) {
        this.event_type = event_type;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    @Override
    public String toString() {
        return "MySlider{" +
                "event_id=" + event_id +
                ", Main_Image_Id='" + Main_Image_Id + '\'' +
                ", main_image_url='" + src + '\'' +
                ", url='" + url + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
