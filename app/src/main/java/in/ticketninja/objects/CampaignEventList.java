package in.ticketninja.objects;

public class CampaignEventList {

    public int event_id = 0;
    private String event_name = "";
    private String day_type = "";
    private String location_name = "";
    private String city = "";
    private String state = "";
    private String event_date = "";
    private String event_date_to = "";
    private String event_time = "";
    private String category = "";
    private String language = "";
    private String main_image_url = "";
    private String poster_image_url = "";
    private String latitude;
    private String longitude;
    private int is_exclusive = 0;
    private int invite_only = 0;
    private String invite_status;
    private int invite_stock = 0;
    private int rsvp = 0;
    private int invite_rsvp = 0;
    private int general_sale = 0;
    private int discount = 0;
}
