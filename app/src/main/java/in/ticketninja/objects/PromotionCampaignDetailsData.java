package in.ticketninja.objects;

import java.util.ArrayList;

public class PromotionCampaignDetailsData {
    private String partner_name ="";
    private String coupon_code = "";
    private String expiry_date;
    private String status;
    private ArrayList<MyEvent> campaign_event_list = new ArrayList<>();
    private ArrayList<EventTerms> campaign_tandc_list = new ArrayList<>();

    public String getPartner_name() {
        return partner_name;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public String getStatus() {
        return status;
    }

    public ArrayList<MyEvent> getCampaign_event_list() {
        return campaign_event_list;
    }

    public ArrayList<EventTerms> getCampaign_tandc_list() {
        return campaign_tandc_list;
    }

    @Override
    public String toString() {
        return "PromotionCampaignDetailsData{" +
                "partner_name='" + partner_name + '\'' +
                ", coupon_code='" + coupon_code + '\'' +
                ", expiry_date='" + expiry_date + '\'' +
                ", status='" + status + '\'' +
                ", campaign_event_list=" + campaign_event_list +
                ", campaign_tandc_list=" + campaign_tandc_list +
                '}';
    }
}
