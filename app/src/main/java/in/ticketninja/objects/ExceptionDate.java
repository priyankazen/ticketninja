package in.ticketninja.objects;

import java.io.Serializable;

public class ExceptionDate implements Serializable {

    private String date="";

    public String getDate() {
        return date;
    }

    @Override
    public String toString() {
        return "ExceptionDate{" +
                "date='" + date + '\'' +
                '}';
    }
}

