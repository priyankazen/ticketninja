/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

/**
 * Created by Rohit on 13-07-2017.
 */

public class UserCouponCode {
    private String generate_type = "";
    private int event_id = 0;
    private int m_id = 0;
    private int coupon_id = 0;
    private String coupon_code = "";
    private double amount = 0.0;
    private String type = "";
    private double min_t_amount = 0.0;
    private double max_d_amount = 0.0;
    private String expiry_date = "";
    private String coupon_code_label = "";
    private String coupon_code_detail = "";

    public String getGenerate_type() {
        return generate_type;
    }

    public int getEvent_id() {
        return event_id;
    }

    public int getM_id() {
        return m_id;
    }

    public int getCoupon_id() {
        return coupon_id;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public double getAmount() {
        return amount;
    }

    public String getType() {
        return type;
    }

    public double getMin_t_amount() {
        return min_t_amount;
    }

    public double getMax_d_amount() {
        return max_d_amount;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public String getCoupon_code_label() {
        return coupon_code_label;
    }

    public String getCoupon_code_detail() {
        return coupon_code_detail;
    }

    @Override
    public String toString() {
        return "UserCouponCode{" +
                "generate_type='" + generate_type + '\'' +
                ", event_id='" + event_id + '\'' +
                ", m_id='" + m_id + '\'' +
                ", coupon_id='" + coupon_id + '\'' +
                ", coupon_code='" + coupon_code + '\'' +
                ", amount='" + amount + '\'' +
                ", type='" + type + '\'' +
                ", min_t_amount='" + min_t_amount + '\'' +
                ", max_d_amount='" + max_d_amount + '\'' +
                ", expiry_date='" + expiry_date + '\'' +
                ", coupon_code_label='" + coupon_code_label + '\'' +
                ", coupon_code_detail='" + coupon_code_detail + '\'' +
                '}';
    }
}
