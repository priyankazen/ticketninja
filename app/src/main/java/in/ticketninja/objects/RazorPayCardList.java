package in.ticketninja.objects;

public class RazorPayCardList {

    public String no = "";
    public String currentYear = "";
    public String monthNo = "";
    public  String monthName ="";


    @Override
    public String toString() {
        return "RazorPayCardList{" +
                "no=" + no +
                ", monthName='" + monthName + '\'' +
                '}';
    }
}
