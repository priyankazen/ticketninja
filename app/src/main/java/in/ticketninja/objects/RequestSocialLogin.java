package in.ticketninja.objects;

import in.ticketninja.common.Constant;

/**
 * Created by Shreya on 3/15/18.
 */

public class RequestSocialLogin {
    public String google_id ;
    public String facebook_id ;
    public String email ;
    public String mobileno ;
    public String firstname ;
    public String lastname ;
    public String birthdate ;
    public String device_type = Constant.DEVICE_TYPE;
    public String device_token;

    public RequestSocialLogin(String google_id, String facebook_id, String email, String mobileno, String firstname, String birthdate, String device_token) {
        this.google_id = google_id;
        this.facebook_id = facebook_id;
        this.email = email;
        this.mobileno = mobileno;
        this.firstname = firstname;
        this.birthdate = birthdate;
        this.device_token = device_token;
    }
}
