/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import java.io.Serializable;

import in.ticketninja.common.Validate;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 22-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class EventPromotion implements Serializable {

    private static final long serialVersionUID = 6663408501416574226L;

    private int code_id;
    private String promo_code = "";
    private String promo_code_desp = "";
    private String promo_code_label = "";
    private double amount = 0.0;
    private double min_t_amount = 0.0;
    private double max_d_amount = 0.0;

    public int getCode_id() {
        return code_id;
    }

    public String getPromo_code() {
        return promo_code;
    }

    public String getPromo_code_desp() {
        return promo_code_desp;
    }

    public String getPromo_code_label() {
        return promo_code_label;
    }

    public double getAmount() {
        return amount;
    }

    public double getMin_t_amount() {
        return min_t_amount;
    }

    public double getMax_d_amount() {
        return max_d_amount;
    }

    @Override
    public String toString() {
        return "EventPromotion{" +
                "code_id=" + code_id +
                ", promo_code='" + promo_code + '\'' +
                ", promo_code_desp='" + promo_code_desp + '\'' +
                ", promo_code_label='" + promo_code_label + '\'' +
                ", amount=" + amount +
                ", min_t_amount=" + min_t_amount +
                ", max_d_amount=" + max_d_amount +
                '}';
    }
}
