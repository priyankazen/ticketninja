package in.ticketninja.objects;

import java.io.Serializable;

public class PromoSliderList implements Serializable {

    public int Promo_Id = 0;
    public String Promo_Image_Id = "";
    public String Promo_Image_URL = "";
    public String Promo_URL = "";

    public int getPromo_Id() {
        return Promo_Id;
    }

    public String getPromo_Image_Id() {
        return Promo_Image_Id;
    }

    public String getPromo_URL() {
        return Promo_URL;
    }

    public String getPromo_Image_URL() {
        return Promo_Image_URL;
    }

    @Override
    public String toString() {
        return "PromoSliderList{" +
                "Promo_Id=" + Promo_Id +
                ", Promo_Image_Id='" + Promo_Image_Id + '\'' +
                ", Promo_Image_URL='" + Promo_Image_URL + '\'' +
                ", Promo_URL='" + Promo_URL + '\'' +
                '}';
    }
}
