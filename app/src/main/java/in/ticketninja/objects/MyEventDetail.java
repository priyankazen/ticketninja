/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import in.ticketninja.common.Constant;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.ws.RestApi;

/**
 * TicketNinja_Working(in.ticketninja.objects) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 13/6/17.
 *
 * @author Suthar Rohit
 */
public class MyEventDetail implements Serializable {

    private static final long serialVersionUID = 6663408501416574222L;

    private long event_id = 0;
    private int rsvp = 0;
    private double discount_per = 0;
    private int is_artistfirst = 0;
    private double rate_from = 0.0;//required for get_event_detail
    private double rate_to = 0.0;//required for get_event_detail
    private int invite_only = 0;
    private int invite_rsvp = 0;
    private int invite_stock = 0;
    private int multi_catergory_select = 0;


    private String type = "";
    @Constant.EventDayTypeAnt
    private String day_type = Constant.EventDayType.OTHER;
    private String detail_image_url = "";
    private String event_name = "";
    private String category = "";
    private String language = "";
    private String duration = "";
    private String event_date = "";
    private String event_date_to = "";
    private String event_time = "";
    private String location_name = "";
    private String location_address = "";
    private String location_city = "";
    private String location_state = "";
    private String location_zip = "";
    private String latitude = "";
    private String longitude = "";
    private String org_id = "";
    private String org_name = "";
    private String org_logo_url = "";
    private String org_site_url = "";
    private String openinghours = "";
    private String share_link_data = "";
    private String discount_label = "";
    private String v_link = "";
    private String s_detail = "";
    private String f_detail = "";
    private String plan_image_url = "";//required for get_event_detail
    private boolean singledaypass = false;//for EventPricingMultidayActivity
    public boolean seasonpass = false;
    private String e_type_icon = "";

    private List<EventArtist> artist_info = new ArrayList<>();
    private List<EventPromotion> event_promotion_list = new ArrayList<>();
    private List<EventPriceCategory> category_list = new ArrayList<>();
    private List<MultipleDay> multipleday_date = new ArrayList<>();
    private List<EventTerms> event_termsandcondition = new ArrayList<>();
    private ArrayList<EventGalleryList> event_gallery_list = new ArrayList<>();

    //adding by priyanka
    //private int advance_days = 0;
    private int advance_days = 0;
    private int stop_sale_hours = 0;
    private int is_sp_inquiry = 0;
    private int is_co_booking = 0;
    public int is_exclusive = 0;
    public String invite_status = "";//available,left,used
    private boolean Invite_Available = false;
    private boolean Invite_Left = false;
    private boolean Invite_Used = false;
    private int per_trans_limit = 0;
    public int is_registration = 0;
    private int stock = 0;

    private String layout_type = "";

    //@SerializedName("exception_dates")
   // private ArrayList<String> exceptionDates = new ArrayList<>();

    private ArrayList<ExceptionDate> exception_dates = new ArrayList<>();


    //unused
    private String Org_URL = "";
    //private String Day_Name = "";
    //private String Y_Date = "";
    // private String M_D_Date = "";
    private String Detail_Image_Id = "";
    private String Plan_Image_Id = "";
    private String Org_logo_Id = "";

    public MyEventDetail() {
    }

    public MyEventDetail(MyEvent myEvent) {
        this.category = myEvent.category;
        this.event_id = myEvent.event_id;
        this.language = myEvent.language;
        this.longitude = myEvent.longitude;
        this.latitude = myEvent.latitude;
        this.event_name = myEvent.event_name;
        this.Detail_Image_Id = myEvent.getMainImageId();
        this.detail_image_url = myEvent.getMainImageUrl();
        this.event_time = myEvent.event_time;
        this.event_date = myEvent.event_date;
        this.location_name = myEvent.location_name;
        this.location_city = myEvent.city;
        this.type = myEvent.type;
        this.invite_only = myEvent.invite_only;
        this.invite_rsvp = myEvent.invite_rsvp;
        this.invite_stock = myEvent.invite_stock;
        this.rsvp = myEvent.rsvp;
        // this.Invite_Available = myEvent.Invite_Available;
        // this.Invite_Left = myEvent.Invite_Left;
        // this.Invite_Used = myEvent.Invite_Used;
        this.invite_status = myEvent.invite_status;
    }


    public int getIs_exclusive() {
        return is_exclusive;
    }

    public long getId() {
        return event_id;
    }

    public String getEvent_name() {
        return Validate.isNotNull(event_name) ? event_name.trim() : "";
    }

    public String getImageId() {
        return Detail_Image_Id;
    }

    public String getDetailImageURL() {
        return detail_image_url;
    }


    public String getShortDetail() {
        return Validate.isNotNull(s_detail) ? s_detail.trim() : "";
    }

    public String getFullDetail() {
        return Validate.isNotNull(f_detail) ? f_detail.trim() : "";
    }

    public String getEvent_date() {
        return event_date;
    }

    public String getDateTo() {
        return event_date_to;
    }

   /* public String getDay() {
        return Day_Name;
    }

    public String getYear() {
        return Y_Date;
    }*/

  /*  public String getMDDate() {
        return M_D_Date;
    }*/

    public String getMDDate() {
        return Utility.getDate(getEvent_date(), getDateTo());
    }

    public String getMDDate1(String type) {
        return Utility.getDate1(getEvent_date(), getDateTo(),type);
    }

    public String getDay() {
        return Utility.getDayName(getEvent_date());
    }

    public String getYear() {
        return Utility.getYear(getEvent_date());
    }

    public String getEvent_time() {
        return event_time;
    }

    public String getDuration() {
        return duration;
    }

    public String getOpeningHours() {
        return openinghours;
    }

    public String getType() {
        return type;
    }

    public String getTypeIcon() {
        return e_type_icon;
    }

    public String getCategory() {
        return category;
    }

    public String getLanguage() {
        return language;
    }

    public double getRateFrom() {
        return rate_from;
    }

    public double getRateTo() {
        return rate_to;
    }

    public String getVideoLink() {
        return v_link;
    }

    public List<EventPromotion> getEvent_promotion_list() {
        return event_promotion_list;
    }

    public List<EventArtist> getArtists() {
        return artist_info;
    }

    public List<EventPriceCategory> getPriceCategory() {
        return category_list;
    }

    public List<EventTerms> getTermsConditions() {
        return event_termsandcondition;
    }

    public ArrayList<EventGalleryList> getEvent_gallery_list() {
        return event_gallery_list;
    }

    @Constant.EventDayTypeAnt
    public String getDayType() {
        return day_type;
    }

    public boolean isSeasonPassAllowed() {
        return seasonpass;
    }

    public boolean isSingleDayPassAllowed() {
        return singledaypass;
    }

    public int isArtistFirst() {
        return is_artistfirst;
    }

    public List<MultipleDay> getMultipleDayList() {
        return multipleday_date;
    }

    public String getVenueName() {
        return location_name;
    }

    public String getVenueFullAddress() {
        List<String> address = new ArrayList<>();
        if (Validate.isNotNull(location_city)) {
            if (Validate.isNotNull(location_address)) address.add(location_address);
            if (Validate.isNotNull(location_city)) address.add(" " + location_city);
            if (Validate.isNotNull(location_zip)) address.add("-" + location_zip);
            if (Validate.isNotNull(location_state)) address.add(", " + location_state);
        } else {
            if (Validate.isNotNull(location_address)) address.add(location_address);
            if (Validate.isNotNull(location_state)) address.add(location_state);
        }
        return TextUtils.join("", address);
    }

    public String getVenueAddress() {
        return location_address;
    }

    public String getVenueCity() {
        return location_city;
    }

    public String getVenueState() {
        return location_state;
    }

    public String getVenueZip() {
        return location_zip;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getPlanImage() {
        return Plan_Image_Id;
    }

    public String getPlan_image_url() {
        return plan_image_url;
    }

    private String getOrg_URL() {
        return Org_URL;
    }

    public String getOrganizerId() {
        return org_id;
    }

    public String getOrganizerName() {
        return org_name;
    }

    private String getOrganizerURL() {
        return Org_URL;
    }

    private String getOrganizerImage() {
        return Org_logo_Id;
    }

    public String getOrganizerImageUrl() {
        return org_logo_url;
    }

    public String getOrg_site_url() {
        return org_site_url;
    }

    public int isInviteOnly() {
        return invite_only;
    }

    public int isInviteRSVP() {
        return invite_rsvp;
    }

    public int isRSVP() {
        return rsvp;
    }

    public int getInviteStock() {
        return invite_stock;
    }

    public String getDiscountLabel() {
        return discount_label;
    }

    public double getDiscountPer() {
        return discount_per;
    }

    public String getShareLinkData() {
        return share_link_data;
    }

    // COMMON
    private int ErrorCode;
    private String ErrorMessage;
    private String ServiceName;

    public boolean isSuccess() {
        return ErrorCode == RestApi.ErrorCode.SUCCESS;
    }

    public String message() {
        return ErrorMessage;
    }

    public int getAdvance_days() {
        return advance_days;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getStop_sale_hours() {
        return stop_sale_hours;
    }

    public int getMax_Ticket_No() {
        return per_trans_limit;
    }

   /* public ArrayList<String> getExceptionDates() {
        return exceptionDates;
    }*/

    public ArrayList<ExceptionDate> getExceptionDates() {
        return exception_dates;
    }

    public int isIs_sp_inquiry() {
        return is_sp_inquiry;
    }

    public int isIs_co_booking() {
        return is_co_booking;
    }

    public int getReg() {
        return is_registration;
    }

   /* public void setReg(Boolean reg) {
        is_registration = reg;
    }*/

    private boolean isInviteAvailable() {
        return Invite_Available;
    }


    private boolean isInviteLeft() {
        return Invite_Left;
    }

    private boolean isInviteUsed() {
        return Invite_Used;
    }

    public String getInvite_status() {
        return invite_status;
    }

    public void setInvite_status(String invite_status) {
        this.invite_status = invite_status;
    }

    public int getMulti_catergory_select() {
        return multi_catergory_select;
    }

    public String getLayout_type() {
        return layout_type;
    }

    @Override
    public String toString() {
        return "MyEventDetail{" +
               // "category='" + category + '\'' +
                "exception_dates='" + exception_dates + '\'' +
                ", event_date='" + event_date + '\'' +
                ", is_registration='" + is_registration + '\'' +
                ", seasonpass='" + seasonpass + '\'' +
                ", singledaypass='" + singledaypass + '\'' +
                ", multi_catergory_select=" + multi_catergory_select +
                ", category_list=" + category_list +
                '}';
    }
}
