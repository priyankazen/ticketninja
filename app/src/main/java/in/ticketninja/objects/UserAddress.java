/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import in.ticketninja.common.Constant;

/**
 * Created by Rohit on 13-07-2017.
 */

public class UserAddress {
    public long Address_Id = 0;


    public String Add_Type = "";
    public String Aline1 = "";
    public String Aline2 = "";
    public String city = "";
    public String fullname = "";
    public String mobileno = "";
    public String pincode = "";
    public String state = "";
    public String country = "";
    @Constant.RegisterWSMode
    private String mode = "";
    public String user_id = "";

    public UserAddress() {
    }

    public UserAddress(String user_Id, String add_Type) {
        user_id = user_Id;
        Add_Type = add_Type;
    }

    public UserAddress(String user_Id, long address_Id, String add_Type, String fullName, String aline1,
                       String aline2, String city, String state, String country, String pinCode,
                       String mobileNo, @Constant.RegisterWSMode String mode) {
        user_id = user_Id;
        Address_Id = address_Id;
        Add_Type = add_Type;
        fullname = fullName;
        Aline1 = aline1;
        Aline2 = aline2;
        this.city = city;
        this.state = state;
        this.country = country;
        pincode = pinCode;
        mobileno = mobileNo;
        this.mode = mode;
    }

    public String getAddressType() {
        return Add_Type;
    }

    public long getAddressId() {
        return Address_Id;
    }

    public String getAline1() {
        return Aline1;
    }

    public String getAline2() {
        return Aline2;
    }

    public String getCity() {
        return city;
    }

    public String getFullName() {
        return fullname;
    }

    public String getMobileNo() {
        return mobileno;
    }

    public String getMode() {
        return mode;
    }

    public String getPincode() {
        return pincode;
    }

    public String getState() {
        return state;
    }

    public String getUserId() {
        return user_id;
    }

}
