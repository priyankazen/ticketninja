/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import androidx.annotation.DrawableRes;
import androidx.annotation.StringRes;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 08-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class MoreMenu {

    @DrawableRes
    private int iconRes;
    @StringRes
    private int textRes;

    public MoreMenu(@DrawableRes int iconRes, @StringRes int textRes) {
        this.iconRes = iconRes;
        this.textRes = textRes;
    }

    @DrawableRes
    public int getIcon() {
        return iconRes;
    }

    @StringRes
    public int getText() {
        return textRes;
    }

}