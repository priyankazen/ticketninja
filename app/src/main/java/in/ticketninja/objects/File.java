/*
 * Copyright (c) 2017. Aditi PArikh
 * Developed by Aditi Parikh for NicheTech Computer Solutions Pvt. Ltd. use only.
 */

package in.ticketninja.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by aditi on 8/22/17.
 */
public class File implements Parcelable {
    private final long id;
    private final String url;
    private String name;
    public int progress;

    public File(long id, String url, String name) {
        this.id = id;
        this.url = url;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public String getUrl() {
        return url;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return url + " " + progress + " %";
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.id);
        dest.writeString(this.url);
        dest.writeString(this.name);
        dest.writeInt(this.progress);
    }

    private File(Parcel in) {
        this.id = in.readLong();
        this.url = in.readString();
        this.name = in.readString();
        this.progress = in.readInt();
    }

    public static final Parcelable.Creator<File> CREATOR = new Parcelable.Creator<File>() {
        public File createFromParcel(Parcel source) {
            return new File(source);
        }

        public File[] newArray(int size) {
            return new File[size];
        }
    };

    public static File getFile(long id, String imageURL, String name) {
        return new File(id, imageURL, name);
    }
}