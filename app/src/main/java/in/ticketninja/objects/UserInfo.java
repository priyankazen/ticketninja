/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import in.ticketninja.ws.RestApi;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 12-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class UserInfo {
    public int error_code;
    public int notification_unread_count = 0;


    public String user_id = "";
    public String firstname = "";
    public String lastname = "";
    public String birthdate = "";
    public String email = "";
    public String country_code = "";
    public String mobileno = "";
    public String gender = "";
    public String user_imageid_url = "";
    public String user_imageid = "";
    private String ErrorMessage;
    private String ServiceName;
    public boolean is_alreadyregister = false;

    public boolean isSuccess() {
        return error_code == RestApi.ErrorCode.SUCCESS;
    }

    public String message() {
        return ErrorMessage;
    }
}
