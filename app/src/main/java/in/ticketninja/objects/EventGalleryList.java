package in.ticketninja.objects;

import java.io.Serializable;

public class EventGalleryList implements Serializable {

    private int id =0;
    private String image_id="";
    private String image_url ="";

    public int getId() {
        return id;
    }

    public String getImage_id() {
        return image_id;
    }

    public String getImage_url() {
        return image_url;
    }

    @Override
    public String toString() {
        return "EventGalleryList{" +
                "id=" + id +
                ", image_id='" + image_id + '\'' +
                ", image_url='" + image_url + '\'' +
                '}';
    }
}
