/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import java.io.Serializable;

/**
 * Created by Rohit on 04-10-2017.
 */

public class Notifications implements Serializable {

    private static final long serialVersionUID = 6663408501416574229L;

    public long notification_id;
    public int n_type;
    public long reference_id;
    public long event_id;
    public int is_read;
    public int status;
    private long user_id;

    public String type;
    public String message;
    public String notification_date;
    public String poster_image_id;
    public String poster_image_url;

    public void setEvent_id(long event_id) {
        this.event_id = event_id;
    }

    public void setIs_read(int is_read) {
        this.is_read = is_read;
    }

    public void setNotification_date(String notification_date) {
        this.notification_date = notification_date;
    }

    public void setNotification_id(long notification_id) {
        this.notification_id = notification_id;
    }

    public void setReference_id(long reference_id) {
        this.reference_id = reference_id;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public void setPoster_image_url(String poster_image_url) {
        this.poster_image_url = poster_image_url;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setN_type(int n_type) {
        this.n_type = n_type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setPoster_image_id(String poster_image_id) {
        this.poster_image_id = poster_image_id;
    }

    public String getEventType() {
        return type;
    }

    public long getEventId() {
        return event_id;
    }

    public int isRead() {
        return is_read;
    }

    public String getMessage() {
        return message;
    }

    public int getType() {
        return n_type;
    }

    public String getNotificationDate() {
        return notification_date;
    }

    public long getNotificationId() {
        return notification_id;
    }

    public String getPosterImageId() {
        return poster_image_id;
    }

    public String getPosterImageURL() {
        return poster_image_url;
    }

    public long getReferenceId() {
        return reference_id;
    }

    public int isStatus() {
        return status;
    }

    public long getUserId() {
        return user_id;
    }

}
