/*
 * Copyright (c) 2018. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import in.ticketninja.ws.RestApi;

public class ApplyOfferCode {
    private int ErrorCode;
    private String ErrorMessage;
    private String ServiceName;
    private int coupon_id = 0;
    private String coupon_code = "";
    private String coupon_type = "";
    private int coupon_per = 0;
    private float discount = 0;
    private float amount = 0;
    private float netamount = 0;
    private double total_processingfee = 0.0;
    private double coupon_amount = 0.0;

    public boolean isSuccess() {
        return ErrorCode == RestApi.ErrorCode.SUCCESS;
    }

    public int getErrorCode() {
        return ErrorCode;
    }

    public String getErrorMessage() {
        return ErrorMessage;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public float getAmount() {
        return amount;
    }

    public String getCoupon_code() {
        return coupon_code;
    }

    public int getCoupon_id() {
        return coupon_id;
    }

    public int getCoupon_per() {
        return coupon_per;
    }

    public String getCoupon_type() {
        return coupon_type;
    }

    public float getDiscount() {
        return discount;
    }

    public float getNetamount() {
        return netamount;
    }
}
