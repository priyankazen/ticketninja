/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import in.ticketninja.common.Validate;

/**
 * Created by Rohit on 16-11-2017.
 */

public class MyEventTimeList implements Serializable {

    private static final long serialVersionUID = 6663408501416574221L;

    //pending123 in multipleday_date in get_event_detail
    private String date = "";
    private String time = "";
    private String s_detail = "";
    private String f_detail = "";
    private String Name = "";

    private List<EventArtist> artist_info = new ArrayList<>();

   /* private long show_id = 0;
    private long showId() {
        return show_id;
    }*/

    public void setName(String name) {
        Name = name;
    }

    public String title() {
        return Validate.isNotNull(Name) ? Name.trim() : "";
    }

    public void setS_detail(String s_detail) {
        this.s_detail = s_detail;
    }

    public String getS_detail() {
        return Validate.isNotNull(s_detail) ? s_detail.trim() : "";
    }


    public void setF_detail(String f_detail) {
        this.f_detail = f_detail;
    }


    public String getF_detail() {
        return Validate.isNotNull(f_detail) ? f_detail.trim() : "";
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTime() {
        return time;
    }

    public void setArtist_info(List<EventArtist> artist_info) {
        this.artist_info = artist_info;
    }

    public List<EventArtist> getArtists() {
        return artist_info;
    }

}
