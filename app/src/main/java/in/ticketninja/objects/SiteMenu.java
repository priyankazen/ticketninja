/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 14-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class SiteMenu {
    private String image_name;
    private int menu_id;
    private String dis_name;
    public String name;

    public String image() {
        return image_name;
    }

    public int id() {
        return menu_id;
    }

    public String name() {
        return dis_name;
    }

    public String key() {
        return name;
    }

    @Override
    public String toString() {
        return "SiteMenu{" +
                "image_name='" + image_name + '\'' +
                ", menu_id=" + menu_id +
                ", dis_name='" + dis_name + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
