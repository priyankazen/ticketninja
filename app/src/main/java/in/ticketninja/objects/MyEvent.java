/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

import in.ticketninja.common.Constant;

/**
 * TicketNinja_Working(in.ticketninja.objects) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 13/6/17.
 *
 * @author Suthar Rohit
 */
public class MyEvent implements Serializable {

    private static final long serialVersionUID = 6663408501416574221L;

    public long event_id = 0;
    private int is_exclusive = 0;
    public int rsvp = 0;
    public int invite_stock = 0;
    public int invite_only = 0;
    public int invite_rsvp = 0;

    public String event_name = "";
    public String type = "";
    @Constant.EventDayTypeAnt
    public String day_type = Constant.EventDayType.OTHER;
    public String location_name = "";
    public String city = "";
    public String event_date = "";
    public String event_date_to = "";
    public String event_time = "";
    public String category = "";
    public String language = "";
    public String main_image_url = "";
    public String poster_image_url = "";
    public String latitude = "";
    public String longitude = "";
    public String state = "";
    public String invite_status = "";//available,left,used
    public String time_type = "";

    public String collection = "";


    //PromotionCampaignDetail
    private int general_sale = 0;
    private double discount = 0;

    //unused
    public String Main_Image_Id = "";
    public String Poster_Image_Id = "";
    private boolean Invite_Available = false;
    private boolean Invite_Left = false;
    private boolean Invite_Used = false;
    private String date_Formated = "";


    public boolean isSelected = false;

    public String getInvite_status() {
        return invite_status;
    }

    public void setInvite_status(String invite_status) {
        this.invite_status = invite_status;
    }

    public String getLocation_name() {
        return location_name;
    }

    public String getCategory() {
        return category;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public long getEventId() {
        return event_id;
    }

    public String getLanguage() {
        return language;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getEvent_name() {
        return event_name;
    }

    public String getMainImageId() {
        return Main_Image_Id;
    }

    public String getMainImageUrl() {
        return main_image_url;
    }

    public String getPosterImageId() {
        return Poster_Image_Id;
    }

    public String getPosterImageUrl() {
        return poster_image_url;
    }

    public String getEvent_time() {
        return event_time;
    }

    @Constant.EventDayTypeAnt
    public String getDayType() {
        return day_type;
    }

    public String getEvent_date() {
        return event_date;
    }

    public String getDateTo() {
        return event_date_to;
    }

    private String getDateFormated() {
        return date_Formated;
    }

    public String getType() {
        return type;
    }

    public int isInviteOnly() {
        return invite_only;
    }


    private boolean isInviteAvailable() {
        return Invite_Available;
    }

    private boolean isInviteLeft() {
        return Invite_Left;
    }

    private boolean isInviteUsed() {
        return Invite_Used;
    }

    public int isInviteRSVP() {
        return invite_rsvp;
    }

    public int isRsvp() {
        return rsvp;
    }

    public int getInviteStock() {
        return invite_stock;
    }

    public int getIs_exclusive() {
        return is_exclusive;
    }

    public String getCollection() {
        return collection;
    }

    public boolean isSelected() {
        return isSelected;
    }

  @NotNull
    @Override
    public String toString() {
        return "MyEvent{" +
                "isSelected='" + isSelected + '\'' +
                "collection='" + collection + '\'' +
                "category='" + category + '\'' +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                ", day_type='" + day_type + '\'' +
                ", event_id=" + event_id +
                ", Invite_Available=" + Invite_Available +
                ", Invite_Left=" + Invite_Left +
                ", invite_only=" + invite_only +
                ", invite_rsvp=" + invite_rsvp +
                ", invite_stock=" + invite_stock +
                ", Invite_Used=" + Invite_Used +
                ", language='" + language + '\'' +
                ", latitude='" + latitude + '\'' +
                ", location_name='" + location_name + '\'' +
                ", longitude='" + longitude + '\'' +
                ", Main_Image_Id='" + Main_Image_Id + '\'' +
                ", main_image_url='" + main_image_url + '\'' +
                ", event_name='" + event_name + '\'' +
                ", Poster_Image_Id='" + Poster_Image_Id + '\'' +
                ", poster_image_url='" + poster_image_url + '\'' +
                ", rsvp=" + rsvp +
                ", event_time='" + event_time + '\'' +
                ", type='" + type + '\'' +
                ", event_date='" + event_date + '\'' +
                ", date_Formated='" + date_Formated + '\'' +
                ", event_date_to='" + event_date_to + '\'' +
                '}';
    }

   /* @Override
    public String toString() {
        return "MyEvent{" +
                "collection='" + collection + '\'' +
                ", isSelected=" + isSelected +
                '}';
    }*/
}
