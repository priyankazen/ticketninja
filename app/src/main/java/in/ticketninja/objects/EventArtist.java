/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import java.io.Serializable;

import in.ticketninja.common.Validate;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 22-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class EventArtist implements Serializable {

    private static final long serialVersionUID = 6663408501416574226L;

    private long artist_id;
    private int show_id;
    private String name;
    private String detail;
    private String photo_id = "";
    private String photo_url = "";



    public EventArtist() {
        /*artist_id = 1000;
        event_name = "FARHAN AKHTAR";
        detail = "Early Life\\nA powerhouse of talent, creativity and a cinematic genius, Farhan Akhtar is an actor, director, producer, singer and writer. He is the son of noted lyricist Javed Akhtar and screenwriter Honey Irani. At the age of 17, Farhan worked as an assistant director on films like Lamhe (1991) and Himalay Putra (1997). He made his directorial debut with the romantic comedy, Dil Chahta Hai (2001), a trendsetting film that went on to attain cult status and become one of the most iconic movies of Bollywood. The movie also bagged several awards including a National Award for Best Feature Film in Hindi.\\nCareer highlights";
        photo_id = "9bb47080-7e93-442b-aa90-c94e652b9056.jpg";*/
    }

    public long id() {
        return artist_id;
    }

    public String name() {
        return Validate.isNotNull(name) ? name.trim() : "";
    }

    public String detail() {
        return Validate.isNotNull(detail) ? detail.trim() : "";
    }

    public String photoId() {
        return photo_id;
    }

    public String photoURL() {
        return photo_url;
    }

    public int getShow_id() {
        return show_id;
    }
}
