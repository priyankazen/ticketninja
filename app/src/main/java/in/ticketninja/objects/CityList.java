/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 12-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class CityList {

    private long city_id;
    private String city;
    private boolean checked;

    public CityList(long city_id, String city_name, boolean checked) {
        this.city_id = city_id;
        this.city = city_name;
        this.checked = checked;
    }

    public long id() {
        return city_id;
    }

    public String name() {
        return city;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}
