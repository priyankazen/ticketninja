/*
 * Copyright (c) 2018. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

public class OfferCode {
    private String session_id;
    private String user_id;
    private String code;
    private String email;
    private String mobileno;

    public OfferCode(String session_Id, String user_Id, String code, String email, String mobileno) {
        this.session_id = session_Id;
        this.user_id = user_Id;
        this.code = code;
        this.email = email;
        this.mobileno = mobileno;
    }
}
