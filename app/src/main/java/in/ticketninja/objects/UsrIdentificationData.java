package in.ticketninja.objects;

import java.util.ArrayList;

public class UsrIdentificationData {

    private String group_title;
    private int pos;
    private ArrayList<UserIdentificationDetails> user_identification_list = new ArrayList<>();


    public void setGroup_title(String group_title) {
        this.group_title = group_title;
    }

    public void setInfo(ArrayList<UserIdentificationDetails> artist_Info) {
        user_identification_list = artist_Info;
    }

    public String getGroup_title() {
        return group_title;
    }

    public ArrayList<UserIdentificationDetails> getUserInfo() {
        return user_identification_list;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    @Override
    public String toString() {
        return "Get_Event_User_Identification_List{" +
                ", group_title='" + group_title + '\'' +
                ", user_identification_list=" + user_identification_list +
                '}';
    }
}
