/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import java.io.Serializable;

/**
 * Created by Rohit on 16-08-2017.
 */

public class EventTimeSlot implements Serializable {
    private String time_slot;
    private int slot_id;

    public EventTimeSlot(String time_Slot, int slot_Id) {
        time_slot = time_Slot;
        slot_id = slot_Id;
    }

    public String timeSlot() {
        return time_slot;
    }

    public void timeSlot(String time_Slot) {
        time_slot = time_Slot;
    }

    public int id() {
        return slot_id;
    }

    public void id(int slot_Id) {
        slot_id = slot_Id;
    }
}
