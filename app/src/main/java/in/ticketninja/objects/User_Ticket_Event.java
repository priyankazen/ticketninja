/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by shabbir on 23/10/17.
 */

public class User_Ticket_Event implements Serializable {

    private static final long serialVersionUID = 6663408501416574233L;

    public long event_id = 0;
    public int is_live_ticket =0;
    public int is_app_ticket =0;

    public String type = "";
    public String day_type = "";
    public String event_name = "";
    public String language = "";
    public String category = "";
    public String event_date = "";
    public String event_date_to = "";
    public String event_time = "";
    public String location_name = "";
    public String latitude = "";
    public String longitude = "";
    public ArrayList<BookingTicketList> ticket_list = new ArrayList<>();
    public String detail_image_url = "";
    public String USER_ID = "";

    public String Seat_No = "";
    public String Seat_Type = "";
    public String Detail_Image_Id = "";


    public String getEvent_name() {
        return event_name;
    }

    public long getEvent_id() {
        return event_id;
    }


    public int isIs_live_ticket() {
        return is_live_ticket;
    }

    public int isIs_app_ticket() {
        return is_app_ticket;
    }

    public String getImageId() {
        return Detail_Image_Id;
    }

    public String getImageURL() {
        return detail_image_url;
    }

    public String getEvent_time() {
        return event_time;
    }

    public String getDateTo() {
        return event_date_to;
    }

    public String getEvent_date() {
        return event_date;
    }

    public String getVenueName() {
        return location_name;
    }

    public String getDayType() {
        return day_type;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getType() {
        return type;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getSeatNo() {
        return Seat_No;
    }

    public String getSeat_Type() {
        return Seat_Type;
    }

    public String getCategory() {
        return category;
    }

    public String getLanguage() {
        return language;
    }

    public boolean getSeasonpass() {
        return false;
    }

    public ArrayList<BookingTicketList> getTicketList() {
        return ticket_list;
    }

    @Override
    public String toString() {
        return "User_Ticket_Event{" +
                "event_id=" + event_id +
                ", type='" + type + '\'' +
                ", day_type='" + day_type + '\'' +
                ", event_name='" + event_name + '\'' +
                ", language='" + language + '\'' +
                ", category='" + category + '\'' +
                ", event_date='" + event_date + '\'' +
                ", event_date_to='" + event_date_to + '\'' +
                ", event_time='" + event_time + '\'' +
                ", location_name='" + location_name + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", is_live_ticket=" + is_live_ticket +
                ", is_app_ticket=" + is_app_ticket +
                ", detail_image_url='" + detail_image_url + '\'' +
                ", USER_ID='" + USER_ID + '\'' +
                ", Seat_No='" + Seat_No + '\'' +
                ", Seat_Type='" + Seat_Type + '\'' +
                ", Detail_Image_Id='" + Detail_Image_Id + '\'' +
                ", ticket_list=" + ticket_list +
                '}';
    }
}
