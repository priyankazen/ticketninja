package in.ticketninja.objects;

public class UserIdentificationDetails {

    private String text = "";
    private String imagePath = "";
    private String imageType = "";
    private String imageBase64 = "";

    //api param
    private int proof_id = 0;
    private String proof_name = "";
    private String proof_type = "";
    private String proof_value = "";

    private boolean blinkCursor = false;

    public void setProof_id(int proof_id) {
        this.proof_id = proof_id;
    }

    public void setProof_name(String proof_name) {
        this.proof_name = proof_name;
    }

    public int getProof_id() {
        return proof_id;
    }

    public String getProof_name() {
        return proof_name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public boolean isBlinkCursor() {
        return blinkCursor;
    }

    public void setBlinkCursor(boolean blinkCursor) {
        this.blinkCursor = blinkCursor;
    }

    public String getProof_type() {
        return proof_type;
    }

    public void setProof_type(String proof_type) {
        this.proof_type = proof_type;
    }

    public String getProof_value() {
        return proof_value;
    }

    public void setProof_value(String proof_value) {
        this.proof_value = proof_value;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }

    public String getImageBase64() {
        return imageBase64;
    }

    public void setImageBase64(String imageBase64) {
        this.imageBase64 = imageBase64;
    }

    @Override
    public String toString() {
        return "UserIdentificationDetails{" +
                "proof_id=" + proof_id +
                ", proof_name='" + proof_name + '\'' +
                ", text='" + text + '\'' +
                ", imagePath='" + imagePath + '\'' +
                ", blinkCursor=" + blinkCursor +
                '}';
    }
}
