package in.ticketninja.objects;

public class RazorPayBankList {

    public String bankName ="";
    public  String bankSortName ="";
    public  String bankIcon ="";

    @Override
    public String toString() {
        return "RazorPayBankList{" +
                "bankName='" + bankName + '\'' +
                ", bankSortName='" + bankSortName + '\'' +
                ", bankIcon='" + bankIcon + '\'' +
                '}';
    }
}
