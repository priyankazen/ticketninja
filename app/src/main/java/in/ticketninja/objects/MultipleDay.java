/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Rohit on 11-09-2017.
 */

public class MultipleDay implements Serializable {

    private static final long serialVersionUID = 6663408501416574227L;

    private long show_id = 0;
    private int seasonpass = 0;
    private String event_date = "";
    private String time = "";
    private String Event_Date_format = "";
    private String show_name = "";
    private String s_detail = "";
    private String f_detail = "";

    //pending123 in  in multipleday_date in get_event_detail
    private ArrayList<MyEventTimeList> Time_List = new ArrayList<>();


    private String banner_url;
    private String v_link;


    public String getS_detail() {
        return s_detail;
    }


    public String getF_detail() {
        return f_detail;
    }

    public String getEvent_date() {
        return event_date;
    }

    public String getTime() {
        return time;
    }


    public int isSeasonPass() {
        return seasonpass;
    }

    public String getShow_name() {
        return show_name;
    }

    private ArrayList<MyEventTimeList> timeList() {
        return Time_List;
    }

    public long id() {
        return show_id;
    }


    private String eventDateFormatted() {
        return Event_Date_format;
    }

    @Override
    public String toString() {
        return "MultipleDay{" +
                "show_id=" + show_id +
                ", event_date='" + event_date + '\'' +
                ", time='" + time + '\'' +
                ", Event_Date_format='" + Event_Date_format + '\'' +
                ", s_detail='" + s_detail + '\'' +
                ", f_detail='" + f_detail + '\'' +
                ", show_name='" + show_name + '\'' +
                ", Time_List=" + Time_List +
                ", seasonpass=" + seasonpass +
                '}';
    }
}
