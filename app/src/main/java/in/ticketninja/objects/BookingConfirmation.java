/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import java.util.List;

import in.ticketninja.ws.RestApi;

/**
 * Created by Rohit on 14-07-2017.
 */

public class BookingConfirmation {
    private long Event_Id;
    private String Name;
    private String Detail_Image_Id;
    private String Detail_Image_URL;
    private String Date;
    private String Day_Name;
    private String Y_Date;
    private String E_Type;
    private String E_Type_Icon;
    private String M_D_Date;
    private String Time;
    private String Location_Name;
    private double Amount;
    private String Email;
    private String MobileNo;
    private String Latitude;
    private String Longitude;
    private double NetAmount;
    private String P_D_Address;
    private String Payment_Id;
    private int Total_Ticket;
    private String D_Time_Slot;
    private String Trans_Date;
    private long Trans_Id;
    private String Trans_No;
    private String QR_Code;
    private String Trans_Type;
    private String Payment_Status;
    private String QR_Code_URL;

    private List<EventPriceCategory> Category_List;

    public String getQR_Code_URL() {
        return QR_Code_URL;
    }

    public long getId() {
        return Event_Id;
    }

    public String getName() {
        return Name;
    }

    public String getImageId() {
        return Detail_Image_Id;
    }

    public String getImageUrl() {
        return Detail_Image_URL;
    }

    public String getDate() {
        return Date;
    }

    public String getType() {
        return E_Type;
    }

    public String getTypeIcon() {
        return E_Type_Icon;
    }

    public String getDay() {
        return Day_Name;
    }

    public String getYear() {
        return Y_Date;
    }

    public String getMDDate() {
        return M_D_Date;
    }

    public String getTime() {
        return Time;
    }

    public String getVenueName() {
        return Location_Name;
    }

    public List<EventPriceCategory> getPriceCategory() {
        return Category_List;
    }

    public double getAmount() {
        return Amount;
    }

    public String getEmail() {
        return Email;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public String getLatitude() {
        return Latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public double getNetAmount() {
        return NetAmount;
    }

    public String getP_D_Address() {
        return P_D_Address;
    }

    public String getPayment_Id() {
        return Payment_Id;
    }

    public int getTotalTicket() {
        return Total_Ticket;
    }

    public String getTimeSlot() {
        return D_Time_Slot;
    }

    public long getTrans_Id() {
        return Trans_Id;
    }

    public String getBookingId() {
        return Trans_No;
    }

    public String getQRCode() {
        return QR_Code;
    }

    public String getPaymentType() {
        return Trans_Type;
    }

    public String getPaymentStatus() {
        return Payment_Status;
    }

    public String getTransDate() {
        return Trans_Date;
    }

    private int ErrorCode;
    private String ErrorMessage;
    private String ServiceName;

    public boolean isSuccess() {
        return ErrorCode == RestApi.ErrorCode.SUCCESS;
    }

    public String message() {
        return ErrorMessage;
    }


}
