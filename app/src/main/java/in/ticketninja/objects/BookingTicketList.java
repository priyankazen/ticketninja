/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.objects;

import java.io.Serializable;

import in.ticketninja.common.Constant;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;

/**
 * Created by Rohit on 18-08-2017.
 */

public class BookingTicketList implements Serializable {

    private static final long serialVersionUID = 6663408501416574231L;

    public long event_id = 0;
    public long trans_id = 0;
    public int noofticket = 0;
    public int seasonpass = 0;
    public long user_id;
    public int is_live_ticket = 0;
    public int is_app_ticket = 0;
    public double rate = 0;

    public String trans_no = "";
    public String booking_id = "";
    public String qr_code = "";
    public String event_name = "";
    public String event_date = "";
    public String event_date_to = "";
    public String event_time = "";
    public String location_name = "";
    public String category_name = "";
    //public int mrp = 0;
    public String type = "";
    public String day_type = "";
    public String seat_type = "";
    public String seat_no = "";
    public String detail_image_url = "";
    public String latitude = "";
    public String longitude = "";
    public String status = ""; // status { Open, Shared, Used, Recived }
    public String share_to = "";
    public String share_from = "";
    public String ticket_desc = "";
    public String qr_code_url = "";
    public String language = "";
    public String D_M_Date = "";
    public String Day_Name = "";
    public String Y_Date = "";
    public String detail_image_id = "";


    public int isIs_app_ticket() {
        return is_app_ticket;
    }

    public int isIs_live_ticket() {
        return is_live_ticket;
    }

    public String getQR_Code_URL() {
        return qr_code_url;
    }

    public String bookingId() {
        return booking_id;
    }

    public String categoryName() {
        return category_name;
    }

    public String language() {
        return language;
    }

   /* public String getD_M_Date() {
        return D_M_Date;
    }

    public String getDayName() {
        return Day_Name;
    }

    public String getYear() {
        return Y_Date;
    }*/


    public String getD_M_Date() {
        return Utility.getDate(getEvent_date(), getDateTo());
    }

    public String getDayName() {
        return Utility.getDayName(getEvent_date());
    }

    public String getYear() {
        return Utility.getYear(getEvent_date());
    }

    public String getEvent_date() {
        return event_date;
    }

    public String getDateTo() {
        return event_date_to;
    }


    public String getDayType() {
        return day_type;
    }

    public String getDetailImageId() {
        return detail_image_id;
    }

    public String getDetailImageURL() {
        return detail_image_url;
    }

    public String getEventType() {
        return type;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public String getLocationName() {
        return location_name;
    }

    public String getQRCode() {
        return qr_code;
    }

    public int noOfTicket() {
        return noofticket;
    }

    public void noOfTicket(int noOfTicket) {
        noofticket = noOfTicket;
    }

    public double rate() {
        return rate;
    }

    public void rate(double rate) {
        this.rate = rate;
    }

    public String seatNo() {
        return seat_no;
    }

    public void seatNo(String seatNo) {
        this.seat_no = seatNo;
    }

    public String seatType() {
        return seat_type;
    }

    public String ticketDesc() {
        return ticket_desc;
    }

    public String getEvent_time() {
        return event_time;
    }

    public long getTransId() {
        return trans_id;
    }

    public String ticketId() {
        return trans_no;
    }


    public String sharedFrom() {
        return share_from;
    }

    public String sharedTo() {
        return share_to;
    }

    public String status() {
        return status;
    }

    public boolean isStatusOpen() {
        return Validate.isNotNull(status) && status.equalsIgnoreCase(Constant.TicketStatus.OPEN);
    }

    public boolean isStatusReceived() {
        return Validate.isNotNull(status) && status.equalsIgnoreCase(Constant.TicketStatus.RECEIVED);
    }

    public boolean isStatusShared() {
        return Validate.isNotNull(status) && status.equalsIgnoreCase(Constant.TicketStatus.SHARED);
    }

    public int isSeasonPass() {
        return seasonpass;
    }

    @Override
    public String toString() {
        return "BookingTicketList{" +
                "event_id=" + event_id +
                ", trans_id=" + trans_id +
                ", trans_no='" + trans_no + '\'' +
                ", booking_id='" + booking_id + '\'' +
                ", qr_code='" + qr_code + '\'' +
                ", event_name='" + event_name + '\'' +
                ", event_date='" + event_date + '\'' +
                ", event_date_to='" + event_date_to + '\'' +
                ", event_time='" + event_time + '\'' +
                ", location_name='" + location_name + '\'' +
                ", category_name='" + category_name + '\'' +
                ", noofticket=" + noofticket +
                ", rate=" + rate +
                ", type='" + type + '\'' +
                ", day_type='" + day_type + '\'' +
                ", seat_type='" + seat_type + '\'' +
                ", seat_no='" + seat_no + '\'' +
                ", detail_image_url='" + detail_image_url + '\'' +
                ", latitude='" + latitude + '\'' +
                ", longitude='" + longitude + '\'' +
                ", seasonpass=" + seasonpass +
                ", user_id=" + user_id +
                ", status='" + status + '\'' +
                ", share_to='" + share_to + '\'' +
                ", share_from='" + share_from + '\'' +
                ", ticket_desc='" + ticket_desc + '\'' +
                ", is_live_ticket=" + is_live_ticket +
                ", is_app_ticket=" + is_app_ticket +
                ", qr_code_url='" + qr_code_url + '\'' +
                ", language='" + language + '\'' +
                ", D_M_Date='" + D_M_Date + '\'' +
                ", Day_Name='" + Day_Name + '\'' +
                ", Y_Date='" + Y_Date + '\'' +
                ", detail_image_id='" + detail_image_id + '\'' +
                '}';
    }

    /*"booking_id": "APS2000105",
         "Category_Logo_css": "zmdi zmdi-male-alt",
         "category_name": "Special Museum (Adult)",
         "D_M_Date": "Aug 15 ",
         "event_date": "2017-08-15",
         "Day_Name": "Tuesday",
         "detail_image_id": "b8ec9bfc-e5b4-4801-a12c-6bd65248d8bb.jpg",
         "detail_image_url": "https://ticketninja.in/Admin/Document/b8ec9bfc-e5b4-4801-a12c-6bd65248d8bb.jpg",
         "type": "Attraction",
         "latitude": "24.5764421",
         "location_name": "city Palace-Udaipur",
         "longitude": "73.6835109",
         "event_name": "The city Palace Museum",
         "noofticket": "1",
         "qr_code": "T-NFO2001165",
         "rate": 700,
         "seat_no": "",
         "seat_type": "Entrance",
         "ticket_desc": "ADMIT ONE PERSON",
         "event_time": "09:00 AM-11:00 AM",
         "trans_id": 2001,
         "trans_no": "NFO2001165",
         "Y_Date": "2017"*/

}
