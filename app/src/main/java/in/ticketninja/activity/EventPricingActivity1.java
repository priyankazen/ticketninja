/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Locale;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.EventPricingAdapter1;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.EventPriceCategory;
import in.ticketninja.objects.MyEventDetail;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestInsertTicketSession;
import in.ticketninja.ws.response.CallbackInsertTicketSession;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventPricingActivity1 extends AppCompatActivity {

    private final String TAG = EventPricingActivity1.class.getSimpleName();

    private CommonClass CC;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;

    private RecyclerView recyclerView;
    private LinearLayout llErrorLayout;
    private View llMainLayout;
    private Button btnContinue;
    private ImageView ivErrorImage;
    private TextView tvErrorMessage, tvErrorButton;

    private MyEventDetail myEvent;

    private TextView tvTitle;
    private TextView tvDateTime, tvTotalPrice;
    private LinearLayout llEventVenue, llContinue;
    private TextView tvVenueName;
    private TextView tvVenueAddress;
    private TextView tvDiscount;
    private RelativeLayout rvDiscount;

    private EventPriceCategory selectedCategory;
    private PreferencesUtils mPref;
    private Context context;
    private EventPricingAdapter1 eventArtistAdapter;
    //private int selectPos = 0;
    private int totalTicketCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_pricing1);
        try {
            Log.e(TAG, "is calledddd..............");

            initializeData();

            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_DATA)) {
                myEvent = (MyEventDetail) getIntent().getSerializableExtra(Constant.ScreenExtras.EVENT_DATA);
            }

            setupActionbar();

            findViewById();

            if (CC.isOnline()) {
                if (myEvent != null) {
                    invalidateData();
                } else {
                    tvErrorMessage.setText(R.string.msg_something_wrong);
                    showError();
                }
            } else {
                showNoInternet();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initializeData() {
        try {
            context = EventPricingActivity1.this;
            mPref = new PreferencesUtils(this);
            CC = new CommonClass(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void findViewById() {
        try {
            recyclerView = findViewById(R.id.recyclerView);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);

            tvTitle = findViewById(R.id.tvEventTitle);
            rvDiscount = findViewById(R.id.rv_discount);
            tvDiscount = findViewById(R.id.tv_discount);

            tvDateTime = findViewById(R.id.tvEventDateTime);
            llContinue = findViewById(R.id.llContinue);
            tvTotalPrice = findViewById(R.id.tvTotalPrice);
            btnContinue = findViewById(R.id.btnContinue);
            llEventVenue = findViewById(R.id.llEventVenue);
            tvVenueName = findViewById(R.id.tvEventVenue);
            tvVenueAddress = findViewById(R.id.tvEventAddress);

            llMainLayout = findViewById(R.id.nestedScrollView);
            llErrorLayout = findViewById(R.id.llErrorLayout);

            ivErrorImage = findViewById(R.id.ivErrorImage);
            tvErrorMessage = findViewById(R.id.tvErrorMessage);
            tvErrorButton = findViewById(R.id.tvErrorButton);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionbar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                // getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onPostResume() {
        try {
            super.onPostResume();
            for (int i = 0; i < myEvent.getPriceCategory().size(); i++) {
                myEvent.getPriceCategory().get(i).noOfTicket(0);
            }
            invalidateData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            Log.e(TAG, "onStop");
            eventArtistAdapter.updateCount(myEvent.getMax_Ticket_No());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == RESULT_OK && requestCode == Constant.REQUEST_CODE_CONFIRM_CONTACT_DETAIL) {
//            if (data != null && data.hasExtra(Constant.ScreenExtras.EVENT_SESSION_ID)) {
//                mPref = new PreferencesUtils(this);
//                if (Validate.isNotNull(mPref.getUserEmailId()) && Validate.isNotNull(mPref.getUserPhoneNumber()) &&
//                        Validate.isNotNull(mPref.getCountryCode())) {
//                    OrderSummaryActivity.start(EventPricingActivity.this, data.getStringExtra(Constant.ScreenExtras.EVENT_SESSION_ID));
//                }
//            }
//        }
//    }


    private void invalidateData() {
        try {
            tvTitle.setText(Validate.isNotNull(myEvent.getEvent_name()) ? String.format("%s", myEvent.getEvent_name()) : "");

            if (myEvent.getDayType().equalsIgnoreCase(Constant.EventDayType.SINGLE_DAY)) {
                tvDateTime.setText(String.format("%s, %s (%s %s)", myEvent.getDay(), myEvent.getMDDate(), myEvent.getEvent_time(), Constant.ONWARDS));

            } else if (myEvent.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY)
                    || myEvent.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW)) {
               // tvDateTime.setText(String.format("%s, %s (%s %s)", myEvent.getMDDate(), myEvent.getYear(), myEvent.getEvent_time(), Constant.ONWARDS));

                //change 13/11/2019
                tvDateTime.setText(String.format("%s (%s %s)", myEvent.getMDDate1(""), myEvent.getEvent_time(), Constant.ONWARDS));

            } else if (myEvent.getDayType().equalsIgnoreCase(Constant.EventDayType.PERPETUAL)) {
                tvDateTime.setText(String.format("%s, %s (%s)", myEvent.getDay(), myEvent.getMDDate(), myEvent.getEvent_time()));

            } else {
                tvDateTime.setText("");
            }

            tvVenueName.setText(Validate.isNotNull(myEvent.getVenueName()) ? String.format("%s", myEvent.getVenueName()) : "");
            tvVenueAddress.setText(Validate.isNotNull(myEvent.getVenueFullAddress()) ? String.format("%s", myEvent.getVenueFullAddress()) : "");

            if (Validate.isNotNull(myEvent.getLatitude()) && Validate.isNotNull(myEvent.getLongitude())) {
                llEventVenue.setOnClickListener(v -> CommonClass.openMap(this, myEvent.getVenueName(), myEvent.getLatitude(), myEvent.getLongitude()));
            }

            try {
                if (Validate.isNotNull(myEvent.getDiscountLabel())) {
                    rvDiscount.setVisibility(View.VISIBLE);
                    tvDiscount.setText(String.format(Locale.getDefault(), "%s %.2f%% off", myEvent.getDiscountLabel(), myEvent.getDiscountPer()));
                } else {
                    rvDiscount.setVisibility(View.GONE);
                }
            }catch (Exception e){
                e.printStackTrace();
            }


            if (myEvent.getPriceCategory() != null && myEvent.getPriceCategory().size() > 0) {

                eventArtistAdapter = new EventPricingAdapter1(this, myEvent.getPriceCategory(), myEvent.getMulti_catergory_select(), myEvent.getMax_Ticket_No());

                eventArtistAdapter.setOnItemClickListener((position, count, priceCategory) -> {
                    try {
                        Log.e(TAG, "count: " + count);
                        totalTicketCount = count;
                        // selectPos = position;
                        if (position >= 0 && count > 0) {
                            selectedCategory = myEvent.getPriceCategory().get(position);
                            //selectedCategory.noOfTicket(count);
                            btnContinue.setEnabled(true);
                            btnContinue.setGravity(Gravity.END | Gravity.CENTER);
                            llContinue.setEnabled(true);
                            tvTotalPrice.setVisibility(View.VISIBLE);
                            //tvTotalPrice.setText(getString(R.string.total).concat("    ₹ ").concat("" + selectedCategory.getTotalRate()));

                            @SuppressLint("DefaultLocale") String s1 = "₹ ".concat(String.format("%.2f", selectedCategory.getTotalRate()));
                            SpannableString ss1 = new SpannableString(s1);
                            ss1.setSpan(new RelativeSizeSpan(1f), 0, s1.length(), 0);

                            String s2 = count + " Tickets";// / set size
                            SpannableString ss2 = new SpannableString(s2);
                            ss2.setSpan(new RelativeSizeSpan(0.7f), 0, s2.length(), 0);

                            CharSequence finalText = TextUtils.concat(ss1, "\n", ss2);// / set size
                            tvTotalPrice.setText(finalText);

                        } else {
                            selectedCategory = null;
                            tvTotalPrice.setText("");
                            tvTotalPrice.setVisibility(View.GONE);
                            btnContinue.setGravity(Gravity.CENTER);
                            btnContinue.setEnabled(false);
                            llContinue.setEnabled(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                });
                recyclerView.setAdapter(eventArtistAdapter);
                tvTotalPrice.setText("");
                tvTotalPrice.setVisibility(View.GONE);
                btnContinue.setGravity(Gravity.CENTER);
                btnContinue.setEnabled(false);
                llContinue.setEnabled(false);

                recyclerView.setVisibility(View.VISIBLE);
            } else {
                recyclerView.setVisibility(View.GONE);
            }

            btnContinue.setOnClickListener(v -> {
                try {
                    Utility.hideKeyboard(this);
                    if (CC.isOnline() && selectedCategory != null) {
                        if (Constant.Code.ONE == selectedCategory.isIdentification()) {
                            Intent intent = new Intent(getApplicationContext(), ExpandableActivity.class);
                            intent.putExtra(Constant.ScreenExtras.EVENT_ID, myEvent.getId());
                            intent.putExtra(Constant.ScreenExtras.PLAN_CATEGORY_ID, selectedCategory.getCategory_id());
                            //intent.putExtra(Constant.ScreenExtras.SEAT_COUNT, selectedCategory.noOfTicket());
                            intent.putExtra(Constant.ScreenExtras.SEAT_COUNT, totalTicketCount);
                            intent.putExtra(Constant.ScreenExtras.EVENT_DATE, selectedCategory.eventDate());
                            intent.putExtra(Constant.ScreenExtras.EVENT_TIME, selectedCategory.time());
                            intent.putExtra(Constant.ScreenExtras.EVENT_DATA, selectedCategory);
                            intent.putExtra(Constant.ScreenExtras.EVENT_DATA1, eventArtistAdapter.getSelectedList());
                            intent.putExtra(Constant.ScreenExtras.TYPE, selectedCategory.seatType());
                            intent.putExtra(Constant.ScreenExtras.EVENT_NAME, myEvent.getEvent_name());
                            intent.putExtra(Constant.ScreenExtras.EVENT_CITY, myEvent.getVenueCity());
                            intent.putExtra(Constant.ScreenExtras.EVENT_ADDRESS, myEvent.getVenueName());
                            startActivity(intent);
                        } else if (Validate.isNotNull(selectedCategory.seatType()) && selectedCategory.seatType().equalsIgnoreCase(Constant.SEAT_TYPE_ARRANGE_SEATING)) {
                            Intent intent = new Intent(getApplicationContext(), SeatSelectionActivity.class);
                            intent.putExtra(Constant.ScreenExtras.EVENT_ID, myEvent.getId());
                            intent.putExtra(Constant.ScreenExtras.PLAN_CATEGORY_ID, selectedCategory.id());
                            intent.putExtra(Constant.ScreenExtras.SHOW_ID, selectedCategory.showId());
                            //intent.putExtra(Constant.ScreenExtras.SEAT_COUNT, selectedCategory.noOfTicket());
                            intent.putExtra(Constant.ScreenExtras.SEAT_COUNT, totalTicketCount);
                            intent.putExtra(Constant.ScreenExtras.IDENTIFICATION, selectedCategory.isIdentification());
                            intent.putExtra(Constant.ScreenExtras.EVENT_DATE, selectedCategory.eventDate());
                            intent.putExtra(Constant.ScreenExtras.EVENT_TIME, selectedCategory.time());
                            intent.putExtra(Constant.ScreenExtras.EVENT_NAME, myEvent.getEvent_name());
                            intent.putExtra(Constant.ScreenExtras.EVENT_CITY, myEvent.getVenueCity());
                            intent.putExtra(Constant.ScreenExtras.EVENT_ADDRESS, myEvent.getVenueName());
                            startActivity(intent);
                            //finish();
                        } else {
                            //wsInsertTicketSession(myEvent.getId(), myEvent.getEvent_date(), myEvent.getEvent_time(), mLogin.getUserId(), selectedCategory);
                            wsInsertTicketSession(myEvent.getId(), myEvent.getEvent_date(), myEvent.getEvent_time(), mLogin.getUserId(), eventArtistAdapter.getSelectedList());
                        }
                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void showNoInternet() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_connection_lost)
                    .placeholder(R.drawable.ic_connection_lost_static)
                    .into(ivErrorImage);
            tvErrorMessage.setText(R.string.msg_no_internet);
            llMainLayout.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.VISIBLE);
            tvErrorButton.setText(R.string.btn_try_again);
            tvErrorButton.setOnClickListener(v -> {

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showError() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_no_result_found)
                    .placeholder(R.drawable.ic_no_result_found)
                    .into(ivErrorImage);
            llMainLayout.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        try {
            Utility.hideKeyboard(this);
            super.onBackPressed();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // WS - Insert Ticket Session
    //private void wsInsertTicketSession(long eventId, String eventDate, String timeSlot, String userId, EventPriceCategory categoryList) {
    private void wsInsertTicketSession(long eventId, String eventDate, String timeSlot, String userId, List<EventPriceCategory> categoryList) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestInsertTicketSession request = new RequestInsertTicketSession(eventId, eventDate, timeSlot, String.valueOf(userId), categoryList, "");

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackInsertTicketSession> call = tnAPI.insertTicketSession(request);
            call.enqueue(new Callback<CallbackInsertTicketSession>() {
                @Override
                public void onResponse(@NonNull Call<CallbackInsertTicketSession> call, @NonNull Response<CallbackInsertTicketSession> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                String sessionID = response.body().data().sessionID();

                                if (Validate.isNull(mPref.getUserEmailId()) || Validate.isNull(mPref.getUserPhoneNumber()) ||
                                        Validate.isNull(mPref.getCountryCode())) {

                                    // TODO: 29/3/18 SHREYA 1 : Contact Detail Flow
//                                startActivity(new Intent(EventPricingActivity.this, ContactConfirmationActivity.class));
                                    ContactConfirmationActivity.Companion.start(context, sessionID, Constant.ContactActions.EDIT, "", "");

                                } else {
                                    OrderSummaryActivity.start(context, sessionID, "", "");
                                }
                            } else if (Validate.isNotNull(response.body().data().message())) {
                                CC.showAlert(response.body().data().message());
                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        } else {
                            CC.showToast(R.string.msg_no_response);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CC.showToast(R.string.msg_something_wrong);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackInsertTicketSession> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }


    }

}
