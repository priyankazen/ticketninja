/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.SpinnerTimeSlotCalendar;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.EventPriceCategory;
import in.ticketninja.objects.EventTimeSlot;
import in.ticketninja.objects.ExceptionDate;
import in.ticketninja.objects.MyEventDetail;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestAttraction;
import in.ticketninja.ws.response.CallbackGetAttraction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings("ALL")
public class SelectDateAndTimeActivity extends AppCompatActivity {

    private static final String TAG = "SelectDate&TimeActivity";
    private CommonClass CC;
    private SRKLoaderDialog mLoader;

    private static final String dateTemplate = "MMMM yyyy";
    private static final String MONTH = "MMMM";
    private static final String YEAR = "yyyy";

    private long eventId;
    private String eventName = "";
    private int multiCategorySelect = 0;

    private Button selectedDayMonthYearButton;
    private TextView currentMonth;
    private ImageView prevMonth;
    private ImageView nextMonth;
    private GridView calendarView;
    private GridCellAdapter adapter;
    private Calendar _calendar;
    private int month, year;

    private int prev_month = 0, prev_year = 0, prev_date = 0;
    private TextView tv_country;
    private ImageView iv_country;
    private String countryName, countryCode;
    private int flagImageId;
    private Spinner sp_time_slot;
    private ArrayList<EventTimeSlot> timeSlotList = new ArrayList<>();
    private ArrayList<EventPriceCategory> categoryList = new ArrayList<>();

    private String extraDiscountLabel = "";
    private double extraDiscountPer = 0;
    private String selectedDateWS;
    private String selectedTimeSlot;
    private Calendar minDate, maxDate;

    //p
    private int advance_days = 0, per_trans_limit = 0, hours = 0;
    //ArrayList<String> exceptionDate = new ArrayList<>();
    ArrayList<ExceptionDate> exceptionDate = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_date_and_time);

        try {
            CC = new CommonClass(this);
            LoginUtils mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);

            getIntentData();

            initializeCalendar();

            int MAXIMUM_DAYS = advance_days;

            minDate = Calendar.getInstance();
            minDate.add(Calendar.HOUR_OF_DAY, minDate.get(Calendar.HOUR) + hours);
            minDate.set(Calendar.MINUTE, 0);
            minDate.set(Calendar.SECOND, 0);
            //Log.e(TAG, "minDate:= " + minDate.getEvent_time());

            maxDate = Calendar.getInstance();
            maxDate.add(Calendar.DATE, MAXIMUM_DAYS);
            maxDate.set(Calendar.HOUR_OF_DAY, 0);
            maxDate.set(Calendar.MINUTE, 0);
            maxDate.set(Calendar.SECOND, 0);
            maxDate.add(Calendar.SECOND, 6);
            //Log.e(TAG, "maxDate:= " + maxDate.getEvent_time());

            setupActionBar();

            TextView tvEventTitle = findViewById(R.id.tvEventTitle);
            tvEventTitle.setText(Validate.isNotNull(eventName) ? eventName : "");
            tv_country = findViewById(R.id.tv_country);
            iv_country = findViewById(R.id.iv_country);
            sp_time_slot = findViewById(R.id.sp_time_slot);

            Country country;
            try {
                if (Validate.isNotNull(mLogin.getCountryCode())) {
                    String countryCode = mLogin.getCountryCode().replace("+", "");
                    PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
                    //Log.e(TAG, "countryCode:" + countryCode);
                    try {
                        String isoCode = phoneNumberUtil.getRegionCodeForCountryCode(Integer.parseInt(countryCode));
                        country = Country.getCountryByISO(isoCode);
                    } catch (Exception e) {
                        e.printStackTrace();
                        country = Country.getCountryByISO(Constant.DEFAULT_COUNTRY_REGION);
                    }
                } else {
                    country = Country.getCountryByISO(Constant.DEFAULT_COUNTRY_REGION);
                }

                iv_country.setImageResource(country.getFlag());
                tv_country.setText(country.getName());

                countryName = country.getName();
                flagImageId = country.getFlag();
                countryCode = country.getCode();

            } catch (Exception e) {
                e.printStackTrace();
            }

            Button btnContinue = findViewById(R.id.btnContinue);
            btnContinue.setOnClickListener(v -> {
                try {
                    if (timeSlotList.size() > 0) {
                        if (sp_time_slot.getSelectedItemPosition() == 0) {
                            CC.showToast(R.string.event_detail_msg_select_time_slot);
                        } else {
                            Intent i = new Intent(this, SelectTicketActivity.class);
                            i.putExtra(Constant.ScreenExtras.EVENT_ID, eventId);
                            i.putExtra(Constant.ScreenExtras.EVENT_NAME, eventName);
                            i.putExtra(Constant.SelectTicket.SELECTED_DATE, selectedDateWS);
                            i.putExtra(Constant.SelectTicket.COUNTRYNAME, countryName);
                            i.putExtra(Constant.SelectTicket.IMAGE_ID, flagImageId);
                            i.putExtra(Constant.SelectTicket.CATEGORYLIST, categoryList);
                            i.putExtra(Constant.SelectTicket.TIME, selectedTimeSlot);
                            i.putExtra(Constant.SelectTicket.COUNTRY_CODE, countryCode);
                            i.putExtra(Constant.SelectTicket.TIME_LIST, timeSlotList);
                            i.putExtra(Constant.ScreenExtras.EVENT_DISCOUNT_LABEL, extraDiscountLabel);
                            i.putExtra(Constant.ScreenExtras.EVENT_DISCOUNT_PER, extraDiscountPer);
                            i.putExtra(Constant.ScreenExtras.EVENT_TOTAL_TICKET, per_trans_limit);
                            i.putExtra(Constant.ScreenExtras.EVENT_ADVANCE_DAYS, advance_days);
                            i.putExtra(Constant.ScreenExtras.HOURS, hours);
                            //  i.putStringArrayListExtra(Constant.ScreenExtras.EXCEPTION_DATES, exceptionDate);
                            i.putExtra(Constant.ScreenExtras.MULTI_CATEGORY_SELECT, multiCategorySelect);
                            startActivity(i);
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }


            });

            tv_country.setOnClickListener(v -> {
                try {
                    CountryPicker picker = CountryPicker.newInstance("Select country");  // dialog title
                    picker.setListener((name, code, dialCode, flagDrawableResID) -> {

                        tv_country.setText(name);
                        iv_country.setImageResource(flagDrawableResID);

                        countryName = name;
                        flagImageId = flagDrawableResID;
                        countryCode = code;

                        Utility.hideKeyboard(SelectDateAndTimeActivity.this, picker.getView());
                        picker.dismiss();

                        if (CC.isOnline()) {
                            wsGetAttractionTimeSlotCategory(eventId, countryCode, selectedDateWS);
                        } else {
                            CC.showToast(R.string.msg_no_internet);
                        }

                        // Utility.hideKeyboard(SelectDateAndTimeActivity.this);
                        // picker.get.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                        // tv_country.setCompoundDrawables(flagDrawableResID,null,null,null);
                    });
                    picker.onCancel(new DialogInterface() {
                        @Override
                        public void cancel() {
                            Utility.hideKeyboard(SelectDateAndTimeActivity.this, picker.getView());
                        }

                        @Override
                        public void dismiss() {
                            Utility.hideKeyboard(SelectDateAndTimeActivity.this, picker.getView());
                        }
                    });
                    picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                }catch (Exception e){
                    e.printStackTrace();
                }


            });

            sp_time_slot.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    try {

                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    if (timeSlotList.size() <= 1) {
                        CC.showToast(R.string.event_detail_msg_no_time_slot_found);
                    }
                    selectSpinnerItemByValue(sp_time_slot, selectedTimeSlot, 1, position);

                    if (timeSlotList != null && timeSlotList.size() > 1) {
                        selectedTimeSlot = timeSlotList.get(position).timeSlot();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            //,,,,,,,,,, Custom Calender ,,,,,,,,,,,,,,,,,,//
            _calendar = Calendar.getInstance(Locale.getDefault());
            month = _calendar.get(Calendar.MONTH) + 1;
            year = _calendar.get(Calendar.YEAR);
            Log.d(TAG, "Calendar Instance:= " + "Month: " + month + " " + "Year: " + year);

            selectedDayMonthYearButton = this.findViewById(R.id.selectedDayMonthYear);

            prevMonth = this.findViewById(R.id.prevMonth);
            prevMonth.setOnClickListener(this::changeMonth);

            String month1 = (DateFormat.format(MONTH, _calendar.getTime()).toString());
            String year1 = (DateFormat.format(YEAR, _calendar.getTime()).toString());

            currentMonth = this.findViewById(R.id.currentMonth);
            currentMonth.setText(DateFormat.format(dateTemplate, _calendar.getTime()));

            setCurrentMonthText(month1, year1);

            nextMonth = this.findViewById(R.id.nextMonth);
            nextMonth.setOnClickListener(this::changeMonth);

            calendarView = this.findViewById(R.id.calendar);

            // Initialised
            adapter = new GridCellAdapter(getApplicationContext(), month, year, 1);
            adapter.notifyDataSetChanged();
            calendarView.setAdapter(adapter);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void getIntentData() {
        try {

            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_ADVANCE_DAYS)) {
                advance_days = getIntent().getIntExtra(Constant.ScreenExtras.EVENT_ADVANCE_DAYS, 0);
                //Log.e(TAG, "advance_days = " + advance_days);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_TOTAL_TICKET)) {
                per_trans_limit = getIntent().getIntExtra(Constant.ScreenExtras.EVENT_TOTAL_TICKET, 0);
                // Log.e(TAG, "total_ticket = " + total_ticket);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.HOURS)) {
                hours = getIntent().getIntExtra(Constant.ScreenExtras.HOURS, 0);
                // Log.e(TAG, "hours = " + hours);
            }

       /* if (getIntent().hasExtra(Constant.ScreenExtras.EXCEPTION_DATES)) {
            exceptionDate = getIntent().getStringArrayListExtra(Constant.ScreenExtras.EXCEPTION_DATES);
            //Log.e(TAG, "exceptionDate = " + exceptionDate);
        }*/

            if (getIntent().hasExtra(Constant.ScreenExtras.EXCEPTION_DATES)) {
                MyEventDetail myEvent = (MyEventDetail) getIntent().getSerializableExtra(Constant.ScreenExtras.EXCEPTION_DATES);
                exceptionDate = myEvent.getExceptionDates();
                Log.e(TAG, "exceptionDate = " + exceptionDate);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_ID))
                eventId = getIntent().getLongExtra(Constant.ScreenExtras.EVENT_ID, 0);
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_NAME))
                eventName = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_NAME);
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_DISCOUNT_LABEL))
                extraDiscountLabel = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_DISCOUNT_LABEL);
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_DISCOUNT_PER))
                extraDiscountPer = getIntent().getDoubleExtra(Constant.ScreenExtras.EVENT_DISCOUNT_PER, 0);

            if (getIntent().hasExtra(Constant.ScreenExtras.MULTI_CATEGORY_SELECT))
                multiCategorySelect = getIntent().getIntExtra(Constant.ScreenExtras.MULTI_CATEGORY_SELECT,0);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setGridCellAdapterToDate(int month, int year) {
        try {
            adapter = new GridCellAdapter(getApplicationContext(), month, year, 0);
            _calendar.set(year, month - 1, _calendar.get(Calendar.DAY_OF_MONTH));

            String month1 = (DateFormat.format(MONTH, _calendar.getTime()).toString());
            String year1 = (DateFormat.format(YEAR, _calendar.getTime()).toString());

            setCurrentMonthText(month1, year1);
            adapter.notifyDataSetChanged();
            calendarView.setAdapter(adapter);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void setCurrentMonthText(String month, String year) {
        try {
            SpannableStringBuilder monthBuilder = new SpannableStringBuilder();
            monthBuilder.append(month).append("\n").append(year);
            monthBuilder.setSpan(new RelativeSizeSpan(1f), 0, month.length(), 0);
            monthBuilder.setSpan(new ForegroundColorSpan(Color.RED), month.length(), month.length() + year.length() + 1, 0);
            monthBuilder.setSpan(new RelativeSizeSpan(0.7f), month.length(), month.length() + year.length() + 1, 0);
            currentMonth.setText(monthBuilder);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    // Inner Class
    private class GridCellAdapter extends BaseAdapter implements View.OnClickListener {
        private final String tag = GridCellAdapter.class.getSimpleName();
        private final Context _context;

        private final List<String> list;
        private static final int DAY_OFFSET = 1;
        private final String[] weekdays = new String[]{"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
        private final String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        private final int[] daysOfMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        private int daysInMonth;
        private int currentDayOfMonth;
        private int currentWeekDay;
        private TextView gridcell;
        private RelativeLayout rl_main;
        private TextView num_events_per_day;
        private final HashMap eventsPerMonthMap;
        private final SimpleDateFormat dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
        private LayoutInflater inflater;

        // Days in Current Month
        GridCellAdapter(Context context, int month, int year, int from) {
            super();
            this._context = context;
            this.list = new ArrayList<>();
            inflater = LayoutInflater.from(_context);
            Log.d(tag, "==> Passed in event_date FOR Month: " + month + " " + "Year: " + year);
            Calendar calendar = Calendar.getInstance();

            if (from == 1) {
                setCurrentDayOfMonth(calendar.get(Calendar.DAY_OF_MONTH));
                setCurrentWeekDay(calendar.get(Calendar.DAY_OF_WEEK));
                prev_year = calendar.get(Calendar.YEAR);
                prev_month = calendar.get(Calendar.MONTH) + 1;

            } else {
                currentDayOfMonth = prev_date;
            }

            // Print Month
            printMonth(month, year);

            // Find Number of Events
            eventsPerMonthMap = findNumberOfEventsPerMonth();
        }

        private String getMonthAsString(int i) {
            return months[i];
        }

        private String getWeekDayAsString(int i) {
            return weekdays[i];
        }

        private int getNumberOfDaysOfMonth(int i) {
            return daysOfMonth[i];
        }

        public String getItem(int position) {
            return list.get(position);
        }

        @Override
        public int getCount() {
            return list.size();
        }

        private void printMonth(int mm, int yy) {
            try {
                Log.d(tag, "==> printMonth: mm: " + mm + " " + "yy: " + yy);
                int trailingSpaces;
                int daysInPrevMonth;
                int prevMonth;
                int prevYear;
                int nextMonth;
                int nextYear;

                int currentMonth = mm - 1;
                String currentMonthName = getMonthAsString(currentMonth);
                daysInMonth = getNumberOfDaysOfMonth(currentMonth);

                Log.d(tag, "Current Month: " + " " + currentMonthName + " having " + daysInMonth + " days.");

                // Gregorian Calendar : MINUS 1, set to FIRST OF MONTH
                GregorianCalendar cal = new GregorianCalendar(yy, currentMonth, 1);
                Log.d(tag, "Gregorian Calendar:= " + cal.getTime().toString());

                if (currentMonth == 11) {
                    prevMonth = currentMonth - 1;
                    daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                    nextMonth = 0;
                    prevYear = yy;
                    nextYear = yy + 1;
                    Log.d(tag, "*->PrevYear: " + prevYear + " PrevMonth:" + prevMonth + " NextMonth: " + nextMonth + " NextYear: " + nextYear);
                } else if (currentMonth == 0) {
                    prevMonth = 11;
                    prevYear = yy - 1;
                    nextYear = yy;
                    daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                    nextMonth = 1;
                    Log.d(tag, "**--> PrevYear: " + prevYear + " PrevMonth:" + prevMonth + " NextMonth: " + nextMonth + " NextYear: " + nextYear);
                } else {
                    prevMonth = currentMonth - 1;
                    nextMonth = currentMonth + 1;
                    nextYear = yy;
                    prevYear = yy;
                    daysInPrevMonth = getNumberOfDaysOfMonth(prevMonth);
                    Log.d(tag, "***---> PrevYear: " + prevYear + " PrevMonth:" + prevMonth + " NextMonth: " + nextMonth + " NextYear: " + nextYear);
                }

                // Compute how much to leave before before the first day of the
                // month.
                // getDay() returns 0 for Sunday.
                int currentWeekDay = cal.get(Calendar.DAY_OF_WEEK) - 1;
                trailingSpaces = currentWeekDay;

                //Log.e(tag, "Week Day:" + currentWeekDay + " is " + getWeekDayAsString(currentWeekDay));
                // Log.e(tag, "No. Trailing space to Add: " + trailingSpaces);
                // Log.e(tag, "No. of Days in Previous Month: " + daysInPrevMonth);

                if (cal.isLeapYear(cal.get(Calendar.YEAR)) && mm == 1) {
                    ++daysInMonth;
                }

                // Trailing Month days
                for (int i = 0; i < trailingSpaces; i++) {
                    // Log.e(tag, "PREV MONTH:= " + prevMonth + " => " + getMonthAsString(prevMonth) + " " + String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i));
                    list.add(String.valueOf((daysInPrevMonth - trailingSpaces + DAY_OFFSET) + i) + "-GREY" + "-" + getMonthAsString(prevMonth) + "-" + prevYear);
                }

                // Current Month Days
                for (int i = 1; i <= daysInMonth; i++) {
                    Log.d(currentMonthName, String.valueOf(i) + " " + getMonthAsString(currentMonth) + " " + yy);

                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.HOUR_OF_DAY, 0);
                    calendar.set(Calendar.MINUTE, 0);
                    calendar.set(Calendar.SECOND, 5);
                    calendar.set(Calendar.DAY_OF_MONTH, i);
                    calendar.set(Calendar.MONTH, currentMonth);
                    calendar.set(Calendar.YEAR, yy);

                    //Mon Aug 13 03:00:00 GMT+05:30 2018
              /*  Calendar cal1 = Calendar.getInstance();
                cal.setTimeInMillis(System.currentTimeMillis());
                if (i == 11 || i == 12 || i == 13) {
                    Log.e(TAG, "i: " + i + ",calendar.time= " + calendar.getEvent_time());
                    Log.e(TAG, "calendar.minDate= " + minDate.getEvent_time());
                    // Log.e(TAG, "cal1.event_date= " + cal1.getEvent_time());
                }*/

                    String tagStr;
                    if (calendar.before(minDate) || calendar.after(maxDate)) {
                        tagStr = String.valueOf(i) + "-GREY" + "-" + getMonthAsString(currentMonth) + "-" + yy;
                    } else {
                        if (i == getCurrentDayOfMonth() && prev_month == mm && prev_year == yy) {
                            tagStr = String.valueOf(i) + "-BLUE" + "-" + getMonthAsString(currentMonth) + "-" + yy;
                        } else {
                            tagStr = String.valueOf(i) + "-WHITE" + "-" + getMonthAsString(currentMonth) + "-" + yy;
                        }
                    }
                    list.add(tagStr);
               /*
                if (i == getCurrentDayOfMonth()&& prev_month == mm && prev_year == yy ){
                    if( d.before(c1.getEvent_time()) ||  d.after(c.getEvent_time())){
                        list.add(String.valueOf(i) + "-GREY" + "-" + getMonthAsString(currentMonth) + "-" + yy);
                    }else{
                        list.add(String.valueOf(i) + "-BLUE" + "-" + getMonthAsString(currentMonth) + "-" + yy);
                    }


                } else {
                    list.add(String.valueOf(i) + "-WHITE" + "-" + getMonthAsString(currentMonth) + "-" + yy);
                }*/
                }


                // Leading Month days
                for (int i = 0; i < list.size() % 7; i++) {
                    Log.d(tag, "NEXT MONTH:= " + getMonthAsString(nextMonth));
                    list.add(String.valueOf(i + 1) + "-GREY" + "-" + getMonthAsString(nextMonth) + "-" + nextYear);
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }

        /**
         * NOTE: YOU NEED TO IMPLEMENT THIS PART Given the YEAR, MONTH, retrieve
         * ALL entries from a SQLite database for that month. Iterate over the
         * List of All entries, and get the dateCreated, which is converted into
         * day.
         *
         * @return {@link HashMap}
         */
        private HashMap findNumberOfEventsPerMonth() {
            return new HashMap<String, Integer>();
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;
            if (row == null) {
                row = inflater.inflate(R.layout.day_gridcell, parent, false);
            }

            try {
                // Get a reference to the Day cell
                gridcell = row.findViewById(R.id.calendar_day_gridcell);
                rl_main = row.findViewById(R.id.rl_main);

                // ACCOUNT FOR SPACING
                // Log.e(tag, "Current Day: " + getCurrentDayOfMonth());
                final String[] day_color = list.get(position).split("-");
                final String theDay = day_color[0];
                final String theMonth = day_color[2];
                final String theYear = day_color[3];
                if (eventsPerMonthMap != null && !eventsPerMonthMap.isEmpty()) {
                    if (eventsPerMonthMap.containsKey(theDay)) {
                        num_events_per_day = row.findViewById(R.id.num_events_per_day);
                        Integer numEvents = (Integer) eventsPerMonthMap.get(theDay);
                        num_events_per_day.setText(String.format(Locale.getDefault(), "%d", numEvents));
                    }
                }

                // Set the Day GridCell
                gridcell.setText(theDay);
                rl_main.setTag(theDay + "-" + theMonth + "-" + theYear);
                Log.d(tag, "Setting GridCell " + theDay + "-" + theMonth + "-" + theYear);

                switch (day_color[1]) {
                    case "GREY":
                        //row.setEnabled(false);
                        gridcell.setTextColor(Color.LTGRAY);
                        rl_main.setBackgroundColor(Color.TRANSPARENT);
                        rl_main.setOnClickListener(null);
                        break;

                    case "WHITE":
                        //row.setEnabled(true);
                        gridcell.setTextColor(Color.BLACK);
                        rl_main.setBackgroundColor(Color.TRANSPARENT);

                        rl_main.setOnClickListener(view -> {
                            String wsDate1 = String.format(Locale.getDefault(), "%d-%02d-%02d", prev_year, prev_month, prev_date);
                            String selectedDateMonthYear = (String) view.getTag();
                            String currentDate = DateTimeUtils.changeDateTimeFormat(wsDate1, "yyyy-MM-dd", DateTimeUtils.CALENDAR_DATE_FORMAT);

                            Date selectedDate = DateTimeUtils.stringToDate(selectedDateMonthYear, DateTimeUtils.CALENDAR_DATE_FORMAT);
                            Calendar calendar = Calendar.getInstance();

                            String year1 = (String) DateFormat.format("yyyy", selectedDate);

                            calendar.set(Calendar.YEAR, Integer.parseInt(year1));
                            calendar.set(Calendar.MONTH, selectedDate.getMonth());
                            calendar.set(Calendar.DAY_OF_MONTH, selectedDate.getDate());
                            calendar.set(Calendar.HOUR_OF_DAY, 0);
                            calendar.set(Calendar.MINUTE, 0);
                            calendar.set(Calendar.SECOND, 5);

                            if (calendar.before(minDate) || calendar.after(maxDate)) {
                                MessageUtils.showToast(SelectDateAndTimeActivity.this, R.string.msg_select_valid_date);
                            } else {
                                if (!currentDate.equalsIgnoreCase(selectedDateMonthYear)) {
                                    // String mm = String.valueOf(selectedDate.getMonth() + 1);

                                    prev_month = selectedDate.getMonth() + 1;
                                    prev_date = selectedDate.getDate();

                                    String year = (String) DateFormat.format("yyyy", selectedDate);
                                    prev_year = Integer.parseInt(year);

                                    selectedDayMonthYearButton.setText(String.format("Selected: %s", selectedDateMonthYear));
                                    try {
                                        Date parsedDate = dateFormatter.parse(selectedDateMonthYear);
                                        Log.d(tag, "Parsed event_date: " + parsedDate.toString());

                                    } catch (ParseException e) {
                                        e.printStackTrace();
                                    }

                                    list.clear();
                                    currentDayOfMonth = Integer.parseInt(theDay);
                                    printMonth(selectedDate.getMonth() + 1, Integer.parseInt(theYear));
                                    notifyDataSetChanged();

                                    String wsDate = String.format(Locale.getDefault(), "%d-%02d-%02d", prev_year, prev_month, prev_date);
                                    if (CC.isOnline()) {
                                        wsGetAttractionTimeSlotCategory(eventId, countryCode, wsDate);
                                    } else {
                                        CC.showToast(R.string.msg_no_internet);
                                    }
                                }
                            }
                        });
                        break;

                    case "BLUE":
                        gridcell.setTextColor(Color.WHITE);
                        if (Build.VERSION_CODES.JELLY_BEAN <= Build.VERSION.SDK_INT) {
                            rl_main.setBackground(ContextCompat.getDrawable(SelectDateAndTimeActivity.this, R.drawable.round));
                        }

                        Date selectedDayMonthYear = DateTimeUtils.stringToDate(theDay + "-" + theMonth + "-" + theYear,
                                DateTimeUtils.CALENDAR_DATE_FORMAT);

                        prev_month = selectedDayMonthYear.getMonth() + 1;
                        prev_date = selectedDayMonthYear.getDate();

                        String year = (String) DateFormat.format("yyyy", selectedDayMonthYear);
                        prev_year = Integer.parseInt(year);

                        String wsDate = String.format(Locale.getDefault(), "%d-%02d-%02d", prev_year, prev_month, prev_date);

                        wsGetAttractionTimeSlotCategory(eventId, countryCode, wsDate);
                        break;
                }
            }catch (Exception e){
                e.printStackTrace();
            }

            return row;
        }

        @Override
        public void onClick(View view) {
            String date_month_year = (String) view.getTag();
            selectedDayMonthYearButton.setText(String.format("Selected: %s", date_month_year));
        }

        int getCurrentDayOfMonth() {
            return currentDayOfMonth;
        }

        private void setCurrentDayOfMonth(int currentDayOfMonth) {
            this.currentDayOfMonth = currentDayOfMonth;
        }

        void setCurrentWeekDay(int currentWeekDay) {
            this.currentWeekDay = currentWeekDay;
        }

        /*int getCurrentWeekDay() {
            return currentWeekDay;
        }*/

    }

    public void changeMonth(View v) {
        try {
            if (v == prevMonth) {
                int minMonth = minDate.get(Calendar.MONTH) + 1;
                int minYear = minDate.get(Calendar.YEAR);
                if (minMonth < month || minYear < year) {
                    if (month <= 1) {
                        month = 12;
                        year--;
                    } else {
                        month--;
                    }
                    setGridCellAdapterToDate(month, year);
                }
            }
            if (v == nextMonth) {
                int maxMonth = maxDate.get(Calendar.MONTH) + 1;
                int maxYear = maxDate.get(Calendar.YEAR);
                if (maxMonth > month || maxYear > year) {
                    if (month > 11) {
                        month = 1;
                        year++;
                    } else {
                        month++;
                    }
                    setGridCellAdapterToDate(month, year);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    // WS - GET ATTRACTION CATEGORY & TIME SLOT
    private void wsGetAttractionTimeSlotCategory(long eventId, String currency, String date) {

        try {

            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            selectedDateWS = date;
            RequestAttraction request = new RequestAttraction();
            request.event_id = eventId;
            request.country_code = currency;
            request.select_date = date;
            request.engine_type = Constant.ENGINE_TYPE;


            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackGetAttraction> call = tnAPI.getAttraction(request);
            call.enqueue(new Callback<CallbackGetAttraction>() {
                @Override
                public void onResponse(Call<CallbackGetAttraction> call, @NonNull Response<CallbackGetAttraction> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                // Log.e(TAG,"result= "+response.body().data());
                                timeSlotList = new ArrayList<>();
                                timeSlotList.add(0, new EventTimeSlot(getResources().getString(R.string.select_time_slot), 0));
                                timeSlotList.addAll(Objects.requireNonNull(response.body()).data().time_slot_list);
                                categoryList = Objects.requireNonNull(response.body()).data().category_list;

                                if (timeSlotList != null && timeSlotList.size() > 0) {
                                    SpinnerTimeSlotCalendar spinnerTimeSlot = new SpinnerTimeSlotCalendar(SelectDateAndTimeActivity.this, timeSlotList);
                                    sp_time_slot.setAdapter(spinnerTimeSlot);
                                    selectSpinnerItemByValue(sp_time_slot, selectedTimeSlot, 0, 0);

                                    if (Objects.requireNonNull(response.body()).data().time_slot_list != null && response.body().data().time_slot_list.size() > 0) {
                                        sp_time_slot.setSelection(1);
                                    } else {
                                        sp_time_slot.setSelection(0);
                                    }
                                }

                            } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                                CC.showToast(Objects.requireNonNull(response.body()).getError_description());

                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        } else {
                            CC.showToast(R.string.msg_no_response);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CC.showToast(R.string.msg_something_wrong);
                    }
                }

                @Override
                public void onFailure(Call<CallbackGetAttraction> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        }catch (Exception e){
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    private void selectSpinnerItemByValue(Spinner spinner, String value, int fromSelection, int pos) {

        try {
            if (timeSlotList != null && timeSlotList.size() > 0) {
                if (fromSelection == 1) {
                    spinner.setSelection(pos);
                    selectedTimeSlot = timeSlotList.get(pos).timeSlot();
                } else {
                    for (int position = 0; position < timeSlotList.size(); position++) {
                        if (timeSlotList.get(position).timeSlot().equalsIgnoreCase(value)) {
                            spinner.setSelection(position);
                            selectedTimeSlot = timeSlotList.get(position).timeSlot();
                            break;
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }


    //adding by priyanka
    private void initializeCalendar() {

        try {

            String strMinDate = DateTimeUtils.millieSecondsToDate(DateTimeUtils.getMinDate(hours), DateTimeUtils.SERVER_FORMAT_DATE);
            // Log.e(TAG, "strMinDate = " + strMinDate);
            Date min_date = DateTimeUtils.stringToDate1(strMinDate, DateTimeUtils.SERVER_FORMAT_DATE);
            // Log.e(TAG, "min_date = " + min_date);

            String currDate = DateTimeUtils.millieSecondsToDate(System.currentTimeMillis(), DateTimeUtils.SERVER_FORMAT_DATE);
            //Log.e(TAG, "currDate = " + currDate);
            Date max_date = DateTimeUtils.stringToDate1(currDate, DateTimeUtils.SERVER_FORMAT_DATE);
            // Log.e(TAG, "max_date = " + max_date);

            String min_day = (String) android.text.format.DateFormat.format("dd", min_date);
            // Log.e(TAG, "min_day = " + min_day);
            String max_day = (String) android.text.format.DateFormat.format("dd", max_date);
            //Log.e(TAG, "max_day = " + max_day);

            //int day = Integer.valueOf(min_day) - Integer.valueOf(max_day);
            // day =  Math.abs(day);
            // Log.e(TAG, "diff_day = " + day);
            // advance_days = advance_days + day;
            // Log.e(TAG, "final_advance_days = " + advance_days);

            //change 30/7/2021
            if (advance_days < 0) {
                advance_days = 0;
            }

            wsGetAttractionTimeSlotCategory(eventId, countryCode, DateTimeUtils.millieSecondsToDate(DateTimeUtils.getMinDate(hours), DateTimeUtils.SERVER_FORMAT_DATE));
            MaterialCalendarView mCalendarView = this.findViewById(R.id.calendarView);
            // mCalendarView.setSelectedDate(System.currentTimeMillis());
            mCalendarView.setSelectedDate(DateTimeUtils.getMinDate(hours));
            mCalendarView.state().edit()
                    .setMinimumDate(DateTimeUtils.getMinDate(hours))
                    .setMaximumDate(DateTimeUtils.getMaxDate1(strMinDate, advance_days))
                    .commit();

            ArrayList<CalendarDay> enabledDates = new ArrayList<>();
            Calendar c = Calendar.getInstance();

            if (exceptionDate!= null && !exceptionDate.isEmpty() && exceptionDate.size() > 0) {
                for (int i = 0; i < exceptionDate.size(); i++) {
                    //Date date = DateTimeUtils.stringToDate(exceptionDate.get(i), DateTimeUtils.SERVER_FORMAT_DATE);
                    Date date = DateTimeUtils.stringToDate(exceptionDate.get(i).getDate(), DateTimeUtils.SERVER_FORMAT_DATE);
                    c.setTime(date);
                    c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                    enabledDates.add(new CalendarDay(c));
                }
            }

            mCalendarView.addDecorator(new AvailableDaysDecorator(R.color.colorPrimary, enabledDates));
            mCalendarView.setOnDateChangedListener((materialCalendarView, calendarDay, b) -> {
                String selectDate = DateTimeUtils.dateToString(DateTimeUtils.getDate(calendarDay.getYear(), calendarDay.getMonth(), calendarDay.getDay()), DateTimeUtils.SERVER_FORMAT_DATE);
                //Log.e(TAG, "selectDate: $selectDate , year: ${calendarDay.year} , month: ${calendarDay.month} ,day: ${calendarDay.day}")
                if (CC.isOnline()) {
                    wsGetAttractionTimeSlotCategory(eventId, countryCode, selectDate);
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
