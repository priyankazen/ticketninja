/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.EventFilterAdapter;
import in.ticketninja.adapters.EventsAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.TextViewUtils;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.MyEvent;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestEventList;
import in.ticketninja.ws.response.CallbackEventList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventListActivity extends AppCompatActivity {
    private final String TAG = EventListActivity.class.getSimpleName();
    private CommonClass CC;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;
    private PreferencesUtils mPreferences;
    private String eventTypeName, eventTypeKey, c_tag;

    private RecyclerView recyclerView,rvh,rvDateFilter;
    private LinearLayout llErrorLayout;
    private HorizontalScrollView hsv;
    private ImageView ivErrorImage;
    private LinearLayout llNoRecord;
    private TextView tvErrorMessage, tvErrorButton;
    private String strDate = null;
    EventFilterAdapter eventFilterAdapter = new EventFilterAdapter(new ArrayList<>());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_list);

        try {
            initializeData();

            getIntentData();

            // ACTIONBAR
            setupActionBar();

            findViewById();

            if (CC.isOnline()) {
                wsEventList(eventTypeKey, mPreferences.getCityName());
            } else {
                showNoInternet();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void findViewById() {
        try {
            recyclerView = findViewById(R.id.recyclerView);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            llErrorLayout = findViewById(R.id.llErrorLayout);
            llErrorLayout.setVisibility(View.GONE);
            ivErrorImage = findViewById(R.id.ivErrorImage);
            tvErrorMessage = findViewById(R.id.tvErrorMessage);
            tvErrorButton = findViewById(R.id.tvErrorButton);

            rvDateFilter = findViewById(R.id.rvDateFilter);
            rvh = findViewById(R.id.rvEventFilter);
            hsv = findViewById(R.id.hsv);
            llNoRecord = findViewById(R.id.llNoRecord);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        try {
            // TYPEFACE
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                //getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(String.format("%s", eventTypeName));
                tvTitle.setTypeface(tfMedium);
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        try {
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_TYPE_KEY)) {
                eventTypeName = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_TYPE_NAME);
                eventTypeKey = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_TYPE_KEY);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_C_TYPE)) {
                c_tag = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_C_TYPE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            CC = new CommonClass(this);
            mPreferences = new PreferencesUtils(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showNoInternet() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_connection_lost)
                    .placeholder(R.drawable.ic_connection_lost_static)
                    .into(ivErrorImage);
            tvErrorMessage.setText(R.string.msg_no_internet);
            recyclerView.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.VISIBLE);
            tvErrorButton.setText(R.string.btn_try_again);
            tvErrorButton.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    wsEventList(eventTypeKey, mPreferences.getCityName());
                } else {
                    showNoInternet();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showError() {
        try {
            recyclerView.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);
            tvErrorButton.setVisibility(View.GONE);
            Glide.with(this)
                    .load(R.drawable.ic_no_result_found)
                    .placeholder(R.drawable.ic_no_result_found)
                    .into(ivErrorImage);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboard(this);
        super.onBackPressed();
    }

    // WS - Event List
    private void wsEventList(String type, String cityName) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestEventList request = new RequestEventList(String.valueOf(mLogin.getUserId()), type, cityName, c_tag);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackEventList> call = tnAPI.eventList(request);
            call.enqueue(new Callback<CallbackEventList>() {
                @Override
                public void onResponse(@NonNull Call<CallbackEventList> call, @NonNull Response<CallbackEventList> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                List<MyEvent> myEvents = Objects.requireNonNull(response.body()).data().featur_event_list;
                                if (myEvents != null && myEvents.size() > 0) {

                                    recyclerView.setVisibility(View.VISIBLE);
                                    llErrorLayout.setVisibility(View.GONE);

                                    LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(EventListActivity.this, R.anim.layout_animation_fall_down);
                                    recyclerView.setLayoutAnimation(controller);
                                    EventsAdapter mAdapter = new EventsAdapter(EventListActivity.this, myEvents, Constant.DASHBOARD);
                                    recyclerView.setAdapter(mAdapter);
                                    recyclerView.scheduleLayoutAnimation();

                                    setDateFilterAdapter(myEvents);
                                    setEventFilterAdapter(myEvents);

                                } else {
                                    tvErrorMessage.setText(TextViewUtils.capitalize(getString(R.string.msg_no_event_type_found, eventTypeName)));
                                    showError();
                                }

                            } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                                tvErrorMessage.setText(Objects.requireNonNull(response.body()).getError_description());
                                showError();

                            } else {
                                tvErrorMessage.setText(R.string.msg_something_wrong);
                                showError();
                            }
                        } else {
                            tvErrorMessage.setText(R.string.msg_no_response);
                            showError();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        try {
                            tvErrorMessage.setText(R.string.msg_something_wrong);
                            showError();
                        }catch (Exception e1){
                            e1.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackEventList> call, @NonNull Throwable t) {
                    try {
                        t.printStackTrace();
                        if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                        tvErrorMessage.setText(R.string.msg_something_wrong);
                        showError();
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG,"arrrrrrrrrrrrrrr:" +e.getMessage());
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            try {
                llErrorLayout.setVisibility(View.VISIBLE);
                tvErrorMessage.setText(R.string.msg_something_wrong);
                showError();
            }catch (Exception e1){
                e1.printStackTrace();
            }
        }
    }

    private void setDateFilterAdapter(List<MyEvent> originalList) {
        try {
            ArrayList<MyEvent> values = new ArrayList<>();

            MyEvent eventData = new MyEvent();
            eventData.collection = Constant.EventFilter.TODAY;
            eventData.isSelected = false;
            values.add(eventData);

            MyEvent eventData1 = new MyEvent();
            eventData1.collection = Constant.EventFilter.TOMORROW;
            eventData1.isSelected = false;
            values.add(eventData1);

            MyEvent eventData2 = new MyEvent();
            eventData2.collection = Constant.EventFilter.THIS_WEEKEND;
            eventData2.isSelected = false;
            values.add(eventData2);

            EventFilterAdapter mAdapter = new EventFilterAdapter(values);
            mAdapter.setOnEventSelectedListener((position, myEvent) -> {
                try {
                    updateCollectionList();
                    setEventFilterAdapter(originalList);
                    if (myEvent != null) {
                        strDate = myEvent.getCollection();
                    }else {
                        strDate = null;
                    }
                    filterAndDisplayEvent(strDate, null ,originalList);
                }catch (Exception e){
                    e.printStackTrace();
                }

            });
            rvDateFilter.setAdapter(mAdapter);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void updateCollectionList(){
      try {
          hsv.fullScroll(View.FOCUS_LEFT);
          //hsv.scrollTo(0,0);
          sortingList();
      }catch (Exception e){
          e.printStackTrace();
      }
    }
    private void setEventFilterAdapter(List<MyEvent> originalList){

        try {
            if (eventFilterAdapter.getItemCount()>0) {
                eventFilterAdapter.updatePosition();
            }else {
                ArrayList<String> values= new ArrayList<>();
                ArrayList<MyEvent> myEventArrayList= new ArrayList<>();
                if (originalList.size() > 0) {
                    for (MyEvent event : originalList) {
                        if (!TextUtils.isEmpty(event.getCollection().trim()) && !values.contains(event.getCollection())) {
                            values.add(event.getCollection());
                            myEventArrayList.add(event);
                        }
                    }
                }
                eventFilterAdapter = new EventFilterAdapter(myEventArrayList);
                eventFilterAdapter.setOnEventSelectedListener((position, myEvent) -> {
                    try {
                        updateCollectionList();
                        filterAndDisplayEvent(strDate, myEvent, originalList);
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                });
                rvh.setAdapter(eventFilterAdapter);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void sortingList(){
        try {
            Collections.sort(eventFilterAdapter.getEventArray(), (o1, o2) -> {
                //Log.e(TAG,"sortingList: "+o1+", "+o2);
                boolean b1 = o1.isSelected;
                boolean b2 = o2.isSelected;
                return Boolean.compare(b2, b1);
            });
            eventFilterAdapter.notifyDataSetChanged();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void filterAndDisplayEvent(String strDate,MyEvent strCollection,List<MyEvent> originalEventList) {
        try {

            List<MyEvent> myEventArrayList = new ArrayList<>();
            Log.e(TAG,"strDate: "+strDate);
            Log.e(TAG,"strCollection: "+strCollection);
            if (strCollection != null || strDate != null ) {
                if (strDate != null  && !TextUtils.isEmpty(strDate)) {
                    switch (strDate) {
                        case Constant.EventFilter.TODAY:
                            String currentDate = DateTimeUtils.dateToString(DateTimeUtils.getTodayDate(), DateTimeUtils.SERVER_FORMAT_DATE);
                            //Log.e(TAG, "currentDate: " + currentDate);
                            for (MyEvent event : originalEventList) {
                                if (currentDate.equalsIgnoreCase(event.getEvent_date()) || currentDate.equalsIgnoreCase(event.getDateTo()) || event.getEvent_date().equalsIgnoreCase("0001-01-01") || event.getDateTo().equalsIgnoreCase("0001-01-01")) {
                                    if (strCollection != null) {
                                        if (strCollection.getCollection().equalsIgnoreCase(event.getCollection())) {
                                            myEventArrayList.add(event);
                                        }
                                    } else {
                                        event.isSelected = false;
                                        myEventArrayList.add(event);
                                    }
                                }
                            }
                            break;
                        case Constant.EventFilter.TOMORROW:
                            String nextDate = DateTimeUtils.dateToString(DateTimeUtils.getTomorrowDate(), DateTimeUtils.SERVER_FORMAT_DATE);
                            //Log.e(TAG, "nextDate: " + nextDate);
                            for (MyEvent event : originalEventList) {
                                if (nextDate.equalsIgnoreCase(event.getEvent_date()) || nextDate.equalsIgnoreCase(event.getDateTo()) || event.getEvent_date().equalsIgnoreCase("0001-01-01") || event.getDateTo().equalsIgnoreCase("0001-01-01")) {
                                    if (strCollection != null) {
                                        if (strCollection.getCollection().equalsIgnoreCase(event.getCollection())) {
                                            myEventArrayList.add(event);
                                        }
                                    } else {
                                        event.isSelected = false;
                                        myEventArrayList.add(event);
                                    }
                                }
                            }
                            break;
                        case Constant.EventFilter.THIS_WEEKEND:

                            Calendar calendar = Calendar.getInstance();
                            //Log.e("sWeekGetCurrentTime",""+calendar.getTime());
                            //first day of week
                            calendar.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
                            String strWeekFirstDate = DateTimeUtils.dateToString(calendar.getTime(), DateTimeUtils.SERVER_FORMAT_DATE);
                            // Log.e("strWeekFirstDate",""+strWeekFirstDate);

                            calendar.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
                            String strWeekLastDate = DateTimeUtils.dateToString(calendar.getTime(), DateTimeUtils.SERVER_FORMAT_DATE);
                            // Log.e("strWeekLastDate",""+strWeekLastDate);

                            for (MyEvent event : originalEventList) {
                                // Log.e("strWeekDate", event.getEvent_name() + "  , " + event.getCollection() + " , " + event.getEvent_date() + " , " + event.getDateTo());
                                if (strWeekFirstDate.equalsIgnoreCase(event.getEvent_date()) || strWeekFirstDate.equalsIgnoreCase(event.getDateTo()) ||
                                        strWeekLastDate.equalsIgnoreCase(event.getEvent_date()) || strWeekLastDate.equalsIgnoreCase(event.getDateTo())
                                        || event.getEvent_date().equalsIgnoreCase("0001-01-01") || event.getDateTo().equalsIgnoreCase("0001-01-01")
                                ) {
                                    if (strCollection != null) {
                                        if (strCollection.getCollection().equalsIgnoreCase(event.getCollection())) {
                                            myEventArrayList.add(event);
                                        }
                                    } else {
                                        event.isSelected = false;
                                        myEventArrayList.add(event);
                                    }
                                }
                            }
                            break;
                    }
                } else {
                    for (MyEvent event : originalEventList) {
                        if (strCollection.getCollection().equalsIgnoreCase(event.getCollection())) {
                            myEventArrayList.add(event);
                        }
                    }
                }
            }else {
                myEventArrayList = originalEventList;
            }

            Log.e(TAG,"myEventArrayList: "+myEventArrayList);
            //Log.e(TAG,"getEventArray: "+eventFilterAdapter.getEventArray());


            LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(EventListActivity.this, R.anim.layout_animation_fall_down);
            recyclerView.setLayoutAnimation(controller);
            EventsAdapter mAdapter = new EventsAdapter(EventListActivity.this, myEventArrayList, Constant.DASHBOARD);
            recyclerView.setAdapter(mAdapter);
            recyclerView.scheduleLayoutAnimation();
            if (myEventArrayList.size() > 0){
                recyclerView.setVisibility(View.VISIBLE);
                llNoRecord.setVisibility(View.GONE);
            }else {
                recyclerView.setVisibility(View.GONE);
                llNoRecord.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
