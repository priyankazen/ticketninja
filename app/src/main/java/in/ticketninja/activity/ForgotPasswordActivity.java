/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestForgotPassword;
import in.ticketninja.ws.response.CallbackForgotPassword;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {

    private CommonClass CC;
    private SRKLoaderDialog mLoader;

    private EditText etEmail;
    private String emailExtra = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        initializeData();

        // ACTIONBAR
        setupActionBar();

        getIntentData();

        findViewById();

        Button btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(v -> {
            Utility.hideKeyboard(this);
            if (CC.isOnline()) {
                if (checkValidation()) {
                    String email = etEmail.getText().toString().trim();
                    wsForgotPassword(email);
                }
            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        });

    }

    private void initializeData() {
        try {

            CC = new CommonClass(this);
            mLoader = new SRKLoaderDialog(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findViewById() {

        try {
            // TYPEFACE
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);
            etEmail = findViewById(R.id.etForgotEmail);
            etEmail.setTypeface(tfMedium);
            etEmail.setText(String.format("%s", emailExtra));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        try {
            if (getIntent().hasExtra(Constant.ScreenExtras.DATA)) {
                emailExtra = getIntent().getStringExtra(Constant.ScreenExtras.DATA);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboard(this);
        super.onBackPressed();
    }

    private boolean checkValidation() {
        String email = etEmail.getText().toString().trim();

        if (Validate.isNull(email)) {
            CC.showToast(R.string.login_msg_enter_email);
            etEmail.requestFocus();
            return false;

        } else if (!Validate.checkEmail(email)) {
            CC.showToast(R.string.login_msg_enter_email_valid);
            etEmail.requestFocus();
            return false;

        } else {
            return true;
        }
    }

    // WS - Forgot password
    private void wsForgotPassword(String email) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestForgotPassword request = new RequestForgotPassword();
            request.email = email;

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackForgotPassword> call = tnAPI.forgotPassword(request);
            call.enqueue(new Callback<CallbackForgotPassword>() {
                @Override
                public void onResponse(@NonNull Call<CallbackForgotPassword> call, @NonNull Response<CallbackForgotPassword> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        // if (response.body().data().isSuccess()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            // CC.showAlert(response.body().data().message(), () -> onBackPressed());
                            CC.showAlert(Objects.requireNonNull(response.body()).getError_description(), () -> onBackPressed());

                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {// else if (Validate.isNotNull(response.body().data().message())) {
                            //CC.showAlert(response.body().data().message());
                            CC.showAlert(Objects.requireNonNull(response.body()).getError_description());
                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackForgotPassword> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

}
