/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

//import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.MessageDigest;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.NotificationChannelUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.AppVersion;
import in.ticketninja.objects.MyEvent;
import in.ticketninja.objects.UserInfo;
import in.ticketninja.service.TimerService;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestBasicInfo;
import in.ticketninja.ws.request.RequestForceUpdate;
import in.ticketninja.ws.request.RequestLogout;
import in.ticketninja.ws.request.RequestLongUrl;
import in.ticketninja.ws.response.CallbackBasicInfo;
import in.ticketninja.ws.response.CallbackForceUpdate;
import in.ticketninja.ws.response.CallbackLogout;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashActivity extends AppCompatActivity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private LoginUtils mLogin;
    private PreferencesUtils mPref;
    private Response<CallbackForceUpdate> res = null;
    private Uri uri;
    FirebaseCrashlytics crashlytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.activity_splash);

            // WifiManager wm = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
            //String ip = Formatter.formatIpAddress(Objects.requireNonNull(wm).getConnectionInfo().getNetworkId());
            //Log.e(TAG, "ip= >" + ip);

            //getLocalIpAddress();

            mLogin = new LoginUtils(this);
            mPref = new PreferencesUtils(this);
            crashlytics = FirebaseCrashlytics.getInstance();

            if (Utility.isOnline(this) && mLogin.isLoggedIn()) {
                wsBasicInfo(mLogin.getUserId());
            }

            getKeyHash();

            uri = handleIntent();
            Log.e(TAG, "uri = " + uri);

            try {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    NotificationChannelUtils channelUtils = new NotificationChannelUtils(this);
                    channelUtils.createChannels();
                }

                Log.e(TAG, "Token= >" + PreferencesUtils.getFCMPushKey(this));
                if (!PreferencesUtils.isFCMTopicRegister(this) && Validate.isNotNull(PreferencesUtils.getFCMPushKey(this))) {
                    FirebaseMessaging.getInstance().subscribeToTopic("Event_Detail");
                    PreferencesUtils.setFCMTopicRegister(this, true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                if (Utility.isOnline(this)) {
                    wsCheckLatestVersion();
                } else {
                    MessageUtils.showToast(getBaseContext(), R.string.msg_no_internet);
                    if (Validate.isNotNull(PreferencesUtils.getAPP_VERSION(getBaseContext())) && Validate.isNotNull(PreferencesUtils.getAppMessage(getBaseContext()))) {
                        if (Integer.parseInt(PreferencesUtils.getAPP_VERSION(getBaseContext())) > Utility.getAppVersionCode(SplashActivity.this)
                                && PreferencesUtils.isIS_FORCEFULLY(getBaseContext())) {

                            Log.e(TAG, " and " + PreferencesUtils.getAPP_VERSION(getBaseContext()));
                            Log.e(TAG, " and " + Utility.getAppVersionCode(SplashActivity.this));
                            Log.e(TAG, " and " + PreferencesUtils.isIS_FORCEFULLY(getBaseContext()));

                            CommonClass.showForceUpdateDialogOffline(SplashActivity.this);
                        } else {
                            onSplashComplete();
                        }
                    } else {
                        onSplashComplete();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getKeyHash() {
        // Add code to print out the key hash
        try {
            @SuppressLint("PackageManagerGetSignatures")
            //PackageInfo info = getPackageManager().getPackageInfo("in.ticketninja.test", PackageManager.GET_SIGNATURES);
                    PackageInfo info = getPackageManager().getPackageInfo(getApplicationContext().getPackageName(), PackageManager.GET_SIGNATURES);
            // Log.e(TAG, "getPackageName= >" + getApplicationContext().getPackageName());

            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onSplashComplete() {

        try {
            new CountDownTimer(3000, 1000) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    if (!isFinishing()) {
                        startNextActivity();
                    }
                }
            }.start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void startLandingActivity1() {
        try {
            //Intent intent = new Intent(getApplicationContext(), NotificationListActivity.class);
            Intent intent = new Intent(getApplicationContext(), LandingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            //finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startNextActivity() {
        try {

            if (Utility.isMyServiceRunning(this, TimerService.class)) {
                stopService(new Intent(this, TimerService.class));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Log.e(TAG, "isOpenApp:" + Utility.isAppOpen(SplashActivity.this) + "");
            if (getIntent() != null && getIntent().getExtras() != null) {

                Bundle bundle = getIntent().getExtras();
                String msg = bundle.getString(Constant.ScreenExtras.MESSAGE);
                //String msg = getIntent().getStringExtra(Constant.ScreenExtras.MESSAGE);
                Log.e(TAG, "message:" + msg);
                if (msg != null && !TextUtils.isEmpty(msg)) {
                    JSONObject msgObject = new JSONObject(msg);
                    JSONObject apsObject = new JSONObject(msgObject.getString("aps"));
                    String msgData = apsObject.optString("data");

                    if (Validate.isNotNull(msgData)) {
                        JSONObject data = new JSONObject(apsObject.getString("data"));

                        String LoginMasterID = data.optString("LoginMasterID", "");
                        int nType = data.optInt("N_Type", 0);

                        if (nType == Constant.NotificationType.BULK_EVENT_DETAIL) {
                            long EventId = data.optLong("Event_Id", 0);
                            MyEvent myEvent = new MyEvent();
                            myEvent.event_id = EventId;
                            Intent noteIntent = new Intent(this, EventDetailActivity.class);
                            noteIntent.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                            noteIntent.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_NOTIFICATION);
                            noteIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                            startActivity(noteIntent);

                        } else if (mLogin.isLoggedIn() && LoginMasterID.equals(mLogin.getUserId())) {
                            long EventId = data.optLong("Event_Id", 0);
                            //long ReferenceId = data.optLong("Reference_Id", 0);
                            //long NotificationID = data.optLong("NotificationID", 0);

                            if (nType == Constant.NotificationType.EVENT) {
                                MyEvent myEvent = new MyEvent();
                                myEvent.event_id = EventId;
                                Intent noteIntent = new Intent(this, EventDetailActivity.class);
                                noteIntent.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                                noteIntent.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_NOTIFICATION);
                                noteIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(noteIntent);

                            } else if (nType == Constant.NotificationType.TICKET) {
                                Intent noteIntent = new Intent(this, MyTicketsActivity.class);
                                noteIntent.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_NOTIFICATION);
                                noteIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(noteIntent);
                            }
                        } else {
                            startLandingActivity();
                        }
                    }
                } else {
                    startLandingActivity();
                }
            } else {
                startLandingActivity();
            }
        } catch (Exception e) {
            e.printStackTrace();
            startLandingActivity();
        }
    }

    // WS - Basic Info
    private void wsBasicInfo(String userId) {
        try {
            RequestBasicInfo request = new RequestBasicInfo(String.valueOf(userId));

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackBasicInfo> call = tnAPI.basicInfo(request);
            call.enqueue(new Callback<CallbackBasicInfo>() {
                @Override
                public void onResponse(@NonNull Call<CallbackBasicInfo> call, @NonNull Response<CallbackBasicInfo> response) {
                    Log.e(TAG, "response:" + response);
                    if (response.isSuccessful()) {
                        //if (Objects.requireNonNull(response.body()).data().isSuccess()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                            // REMOVE CACHED DASHBOARD DATA
                            // CacheHelper mCacheHelper = new CacheHelper(SplashActivity.this);
                            // mCacheHelper.remove(CacheHelper.KEY_DASHBOARD);

                            UserInfo userInfo = Objects.requireNonNull(response.body()).data();
                            LoginUtils mLogin = new LoginUtils(SplashActivity.this);
                            mLogin.setUserId(userInfo.user_id);
                            mLogin.setFirstName(userInfo.firstname);
                            mLogin.setLastName(userInfo.lastname);
                            mLogin.setEmail(userInfo.email);
                            mLogin.setCountryCode(userInfo.country_code);
                            mLogin.setMobile(userInfo.mobileno);
                            mLogin.setGender(userInfo.gender);
                            mLogin.setPicture(userInfo.user_imageid_url);
                            mLogin.setUserDOBWithParse(userInfo.birthdate);
                            mLogin.setNotifications(userInfo.notification_unread_count);

                            if (Validate.isNotNull(mPref.getUserEmailId())) {
                                mPref.setUserEmailId(userInfo.email);
                            }
                            if (Validate.isNotNull(mPref.getUserPhoneNumber())) {
                                mPref.setUserPhoneNumber(userInfo.mobileno);
                            }
                            if (Validate.isNotNull(mPref.getCountryCode())) {
                                mPref.setCountryCode(userInfo.country_code);
                            }


                            try {
                                // FirebaseAnalytics
                                FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
                                mFirebaseAnalytics.setUserProperty("tn_user_id", mLogin.getUserId() + "");
                                mFirebaseAnalytics.setUserProperty("tn_user_name", mLogin.getName() + "");
                                mFirebaseAnalytics.setUserId(mLogin.getUserId() + "");
                                // Crashlytics
                                crashlytics.setUserId("" + mLogin.getUserId());
                                //Crashlytics.setUserIdentifier("" + mLogin.getUserId());
                                //Crashlytics.setUserEmail("" + mLogin.getEmail());
                               // Crashlytics.setUserName("" + mLogin.getName());
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            checkLogOut();
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackBasicInfo> call, @NonNull Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // WS - CHECK LATEST APP VERSION
    private void wsCheckLatestVersion() {
        try {
            RequestForceUpdate request = new RequestForceUpdate(getPackageName());

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackForceUpdate> call = tnAPI.forceUpdate(request);
            call.enqueue(new Callback<CallbackForceUpdate>() {
                @Override
                public void onResponse(@NonNull Call<CallbackForceUpdate> call, @NonNull Response<CallbackForceUpdate> response) {
                    try {
                        Log.e(TAG, "wsCheckLatestVersion response:" + response.code());
                        if (response.code() == 404) {
                            CommonClass.showForceUpdateDialog(SplashActivity.this, null);
                        } else {
                            if (response.isSuccessful()) {

                                AppVersion appVersion = Objects.requireNonNull(response.body()).data();
                                //if (appVersion != null && appVersion.isSuccess()) {
                                Log.e(TAG, "wsCheckLatestVersion getError_code:" + response.body().getError_code());
                                if (appVersion != null && String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                                    PreferencesUtils.setVersionCode(getBaseContext(), String.valueOf(appVersion.getAppVersion()));
                                    Log.e(TAG, appVersion.getAppVersion() + " and " + PreferencesUtils.getAPP_VERSION(getBaseContext()));

                                    PreferencesUtils.setIS_FORCEFULLY(getBaseContext(), appVersion.isForcefully());
                                    PreferencesUtils.setVersionMessage(getBaseContext(), appVersion.getMessage());
                                    Log.e(TAG, appVersion.isForcefully() + " and " + PreferencesUtils.isIS_FORCEFULLY(getBaseContext()) + " and " + PreferencesUtils.getAppMessage(getBaseContext()));

                                    if (appVersion.getAppVersion() > Utility.getAppVersionCode(SplashActivity.this)
                                            && appVersion.isForcefully()) {
                                        CommonClass.showForceUpdateDialog(SplashActivity.this, appVersion);
                                    } else {
                                        res = response;
                                        onSplashComplete();
                                    }
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        onSplashComplete();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackForceUpdate> call, @NonNull Throwable t) {
                    Log.e(TAG, "onFailure:" + t.getMessage());
                    t.printStackTrace();
                    onSplashComplete();
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void startLandingActivity() {
        try {

            if (uri != null) {
                openActivity(uri);
            } else {
                if (res != null && Objects.requireNonNull(res.body()).data.isLogin_required()) {
                    PreferencesUtils.setIsLoginRequired(this, Objects.requireNonNull(res.body()).data.isLogin_required());
                    if (Utility.isOnline(this)) {
                        if (mLogin.isLoggedIn()) {
                            startLandingActivity1();
                        } else {
                            startLoginActivity("");
                        }
                    } else {
                        MessageUtils.showToast(getBaseContext(), R.string.msg_no_internet);
                    }
                } else {
                    PreferencesUtils.setIsLoginRequired(this, false);
                    startLandingActivity1();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void startLoginActivity(String mailExtra) {
        try {
            Intent intLogin = new Intent(this, LoginActivity.class);
            intLogin.putExtra(Constant.ScreenExtras.EMAIL, mailExtra);
            intLogin.putExtra(Constant.ScreenExtras.LOGIN_REQUIRED, Objects.requireNonNull(res.body()).data.isLogin_required());
            startActivityForResult(intLogin, Constant.ActivityForResult.LOGIN);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            Log.e(TAG, "requestCode: " + requestCode + " , resultCode:" + resultCode);
            if (requestCode == Constant.ActivityForResult.LOGIN &&
                    resultCode == Activity.RESULT_OK) {
                int intExtra = data.getIntExtra(Constant.ScreenExtras.DATA, 0);
                if (Constant.ScreenExtras.PRESS_SIGNUP == intExtra) {
                    openRegister();
                } else {
                    startLandingActivity1();
                }
            } else if (requestCode == Constant.ActivityForResult.REGISTER
                    && resultCode == Activity.RESULT_OK) {
                int intExtra = data.getIntExtra(Constant.ScreenExtras.DATA, 0);
                if (Constant.ScreenExtras.PRESS_LOGIN == intExtra) {
                    String mailExtra = "";
                    if (data.hasExtra(Constant.ScreenExtras.EMAIL))
                        mailExtra = data.getStringExtra(Constant.ScreenExtras.EMAIL);
                    startLoginActivity(mailExtra);
                }
            } else if (requestCode == Constant.ActivityForResult.REGISTER) {
                startLoginActivity("");
            } else if (requestCode == Constant.ActivityForResult.LOGIN) {
                finishAffinity();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void openRegister() {
        try {
            Intent intRegister = new Intent(this, RegisterActivity.class);
            intRegister.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_MY_PROFILE);
            startActivityForResult(intRegister, Constant.ActivityForResult.REGISTER);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void checkLogOut() {
        try {
            // boolean digitsOnly = TextUtils.isDigitsOnly(mLogin.getUserId());//b899461b7561077a1b88a9f9f9eb4b07
            // if (digitsOnly) {
            String deviceToken = PreferencesUtils.getFCMPushKey(this);
            wsLogout(mLogin.getUserId(), deviceToken);
            mLogin.logout();
            //}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // WS - LOGOUT
    private void wsLogout(String userId, String deviceToken) {
        try {
            RequestLogout request = new RequestLogout(String.valueOf(userId), deviceToken);
            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackLogout> call = tnAPI.logout(request);
            call.enqueue(new Callback<CallbackLogout>() {
                @Override
                public void onResponse(@NonNull Call<CallbackLogout> call, @NonNull Response<CallbackLogout> response) {
                }

                @Override
                public void onFailure(@NonNull Call<CallbackLogout> call, @NonNull Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

   /* private String getLocalIpAddress() {

        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        Log.i(TAG, "***** IP=" + inetAddress.getHostAddress());
                        return inetAddress.getHostAddress();
                    }
                }
            }
        } catch (SocketException ex) {
            ex.printStackTrace();
        }
        return null;
    }*/

    private Uri handleIntent() {
        try {
            // ATTENTION: This was auto-generated to handle app links.
            Intent appLinkIntent = getIntent();
            String appLinkAction = appLinkIntent.getAction();
            Log.e(TAG, "appLinkAction =" + appLinkAction);
            Uri appLinkData = appLinkIntent.getData();
            Log.e(TAG, "appLinkData =" + appLinkData);
            Log.e(TAG, "appLinkScheme =" + appLinkIntent.getScheme());

            if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null) {
                Log.e(TAG, "appLinkPath =" + appLinkData.getPath());
                String recipeId = appLinkData.getLastPathSegment();
                Log.e(TAG, "recipeId =" + recipeId);
                //openActivity(appLinkData);

            }
            return appLinkData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    private void openActivity(Uri myUri) {
        try {
            Log.e(TAG, "myUri.getPath() =" + myUri.getPath());
            if (Objects.requireNonNull(myUri.getPath()).contains(Constant.DeepLinkPathPrefix.EVENT)) {
                Intent noteIntent = new Intent(getApplicationContext(), EventDetailActivity.class);
                noteIntent.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_NOTIFICATION);
                noteIntent.putExtra(Constant.ScreenExtras.DEEP_LINK_URI, myUri.toString());
                noteIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(noteIntent);
                finish();
            } else if (myUri.getPath().contains(Constant.DeepLinkPathPrefix.BOOKING_CONFIRMATION)) {
                Intent intConfirm = new Intent(getApplicationContext(), BookingHistoryDetailActivity.class);
                intConfirm.putExtra(Constant.ScreenExtras.EVENT_HISTORY_TYPE, Constant.HistoryType.PAST);
                intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, (long) 0);
                intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE, "");
                intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TICKET_LIVE, 0);
                intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_APP_TICKET, 0);
                intConfirm.putExtra(Constant.ScreenExtras.DEEP_LINK_URI, myUri.toString());
                intConfirm.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intConfirm);
                finish();
            } else if (myUri.getPath().contains(Constant.DeepLinkPathPrefix.VIEW_TICKET)) {
                Intent intViewTicket = new Intent(getApplicationContext(), MTicketsActivity.class);
                intViewTicket.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, 0);
                intViewTicket.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE, "");
                intViewTicket.putExtra(Constant.ScreenExtras.DEEP_LINK_URI, myUri.toString());
                intViewTicket.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intViewTicket);
                finish();
            } else {
                wsLongUrl(myUri.getLastPathSegment());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    // WS -getLongUrl
    private void wsLongUrl(String short_code) {
        try {
            RequestLongUrl request = new RequestLongUrl(short_code);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<ResponseBody> call = tnAPI.getLongUrl(request);
            call.enqueue(new Callback<ResponseBody>() {
                @SuppressWarnings("ConstantConditions")
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    try {
                        String myData = response.body().string();
                        Log.e(TAG, "data =" + myData);
                        // {"error_code":"1","error_description":"sucess","data":{"long_url":"https://ticketninja.in/BookingConfirmation/OWK70569694"}}
                        if (response.isSuccessful()) {
                            try {
                                JSONObject jo = new JSONObject(myData);
                                String des = jo.getString("error_description");
                                if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(jo.getString("error_code")))) {
                                    Log.e(TAG, "if");
                                    JSONObject joData = jo.getJSONObject("data");
                                    String url = joData.getString("long_url");
                                    Log.e(TAG, "url : " + url);
                                    Uri myUri = Uri.parse(url);
                                    openActivity(myUri);
                                } else {
                                    Log.e(TAG, "des: " + des);
                                    startLandingActivity();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            showError();
                            startLandingActivity();
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                        startLandingActivity();
                    }

                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    showError();
                    startLandingActivity();
                }

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void showError() {
        try {
            Toast.makeText(this, "", Toast.LENGTH_LONG).show();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


}
