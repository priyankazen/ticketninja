package in.ticketninja.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.EventTermsAdapter;
import in.ticketninja.adapters.EventsAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.MyEvent;
import in.ticketninja.objects.PromotionCampaignDetailsData;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestPromotionCampaignDetails;
import in.ticketninja.ws.response.CallbackPromotionCampaignDetails;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CouponCodesActivity extends AppCompatActivity {
    //private static final String TAG = CouponCodesActivity.class.getSimpleName();
    private SRKLoaderDialog mLoader;
    private CommonClass CC;
    private RecyclerView recyclerView, rvEventTerms;
    private String cId;
    private int mId;
    private TextView txtErrorMsg;
    private TextView txtCode, txtIssuedBy, txtValidUp, txtStatus;
    private NestedScrollView nsCouponCode;
    private  LinearLayout llEventTerms;
    private FrameLayout llTermsHeader;
    private ImageView ivEventTermsArrow;
    private boolean isTermsExpand = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_code_details);
        try {
            initializeData();
            getIntentData();
            findViewById();
            setOnClickEvent();
            setUpToolbar();
            if (CC != null && CC.isOnline()) {
                wsUserTicket();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        try {
            if (getIntent() != null) {
                if (getIntent().hasExtra(Constant.ScreenExtras.C_ID)) {
                    cId = getIntent().getStringExtra(Constant.ScreenExtras.C_ID);
                }
                if (getIntent().hasExtra(Constant.ScreenExtras.M_ID)) {
                    mId = getIntent().getIntExtra(Constant.ScreenExtras.M_ID, 0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void initializeData() {
        try {
            CC = new CommonClass(this);
            mLoader = new SRKLoaderDialog(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findViewById() {
        try {
            recyclerView = findViewById(R.id.rv);
            rvEventTerms = findViewById(R.id.rvEventTerms);
            txtCode = findViewById(R.id.txtCode);
            txtIssuedBy = findViewById(R.id.txtIssuedBy);
            txtValidUp = findViewById(R.id.txtValidUp);
            txtStatus = findViewById(R.id.txtStatus);
            txtErrorMsg = findViewById(R.id.txtErrorMsg);
            nsCouponCode = findViewById(R.id.nsCouponCode);
            llEventTerms = findViewById(R.id.llEventTerms);
            llTermsHeader = findViewById(R.id.llTermsHeader);
            ivEventTermsArrow = findViewById(R.id.ivEventTermsArrow);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpToolbar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);
                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getResources().getString(R.string.coupon_details));
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
                // findViewById(R.id.ivActionLogo).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOnClickEvent(){
        try {
            llTermsHeader.setOnClickListener(v -> {
                if (isTermsExpand) {
                    expandCollapseTerms(false);
                } else {
                    expandCollapseTerms(true);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void setAdapter(List<MyEvent> resultList) {
        try {
            EventsAdapter eventsAdapter = new EventsAdapter(this, resultList, Constant.WALLET);
            recyclerView.setAdapter(eventsAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setData(PromotionCampaignDetailsData data) {
        try {
            txtCode.setText(data.getCoupon_code());
            txtIssuedBy.setText(data.getPartner_name());
            txtStatus.setText(data.getStatus());
            txtValidUp.setText(data.getExpiry_date());

            List<MyEvent> resultList = data.getCampaign_event_list();
            setAdapter(resultList);

            if (data.getCampaign_tandc_list() != null && data.getCampaign_tandc_list().size() > 0) {
                llEventTerms.setVisibility(View.VISIBLE);
                rvEventTerms.setAdapter(new EventTermsAdapter(data.getCampaign_tandc_list()));
                expandCollapseTerms(false);
            }else {
                llEventTerms.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void expandCollapseTerms(boolean expand) {
        try {
            if (expand || llEventTerms.getVisibility() != View.VISIBLE) {
                rvEventTerms.setVisibility(View.VISIBLE);
                ivEventTermsArrow.animate().scaleY(-1f).start();
                ivEventTermsArrow.setRotation(1800);
                isTermsExpand = true;
                nsCouponCode.post(() -> nsCouponCode.fullScroll(ScrollView.FOCUS_DOWN));
            } else {
                rvEventTerms.setVisibility(View.GONE);
                ivEventTermsArrow.animate().scaleY(1f).start();
                ivEventTermsArrow.setRotation(0);
                isTermsExpand = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void wsUserTicket() {
        try {
            if (!mLoader.isShowing() && !Objects.requireNonNull(this).isFinishing())
                mLoader.show();

            RequestPromotionCampaignDetails request = new RequestPromotionCampaignDetails();
            request.c_id = cId;
            request.m_id = mId;

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackPromotionCampaignDetails> call = tnAPI.getPromotionCampaignDetails(request);
            call.enqueue(new Callback<CallbackPromotionCampaignDetails>() {
                @Override
                public void onResponse(@NonNull Call<CallbackPromotionCampaignDetails> call, @NonNull Response<CallbackPromotionCampaignDetails> response) {
                    try {
                        if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                showError(false,"");
                                PromotionCampaignDetailsData data = response.body().getData();
                                setData(data);
                            } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                                showError(true,Objects.requireNonNull(response.body()).getError_description());
                                //CC.showToast(Objects.requireNonNull(response.body()).getError_description());
                            } else {
                                showError(true,getResources().getString(R.string.msg_something_wrong));
                                //CC.showToast(R.string.msg_something_wrong);
                            }
                        } else {
                            showError(true,getResources().getString(R.string.msg_something_wrong));
                            //CC.showToast(R.string.msg_something_wrong);
                        }
                    } catch (Exception e) {
                        // e.printStackTrace();
                        if (!isFinishing()) {
                            showError(true,getResources().getString(R.string.msg_something_wrong));
                           // CC.showToast(R.string.msg_something_wrong);
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackPromotionCampaignDetails> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showError(boolean b,String errorMsg) {
        try {
            if (b){
                nsCouponCode.setVisibility(View.GONE);
                txtErrorMsg.setVisibility(View.VISIBLE);
                txtErrorMsg.setText(errorMsg);
            }else {
                nsCouponCode.setVisibility(View.VISIBLE);
                txtErrorMsg.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
