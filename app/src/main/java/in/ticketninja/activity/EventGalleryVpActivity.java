package in.ticketninja.activity;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;

import in.ticketninja.R;
import in.ticketninja.adapters.EventGalleryPagerAdapter;
import in.ticketninja.common.Constant;
import in.ticketninja.common.Utility;
import in.ticketninja.objects.EventGalleryList;
import me.relex.circleindicator.CircleIndicator;

public class EventGalleryVpActivity extends AppCompatActivity {
   // private final String TAG = EventGalleryVpActivity.class.getSimpleName();
    private TextView txtCount;
    private ImageView ibClose;
    private ViewPager viewPager;
    private ImageButton leftNav, rightNav;
    private ArrayList<EventGalleryList> galleryList;
    private int position;
    private CircleIndicator indicator;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_event_gallery_vp);
            setUpToolbar();
            findViewById();
            setOnclickEvent();
            getIntentData();
            setViewPager();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findViewById() {
        try {
            viewPager = findViewById(R.id.viewpager);
            txtCount = findViewById(R.id.txtCount);
            ibClose = findViewById(R.id.ibClose);
            leftNav = findViewById(R.id.left_nav);
            rightNav = findViewById(R.id.right_nav);
            indicator = findViewById(R.id.indicator);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        try {
            if (getIntent() != null) {
                if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_GALLERY_DATA)) {
                    galleryList = (ArrayList<EventGalleryList>) getIntent().getSerializableExtra(Constant.ScreenExtras.EVENT_GALLERY_DATA);
                }
                if (getIntent().hasExtra(Constant.ScreenExtras.POSITION)) {
                    position = getIntent().getIntExtra(Constant.ScreenExtras.POSITION,0);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOnclickEvent() {
        try {
            // Images left navigation
            leftNav.setOnClickListener(v -> {
                int tab = viewPager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    viewPager.setCurrentItem(tab);
                } else if (tab == 0) {
                    viewPager.setCurrentItem(tab);
                }
            });

            // Images right navigatin
            rightNav.setOnClickListener(v -> {
                int tab = viewPager.getCurrentItem();
                tab++;
                viewPager.setCurrentItem(tab);
            });

            ibClose.setOnClickListener(view -> {
                Log.e("TAg","click");
                finish();
            });

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                }

                @Override
                public void onPageSelected(int position) {
                    Log.e("TAg","onPageSelected");
                    setCount();
                }

                @Override
                public void onPageScrollStateChanged(int state) {
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpToolbar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);
                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getResources().getString(R.string.gallery));
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
               // findViewById(R.id.ivActionLogo).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setViewPager() {
        try {
            EventGalleryPagerAdapter customPagerAdapter = new EventGalleryPagerAdapter(this, galleryList);
            viewPager.setAdapter(customPagerAdapter);
            viewPager.setCurrentItem(position);
            //viewPager.beginFakeDrag();
           // viewPager.setOnTouchListener((v, event) -> true);
            indicator.setViewPager(viewPager);
            setCount();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void setCount(){
        try {
            txtCount.setText(String.format("%s/%s", viewPager.getCurrentItem()+1, galleryList.size()));
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
