package in.ticketninja.activity;

import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.textfield.TextInputEditText;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestBrandAssociation;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BrandAssociateActivity extends AppCompatActivity {
    private final String TAG = BrandAssociateActivity.class.getSimpleName();
    private SRKLoaderDialog mLoader;
    private TextInputEditText edtContactPerson, edtMobileNo, edtEmail, edtCmpnyName;
    private CheckBox cbCBooking;
    private long eventId;
    private CommonClass commonClass;
    private Button btnReggi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.activity_brand_association);
            initializeData();

            findViewById();

            eventId = getIntent().getLongExtra(Constant.ScreenExtras.EVENT_ID, 0);

            setupActionbar();

            btnReggi.setOnClickListener(view -> wsBrandAssociation());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initializeData() {
        try {
            mLoader = new SRKLoaderDialog(this);
            commonClass = new CommonClass(this);
            //getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionbar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(R.string.brand_association);
                Utility.convertToLowerCase(tvTitle);

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findViewById() {
        try {
            edtContactPerson = findViewById(R.id.edtContactPerson);
            edtMobileNo = findViewById(R.id.edtMno);
            edtEmail = findViewById(R.id.edtEmailId);
            edtCmpnyName = findViewById(R.id.edtCmpnyName);
            cbCBooking = findViewById(R.id.cbBrand);
            btnReggi = findViewById(R.id.btnBrandReggi);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        finish();
        return super.onTouchEvent(event);
    }

    // WS - Get Brand Association
    //Insert_Sponsor_Inquiry(string event_id, string C_Name, string event_name, string mobileno, string email
    private void wsBrandAssociation() {
        try {
            String name = Objects.requireNonNull(edtContactPerson.getText()).toString();
            String mobile_no = Objects.requireNonNull(edtMobileNo.getText()).toString();
            String email = Objects.requireNonNull(edtEmail.getText()).toString();
            String ticket = Objects.requireNonNull(edtCmpnyName.getText()).toString();


            if (TextUtils.isEmpty(ticket)) {
                showError(getString(R.string.please_enter_company_name));
                return;
            } else if (TextUtils.isEmpty(name)) {
                showError(getString(R.string.please_enter_contact_person));
                return;
            } else if (TextUtils.isEmpty(mobile_no)) {
                showError(getString(R.string.please_enter_mobile_no));
                return;
            } else if (!TextUtils.isEmpty(mobile_no) && mobile_no.length() != 10) {
                showError(getString(R.string.common_msg_enter_mobile_valid));
                return;
            } else if (TextUtils.isEmpty(email)) {
                showError(getString(R.string.please_enter_email));
                return;
            } else if (!TextUtils.isEmpty(email) && !Validate.checkEmail(email)) {
                showError(getString(R.string.common_msg_enter_email_valid));
                return;
            } else if ((!cbCBooking.isChecked())) {
                showError(getString(R.string.please_select_terms_condition));
                return;
            }

            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestBrandAssociation request = new RequestBrandAssociation();
            request.C_Name = name;
            request.email = email;
            request.event_id = String.valueOf(eventId);
            request.mobileno = mobile_no;


            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<ResponseBody> call = tnAPI.getBrandAssociate(request);
            call.enqueue(new Callback<ResponseBody>() {
                @SuppressWarnings("ConstantConditions")
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();

                    try {
                        String myData = response.body().string();
                        Log.e(TAG, "data =" + myData);
                        if (response.isSuccessful()) {
                            JSONObject jo;
                            try {
                                jo = new JSONObject(myData);
                                // JSONObject jo1 = jo.getJSONObject("Insert_Sponsor_Inquiry");
                                String errorMessage = jo.getString("error_description");
                                String errorCode = jo.getString("error_code");
                                if (errorCode.equals("1")) {
                                    commonClass.showAlert(errorMessage, () -> onBackPressed());
                                } else {
                                    commonClass.showAlert(errorMessage, (MessageUtils.OnOkClickListener) null);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            showError("");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call,@NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    showError("");
                }

            });
        }catch (Exception e){
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    private void showError(String msg) {
        try {
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
