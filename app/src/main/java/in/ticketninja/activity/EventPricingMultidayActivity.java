/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.EventPricingAdapter1;
import in.ticketninja.adapters.EventPricingDateChooserAdapter;
import in.ticketninja.adapters.EventPricingTimeChooserAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.EventPriceCategory;
import in.ticketninja.objects.MultipleDay;
import in.ticketninja.objects.MyEventDetail;
import in.ticketninja.objects.MyEventTimeList;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestInsertTicketSession;
import in.ticketninja.ws.response.CallbackInsertTicketSession;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventPricingMultidayActivity extends AppCompatActivity {

    private final String TAG = EventPricingMultidayActivity.class.getSimpleName();

    private CommonClass CC;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;

    private RecyclerView rvDateSelector;
    private RecyclerView rvTimeSelector;
    private RecyclerView recyclerView;
    private LinearLayout llErrorLayout;
    private View llMainLayout;
    private Button btnContinue;
    private ImageView ivErrorImage;
    private TextView tvErrorMessage, tvErrorButton;

    private MyEventDetail myEvent;

    private TextView tvTitle;
    private TextView tvDateTime;
    private TextView tvShowTitle;
    private LinearLayout llEventVenue;
    private TextView tvVenueName;
    private TextView tvVenueAddress;
    private LinearLayout llSeasonPass, llTimeView, llContinue;
    private TextView tvBtnSingleDayPass, tvBtnSeasonPass, tvTotalPrice;

    private EventPriceCategory selectedCategory;
    private boolean isSeasonPass = false;
    private int selectedDatePos = 0;
    private int selectedTimePos = 0;
    private MultipleDay seasonPassDay = new MultipleDay();
    private ArrayList<MultipleDay> displayDayList = new ArrayList<>();
    private List<EventPriceCategory> pricingCategoryList = new ArrayList<>();
    private EventPricingDateChooserAdapter mDateAdapter;
    private EventPricingAdapter1 mEventPricingAdapter;
    private TextView tvDiscount;
    private RelativeLayout rvDiscount;
    private PreferencesUtils mPref;
    private int totalTicketCount = 0;
    private long showId = 0;
    private String showTime = "";
    private String showDate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_pricing_multiple_day);

        try {

            initializeData();

            getIntentData();

            setupActionbar();

            findViewById();

            tvBtnSingleDayPass.setOnClickListener(view -> {
                if (isSeasonPass) {
                    isSeasonPass = false;
                    changePassType();
                }

            });
            tvBtnSeasonPass.setOnClickListener(view -> {
                if (!isSeasonPass) {
                    isSeasonPass = true;
                    changePassType();
                }
            });


            if (CC.isOnline()) {
                if (myEvent != null) {
                    invalidateData();
                } else {
                    tvErrorMessage.setText(R.string.msg_something_wrong);
                    showError();
                }
            } else {
                showNoInternet();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void findViewById() {
        try {
            rvDateSelector = findViewById(R.id.rvDateSelector);
            rvDateSelector.setHasFixedSize(true);
            rvDateSelector.setNestedScrollingEnabled(false);

            rvTimeSelector = findViewById(R.id.rvTimeSelector);
            rvTimeSelector.setHasFixedSize(true);
            rvTimeSelector.setNestedScrollingEnabled(false);

            recyclerView = findViewById(R.id.recyclerView);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);

            tvTitle = findViewById(R.id.tvEventTitle);
            tvDiscount = findViewById(R.id.tv_discount);
            rvDiscount = findViewById(R.id.rv_discount);
            tvDateTime = findViewById(R.id.tvEventDateTime);
            btnContinue = findViewById(R.id.btnContinue);
            llContinue = findViewById(R.id.llContinue);
            tvTotalPrice = findViewById(R.id.tvTotalPrice);
            llEventVenue = findViewById(R.id.llEventVenue);
            tvVenueName = findViewById(R.id.tvEventVenue);
            tvVenueAddress = findViewById(R.id.tvEventAddress);
            tvShowTitle = findViewById(R.id.tv_show_title);

            llTimeView = findViewById(R.id.llTimeView);
            llSeasonPass = findViewById(R.id.llSeasonPass);
            tvBtnSingleDayPass = findViewById(R.id.tvBtnSingleDayPass);
            tvBtnSeasonPass = findViewById(R.id.tvBtnSeasonPass);
            llMainLayout = findViewById(R.id.nestedScrollView);
            llErrorLayout = findViewById(R.id.llErrorLayout);
            ivErrorImage = findViewById(R.id.ivErrorImage);
            tvErrorMessage = findViewById(R.id.tvErrorMessage);
            tvErrorButton = findViewById(R.id.tvErrorButton);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionbar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        try {
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_DATA)) {
                //EventDetail{category='Food Fest ', event_date='2021-12-01', is_registration='false',
                // category_list=[EventPriceCategory{c_desc='', category_id=1047,
                // category_name='Ticket to Happiness', seat_no='', seat_type='Entrance', identification=0}, EventPriceCategory{c_desc='', category_id=1047, category_name='Ticket to Happiness', seat_no='', seat_type='Entrance', identification=0}, EventPriceCategory{c_desc='', category_id=1047, category_name='Ticket to Happiness', seat_no='', seat_type='Entrance', identification=0}]}
                myEvent = (MyEventDetail) getIntent().getSerializableExtra(Constant.ScreenExtras.EVENT_DATA);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            mPref = new PreferencesUtils(this);
            CC = new CommonClass(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == RESULT_OK && requestCode == Constant.REQUEST_CODE_CONFIRM_CONTACT_DETAIL) {
//            if (data != null && data.hasExtra(Constant.ScreenExtras.EVENT_SESSION_ID)) {
//                mPref = new PreferencesUtils(this);
//                if (Validate.isNotNull(mPref.getUserEmailId()) && Validate.isNotNull(mPref.getUserPhoneNumber()) &&
//                        Validate.isNotNull(mPref.getCountryCode())) {
//                    OrderSummaryActivity.start(EventPricingMultidayActivity.this, data.getStringExtra(Constant.ScreenExtras.EVENT_SESSION_ID));
//                }
//            }
//        }
//    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        try {
            if (myEvent != null) {
                for (int i = 0; i < myEvent.getPriceCategory().size(); i++) {
                    myEvent.getPriceCategory().get(i).noOfTicket(0);
                }
            }
            mEventPricingAdapter.notifyDataSetChanged();
            clearData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            Log.e(TAG, "onStop");
            if (myEvent != null) {
                mEventPricingAdapter.updateCount(myEvent.getMax_Ticket_No());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void changePassType() {
        try {
            if (isSeasonPass) {
                tvBtnSingleDayPass.setSelected(false);
                tvBtnSeasonPass.setSelected(true);
                if (mDateAdapter != null && myEvent != null) {
                    mDateAdapter.setSeasonPass(isSeasonPass && myEvent.isSeasonPassAllowed(), 0);
                    mDateAdapter.notifyDataSetChanged();
                }
                if (seasonPassDay != null && Validate.isNotNull(seasonPassDay.getEvent_date()))
                    setupTime(seasonPassDay);
            } else {
                tvBtnSingleDayPass.setSelected(true);
                tvBtnSeasonPass.setSelected(false);
                if (mDateAdapter != null && myEvent != null) {
                    mDateAdapter.setSeasonPass(isSeasonPass && myEvent.isSeasonPassAllowed(), selectedDatePos);
                    mDateAdapter.notifyDataSetChanged();
                }
                if (displayDayList.size() > 0) {
                    setupTime(displayDayList.get(selectedDatePos));
                    //filterAndDisplayCategory(multipleDayList.get(selectedDatePos).eventDate(), "");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setupTime(MultipleDay day) {
        try {
            selectedTimePos = 0;
            //ArrayList<MyEventTimeList> timeLists = day.timeList();
            ArrayList<MyEventTimeList> timeLists = new ArrayList<>();

            //add by priyanka
            if (myEvent != null) {
                for (MultipleDay multipleDay : myEvent.getMultipleDayList()) {
                    MyEventTimeList timeList = new MyEventTimeList();
                    if (day.getEvent_date().equals(multipleDay.getEvent_date())) {
                        timeList.setDate(multipleDay.getEvent_date());
                        timeList.setTime(multipleDay.getTime());
                        timeList.setName(multipleDay.getShow_name());
                        timeList.setS_detail(multipleDay.getS_detail());
                        timeList.setF_detail(multipleDay.getF_detail());
                        timeLists.add(timeList);
                    }
                }
            }

            if (timeLists.size() > 0) {
                showTime = timeLists.get(0).getTime();
                EventPricingTimeChooserAdapter mTimeAdapter = new EventPricingTimeChooserAdapter(timeLists);
                mTimeAdapter.setOnTimeSelectedListener((position, time) -> {
                    showTime = time.getTime();
                    selectedTimePos = position;
                    filterAndDisplayCategory(day.getEvent_date(), time.getTime(), time.title());
                });
                rvTimeSelector.setAdapter(mTimeAdapter);
                llTimeView.setVisibility(View.VISIBLE);

                filterAndDisplayCategory(day.getEvent_date(), timeLists.get(selectedTimePos).getTime(), timeLists.get(selectedTimePos).title());

            } else {
                llTimeView.setVisibility(View.GONE);

                if (displayDayList.size() > 0)
                    filterAndDisplayCategory(displayDayList.get(selectedDatePos).getEvent_date(), "", "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void filterAndDisplayCategory(String date, String time, String title) {

        try {
            //Log.e(TAG, "event_date=" + event_date + "  time=" + time + "  title=" + title);
            Log.e(TAG, "getDayType =" + myEvent.getDayType());

            //add by 9/jul/2019
            if ("Multipledayevent".equals(myEvent.getDayType())) {
                tvShowTitle.setText(Validate.isNotNull(title) ? title : "");
                tvShowTitle.setVisibility(Validate.isNotNull(title) ? View.VISIBLE : View.GONE);
            } else {
                tvShowTitle.setVisibility(View.GONE);
            }


            List<EventPriceCategory> pricingList = new ArrayList<>();

            for (EventPriceCategory eventPricing : pricingCategoryList) {

                eventPricing.noOfTicket(0);// add by priyanka 10 april 2019

                if (isSeasonPass && Constant.Code.ONE == eventPricing.isSeasonPass()
                        && eventPricing.time().equalsIgnoreCase(time)) {
                    pricingList.add(eventPricing);
                } else if (!isSeasonPass
                        && eventPricing.eventDate().equalsIgnoreCase(date)
                        && eventPricing.time().equalsIgnoreCase(time)) {
                    pricingList.add(eventPricing);
                }
            }

        /*
        if (mEventPricingAdapter == null) {
            mEventPricingAdapter = new EventPricingAdapter(pricingList);
            mEventPricingAdapter.setOnItemClickListener((position, count, priceCategory) -> {
                if (position >= 0 && count > 0 && priceCategory != null) {
                    selectedCategory = priceCategory;
                    selectedCategory.noOfTicket(count);
                    btnContinue.setEnabled(true);
                    //Log.e(TAG, "selectedCategory==  event_date=" + selectedCategory.eventDate() + "  time=" + selectedCategory.time());

                } else {
                    selectedCategory = null;
                    btnContinue.setEnabled(false);
                }
            });
            recyclerView.setAdapter(mEventPricingAdapter);
        } else {
            selectedCategory = null;
            btnContinue.setEnabled(false);
            mEventPricingAdapter.setDataList(pricingList);
            mEventPricingAdapter.notifyDataSetChanged();
        }*/


            if (mEventPricingAdapter == null) {
                mEventPricingAdapter = new EventPricingAdapter1(this, pricingList, myEvent.getMulti_catergory_select(), myEvent.getMax_Ticket_No());
                mEventPricingAdapter.setOnItemClickListener((position, count, priceCategory) -> {
                    try {
                        Log.e(TAG, "count: " + count);
                        totalTicketCount = count;
                        if (position >= 0 && count > 0 && priceCategory != null) {
                            selectedCategory = priceCategory;
                            //selectedCategory.noOfTicket(count);
                            btnContinue.setEnabled(true);
                            llContinue.setEnabled(true);
                            btnContinue.setGravity(Gravity.END | Gravity.CENTER);
                            tvTotalPrice.setVisibility(View.VISIBLE);
                            //tvTotalPrice.setText(getString(R.string.total).concat("    ₹ ").concat("" + selectedCategory.getTotalRate()));

                            @SuppressLint("DefaultLocale") String s1 = "₹ ".concat(String.format("%.2f", selectedCategory.getTotalRate()));
                            SpannableString ss1 = new SpannableString(s1);
                            ss1.setSpan(new RelativeSizeSpan(1f), 0, s1.length(), 0);

                            String s2 = count + " Tickets";// / set size
                            SpannableString ss2 = new SpannableString(s2);
                            ss2.setSpan(new RelativeSizeSpan(0.7f), 0, s2.length(), 0);

                            CharSequence finalText = TextUtils.concat(ss1, "\n", ss2);// / set size
                            tvTotalPrice.setText(finalText);
                        } else {
                            selectedCategory = null;
                            btnContinue.setEnabled(false);
                            tvTotalPrice.setText("");
                            tvTotalPrice.setVisibility(View.GONE);
                            llContinue.setEnabled(false);
                            btnContinue.setGravity(Gravity.CENTER);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                });
                recyclerView.setAdapter(mEventPricingAdapter);
            } else {
                selectedCategory = null;
                //btnContinue.setEnabled(false);
                clearData();
                mEventPricingAdapter.setDataList(pricingList);
                mEventPricingAdapter.notifyDataSetChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void clearData() {
        try {
            if (Constant.SVG_BLOCK.equals(Objects.requireNonNull(myEvent).getLayout_type())){
                recyclerView.setVisibility(View.GONE);
                btnContinue.setEnabled(true);
                llContinue.setEnabled(true);
            }else {
                btnContinue.setEnabled(false);
                tvTotalPrice.setText("");
                tvTotalPrice.setVisibility(View.GONE);
                llContinue.setEnabled(false);
                btnContinue.setGravity(Gravity.CENTER);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void invalidateData() {

        try {
            tvTitle.setText(Validate.isNotNull(myEvent.getEvent_name()) ? String.format("%s", myEvent.getEvent_name()) : "");

            if (myEvent.getDayType().equalsIgnoreCase(Constant.EventDayType.SINGLE_DAY)) {
                tvDateTime.setText(String.format("%s, %s (%s %s)", myEvent.getDay(), myEvent.getMDDate(), myEvent.getEvent_time(), Constant.ONWARDS));

            } else if (myEvent.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY)
                    || myEvent.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW)) {
               // tvDateTime.setText(String.format("%s, %s (%s %s)", myEvent.getMDDate(), myEvent.getYear(), myEvent.getEvent_time(), Constant.ONWARDS));

                //change 13/11/2019
                tvDateTime.setText(String.format("%s (%s %s)", myEvent.getMDDate1(""), myEvent.getEvent_time(), Constant.ONWARDS));

            } else if (myEvent.getDayType().equalsIgnoreCase(Constant.EventDayType.PERPETUAL)) {
                tvDateTime.setText(String.format("%s, %s (%s)", myEvent.getDay(), myEvent.getMDDate(), myEvent.getEvent_time()));

            } else {
                tvDateTime.setText("");
            }

            tvVenueName.setText(Validate.isNotNull(myEvent.getVenueName()) ? String.format("%s", myEvent.getVenueName()) : "");
            tvVenueAddress.setText(Validate.isNotNull(myEvent.getVenueFullAddress()) ? String.format("%s", myEvent.getVenueFullAddress()) : "");


            try {
                if (Validate.isNotNull(myEvent.getDiscountLabel())) {
                    rvDiscount.setVisibility(View.VISIBLE);
                    tvDiscount.setText(String.format(Locale.getDefault(), "%s %.2f%% off", myEvent.getDiscountLabel(), myEvent.getDiscountPer()));
                } else {
                    rvDiscount.setVisibility(View.GONE);
                }
            }catch (Exception e){
                e.printStackTrace();
            }


            if (Validate.isNotNull(myEvent.getLatitude()) && Validate.isNotNull(myEvent.getLongitude())) {
                llEventVenue.setOnClickListener(v -> CommonClass.openMap(this, myEvent.getVenueName(), myEvent.getLatitude(), myEvent.getLongitude()));
            }

            llSeasonPass.setVisibility(myEvent.isSeasonPassAllowed() && myEvent.isSingleDayPassAllowed() ? View.VISIBLE : View.GONE);

            ArrayList<String> dateArray = new ArrayList<>();

            if (myEvent.getMultipleDayList() != null && myEvent.getMultipleDayList().size() > 0) {
                List<MultipleDay> multipleDayList = myEvent.getMultipleDayList();
                for (MultipleDay day : multipleDayList) {
                    boolean b = day.isSeasonPass() == 1;
                    if (b) {
                        seasonPassDay = day;
                    } else {
                        try {
                            Log.e("EventPricingMultiAct", "displayDayList123: " + displayDayList);
                        /*if (displayDayList.size() > 0) {
                            for (MultipleDay day1 : displayDayList) {
                                String value = day1.getEvent_date();
                                if (!day.getEvent_date().equals(value) && !day.getEvent_date().equals("0001-01-01")) {
                                    displayDayList.add(day);
                                }
                            }
                        } else {
                            Log.e("EventPricingMultiAct", "displayDayList: " + displayDayList);
                            if (!day.getEvent_date().equals("0001-01-01")) {
                                displayDayList.add(day);
                            }
                        }*/

                            if (!dateArray.contains(day.getEvent_date()) && !day.getEvent_date().equals("0001-01-01")) {
                                displayDayList.add(day);
                                dateArray.add(day.getEvent_date());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }
                mDateAdapter = new EventPricingDateChooserAdapter(displayDayList, isSeasonPass && myEvent.isSeasonPassAllowed());
                showId = displayDayList.get(selectedDatePos).id();
                showDate = displayDayList.get(selectedDatePos).getEvent_date();
                mDateAdapter.setOnDateSelectedListener((position, day) -> {
                    selectedDatePos = position;
                    //filterAndDisplayCategory(day.eventDate());
                    showId = day.id();
                    Log.e(TAG,"showId: "+showId);
                    showDate = day.getEvent_date();
                    Log.e(TAG,"showDate: "+showDate);
                    setupTime(day);
                });
                rvDateSelector.setAdapter(mDateAdapter);
                rvDateSelector.setVisibility(View.VISIBLE);
            } else {
                rvDateSelector.setVisibility(View.GONE);
            }

        /*if (myEvent.getMultipleDayList() != null && myEvent.getMultipleDayList().size() > 0) {
            List<MultipleDay> multipleDayList = myEvent.getMultipleDayList();
            for (MultipleDay day : multipleDayList) {
                if (day.isSeasonPass()) {
                    seasonPassDay = day;
                } else {
                    displayDayList.add(day);
                }
            }
            mDateAdapter = new EventPricingDateChooserAdapter(displayDayList, isSeasonPass && myEvent.isSeasonPassAllowed());
            mDateAdapter.setOnDateSelectedListener((position, day) -> {
                selectedDatePos = position;
                //filterAndDisplayCategory(day.eventDate());
                setupTime(day);
            });
            rvDateSelector.setAdapter(mDateAdapter);
            rvDateSelector.setVisibility(View.VISIBLE);
        } else {
            rvDateSelector.setVisibility(View.GONE);
        }*/


            if (myEvent.getPriceCategory() != null && myEvent.getPriceCategory().size() > 0) {
                pricingCategoryList = myEvent.getPriceCategory();
                btnContinue.setEnabled(false);
                llContinue.setEnabled(false);
                recyclerView.setVisibility(View.VISIBLE);
            } else {
                recyclerView.setVisibility(View.GONE);
            }

            btnContinue.setOnClickListener(v -> {

                try {
                    Utility.hideKeyboard(this);
                    if (CC.isOnline()) {
                        if (Constant.SVG_BLOCK.equals(myEvent.getLayout_type())){
                            Intent intent = new Intent(getApplicationContext(), SeatSelectionActivity.class);
                            intent.putExtra(Constant.ScreenExtras.EVENT_ID, myEvent.getId());
                            intent.putExtra(Constant.ScreenExtras.SHOW_ID,showId);
                            intent.putExtra(Constant.SVG_BLOCK, myEvent.getLayout_type());
                            intent.putExtra(Constant.ScreenExtras.EVENT_NAME, myEvent.getEvent_name());
                            intent.putExtra(Constant.ScreenExtras.EVENT_TIME, showTime);
                            intent.putExtra(Constant.ScreenExtras.EVENT_DATE, showDate);
                            intent.putExtra(Constant.ScreenExtras.EVENT_ADDRESS, myEvent.getVenueAddress());
                            intent.putExtra(Constant.ScreenExtras.EVENT_CITY, myEvent.getVenueCity());
                            intent.putExtra(Constant.ScreenExtras.EVENT_ADDRESS, myEvent.getVenueName());
                            startActivity(intent);
                        }else {
                            if (selectedCategory != null) {
                                if (Constant.Code.ONE == selectedCategory.isIdentification()) {
                                    Intent intent = new Intent(getApplicationContext(), ExpandableActivity.class);
                                    intent.putExtra(Constant.ScreenExtras.EVENT_ID, myEvent.getId());
                                    intent.putExtra(Constant.ScreenExtras.PLAN_CATEGORY_ID, selectedCategory.getCategory_id());
                                    // intent.putExtra(Constant.ScreenExtras.SEAT_COUNT, selectedCategory.noOfTicket());
                                    intent.putExtra(Constant.ScreenExtras.SEAT_COUNT, totalTicketCount);
                                    intent.putExtra(Constant.ScreenExtras.EVENT_DATE, selectedCategory.eventDate());
                                    intent.putExtra(Constant.ScreenExtras.EVENT_TIME, selectedCategory.time());
                                    intent.putExtra(Constant.ScreenExtras.EVENT_DATA, selectedCategory);
                                    intent.putExtra(Constant.ScreenExtras.EVENT_DATA1, mEventPricingAdapter.getSelectedList());
                                    intent.putExtra(Constant.ScreenExtras.TYPE, selectedCategory.seatType());
                                    intent.putExtra(Constant.ScreenExtras.EVENT_NAME, myEvent.getEvent_name());
                                    intent.putExtra(Constant.ScreenExtras.EVENT_CITY, myEvent.getVenueCity());
                                    intent.putExtra(Constant.ScreenExtras.EVENT_ADDRESS, myEvent.getVenueName());
                                    startActivity(intent);
                                } else if (Validate.isNotNull(selectedCategory.seatType()) && selectedCategory.seatType().equalsIgnoreCase(Constant.SEAT_TYPE_ARRANGE_SEATING)) {
                                    Intent intent = new Intent(getApplicationContext(), SeatSelectionActivity.class);
                                    intent.putExtra(Constant.ScreenExtras.EVENT_ID, myEvent.getId());
                                    intent.putExtra(Constant.ScreenExtras.SHOW_ID, selectedCategory.showId());
                                    intent.putExtra(Constant.ScreenExtras.PLAN_CATEGORY_ID, selectedCategory.id());
                                    //intent.putExtra(Constant.ScreenExtras.SEAT_COUNT, selectedCategory.noOfTicket());
                                    intent.putExtra(Constant.ScreenExtras.SEAT_COUNT, totalTicketCount);
                                    intent.putExtra(Constant.ScreenExtras.EVENT_DATE, selectedCategory.eventDate());
                                    intent.putExtra(Constant.ScreenExtras.EVENT_TIME, selectedCategory.time());
                                    intent.putExtra(Constant.ScreenExtras.EVENT_NAME, myEvent.getEvent_name());
                                    intent.putExtra(Constant.ScreenExtras.EVENT_CITY, myEvent.getVenueCity());
                                    intent.putExtra(Constant.ScreenExtras.EVENT_ADDRESS, myEvent.getVenueName());
                                    startActivity(intent);
                                    //finish();
                                } else {
                                    //wsInsertTicketSession(myEvent.getId(), selectedCategory.eventDate(), selectedCategory.time(), mLogin.getUserId(), selectedCategory);
                                    wsInsertTicketSession(myEvent.getId(), selectedCategory.eventDate(), selectedCategory.time(), mLogin.getUserId(), mEventPricingAdapter.getSelectedList());
                                }
                            }
                        }
                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });

            //Log.e(TAG, "myEvent.isSeasonPassAllowed()=>" + myEvent.isSeasonPassAllowed());
            //Log.e(TAG, "myEvent.isSingleDayPassAllowed()=>" + myEvent.isSingleDayPassAllowed());

            isSeasonPass = myEvent.isSeasonPassAllowed() && !myEvent.isSingleDayPassAllowed();

            changePassType();

            if (Constant.SVG_BLOCK.equals(Objects.requireNonNull(myEvent).getLayout_type())){
                recyclerView.setVisibility(View.GONE);
                btnContinue.setEnabled(true);
                llContinue.setEnabled(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showNoInternet() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_connection_lost)
                    .placeholder(R.drawable.ic_connection_lost_static)
                    .into(ivErrorImage);
            tvErrorMessage.setText(R.string.msg_no_internet);
            llMainLayout.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.VISIBLE);
            tvErrorButton.setText(R.string.btn_try_again);
            tvErrorButton.setOnClickListener(v -> {

            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showError() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_no_result_found)
                    .placeholder(R.drawable.ic_no_result_found)
                    .into(ivErrorImage);
            llMainLayout.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        try {
            Utility.hideKeyboard(this);
            super.onBackPressed();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // WS - Insert Ticket Session
    // private void wsInsertTicketSession(long eventId, String eventDate, String timeSlot, String userId, EventPriceCategory categoryList) {
    private void wsInsertTicketSession(long eventId, String eventDate, String timeSlot, String userId, List<EventPriceCategory> categoryList) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestInsertTicketSession request = new RequestInsertTicketSession(eventId, eventDate, timeSlot, String.valueOf(userId), categoryList, "");

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackInsertTicketSession> call = tnAPI.insertTicketSession(request);
            call.enqueue(new Callback<CallbackInsertTicketSession>() {
                @Override
                public void onResponse(@NonNull Call<CallbackInsertTicketSession> call, @NonNull Response<CallbackInsertTicketSession> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                String sessionID = response.body().data().sessionID();

                                if (Validate.isNull(mPref.getUserEmailId()) || Validate.isNull(mPref.getUserPhoneNumber()) ||
                                        Validate.isNull(mPref.getCountryCode())) {

                                    // TODO: 29/3/18 SHREYA 1 : Contact Detail Flow
//                                startActivity(new Intent(EventPricingMultidayActivity.this, ContactConfirmationActivity.class));

                                    ContactConfirmationActivity.Companion.start(EventPricingMultidayActivity.this, sessionID, Constant.ContactActions.EDIT, "", "");

                                } else {
                                    OrderSummaryActivity.start(EventPricingMultidayActivity.this, sessionID, "", "");
                                }
                            } else if (Validate.isNotNull(response.body().data().message())) {
                                CC.showAlert(response.body().data().message());
                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        } else {
                            CC.showToast(R.string.msg_no_response);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CC.showToast(R.string.msg_something_wrong);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackInsertTicketSession> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
        }
    }

}