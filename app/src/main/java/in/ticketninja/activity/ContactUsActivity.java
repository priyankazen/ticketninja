/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.SpinnerTypeOfEnquiry;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.CustomTypefaceSpan;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.TextViewUtils;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestContactUs;
import in.ticketninja.ws.response.CallbackContactUs;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsActivity extends AppCompatActivity {

   // private final String TAG = ContactUsActivity.class.getSimpleName();
    private CommonClass CC;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;
    private PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

    private Spinner sp_select_enquery;
    private List<String> enquerytype = new ArrayList<>();
    private EditText etName, etEmail, etMobile, etMessage, etCountryCode;
    private String enquiry = "";
   private Button btnSubmit;
    private boolean mDoubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        try {

            initializeData();

            Typeface tfBold = ResourcesCompat.getFont(this, R.font.helvetica_neue_bold);

            setupActionBar();

            findViewById();

            TextView tv_email = findViewById(R.id.tv_email);
            TextView tv_time = findViewById(R.id.tv_time);
            TextView tv_hotline = findViewById(R.id.tv_hotline);
            TextView tv_customer_care = findViewById(R.id.tv_customer_care);
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);
            tv_hotline.setTypeface(tfMedium);
            tv_email.setTypeface(tfMedium);
            tv_time.setTypeface(tfMedium);
            tv_customer_care.setTypeface(tfMedium);


            if (mLogin.isLoggedIn()) {
                etName.setText(Validate.isNotNull(mLogin.getName()) ? mLogin.getName() : "");
                etEmail.setText(Validate.isNotNull(mLogin.getEmail()) ? mLogin.getEmail() : "");
                etMobile.setText(Validate.isNotNull(mLogin.getMobile()) ? mLogin.getMobile() : "");
                etCountryCode.setText(Validate.isNotNull(mLogin.getCountryCode()) ? mLogin.getCountryCode() : "+91");
            } else {
                Country country = Country.getCountryByISO(Constant.DEFAULT_COUNTRY_REGION);
                etCountryCode.setText(country.getDialCode());
            }

            etCountryCode.setOnClickListener(v -> {

                try {
                    CountryPicker picker = CountryPicker.newInstance("Select country");  // dialog title
                    picker.setListener((name, code, dialCode, flagDrawableResID) -> {
                        etCountryCode.setText(dialCode);

                        Utility.hideKeyboard(ContactUsActivity.this, picker.getView());

                        picker.dismiss();

                        //  tv_country.setCompoundDrawables(flagDrawableResID,null,null,null);
                    });

                    picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });



            preparedate();
            SpinnerTypeOfEnquiry spinnerTypeOfenquery = new SpinnerTypeOfEnquiry(ContactUsActivity.this, enquerytype);
            sp_select_enquery.setAdapter(spinnerTypeOfenquery);


            SpannableString styledString = new SpannableString(getString(R.string.str_opration_hour));
            styledString.setSpan(new CustomTypefaceSpan("", tfBold), 0, 16, 0);
            styledString.setSpan(new ForegroundColorSpan(Color.parseColor("#4A4A4A")), 0, 16, 0);
            styledString.setSpan(new TextViewUtils.MyClickableSpan() {
                @Override
                public void onClick(@NonNull View widget) {
                    Utility.hideKeyboard(ContactUsActivity.this);


                }
            }, 0, 16, 0);
            tv_time.setMovementMethod(LinkMovementMethod.getInstance());
            tv_time.setText(styledString);

            styledString = new SpannableString(getString(R.string.str_customer_care_hotline));
            styledString.setSpan(new CustomTypefaceSpan("", tfBold), 16, 29, 0);
            styledString.setSpan(new ForegroundColorSpan(Color.parseColor("#4A4A4A")), 16, 29, 0);
            styledString.setSpan(new TextViewUtils.MyClickableSpan() {
                @Override
                public void onClick(@NonNull View widget) {
                    Utility.hideKeyboard(ContactUsActivity.this);

                    Utility.callPhone(ContactUsActivity.this, "1800 241 7777");


                }
            }, 16, 29, 0);
            tv_customer_care.setMovementMethod(LinkMovementMethod.getInstance());
            tv_customer_care.setText(styledString);

            styledString = new SpannableString(getString(R.string.str_tr_hotline));
            styledString.setSpan(new CustomTypefaceSpan("", tfBold), 21, 34, 0);
            styledString.setSpan(new ForegroundColorSpan(Color.parseColor("#4A4A4A")), 21, 34, 0);
            styledString.setSpan(new TextViewUtils.MyClickableSpan() {
                @Override
                public void onClick(@NonNull View widget) {
                    Utility.hideKeyboard(ContactUsActivity.this);

                    Utility.callPhone(ContactUsActivity.this, "1800 241 8888");
                }
            }, 21, 34, 0);
            tv_hotline.setMovementMethod(LinkMovementMethod.getInstance());
            tv_hotline.setText(styledString);

            styledString = new SpannableString(getString(R.string.str_cc_email_id));
            styledString.setSpan(new CustomTypefaceSpan("", tfBold), 0, 17, 0);
            styledString.setSpan(new ForegroundColorSpan(Color.parseColor("#F53033")), 0, 17, 0);
            styledString.setSpan(new TextViewUtils.MyClickableSpan() {
                @Override
                public void onClick(@NonNull View widget) {
                    Utility.hideKeyboard(ContactUsActivity.this);


                    Intent i = new Intent(Intent.ACTION_SENDTO);
                    //i.setType("message/rfc822");
                    // i.putExtra(Intent.EXTRA_EMAIL, new String[]{"cs@ticketninja.in"});
                    i.setData(Uri.parse("mailto:cs@ticketninja.in"));

                    try {
                        startActivity(i);
                    } catch (android.content.ActivityNotFoundException ex) {
                        MessageUtils.showToast(ContactUsActivity.this, R.string.msg_no_email_client_found);
                    }

              /*  Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{"cs@ticketninja.in"});

                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    MessageUtils.showToast(ContactUsActivity.this, "There are no email clients installed.");
                }*/


                }
            }, 0, 17, 0);
            tv_email.setMovementMethod(LinkMovementMethod.getInstance());
            tv_email.setText(styledString);

            btnSubmit.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    if (validate()) {
                        wsContactUs();
                    }
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }


            });

            sp_select_enquery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    enquiry = enquerytype.get(position);

                    etEmail.setFocusable(true);
                    etEmail.setFocusableInTouchMode(true);
                    etEmail.requestFocus();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void findViewById() {
        try {
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);


            TextView tv_address_title = findViewById(R.id.tv_address_title);
            TextView tv_address = findViewById(R.id.tv_address);
            TextView tvCIN = findViewById(R.id.tvCIN);


            etName = findViewById(R.id.et_name);
            etEmail = findViewById(R.id.et_email);
            etMobile = findViewById(R.id.et_number);
            etMessage = findViewById(R.id.et_message);
            etCountryCode = findViewById(R.id.et_coumtrycode);
            btnSubmit = findViewById(R.id.btn_submit);
            sp_select_enquery = findViewById(R.id.sp_select_enquery);

            etName.setTypeface(tfMedium);
            etEmail.setTypeface(tfMedium);
            etMobile.setTypeface(tfMedium);
            etMessage.setTypeface(tfMedium);
            btnSubmit.setTypeface(tfMedium);

            tv_address_title.setTypeface(tfMedium);
            tv_address.setTypeface(tfMedium);
            tvCIN.setTypeface(tfMedium);


            Utility.setEditTextSingleLine(etName);
            Utility.setEditTextSingleLine(etEmail);
            Utility.setEditTextSingleLine(etMobile);
            Utility.setEditTextSingleLine(etMessage);
            etMessage.setMinLines(4);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            CC = new CommonClass(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    
    }
    

    private void setupActionBar() {
     try {
         // TYPEFACE
         Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);
         // ACTIONBAR
         Toolbar toolbar = findViewById(R.id.toolbar);
         setSupportActionBar(toolbar);
         if (getSupportActionBar() != null) {
             //getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM);
             getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
             getSupportActionBar().setDisplayHomeAsUpEnabled(false);
             getSupportActionBar().setDisplayShowHomeEnabled(false);
             ViewCompat.setElevation(toolbar, 10);

             ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
             ivActionBack.setOnClickListener(v -> onBackPressed());
             TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
             tvTitle.setText(R.string.title_activity_contact_us);
             tvTitle.setTypeface(tfMedium);
             Utility.convertToLowerCase(tvTitle);
             if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                 findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
         }
     }catch (Exception e){
         e.printStackTrace();
     }
    }
    

    public void preparedate() {

        enquerytype = new ArrayList<>();
        enquerytype.add(0, "Select type of Enquiry");
        enquerytype.add(1, "Customer Queries");
        enquerytype.add(2, "Business enquiries");
        enquerytype.add(3, "Media/Press enquiries");
        enquerytype.add(4, "Want us to sell tickets for your event");
        enquerytype.add(5, "Other enquiries");


    }

    public boolean validate() {

        String name = etName.getText().toString().trim();
        String countryCode = etCountryCode.getText().toString().trim();
        String mobile = etMobile.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String message = etMessage.getText().toString().trim();

        if (Validate.isNull(name)) {
            CC.showToast(R.string.common_msg_enter_name);
            etName.setFocusable(true);
            etName.requestFocus();
            return false;

        } else if (enquiry.equalsIgnoreCase("Select type of Enquiry")) {
            CC.showToast(R.string.customer_care_msg_select_enquiry_type);
            return false;

        } else if (Validate.isNull(email)) {
            CC.showToast(R.string.common_msg_enter_email);
            etEmail.setFocusable(true);
            etEmail.requestFocus();
            return false;

        } else if (!Validate.checkEmail(email)) {
            CC.showToast(R.string.common_msg_enter_email_valid);
            etEmail.setFocusable(true);
            etEmail.requestFocus();
            return false;

        } else if (Validate.isNull(countryCode)) {
            CC.showToast(R.string.common_msg_enter_country_code);
            etCountryCode.setFocusable(true);
            etCountryCode.requestFocus();
            return false;

        } else if (Validate.isNull(mobile)) {
            CC.showToast(R.string.common_msg_enter_phone);
            etMobile.setFocusable(true);
            etMobile.requestFocus();
            return false;

        } else if (!Validate.checkPhone_libPhoneNumber(countryCode, mobile)) {
            CC.showToast(R.string.common_msg_enter_mobile_valid);
            etMobile.setFocusable(true);
            etMobile.requestFocus();
            return false;

        } else if (Validate.isNull(message)) {
            CC.showToast(R.string.common_msg_enter_message);
            etMessage.setFocusable(true);
            etMessage.requestFocus();
            return false;

        } else {
            return true;
        }


    }

    //.......................Contact us Ws ...............//
    @Override
    public void onBackPressed() {
        Utility.hideKeyboard(this);
        if (!mDoubleBackToExitPressedOnce && isDataChanged()) {
            AlertDialog.Builder alert = new AlertDialog.Builder(this)
                    .setMessage(R.string.msg_confirm_exit_without_submit)
                    .setPositiveButton(android.R.string.yes, (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        mDoubleBackToExitPressedOnce = true;
                        onBackPressed();
                    })
                    .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());
            AlertDialog dialog = alert.create();
            if (!isFinishing()) dialog.show();
        } else {
            super.onBackPressed();
        }
    }

    private boolean isDataChanged() {
        String name = etName.getText().toString().trim();
        int type = sp_select_enquery.getSelectedItemPosition();
        String email = etEmail.getText().toString().trim();
        String phone = etMobile.getText().toString().trim();
        String message = etMessage.getText().toString().trim();

        return type > 0
                || (!mLogin.isLoggedIn() && Validate.isNotNull(name))
                || (!mLogin.isLoggedIn() && Validate.isNotNull(email))
                || (!mLogin.isLoggedIn() && Validate.isNotNull(phone))
                || (mLogin.isLoggedIn() && (!name.equals(mLogin.getName()) || !email.equals(mLogin.getEmail()) || !phone.equals(mLogin.getMobile())))
                || Validate.isNotNull(message);
    }

    private void wsContactUs() {
        if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

        String email = etEmail.getText().toString().trim();
        String countryCode = etCountryCode.getText().toString().trim();
        String mobile = etMobile.getText().toString().trim();

        try {
            if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile, Constant.DEFAULT_COUNTRY_REGION);
                countryCode = "+" + phoneNumber.getCountryCode();
                mobile = phoneNumber.getNationalNumber() + "";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        RequestContactUs request = new RequestContactUs();
        request.name = etName.getText().toString().trim();
        request.email = email;
        if (Validate.isNotNull(countryCode)) {
            request.mobileno = countryCode + mobile;
        } else {
            request.mobileno = mobile;
        }

        request.enquirytype = enquiry;
        request.message = etMessage.getText().toString().trim();

        TicketNinjaAPI tnAPI = RestApi.createAPI();
        Call<CallbackContactUs> call = tnAPI.ContactUs(request);
        call.enqueue(new Callback<CallbackContactUs>() {
            @Override
            public void onResponse(@NonNull Call<CallbackContactUs> call, @NonNull Response<CallbackContactUs> response) {
                if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                try {
                    if (response.isSuccessful()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            mDoubleBackToExitPressedOnce = true;
                            CC.showAlert(Objects.requireNonNull(response.body()).getError_description(), () -> onBackPressed());

                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                            CC.showToast(Objects.requireNonNull(response.body()).getError_description());

                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    CC.showToast(R.string.msg_something_wrong);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CallbackContactUs> call, @NonNull Throwable t) {
                if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                CC.showToast(R.string.msg_something_wrong);
                t.printStackTrace();
            }
        });
    }

}
