/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.mukesh.countrypicker.Country;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.jetbrains.annotations.NotNull;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.OrderSummaryCategoryAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.TextViewUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.database.DatabaseHelper;
import in.ticketninja.objects.ApplyOfferCode;
import in.ticketninja.objects.MyEvent;
import in.ticketninja.objects.TicketSessionData;
import in.ticketninja.objects.UserAddress;
import in.ticketninja.objects.User_Ticket_Event;
import in.ticketninja.service.TimerService;
import in.ticketninja.widget.OTPVerificationDialog;
import in.ticketninja.widget.PaymentSuccessDialog;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestApplyPromoCode;
import in.ticketninja.ws.request.RequestCheckPaymentSession;
import in.ticketninja.ws.request.RequestGeneratePaytmChecksum;
import in.ticketninja.ws.request.RequestGetTicketSessionData;
import in.ticketninja.ws.request.RequestInsertPlaceOrder;
import in.ticketninja.ws.request.RequestRemovePromoCode;
import in.ticketninja.ws.request.RequestRemoveSessionData;
import in.ticketninja.ws.request.RequestUserTicket;
import in.ticketninja.ws.response.CallbackApplyPromoCode;
import in.ticketninja.ws.response.CallbackGeneratePaytmChecksum;
import in.ticketninja.ws.response.CallbackGetTicketSessionData;
import in.ticketninja.ws.response.CallbackInsertPlaceOrder;
import in.ticketninja.ws.response.CallbackRemovePromoCode;
import in.ticketninja.ws.response.CallbackRemoveTicketSession;
import in.ticketninja.ws.response.CallbackUserTicket;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderSummaryActivity extends AppCompatActivity /*implements PaymentResultWithDataListener*/ {

    private final String TAG = OrderSummaryActivity.class.getSimpleName();
    private CommonClass CC;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;
    private String eventSessionId = "";

    private View llMainLayout;
    private View llErrorLayout;
    private ImageView ivErrorImage;
    private TextView tvErrorMessage, tvErrorButton;

    private ImageView ivEventPoster;
    private TextView tvEventTitle;
    private TextView tvEventDate;
    private TextView tvEventTime;
    private TextView tvOrderTicketCount;
    private TextView tvOrderVenue;
    private RecyclerView rvOrderCategory;
    private TextView tvOrderAmount;
    private TextView tvEmailId;
    private TextView tvMobile;

    //private CountDownTimer mTimer;
    private ProgressBar progressBar;
    private TextView tvTimerCountDown,tvTime;
    private Intent timerServiceIntent;
    private TicketSessionData ticketData;

    private AlertDialog backAlertDialog;
    private boolean doubleBackToExitPressedOnce = false;

    private boolean isRegister = false;
    private LinearLayout llEventTerms;
    private TextView tvTerms, tvTitleTerms;
    private LinearLayout llTermsList;
    private ImageView ivEventTermsArrow;
    private PreferencesUtils mPref;
    private NestedScrollView nestedScrollView;
    private View viewDivider;
    private Button btn_continue, btn_paytm, btn_place_order;

    private TextView tvTotalProcessingFee, tvPFee1, tvPFee2, tvLabelPFee2;

    private boolean isTermsExpand = false;

    private TicketSessionData eventSessionData;
    private double Amount_show;
    private PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    private OTPVerificationDialog otpVerificationDialog;

    private LinearLayout llMainPromoCode, llCongratsMsg, llProcessingFee;
    private RelativeLayout rlProcessingFee, rlSubTotal, rlDiscount;
    private RelativeLayout rlPromoCode;
    private RelativeLayout llTermsHeader;
    private TextView tvHavePromoCode;
    private TextView tvCongratulations;
    private Button btnApplyCode;
    private EditText etPromoCode;
    private String mPromoCode;
    private LinearLayout llSubTotalDiscount, llProcessing;
    private TextView tvDiscount, tvSubTotal, tvLabelProcessingFee;
    private static String user_Info_List, seatType;


    //adding by priyanka
  /*  private static OrderSummaryActivity singletonObject;
    public static synchronized OrderSummaryActivity getSingletonObject()
    {
        if (singletonObject == null)
        {
            singletonObject = new OrderSummaryActivity();
        }
        return singletonObject;
    }*/
    //complete

    private BroadcastReceiver timerBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //long counter = intent.getLongExtra("countdown", 0);
            int percentage = intent.getIntExtra("percentage", 0);
            boolean in_progress = intent.getBooleanExtra("in_progress", true);
            String string_to_show = intent.getStringExtra("string_to_show");

            if (in_progress) {
                progressBar.setProgress(percentage);
                // Log.d(TAG, "progressBar ==" + progressBar.getProgress());
                tvTimerCountDown.setText(string_to_show);
                tvTime.setText(string_to_show);
               // tvTime.setText(String.format("%s - %s","Time left",string_to_show));

            } else {
                onStopTimerService();
            }
           /* Log.d(TAG, "---------------------------------------");
            Log.d(TAG, "counter == " + counter);
            Log.d(TAG, "percentage == " + percentage);
            Log.d(TAG, "in_progress == " + in_progress);
            Log.d(TAG, "string_to_show == " + string_to_show);*/
        }
    };
    private String mCoupenCode;

    public static void start(Context context, String sessionID, String seatType, String userIdentificationList) {
        Intent starter = new Intent(context, OrderSummaryActivity.class);
        starter.putExtra(Constant.ScreenExtras.EVENT_SESSION_ID, sessionID);
        starter.putExtra(Constant.ScreenExtras.TYPE, seatType);
        user_Info_List = userIdentificationList;
        //starter.putExtra(Constant.ScreenExtras.USER_IDENTIFICATION_LIST, user_identification_list);
        context.startActivity(starter);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {

            setContentView(R.layout.activity_order_summary);

            getIntentData();

            initializeData();

            AlertDialog.Builder backAlert = new AlertDialog.Builder(this)
                    .setMessage(R.string.msg_confirm_cancel_order)
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.yes, (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        wsRemoveTicketSession(eventSessionId);
                    }).setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());
            backAlertDialog = backAlert.create();

            setUpToolbar();

            findViewById();

            tvOrderAmount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    Log.e(TAG, "onTextChanged:" + s);
                    if (s.toString().contains("Free")) {
                        updateUIForFreePaybleAmount();
                    } else {
                        updateUIForPaybleAmount();
                    }

                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

            isTermsExpand = false;
            llTermsHeader.setOnClickListener(v -> {
                //Log.e(TAG, "llTermsHeader == onClick");
                if (isTermsExpand) {
                    expandCollapseTerms(false);
                } else {
                    expandCollapseTerms(true);
                }
            });
            TextView tvEditContactDetail = findViewById(R.id.tvEditContactDetail);
            tvEditContactDetail.setOnClickListener(view -> startActivityForResult(new Intent(OrderSummaryActivity.this, ContactConfirmationActivity.class)
                            .putExtra(Constant.ScreenExtras.CONTACT_ACTION, Constant.ContactActions.EDIT)
                    , Constant.ActivityForResult.UPDATE_CONTACT_DETAIL));


            progressBar.setProgress(100);
        /*int maxSeconds = (int) TimeUnit.MILLISECONDS.toSeconds(Constant.PAYMENT_SESSION_TIMEOUT);
        progressBar.setMax(maxSeconds);
        progressBar.setProgress(maxSeconds);*/

            btn_place_order.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    wsInsertPlaceOrder(mLogin.getUserId(), eventSessionId, Constant.PaymentType.COMPLEMENTARY,
                            "", 0, "", mPref.getCountryCode(),
                            mPref.getUserPhoneNumber(), mPref.getUserEmailId(), "", null);
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });


            btn_paytm.setOnClickListener(view -> paytm());

            btn_continue.setOnClickListener(v -> {
                if (isUserLoggedIn()) {
                    if (CC.isOnline()) {
                        if (Constant.Code.ONE == eventSessionData.isRSVP() || Constant.Code.ONE == eventSessionData.isInviteRSVP()) {
                            rsvpEvent();
                        } else {
                            String name = mLogin.getName() + "";
                            String email = mPref.getUserEmailId();
                            String countryCode = mPref.getCountryCode();
                            String mobile = mPref.getUserPhoneNumber();
                            try {
                                if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                                    Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile, Constant.DEFAULT_COUNTRY_REGION);
                                    countryCode = "+" + phoneNumber.getCountryCode();
                                    mobile = phoneNumber.getNationalNumber() + "";
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            //Log.e(TAG, "Amount_show=" + Amount_show);
                            checkPaymentSession(eventSessionId, name, email, mobile, countryCode);

                            //startPayment(Amount_show * 100, eventSessionData.getEvent_name(), name, email, mobile, countryCode);
                            //startPayment(1 * 100, eventSessionData.getEvent_name(), email, mobile);
                        }

                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                }
            });


            rvOrderCategory.setHasFixedSize(true);
            rvOrderCategory.setNestedScrollingEnabled(false);

        /*timerServiceIntent = new Intent(this, TimerService.class);
        startService(timerServiceIntent);*/


            setHavePromoCodeText();
            tvHavePromoCode.setOnClickListener(view -> showLayoutToAddPromoCode());

            TextView tvRemovePromoCode = findViewById(R.id.tvRemovePromoCode);
            tvRemovePromoCode.setOnClickListener(view -> CC.showAlert(String.format(getString(R.string.alert_remove_promo_code), mCoupenCode), android.R.string.yes, android.R.string.no, () -> {
                if (Utility.isOnline(OrderSummaryActivity.this)) {
                    removePromoCodeWSCall(eventSessionId, false);
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            }));

            disableButtonApplyCode();
            btnApplyCode.setOnClickListener(view -> {
                if (Utility.isOnline(OrderSummaryActivity.this)) {
                    Log.e(TAG, "email1 : " + mLogin.getEmail());
                    applyPromoCodeWSCall(eventSessionId, String.valueOf(mLogin.getUserId()), mPromoCode, mLogin.getEmail(), mLogin.getMobile());
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });

            etPromoCode.setSingleLine(true);
            etPromoCode.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    Log.e(TAG, "onTextChanged :" + charSequence);
                    mPromoCode = charSequence.toString().trim();
                    if (Validate.isNotNull(mPromoCode)) {
                        enableButtonApplyCode();
                    } else {
                        disableButtonApplyCode();
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });


            tvLabelProcessingFee.setOnClickListener(view -> {
                if (llProcessing.getVisibility() == View.VISIBLE) {
                    tvLabelProcessingFee.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_share_arrow_down, 0);
                    llProcessing.setVisibility(View.GONE);
                } else {
                    tvLabelProcessingFee.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_share_arrow_up, 0);
                    llProcessing.setVisibility(View.VISIBLE);
                }
            });

            showContactDetail();

            if (CC.isOnline()) {
                wsGetTicketSessionData(eventSessionId);
            } else {
                showNoInternet();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpToolbar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
                findViewById(R.id.ivActionLogo).setVisibility(View.VISIBLE);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initializeData() {

        try {
            mPref = new PreferencesUtils(this);
            CC = new CommonClass(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
            otpVerificationDialog = new OTPVerificationDialog(this);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        try {
            if (getIntent().hasExtra(Constant.ScreenExtras.USER_IDENTIFICATION_LIST)) {
                user_Info_List = getIntent().getStringExtra(Constant.ScreenExtras.USER_IDENTIFICATION_LIST);
                Log.e(TAG, "user_Info_List : " + user_Info_List);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.TYPE)) {
                seatType = getIntent().getStringExtra(Constant.ScreenExtras.TYPE);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_SESSION_ID)) {
                eventSessionId = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_SESSION_ID);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void findViewById() {
        try {
            llMainLayout = findViewById(R.id.llMainLayout);
            llMainLayout.setVisibility(View.GONE);
            viewDivider = findViewById(R.id.viewDivider);
            llErrorLayout = findViewById(R.id.llErrorLayout);
            ivErrorImage = findViewById(R.id.ivErrorImage);
            tvErrorMessage = findViewById(R.id.tvErrorMessage);
            tvErrorButton = findViewById(R.id.tvErrorButton);

            nestedScrollView = findViewById(R.id.nestedScrollView);
            ivEventPoster = findViewById(R.id.ivEventPoster);
            tvEventTitle = findViewById(R.id.tvEventTitle);
            tvEventDate = findViewById(R.id.tvEventDate);
            tvEventTime = findViewById(R.id.tvEventTime);
            tvOrderTicketCount = findViewById(R.id.tvOrderTicketCount);
            tvOrderVenue = findViewById(R.id.tvOrderVenue);
            tvOrderAmount = findViewById(R.id.tvOrderAmount);
            llEventTerms = findViewById(R.id.llEventTerms);
            tvEmailId = findViewById(R.id.tvEmailId);
            tvMobile = findViewById(R.id.tvMobile);
            tvTerms = findViewById(R.id.tvTerms);
            tvTitleTerms = findViewById(R.id.tvTitleTerms);
            setTitleTerms();
            llTermsList = findViewById(R.id.llTermsList);
            ivEventTermsArrow = findViewById(R.id.ivEventTermsArrow);
            llTermsHeader = findViewById(R.id.llTermsHeader);
            progressBar = findViewById(R.id.progressBar);
            tvTimerCountDown = findViewById(R.id.tv_count_down);
            tvTime = findViewById(R.id.tvTime);

            // Sub total Discount layout
            llSubTotalDiscount = findViewById(R.id.llSubTotalDiscount);
            tvSubTotal = findViewById(R.id.tvSubTotal);
            tvDiscount = findViewById(R.id.tvDiscount);

            llProcessingFee = findViewById(R.id.llProcessingFee);
            rlSubTotal = findViewById(R.id.rlSubTotal);
            rlDiscount = findViewById(R.id.rlDiscount);
            rlProcessingFee = findViewById(R.id.rlProcessingFee);
            tvTotalProcessingFee = findViewById(R.id.tvTotalProcessingFee);
            tvPFee1 = findViewById(R.id.tvPFee1);
            tvPFee2 = findViewById(R.id.tvPFee2);
            tvLabelPFee2 = findViewById(R.id.tvLabelPFee2);
            llProcessing = findViewById(R.id.llProcessing);
            llProcessing.setVisibility(View.GONE);
            tvLabelProcessingFee = findViewById(R.id.tvLabelProcessingFee);

            btn_paytm = findViewById(R.id.btn_paytm);

            llMainPromoCode = findViewById(R.id.llMainPromoCode);
            rlPromoCode = findViewById(R.id.rlPromoCode);
            llCongratsMsg = findViewById(R.id.llCongratsMsg);
            tvCongratulations = findViewById(R.id.tvCongratulations);
            tvHavePromoCode = findViewById(R.id.tvHavePromoCode);

            btnApplyCode = findViewById(R.id.btnApplyCode);
            rvOrderCategory = findViewById(R.id.rvOrderSummaryCategory);
            btn_continue = findViewById(R.id.btn_continue);
            btn_place_order = findViewById(R.id.btn_place_order);
            etPromoCode = findViewById(R.id.etPromoCode);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setHavePromoCodeText() {
        try {
            String str = getString(R.string.str_have_a_promo_code);
            SpannableString content = new SpannableString(str);
            content.setSpan(new UnderlineSpan(), 0, str.length(), 0);
            tvHavePromoCode.setText(content);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    //WS Remove Promo Code
    private void removePromoCodeWSCall(String eventSessionId, boolean b) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestRemovePromoCode request = new RequestRemovePromoCode(eventSessionId);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackRemovePromoCode> call = tnAPI.removePromoCode(request);
            call.enqueue(new Callback<CallbackRemovePromoCode>() {
                @Override
                public void onResponse(@NonNull Call<CallbackRemovePromoCode> call, @NonNull Response<CallbackRemovePromoCode> response) {

                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {

                        // TODO: 3/5/18 SHREYA : WS res Validation
//                    Log.e(TAG,)
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                            ApplyOfferCode data = Objects.requireNonNull(response.body()).data();
                            hideLayoutSubTotalDiscountDetail(data);
                            showLayoutPromoCode();

                            Amount_show = data.getNetamount();

                            //adding by priyanka
                            eventSessionData.setNetamount(data.getNetamount());
                            showErrorDialog(b);
                            btn_continue.setVisibility(View.VISIBLE);//add 18/10/2019
                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                            CC.showAlert(Objects.requireNonNull(response.body()).getError_description());

                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackRemovePromoCode> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    //WS Apply Promo Code
    private void applyPromoCodeWSCall(String session_Id, String user_Id, String code, String email, String mobileno) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            // RequestApplyPromoCode request = new RequestApplyPromoCode(new OfferCode(session_Id, user_Id, code, email, mobileno));
            RequestApplyPromoCode request = new RequestApplyPromoCode(session_Id, user_Id, code, email, mobileno);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackApplyPromoCode> call = tnAPI.applyPromoCode(request);
            call.enqueue(new Callback<CallbackApplyPromoCode>() {
                @Override
                public void onResponse(@NonNull Call<CallbackApplyPromoCode> call, @NonNull Response<CallbackApplyPromoCode> response) {

                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        // TODO: 3/5/18 SHREYA : WS res VAlidation
//                    Log.e(TAG,)
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            ApplyOfferCode data = Objects.requireNonNull(response.body()).data();
                            showLayoutSubTotalDiscountDetail(data);
                            showLayoutCongretsOnUsePromoCode(data);

                            Amount_show = data.getNetamount();
                            mCoupenCode = data.getCoupon_code();

                            //adding by priyanka
                            eventSessionData.setNetamount(data.getNetamount());

                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                            CC.showAlert(Objects.requireNonNull(response.body()).getError_description());

                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackApplyPromoCode> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    private void hideLayoutSubTotalDiscountDetail(ApplyOfferCode data) {
        try {
            //llSubTotalDiscount.setVisibility(View.GONE);
            rlSubTotal.setVisibility(View.GONE);
            rlDiscount.setVisibility(View.GONE);
            //set Order Amount
            if (data.getNetamount() == 0) {
                tvOrderAmount.setText(R.string.str_price_free);
            } else {
                tvOrderAmount.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getNetamount()));
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void showLayoutSubTotalDiscountDetail(ApplyOfferCode data) {
        try {
            llSubTotalDiscount.setVisibility(View.VISIBLE);
            rlSubTotal.setVisibility(View.VISIBLE);
            rlDiscount.setVisibility(View.VISIBLE);
            //set Sub Total
            if (data.getAmount() == 0) {
                tvSubTotal.setText(R.string.str_price_free);
            } else {
                tvSubTotal.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getAmount()));
            }

            //set Discount
            if (data.getDiscount() == 0) {
                tvDiscount.setText(R.string.str_price_free);
            } else {
                tvDiscount.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getDiscount()));
            }

            //set Order Amount
            if (data.getNetamount() == 0) {
                tvOrderAmount.setText(R.string.str_price_free);
            } else {
                tvOrderAmount.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getNetamount()));
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void disableButtonApplyCode() {
        try {
            btnApplyCode.setEnabled(false);
            btnApplyCode.setAlpha(0.5f);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void enableButtonApplyCode() {
        try {
            btnApplyCode.setEnabled(true);
            btnApplyCode.setAlpha(1f);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onStop() {
        //Log.e(TAG, "onStop");
        try {
            if (timerBroadcastReceiver != null && isRegister) {
                unregisterReceiver(timerBroadcastReceiver);
                isRegister = false;
            }
            super.onStop();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            mPref = new PreferencesUtils(this);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onStart() {
        //Log.e(TAG, "onStart");
        super.onStart();
        try {
            if (timerServiceIntent != null && !Utility.isMyServiceRunning(this, TimerService.class)) {
                try {
                    timerBroadcastReceiver = null;

                    onStopTimerService();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (timerBroadcastReceiver != null && Utility.isMyServiceRunning(this, TimerService.class)) {
                registerReceiver(timerBroadcastReceiver, new IntentFilter(TimerService.BROADCAST_ACTION));
                isRegister = true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        try {
            Utility.hideKeyboard(this);
            if (doubleBackToExitPressedOnce) {
                stopTimerService();
                super.onBackPressed();

            } else if (Validate.isNotNull(eventSessionId)) {
                if (!isFinishing()) backAlertDialog.show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        try {
            Log.e(TAG, "onDestroy");
            stopTimerService();
            dismissProgressDialog();
            super.onDestroy();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {

                if (requestCode == Constant.ActivityForResult.LOGIN && data != null) {
                    int intExtra = data.getIntExtra(Constant.ScreenExtras.DATA, 0);
                    if (intExtra == Constant.ScreenExtras.PRESS_SIGNUP) {
                        openRegister();
                    } else {
                        mLogin = new LoginUtils(this);
                        syncBookingHistory();
                    }
                } else if (requestCode == Constant.ActivityForResult.REGISTER && data != null) {

                    int intExtra = data.getIntExtra(Constant.ScreenExtras.DATA, 0);
                    if (intExtra == Constant.ScreenExtras.PRESS_LOGIN) {
                        String mailExtra = "";
                        if (data.hasExtra(Constant.ScreenExtras.EMAIL))
                            mailExtra = data.getStringExtra(Constant.ScreenExtras.EMAIL);
                        openLogin(mailExtra);
                    } else {
                        mLogin = new LoginUtils(this);
                        syncBookingHistory();
                    }
                } else if (requestCode == Constant.ActivityForResult.UPDATE_CONTACT_DETAIL) {
                    Utility.hideKeyboard(this);
                    showContactDetail();
                    removePromoCode();
                }
            }/*else if (resultCode == RESULT_CANCELED) {
                if (!Utility.isMyServiceRunning(this, TimerService.class)) startTimerService();
            }*/

            else {
                stopTimerService();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //adding by priyanka
    private void removePromoCode() {
        try {
            String previousEmail = mLogin.getEmail();
            Log.e(TAG, "previousEmail : " + previousEmail);

            String previousPhoneNo = mLogin.getMobile();
            Log.e(TAG, "previousPhoneNo : " + previousPhoneNo);

            mLogin = new LoginUtils(this);
            Log.e(TAG, "email1 : " + mLogin.getEmail());
            Log.e(TAG, "mobile : " + mLogin.getMobile());
            if (!previousEmail.equals(mLogin.getEmail()) || !previousPhoneNo.equals(mLogin.getMobile())) {
                if (rlDiscount.getVisibility() == View.VISIBLE) {
                    Log.e(TAG, "tvDiscount visible");
                    if (Utility.isOnline(this)) {
                        removePromoCodeWSCall(eventSessionId, true);
                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setTitleTerms() {
        try {
            //      Terms and Conditions title
            SpannableString styledString = new SpannableString("By placing the order, you agree to the Terms-and-conditions.");
            styledString.setSpan(new ForegroundColorSpan(Color.parseColor("#F53033")), 39, 60, 0);
            styledString.setSpan(new TextViewUtils.MyClickableSpan() {
                @Override
                public void onClick(@NotNull View widget) {
                    Utility.hideKeyboard(OrderSummaryActivity.this);
                    if (CC.isOnline()) {
                        Intent iterms_condition = new Intent(getApplicationContext(), TermsAndConditionActivity.class);
                        startActivity(iterms_condition);
                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                }
            }, 39, 60, 0);
            tvTitleTerms.setMovementMethod(LinkMovementMethod.getInstance());
            tvTitleTerms.setText(styledString);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean isUserLoggedIn() {
        try {
            if (!backAlertDialog.isShowing()) {
                if (CC.isOnline()) {
//                if (mLogin.isLoggedIn()) {
                    return true;
//                } else {
//                    openLogin("");
//                    return false;
//                }
                } else {
                    CC.showToast(R.string.msg_no_internet);
                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    private void showLayoutProcessingFee(TicketSessionData data) {
        try {
            llSubTotalDiscount.setVisibility(View.VISIBLE);
            llProcessingFee.setVisibility(View.VISIBLE);
            tvTotalProcessingFee.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getTotal_processingfee()));
            tvPFee1.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getProcessingfee()));
            tvPFee2.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getTax()));
            tvLabelPFee2.setText(data.getTax_type());
            rlSubTotal.setVisibility(View.GONE);
            rlDiscount.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void hideLayoutProcessingFee() {
        try {
            llSubTotalDiscount.setVisibility(View.GONE);
            llProcessingFee.setVisibility(View.GONE);
            rlSubTotal.setVisibility(View.GONE);
            rlDiscount.setVisibility(View.GONE);
            rlProcessingFee.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showLayoutPromoCode() {
        try {
            llMainPromoCode.setVisibility(View.VISIBLE);
            tvHavePromoCode.setVisibility(View.VISIBLE);
            llCongratsMsg.setVisibility(View.GONE);
            rlPromoCode.setVisibility(View.GONE);
            etPromoCode.setText("");
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void showErrorDialog(boolean b) {
        try {
            if (b) {
                CC.showAlert(R.string.offer_code_removed);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @SuppressLint("StringFormatMatches")
    private void showLayoutCongretsOnUsePromoCode(ApplyOfferCode data) {
        try {
            llMainPromoCode.setVisibility(View.VISIBLE);
            tvHavePromoCode.setVisibility(View.GONE);
            llCongratsMsg.setVisibility(View.VISIBLE);
            rlPromoCode.setVisibility(View.GONE);
//        setCongretsText();
            tvCongratulations.setText(String.format(getString(R.string.str_congratulation_on_promocode_discount), data.getDiscount(), data.getCoupon_code()));
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void showLayoutToAddPromoCode() {
        llMainPromoCode.setVisibility(View.VISIBLE);
        tvHavePromoCode.setVisibility(View.GONE);
        llCongratsMsg.setVisibility(View.GONE);
        rlPromoCode.setVisibility(View.VISIBLE);
    }

    private void expandCollapseTerms(boolean expand) {
        try {
            if (expand || llEventTerms.getVisibility() != View.VISIBLE) {
                viewDivider.setVisibility(View.VISIBLE);
                llTermsList.setVisibility(View.VISIBLE);
                //ivTermsArrow.animate().rotation(180).start();
                ivEventTermsArrow.animate().scaleY(-1f).start();
                //rvEventTerms.animate().scaleY(1f).start();
                //rvEventTerms.animate().translationY(-rvEventTerms.getHeight()).translationYBy(rvEventTerms.getHeight()).start();
                ivEventTermsArrow.setRotation(1800);
                isTermsExpand = true;
                nestedScrollView.post(() -> nestedScrollView.fullScroll(ScrollView.FOCUS_DOWN));
            } else {
                //rvEventTerms.animate().scaleY(0f).start();
                //rvEventTerms.animate().translationY(rvEventTerms.getHeight()).translationYBy(-rvEventTerms.getHeight()).start();
                llTermsList.setVisibility(View.GONE);
                viewDivider.setVisibility(View.GONE);
                ivEventTermsArrow.animate().scaleY(1f).start();
                //ivTermsArrow.animate().rotation(0).start();
                ivEventTermsArrow.setRotation(0);
                isTermsExpand = false;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void showContactDetail() {
        try {
            mPref = new PreferencesUtils(OrderSummaryActivity.this);
            tvEmailId.setText(mPref.getUserEmailId());
            tvMobile.setText(mPref.getUserPhoneNumber());
            if (Validate.isNotNull(mPref.getCountryCode())) {
                tvMobile.setText(String.format("%1s-%2s", mPref.getCountryCode(), mPref.getUserPhoneNumber()));
            } else {
                Country country = Country.getCountryByISO(Constant.DEFAULT_COUNTRY_REGION);
                tvMobile.setText(String.format("%1s-%2s", country.getDialCode(), mPref.getUserPhoneNumber()));

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void invalidateData(TicketSessionData data) {
        try {
            if (data == null) {
                return;
            }
            eventSessionData = data;
            //Amount_show = eventSessionData.getAmount();
            Amount_show = eventSessionData.getNetamount();

            //btn_paytm.setVisibility((eventSessionData.isPaytmPaymentAllow() && (!eventSessionData.isRSVP() && !eventSessionData.isInviteRSVP())) ? View.VISIBLE : View.GONE);
            btn_paytm.setVisibility((eventSessionData.isPaytmPaymentAllow() && (Constant.Code.ONE == eventSessionData.isRSVP() && Constant.Code.ONE == eventSessionData.isInviteRSVP())) ? View.VISIBLE : View.GONE);
            btn_paytm.setVisibility(View.GONE);//add 18/10/2019

            //btn_continue.setText((eventSessionData.isRazorpayPaymentAllow() && (eventSessionData.isRSVP() || eventSessionData.isInviteRSVP())) ? R.string.event_btn_rsvp : R.string.btn_pay_now);
            btn_continue.setText((eventSessionData.isRazorpayPaymentAllow() && (Constant.Code.ONE == eventSessionData.isRSVP() || Constant.Code.ONE == eventSessionData.isInviteRSVP())) ? R.string.event_btn_rsvp : R.string.btn_pay_now);
            btn_continue.setVisibility(View.VISIBLE);//add 18/10/2019

            //Log.e(TAG, "invalidateData");
            this.ticketData = data;
            if (Validate.isNotNull(data.getImageUrl())) {
                Glide.with(this)
                        .load(data.getImageUrl())
                        //.centerCrop()
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(ivEventPoster);
                ivEventPoster.setVisibility(View.VISIBLE);
            } else {
                ivEventPoster.setVisibility(View.GONE);
            }

            tvEventTitle.setText(Validate.isNotNull(data.getEvent_name()) ? String.format("%s", data.getEvent_name()) : "");
            if (Validate.isNotNull(data.getMDDate())) {
               /* if ((data.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY)
                        || data.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW))
                    *//*&& data.isSeasonPass()*//*) {
                    tvEventDate.setText(Validate.isNotNull(data.getMDDate()) ? String.format("%s, %s", data.getMDDate(), data.getYear()) : "");
                } else {
                    tvEventDate.setText(Validate.isNotNull(data.getMDDate()) ? String.format("%s, %s\n%s", data.getMDDate(), data.getYear(), data.getDay()) : "");
                }*/

                if (!data.getEvent_date().equals(data.getEvent_date_to())
                    /*&& data.isSeasonPass()*/) {
                    //tvEventDate.setText(Validate.isNotNull(data.getMDDate()) ? String.format("%s, %s", data.getMDDate(), data.getYear()) : "");
                    tvEventDate.setText(Validate.isNotNull(data.getMDDate1(Utility.EVENT_DETAIL)) ? String.format("%s", data.getMDDate1(Utility.EVENT_DETAIL)) : "");
                } else {
                    tvEventDate.setText(Validate.isNotNull(data.getMDDate()) ? String.format("%s, %s\n%s", data.getMDDate(), data.getYear(), data.getDay()) : "");
                }

            } else {
                tvEventDate.setText("-");
            }

            if (data.getDayType().equalsIgnoreCase(Constant.EventDayType.SINGLE_DAY)) {
                tvEventTime.setText(String.format("%s %s", data.getEvent_time(), Constant.ONWARDS));
            } else if (data.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY)
                    || data.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW)) {
                tvEventTime.setText(String.format("%s %s", data.getEvent_time(), Constant.ONWARDS));
            } else if (data.getDayType().equalsIgnoreCase(Constant.EventDayType.PERPETUAL)) {
                tvEventTime.setText(String.format("%s", data.getEvent_time()));
            } else {
                tvEventTime.setText(String.format("%s", data.getEvent_time()));
            }

            tvOrderVenue.setText(Validate.isNotNull(data.getVenueName()) ? String.format("%s", data.getVenueName()) : "");
            tvOrderVenue.setOnClickListener(v -> CommonClass.openMap(v.getContext(), data.getVenueName(), data.getLatitude(), data.getLongitude()));

            tvOrderTicketCount.setText(String.format(Locale.getDefault(), data.getTotalTicket() > 1 ? "%d\nTickets" : "%d\nTicket", data.getTotalTicket()));
            if (data.getAmount() == 0) {
                tvOrderAmount.setText(R.string.str_price_free);
            } else {
                // tvOrderAmount.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getAmount()));
                tvOrderAmount.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getNetamount()));
            }

            if (data.getPriceCategory() != null) {
                OrderSummaryCategoryAdapter mAdapter = new OrderSummaryCategoryAdapter(data.getPriceCategory(), data.eventType());
                rvOrderCategory.setAdapter(mAdapter);
            }

            if (data.getPaymentNoteList() != null && data.getPaymentNoteList().size() > 0) {
                List<String> noteList = new ArrayList<>();
                for (int i = 0; i < data.getPaymentNoteList().size(); i++)
                    noteList.add((i + 1) + ". " + data.getPaymentNoteList().get(i));
                tvTerms.setText(TextUtils.join("\n", noteList));
                llEventTerms.setVisibility(View.VISIBLE);
            } else {
                llEventTerms.setVisibility(View.GONE);
            }


            if (!Utility.isMyServiceRunning(this, TimerService.class)) startTimerService();

        /*mTimer = new CountDownTimer(TimeUnit.MINUTES.toMillis(Constant.PAYMENT_SESSION_TIMEOUT), TimeUnit.SECONDS.toMillis(1)) {
            public void onTick(long millisUntilFinished) {
                progressBar.setProgress(((int) millisUntilFinished / 1000 * 100) / (Constant.PAYMENT_SESSION_TIMEOUT * 60));

                if (tvTimerCountDown != null) {
                    tvTimerCountDown.setText(String.format(Locale.getDefault(), Constant.TIMER_FORMAT,
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                    TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                }
            }

            public void onFinish() {
                try {
                    Utility.hideKeyboard(OrderSummaryActivity.this);
                    tvTimerCountDown.setText(R.string.label_done);
                    CC.showAlert(R.string.purchase_ticket_msg_session_timeout, () -> {

                        MyEvent myEvent = new MyEvent();
                        myEvent.event_id = data.getId();
                        myEvent.Poster_Image_Id = data.getImageId();
                        myEvent.poster_image_url = data.getImageUrl();

                        Intent i1 = new Intent(OrderSummaryActivity.this, EventDetailActivity.class);
                        i1.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                        i1.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_PAYMENT);
                        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i1);
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void startTimerService() {
        //Log.e(TAG, "startTimerService");
        try {

            // start service
            MyEvent myEvent = new MyEvent();
            myEvent.event_id = ticketData.getId();
            myEvent.Poster_Image_Id = ticketData.getImageId();
            myEvent.poster_image_url = ticketData.getImageUrl();

            timerServiceIntent = new Intent(this, TimerService.class);
            timerServiceIntent.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
            startService(timerServiceIntent);
            registerReceiver(timerBroadcastReceiver, new IntentFilter(TimerService.BROADCAST_ACTION));
            isRegister = true;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopTimerService() {
        // Log.e(TAG, "stopTimerService");
        try {
            if (timerBroadcastReceiver != null) {
                isRegister = false;
                unregisterReceiver(timerBroadcastReceiver);
                timerBroadcastReceiver = null;
            }
            if (timerServiceIntent != null) {
                stopService(timerServiceIntent);
                timerServiceIntent = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            //e.printStackTrace();
        }
    }

    private void onStopTimerService() {
        // Log.e(TAG, "onStopTimerService");
        try {
            progressBar.setProgress(0);
            tvTimerCountDown.setText(R.string.btn_done);
            tvTime.setText(R.string.btn_done);
            Log.d(TAG, "Finished ");
            CC.showAlert(R.string.purchase_ticket_msg_session_timeout, () -> {

                MyEvent myEvent = new MyEvent();
                myEvent.event_id = ticketData.getId();
                myEvent.Poster_Image_Id = ticketData.getImageId();
                myEvent.poster_image_url = ticketData.getImageUrl();

                Intent i1 = new Intent(OrderSummaryActivity.this, EventDetailActivity.class);
                i1.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                i1.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_PAYMENT);
                i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i1);

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showNoInternet() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_connection_lost)
                    .placeholder(R.drawable.ic_connection_lost_static)
                    .into(ivErrorImage);
            tvErrorMessage.setText(R.string.msg_no_internet);
            llMainLayout.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.VISIBLE);
            tvErrorButton.setText(R.string.btn_try_again);
            tvErrorButton.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    wsGetTicketSessionData(eventSessionId);
                } else {
                    showNoInternet();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showError() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_no_result_found)
                    .placeholder(R.drawable.ic_no_result_found)
                    .into(ivErrorImage);
            llMainLayout.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void updateUIForFreePaybleAmount() {
        btn_continue.setVisibility(View.GONE);
        btn_paytm.setVisibility(View.GONE);
        btn_place_order.setVisibility(View.VISIBLE);
    }

    public void updateUIForPaybleAmount() {
        btn_place_order.setVisibility(View.GONE);
        //btn_paytm.setVisibility((eventSessionData.isPaytmPaymentAllow() && (!eventSessionData.isRSVP() && !eventSessionData.isInviteRSVP())) ? View.VISIBLE : View.GONE);
        //btn_continue.setText((eventSessionData.isRazorpayPaymentAllow() && (eventSessionData.isRSVP() || eventSessionData.isInviteRSVP())) ? R.string.event_btn_rsvp : R.string.btn_pay_now);
        btn_paytm.setVisibility((eventSessionData.isPaytmPaymentAllow() &&
                (Constant.Code.ONE == eventSessionData.isRSVP() && Constant.Code.ONE == eventSessionData.isInviteRSVP())) ? View.VISIBLE : View.GONE);
        btn_continue.setText((eventSessionData.isRazorpayPaymentAllow() &&
                (Constant.Code.ONE == eventSessionData.isRSVP() || Constant.Code.ONE == eventSessionData.isInviteRSVP()))
                ? R.string.event_btn_rsvp : R.string.btn_pay_now);
        btn_continue.setVisibility(View.VISIBLE);//add 18/10/2019
        btn_paytm.setVisibility(View.GONE);//add 18/10/2019
    }

    private void hideLayoutPromoCode() {
        llMainPromoCode.setVisibility(View.GONE);
        tvHavePromoCode.setVisibility(View.GONE);
        rlPromoCode.setVisibility(View.GONE);
        llCongratsMsg.setVisibility(View.GONE);
    }


    private void openLogin(String email) {
        Intent intLogin = new Intent(getApplicationContext(), LoginActivity.class);
        intLogin.putExtra(Constant.ScreenExtras.EMAIL, email);
        startActivityForResult(intLogin, Constant.ActivityForResult.LOGIN);
    }

    private void openRegister() {
        Intent intRegister = new Intent(getApplicationContext(), RegisterActivity.class);
        intRegister.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_MY_PROFILE);
        startActivityForResult(intRegister, Constant.ActivityForResult.REGISTER);
    }

    private void syncBookingHistory() {
        try {
            if (CC.isOnline() && mLogin.isLoggedIn()) {
                if (ContextCompat.checkSelfPermission(this,
                        android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                        || ContextCompat.checkSelfPermission(this,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this,
                            new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                            Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE);
                } else {
                    wsGetBookingHistory(this, mLogin.getUserId());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    //..............................WS CALL FOR GET BOOKING HISTORY..........//
    private void wsGetBookingHistory(Context context, String userId) {

        try {
            RequestUserTicket request = new RequestUserTicket(mLogin.getUserId());

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackUserTicket> call = tnAPI.UserTicket(request);
            call.enqueue(new Callback<CallbackUserTicket>() {
                @Override
                public void onResponse(@NonNull Call<CallbackUserTicket> call, @NonNull Response<CallbackUserTicket> response) {
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                DatabaseHelper DB = new DatabaseHelper(context);

                                try {
                                    if (DB.doesDatabaseExist(OrderSummaryActivity.this)) {
                                        DB.clearMyTicketTable(mLogin.getUserId());
                                        DB.clearMyTicketListTable(mLogin.getUserId());
                                    }
                                }catch (Exception e){
                                    e.printStackTrace();
                                }


                                ArrayList<User_Ticket_Event> ticketList = Objects.requireNonNull(response.body()).data().userTicketEvents();
                                if (ticketList != null && ticketList.size() > 0) {

                                    for (int i = 0; i < ticketList.size(); i++) {
                                        DB.addMyTickets(ticketList.get(i), userId);
                                    }

                                    DB.startDownloadingMyTicketsInBackground();
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackUserTicket> call, @NonNull Throwable t) {
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // WS - Get Ticket Session Data
    private void wsGetTicketSessionData(String sessionId) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestGetTicketSessionData request = new RequestGetTicketSessionData(sessionId);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackGetTicketSessionData> call = tnAPI.getTicketSessionData(request);
            call.enqueue(new Callback<CallbackGetTicketSessionData>() {
                @Override
                public void onResponse(@NonNull Call<CallbackGetTicketSessionData> call, @NonNull Response<CallbackGetTicketSessionData> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                            TicketSessionData data = Objects.requireNonNull(response.body()).data();
                            invalidateData(data);

                            if (Objects.requireNonNull(response.body()).data().getTotal_processingfee() > 0) {
                                showLayoutProcessingFee(data);
                            } else {
                                hideLayoutProcessingFee();
                            }

                            //CHECK FOR PROMO CODE
                            if (Constant.Code.ONE == Objects.requireNonNull(response.body()).data().canAddDiscount()) {
                                showLayoutPromoCode();
                            } else {
                                hideLayoutPromoCode();
                            }

                            llMainLayout.setVisibility(View.VISIBLE);
                            llErrorLayout.setVisibility(View.GONE);

                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                            tvErrorMessage.setText(Objects.requireNonNull(response.body()).getError_description());
                            showError();

                        } else {
                            tvErrorMessage.setText(R.string.msg_something_wrong);
                            showError();
                        }
                    } else {
                        tvErrorMessage.setText(R.string.msg_no_response);
                        showError();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackGetTicketSessionData> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    tvErrorMessage.setText(R.string.msg_something_wrong);
                    showError();
                }
            });

            if (Constant.SEAT_TYPE_ARRANGE_SEATING.equalsIgnoreCase(seatType)) {
                wsUserInfo(sessionId, user_Info_List);
            }
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    // WS - Remove Ticket Session
    private void wsRemoveTicketSession(String sessionId) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestRemoveSessionData request = new RequestRemoveSessionData(sessionId);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackRemoveTicketSession> call = tnAPI.removeTicketSession(request);
            call.enqueue(new Callback<CallbackRemoveTicketSession>() {
                @Override
                public void onResponse(@NonNull Call<CallbackRemoveTicketSession> call, @NonNull Response<CallbackRemoveTicketSession> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            doubleBackToExitPressedOnce = true;
                            onBackPressed();

                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                            CC.showToast(Objects.requireNonNull(response.body()).getError_description());

                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackRemoveTicketSession> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    showError();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }
    }

    // WS - Insert Place Order
    private void wsInsertPlaceOrder(String user_Id, String session_Id, @Constant.PaymentWSType String payment_Type,
                                    String payment_Id, long address_Id, String d_Time_Slot,
                                    String countryCode, String mobileNo, String email, String codOTP, UserAddress userAddress) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestInsertPlaceOrder request;
            if (payment_Type.toLowerCase().equals(Constant.PaymentType.RSVP.toLowerCase())) {
                request = new RequestInsertPlaceOrder(String.valueOf(user_Id), session_Id, payment_Type, countryCode, mobileNo, email);
            } else {
                request = new RequestInsertPlaceOrder(String.valueOf(user_Id), session_Id, payment_Type,
                        payment_Id, address_Id, d_Time_Slot, countryCode, mobileNo, email, codOTP, userAddress);
            }

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackInsertPlaceOrder> call = tnAPI.InsertPlaceOrder(request);
            call.enqueue(new Callback<CallbackInsertPlaceOrder>() {
                @Override
                public void onResponse(@NonNull Call<CallbackInsertPlaceOrder> call, @NonNull Response<CallbackInsertPlaceOrder> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {

                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                if (otpVerificationDialog != null) otpVerificationDialog.dismiss();

                                stopTimerService();

                                long transId = Objects.requireNonNull(response.body()).data().getTransId();

                                Intent intConfirm = new Intent(getApplicationContext(), BookingConfirmationActivity.class);
                                intConfirm.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE, payment_Type);
                                intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, transId);
                                startActivity(intConfirm);

                            } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                                if (otpVerificationDialog != null && otpVerificationDialog.isShowing())
                                    otpVerificationDialog.reset();
                                CC.showAlert(Objects.requireNonNull(response.body()).getError_description());

                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        } else {
                            CC.showToast(R.string.msg_no_response);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(@NonNull Call<CallbackInsertPlaceOrder> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    private void wsInsertPlaceOrder(String user_Id, String session_Id, String orderId, @Constant.PaymentWSType String payment_Type,
                                    @Constant.MerchantWSType String merchant,
                                    String payment_Id, String countryCode, String mobileNo, String email) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestInsertPlaceOrder request = new RequestInsertPlaceOrder(String.valueOf(user_Id), session_Id, orderId,
                    payment_Type, merchant, payment_Id, countryCode, mobileNo, email);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackInsertPlaceOrder> call = tnAPI.InsertPlaceOrder(request);
            call.enqueue(new Callback<CallbackInsertPlaceOrder>() {
                @Override
                public void onResponse(@NonNull Call<CallbackInsertPlaceOrder> call, @NonNull Response<CallbackInsertPlaceOrder> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                if (otpVerificationDialog != null) otpVerificationDialog.dismiss();

                                long transId = Objects.requireNonNull(response.body()).data().getTransId();
                                String transNo = Objects.requireNonNull(response.body()).data().getTransNo();

                                stopTimerService();
                                //mTimer.cancel();

                                PaymentSuccessDialog successDialog = new PaymentSuccessDialog(OrderSummaryActivity.this);
                                // successDialog.setTransactionId(String.valueOf(transId));
                                successDialog.setTransactionId(String.valueOf(transNo));
                                if (Amount_show == 0) {
                                    successDialog.setAmount(R.string.str_price_free);
                                } else {
                                    successDialog.setAmount(String.format(Locale.getDefault(), "₹ %.2f", Amount_show));
                                }
                                successDialog.setOnOkClickListener(() -> {
                                    Intent intConfirm = new Intent(getApplicationContext(), BookingConfirmationActivity.class);
                                    intConfirm.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE, payment_Type);
                                    intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, transId);
                                    startActivity(intConfirm);

                                });

                                try {

                                    if (!isFinishing()) {
                                        successDialog.show();
                                    }

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }


                            } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                                if (otpVerificationDialog != null && otpVerificationDialog.isShowing())
                                    otpVerificationDialog.reset();
                                CC.showAlert(Objects.requireNonNull(response.body()).getError_description());

                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        } else {
                            CC.showToast(R.string.msg_no_response);
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(@NonNull Call<CallbackInsertPlaceOrder> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }
    }

    private void paytm() {
        try {
            if (isUserLoggedIn()) {
                if (CC.isOnline()) {

                    String txnAmount = Amount_show + "";
                    Log.e(TAG, "txnAmount=" + txnAmount);
                    String mobileNo = mPref.getUserPhoneNumber();
                    try {
                        if (Validate.isNotNull(mPref.getCountryCode())) {
                            Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(mPref.getCountryCode() + mobileNo, Constant.DEFAULT_COUNTRY_REGION);
                            mobileNo = phoneNumber.getNationalNumber() + "";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    wsGeneratePaytmChecksum(mLogin.getUserId(), eventSessionId, txnAmount, mPref.getUserEmailId(), mobileNo);

                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    //for razorPay
    public void startPayment(double amount, String eventName, String name, String email, String mobile, String countryCode) {
        try {
            //startPayment_amount=3000.0  eventName=Attraction Test 01  name=Romit Sadaria
            // email=romit@zengroup.co.in  mobile=9377433176  countryCode=+91

            Log.e(TAG, "startPayment_amount=" + amount + "  eventName=" + eventName + "  name=" + name + "  email=" + email + "  mobile=" + mobile + "  countryCode=" + countryCode);


            int amt = (int)amount;
            Log.e(TAG, "amt: "+amt);

          //  Checkout checkout = new Checkout();
           // checkout.setFullScreenDisable(true);
            //Checkout.clearUserData(this);

          //  checkout.setImage(R.drawable.ic_launcher);//Set your logo here

            // Pass your payment options to the Razorpay Checkout as a JSONObject


            JSONObject options = new JSONObject();
            options.put("categoryName", "Ticket Ninja");
            options.put("description", "" + eventName);
            //options.put("notes", "App_Android_Session_Id=" + eventSessionData.getId());
            options.put("currency", "INR");
            //options.put("amount", "" + amount);
            options.put("amount",  amt);
            options.put("receipt", "TicketNinja-" + eventSessionData.getId());
            options.put("prefill.email", email);
            options.put("prefill.contact", mobile);

            JSONObject externals = new JSONObject();
            externals.put("wallets", new String[]{"paytm"});
            options.put("external", externals);
            JSONObject notes = new JSONObject();
            notes.put("App_Android_Session_Id", "" + eventSessionId);
            options.put("notes", notes);

            JSONObject preFill = new JSONObject();
            preFill.put("name", "" + name);
            preFill.put("email", "" + email);
//            preFill.put("rzp_user_email", "" + email);
//            options.putOpt("rzp_user_email",email);

            preFill.put("contact", "" + mobile);

            //preFill.put("country_code", "" + countryCode);
//            options.put("preFill", preFill);

            JSONObject theme = new JSONObject();
            theme.put("color", "#F53033");
            options.put("theme", theme);

            //{"categoryName":"Ticket Ninja","description":"Attraction Test 01",
            // "currency":"INR","amount":"3000.0","receipt":"TicketNinja-1009",
            // "prefill.email":"romit@zengroup.co.in","prefill.contact":"9377433176",
            // "external":{"wallets":"[Ljava.lang.String;@fd932ca"},"
            // notes":{"App_Android_Session_Id":"75R0dS9Tw6ZQHklnFmyyw92ccCGaxmWv"},
            // "theme":{"color":"#F53033"}}


            //old:{"categoryName":"Ticket Ninja","
            // description":"Test Daily Park","currency":"INR",
            // "amount":"10000.0","receipt":"TicketNinja-1044",
            // "prefill.email":"romit@zengroup.co.in","prefill.contact":"1234567890",
            // "external":{"wallets":"[Ljava.lang.String;@30dbbf1"},
            // "notes":{"App_Android_Session_Id":"56a70b30ddc1441e99e689c8e122ce0a"},
            // "theme":{"color":"#F53033"}}

            //  checkout.open(activity, options);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //razorPay Payment success
   /* @Override
    public void onPaymentSuccess(String razorPayPaymentID, PaymentData data) {
        *//*String paymentId = data.getPaymentId();
        String signature = data.getData().toString();
        String orderId = data.getOrderId();*//*
        Log.e(TAG, "onPaymentSuccess");
        try {
            Answers.getInstance().logPurchase(new PurchaseEvent()
                    .putItemPrice(BigDecimal.valueOf(Amount_show))
                    .putCurrency(Currency.getInstance("INR"))
                    .putItemName(eventSessionData.getEvent_name())
                    .putItemType(eventSessionData.eventType())
                    .putItemId("" + eventSessionData.getId())
                    .putCustomAttribute("paymentID", "" + razorPayPaymentID)
                    .putSuccess(true));
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            if (CC.isOnline()) {
                String email = data.getUserEmail();
                String countryCode = mPref.getCountryCode();
                String mobile = data.getUserContact();
                String sessionId = eventSessionId;
                try {
                    if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                        Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile, Constant.DEFAULT_COUNTRY_REGION);
                        countryCode = "+" + phoneNumber.getCountryCode();
                        mobile = phoneNumber.getNationalNumber() + "";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                wsInsertPlaceOrder(mLogin.getUserId(), sessionId, eventSessionId, Constant.PaymentType.ONLINE,
                        Constant.MerchantType.RAZORPAY,
                        razorPayPaymentID, countryCode, mobile, email);

            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //razorPay payment error
    @Override
    public void onPaymentError(int code, String response, PaymentData data) {
        Log.e(TAG, "onPaymentError: " + response +" , code : "+code +", PaymentData: "+data.toString());
        try {
            if (code > 0) CC.showAlert("Payment Failure :", response);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "getMessage: " + e.getMessage());
        }

        try {
            Answers.getInstance().logPurchase(new PurchaseEvent()
                    .putItemPrice(BigDecimal.valueOf(Amount_show))
                    .putCurrency(Currency.getInstance("INR"))
                    .putItemName(eventSessionData.getEvent_name())
                    .putItemType(eventSessionData.eventType())
                    .putItemId("" + eventSessionData.getId())
                    .putCustomAttribute("errorCode", "" + code)
                    .putCustomAttribute("errorMessage", "" + response)
                    .putSuccess(false));
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "getMessage1: " + e.getMessage());
        }

    }*/

    private void rsvpEvent() {
        try {
            if (CC.isOnline()) {

//            String email = etEmail.getText().toString().trim();
                String countryCode = mPref.getCountryCode();
                String mobile = mPref.getUserPhoneNumber();
                try {
                    if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                        Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile, Constant.DEFAULT_COUNTRY_REGION);
                        countryCode = "+" + phoneNumber.getCountryCode();
                        mobile = phoneNumber.getNationalNumber() + "";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                wsInsertPlaceOrder(mLogin.getUserId(), eventSessionId, Constant.PaymentType.RSVP,
                        "", 0, "", countryCode, mobile, mPref.getUserEmailId(), "", null);
            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    // WS - Generate Paytm Checksum
    private void wsGeneratePaytmChecksum(String userId, String sessionId, String txnAmount, String email, String mobileNo) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestGeneratePaytmChecksum request = new RequestGeneratePaytmChecksum(String.valueOf(userId), sessionId, txnAmount, email, mobileNo);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackGeneratePaytmChecksum> call = tnAPI.GeneratePaytmChecksum(request);
            call.enqueue(new Callback<CallbackGeneratePaytmChecksum>() {
                @Override
                public void onResponse(@NonNull Call<CallbackGeneratePaytmChecksum> call, @NonNull Response<CallbackGeneratePaytmChecksum> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                                String mid = response.body().data().merchantID();
                                String orderId = response.body().data().orderId();
                                String custId = response.body().data().custID();
                                String industryTypeId = response.body().data().industryTypeId();
                                String channelId = response.body().data().channelID();
                                String website = response.body().data().website();
                                String amount = response.body().data().txnAmount();
                                //String paytmChecksum = response.body().data().paytmChecksum();
                                String paytmChecksum = response.body().data().getCHECKSUMHASH();
                                String callbackUrl = response.body().data().callbackUrl();
                                String email = response.body().data().email();
                                String mobile = response.body().data().mobileNo();
                                boolean isPaytmLive = response.body().data().isPaytmLive();

                                startPaytm(mid, orderId, custId, industryTypeId, channelId, website, amount, paytmChecksum, callbackUrl, email, mobile, isPaytmLive);


                            } else if (Validate.isNotNull(response.body().getError_description())) {
                                CC.showAlert(response.body().getError_description());
                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        } else {
                            CC.showToast(R.string.msg_no_response);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CC.showToast(R.string.msg_something_wrong);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackGeneratePaytmChecksum> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                syncBookingHistory();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    //paytm payment
    private void startPaytm(String mid, String orderId, String custId, String industryTypeId,
                            String channelId, String website, String amount, String paytmChecksum,
                            String callbackUrl, String email, String mobile, boolean isPaytmLive) {

        try {
            HashMap<String, String> paramMap = new HashMap<>();
            paramMap.put("MID", mid);
            paramMap.put("ORDER_ID", orderId + "");
            paramMap.put("CUST_ID", custId + "");
            paramMap.put("INDUSTRY_TYPE_ID", industryTypeId);
            paramMap.put("CHANNEL_ID", channelId);
            paramMap.put("TXN_AMOUNT", amount);
            paramMap.put("WEBSITE", website);
            paramMap.put("CALLBACK_URL", callbackUrl);
            paramMap.put("EMAIL_CODE", email);
            paramMap.put("MOBILE_NO_CODE", mobile);
            paramMap.put("CHECKSUMHASH", paytmChecksum);

            PaytmOrder Order = new PaytmOrder(paramMap);
            PaytmPGService service = PaytmPGService.getStagingService();
            if (isPaytmLive) service = PaytmPGService.getProductionService();

            service.enableLog(this);
            service.initialize(Order, null);
            service.startPaymentTransaction(this, true, false, new PaytmPaymentTransactionCallback() {
                @Override
                public void onTransactionResponse(Bundle inResponse) {
                    Log.e(TAG, "Paytm Payment Transaction : " + inResponse);
                    //Toast.makeText(getApplicationContext(), "Payment Transaction response " + inResponse.toString(), Toast.LENGTH_LONG).show();

                    String status = inResponse.getString("STATUS", "");
                    if (status.equalsIgnoreCase("TXN_SUCCESS")) {


                   /* String txnamnt = inResponse.getString("TXNAMOUNT", "");
                    long userId = mLogin.getUserId();
                    String txndate = inResponse.getString("TXNDATE", "");
                    String MID = inResponse.getString("MID", "");
                    String respcode = inResponse.getString("RESPCODE", "");
                    String bankName = inResponse.getString("BANKNAME", "");
                    String paymentmode = inResponse.getString("PAYMENTMODE", "");
                    String banktxnid = inResponse.getString("BANKTXNID", "");
                    String currency = inResponse.getString("CURRENCY", "INR");
                    String gatwayname = inResponse.getString("GATEWAYNAME", "");
                    String IS_CHECKSUM_VALID = inResponse.getString("IS_CHECKSUM_VALID", "Y");
                    String respmsg = inResponse.getString("RESPMSG", "");*/

                        //String payStatus = Constant.PAYMENT_SUCCESS_TITLE;
                        String orderId = inResponse.getString("ORDERID", "");
                        String txnid = inResponse.getString("TXNID", "");

                        if (CC.isOnline()) {
                            String countryCode = mPref.getCountryCode();
                            String mobile = mPref.getUserPhoneNumber();
                            try {
                                if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                                    Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile,
                                            Constant.DEFAULT_COUNTRY_REGION);
                                    countryCode = "+" + phoneNumber.getCountryCode();
                                    mobile = phoneNumber.getNationalNumber() + "";
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            wsInsertPlaceOrder(mLogin.getUserId(), eventSessionId, orderId, Constant.PaymentType.ONLINE,
                                    Constant.MerchantType.PAYTM, txnid, countryCode, mobile, email);
                       /* callPaymentRespondWS(userId, orderId, bankName, payStatus,
                                txnamnt, txndate, txnid, respcode, paymentmode,
                                banktxnid, currency, gatwayname, respmsg, status, IS_CHECKSUM_VALID);*/
                        } else {
                            CC.showAlert(R.string.payment_msg_success_no_internet,
                                    () -> {
                                        Intent intSuccess = new Intent(getApplicationContext(), LandingActivity.class);
                                        intSuccess.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intSuccess);
                                    });
                        }

                        // After successful transaction this method gets called.
                        // // Response bundle contains the merchant response
                        // parameters.
                        Log.e(TAG, "Paytm Payment Transaction is successful " + inResponse);
                    } else {
                    /*long userId = mLogin.getUserId();
                    String orderId = inResponse.getString("ORDERID", "");
                    String txnamnt = inResponse.getString("TXNAMOUNT", "");
                    String txndate = inResponse.getString("TXNDATE", "");
                    String MID = inResponse.getString("MID", "");
                    String respcode = inResponse.getString("RESPCODE", "");
                    String bankName = inResponse.getString("BANKNAME", "");
                    String paymentmode = inResponse.getString("PAYMENTMODE", "");
                    String txnid = inResponse.getString("TXNID", "");
                    String banktxnid = inResponse.getString("BANKTXNID", "");
                    String currency = inResponse.getString("CURRENCY", "INR");
                    String gatwayname = inResponse.getString("GATEWAYNAME", "");
                    String IS_CHECKSUM_VALID = inResponse.getString("IS_CHECKSUM_VALID", "Y");*/

                        // String payStatus = Constant.PAYMENT_FAILED_TITLE;
                        String respMsg = inResponse.getString("RESPMSG", "");

                        if (Validate.isNotNull(respMsg)) {
                            CC.showAlert(respMsg);
                        }

                       /* if (status.equalsIgnoreCase("PENDING")) {
                            payStatus = Constant.PAYMENT_PENDING_TITLE;
                        }*/

                    /*callPaymentRespondWS(userId, orderId, bankName, payStatus,
                            txnamnt, txndate, txnid, respcode, paymentmode,
                            banktxnid, currency, gatwayname, respmsg, status, IS_CHECKSUM_VALID);*/

                        //Log.e(TAG, "Payment Transaction Failed " + inErrorMessage);
                        Log.e(TAG, "Paytm Payment Transaction Failed " + inResponse.toString());
                    }
                }

                @Override
                public void networkNotAvailable() {
                    Log.e(TAG, "Paytm networkNotAvailable ");
                    CC.showToast(R.string.payment_msg_no_internet);
                }

                @Override
                public void clientAuthenticationFailed(String inErrorMessage) {
                    Log.e(TAG, "Paytm clientAuthenticationFailed " + inErrorMessage);
                    CC.showToast(R.string.payment_msg_error);
                }

                @Override
                public void someUIErrorOccurred(String inErrorMessage) {
                    Log.e(TAG, "Paytm someUIErrorOccurred = " + inErrorMessage);
                    CC.showToast(R.string.payment_msg_error);
                }

                @Override
                public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                    Log.e(TAG, "Paytm onErrorLoadingWebPage " + iniErrorCode);
                    Log.e(TAG, "Paytm onErrorLoadingWebPage " + inErrorMessage);
                    Log.e(TAG, "Paytm onErrorLoadingWebPage " + inFailingUrl);
                    CC.showToast(R.string.payment_msg_error);
                }

                @Override
                public void onBackPressedCancelTransaction() {
                    Log.e(TAG, "Paytm onBackPressedCancelTransaction");
                    //callPaymentStatusUpdateWS(data.getOrderId(), Constant.PAYMENT_CANCELED_TITLE);
                    CC.showToast(R.string.payment_msg_cancel);
                }

                @Override
                public void onTransactionCancel(String s, Bundle bundle) {
                    Log.e(TAG, "Paytm onTransactionCancel");
                    //callPaymentStatusUpdateWS(data.getOrderId(), Constant.PAYMENT_CANCELED_TITLE);
                    CC.showToast(R.string.payment_msg_cancel);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    //add by priyanka
    @SuppressWarnings("deprecation")
    private void wsUserInfo(String sessionId, String User_Info_List) {
        Log.e(TAG, "sessionId: " + sessionId);
        Log.e(TAG, "user_identification_list: " + User_Info_List);
        if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

  /*{"User_Identification_Data":{
            "session_id":"a9403d21a9df46d79059c3a684d88528",
                    "User_Info_List":[{
                "Full_Name":"zen",
                        "gender":"Male",
                        "Birth_Date":"2018-09-27",
                        "Address":"add",
                        "city":"Ahmedabad",
                        "Photo_Id":"",
                        "Organization_Name":"",
                        "mobileno":"",
                        "email":"",
                        "Id_Type":"",
                        "Id_Number":""
            }]
        }
        }*/

        try {
            //JSONObject jo = new JSONObject();
            JSONObject jsonObject = new JSONObject();
            JSONArray jsonArray = new JSONArray(User_Info_List);
            jsonObject.put(Constant.ScreenExtras.SESSION_ID, sessionId);
            jsonObject.put(Constant.ScreenExtras.USER_INFO_DATA, jsonArray);
            //jo.put(Constant.ScreenExtras.USER_IDENTIFICATION_DATA, jsonObject);
            Log.e(TAG, "json = " + jsonObject.toString());

            RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<ResponseBody> call = tnAPI.userIdentificationData(body);
            call.enqueue(new Callback<ResponseBody>() {
                @SuppressWarnings("ConstantConditions")
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            String json = response.body().string();
                            Log.e(TAG, "json:" + json);
                        } else {
                            CC.showToast(R.string.msg_no_response);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CC.showToast(R.string.msg_something_wrong);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (JSONException e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }
    }


    //adding by priyanka
    private void checkPaymentSession(String eventSessionId, String name, String email, String mobile, String countryCode) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestCheckPaymentSession request = new RequestCheckPaymentSession();
            request.email = mLogin.getEmail();
            request.mobileno = mLogin.getMobile();
            request.user_id = String.valueOf(mLogin.getUserId());
            request.session_id = eventSessionId;

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<ResponseBody> call = tnAPI.checkPaymentSession(request);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {

                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        try {
                            JSONObject jo = new JSONObject(Objects.requireNonNull(response.body()).string());
                            // JSONObject jo1 = jo.getJSONObject("Check_Payment_Session");
                            String code = jo.getString("error_code");

                            if (Validate.isNotNull(code)) {
                                if (code.equals("0")) {
                                    CC.showAlert(jo.getString("error_description"));
                                    return;
                                }
                            }

                          //startPayment(Amount_show * 100, eventSessionData.getEvent_name(), name, email, mobile, countryCode);
                            Log.e(TAG, "Amount_show: " + Amount_show);
                            Log.e(TAG, "eventSessionData: " + eventSessionData);

                            Intent intent = new Intent(OrderSummaryActivity.this,RazorPayOrderPlaceActivity.class);
                            intent.putExtra(Constant.ScreenExtras.EVENT_SESSION_ID,eventSessionId);
                            intent.putExtra(Constant.ScreenExtras.TICKET_SESSION_DATA,eventSessionData);
                            intent.putExtra(Constant.ScreenExtras.TYPE,"");
                            startActivity(intent);

                          // paytm();

                        } catch (JSONException | IOException e) {
                            e.printStackTrace();
                        }


                    } else {
                        CC.showToast(R.string.msg_something_wrong);
                    }

                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                }
            });
        }catch (Exception e){
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }
    }

    //adding by priyanka
    /*public void resetTimer(Context context) {
        try {
            if (timerServiceIntent != null) {
                context.stopService(timerServiceIntent);
                timerServiceIntent = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/


    private void dismissProgressDialog() {
        try{
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

}
