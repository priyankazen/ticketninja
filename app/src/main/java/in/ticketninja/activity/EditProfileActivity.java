/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.InputType;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import in.ticketninja.R;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.common.FilesUtils;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.PathUtil;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.widget.MyDatePickerDialog;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestRegister;
import in.ticketninja.ws.request.RequestUploadProfileImage;
import in.ticketninja.ws.response.CallbackRegister;
import in.ticketninja.ws.response.CallbackUploadProfileImage;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {

    //private final String TAG = EditProfileActivity.class.getSimpleName();
    private CommonClass CC;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;
    private PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    private boolean isAnyDataUpdated = false;

    private MyDatePickerDialog datePickerDialog;
    private SimpleDateFormat showDateFormat;

    private ConstraintLayout constraintLayout;
    private Button btn_update;
    private ImageView img_user;
    private RadioButton rb_male, rb_female;
    private EditText et_firstName, et_lastName, et_email, et_mobile_no, et_dateOfBirth;
    private EditText etCountryCode;

    private CircleImageView ivAccountPic;
    @Nullable
    private Uri mImageUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        try {
            initializeData();

            // ACTIONBAR
            setupActionBar();

            findViewById();

            setOnClickEvent();

            invalidateData();

            if (Validate.isNotNull(mLogin.getCountryCode())) {
                etCountryCode.setText(mLogin.getCountryCode());
            } else {
                Country country = Country.getCountryByISO(Constant.DEFAULT_COUNTRY_REGION);
                etCountryCode.setText(country.getDialCode());
            }

            setDateTimeField(et_dateOfBirth);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setOnClickEvent() {
        try {
            etCountryCode.setOnClickListener(v -> {
                try {
                    CountryPicker picker = CountryPicker.newInstance("Select country");  // dialog title
                    picker.setListener((name, code, dialCode, flagDrawableResID) -> {

                        etCountryCode.setText(dialCode);

                        Utility.hideKeyboard(EditProfileActivity.this, picker.getView());

                        picker.dismiss();
                    });
               /* picker.onCancel(new DialogInterface() {
                    @Override
                    public void cancel() {
                    }

                    @Override
                    public void dismiss() {
                        etCountryCode.requestFocus();
                        Utility.hideKeyboard(EditProfileActivity.this, etCountryCode);
                    }
                });*/
                    picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            });

            btn_update.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    if (checkValidation()) {
                        wsEditProfile();
                    }
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });

            img_user.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
                    builder.setTitle("Choose Image");
                    builder.setItems(new CharSequence[]{"Gallery", "Camera"},
                            (dialog, which) -> {
                                switch (which) {
                                    case 0:
                                        // GALLERY IMAGE
                                        if (ContextCompat.checkSelfPermission(EditProfileActivity.this,
                                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                            ActivityCompat.requestPermissions(EditProfileActivity.this,
                                                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                    Constant.RequestPermissions.GALLERY);
                                        } else {
                                            openGallery();
                                        }
                                        break;

                                    case 1:
                                        // CAMERA IMAGE
                                        if (ContextCompat.checkSelfPermission(EditProfileActivity.this,
                                                android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                                                || ContextCompat.checkSelfPermission(EditProfileActivity.this,
                                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                            ActivityCompat.requestPermissions(EditProfileActivity.this,
                                                    new String[]{android.Manifest.permission.CAMERA,
                                                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                                    Constant.RequestPermissions.CAMERA);
                                        } else {
                                            openCamera();
                                        }
                                        break;

                                    default:
                                        break;
                                }
                            });
                    builder.show();
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });

            et_dateOfBirth.setOnClickListener(v -> datePickerDialog.show());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        try {
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                //ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                tvTitle.setTypeface(tfMedium);
                Utility.convertToLowerCase(tvTitle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findViewById() {
        try {
            // TYPEFACE
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);

            constraintLayout = findViewById(R.id.constraintLayout);
            btn_update = findViewById(R.id.btn_update);
            rb_male = findViewById(R.id.rb_male);
            rb_female = findViewById(R.id.rb_female);

            et_firstName = findViewById(R.id.et_firstname);
            et_lastName = findViewById(R.id.et_lastname);
            et_email = findViewById(R.id.et_email);
            et_mobile_no = findViewById(R.id.et_mobile_no);
            et_dateOfBirth = findViewById(R.id.et_dateofbirth);
            et_dateOfBirth.setInputType(InputType.TYPE_NULL);
            img_user = findViewById(R.id.img_user);
            ivAccountPic = findViewById(R.id.ivAccountPic);
            etCountryCode = findViewById(R.id.etCountryCode);

            etCountryCode.setTypeface(tfMedium);
            et_firstName.setTypeface(tfMedium);
            et_lastName.setTypeface(tfMedium);
            et_email.setTypeface(tfMedium);
            et_mobile_no.setTypeface(tfMedium);
            et_dateOfBirth.setTypeface(tfMedium);
            rb_male.setTypeface(tfMedium);
            rb_female.setTypeface(tfMedium);


            Utility.setEditTextSingleLine(et_firstName);
            Utility.setEditTextSingleLine(et_lastName);
            Utility.setEditTextSingleLine(et_email);
            Utility.setEditTextSingleLine(et_mobile_no);
            Utility.setEditTextSingleLine(et_dateOfBirth);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            CC = new CommonClass(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void invalidateData() {
        try {
            if (Validate.isNotNull(mLogin.getPicture())) {
                Glide.with(this)
                        .load(mLogin.getPicture())
                        .centerCrop()
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                ivAccountPic.setImageDrawable(resource);
                                return true;
                            }
                        })
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .placeholder(R.drawable.ic_palceholder_edit_profile)
                        .error(R.drawable.ic_palceholder_edit_profile)
                        .into(ivAccountPic);
            } else {
                Glide.with(this)
                        .load(R.drawable.ic_palceholder_edit_profile)
                        .into(ivAccountPic);
            }

            //SimpleDateFormat dateFormatter = new SimpleDateFormat(DateTimeUtils.SERVER_FORMAT_DATE, Locale.getDefault());
            showDateFormat = new SimpleDateFormat(DateTimeUtils.SERVER_FORMAT_DATE, Locale.getDefault());

            et_firstName.setText(mLogin.getFirstName());
            et_lastName.setText(mLogin.getLastName());
            et_mobile_no.setText(mLogin.getMobile());
            et_email.setText(mLogin.getEmail());
            et_dateOfBirth.setText(mLogin.getDOB());

     /*   //rb_male.setSelected(false);
        rb_male.setChecked(false);
        //rb_female.setSelected(false);
        rb_female.setChecked(false);*/

            if (mLogin.getGender().equalsIgnoreCase(Constant.Gender.MALE)) {
                //rb_male.setSelected(true);
                rb_male.setChecked(true);
                //rb_female.setSelected(false);
                rb_female.setChecked(false);
            } else if (mLogin.getGender().equalsIgnoreCase(Constant.Gender.FEMALE)) {
                //rb_male.setSelected(false);
                rb_male.setChecked(false);
                //rb_female.setSelected(true);
                rb_female.setChecked(true);
            } else {
                //rb_male.setSelected(false);
                rb_female.setChecked(false);
                rb_male.setChecked(false);
                //rb_female.setSelected(false);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void openGallery() {
        try {
  /*Intent intImage = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intImage, Constant.ActivityForResult.GALLERY);*/
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent, "Choose Profile Image from"), Constant.ActivityForResult.GALLERY);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void openCamera() {
        try {

            // Define the file-categoryName to save photo taken by Camera activity
            String fileName = "TicketNinja_" + DateTimeUtils.getCurrentDateTimeMix() + ".jpg";

            // Create parameters for Intent with filename
            ContentValues values = new ContentValues();
            values.put(MediaStore.Images.Media.TITLE, fileName);
            values.put(MediaStore.Images.Media.DESCRIPTION, "Image captured by Camera on TicketNinja App.");
            // mImageUri is the current activity attribute, define and save it for later usage
            mImageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

            // Standard Intent action that can be sent to have the camera application capture an image and return it.
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
            startActivityForResult(intent, Constant.ActivityForResult.CAMERA);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        try {
            switch (requestCode) {
                case Constant.RequestPermissions.CAMERA:
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                            && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                        openCamera();
                    }
                    break;

                case Constant.RequestPermissions.GALLERY:
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        openGallery();
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        try {
            if (resultCode == Activity.RESULT_OK) {
                switch (requestCode) {
                    case Constant.ActivityForResult.CAMERA:
                        if (CC.isOnline()) {
                            Bitmap bitmap = decodeSampledBitmapFromCamera();
                            if (bitmap != null) {
                                String imageData = FilesUtils.bitmapToBase64(bitmap);
                                String type = Utility.getImageType(bitmap);
                                wsUploadProfileImage(mLogin.getUserId(), imageData, type);
                            }

                        } else {
                            CC.showToast(R.string.msg_no_internet);
                        }
                        break;

                    case Constant.ActivityForResult.GALLERY:
                        if (CC.isOnline()) {
                            if (data != null) {
                                mImageUri = data.getData();

                                Bitmap bm = Utility.getBitmap(PathUtil.getPath(this, mImageUri));
                                String type = Utility.getImageType(Objects.requireNonNull(bm));

                                new FilesUtils.GetBase64FromUri(this,
                                        imageData -> wsUploadProfileImage(mLogin.getUserId(), imageData, type))
                                        .execute(data.getData());
                            }
                        } else {
                            CC.showToast(R.string.msg_no_internet);
                        }
                        break;
                }
            } else {
                super.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private boolean checkValidation() {
        String email = et_email.getText().toString().trim();
        String countryCode = etCountryCode.getText().toString().trim();
        String mobile = et_mobile_no.getText().toString().trim();
        //String dob = et_dateOfBirth.getText().toString().trim();

        if (Validate.isNull(et_firstName.getText().toString().trim())) {
            CC.showToast(R.string.common_msg_enter_first_name);
            et_firstName.setFocusable(true);
            et_firstName.requestFocus();
            return false;

        } else if (Validate.isNull(et_lastName.getText().toString())) {
            CC.showToast(R.string.common_msg_enter_last_name);
            et_lastName.setFocusable(true);
            et_lastName.requestFocus();
            return false;

        } else if (Validate.isNull(email)) {
            CC.showToast(R.string.common_msg_enter_email);
            et_email.setFocusable(true);
            et_email.requestFocus();
            return false;

        } else if (!Validate.checkEmail(email)) {
            CC.showToast(R.string.common_msg_enter_email_valid);
            et_email.setFocusable(true);
            et_email.requestFocus();
            return false;

        } else if (Validate.isNull(countryCode)) {
            CC.showToast(R.string.common_msg_enter_country_code);
            et_mobile_no.setFocusable(true);
            et_mobile_no.requestFocus();
            return false;

        } else if (Validate.isNull(mobile)) {
            CC.showToast(R.string.common_msg_enter_mobile);
            et_mobile_no.setFocusable(true);
            et_mobile_no.requestFocus();
            return false;

        /*} else if (mobile.length() != 10) {
            CC.showToast(R.string.common_msg_enter_mobile_valid);
            et_mobile_no.setFocusable(true);
            et_mobile_no.requestFocus();
            return false;*/

        /*} else if (!isValidPhoneNumber(mobile)) {
            CC.showToast(R.string.common_msg_enter_mobile_valid);
            et_mobile_no.requestFocus();
            return false;*/

        } else if (!Validate.checkPhone_libPhoneNumber(countryCode, mobile)) {
            CC.showToast(R.string.common_msg_enter_mobile_valid);
            et_mobile_no.requestFocus();
            return false;

        /*} else if (Validate.isNull(dob)) {
            CC.showToast(R.string.common_msg_enter_birth_date);
            et_dateOfBirth.requestFocus();
            return false;

        } else if (!rb_female.isChecked() && !rb_male.isChecked()) {
            CC.showToast(R.string.common_msg_enter_gender);
            return false;*/

        } else {
            return true;
        }

    }

    private void setDateTimeField(final EditText etEditDOB) {
        try {
            final Calendar newCalendar = Calendar.getInstance();
            newCalendar.set(Calendar.HOUR_OF_DAY, 0);
            newCalendar.set(Calendar.MINUTE, 0);
            newCalendar.set(Calendar.SECOND, 0);

            String dobDefault = et_dateOfBirth.getText().toString().trim();
            if (Validate.isDateNotNull(dobDefault)) {
                // Date date = DateTimeUtils.stringToDate(dobDefault, DateTimeUtils.OUTPUT_FORMAT_DOB);
                Date date = DateTimeUtils.stringToDate(dobDefault, DateTimeUtils.SERVER_FORMAT_DATE);
                if (date != null) {
                    newCalendar.setTime(date);
                    newCalendar.set(Calendar.HOUR_OF_DAY, 0);
                    newCalendar.set(Calendar.MINUTE, 0);
                    newCalendar.set(Calendar.SECOND, 0);
                }
            }

            datePickerDialog = new MyDatePickerDialog(EditProfileActivity.this, R.style.datepicker,
                    (view, year, monthOfYear, dayOfMonth) -> {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);

                        etEditDOB.setText(showDateFormat.format(newDate.getTime()));
                    }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            Calendar calMax = Calendar.getInstance();
            calMax.add(Calendar.YEAR, -15);
            calMax.set(Calendar.HOUR_OF_DAY, 0);
            calMax.set(Calendar.MINUTE, 0);
            calMax.set(Calendar.SECOND, 0);
            datePickerDialog.getDatePicker().setMaxDate(calMax.getTimeInMillis());
            Calendar calMin = Calendar.getInstance();
            calMin.add(Calendar.YEAR, -90);
            calMin.set(Calendar.HOUR_OF_DAY, 0);
            calMin.set(Calendar.MINUTE, 0);
            calMin.set(Calendar.SECOND, 0);
            //DatePickerDialog.getDatePicker().mode(false);
            if (Build.VERSION_CODES.JELLY_BEAN_MR2 <= Build.VERSION.SDK_INT) {
                datePickerDialog.getDatePicker().setLayoutMode(1);
            }
            datePickerDialog.getDatePicker().setCalendarViewShown(false);
            datePickerDialog.getDatePicker().setMinDate(calMin.getTimeInMillis());
            datePickerDialog.setTitle(getResources().getString(R.string.select_date_of_birth));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // WS - Edit Profile
    @SuppressLint("WrongConstant")
    private void wsEditProfile() {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            String countryCode = etCountryCode.getText().toString().trim();
            String mobile = et_mobile_no.getText().toString().trim();

            try {
                if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                    Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile, Constant.DEFAULT_COUNTRY_REGION);
                    countryCode = "+" + phoneNumber.getCountryCode();
                    mobile = phoneNumber.getNationalNumber() + "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            RequestRegister registerClass = new RequestRegister();
            registerClass.firstname = et_firstName.getText().toString().trim();
            registerClass.lastname = et_lastName.getText().toString().trim();
            registerClass.email = et_email.getText().toString().trim();
            registerClass.country_code = countryCode;
            registerClass.mobileno = mobile;
            try {
                registerClass.birthdate = DOB();
            } catch (Exception e) {
                e.printStackTrace();
            }

            registerClass.user_id = String.valueOf(mLogin.getUserId());

            if (rb_male.isChecked()) {
                registerClass.gender = Constant.Gender.MALE;
            } else if (rb_female.isChecked()) {
                registerClass.gender = Constant.Gender.FEMALE;
            } else {
                registerClass.gender = "";
            }
            registerClass.device_token = PreferencesUtils.getFCMPushKey(EditProfileActivity.this);
            // registerClass.password = password;
            registerClass.mode = Constant.RegisterMode.UPDATE;

            // RequestRegister request = new RequestRegister(registerClass);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackRegister> call = tnAPI.registerEditProfile(registerClass);
            call.enqueue(new Callback<CallbackRegister>() {
                @Override
                public void onResponse(@NonNull Call<CallbackRegister> call, @NonNull Response<CallbackRegister> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            isAnyDataUpdated = true;
                            //if (response.body().data().isSuccess()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                mLogin.setFirstName(response.body().data().firstname);
                                mLogin.setLastName(response.body().data().lastname);
                                mLogin.setCountryCode(response.body().data().country_code);
                                mLogin.setMobile(response.body().data().mobileno);
                                mLogin.setGender(response.body().data().gender);
                                mLogin.setUserDOBWithParse(response.body().data().birthdate);
                                mLogin.setNotifications(response.body().data().notification_unread_count);
                                CC.showAlert(response.body().getError_description(), () -> onBackPressed());
                            } else if (Validate.isNotNull(response.body().getError_description())) {
                                CC.showAlert(response.body().getError_description());
                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        } else {
                            CC.showToast(R.string.msg_no_response);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CC.showToast(R.string.msg_something_wrong);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackRegister> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    // WS - UPLOAD PROFILE IMAGE
    private void wsUploadProfileImage(String userId, String imgStr, String imgType) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            String url = Utility.getImageUrlData(imgType).concat(imgStr);

            RequestUploadProfileImage request = new RequestUploadProfileImage(userId, url, ".jpg");
            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackUploadProfileImage> call = tnAPI.uploadProfileImage(request);
            call.enqueue(new Callback<CallbackUploadProfileImage>() {
                @Override
                public void onResponse(@NonNull Call<CallbackUploadProfileImage> call, @NonNull Response<CallbackUploadProfileImage> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            isAnyDataUpdated = true;
                            //if (response.body().data().isSuccess()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                mLogin.setPicture(Objects.requireNonNull(response.body()).data().getUserImageURL());
                                if (Validate.isNotNull(mLogin.getPicture())) {
                                    Glide.with(EditProfileActivity.this)
                                            .load(mLogin.getPicture())
                                            .centerCrop()
                                            .listener(new RequestListener<String, GlideDrawable>() {
                                                @Override
                                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                                    return false;
                                                }

                                                @Override
                                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                                    ivAccountPic.setImageDrawable(resource);
                                                    return true;
                                                }
                                            })
                                            .skipMemoryCache(false)
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .placeholder(R.drawable.ic_palceholder_edit_profile)
                                            .error(R.drawable.ic_palceholder_edit_profile)
                                            .into(ivAccountPic);
                                } else {
                                    Glide.with(EditProfileActivity.this)
                                            .load(R.drawable.ic_palceholder_edit_profile)
                                            .into(ivAccountPic);
                                }
                                MessageUtils.showSnackbar(constraintLayout, Objects.requireNonNull(response.body()).getError_description());

                            } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                                CC.showAlert(Objects.requireNonNull(response.body()).getError_description());

                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        } else {
                            CC.showToast(R.string.msg_no_response);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CC.showToast(R.string.msg_something_wrong);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackUploadProfileImage> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }


    }

    //...........................On Activity Result...............................//
    @Nullable
    public Bitmap decodeSampledBitmapFromCamera() {
        try {
            getContentResolver().notifyChange(Objects.requireNonNull(mImageUri), null);
            ContentResolver cr = getContentResolver();
            Bitmap bitmap = null;
            try {
                bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, mImageUri);
                if (bitmap == null) {
                    bitmap = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), mImageUri);
                }
                if (bitmap != null) {
                    //bitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, false);
                    bitmap = FilesUtils.scaleBitmap(bitmap);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return bitmap;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }


    }

  /*  public String DOB() {
        return DateTimeUtils.changeDateTimeFormat(et_dateOfBirth.getText().toString().trim(),
                DateTimeUtils.SERVER_FORMAT_DATE,
                DateTimeUtils.SERVER_FORMAT_DATE);
    }*/


    public String DOB() {
        return DateTimeUtils.changeDateTimeFormat(et_dateOfBirth.getText().toString().trim(),
                DateTimeUtils.OUTPUT_FORMAT_DOB,
                DateTimeUtils.SERVER_FORMAT_DATE);
    }

    @Override
    public void onBackPressed() {
        try {
            Utility.hideKeyboard(this);
            if (isAnyDataUpdated) {
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

}
