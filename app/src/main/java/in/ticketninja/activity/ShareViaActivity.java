/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

import in.ticketninja.R;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.UserCircleTransform;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.database.DatabaseHelper;
import in.ticketninja.widget.ColorGenerator;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.widget.TextDrawable;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestShareTicket;
import in.ticketninja.ws.request.RequestShareTicketEmailSMS;
import in.ticketninja.ws.response.CallbackShareTicket;
import in.ticketninja.ws.response.CallbackShareTicketEmailSMS;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShareViaActivity extends AppCompatActivity {

    private static final String TAG = ShareViaActivity.class.getSimpleName();
    private CommonClass CC;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;

    private EditText etCountryCode;
    private EditText etMobileNo;

    private EditText etEmail;

    private LinearLayout llContactInfo;
    private ImageView tvContactPic;
    private TextView tvContactName;
    private TextView tvContactData;
    private Button btnChangeContact;

    private List<String> contactList = new ArrayList<>();
    private int fromScreen = 0;
    private long transactionId;
    private String type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_share_via);

            initializeData();

            getIntentData();
            setupActionBar();

            LinearLayout llShareViaMobile = findViewById(R.id.llShareViaMobile);
            etCountryCode = findViewById(R.id.etCountryCode);
            etMobileNo = findViewById(R.id.etMobileNo);
            Button btnSendMobile = findViewById(R.id.btnSendMobile);
            btnSendMobile.setOnClickListener(view -> {
                Utility.hideKeyboard(this);
                if (checkMobileNumber()) {
                    wsShareTicketEmailSMS(type, transactionId, "", etMobileNo.getText().toString().trim());
                }
            });

            etCountryCode.setOnClickListener(v -> pickCountry());
            if (mLogin.isLoggedIn() && Validate.isNotNull(mLogin.getCountryCode())) {
                etCountryCode.setText(mLogin.getCountryCode());
            } else {
                Country country = Country.getCountryByISO(Constant.DEFAULT_COUNTRY_REGION);
                etCountryCode.setText(country.getDialCode());
            }

            // EMAIL_CODE ID
            LinearLayout llShareViaEmail = findViewById(R.id.llShareViaEmail);
            etEmail = findViewById(R.id.etEmail);
            Button btnSendEmail = findViewById(R.id.btnSendEmail);
            btnSendEmail.setOnClickListener(view -> {
                Utility.hideKeyboard(this);
                if (checkEmail()) {
                    // wsShareTicketEmailSMS(type, transactionId, etEmail.getText().toString().trim(), "");
                    wsShareTicket(mLogin.getUserId(), transactionId, etEmail.getText().toString().trim());
                }
            });

            // CONTACT
            LinearLayout llShareViaContact = findViewById(R.id.llShareViaContact);
            llContactInfo = findViewById(R.id.llContactInfo);
            tvContactPic = findViewById(R.id.tvContactPic);
            tvContactName = findViewById(R.id.tvContactName);
            tvContactData = findViewById(R.id.tvContactData);
            btnChangeContact = findViewById(R.id.btnChangeContact);
            btnChangeContact.setOnClickListener(v -> pickContact());
            Button btnSendContact = findViewById(R.id.btnSendContact);
            btnSendContact.setOnClickListener(view -> {
                Utility.hideKeyboard(this);
                if (Utility.isOnline(ShareViaActivity.this)) {
                    String data = tvContactData.getText().toString().trim();
                    if (Validate.isNotNull(data)) {
                        if (Validate.checkEmail(data)) {
                            // wsShareTicketEmailSMS(type, transactionId, data, "");
                            wsShareTicket(mLogin.getUserId(), transactionId, data);
                        } else if (isValidMobile(data)) {
                            String email = "";
                            wsShareTicketEmailSMS(type, transactionId, email, data);
                        } else {
                            CC.showToast(R.string.msg_select_valid_contact);
                        }
                    } else {
                        CC.showToast(R.string.msg_select_contact);
                    }
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });

            int shareVia = getIntent().getIntExtra(Constant.ScreenExtras.SHARE_VIA, 0);
            if (shareVia == Constant.ShareVia.MOBILE_NO) {
                llShareViaMobile.setVisibility(View.VISIBLE);

            } else if (shareVia == Constant.ShareVia.EMAIL) {
                llShareViaEmail.setVisibility(View.VISIBLE);

            } else if (shareVia == Constant.ShareVia.CONTACT) {
                llShareViaContact.setVisibility(View.VISIBLE);
                pickContact();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            CC = new CommonClass(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        try {

            fromScreen = getIntent().getIntExtra(Constant.ScreenExtras.FROM_SCREEN, 0);
            transactionId = getIntent().getLongExtra(Constant.ScreenExtras.TRAN_ID, 0);
            type = getIntent().getStringExtra(Constant.ScreenExtras.TYPE);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setupActionBar() {

        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Constant.RequestPermissions.READ_CONTACTS && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            pickContact();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constant.ActivityForResult.PICK_CONTACT && resultCode == RESULT_OK) {
            contactPicked(data);
        }
    }

    public void pickContact() {
        try {
            if ((ContextCompat.checkSelfPermission(ShareViaActivity.this,
                    Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)) {
                ActivityCompat.requestPermissions(ShareViaActivity.this,
                        new String[]{android.Manifest.permission.READ_CONTACTS},
                        Constant.RequestPermissions.READ_CONTACTS);
            } else {
                //Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Email.CONTENT_URI);
                startActivityForResult(contactPickerIntent, Constant.ActivityForResult.PICK_CONTACT);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void pickCountry() {
        try {
            CountryPicker picker = CountryPicker.newInstance("Select country");
            picker.setListener((name, code, dialCode, flagDrawableResID) -> {
                etCountryCode.setText(dialCode);
                Utility.hideKeyboard(ShareViaActivity.this, picker.getView());
                picker.dismiss();
            });
            picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDialogList() {
        try {
            AlertDialog.Builder adb = new AlertDialog.Builder(this);
            //CharSequence[] cs = contactList.toArray(new CharSequence[contactList.size()]);
            CharSequence[] cs = contactList.toArray(new CharSequence[0]);
            adb.setCancelable(false);
            //CharSequence items[] = new CharSequence[] {(CharSequence) contactList};
            adb.setItems(cs, (d, n) -> {
                tvContactData.setText(contactList.get(n));
                d.dismiss();
            });
            //adb.setNegativeButton(android.R.string.cancel, null);
            adb.setTitle("Which one?");
            adb.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void contactPicked(Intent data) {
        //ContentResolver cr = getContentResolver();
        Cursor cursor;
        try {
            Uri uri = data.getData();
            cursor = getContentResolver().query(Objects.requireNonNull(uri), null, null, null, null);
            Objects.requireNonNull(cursor).moveToFirst();

            // CONTACT ID
            String contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            Log.e(TAG, "Contact id = " + contactId);

            /*for (int i = 0; i < cursor.getColumnCount(); i++) {
                String ccc = cursor.getColumnName(i);
                String fff = cursor.getString(cursor.getColumnIndex(cursor.getColumnName(i)));
                Log.e(TAG, "My For " + ccc + " = " + fff);
            }*/

            // CONTACT NAME
            String contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            tvContactName.setText(Validate.isNotNull(contactName) ? contactName : "");
            Log.e(TAG, "Contact name = " + contactName);

            // CONTACT PHOTO
            String contactPhoto = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.PHOTO_URI));
            Log.e(TAG, "Contact Photo = " + contactPhoto);
            if (Validate.isNotNull(contactPhoto)) {
                Glide.with(this)
                        .load(Uri.parse(contactPhoto))
                        .transform(new UserCircleTransform(this))
                        .dontAnimate()
                        .into(tvContactPic);

            } else {
                ColorGenerator mColorGenerator = ColorGenerator.MATERIAL;
                TextDrawable drawable = TextDrawable.builder().buildRound(contactName.charAt(0) + "", mColorGenerator.getColor(contactName));
                tvContactPic.setImageDrawable(drawable);

            }

            contactList = new ArrayList<>();

            // MOBILE NUMBERS
            /*Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{contactId}, null);
            while (pCur.moveToNext()) {
                String mobileno = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                mobileno = mobileno.replaceAll(Constant.CONTACT_REPLACE_EXPRESSION, "");
                if (!contactList.contains(mobileno)) contactList.add(mobileno);
                Log.e(TAG, "Contact mobileno = " + mobileno);
            }
            pCur.close();*/

            // EMAIL_CODE IDs
            String email = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.Data.DATA1));
            Log.e(TAG, "Contact email = " + email);
            if (Validate.isNotNull(email)) {
                if (!contactList.contains(email)) contactList.add(email);
            }
            /*Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.email.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.email.CONTACT_ID + " = ?", new String[]{contactId}, null);
            while (emailCur.moveToNext()) {
                String email = emailCur.getString(
                        emailCur.getColumnIndex(ContactsContract.CommonDataKinds.email.ADDRESS_CODE)).toLowerCase();
                if (!contactList.contains(email)) contactList.add(email);
                Log.e(TAG, "Contact email = " + email);
            }
            emailCur.close();*/

            if (contactList.size() > 1) {
                showDialogList();
                tvContactData.setText("");
            } else {
                tvContactData.setText(TextUtils.join("\n", contactList));
                Log.e(TAG, "Contact = " + TextUtils.join("\n", contactList));
            }

            llContactInfo.setVisibility(View.VISIBLE);
            btnChangeContact.setText(R.string.btn_change_password);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // VALIDATION FOR MOBILE NUMBER
    private boolean checkMobileNumber() {
        try {
            String countryCode = etCountryCode.getText().toString().trim();
            String mobile = etMobileNo.getText().toString().trim();

            if (Validate.isNull(mobile)) {
                CC.showToast(R.string.login_msg_enter_mobile);
                etMobileNo.requestFocus();
                return false;

            } else if (!Validate.checkPhone_libPhoneNumber(countryCode, mobile)) {
                CC.showToast(R.string.common_msg_enter_mobile_valid);
                etMobileNo.requestFocus();
                return false;

            } else {

                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    // VALIDATION FOR EMAIL_CODE
    private boolean checkEmail() {
        try {
            //String countryCode = etCountryCode.getText().toString().trim();
            String email = etEmail.getText().toString().trim();

            if (Validate.isNull(email)) {
                CC.showToast(R.string.login_msg_enter_email);
                etEmail.requestFocus();
                return false;

            } else if (!Validate.checkEmail(email)) {
                CC.showToast(R.string.login_msg_enter_email_valid);
                etEmail.requestFocus();
                return false;

            } else {
                return true;
            }

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean isValidMobile(String phone) {
        boolean check = false;
        try {

            if (!Pattern.matches("[a-zA-Z]+", phone)) {
                if (phone.length() < 6 || phone.length() > 13) {
                    // if(mobileno.length() != 10) {
                    CC.showToast("Invalid Number ");
                    check = false;

                } else {
                    check = true;
                }
            } else {
                CC.showToast("Invalid Number ");
                check = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return check;
    }

    // WS - SHARE TICKET ON SMS EMAIL_CODE
    private void wsShareTicketEmailSMS(String type, long tranId, String email, String mobileNo) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestShareTicketEmailSMS request = new RequestShareTicketEmailSMS();
            request.email = email;
            request.mobileno = mobileNo;
            request.trans_id = tranId;
            request.type = type;

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackShareTicketEmailSMS> call = tnAPI.ShareTicketEmailSMS(request);
            call.enqueue(new Callback<CallbackShareTicketEmailSMS>() {
                @Override
                public void onResponse(@NonNull Call<CallbackShareTicketEmailSMS> call, @NonNull Response<CallbackShareTicketEmailSMS> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            CC.showToast(Objects.requireNonNull(response.body()).getError_description());
                            onBackPressed();

                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                            CC.showAlert(Objects.requireNonNull(response.body()).getError_description());

                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackShareTicketEmailSMS> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
        }


    }

    // WS - SHARE TICKET
    private void wsShareTicket(String userId, long tranId, String email) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestShareTicket request = new RequestShareTicket(userId, tranId, email);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackShareTicket> call = tnAPI.ShareTicket(request);
            call.enqueue(new Callback<CallbackShareTicket>() {
                @Override
                public void onResponse(@NonNull Call<CallbackShareTicket> call, @NonNull Response<CallbackShareTicket> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                            String status = Objects.requireNonNull(response.body()).data().status();
                            String shareTo = Objects.requireNonNull(response.body()).data().shareTo();

                            DatabaseHelper DB = new DatabaseHelper(ShareViaActivity.this);
                            if (fromScreen == Constant.ScreenExtras.FROM_ACTIVITY_BOOKING_HISTORY) {
                                DB.updateBookingTicketListStatusShared(tranId, mLogin.getUserId(), status, shareTo);
                            } else if (fromScreen == Constant.ScreenExtras.FROM_ACTIVITY_MY_TICKETS) {
                                DB.updateMyTicketListStatusShared(tranId, mLogin.getUserId(), status, shareTo);
                            }

                            CC.showToast(Objects.requireNonNull(response.body()).getError_description());

                            Intent returnIntent = new Intent();
                            setResult(Activity.RESULT_OK, returnIntent);
                            finish();

                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                            CC.showAlert(Objects.requireNonNull(response.body()).getError_description());

                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackShareTicket> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        try {
            Utility.hideKeyboard(this);
            super.onBackPressed();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
