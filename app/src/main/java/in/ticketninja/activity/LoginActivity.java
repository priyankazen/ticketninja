/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDialog;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;

//import com.crashlytics.android.Crashlytics;
//import com.crashlytics.android.answers.Answers;
//import com.crashlytics.android.answers.LoginEvent;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.common.CacheHelper;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.CustomTypefaceSpan;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.TextViewUtils;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.database.DatabaseHelper;
import in.ticketninja.objects.RequestSocialLogin;
import in.ticketninja.objects.UserCouponCode;
import in.ticketninja.objects.UserInfo;
import in.ticketninja.objects.User_Ticket_Event;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestLogin;
import in.ticketninja.ws.request.RequestUserTicket;
import in.ticketninja.ws.response.CallbackLogin;
import in.ticketninja.ws.response.CallbackSocialLogin;
import in.ticketninja.ws.response.CallbackUserTicket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressWarnings("deprecation")
public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = LoginActivity.class.getSimpleName();
    private CommonClass CC;
    private ProgressDialog mLoader;
    private EditText etEmail, etPassword;
    private ImageView ivActionBack;
    private CheckBox checkbox;
    private SignInButton btnGoogleSignIn;
    private Button btnLogin, btnForgotPassword;
    private TextView tvRegisterNow;
    private GoogleSignInClient mGoogleSignInClient;
    private CallbackManager callbackManager;
    private LoginResult mLoginResult;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private PreferencesUtils mPref;
    private DatabaseHelper DB;
    private boolean b = false;
    private String emailExtra = "";
    FirebaseCrashlytics crashlytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            getIntentData();

            if (b) {
                setContentView(R.layout.activity_login_temp);
            } else {
                setContentView(R.layout.activity_login);
            }


            initializeData();

            // [START config_signin]
            // Configure Google Sign In
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(getString(R.string.web_client_id))
                    .requestEmail()
                    .build();
            // [END config_signin]

            mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

            //FACEBOOK LOGIN
            setUpFacebookLogin();

            // Initialize FirebaseAuth
            initializeFireBaseAuth();

            try {
                LoginUtils mLogin = new LoginUtils(this);
                // FirebaseAnalytics
                FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
                mFirebaseAnalytics.setUserProperty("tn_user_id", mLogin.getUserId() + "");
                mFirebaseAnalytics.setUserProperty("tn_user_name", mLogin.getName() + "");
                mFirebaseAnalytics.setUserId(mLogin.getUserId() + "");
                // Crashlytics
                crashlytics.setUserId("" + mLogin.getUserId());
                //Crashlytics.setUserIdentifier("" + mLogin.getUserId());
               // Crashlytics.setUserEmail("" + mLogin.getEmail());
               // Crashlytics.setUserName("" + mLogin.getName());
            } catch (Exception e) {
                e.printStackTrace();
            }

            findViewById();
            setOnClickEvent();

            etEmail.setText(emailExtra);

            Typeface tfBold = ResourcesCompat.getFont(this, R.font.helvetica_neue_bold);
            SpannableString styledString = new SpannableString(getString(R.string.login_text_register_now));
            styledString.setSpan(new CustomTypefaceSpan("", tfBold), 23, 30, 0);
            styledString.setSpan(new ForegroundColorSpan(Color.parseColor("#4A4A4A")), 23, 30, 0);
            styledString.setSpan(new TextViewUtils.MyClickableSpan() {
                @Override
                public void onClick(@NonNull View widget) {
                    Utility.hideKeyboard(LoginActivity.this);
                /*if (CC.isOnline()) {
                    Intent intRegister = new Intent(getApplicationContext(), RegisterActivity.class);
                    startActivity(intRegister);
                    finish();
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }*/
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra(Constant.ScreenExtras.DATA, Constant.ScreenExtras.PRESS_SIGNUP);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();
                }
            }, 23, 30, 0);
            tvRegisterNow.setMovementMethod(LinkMovementMethod.getInstance());
            tvRegisterNow.setText(styledString);

        /*if (BuildConfig.DEBUG) {
            etEmail.setText("rohit@nichetech.in");
            etPassword.setText("123456");
        }*/

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initializeFireBaseAuth() {
        try {

            mAuth = FirebaseAuth.getInstance();
            mAuthListener = firebaseAuth -> {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.e(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.e(TAG, "onAuthStateChanged:signed_out");
                }
            };
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        try {
            if (getIntent().hasExtra(Constant.ScreenExtras.LOGIN_REQUIRED)) {
                b = getIntent().getBooleanExtra(Constant.ScreenExtras.LOGIN_REQUIRED, false);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.EMAIL)) {
                emailExtra = getIntent().getStringExtra(Constant.ScreenExtras.EMAIL);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            mPref = new PreferencesUtils(this);
            //LoginUtils mLogin1 = new LoginUtils(this);
            CC = new CommonClass(this);
            mLoader = new ProgressDialog(this);
            mLoader.setMessage(getString(R.string.msg_loading));
            mLoader.setCancelable(false);
            DB = new DatabaseHelper(this);
            crashlytics = FirebaseCrashlytics.getInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findViewById() {

        try {
            // TYPEFACE

            Typeface tfBold = ResourcesCompat.getFont(this, R.font.helvetica_neue_bold);
            Typeface tfRegular = TypefaceUtils.HelveticaRegular(this);
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);

            etEmail = findViewById(R.id.etLoginEmail);
            etEmail.setTypeface(tfMedium);
            etPassword = findViewById(R.id.etLoginPassword);
            etPassword.setTypeface(tfMedium);
            etPassword.setSelection(etPassword.getText().length());
            ivActionBack = findViewById(R.id.ivActionBack);
            checkbox = findViewById(R.id.checkBox);
            btnGoogleSignIn = findViewById(R.id.btnGoogleSignIn);
            btnLogin = findViewById(R.id.btnLogin);
            btnForgotPassword = findViewById(R.id.btnForgotPassword);
            btnForgotPassword.setTypeface(tfBold);
            tvRegisterNow = findViewById(R.id.tvRegisterNow);
            tvRegisterNow.setTypeface(tfRegular);

            if (b) {
                btnGoogleSignIn.setVisibility(View.GONE);
            }
            setButtonText(btnGoogleSignIn);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setOnClickEvent() {
        try {

            ivActionBack.setOnClickListener(v -> LoginActivity.this.onBackPressed());

            btnGoogleSignIn.setOnClickListener(view -> {
                if (!mLoader.isShowing() && !isFinishing()) mLoader.show();
                signInWithGoogle();
            });
            btnLogin.setOnClickListener(v -> {
                Utility.hideKeyboard(this);
                if (CC.isOnline()) {
                    if (checkValidation()) {
                        String email = etEmail.getText().toString().trim();
                        String password = etPassword.getText().toString().trim();
                        wsLogin(email, password);
                    }
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });


            btnForgotPassword.setOnClickListener(v -> {
                Utility.hideKeyboard(this);
                if (CC.isOnline()) {
                    String email = etEmail.getText().toString().trim() + "";
                    Intent intRegister = new Intent(getApplicationContext(), ForgotPasswordActivity.class);
                    intRegister.putExtra(Constant.ScreenExtras.DATA, email);
                    startActivity(intRegister);
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }

            });


            checkbox.setOnCheckedChangeListener((compoundButton, isChecked) -> {
                if (isChecked) {
                    // show password
                    Constant.showPassword(etPassword);
                } else {
                    // hide password
                    Constant.hidePassword(etPassword);
                }
            });

        /*tvRegisterNow.setOnClickListener(v -> {
            Utility.hideKeyboard(this);
            if (CC.isOnline()) {
                Intent intRegister = new Intent(getApplicationContext(), RegisterActivity.class);
                startActivity(intRegister);
            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        });*/


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setUpFacebookLogin() {
        try {
            int pxSidePadding = Utility.intToDP(LoginActivity.this, 4);
            int pxTopBottomPadding = Utility.intToDP(LoginActivity.this, 10);
            Drawable drawable = ContextCompat.getDrawable(this, R.drawable.ic_facebook);
            Utility.printFacebookHash(this);
            callbackManager = CallbackManager.Factory.create();
            LoginButton authFacebookSignIn = findViewById(R.id.authFacebookSignIn);
            authFacebookSignIn.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
            authFacebookSignIn.setCompoundDrawablePadding(0);
            authFacebookSignIn.setPadding(pxSidePadding, pxTopBottomPadding, pxSidePadding, pxTopBottomPadding);
            authFacebookSignIn.setReadPermissions(Arrays.asList("public_profile", "user_friends", "email", "user_birthday"));
            authFacebookSignIn.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                @Override
                public void onSuccess(LoginResult loginResult) {
                    mLoginResult = loginResult;
                    Log.e(TAG, "authFacebookSignIn:mLoginResult:" + mLoginResult);
                    if (Utility.isOnline(LoginActivity.this)) {
                        firebaseAuthWithFacebook(loginResult.getAccessToken());
                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                }

                @Override
                public void onCancel() {


                }

                @Override
                public void onError(FacebookException error) {
                    error.printStackTrace();

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @SuppressLint("SetTextI18n")
    protected void setButtonText(SignInButton signInButton) {
        try {
            for (int i = 0; i < signInButton.getChildCount(); i++) {
                View v = signInButton.getChildAt(i);

                // if the view is instance of TextView then change the text SignInButton
                if (v instanceof TextView) {
                    TextView tv = (TextView) v;
                    tv.setPadding(0, 0, 0, 0);
                    tv.setText("Continue with Google");
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void getFacebookInformation(AccessToken token) {
        // App code
        try {
            GraphRequest request = GraphRequest.newMeRequest(mLoginResult.getAccessToken(),
                    (user, response) -> {
                        if (user != null) {
                            try {

                                Log.e(TAG, "user==" + user.toString());
                                final String id = user.optString("id");
                                final String firstName = user.optString("first_name");
                                final String lastName = user.optString("last_name");
                                final String email = user.optString("email");

                                final String name = user.optString("name");
                                Log.e(TAG, "firebaseAuthWithFacebook" + "email:" + email + ", name:" + name + ", fName:" + firstName + ", lName:" + lastName + ", id:" + id);
                                String mn = "";
                                String bd = "";
                                if (Utility.isOnline(LoginActivity.this)) {
                                    socialLoginWSCall("", token.getUserId(), email, mn, name, bd);
                                } else {
                                    CC.showToast(R.string.msg_no_internet);
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });
            Bundle parameters = new Bundle();
            parameters.putString("fields", "id, first_name, last_name,name, email, gender, birthday, location");
            request.setParameters(parameters);
            request.executeAsync();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();
            Log.e(TAG, "firebaseAuthWithGooogleId:" + acct.getId());
            Log.e(TAG, "firebaseAuthWithGooogleIdToken:" + acct.getIdToken());
            AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(this, task -> {
                        Log.e(TAG, "firebaseAuthWithGoogle signInWithCredential:onComplete:" + task.isSuccessful());
                        if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "firebaseAuthWithGoogle signInWithCredential", task.getException());
                            CC.showAlert(R.string.msg_something_wrong, this::signoutFromFirebase);

                        } else {
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null) {
                                Log.e(TAG, "firebaseAuthWithGoogle user:" + user);
                                Log.e(TAG, "firebaseAuthWithGoogle DisplayName:" + user.getDisplayName());
                                String email;
                                if (!TextUtils.isEmpty(user.getEmail())) {
                                    email = user.getEmail();
                                } else {
                                    email = acct.getEmail();
                                }
                                Log.e(TAG, "firebaseAuthWithGoogle email:" + email);

                                if (Utility.isOnline(LoginActivity.this)) {
                                    socialLoginWSCall(acct.getId(), "", email, "", user.getDisplayName(), "");
                                } else {
                                    CC.showToast(R.string.msg_no_internet);
                                }
                            }
                        }
                    });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    private void firebaseAuthWithFacebook(AccessToken token) {
        try {
            Log.e(TAG, "firebaseAuthWithFacebook AccessToken:" + token);

            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();
            final AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
            mAuth.signInWithCredential(credential)
                    .addOnCompleteListener(this, task -> {
                        Log.e(TAG, "firebaseAuthWithFacebook signInWithCredential:onComplete:" + task.isSuccessful());

                        if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.e(TAG, "signInWithCredential", task.getException());
                            CC.showAlert(R.string.msg_something_wrong, this::signoutFromFirebase);
                        } else {
                            FirebaseUser user = mAuth.getCurrentUser();

                            if (user != null) {
                                Log.e(TAG, "firebaseAuthWithFacebook user : " + user);
                                Log.e(TAG, "firebaseAuthWithFacebook getEmail : " + user.getEmail());
                                if (TextUtils.isEmpty(user.getEmail())) {
                                    getFacebookInformation(token);
                                } else {
                                    if (Utility.isOnline(LoginActivity.this)) {
                                        socialLoginWSCall("", token.getUserId(), user.getEmail(), "", user.getDisplayName(), "");
                                    } else {
                                        CC.showToast(R.string.msg_no_internet);
                                    }
                                }

                            }

                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
        }

    }

    private void socialLoginWSCall(String googlId, String fbId, String email, String mobile, String displayName, String birthdate) {

        try {
            Log.e(TAG, "googlId : " + googlId);
            Log.e(TAG, "fbId : " + fbId);
            Log.e(TAG, "email : " + email);
            Log.e(TAG, "mobile : " + mobile);
            Log.e(TAG, "displayName : " + displayName);
            Log.e(TAG, "birthdate : " + birthdate);

            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();
            RequestSocialLogin request = new RequestSocialLogin(googlId, fbId, email, mobile, displayName, birthdate, PreferencesUtils.getFCMPushKey(LoginActivity.this));
            socialLoginWSCall2(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void socialLoginWSCall2(RequestSocialLogin request) {

        try {

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackSocialLogin> call = tnAPI.checkSocialLogin(request);
            call.enqueue(new Callback<CallbackSocialLogin>() {
                @Override
                public void onResponse(@NonNull Call<CallbackSocialLogin> call, @NonNull Response<CallbackSocialLogin> response) {
                    try {
                       // if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                        Log.e(TAG, "__socialLoginWSCall2 response: " + response);
                        Log.e(TAG, "__socialLoginWSCall2 message: " + response.message());
                        Log.e(TAG, "__socialLoginWSCall2 isSuccessful: " + response.isSuccessful());
                        Log.e(TAG, "__socialLoginWSCall2 isSuccess: " + Objects.requireNonNull(response.body()).getError_code());
                        wsGetSocialBookingHistory(Objects.requireNonNull(response.body()).data().user_id,request,response);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(@NonNull Call<CallbackSocialLogin> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    signoutFromFirebase();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    private void socialLoginResponse(RequestSocialLogin request,Response<CallbackSocialLogin> response){
        try {
            if (response.isSuccessful()) {
                if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                    CallbackSocialLogin.UserSocialLogin userInfo = Objects.requireNonNull(response.body()).data();
                    Log.e(TAG, "_: user_id: " + userInfo.user_id);
                    Log.e(TAG, "__response.body().data(): " + userInfo);
                    LoginUtils mLogin = new LoginUtils(LoginActivity.this);
                    mLogin.setUserId(userInfo.user_id);
                    mLogin.setFirstName(userInfo.firstname);
                    mLogin.setLastName(userInfo.lastname);
                    mLogin.setEmail(userInfo.email);
                    mLogin.setCountryCode(userInfo.country_code);
                    mLogin.setMobile(userInfo.mobileno);
                    mLogin.setGender(userInfo.gender);
                    mLogin.setPicture(userInfo.user_image_url);
                    mLogin.setUserDOBWithParse(userInfo.birthdate);
                    mLogin.setNotifications(userInfo.notification_unread_count);

                    //set Preferences
                    mPref.setUserEmailId(userInfo.email);
                    mPref.setUserPhoneNumber(userInfo.mobileno);
                    mPref.setCountryCode(userInfo.country_code);

                    try {
                        // FirebaseAnalytics
                        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
                        mFirebaseAnalytics.setUserProperty("tn_user_id", mLogin.getUserId() + "");
                        mFirebaseAnalytics.setUserProperty("tn_user_name", mLogin.getName() + "");
                        mFirebaseAnalytics.setUserId(mLogin.getUserId() + "");
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    try {
                        // Crashlytics
                        crashlytics.setUserId("" + mLogin.getUserId());
                        //Crashlytics.setUserIdentifier("" + mLogin.getUserId());
                        //Crashlytics.setUserEmail("" + mLogin.getEmail());
                        //Crashlytics.setUserName("" + mLogin.getName());

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    /*try {
                        if (Validate.isNotNull(request.google_id)) {
                            Answers.getInstance().logLogin(new LoginEvent().putMethod("Google Login").putSuccess(true));
                        } else if (Validate.isNotNull(request.facebook_id)) {
                            Answers.getInstance().logLogin(new LoginEvent().putMethod("Facebook Login").putSuccess(true));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }*/

                    Intent returnIntent = new Intent();
                    // returnIntent.putExtra("result", result);
                    setResult(Activity.RESULT_OK, returnIntent);
                    signoutFromFirebase();
                    finish();

                } else {
                    if (Objects.requireNonNull(response.body()).data().is_email_required) {
                        showCustomDialog(LoginActivity.this, Objects.requireNonNull(response.body()).getError_description(), request);
                    } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                        CC.showAlert(Objects.requireNonNull(response.body()).getError_description());
                    } else {
                        signoutFromFirebase();
                        CC.showToast(R.string.msg_something_wrong);
                    }
                }

            } else {
                signoutFromFirebase();
                CC.showToast(R.string.msg_no_response);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void showCustomDialog(@NonNull Activity activity, String msg, RequestSocialLogin request) {
        try {
            Log.e(TAG, "msg : " + msg);

            @SuppressLint("InflateParams") View view = LayoutInflater.from(activity).inflate(R.layout.dialog_required_email_id, null, false);
            final AppCompatDialog alertDialog = new AppCompatDialog(activity);
//        alertDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
            alertDialog.supportRequestWindowFeature(Window.FEATURE_NO_TITLE);
            Objects.requireNonNull(alertDialog.getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.setContentView(view);
            alertDialog.setCancelable(false);
            alertDialog.setCanceledOnTouchOutside(false);

//        TextView tvErrorMessage = (TextView) view.findViewById(R.id.tvErrorMessage);
            final EditText etEmailId = view.findViewById(R.id.etEmailId);
            final Button btnNegative = view.findViewById(R.id.btnNegative);
            final Button btnPositive = view.findViewById(R.id.btnPositive);

            btnPositive.setOnClickListener(v -> {

                Utility.hideKeyboard(activity, etEmailId);
                if (Validate.isNotNull(etEmailId.getText().toString().trim())) {
                    request.email = etEmailId.getText().toString().trim();
                    socialLoginWSCall2(request);
                } else {
                    CC.showAlert(R.string.common_msg_enter_email_valid);
                }

            });
            btnNegative.setOnClickListener(v -> {
                Utility.hideKeyboard(activity, view);
                alertDialog.dismiss();
                signoutFromFirebase();
            });
            alertDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void signInWithGoogle() {
        try {
            Intent signInIntent = mGoogleSignInClient.getSignInIntent();
            if (Validate.isNotNull(signInIntent.getDataString()))
                CC.showAlert(signInIntent.getDataString());
            startActivityForResult(signInIntent, Constant.REQUEST_CODE_GOOGLE_SIGN_IN);
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            mAuth.addAuthStateListener(mAuthListener);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            if (mAuthListener != null) {
                mAuth.removeAuthStateListener(mAuthListener);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
        CC.showToast(R.string.msg_google_play_service_error);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
        try {
            Log.e(TAG, "requestCode : " + requestCode + ", resultCode: " + resultCode);
            // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
            if (requestCode == Constant.REQUEST_CODE_GOOGLE_SIGN_IN) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    GoogleSignInAccount account = task.getResult(ApiException.class);
                    Log.e(TAG, "SuccessFull : " + Objects.requireNonNull(account).getEmail());

                    if (Utility.isOnline(LoginActivity.this)) {
                        firebaseAuthWithGoogle(account);
                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                } catch (ApiException e) {

                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    e.printStackTrace();
//                CC.showAlert(e.getMessage() + "" + e.getLocalizedMessage());
                    // Google Sign In failed, update UI appropriately
                    Log.e(TAG, "Google sign in failed :", e);
                    // ...
                } catch (Exception e) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    e.printStackTrace();
                }
            } else {
                callbackManager.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboard(this);
       /* if (b && mLogin.isLoggedIn() ) {
            finishAffinity();
        }*/
        super.onBackPressed();
    }

    private boolean checkValidation() {

        try {
            String email = etEmail.getText().toString().trim();
            String password = etPassword.getText().toString().trim();

            if (Validate.isNull(email)) {
                CC.showToast(R.string.login_msg_enter_email);
                etEmail.requestFocus();
                return false;
            } else if (TextUtils.isDigitsOnly(etEmail.getText())) {
                Log.e(TAG, "No");
                if (email.length() < 10) {
                    CC.showToast(R.string.login_msg_enter_mobile_valid);
                    etPassword.requestFocus();
                    return false;
                }
                return true;
            } else if (!Validate.checkEmail(email)) {
                CC.showToast(R.string.login_msg_enter_email_valid);
                etEmail.requestFocus();
                return false;

            } else if (Validate.isNull(password)) {
                CC.showToast(R.string.login_msg_enter_password);
                etPassword.requestFocus();
                return false;

            } else if (password.length() < 6 || password.length() > 15) {
                CC.showToast(R.string.login_msg_enter_password_valid);
                etPassword.requestFocus();
                return false;

            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    // WS - Login
    private void wsLogin(String email, String password) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();
            RequestLogin request = new RequestLogin();
            request.email = email;
            request.password = password;
            request.device_token = PreferencesUtils.getFCMPushKey(LoginActivity.this);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackLogin> call = tnAPI.login(request);
            call.enqueue(new Callback<CallbackLogin>() {
                @Override
                public void onResponse(@NonNull Call<CallbackLogin> call, @NonNull Response<CallbackLogin> response) {
                    try {
                        //if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                        wsGetBookingHistory(Objects.requireNonNull(response.body()).data().user_id,response);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onFailure(@NonNull Call<CallbackLogin> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }
    }

    private void loginResponse(Response<CallbackLogin> response){
        try {
            if (response.isSuccessful()) {
                //if (response.body().data().isSuccess()) {
                if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                    DB.deleteDatabase1(LoginActivity.this);

                    // REMOVE CACHED DASHBOARD DATA
                    CacheHelper mCacheHelper = new CacheHelper(LoginActivity.this);
                    mCacheHelper.remove(CacheHelper.KEY_DASHBOARD);

                    UserInfo userInfo = Objects.requireNonNull(response.body()).data();
                    LoginUtils mLogin = new LoginUtils(LoginActivity.this);
                    mLogin.setUserId(userInfo.user_id);
                    mLogin.setFirstName(userInfo.firstname);
                    mLogin.setLastName(userInfo.lastname);
                    mLogin.setEmail(userInfo.email);
                    mLogin.setCountryCode(userInfo.country_code);
                    mLogin.setMobile(userInfo.mobileno);
                    mLogin.setGender(userInfo.gender);
                    mLogin.setPicture(userInfo.user_imageid_url);
                    mLogin.setNotifications(userInfo.notification_unread_count);
                    mPref.setUserEmailId(userInfo.email);
                    mPref.setUserPhoneNumber(userInfo.mobileno);
                    mPref.setCountryCode(userInfo.country_code);
                    mLogin.setUserDOBWithParse(userInfo.birthdate);

                    // FirebaseAnalytics
                    FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
                    mFirebaseAnalytics.setUserProperty("tn_user_id", mLogin.getUserId() + "");
                    mFirebaseAnalytics.setUserProperty("tn_user_name", mLogin.getName() + "");
                    mFirebaseAnalytics.setUserId(mLogin.getUserId() + "");

                    try {
                        // Crashlytics
                        crashlytics.setUserId("" + mLogin.getUserId());
                        //Crashlytics.setUserIdentifier("" + mLogin.getUserId());
                        //Crashlytics.setUserEmail("" + mLogin.getEmail());
                        //Crashlytics.setUserName("" + mLogin.getName());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                                /*try {
                                    Answers.getInstance().logLogin(new LoginEvent().putMethod("email").putSuccess(true));
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }*/

                    signoutFromFirebase();



                    Intent returnIntent = new Intent();
                    // returnIntent.putExtra("result", result);
                    setResult(Activity.RESULT_OK, returnIntent);
                    finish();

                } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                    CC.showAlert(Objects.requireNonNull(response.body()).getError_description());
                } else {
                    CC.showToast(R.string.msg_something_wrong);
                }
            } else {
                CC.showToast(R.string.msg_no_response);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void signoutFromFirebase() {
        try {
            FirebaseAuth.getInstance().signOut();
            LoginManager.getInstance().logOut();
            mGoogleSignInClient.signOut();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //..............................WS CALL FOR GET BOOKING HISTORY..........//
    private void wsGetSocialBookingHistory(String userId,RequestSocialLogin requestSocialLogin,Response<CallbackSocialLogin> socialLoginResponse) {

        try {
            RequestUserTicket request = new RequestUserTicket(userId);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackUserTicket> call = tnAPI.UserTicket(request);
            call.enqueue(new Callback<CallbackUserTicket>() {
                @Override
                public void onResponse(@NonNull Call<CallbackUserTicket> call, @NonNull Response<CallbackUserTicket> response) {
                    try {
                        if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                                try {
                                    if (DB.doesDatabaseExist(LoginActivity.this)) {
                                        DB.clearMyTicketTable(userId);
                                        DB.clearMyTicketListTable(userId);
                                    } else {
                                        DB.reCreteDatabase(LoginActivity.this);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                ArrayList<User_Ticket_Event> ticketList = Objects.requireNonNull(response.body()).data().userTicketEvents();
                                if (ticketList != null && ticketList.size() > 0) {
                                    for (int i = 0; i < ticketList.size(); i++) {
                                        DB.addMyTickets(ticketList.get(i), userId);
                                    }
                                    DB.startDownloadingMyTicketsInBackground();
                                    mPref.setUserTickets(LoginActivity.this, true);
                                }else {
                                    mPref.setUserTickets(LoginActivity.this, false);
                                }

                                ArrayList<UserCouponCode> list = response.body().data().getUser_coupon_code();
                                if (list != null && list.size() > 0) {
                                    mPref.setUserCode(LoginActivity.this, true);
                                } else {
                                    mPref.setUserCode(LoginActivity.this, false);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    socialLoginResponse(requestSocialLogin,socialLoginResponse);
                }

                @Override
                public void onFailure(@NonNull Call<CallbackUserTicket> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    t.printStackTrace();
                    socialLoginResponse(requestSocialLogin,socialLoginResponse);
                }
            });


        }catch (Exception e){
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
            socialLoginResponse(requestSocialLogin,socialLoginResponse);
        }

    }


    //..............................WS CALL FOR GET BOOKING HISTORY..........//
    private void wsGetBookingHistory(String userId,Response<CallbackLogin> loginResponse) {

        try {
            RequestUserTicket request = new RequestUserTicket(userId);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackUserTicket> call = tnAPI.UserTicket(request);
            call.enqueue(new Callback<CallbackUserTicket>() {
                @Override
                public void onResponse(@NonNull Call<CallbackUserTicket> call, @NonNull Response<CallbackUserTicket> response) {
                    try {
                        if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                                try {
                                    if (DB.doesDatabaseExist(LoginActivity.this)) {
                                        DB.clearMyTicketTable(userId);
                                        DB.clearMyTicketListTable(userId);
                                    } else {
                                        DB.reCreteDatabase(LoginActivity.this);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                ArrayList<User_Ticket_Event> ticketList = Objects.requireNonNull(response.body()).data().userTicketEvents();
                                if (ticketList != null && ticketList.size() > 0) {
                                    for (int i = 0; i < ticketList.size(); i++) {
                                        DB.addMyTickets(ticketList.get(i), userId);
                                    }
                                    DB.startDownloadingMyTicketsInBackground();
                                    mPref.setUserTickets(LoginActivity.this, true);
                                }else {
                                    mPref.setUserTickets(LoginActivity.this, false);
                                }

                                ArrayList<UserCouponCode> list = response.body().data().getUser_coupon_code();
                                if (list != null && list.size() > 0) {
                                    mPref.setUserCode(LoginActivity.this, true);
                                } else {
                                    mPref.setUserCode(LoginActivity.this, false);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    loginResponse(loginResponse);
                }

                @Override
                public void onFailure(@NonNull Call<CallbackUserTicket> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    t.printStackTrace();
                    loginResponse(loginResponse);
                }
            });


        }catch (Exception e){
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
            loginResponse(loginResponse);
        }

    }
}