/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.annotation.SuppressLint;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
//import com.crashlytics.android.answers.Answers;
//import com.crashlytics.android.answers.ContentViewEvent;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.EventArtistAdapter;
import in.ticketninja.adapters.EventGalleryAdapter;
import in.ticketninja.adapters.EventPromotionAdapter;
import in.ticketninja.adapters.EventShowInfoAdapter;
import in.ticketninja.adapters.EventTermsAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.EventArtist;
import in.ticketninja.objects.EventGalleryList;
import in.ticketninja.objects.EventPromotion;
import in.ticketninja.objects.MultipleDay;
import in.ticketninja.objects.MyEvent;
import in.ticketninja.objects.MyEventDetail;
import in.ticketninja.objects.MyEventTimeList;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestEventDetail;
import in.ticketninja.ws.response.CallbackEventDetail;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.GONE;

public class EventDetailActivity extends AppCompatActivity implements EventPromotionAdapter.EventPromotionClickEvent, EventGalleryAdapter.EventGalleryClickEvent {

    final Handler handler = new Handler();
    private final String TAG = EventDetailActivity.class.getSimpleName();
    private CommonClass CC;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;
    private TextView tvToolbarTitle;
    private ImageView ivActionBack;
    private ImageView ivActionShare;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private NestedScrollView nestedScrollView;
    private View llBoxesContainer, flEventDate, flEventTime, flEventType;
    private TextView tvTitle;
    private TextView tvShortDesc;
    private TextView tvVenueName;
    private TextView tvVenueAddress;
    private LinearLayout llEventPlan;
    private ImageView ivEventPlan;
    private TextView tvPriceRange;
    private TextView tvInviteTag;
    private Button btnEventBuyTicket;
    private View llEventBuyTicket;
    private TextView tvCategory;
    private TextView tvEventDate;
    private TextView tvEventTime;
    private TextView tvEventDuration;
    private TextView tvEventType;
    private ImageView ivEventTypeIcon;
    private ImageView ivPoster;
    private TextView tvLanguage;
    private LinearLayout llEventManagedBy;
    private RelativeLayout llEventManagedByName;
    private TextView tvEventManagedBy;
    private ImageView ivOrganizer;
    private LinearLayout llEventArtists;
    private LinearLayout llEventPromotionList;
    private LinearLayout llEventGalleryList;
    private TextView tvLabelArtists;
    private RecyclerView rvEventPromotion;
    private RecyclerView rvEventArtists;
    private LinearLayout llEventArtistDetail;
    private TextView tvArtistName;
    private TextView tvArtistDetail;
    private TextView tvLabelAbout;
    private LinearLayout llEventAbout;
    private TextView tvEventDetail;
    private WebView tvEventDetail1;
    private LinearLayout llEventTerms;
    private ImageView ivTermsArrow;
    private RecyclerView rvShowInfo;
    private RecyclerView rvEventTerms;
    private RecyclerView rvEventGallery;
    private int from_where = 0;
    private AppBarLayout appbar;
    private boolean isTermsExpand = false;
    private boolean isAppbarExpanded = true;
    private boolean isImageLoaded = false;
    private MyEventDetail myEventDetail;
    private ImageView ivEventPoster1;
    private RelativeLayout rlPlayVideo;
    private Uri uri;
    private LinearLayout llForm;
    private TextView tvCorpBooking, tvBrandAss, txtExc, tvLabelTerms;
    private View llTermsHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
            setContentView(R.layout.activity_event_detail);

            initializeData();

            uri = handleIntent();

            supportPostponeEnterTransition();

            getIntentData();

            // ACTIONBAR
            setupActionBar();

            findViewById();

            setOnClickEvent();

            if (myEventDetail != null) {
                /*try {
                    Answers.getInstance().logContentView(new ContentViewEvent()
                            .putContentName("" + myEventDetail.getEvent_name())
                            .putContentType("" + myEventDetail.getType())
                            .putContentId("" + myEventDetail.getId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                //if (Validate.isNotNull(myEventDetail.getImageId())) {
                if (Validate.isNotNull(myEventDetail.getDetailImageURL())) {
                    Glide.with(this)
                            .load(myEventDetail.getDetailImageURL())
                            //.centerCrop()
                            .skipMemoryCache(false)
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .listener(new RequestListener<String, GlideDrawable>() {
                                @Override
                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                    supportStartPostponedEnterTransition();
                                    return false;
                                }
                            })
                            .into(ivPoster);
                    isImageLoaded = true;
                }
                invalidateData(true);
                setUpAnalytics();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            CC = new CommonClass(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        try {
            if (getIntent() != null) {
                if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_DATA)) {
                    MyEvent event = (MyEvent) getIntent().getSerializableExtra(Constant.ScreenExtras.EVENT_DATA);
                    Log.e(TAG, "event = " + event);
                    if (event != null) {
                        myEventDetail = new MyEventDetail(event);
                    } else {
                        myEventDetail = new MyEventDetail();
                    }
                }

                from_where = getIntent().getIntExtra(Constant.ScreenExtras.FROM_SCREEN, 0);
                if (from_where == Constant.ScreenExtras.FROM_ACTIVITY_SERVICE) {
                    CC.showAlert(R.string.purchase_ticket_msg_session_timeout);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOnClickEvent() {
        try {
            tvLabelTerms.setOnClickListener(v -> {
                //Log.e(TAG, "tvLabelTerms == onClick");
                if (isTermsExpand) {
                    expandCollapseTerms(false);
                } else {
                    expandCollapseTerms(true);
                }
            });
            llTermsHeader.setOnClickListener(v -> {
                //Log.e(TAG, "llTermsHeader == onClick");
                if (isTermsExpand) {
                    expandCollapseTerms(false);
                } else {
                    expandCollapseTerms(true);
                }
            });


            tvCorpBooking.setOnClickListener(view -> {
                Log.e("Tag", "click");

                Intent i = new Intent(EventDetailActivity.this, CorporateBookingActivity.class);
                if (myEventDetail != null) {
                    i.putExtra(Constant.ScreenExtras.EVENT_ID, myEventDetail.getId());
                }
                startActivity(i);

            });

            tvBrandAss.setOnClickListener(view -> {
                Log.e("Tag", "click1");
                Intent i = new Intent(EventDetailActivity.this, BrandAssociateActivity.class);
                if (myEventDetail != null) {
                    i.putExtra(Constant.ScreenExtras.EVENT_ID, myEventDetail.getId());
                }
                startActivity(i);
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findViewById() {
        try {
            nestedScrollView = findViewById(R.id.nestedScrollView);
            rlPlayVideo = findViewById(R.id.rlPlayVideo);
            ivPoster = findViewById(R.id.ivEventPoster);
            ivEventPoster1 = findViewById(R.id.ivEventPoster1);
            tvTitle = findViewById(R.id.tvEventTitle);
            tvShortDesc = findViewById(R.id.tvEventShortDesc);
            tvVenueName = findViewById(R.id.tvEventVenue);
            tvVenueAddress = findViewById(R.id.tvEventAddress);
            llEventPlan = findViewById(R.id.llEventPlan);
            ivEventPlan = findViewById(R.id.ivEventPlan);
            tvPriceRange = findViewById(R.id.tvEventPriceRange);
            tvInviteTag = findViewById(R.id.tvInviteTag);
            btnEventBuyTicket = findViewById(R.id.btnEventBuyTicket);
            llEventBuyTicket = findViewById(R.id.llEventBuyTicket);
            llBoxesContainer = findViewById(R.id.llBoxesContainer);
            flEventDate = findViewById(R.id.flEventDate);
            flEventTime = findViewById(R.id.flEventTime);
            flEventType = findViewById(R.id.flEventType);
            tvEventDate = findViewById(R.id.tvEventDate);
            tvEventTime = findViewById(R.id.tvEventTime);
            tvEventDuration = findViewById(R.id.tvEventDuration);
            tvEventType = findViewById(R.id.tvEventType);
            ivEventTypeIcon = findViewById(R.id.ivEventTypeIcon);
            tvCategory = findViewById(R.id.tvEventCategory);
            tvLanguage = findViewById(R.id.tvEventLanguage);
            llEventManagedBy = findViewById(R.id.llEventManagedBy);
            llEventManagedByName = findViewById(R.id.llEventManagedByName);
            tvEventManagedBy = findViewById(R.id.tvEventManagedBy);
            ivOrganizer = findViewById(R.id.ivEventOrganizer);
            llEventGalleryList = findViewById(R.id.llEventGalleryList);
            llEventPromotionList = findViewById(R.id.llEventPromotionList);
            llEventArtists = findViewById(R.id.llEventArtists);
            tvLabelArtists = findViewById(R.id.tvLabelArtists);

            rvEventPromotion = findViewById(R.id.rvEventPromotion);
            rvEventPromotion.setHasFixedSize(true);
            rvEventPromotion.setNestedScrollingEnabled(false);

            rvEventGallery = findViewById(R.id.rvEventGallery);
            rvEventGallery.setHasFixedSize(true);
            rvEventGallery.setNestedScrollingEnabled(false);

            rvEventArtists = findViewById(R.id.rvEventArtists);
            rvEventArtists.setHasFixedSize(true);
            rvEventArtists.setNestedScrollingEnabled(false);
            llEventArtistDetail = findViewById(R.id.llEventArtistDetail);
            tvArtistName = findViewById(R.id.tvLabelArtistName);
            tvArtistDetail = findViewById(R.id.tvEventArtistDetail);
            llEventAbout = findViewById(R.id.llEventAbout);
            tvLabelAbout = findViewById(R.id.tvLabelAbout);
            tvEventDetail = findViewById(R.id.tvEventDetail);
            tvEventDetail1 = findViewById(R.id.tvEventDetail1);
            llEventTerms = findViewById(R.id.llEventTerms);

            llTermsHeader = findViewById(R.id.llTermsHeader);
            ivTermsArrow = findViewById(R.id.ivEventTermsArrow);
            tvLabelTerms = findViewById(R.id.tvLabelTerms);
            rvShowInfo = findViewById(R.id.rvShowInfo);
            rvShowInfo.setHasFixedSize(true);
            rvShowInfo.setNestedScrollingEnabled(false);
            rvEventTerms = findViewById(R.id.rvEventTerms);
            rvEventTerms.setHasFixedSize(true);
            rvEventTerms.setNestedScrollingEnabled(false);
            //adding by priyanka
            llForm = findViewById(R.id.llForm);
            tvCorpBooking = findViewById(R.id.tvCorpBooking);
            tvBrandAss = findViewById(R.id.tvBrandAss);
            txtExc = findViewById(R.id.txtExc);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                ivActionShare = toolbar.findViewById(R.id.ivActionShare);
                ivActionShare.setOnClickListener(v -> onBackPressed());
                tvToolbarTitle = toolbar.findViewById(R.id.tvTitle);
                tvToolbarTitle.setText(getTitle());
                Utility.convertToLowerCase(tvToolbarTitle);

                appbar = findViewById(R.id.appBarLayout);
                collapsingToolbarLayout = findViewById(R.id.collapsing_toolbar);
                collapsingToolbarLayout.setTitleEnabled(true);
                collapsingToolbarLayout.setExpandedTitleGravity(GravityCompat.START | Gravity.BOTTOM);
                collapsingToolbarLayout.setExpandedTitleColor(Color.WHITE);
                collapsingToolbarLayout.setCollapsedTitleTextColor(Color.WHITE);
                collapsingToolbarLayout.setTitle(getTitle());
                collapsingToolbarLayout.setContentScrimColor(Color.TRANSPARENT);
                collapsingToolbarLayout.setScrimAnimationDuration(1000);
                ViewCompat.setElevation(collapsingToolbarLayout, 10);
                setTitle(getTitle());
                appbar.setExpanded(true, false);
                ViewCompat.setElevation(appbar, 10);
                appbar.addOnOffsetChangedListener((appBarLayout, verticalOffset) -> {
                    boolean toolbarCollapsed = Math.abs(verticalOffset) >= appBarLayout.getTotalScrollRange();
                    ivActionBack.setImageResource(toolbarCollapsed ? R.drawable.ic_action_arrow_back : R.drawable.ic_action_arrow_back_white);
                    ivActionShare.setImageResource(toolbarCollapsed ? R.drawable.ic_action_share_red : R.drawable.ic_action_share);
                    tvToolbarTitle.setVisibility(toolbarCollapsed ? View.VISIBLE : View.GONE);
                    toolbar.setBackgroundColor(toolbarCollapsed ? Color.WHITE : Color.TRANSPARENT);
                    isAppbarExpanded = Math.abs(verticalOffset) != appBarLayout.getTotalScrollRange();
                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @SuppressWarnings("deprecation")
    private void invalidateData(boolean isApiAllow) {
        try {
            setTitle(Validate.isNotNull(myEventDetail.getEvent_name()) ? String.format("%s", myEventDetail.getEvent_name()) : "");
            tvToolbarTitle.setText(Validate.isNotNull(myEventDetail.getEvent_name()) ? String.format("%s", myEventDetail.getEvent_name()) : "");
            collapsingToolbarLayout.setTitle(Validate.isNotNull(myEventDetail.getEvent_name()) ? String.format("%s", myEventDetail.getEvent_name()) : "");
            tvTitle.setText(Validate.isNotNull(myEventDetail.getEvent_name()) ? String.format("%s", myEventDetail.getEvent_name()) : "");

            tvShortDesc.setText(Validate.isNotNull(myEventDetail.getShortDetail()) ? String.format("%s", myEventDetail.getShortDetail()) : "");
            tvShortDesc.setVisibility(Validate.isNotNull(myEventDetail.getShortDetail()) ? View.VISIBLE : View.GONE);
            tvVenueName.setText(Validate.isNotNull(myEventDetail.getVenueName()) ? String.format("%s", myEventDetail.getVenueName()) : "");
            tvVenueAddress.setText(Validate.isNotNull(myEventDetail.getVenueFullAddress()) ? String.format("%s", myEventDetail.getVenueFullAddress()) : "");

            if (Validate.isNotNull(myEventDetail.getDetailImageURL()) && !isImageLoaded) {
                Glide.with(this)
                        .load(myEventDetail.getDetailImageURL())
                        //.centerCrop()
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                supportStartPostponedEnterTransition();
                                return false;
                            }
                        })
                        .into(ivPoster);
                isImageLoaded = true;
            }
            if (Validate.isNotNull(myEventDetail.getVideoLink())) {

                rlPlayVideo.setVisibility(View.VISIBLE);
                ivEventPoster1.setOnClickListener(view -> {
//                    Log.e("VIDEO_CODE",myEventDetail.getVideoLink().substring(myEventDetail.getVideoLink().lastIndexOf("/"),
//                            myEventDetail.getVideoLink().length()));
                    startActivity(new Intent(EventDetailActivity.this, YouTubeActivity.class)
                            .putExtra(Constant.ScreenExtras.YOU_TUBE_CODE, myEventDetail.getVideoLink().substring(myEventDetail.getVideoLink().lastIndexOf("/") + 1
                            )));
                });
            } else {
                rlPlayVideo.setVisibility(View.GONE);
            }

            if (Validate.isNotNull(myEventDetail.getShareLinkData())) {
                ivActionShare.setVisibility(View.VISIBLE);
                ivActionShare.setOnClickListener(v -> CommonClass.shareEvent(this, myEventDetail.getId(), myEventDetail.getEvent_name(), myEventDetail.getShareLinkData()));
            } else {
                ivActionShare.setVisibility(View.INVISIBLE);
            }

            if (myEventDetail.getDayType().equalsIgnoreCase(Constant.EventDayType.SINGLE_DAY)) {
                flEventDate.setVisibility(View.VISIBLE);

                tvEventDate.setText(String.format("%s, %s", myEventDetail.getMDDate(), myEventDetail.getYear()));
                tvEventTime.setText(Validate.isNotNull(myEventDetail.getDay()) ? String.format("%s", myEventDetail.getDay()) : "");

                tvEventDuration.setText(String.format("%s %s", myEventDetail.getEvent_time(), Constant.ONWARDS));

            } else if (myEventDetail.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY) || myEventDetail.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW)) {
                flEventDate.setVisibility(View.VISIBLE);
                tvEventDate.setText(String.format("%s", myEventDetail.getMDDate1(Utility.EVENT_DETAIL)));
                // tvEventTime.setText(myEventDetail.getYear());
                tvEventDuration.setText(String.format("%s %s", myEventDetail.getEvent_time(), Constant.ONWARDS));

            } else if (myEventDetail.getDayType().equalsIgnoreCase(Constant.EventDayType.PERPETUAL)) {
                flEventDate.setVisibility(View.GONE);
                LinearLayout.LayoutParams params_label = (LinearLayout.LayoutParams) flEventTime.getLayoutParams();
                params_label.weight = 2f;
                flEventTime.setLayoutParams(params_label);
                flEventType.setLayoutParams(params_label);

                tvEventTime.setText("");
                tvEventDuration.setText(myEventDetail.getOpeningHours());

            } else {
                flEventDate.setVisibility(View.VISIBLE);
                tvEventDate.setText("");

                tvEventTime.setText("");
                tvEventDuration.setText("");
            }

            llBoxesContainer.setVisibility(View.VISIBLE);

            tvEventType.setText(Validate.isNotNull(myEventDetail.getType()) ? String.format("%s", myEventDetail.getType()) : "");
            if (Validate.isNotNull(myEventDetail.getType())) {
                tvLabelAbout.setText(String.format("About the %s", myEventDetail.getType()));
            }

            if (Validate.isNotNull(myEventDetail.getTypeIcon())) {
                Glide.with(this)
                        //.load(RestApi.ICON_RED_IMAGE + myEventDetail.getTypeIcon())
                        .load(myEventDetail.getTypeIcon())
                        .into(ivEventTypeIcon);
            } else {
                Glide.with(this).load(R.drawable.ic_event_detail_type).into(ivEventTypeIcon);
            }

            // btnEventBuyTicket.setText(myEventDetail.isRSVP() || myEventDetail.isInviteRSVP() ? R.string.event_btn_rsvp : R.string.event_btn_buy_ticket);
           /*
            if (myEventDetail.isInviteOnly() && myEventDetail.isInviteUsed()) {
                tvInviteTag.setText(R.string.event_list_invite_used_label);
                tvInviteTag.setVisibility(View.VISIBLE);
                tvInviteTag.setSelected(true);
                llEventBuyTicket.setEnabled(false);

            } else if (myEventDetail.isInviteOnly() && myEventDetail.isInviteAvailable()) {
                tvInviteTag.setText(R.string.event_list_invite_only_label);
                tvInviteTag.setVisibility(View.VISIBLE);
                tvInviteTag.setSelected(false);
                llEventBuyTicket.setEnabled(true);

            } else if (myEventDetail.isInviteOnly() && myEventDetail.isInviteLeft()) {
                tvInviteTag.setText(getString(R.string.event_list_invite_remaining_label, myEventDetail.getInviteStock()));
                tvInviteTag.setVisibility(View.VISIBLE);
                tvInviteTag.setSelected(false);
                llEventBuyTicket.setEnabled(true);

            } else {
                tvInviteTag.setText(R.string.event_list_invite_used_label);
                tvInviteTag.setVisibility(View.GONE);
                llEventBuyTicket.setEnabled(true);
            }*/

            btnEventBuyTicket.setText(Constant.Code.ONE == myEventDetail.isRSVP() || Constant.Code.ONE == myEventDetail.isInviteRSVP() ? R.string.event_btn_rsvp : R.string.event_btn_buy_ticket);
            if (Constant.Code.ONE == myEventDetail.isInviteOnly() && Constant.InviteStatus.USED.equals(myEventDetail.getInvite_status())) {
                tvInviteTag.setText(R.string.event_list_invite_used_label);
                tvInviteTag.setVisibility(View.VISIBLE);
                tvInviteTag.setSelected(true);
                llEventBuyTicket.setEnabled(false);

            } else if (Constant.Code.ONE == myEventDetail.isInviteOnly() && Constant.InviteStatus.AVAILABLE.equals(myEventDetail.getInvite_status())) {
                tvInviteTag.setText(R.string.event_list_invite_only_label);
                tvInviteTag.setVisibility(View.VISIBLE);
                tvInviteTag.setSelected(false);
                llEventBuyTicket.setEnabled(true);

            } else if (Constant.Code.ONE == myEventDetail.isInviteOnly() && Constant.InviteStatus.LEFT.equals(myEventDetail.getInvite_status())) {
                tvInviteTag.setText(getString(R.string.event_list_invite_remaining_label, myEventDetail.getInviteStock()));
                tvInviteTag.setVisibility(View.VISIBLE);
                tvInviteTag.setSelected(false);
                llEventBuyTicket.setEnabled(true);

            } else {
                tvInviteTag.setText(R.string.event_list_invite_used_label);
                tvInviteTag.setVisibility(View.GONE);
                llEventBuyTicket.setEnabled(true);
            }


            if (Validate.isNotNull(myEventDetail.getLatitude()) && Validate.isNotNull(myEventDetail.getLongitude())) {
                tvVenueName.setOnClickListener(v -> CommonClass.openMap(this, myEventDetail.getVenueName(), myEventDetail.getLatitude(), myEventDetail.getLongitude()));
                tvVenueAddress.setOnClickListener(v -> CommonClass.openMap(this, myEventDetail.getVenueName(), myEventDetail.getLatitude(), myEventDetail.getLongitude()));
            }

            Log.e(TAG, "click_myEventDetail = " + myEventDetail.getPriceCategory());
            if (myEventDetail.getPriceCategory() != null && myEventDetail.getPriceCategory().size() > 0 || myEventDetail.getDayType().equalsIgnoreCase(Constant.EventDayType.PERPETUAL)) {
                btnEventBuyTicket.setText(R.string.event_btn_buy_ticket);
                llEventBuyTicket.setBackgroundDrawable(getResources().getDrawable(R.drawable.btn_theme_bg_large_effect));
                llEventBuyTicket.setOnClickListener(v -> {
                    if (CC.isOnline()) {
                        // Log.e(TAG, "btn_buy_getDayType = " + myEventDetail.getDayType());
                        Log.e(TAG, "click_myEventDetail = " + myEventDetail.getExceptionDates().toString());
                        
                        if (myEventDetail.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY)
                                || myEventDetail.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW)) {
                            Intent intPricing = new Intent(getApplicationContext(), EventPricingMultidayActivity.class);
                            intPricing.putExtra(Constant.ScreenExtras.EVENT_DATA, myEventDetail);
                            startActivity(intPricing);
                        } else if (myEventDetail.getDayType().equalsIgnoreCase(Constant.EventDayType.PERPETUAL)) {
                            // convert into int
                            //int value = (int)myEventDetail.getAdvance_days();
                            Intent intPricing = new Intent(getApplicationContext(), SelectDateAndTimeActivity.class);
                            intPricing.putExtra(Constant.ScreenExtras.EVENT_ID, myEventDetail.getId());
                            intPricing.putExtra(Constant.ScreenExtras.EVENT_NAME, myEventDetail.getEvent_name());
                            intPricing.putExtra(Constant.ScreenExtras.EVENT_DISCOUNT_LABEL, myEventDetail.getDiscountLabel());
                            intPricing.putExtra(Constant.ScreenExtras.EVENT_DISCOUNT_PER, myEventDetail.getDiscountPer());
                            intPricing.putExtra(Constant.ScreenExtras.EVENT_ADVANCE_DAYS, myEventDetail.getAdvance_days());
                            //intPricing.putExtra(Constant.ScreenExtras.EVENT_ADVANCE_DAYS, value);
                            intPricing.putExtra(Constant.ScreenExtras.EVENT_TOTAL_TICKET, myEventDetail.getMax_Ticket_No());
                            intPricing.putExtra(Constant.ScreenExtras.HOURS, myEventDetail.getStop_sale_hours());
                            //intPricing.putStringArrayListExtra(Constant.ScreenExtras.EXCEPTION_DATES, myEventDetail.getExceptionDates());
                            intPricing.putExtra(Constant.ScreenExtras.EXCEPTION_DATES, myEventDetail);
                            intPricing.putExtra(Constant.ScreenExtras.MULTI_CATEGORY_SELECT, myEventDetail.getMulti_catergory_select());
                            startActivity(intPricing);
                        } else {
                            if (Constant.SVG_BLOCK.equals(myEventDetail.getLayout_type())){
                                Intent intent = new Intent(getApplicationContext(), SeatSelectionActivity.class);
                                intent.putExtra(Constant.ScreenExtras.EVENT_ID, myEventDetail.getId());
                                intent.putExtra(Constant.ScreenExtras.SHOW_ID, Long.parseLong("0"));
                                intent.putExtra(Constant.SVG_BLOCK, myEventDetail.getLayout_type());
                                intent.putExtra(Constant.ScreenExtras.EVENT_NAME, myEventDetail.getEvent_name());
                                intent.putExtra(Constant.ScreenExtras.EVENT_TIME, myEventDetail.getEvent_time());
                                intent.putExtra(Constant.ScreenExtras.EVENT_DATE, myEventDetail.getEvent_date());
                                intent.putExtra(Constant.ScreenExtras.EVENT_ADDRESS, myEventDetail.getVenueAddress());
                                intent.putExtra(Constant.ScreenExtras.EVENT_CITY, myEventDetail.getVenueCity());
                                startActivity(intent);
                                return;
                            }
                            Intent intPricing = new Intent(getApplicationContext(), EventPricingActivity1.class);
                            intPricing.putExtra(Constant.ScreenExtras.EVENT_DATA, myEventDetail);
                            startActivity(intPricing);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                });
            } else {
                if (Constant.Code.ONE == myEventDetail.getReg()) {
                    btnEventBuyTicket.setText(R.string.register);
                    llEventBuyTicket.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    llEventBuyTicket.setOnClickListener(v -> {
                        Intent intPricing = new Intent(getApplicationContext(), EventRegisterActivity.class);
                        intPricing.putExtra(Constant.ScreenExtras.EVENT_ID, myEventDetail.getId());
                        startActivity(intPricing);
                    });
                }
            }

                /*else {
            btnEventBuyTicket.setText(R.string.booking_open_soon);
            llEventBuyTicket.setBackgroundColor(getResources().getColor(R.color.button_bgColor_disabled));
        }*/

            if (Validate.isNotNull(myEventDetail.getPlan_image_url())) {
                Glide.with(this)
                        .load(myEventDetail.getPlan_image_url())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(ivEventPlan);
                llEventPlan.setVisibility(View.VISIBLE);
                llEventPlan.setOnClickListener(v -> {
                    startActivity(new Intent(this, FullScreenImageActivity.class)
                            .putExtra(Constant.ScreenExtras.IMAGE_URI, myEventDetail.getPlan_image_url())
                            .putExtra(Constant.ScreenExtras.KEY_IMAGE_TITLE, myEventDetail.getEvent_name()));

//                FullImageDialog dialog = new FullImageDialog(this);
//                dialog.setImageUrl(myEventDetail.getPlan_image_url());
//                dialog.show();
                });
            } else {
                llEventPlan.setVisibility(View.GONE);
                llEventPlan.setOnClickListener(null);
            }

           /* if (Validate.isNotNull(myEventDetail.getOrganizerName()) || Validate.isNotNull(myEventDetail.getOrganizerImage())) {
                tvEventManagedBy.setText(Validate.isNotNull(myEventDetail.getOrganizerName()) ? myEventDetail.getOrganizerName() : "");
                Glide.with(this)
                        .load(myEventDetail.getOrganizerImageUrl())
                        .into(ivOrganizer);
                llEventManagedByName.setOnClickListener(v -> {
                    if (Validate.isNotNull(myEventDetail.getOrganizerURL())) {
                        String geoUri = myEventDetail.getOrganizerURL();
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                        startActivity(intent);
                    }
                });
                llEventManagedBy.setVisibility(View.VISIBLE);
            } else {
                llEventManagedByName.setOnClickListener(null);
                llEventManagedBy.setVisibility(View.GONE);
            }*/

           /* if (Validate.isNotNull(myEventDetail.getOrg_site_url()) || Validate.isNotNull(myEventDetail.getOrganizerImageUrl())) {

                tvEventManagedBy.setText(Validate.isNotNull(myEventDetail.getOrg_site_url()) ? myEventDetail.getOrganizerName() : "");

                Glide.with(this)
                        .load(myEventDetail.getOrganizerImageUrl())
                        .into(ivOrganizer);

                llEventManagedByName.setOnClickListener(v -> {
                    if (Validate.isNotNull(myEventDetail.getOrg_site_url())) {
                        String geoUri = myEventDetail.getOrg_site_url();
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                        startActivity(intent);
                    }
                });
                llEventManagedBy.setVisibility(View.VISIBLE);
            } else {
                llEventManagedByName.setOnClickListener(null);
                llEventManagedBy.setVisibility(View.GONE);
            }*/

            //change  4th sept 2019
            if (Validate.isNotNull(myEventDetail.getOrg_site_url()) || Validate.isNotNull(myEventDetail.getOrganizerImageUrl()) || Validate.isNotNull(myEventDetail.getOrganizerName())) {
                llEventManagedBy.setVisibility(View.VISIBLE);
                llEventManagedByName.setVisibility(View.VISIBLE);
                tvEventManagedBy.setText(Validate.isNotNull(myEventDetail.getOrganizerName()) ? myEventDetail.getOrganizerName() : "");

                if (!TextUtils.isEmpty(myEventDetail.getOrganizerImageUrl().trim())) {
                    Glide.with(this)
                            .load(myEventDetail.getOrganizerImageUrl())
                            .into(ivOrganizer);
                }

                llEventManagedByName.setOnClickListener(v -> {
                    if (Validate.isNotNull(myEventDetail.getOrg_site_url())) {
                        String geoUri = myEventDetail.getOrg_site_url();
                        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
                        startActivity(intent);
                    }
                });

            } else {
                llEventManagedByName.setOnClickListener(null);
                llEventManagedBy.setVisibility(View.GONE);
            }


            double minPrice = myEventDetail.getRateFrom();//100
            double maxPrice = myEventDetail.getRateTo();//500
            if ((minPrice > 0 || maxPrice > 0) && minPrice == maxPrice) {
                tvPriceRange.setVisibility(View.VISIBLE);
                tvPriceRange.setText(Html.fromHtml(String.format(Locale.getDefault(), "₹ <font color=\"#484848\">%.2f</font>", minPrice)));
            } else if (minPrice > 0 || maxPrice > 0) {
                //Log.e(TAG,"minprice: "+ minPrice);
                //Log.e(TAG,"maxPrice: "+ maxPrice);
                tvPriceRange.setVisibility(View.VISIBLE);
                tvPriceRange.setText(Html.fromHtml(String.format(Locale.getDefault(), "₹ <font color=\"#484848\">%.2f</font>&nbsp;<font color=\"#484848\"> to</font> &nbsp;₹ <font color=\"#484848\">%.2f</font>", minPrice, maxPrice)));
            } else {
                tvPriceRange.setText("");
                tvPriceRange.setVisibility(GONE);
            }

            tvCategory.setText(Validate.isNotNull(myEventDetail.getCategory()) ? myEventDetail.getCategory() : "");
            tvCategory.setVisibility(Validate.isNotNull(myEventDetail.getCategory()) ? View.VISIBLE : View.GONE);
            tvLanguage.setText(Validate.isNotNull(myEventDetail.getLanguage()) ? myEventDetail.getLanguage() : "");
            tvLanguage.setVisibility(Validate.isNotNull(myEventDetail.getLanguage()) ? View.VISIBLE : View.GONE);

            if (myEventDetail.getEvent_promotion_list() != null && myEventDetail.getEvent_promotion_list().size() > 0) {
                for (int i = 0; i < myEventDetail.getEvent_promotion_list().size(); i++) {
                    EventPromotionAdapter eventPromotionAdapter = new EventPromotionAdapter(this, myEventDetail.getEvent_promotion_list());
                    eventPromotionAdapter.setClickEvent(this);
                    rvEventPromotion.setAdapter(eventPromotionAdapter);
                    llEventPromotionList.setVisibility(View.VISIBLE);
                }
            } else {
                llEventPromotionList.setVisibility(View.GONE);
            }

            if (myEventDetail.getEvent_gallery_list() != null && myEventDetail.getEvent_gallery_list().size() > 0) {
                for (int i = 0; i < myEventDetail.getEvent_gallery_list().size(); i++) {
                    EventGalleryAdapter eventPromotionAdapter = new EventGalleryAdapter(this, myEventDetail.getEvent_gallery_list());
                    eventPromotionAdapter.setClickEvent(this);
                    rvEventGallery.setAdapter(eventPromotionAdapter);
                    llEventGalleryList.setVisibility(View.VISIBLE);
                }
            } else {
                llEventGalleryList.setVisibility(View.GONE);
            }

            if (myEventDetail.getArtists() != null && myEventDetail.getArtists().size() > 0) {
                for (int i = 0; i < myEventDetail.getArtists().size(); i++) {
                    if (myEventDetail.getArtists().get(i).getShow_id() == 0) {
                        tvLabelArtists.setText(myEventDetail.getArtists().size() > 1 ? R.string.event_detail_organizer_artists : R.string.event_detail_organizer_artist);
                        EventArtistAdapter eventArtistAdapter = new EventArtistAdapter(this, myEventDetail.getArtists());
                        eventArtistAdapter.setOnItemClickListener((position, isChecked, artist) -> {
                            // expandCollapseArtist(isChecked, artist);
                            if (isChecked && artist != null) {
                                llEventArtistDetail.setVisibility(View.VISIBLE);
                                tvArtistName.setText(String.format("About %s", artist.name()));
                                tvArtistDetail.setText(artist.detail());
                                handler.postDelayed(() -> {
                                    if (isAppbarExpanded) collapseToolbar();
                                    tvArtistDetail.getParent().requestChildFocus(tvArtistDetail, tvArtistDetail);
                                }, 200);
                                rvEventArtists.smoothScrollToPosition(position);

                            } else {
                                llEventArtistDetail.setVisibility(View.GONE);
                                tvArtistName.setText("");
                                tvArtistDetail.setText("");
                            }

                        });
                        rvEventArtists.setAdapter(eventArtistAdapter);

                        llEventArtistDetail.setVisibility(View.GONE);
                        tvArtistName.setText("");
                        tvArtistDetail.setText("");

                        llEventArtists.setVisibility(View.VISIBLE);
                    } else {
                        llEventArtists.setVisibility(View.GONE);
                    }
                }
            } else {
                llEventArtists.setVisibility(View.GONE);
            }

            if (Validate.isNotNull(myEventDetail.getFullDetail())) {
                String eventDetail = myEventDetail.getFullDetail();
                tvEventDetail.setText(Html.fromHtml(eventDetail));
                tvEventDetail1.loadData(eventDetail, "text/html", null);
                llEventAbout.setVisibility(View.VISIBLE);
            } else {
                llEventAbout.setVisibility(View.GONE);
            }

            if (myEventDetail.getTermsConditions() != null && myEventDetail.getTermsConditions().size() > 0) {
                rvEventTerms.setAdapter(new EventTermsAdapter(myEventDetail.getTermsConditions()));
                llEventTerms.setVisibility(View.VISIBLE);
                expandCollapseTerms(false);
            } else {
                llEventTerms.setVisibility(View.GONE);
            }

            if (myEventDetail.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW)
                    && myEventDetail.getMultipleDayList().size() > 0) {
                ArrayList<MyEventTimeList> showList = new ArrayList<>();
                for (MultipleDay multipleDay : myEventDetail.getMultipleDayList()) {
                    //if (!multipleDay.isSeasonPass()) {
                    if (Constant.Code.ZERO == multipleDay.isSeasonPass()) {

                       /* for (myEventDetailTimeList multiTime : multipleDay.timeList()) {
                            if (Validate.isNotNull(multiTime.title())) {
                                multiTime.date(multipleDay.eventDate());
                                showList.add(multiTime);
                            }
                        }*/

                        //add by priyanka
                        MyEventTimeList timeList = new MyEventTimeList();
                        timeList.setDate(multipleDay.getEvent_date());
                        timeList.setTime(multipleDay.getTime());
                        timeList.setName(multipleDay.getShow_name());
                        timeList.setS_detail(multipleDay.getS_detail());
                        timeList.setF_detail(multipleDay.getF_detail());

                        List<EventArtist> artist_info = new ArrayList<>();
                        for (int i = 0; i < myEventDetail.getArtists().size(); i++) {
                            EventArtist eventArtist = myEventDetail.getArtists().get(i);
                            if (myEventDetail.getArtists().get(i).getShow_id() == multipleDay.id()) {
                                artist_info.add(eventArtist);
                            }
                        }
                        timeList.setArtist_info(artist_info);
                        showList.add(timeList);

                    }
                }

                rvShowInfo.setAdapter(new EventShowInfoAdapter(this, showList, Constant.Code.ONE == myEventDetail.isArtistFirst(), myEventDetail.getType()));
                rvShowInfo.setVisibility(View.VISIBLE);
            } else {
                rvShowInfo.setVisibility(View.GONE);
            }

            if (CC.isOnline() && isApiAllow) {
                wsEventDetail(myEventDetail.getId());
            }

            //adding by priyanka
            if (Constant.Code.ZERO == myEventDetail.isIs_co_booking() && Constant.Code.ZERO == myEventDetail.isIs_sp_inquiry()) {
                llForm.setVisibility(View.GONE);
            } else {
                llForm.setVisibility(View.VISIBLE);
            }

            if (Constant.Code.ONE == myEventDetail.isIs_sp_inquiry()) {
                tvBrandAss.setVisibility(View.VISIBLE);
            } else {
                tvBrandAss.setVisibility(View.GONE);
            }

            if (Constant.Code.ONE == myEventDetail.isIs_co_booking()) {
                tvCorpBooking.setVisibility(View.VISIBLE);
            } else {
                tvCorpBooking.setVisibility(View.GONE);
            }


            if (Constant.Code.ONE == myEventDetail.getIs_exclusive()) {
                Log.e("Tag", "exc: " + myEventDetail.getIs_exclusive() + ",name:" + myEventDetail.getEvent_name());
                txtExc.setVisibility(View.VISIBLE);
            } else {
                Log.e("Tag1", "exc1: " + myEventDetail.getIs_exclusive() + ",name:" + myEventDetail.getEvent_name());
                txtExc.setVisibility(GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void collapseToolbar() {
        appbar.setExpanded(false, true);
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        try {
            // Respond to the action bar's Up/Home button
            if (item.getItemId() == android.R.id.home) {
                supportFinishAfterTransition();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.onOptionsItemSelected(item);
    }

    /*private void expandCollapseArtist(boolean expand, EventArtist artist) {
        if (expand) {
            llEventArtistDetail.setVisibility(View.VISIBLE);
            tvArtistName.setText(String.format("About %s", artist.name()));
            tvArtistDetail.setText(artist.detail());
        } else {
            llEventArtistDetail.setVisibility(View.GONE);
            tvArtistName.setText("");
            tvArtistDetail.setText("");
        }
    }*/

    private void expandCollapseTerms(boolean expand) {
        try {
            if (expand || llEventTerms.getVisibility() != View.VISIBLE) {
                rvEventTerms.setVisibility(View.VISIBLE);
                //ivTermsArrow.animate().rotation(180).start();
                ivTermsArrow.animate().scaleY(-1f).start();
                //rvEventTerms.animate().scaleY(1f).start();
                //rvEventTerms.animate().translationY(-rvEventTerms.getHeight()).translationYBy(rvEventTerms.getHeight()).start();
                ivTermsArrow.setRotation(1800);
                isTermsExpand = true;
                nestedScrollView.post(() -> nestedScrollView.fullScroll(ScrollView.FOCUS_DOWN));
            } else {
                //rvEventTerms.animate().scaleY(0f).start();
                //rvEventTerms.animate().translationY(rvEventTerms.getHeight()).translationYBy(-rvEventTerms.getHeight()).start();
                rvEventTerms.setVisibility(View.GONE);
                ivTermsArrow.animate().scaleY(1f).start();
                //ivTermsArrow.animate().rotation(0).start();
                ivTermsArrow.setRotation(0);
                isTermsExpand = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // WS - Event Detail
    private void wsEventDetail(long eventId) {
        try {
            Log.e(TAG, "eventId: " + eventId);
            try {
                if (mLoader != null) {
                    if (!mLoader.isShowing() && !isFinishing()) mLoader.show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            supportPostponeEnterTransition();

            String userId;
            if (mLogin != null) {
                userId = mLogin.getUserId();
            } else {
                SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
                userId = pref.getString(LoginUtils.PREF_ID, "");
            }

            Log.e(TAG, "userId: " + userId);

            RequestEventDetail request = new RequestEventDetail(String.valueOf(userId), eventId);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackEventDetail> call = tnAPI.eventDetail(request);
            call.enqueue(new Callback<CallbackEventDetail>() {
                @Override
                public void onResponse(@NonNull Call<CallbackEventDetail> call, @NonNull Response<CallbackEventDetail> response) {
                    supportStartPostponedEnterTransition();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        //if (response.body().data().isSuccess()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            myEventDetail = Objects.requireNonNull(response.body()).data();

                            //adding by priyanka
                            if (myEventDetail.getPriceCategory() != null && myEventDetail.getPriceCategory().size() > 0) {
                                Log.e(TAG, "response_myEventDetail: " + myEventDetail);
                            } else {
                                btnEventBuyTicket.setText(R.string.booking_open_soon);
                                llEventBuyTicket.setBackgroundColor(getResources().getColor(R.color.button_bgColor_disabled));
                            }
                            if (myEventDetail != null) {
                                if (!isFinishing()) invalidateData(false);
                            }
                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                            CC.showToast(Objects.requireNonNull(response.body()).getError_description());
                            if (uri != null) {
                                openAct();
                            }
                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackEventDetail> call, @NonNull Throwable t) {
                    supportStartPostponedEnterTransition();
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        try {
            Log.e(TAG, "onBackPressed: from_where= " + from_where);

            if (uri != null) {
                openAct();
                return;
            }

            if (from_where == Constant.ScreenExtras.FROM_ACTIVITY_PAYMENT
                    || from_where == Constant.ScreenExtras.FROM_ACTIVITY_SERVICE
                    || from_where == Constant.ScreenExtras.FROM_ACTIVITY_NOTIFICATION) {
                Intent intent = new Intent(this, LandingActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            } else {
                // supportFinishAfterTransition();
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void openAct() {
        try {
            Intent intent = new Intent(this, LandingActivity.class);
            intent.putExtra("deepLink", true);
             /* TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
              stackBuilder.addNextIntentWithParentStack(intent);
              stackBuilder.startActivities();*/
            startActivity(intent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private Uri handleIntent() {
        try {
            // ATTENTION: This was auto-generated to handle app links.
            Intent appLinkIntent = getIntent();
            String appLinkAction = appLinkIntent.getAction();
            Log.e(TAG, "appLinkAction = " + appLinkAction);
            Uri appLinkData = appLinkIntent.getData();
            Log.e(TAG, "appLinkData = " + appLinkData);
            Log.e(TAG, "appLinkScheme = " + appLinkIntent.getScheme());

            if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null) {
                Log.e(TAG, "appLinkPath =" + appLinkData.getPath());
                String recipeId = appLinkData.getLastPathSegment();
                Log.e(TAG, "recipeId =" + recipeId);
                if (recipeId != null && !TextUtils.isEmpty(recipeId)) {
                    wsEventDetail(Long.parseLong(Objects.requireNonNull(recipeId)));
                }
            } else {
                if (getIntent().hasExtra(Constant.ScreenExtras.DEEP_LINK_URI)) {
                    String url = getIntent().getStringExtra(Constant.ScreenExtras.DEEP_LINK_URI);
                    Log.e(TAG, "url=" + url);
                    appLinkData = Uri.parse(url);
                    String recipeId = appLinkData.getLastPathSegment();
                    wsEventDetail(Long.parseLong(Objects.requireNonNull(recipeId)));
                }
            }
            return appLinkData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    @Override
    protected void onDestroy() {
        try {
            dismissProgressDialog();
            super.onDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void dismissProgressDialog() {
        try {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpAnalytics() {
        try {
            // FirebaseAnalytics
            FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
            //mFirebaseAnalytics.setUserProperty("tn_user_id", mLogin.getUserId() + "");
            // mFirebaseAnalytics.setUserProperty("tn_user_name", mLogin.getEvent_name() + "");
            // mFirebaseAnalytics.setUserId(mLogin.getUserId() + "");
            if (myEventDetail != null) {
                Log.e(TAG, "myEventDetailId =" + myEventDetail.getId() + ", " + myEventDetail.getEvent_name());
                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, "" + myEventDetail.getId());
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, myEventDetail.getEvent_name());
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "event");
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void bottomDialog(EventPromotion eventPromotion) {
        try {
            BottomSheetDialog mBottomSheetDialog = new BottomSheetDialog(this);
            @SuppressLint("InflateParams") View sheetView = getLayoutInflater().inflate(R.layout.dialog_bottom, null);
            mBottomSheetDialog.setContentView(sheetView);
            ImageView imgClose = sheetView.findViewById(R.id.imgClose);
            TextView txtClose = sheetView.findViewById(R.id.txtClose);
            TextView txtCopy = sheetView.findViewById(R.id.txtCopy);
            TextView txtCode = sheetView.findViewById(R.id.txtCode);
            TextView txtLCode = sheetView.findViewById(R.id.txtdPromoCode);
            TextView txtDes = sheetView.findViewById(R.id.txtdPromoDes);
            txtLCode.setText(String.format("Use code %s", eventPromotion.getPromo_code()));
            txtDes.setText(eventPromotion.getPromo_code_label());
            txtCode.setText(eventPromotion.getPromo_code());
            imgClose.setOnClickListener(view -> mBottomSheetDialog.dismiss());
            txtClose.setOnClickListener(view -> mBottomSheetDialog.dismiss());
            txtCopy.setOnClickListener(view -> {
                Log.e(TAG, "copy");
                Toast.makeText(this, "Copied Text", Toast.LENGTH_LONG).show();
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("Copied Text", txtCode.getText().toString());
                Objects.requireNonNull(clipboard).setPrimaryClip(clip);
            });
            mBottomSheetDialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void eventPromotionClickEvent(EventPromotion eventPromotion) {
        bottomDialog(eventPromotion);
    }

    @Override
    public void eventGalleryClickEvent(EventGalleryList eventGallery,int position) {
        try {
            Intent intent = new Intent(this, EventGalleryVpActivity.class);
            intent.putExtra(Constant.ScreenExtras.EVENT_GALLERY_DATA, myEventDetail.getEvent_gallery_list());
            intent.putExtra(Constant.ScreenExtras.POSITION, position);
            startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
