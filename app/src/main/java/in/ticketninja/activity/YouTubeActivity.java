/*
 * Copyright (c) 2018. Shreya Prajapati
 * Developed by Shreya Prajapati for NicheTech Computer Solutions Pvt. Ltd. use only.
 *
 * @author Shreya Prajapati
 */

package in.ticketninja.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.PlayerStyle;
import com.google.android.youtube.player.YouTubePlayerView;

import in.ticketninja.R;
import in.ticketninja.common.Config;
import in.ticketninja.common.Constant;

public class YouTubeActivity extends YouTubeBaseActivity implements
        YouTubePlayer.OnInitializedListener {

    private static final String TAG = YouTubeActivity.class.getSimpleName();
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    private  java.lang.String YOUTUBE_VIDEO_CODE ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);

            setContentView(R.layout.activity_youtube);
            if (getIntent().hasExtra(Constant.ScreenExtras.YOU_TUBE_CODE)){
                YOUTUBE_VIDEO_CODE=getIntent().getStringExtra(Constant.ScreenExtras.YOU_TUBE_CODE);
            }

            YouTubePlayerView youTubeView = findViewById(R.id.youtube_view);

            // Initializing video player with developer key
            youTubeView.initialize(Config.YOUTUBE_API_KEY, this);

        }catch (Exception e){
            e.printStackTrace();
        }

    }
 
    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider,
            YouTubeInitializationResult errorReason) {

        try {
            if (errorReason.isUserRecoverableError()) {
                errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
            } else {
                String errorMessage = String.format(
                        getString(R.string.error_player), errorReason.toString());
                Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }
 
    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider,
            YouTubePlayer player, boolean wasRestored) {

        try {
            if (!wasRestored) {

                // loadVideo() will auto play video
                Log.e(TAG,YOUTUBE_VIDEO_CODE);
                player.loadVideo(YOUTUBE_VIDEO_CODE);

                // Hiding player controls
                player.setPlayerStyle(PlayerStyle.DEFAULT);
                player.setShowFullscreenButton(false);
//            player.
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }
 
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        try {
            if (requestCode == RECOVERY_DIALOG_REQUEST) {
                // Retry initialization if user performed a recovery action
                getYouTubePlayerProvider().initialize(Config.YOUTUBE_API_KEY, this);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
 
    private YouTubePlayer.Provider getYouTubePlayerProvider() {
        return (YouTubePlayerView) findViewById(R.id.youtube_view);
    }

}