/*
 * Copyright (c) 2017. Aditi PArikh
 * Developed by Aditi Parikh for NicheTech Computer Solutions Pvt. Ltd..
 */

package in.ticketninja.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;
import androidx.core.view.ViewCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.FilesUtils;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.database.DatabaseHelper;
import in.ticketninja.fragments.MTicketBarcodePageFragment;
import in.ticketninja.fragments.MTicketPageFragment;
import in.ticketninja.objects.BookingHistory;
import in.ticketninja.objects.BookingTicketList;
import in.ticketninja.widget.DepthPageTransformer;
import in.ticketninja.widget.MyViewPager;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestDownloadBooking;
import in.ticketninja.ws.request.RequestGetBookingDetailForOnline;
import in.ticketninja.ws.request.RequestTransNo;
import in.ticketninja.ws.response.CallbackDownloadTicket;
import in.ticketninja.ws.response.CallbackGetBookingDetailForOnline;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MTicketsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = MTicketsActivity.class.getSimpleName();
    //private final String TAG = MTicketsActivity.class.getSimpleName();
    private CommonClass CC;
   // private static DatabaseHelper DB;
    private DatabaseHelper DB;
    private static ViewPager vpBarcode;
    private static View barcodeView;
    private static LoginUtils mLogin;
    public static List<BookingTicketList> ticketList = new ArrayList<>();
    private SRKLoaderDialog mLoader;
    private static MyViewPager viewpager_tickets;
    private BookingHistory bookingHistory = new BookingHistory();
    private PagerAdapter mTicketsAdapter;
    private PagerAdapter mBarcodeAdapter;
    private ImageView btnShare;
    private ImageView btnDownload;
    private TextView tvTicketId;
    private TextView tvBookingId;
    private TextView tvSeatNo;
    private TextView tv_ticket_nos;
    private TextView tv_barcode_nos;
    private ImageView iv_prev_nav,iv_barcode_prev_nav,iv_next_nav,iv_barcode_next_nav,ivCloseButton ;
    private long ticketTranId;
    private static ViewPager.OnPageChangeListener pageChangeListener, pageChange1Listener;
    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_m_ticket);

        try {
            initializeData();

            getIntentData();

            findViewById();

            // ACTIONBAR
            setupActionBar();

            setOnClickEvent();

            setUpViewPager();
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    private void setUpViewPager() {
        try {
            viewpager_tickets = findViewById(R.id.viewpager_tickets);
            vpBarcode = findViewById(R.id.vpBarcode);
            mTicketsAdapter = new ScreenSlideTicketPagerAdapter(getSupportFragmentManager());
            mBarcodeAdapter = new ScreenSlideBarcodePagerAdapter(getSupportFragmentManager());
            viewpager_tickets.setAdapter(mTicketsAdapter);
            vpBarcode.setAdapter(mBarcodeAdapter);

            //long transactionId = getIntent().getLongExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, 0);

            // openBarcode();

            viewpager_tickets.setPageTransformer(true, new DepthPageTransformer());
            vpBarcode.setPageTransformer(true, new DepthPageTransformer());

            pageChangeListener = new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrollStateChanged(int arg0) {
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                @Override
                public void onPageSelected(int position) {
                    vpBarcode.setCurrentItem(position);
                    vpBarcode.setSelected(true);
                    onPageChange(position);
                }
            };

            viewpager_tickets.addOnPageChangeListener(pageChangeListener);

            pageChange1Listener = new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrollStateChanged(int arg0) {
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                @Override
                public void onPageSelected(int position) {
                    viewpager_tickets.setCurrentItem(position);
                    viewpager_tickets.setSelected(true);
                    onPageChange(position);
                }
            };

            vpBarcode.addOnPageChangeListener(pageChange1Listener);

            viewpager_tickets.post(() -> pageChangeListener.onPageSelected(0));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        try {
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);

            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                //getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> {
                    if (barcodeView.getVisibility() != View.VISIBLE) {
                        onBackPressed();
                    }
                });
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(R.string.title_activity_m_ticket);
                Utility.convertToLowerCase(tvTitle);

                tvTitle.setTypeface(tfMedium);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
                findViewById(R.id.ivActionLogo).setVisibility(View.VISIBLE);
                if (Constant.Code.ONE == bookingHistory.isIs_live_ticket() || Constant.Code.ONE == bookingHistory.isIs_app_ticket()) {
                    ImageView ivActionRing = findViewById(R.id.ivActionRing);
                    ivActionRing.setVisibility(View.VISIBLE);
                    Utility.blinkBorder(ivActionRing);
                    btnDownload.setVisibility(View.INVISIBLE);
                    btnShare.setVisibility(View.INVISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOnClickEvent() {
        try {
            iv_next_nav.setOnClickListener(this);
            iv_prev_nav.setOnClickListener(this);
            iv_barcode_prev_nav.setOnClickListener(this);
            iv_barcode_next_nav.setOnClickListener(this);
            ivCloseButton.setOnClickListener(v -> closeBarcode());
            btnShare.setOnClickListener(view -> {
                if (barcodeView.getVisibility() != View.VISIBLE) {
                    if (CC.isOnline()) {
//                    if (mLogin.isLoggedIn()) {
                        Utility.openBottomSheetMenu(MTicketsActivity.this, Constant.ScreenExtras.FROM_ACTIVITY_BOOKING_HISTORY, ticketTranId, Constant.ScreenExtras.TICKET);
//                    }
                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                }
            });

            btnDownload.setOnClickListener(view -> {
                if (barcodeView.getVisibility() != View.VISIBLE) {
                    if (CC.isOnline()) {
                        if (ContextCompat.checkSelfPermission(MTicketsActivity.this,
                                android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                                || ContextCompat.checkSelfPermission(MTicketsActivity.this,
                                android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(MTicketsActivity.this,
                                    new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                                    Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE);
                        } else {
                            if (ticketList.size() > 0) {
                                wsDownloadTicket(ticketList.get(viewpager_tickets.getCurrentItem()).getTransId(),
                                        ticketList.get(viewpager_tickets.getCurrentItem()).ticketId());
                            }
                        }

                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findViewById() {
        try {
            // TYPEFACE
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);
            Typeface tfReguler = TypefaceUtils.HelveticaRegular(this);

            tv_ticket_nos = findViewById(R.id.tv_ticket_nos);
            tv_barcode_nos = findViewById(R.id.tv_barcode_nos);

            iv_prev_nav = findViewById(R.id.iv_prev_nav);
            iv_barcode_prev_nav = findViewById(R.id.iv_barcode_prev_nav);
            btnShare = findViewById(R.id.iv_share);
            btnDownload = findViewById(R.id.iv_download_nav);
            iv_next_nav = findViewById(R.id.iv_next_nav);
            iv_barcode_next_nav = findViewById(R.id.iv_barcode_next_nav);

            tv_ticket_nos.setTypeface(tfReguler);
            tv_barcode_nos.setTypeface(tfReguler);

            //Dialog View elements
            barcodeView = findViewById(R.id.barcodeView);
            TextView tvBookingIdLabel = findViewById(R.id.tvBookingIdLabel);
            tvBookingIdLabel.setTypeface(tfReguler);
            tvBookingId = findViewById(R.id.tvBookingId);
            tvBookingId.setTypeface(tfMedium);
            TextView tvTicketIdLabel = findViewById(R.id.tvTicketIdLabel);
            tvTicketIdLabel.setTypeface(tfReguler);
            tvTicketId = findViewById(R.id.tvTicketId);
            tvTicketId.setTypeface(tfMedium);
            tvSeatNo = findViewById(R.id.tvSeatNo);
            tvSeatNo.setTypeface(tfMedium);

            ivCloseButton = findViewById(R.id.ivBarcodeClose);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        try {
            if (getIntent().hasExtra(Constant.ScreenExtras.BOOKING_DATA)) {
                bookingHistory = (BookingHistory) getIntent().getSerializableExtra(Constant.ScreenExtras.BOOKING_DATA);
                Log.e(TAG, "if_bookingHistory=" + bookingHistory);
                ticketList = bookingHistory.getTicketList();
                Log.e(TAG, "if_bookingHistory=" + bookingHistory);
            } else {
                uri = handleIntent();
                if (uri != null) {
                    bookingHistory = new DatabaseHelper(this).getBookingHistoryByTransNo(uri.getLastPathSegment(), mLogin.getUserId());
                    Log.e(TAG, "else_bookingHistory=" + bookingHistory);
                    if (bookingHistory.getTicketList().size() > 0) {
                        ticketList = bookingHistory.getTicketList();
                    } else {
                        wsGetTransId(uri.getLastPathSegment());
                    }
                }

            }

            Log.e(TAG, "bookingHistory=" + bookingHistory);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            CC = new CommonClass(this);
            DB = new DatabaseHelper(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static BookingTicketList refreshCurrentTicket(DatabaseHelper DB) {
        try {
            if (ticketList != null && ticketList.size() > 0) {
                int pos = viewpager_tickets.getCurrentItem();

                BookingTicketList list = ticketList.get(pos);
                BookingTicketList ticket = DB.getBookingTicketListDataByTansId(null, list.trans_id, mLogin.getUserId());
                ticketList.set(pos, ticket);

                Objects.requireNonNull(viewpager_tickets.getAdapter()).notifyDataSetChanged();
                Objects.requireNonNull(vpBarcode.getAdapter()).notifyDataSetChanged();
                pageChangeListener.onPageSelected(pos);
                pageChange1Listener.onPageSelected(pos);
                return ticket;
            } else {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    private void onPageChange(int position) {
        try {
            if (ticketList != null && ticketList.size() > 0) {
                Log.e(TAG, "onPageChange ticketList: " + ticketList);
                BookingTicketList ticket = ticketList.get(position);
                ticketTranId = ticket.trans_id;
                tvBookingId.setText(String.valueOf(ticket.bookingId()));
                tvTicketId.setText(ticket.ticketId());
                tv_ticket_nos.setText(String.format(Locale.getDefault(), "%d/%d",
                        viewpager_tickets.getCurrentItem() + 1, mTicketsAdapter.getCount()));
                tv_barcode_nos.setText(String.format(Locale.getDefault(), "%d/%d",
                        vpBarcode.getCurrentItem() + 1, mBarcodeAdapter.getCount()));
                if (ticket.isStatusOpen()) {
                    btnShare.setEnabled(true);
                    btnShare.setAlpha(1f);
                } else {
                    btnShare.setEnabled(false);
                    btnShare.setAlpha(0.5f);
                }

                //tvSeatNo.setText(String.format("Seat No. : %s", ticket.seatNo()));
                //tvSeatNo.setVisibility(Validate.isNotNull(ticket.seatNo()) ? View.VISIBLE : View.INVISIBLE);

                if (ticket.seatType().equalsIgnoreCase(Constant.EVENT_TYPE_ATTRACTION)) {
                    tvSeatNo.setText("");
                    tvSeatNo.setVisibility(View.GONE);

                } else if (Validate.isNotNull(ticket.seatNo())) {
                    tvSeatNo.setText(String.format("Seat No. : %s", ticket.seatNo()));
                    tvSeatNo.setVisibility(View.VISIBLE);

                } else if (Validate.isNotNull(ticket.seatType())) {
                    tvSeatNo.setText(ticket.seatType());
                    tvSeatNo.setVisibility(View.VISIBLE);

                } else {
                    tvSeatNo.setText(Constant.SEAT_TYPE_FREE_SEATING);
                    tvSeatNo.setVisibility(View.VISIBLE);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @Override
    public void onClick(View v) {
        try {
            switch (v.getId()) {

                case R.id.iv_prev_nav:
                    if (barcodeView.getVisibility() != View.VISIBLE) {
                        prevTicketSlide();
                    }
                    break;

                case R.id.iv_next_nav:
                    if (barcodeView.getVisibility() != View.VISIBLE) {
                        nextTicketSlide();
                    }
                    break;

                case R.id.iv_barcode_prev_nav:
                    if (barcodeView.getVisibility() == View.VISIBLE) {
                        prevTicketSlide();
                    }
                    break;

                case R.id.iv_barcode_next_nav:
                    if (barcodeView.getVisibility() == View.VISIBLE) {
                        nextTicketSlide();
                    }
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void prevTicketSlide() {
        try {
            if (viewpager_tickets.getCurrentItem() != 0) {
                viewpager_tickets.setCurrentItem(viewpager_tickets.getCurrentItem() - 1);
                viewpager_tickets.setSelected(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void nextTicketSlide() {
        try {
            if (viewpager_tickets.getCurrentItem() != Objects.requireNonNull(viewpager_tickets.getAdapter()).getCount()) {
                viewpager_tickets.setCurrentItem(viewpager_tickets.getCurrentItem() + 1);
                viewpager_tickets.setSelected(true);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @SuppressWarnings("deprecation")
    private class ScreenSlideTicketPagerAdapter extends FragmentStatePagerAdapter {
        ScreenSlideTicketPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return MTicketPageFragment.create(MTicketsActivity.this, position, bookingHistory);
        }

        @Override
        public int getCount() {
            return ticketList.size();
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

    }

    @SuppressWarnings("deprecation")
    private class ScreenSlideBarcodePagerAdapter extends FragmentStatePagerAdapter {
        ScreenSlideBarcodePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return MTicketBarcodePageFragment.newInstance(position, ticketList.get(position));
        }

        @Override
        public int getCount() {
            return ticketList.size();
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

    }

    public static void openBarcode() {
        try {
            barcodeView.setVisibility(View.VISIBLE);
            viewpager_tickets.setPagingEnabled(false);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void closeBarcode() {
        try {
            barcodeView.setVisibility(View.GONE);
            viewpager_tickets.setPagingEnabled(true);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == Constant.ActivityForResult.SHARE_TICKETS
                    && resultCode == RESULT_OK) {
                try {
                    refreshCurrentTicket(DB);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        try {
            if (requestCode == Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    btnDownload.performClick();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    // WS - DOWNLOAD TICKET
    @RequiresPermission(allOf = {android.Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE})
    private void wsDownloadTicket(long transId, String bookingId) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestDownloadBooking request = new RequestDownloadBooking(transId);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackDownloadTicket> call = tnAPI.downloadTicket(request);
            call.enqueue(new Callback<CallbackDownloadTicket>() {
                @Override
                public void onResponse(@NonNull Call<CallbackDownloadTicket> call, @NonNull Response<CallbackDownloadTicket> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                            String imageData = Objects.requireNonNull(response.body()).data().getImageData();
                            File fileDir = new File(Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_PICTURES, "TicketNinja");
                            if (!fileDir.exists()) fileDir.mkdirs();
                            File file = FilesUtils.base64toAnyFile(MTicketsActivity.this, imageData, new File(fileDir, bookingId + ".png"));

                            FilesUtils.refreshGallery(MTicketsActivity.this, file);

                            CC.showAlert(R.string.booking_confirmation_download_success);

                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                            CC.showToast(Objects.requireNonNull(response.body()).getError_description());

                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackDownloadTicket> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                }
            });
        }catch (Exception e){
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    // WS - Get Booking Detail For Online
    private void wsGetTransId(String transNo) {

        try {
            Log.e(TAG, "transNo: " + transNo);
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestTransNo request = new RequestTransNo();
            request.trans_no = transNo;

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<ResponseBody> call = tnAPI.getTransId(request);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    // if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            try {
                                //{"Get_TransId_byTrans_No":{"error_code":1,"error_description":"success","ServiceName":"Get_TransId_byTrans_No","Data":"8089"}}
                                //{"data":{"trans_id":70569},"error_code":"1","error_description":"Sucess"}
                                String myData = response.body().string();
                                Log.e(TAG, "data =" + myData);
                           /* try {
                                JSONObject jo = new JSONObject(myData);
                                JSONObject jo1 = jo.getJSONObject("Get_TransId_byTrans_No");
                                String errorMessage = jo1.getString("error_description");
                                if (errorMessage.equals("success")) {
                                    String transId = jo1.getString("Data");
                                    Log.e(TAG, "transId =" + transId);
                                    wsGetBookingDetailForOnline(Long.parseLong(transId));
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }*/

                                try {
                                    JSONObject jo = new JSONObject(myData);
                                    //String errorMessage = jo.getString("error_description");
                                    if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(jo.getString("error_code")))) {
                                        JSONObject joData = jo.getJSONObject("data");
                                        String transId = joData.getString("trans_id");
                                        Log.e(TAG, "transId =" + transId);
                                        wsGetBookingDetailForOnline(Long.parseLong(transId));
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    t.printStackTrace();

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    // WS - Get Booking Detail For Online
    private void wsGetBookingDetailForOnline(long transId) {
        // if (!mLoader.isShowing() && !isFinishing()) mLoader.show();
        try {
            RequestGetBookingDetailForOnline request = new RequestGetBookingDetailForOnline(transId, "");

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackGetBookingDetailForOnline> call = tnAPI.getBookingDetailForOnline(request);
            call.enqueue(new Callback<CallbackGetBookingDetailForOnline>() {
                @Override
                public void onResponse(@NonNull Call<CallbackGetBookingDetailForOnline> call, @NonNull Response<CallbackGetBookingDetailForOnline> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        // if (response.body().data().isSuccess()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            BookingHistory data = response.body().data();
                            ticketList = data.getTicketList();
                            Log.e(TAG, "ws_ticketList = " + ticketList);
                            DB.addBookingHistory(data, mLogin.getUserId());
                            bookingHistory = data;
                            Log.e(TAG, "ws_bookingHistory=" + bookingHistory);
                            refreshCurrentTicket(DB);
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackGetBookingDetailForOnline> call, @NonNull Throwable t) {
                    t.printStackTrace();

                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        try {
            if (uri != null) {
                Intent intent = new Intent(this, LandingActivity.class);
                intent.putExtra(Constant.ScreenExtras.DEEP_LINK, true);
                startActivity(intent);
                finish();
                return;
            }

            if (barcodeView.getVisibility() == View.VISIBLE) {
                closeBarcode();
            } else {
                // super.onBackPressed();
                Intent returnIntent = new Intent();
                setResult(Activity.RESULT_CANCELED, returnIntent);
                finish();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private Uri handleIntent() {
        try {
            // ATTENTION: This was auto-generated to handle app links.
            Intent appLinkIntent = getIntent();
            String appLinkAction = appLinkIntent.getAction();
            Log.e(TAG, "appLinkAction =" + appLinkAction);
            Uri appLinkData = appLinkIntent.getData();
            Log.e(TAG, "appLinkData =" + appLinkData);
            Log.e(TAG, "appLinkScheme =" + appLinkIntent.getScheme());

            if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null) {
                Log.e(TAG, "appLinkPath =" + appLinkData.getPath());
                String recipeId = appLinkData.getLastPathSegment();
                Log.e(TAG, "recipeId =" + recipeId);
            } else {
                if (getIntent().hasExtra(Constant.ScreenExtras.DEEP_LINK_URI)) {
                    String url = getIntent().getStringExtra(Constant.ScreenExtras.DEEP_LINK_URI);
                    Log.e(TAG, "url=" + url);
                    appLinkData = Uri.parse(url);
                    String recipeId = appLinkData.getLastPathSegment();
                    Log.e(TAG, "else_recipeId =" + recipeId);
                }
            }
            return appLinkData;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }
}
