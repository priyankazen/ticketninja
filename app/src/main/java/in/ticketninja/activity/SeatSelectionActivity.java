/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.net.UrlQuerySanitizer;
import android.os.Build;
import android.os.Bundle;

import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;
import in.ticketninja.BuildConfig;
import in.ticketninja.R;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.widget.SRKLoaderDialog;

public class SeatSelectionActivity extends AppCompatActivity {

    private final String TAG = SeatSelectionActivity.class.getSimpleName();
    private SRKLoaderDialog mLoader;
    private PreferencesUtils mPref;
    private String seatType, user_identification_list;
    private   long eventId,showId,eventCategoryId;
    private int seatCount;
    private String eventDate = "";
    private String eventTime = "";
    private String svgBlock = "";
    private String eventName = "";
    private String eventAddress = "";
    private String eventCity = "";


    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_seat_selection);

            Log.e(TAG, "is calledddd..............");
            mPref = new PreferencesUtils(this);
            LoginUtils mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);

            getIntentData();

            setupActionBar();

            WebView webView = findViewById(R.id.webView);

            if (Constant.SVG_BLOCK.equals(svgBlock)){
                webView.loadUrl(BuildConfig.SEAT_URL + "events/blocklayout/"
                        + eventId
                        + "/" + showId
                        + "?device_type=" + Constant.DEVICE_TYPE
                        + "&device_info=" + Constant.DEVICE_INFO
                        + "&engine_type=" + Constant.ENGINE_TYPE);
            }else {
                webView.loadUrl(BuildConfig.SEAT_URL + "appseatlayout?"
                        + "e_id=" + eventId
                        + "&show_id=" + showId
                        + "&user_id=" + mLogin.getUserId()
                        + "&c_id=" + eventCategoryId
                        + "&seat=" + seatCount
                        + "&event_date=" + eventDate
                        + "&time=" + eventTime
                        + "&device_type=" + Constant.DEVICE_TYPE
                        + "&device_info=" + Constant.DEVICE_INFO
                        + "&engine_type=" + Constant.ENGINE_TYPE);
            }

            WebSettings webSettings = webView.getSettings();
            webSettings.setDomStorageEnabled(true);
            webSettings.setJavaScriptEnabled(true);
            webView.setWebViewClient(new WebViewClient() {
                @Override
                public void onPageStarted(WebView view, String url, Bitmap favicon) {
                    super.onPageStarted(view, url, favicon);
                    Log.e(TAG, "onPageStarted  URL=>" + url);

                    //https://ticketninja.in:3002/appsession?session_id=yBsksBL1JAb3nHFTA60hdyUk8OcxIv9I
                    if (url.contains(BuildConfig.SEAT_URL + "appsession")) {
                        if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                        //https://ticketninja.in/BookingSuccess?sessionId=165930803
                        UrlQuerySanitizer sanitzer = new UrlQuerySanitizer(url);
                        String sessionId = sanitzer.getValue("session_id");
                        Log.e(TAG, "sessionId==> " + sessionId);

                        if (Validate.isNull(mPref.getUserEmailId()) || Validate.isNull(mPref.getUserPhoneNumber()) ||
                                Validate.isNull(mPref.getCountryCode())) {
                            // TODO: 29/3/18 SHREYA 1 : Contact Detail Flow
//                        startActivity(new Intent(SeatSelectionActivity.this, ContactConfirmationActivity.class)););
                            Log.e(TAG, "seatType==> " + seatType);

                            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                            ContactConfirmationActivity.Companion.start(SeatSelectionActivity.this, sessionId, Constant.ContactActions.EDIT, seatType, user_identification_list);
                            finish();

                        } else {
                            OrderSummaryActivity.start(SeatSelectionActivity.this, sessionId, seatType, user_identification_list);
                            finish();
                        }

                    } else if (url.contains(BuildConfig.SEAT_URL + "BookingFail/")) {

                        UrlQuerySanitizer sanitzer = new UrlQuerySanitizer(url);
                        String errorMessage = sanitzer.getValue("errorMessage");
                        if (Validate.isNotNull(errorMessage)) {
                            MessageUtils.showAlert(SeatSelectionActivity.this, errorMessage, () -> finish());
                        } else {
                            finish();
                        }
                    } else {
                        Log.e(TAG, "else URL ");
                        if (mLoader != null && !mLoader.isShowing() && !isFinishing())
                            mLoader.show();
                    }

               /* try{

                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                }*/
                /*if (src.equals(RestApi.WAJA_URL + "/")) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (webView != null) webView.stopLoading();
                    // https://www.knetpaytest.com.kw/CGW302/hppaction
                    iv_back.setVisibility(View.GONE);
                    MessageUtils.showToast(PaymentActivity.this, "Payment Successful.");
                    IspaymentSuccessful = 1;
                    IsSucced = 1;
                    Intent intPay = new Intent(getApplicationContext(), Activity_home.class);
                    intPay.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intPay);

                } else if (src.equals(RestApi.WAJA_URL + "/Order/Order")) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (webView != null) webView.stopLoading();
                    // https://www.knetpaytest.com.kw/CGW302/hppaction
                    iv_back.setVisibility(View.GONE);
                    MessageUtils.showToast(PaymentActivity.this, "Payment Failed.");
                    IspamentFail = 1;
                    IsFailed = 1;
                    doubleBackToExitPressedOnce = true;
                    onBackPressed();

                } else if (src.contains(RestApi.WAJA_URL + "/Knet/Error?")) {
                    iv_back.setVisibility(View.GONE);
                    IspaymentSuccessful = 1;
                    IspamentFail = 1;
                    IsSucced = 1;
                    IsFailed = 1;


                } else if (src.contains(RestApi.WAJA_URL + "/Knet/Result?")) {

                    iv_back.setVisibility(View.GONE);
                    IspaymentSuccessful = 1;
                    IspamentFail = 1;
                    IsSucced = 1;
                    IsFailed = 1;
                    is_from_succed = 1;

                } else {
                    if (mLoader != null && !mLoader.isShowing() && !isFinishing()) mLoader.show();
                }*/
                }

                @Override
                public void onPageFinished(WebView view, String url) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    Log.e(TAG, "onPageFinished  URL=>" + url);
                    super.onPageFinished(view, url);
                }

                @Override
                public void onLoadResource(WebView view, String url) {
                    super.onLoadResource(view, url);
                }

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    return super.shouldOverrideUrlLoading(view, url);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getIntentData() {
        try {
            eventId = getIntent().getLongExtra(Constant.ScreenExtras.EVENT_ID, 0);
            showId = getIntent().getLongExtra(Constant.ScreenExtras.SHOW_ID, 0);
            eventCategoryId = getIntent().getLongExtra(Constant.ScreenExtras.PLAN_CATEGORY_ID, 0);
            seatCount = getIntent().getIntExtra(Constant.ScreenExtras.SEAT_COUNT, 0);

            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_DATE)) {
                eventDate = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_DATE);
                Log.e(TAG, "eventDate: " + eventDate);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_TIME)) {
                eventTime = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_TIME);
                Log.e(TAG, "eventTime: " + eventTime);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.TYPE)) {
                seatType = getIntent().getStringExtra(Constant.ScreenExtras.TYPE);
                Log.e(TAG, "seatType: " + seatType);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.USER_IDENTIFICATION_LIST)) {
                user_identification_list = getIntent().getStringExtra(Constant.ScreenExtras.USER_IDENTIFICATION_LIST);
                Log.e(TAG, "user_identification_list: " + user_identification_list);
            }

            if (getIntent().hasExtra(Constant.SVG_BLOCK)) {
                svgBlock = getIntent().getStringExtra(Constant.SVG_BLOCK);
                Log.e(TAG, "svgBlock: " + svgBlock);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_NAME)) {
                eventName = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_NAME);
                Log.e(TAG, "eventName: " + eventName);
            }
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_ADDRESS)) {
                eventAddress = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_ADDRESS);
                Log.e(TAG, "eventAddress: " + eventAddress);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_CITY)) {
                eventCity = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_CITY);
                Log.e(TAG, "eventCity: " + eventCity);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                TextView tvAdd = toolbar.findViewById(R.id.tvAdd);
             //   if (Constant.SVG_BLOCK.equals(svgBlock)){
                    tvTitle.setText(eventName);
                    String date = DateTimeUtils.changeDateTimeFormat(eventDate,DateTimeUtils.SERVER_FORMAT_DATE,DateTimeUtils.DATE_F);
                    //String address="";
                   /* if (!TextUtils.isEmpty(eventAddress)) {
                        address = eventAddress.split(",")[1];
                    }else {
                        address ="";
                    }*/
                    Log.e(TAG,"date : "+date);
                    Log.e(TAG,"eventTime : "+eventTime);
                    Log.e(TAG,"address : "+eventAddress);
                    Log.e(TAG,"eventCity : "+eventCity);

                    tvAdd.setText(String.format("%s | %s at %s : %s", date, eventTime,eventAddress , eventCity));
               /* }else {
                    tvTitle.setText(getTitle());
                }*/
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == RESULT_OK && requestCode == Constant.REQUEST_CODE_CONFIRM_CONTACT_DETAIL) {
//            if (data != null && data.hasExtra(Constant.ScreenExtras.EVENT_SESSION_ID)) {
//                mPref = new PreferencesUtils(this);
//                if (Validate.isNotNull(mPref.getUserEmailId()) && Validate.isNotNull(mPref.getUserPhoneNumber()) &&
//                        Validate.isNotNull(mPref.getCountryCode())) {
//                    OrderSummaryActivity.start(SeatSelectionActivity.this, data.getStringExtra(Constant.ScreenExtras.EVENT_SESSION_ID));
//                }
//            }
//        }
//    }

    @Override
    protected void onDestroy() {
        if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        Log.e(TAG,"mLoader.isShowing() :"+mLoader.isShowing());
    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboard(this);
        super.onBackPressed();
    }

}

//https://ticketninja.in:3002/appseatlayout?e_id=1006&show_id=1396&user_id=1000&c_id=1006&seat=1&event_date=2020-02-02&time=6:00%20PM&device_type=android&engine_type=App
//https://ticketninja.in:3002/appseatlayout?e_id=1041&show_id=0&user_id=1000&c_id=1006&seat=1&event_date=&time=&device_type=android&engine_type=App
//07-13 11:31:22.515 16604-16604/in.ticketninja E/SeatSelectionActivity: onPageStarted  URL=>https://ticketninja.in:3001/appseatlayout?e_id=1750&show_id=2034&user_id=f9ef7dcc2b9f6a48b63310e9a4593e70&c_id=1147&seat=1&event_date=2019-08-24&time=5:00%20PM&device_type=android&device_info=App&engine_type=App
