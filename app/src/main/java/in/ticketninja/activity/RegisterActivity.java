/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.text.InputType;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

//import com.crashlytics.android.Crashlytics;
//import com.crashlytics.android.answers.Answers;
//import com.crashlytics.android.answers.SignUpEvent;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.common.CacheHelper;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.CustomTypefaceSpan;
import in.ticketninja.common.GPSTracker;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.TextViewUtils;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.UserInfo;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestRegister;
import in.ticketninja.ws.response.CallbackRegister;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    private CommonClass CC;
    private SRKLoaderDialog mLoader;
    private PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();

    private int mFromScreen = 0;

    private EditText etEmail, etMobile, etPassword, etConfirmPassword;
    private EditText etCountryCode;
    private ImageView ivActionBack;
    private Button btnRegister;
    FirebaseCrashlytics crashlytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            setContentView(R.layout.activity_register);

            initializeData();

            mFromScreen = getIntent().getIntExtra(Constant.ScreenExtras.FROM_SCREEN, 0);

            findViewById();

            setOnClickEvent();

            Typeface tfBold = ResourcesCompat.getFont(this, R.font.helvetica_neue_bold);
            TextView tvLoginNow = findViewById(R.id.tvLoginNow);
            SpannableString styledString = new SpannableString(getString(R.string.login_text_login_now));
            styledString.setSpan(new CustomTypefaceSpan("", tfBold), 25, 32, 0);
            styledString.setSpan(new ForegroundColorSpan(Color.parseColor("#4A4A4A")), 25, 32, 0);
            styledString.setSpan(new TextViewUtils.MyClickableSpan() {
                @Override
                public void onClick(@NonNull View widget) {
                    Utility.hideKeyboard(RegisterActivity.this);
                    if (mFromScreen == Constant.ScreenExtras.FROM_ACTIVITY_MY_PROFILE) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra(Constant.ScreenExtras.DATA, Constant.ScreenExtras.PRESS_LOGIN);
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    } else {
                        onBackPressed();
                    }
                }
            }, 25, 32, 0);
            tvLoginNow.setMovementMethod(LinkMovementMethod.getInstance());
            tvLoginNow.setText(styledString);

        /*if (BuildConfig.DEBUG) {
            etEmail.setText("rohit@nichetech.in");
            etMobile.setText("7990850873");
            etPassword.setText("123456");
            etConfirmPassword.setText("123456");
        }*/
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void initializeData() {
        try {
            CC = new CommonClass(this);
            mLoader = new SRKLoaderDialog(this);
            crashlytics = FirebaseCrashlytics.getInstance();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setOnClickEvent() {
        try {
            ivActionBack.setOnClickListener(v -> RegisterActivity.this.onBackPressed());

            etCountryCode.setOnClickListener(v -> {

                try {
                    CountryPicker picker = CountryPicker.newInstance("Select country");  // dialog title
                    picker.setListener((name, code, dialCode, flagDrawableResID) -> {

                        etCountryCode.setText(dialCode);
                        Utility.hideKeyboard(RegisterActivity.this, picker.getView());
                        picker.dismiss();

                    });
                    picker.onCancel(new DialogInterface() {
                        @Override
                        public void cancel() {
                        }

                        @Override
                        public void dismiss() {

                            Utility.hideKeyboard(RegisterActivity.this, picker.getView());
                        }
                    });
                    picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });


            btnRegister.setOnClickListener(v -> {
                Utility.hideKeyboard(this);

                if (CC.isOnline()) {
                    if (checkValidation()) {
                        String email = etEmail.getText().toString().trim();
                        String countryCode = etCountryCode.getText().toString().trim();
                        String mobile = etMobile.getText().toString().trim();
                        String password = etPassword.getText().toString().trim();

                        try {
                            if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                                Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile, Constant.DEFAULT_COUNTRY_REGION);
                                countryCode = "+" + phoneNumber.getCountryCode();
                                mobile = phoneNumber.getNationalNumber() + "";
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        wsRegister(email, countryCode, mobile, password);
                    }
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void findViewById() {
        try {
            // TYPEFACE
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);
            etEmail = findViewById(R.id.etRegisterEmail);
            etEmail.setTypeface(tfMedium);
            etCountryCode = findViewById(R.id.etCountryCode);
            etCountryCode.setTypeface(tfMedium);
            etMobile = findViewById(R.id.etRegisterMobile);
            etMobile.setTypeface(tfMedium);
            etPassword = findViewById(R.id.etRegisterPassword);
            etPassword.setTypeface(tfMedium);
            etConfirmPassword = findViewById(R.id.etRegisterConfirmPassword);
            etConfirmPassword.setTypeface(tfMedium);

            Country country = Country.getCountryByISO(Constant.DEFAULT_COUNTRY_REGION);
            etCountryCode.setText(country.getDialCode());

            etCountryCode.setInputType(InputType.TYPE_NULL);
            ivActionBack = findViewById(R.id.ivActionBack);
            btnRegister = findViewById(R.id.btnRegister);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        try {
            Utility.hideKeyboard(this);
            super.onBackPressed();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean checkValidation() {
        try {
            String email = etEmail.getText().toString().trim();
            String countryCode = etCountryCode.getText().toString().trim();
            String mobile = etMobile.getText().toString().trim();
            String password = etPassword.getText().toString().trim();
            String confirmPassword = etConfirmPassword.getText().toString().trim();

            if (Validate.isNull(email)) {
                CC.showToast(R.string.login_msg_enter_email);
                etEmail.requestFocus();
                return false;

            } else if (!Validate.checkEmail(email)) {
                CC.showToast(R.string.login_msg_enter_email_valid);
                etEmail.requestFocus();
                return false;

            } else if (Validate.isNull(mobile)) {
                CC.showToast(R.string.login_msg_enter_mobile);
                etMobile.requestFocus();
                return false;

            } else if (!Validate.checkPhone_libPhoneNumber(countryCode, mobile)) {
                CC.showToast(R.string.common_msg_enter_mobile_valid);
                etMobile.requestFocus();
                return false;

            } else if (Validate.isNull(password)) {
                CC.showToast(R.string.login_msg_enter_password);
                etPassword.requestFocus();
                return false;

            } else if (password.length() < 6 || password.length() > 15) {
                CC.showToast(R.string.login_msg_enter_password_valid);
                etPassword.requestFocus();
                return false;

            } else if (Validate.isNull(confirmPassword)) {
                CC.showToast(R.string.login_msg_enter_confirm_password);
                etConfirmPassword.requestFocus();
                return false;

            } else if (confirmPassword.length() < 6 || confirmPassword.length() > 15) {
                CC.showToast(R.string.login_msg_enter_confirm_password_valid);
                etConfirmPassword.requestFocus();
                return false;

            } else if (!confirmPassword.equals(password)) {
                CC.showToast(R.string.login_msg_enter_confirm_password_not_match);
                etConfirmPassword.requestFocus();
                return false;

            } else {
                return true;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    // WS - Register
    private void wsRegister(String email, String countryCode, String mobile, String password) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestRegister.RegisterClass registerClass = new RequestRegister.RegisterClass();
            registerClass.email = email;
            registerClass.mobileno = mobile;
            registerClass.password = password;
            registerClass.mode = Constant.RegisterMode.INSERT;
            registerClass.device_token = PreferencesUtils.getFCMPushKey(RegisterActivity.this);
            registerClass.country_code = countryCode;
           // RequestRegister request = new RequestRegister(registerClass);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackRegister> call = tnAPI.registerEditProfile(registerClass);
            call.enqueue(new Callback<CallbackRegister>() {
                @Override
                public void onResponse(@NonNull Call<CallbackRegister> call, @NonNull Response<CallbackRegister> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {

                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            CC.showAlert(Objects.requireNonNull(response.body()).getError_description(), () -> {
                                if (mFromScreen == Constant.ScreenExtras.FROM_ACTIVITY_MY_PROFILE) {

                                    // REMOVE CACHED DASHBOARD DATA
                                    CacheHelper mCacheHelper = new CacheHelper(RegisterActivity.this);
                                    mCacheHelper.remove(CacheHelper.KEY_DASHBOARD);

                                    UserInfo userInfo = Objects.requireNonNull(response.body()).data();
                                    LoginUtils mLogin = new LoginUtils(RegisterActivity.this);
                                    mLogin.setUserId(userInfo.user_id);
                                    mLogin.setFirstName(userInfo.firstname);
                                    mLogin.setLastName(userInfo.lastname);
                                    mLogin.setEmail(userInfo.email);
                                    mLogin.setCountryCode(userInfo.country_code);
                                    mLogin.setMobile(userInfo.mobileno);
                                    mLogin.setGender(userInfo.gender);
                                    mLogin.setPicture(userInfo.user_imageid_url);
                                    mLogin.setUserDOBWithParse(userInfo.birthdate);
                                    mLogin.setNotifications(Objects.requireNonNull(response.body()).data().notification_unread_count);

                                    try {
                                        // FirebaseAnalytics
                                        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
                                        mFirebaseAnalytics.setUserProperty("tn_user_id", mLogin.getUserId() + "");
                                        mFirebaseAnalytics.setUserProperty("tn_user_name", mLogin.getName() + "");
                                        mFirebaseAnalytics.setUserId(mLogin.getUserId() + "");
                                        // Crashlytics
                                        crashlytics.setUserId("" + mLogin.getUserId());
                                        //Crashlytics.setUserIdentifier("" + mLogin.getUserId());
                                        //Crashlytics.setUserEmail("" + mLogin.getEmail());
                                       // Crashlytics.setUserName("" + mLogin.getName());

                                        //Answers.getInstance().logSignUp(new SignUpEvent().putMethod("email").putSuccess(true));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }

                                    Intent returnIntent = new Intent();
                                    setResult(Activity.RESULT_OK, returnIntent);
                                    finish();

                             /*   Intent returnIntent = new Intent();
                                returnIntent.putExtra(Constant.ScreenExtras.DATA, Constant.ScreenExtras.PRESS_LOGIN);
                                setResult(Activity.RESULT_OK, returnIntent);
                                finish();*/
                                } else {
                                    onBackPressed();
                                }
                            });
                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {

                            if (Objects.requireNonNull(response.body()).data().is_alreadyregister) {
                                showAlreadyRegisterDialog(Objects.requireNonNull(response.body()).getError_description(), email);
                            } else {
                                CC.showAlert(Objects.requireNonNull(response.body()).getError_description());
                            }

                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackRegister> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        }catch (Exception e){
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }


    }

    private void showAlreadyRegisterDialog(String message, String email) {
        try {
            Utility.hideKeyboard(RegisterActivity.this);


            AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            // alertDialog.setTitle("Dialog Button");
            alertDialog.setMessage(message);
            alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Sign In", (dialog, id) -> {
           /*Intent i = new Intent(RegisterActivity.this, LoginActivity.class);
            i.putExtra(Constant.Register.EMAIL__CODE, email);
            startActivity(i);
            finish();*/
                Intent returnIntent = new Intent();
                returnIntent.putExtra(Constant.ScreenExtras.EMAIL, email);
                returnIntent.putExtra(Constant.ScreenExtras.DATA, Constant.ScreenExtras.PRESS_LOGIN);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
                alertDialog.dismiss();
                //...
            });

            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok", (dialog, id) -> {
                alertDialog.dismiss();
                //...
            });

            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "Forgot password", (dialog, id) -> {
                Intent i = new Intent(RegisterActivity.this, ForgotPasswordActivity.class);
                i.putExtra(Constant.ScreenExtras.DATA, email);
                startActivity(i);
                finish();
                alertDialog.dismiss();
                //..
            });
            alertDialog.show();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static String getCountryName(Context context, double latitude, double longitude) {
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);

                if (addresses != null && !addresses.isEmpty()) {
                    return addresses.get(0).getCountryName();
                }
                return null;
            } catch (IOException ignored) {
                return null;
            }

        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            if (requestCode == Utility.REQUEST_PERMISSION_ACCESS_FINE_LOCATION) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    GPSTracker gps = new GPSTracker(RegisterActivity.this);
                    if (gps.canGetLocation()) {
                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();
                        Log.e("latitude==>", "" + latitude);
                        Log.e("longitude==>", "" + longitude);

                        Country country = Country.getCountryByName(getCountryName(RegisterActivity.this, latitude, longitude));
                        etCountryCode.setText(Objects.requireNonNull(country).getDialCode());

                    } else {
                        gps.showSettingsAlert();
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


}
