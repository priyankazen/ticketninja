package in.ticketninja.activity;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputEditText;

import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestCorporateBooking;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CorporateBookingActivity extends AppCompatActivity {
    private final String TAG = CorporateBookingActivity.class.getSimpleName();
    private SRKLoaderDialog mLoader;
    private TextInputEditText edtName, edtMobileNo, edtEmail, edtTicket;
    private CheckBox cbCBooking;
    private  Button btnReggi;
    private long eventId;
    private CommonClass commonClass;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {

            setContentView(R.layout.activity_corporate_booking);

            initializeData();

            //getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

            findViewById();

            eventId = getIntent().getLongExtra(Constant.ScreenExtras.EVENT_ID, 0);

            setupActionBar();

            btnReggi.setOnClickListener(view -> wsCorporateBooking());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initializeData() {
        try {
            mLoader = new SRKLoaderDialog(this);
            commonClass = new CommonClass(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void findViewById() {

        try {
            edtName = findViewById(R.id.edtName);
            edtMobileNo = findViewById(R.id.edtMobileNo);
            edtEmail = findViewById(R.id.edtEmail);
            edtTicket = findViewById(R.id.edtTicket);
            cbCBooking = findViewById(R.id.cbCBooking);
            btnReggi = findViewById(R.id.btnRegi);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(R.string.corporate_booking);
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {
        finish();
        return super.onTouchEvent(event);
    }

    // WS - Get Brand Association
    //Insert_Corporate_Booking_Inquiry(string event_id, string event_name, string mobileno, string email, string category_name, string Tickets_Looking)
    private void wsCorporateBooking() {
        try {
            String name = Objects.requireNonNull(edtName.getText()).toString().trim();
            String mobile_no = Objects.requireNonNull(edtMobileNo.getText()).toString().trim();
            String email = Objects.requireNonNull(edtEmail.getText()).toString().trim();
            String ticket = Objects.requireNonNull(edtTicket.getText()).toString().trim();

            Log.e(TAG, "name=" + name);
            Log.e(TAG, "mobile_no=" + mobile_no);
            Log.e(TAG, "email=" + email);
            Log.e(TAG, "ticket=" + ticket);
            Log.e(TAG, "eventId=" + eventId);
            Log.e(TAG, "cb=" + cbCBooking.isChecked());


            if (TextUtils.isEmpty(name)) {
                showError(getString(R.string.please_enter_name));
                return;
            } else if (TextUtils.isEmpty(mobile_no)) {
                showError(getString(R.string.please_enter_mobile_no));
                return;
            } else if (!TextUtils.isEmpty(mobile_no) && mobile_no.length() != 10) {
                showError(getString(R.string.common_msg_enter_mobile_valid));
                return;
            } else if (TextUtils.isEmpty(email)) {
                showError(getString(R.string.please_enter_email));
                return;
            } else if (!TextUtils.isEmpty(email) && !Validate.checkEmail(email)) {
                showError(getString(R.string.common_msg_enter_email_valid));
                return;
            } else if (TextUtils.isEmpty(ticket)) {
                showError(getString(R.string.please_enter_ticket));
                return;
            } else if ((!cbCBooking.isChecked())) {
                showError(getString(R.string.please_select_terms_condition));
                return;
            }

            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestCorporateBooking request = new RequestCorporateBooking();
            request.category_name = "";
            request.email = email;
            request.event_id = String.valueOf(eventId);
            request.mobileno = mobile_no;
            request.name = name;
            request.Tickets_Looking = ticket;

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<ResponseBody> call = tnAPI.getCorporationBooking(request);
            call.enqueue(new Callback<ResponseBody>() {
                @SuppressWarnings("ConstantConditions")
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        String myData = response.body().string();
                        Log.e(TAG, "data =" + myData);
                        if (response.isSuccessful()) {
                            Log.e(TAG, "if");
                            JSONObject jo;
                            try {
                                jo = new JSONObject(myData);
                                //JSONObject jo1 = jo.getJSONObject("Insert_Corporate_Booking_Inquiry");
                                String errorCode = jo.getString("error_code");
                                String errorMessage = jo.getString("error_description");
                                if (errorCode.equals("1")) {
                                    commonClass.showAlert(errorMessage, () -> onBackPressed());
                                } else {
                                    commonClass.showAlert(errorMessage, (MessageUtils.OnOkClickListener) null);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            showError("");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    showError("");
                }

            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void showError(String msg) {
        try {
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
