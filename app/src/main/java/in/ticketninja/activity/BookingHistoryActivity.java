/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.BookingHistoryAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.database.DatabaseHelper;
import in.ticketninja.objects.BookingHistory;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestBookingHistory;
import in.ticketninja.ws.response.CallbackBookingHistory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingHistoryActivity extends AppCompatActivity {

    private CommonClass CC;
    private DatabaseHelper DB;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;

    private List<BookingHistory> bookingHistoryList = new ArrayList<>();

    private RecyclerView recyclerView;
    private LinearLayout llErrorLayout;
    private ImageView ivErrorImage;
    private TextView tvErrorMessage, tvErrorButton;
    private Button btn_past_ticket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_booking_history);

            initializeData();

            setupActionbar();

            findViewById();

            //VCM7882780,QZK7967202,,ZTD8087949 ZTD8087949
            if (CC.isOnline()) {
                wsGetBookingHistory();
            } else {
                bookingHistoryList = DB.getBookingHistory(mLogin.getUserId());
                if (bookingHistoryList != null && bookingHistoryList.size() > 0) {
                    invalidateData();
                } else {
                    showNoInternet();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initializeData() {
        try {
            CC = new CommonClass(this);
            DB = new DatabaseHelper(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void findViewById() {
        try {
            recyclerView = findViewById(R.id.recyclerView);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);

            llErrorLayout = findViewById(R.id.llErrorLayout);
            ivErrorImage = findViewById(R.id.ivErrorImage);
            tvErrorMessage = findViewById(R.id.tvErrorMessage);
            tvErrorButton = findViewById(R.id.tvErrorButton);
            btn_past_ticket = findViewById(R.id.btn_past_ticket);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionbar() {

        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                //getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showNoInternet() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_connection_lost)
                    .placeholder(R.drawable.ic_connection_lost_static)
                    .into(ivErrorImage);
            tvErrorMessage.setText(R.string.msg_no_internet);
            recyclerView.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.VISIBLE);
            tvErrorButton.setText(R.string.btn_try_again);
            tvErrorButton.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    wsGetBookingHistory();
                } else {
                    showNoInternet();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void showError(boolean showErrorButton) {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_no_result_found)
                    .placeholder(R.drawable.ic_no_result_found)
                    .into(ivErrorImage);
            recyclerView.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            if (showErrorButton) {
                tvErrorButton.setVisibility(View.VISIBLE);
                tvErrorButton.setText(R.string.btn_book_now);
                tvErrorButton.setOnClickListener(v -> {
                    Utility.hideKeyboard(this);
                    Intent i1 = new Intent(getApplicationContext(), LandingActivity.class);
                    i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(i1);
                });
            } else {
                tvErrorButton.setVisibility(View.GONE);
            }

            btn_past_ticket.setVisibility(View.VISIBLE);
            btn_past_ticket.setOnClickListener(v -> {
                Utility.hideKeyboard(this);
                Intent i = new Intent(getApplicationContext(), PastTicketActivity.class);
                startActivity(i);

            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        try {
            Utility.hideKeyboard(this);
            super.onBackPressed();
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    private void invalidateData() {
        try {
            Collections.reverse(bookingHistoryList);

            BookingHistoryAdapter adapter = new BookingHistoryAdapter(BookingHistoryActivity.this, bookingHistoryList, 1);

            adapter.setOnItemClickListener((position, history) -> {
                Intent intConfirm = new Intent(getApplicationContext(), BookingHistoryDetailActivity.class);
                //intConfirm.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intConfirm.putExtra(Constant.ScreenExtras.EVENT_HISTORY_TYPE, Constant.HistoryType.FUTURE);
                intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, history.trans_id);
                intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE, history.payment_type);
                intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TICKET_LIVE, history.isIs_live_ticket());
                intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_APP_TICKET, history.isIs_app_ticket());
                startActivityForResult(intConfirm, Constant.ActivityForResult.FROM_MY_TICKETS);
            });
            recyclerView.setAdapter(adapter);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    //..............................WS CALL FOR GET BOOKING HISTORY..........//
    private void wsGetBookingHistory() {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestBookingHistory request = new RequestBookingHistory();

            request.user_id = String.valueOf(mLogin.getUserId());
            request.h_type = Constant.HistoryType.FUTURE;

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackBookingHistory> call = tnAPI.GetBookingHistory(request);
            call.enqueue(new Callback<CallbackBookingHistory>() {
                @Override
                public void onResponse(@NonNull Call<CallbackBookingHistory> call, @NonNull Response<CallbackBookingHistory> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                                Log.e("Tag", "bookingHistoryList:" + DB.getBookingHistory(mLogin.getUserId()));

                                try {
                                    DB.clearBookingHistoryTable(mLogin.getUserId());
                                    DB.clearBookingTicketListTable(mLogin.getUserId());
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                bookingHistoryList = response.body().data().booking_history;
                                if (bookingHistoryList != null && bookingHistoryList.size() > 0) {

                                    for (int i = 0; i < bookingHistoryList.size(); i++) {
                                        DB.addBookingHistory(bookingHistoryList.get(i), mLogin.getUserId());
                                    }

                                    if (ContextCompat.checkSelfPermission(BookingHistoryActivity.this,
                                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                                            || ContextCompat.checkSelfPermission(BookingHistoryActivity.this,
                                            android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                        ActivityCompat.requestPermissions(BookingHistoryActivity.this,
                                                new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                                                Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE);
                                    } else {
                                        DB.startDownloadingBookingHistoryInBackground();
//                                    DB.startDownloadingTicketListinBackground();
                                    }
                                    bookingHistoryList = DB.getBookingHistory(mLogin.getUserId());
                                    invalidateData();

                                } else {
                                    tvErrorMessage.setText(R.string.msg_no_history_records_found);
                                    showError(true);
                                }

                            } else if (Validate.isNotNull(response.body().getError_description())) {
                                tvErrorMessage.setText(response.body().getError_description());
                                showError(false);

                            } else {
                                tvErrorMessage.setText(R.string.msg_something_wrong);
                                showError(false);
                            }
                        } else {
                            tvErrorMessage.setText(R.string.msg_no_response);
                            showError(false);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                        tvErrorMessage.setText(R.string.msg_something_wrong);
                        showError(false);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackBookingHistory> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    tvErrorMessage.setText(R.string.msg_something_wrong);
                    showError(false);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        try {
            if (requestCode == Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    DB.startDownloadingBookingHistoryInBackground();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == Constant.ActivityForResult.FROM_MY_TICKETS) {
                try {
                    bookingHistoryList = DB.getBookingHistory(mLogin.getUserId());
                    invalidateData();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


}
