/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.app.Application;
import android.content.Context;
import android.view.View;

import androidx.multidex.MultiDex;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import in.ticketninja.common.LoginUtils;
//import io.fabric.sdk.android.Fabric;
//import com.crashlytics.android.Crashlytics;

/**
 * TicketNinja(in.ticketninja) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */

@SuppressWarnings("deprecation")
public class TicketNinjaApplication extends Application {

    LoginUtils mLogin;
    FirebaseCrashlytics crashlytics;

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            FacebookSdk.sdkInitialize(getApplicationContext());
            AppEventsLogger.activateApp(this);

            mLogin = new LoginUtils(this);
            // FirebaseAnalytics
            FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getApplicationContext());
            mFirebaseAnalytics.setUserProperty("tn_user_id", mLogin.getUserId() + "");
            mFirebaseAnalytics.setUserProperty("tn_user_name", mLogin.getName() + "");
            mFirebaseAnalytics.setUserId(mLogin.getUserId() + "");

           // Fabric.with(this, new Crashlytics());
           logUser();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void logUser() {
        // TODO: Use the current user's information
        // You can call any combination of these three methods
        // Crashlytics
        crashlytics = FirebaseCrashlytics.getInstance();
        crashlytics.setUserId("" + mLogin.getUserId());
        //Crashlytics.setUserIdentifier("" + mLogin.getUserId());
        //Crashlytics.setUserEmail("" + mLogin.getEmail());
        //Crashlytics.setUserName("" + mLogin.getName());
    }

    public void forceCrash(View view) {
        throw new RuntimeException("This is a test crash");
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}

//#android.useAndroidX=true
//#android.enableJetifier=true
//C2:77:89:77:2F:E1:BA:BA:1D:2F:28:BD:A8:57:04:54:67:5F:6D:1B

/*

 Button crashButton = new Button(this);
        crashButton.setText("Crash!");
        crashButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Crashlytics.getInstance().crash(); // Force a crash
            }
        });
        setContentView(crashButton);

 */