/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.InputType;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.SpinnerAddress;
import in.ticketninja.adapters.SpinnerCODTiming;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.TextViewUtils;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.DeliveryTimeList;
import in.ticketninja.objects.MyEvent;
import in.ticketninja.objects.PaymentNoteList;
import in.ticketninja.objects.TicketSessionData;
import in.ticketninja.objects.UserAddress;
import in.ticketninja.service.TimerService;
import in.ticketninja.widget.OTPVerificationDialog;
import in.ticketninja.widget.PaymentSuccessDialog;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestCOD_OTP_Send;
import in.ticketninja.ws.request.RequestGeneratePaytmChecksum;
import in.ticketninja.ws.request.RequestInsertPlaceOrder;
import in.ticketninja.ws.request.RequestPaymentCODdate;
import in.ticketninja.ws.response.CallbackCOD_OTP_Send;
import in.ticketninja.ws.response.CallbackGeneratePaytmChecksum;
import in.ticketninja.ws.response.CallbackInsertPlaceOrder;
import in.ticketninja.ws.response.CallbackPaymentCODdate;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PurchaseTicketActivity extends AppCompatActivity /*implements PaymentResultWithDataListener*/ {

    private final String TAG = PurchaseTicketActivity.class.getSimpleName();
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;
    private PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
    private OTPVerificationDialog otpVerificationDialog;

    private String eventSessionId = "";
    private TicketSessionData eventSessionData = new TicketSessionData();
    private ArrayList<PaymentNoteList> eventPaymentNoteList = new ArrayList<>();
    private long selectedAddressId = 0;

    private ProgressBar progressBar;
    private Button btn_continue, btn_paytm;
    private Spinner sp_address, sp_delivery;
    private TextView tvTimerCountDown;
    private RadioButton rb_online, rb_cod;
    private LinearLayout ll_cod;

    private TextView tv_terms_and_condition;
    private EditText etEmail, etMobile, etCountryCode, etAdrsFullName, etAdrsLine1, etAdrsLine2, etAdrsPinCode, etAdrsCity, etAdrsState, etAdrsMobile, etAdrsType;
    private TextView tv_rule;
    private CommonClass CC;
    private CountDownTimer mTimer;
    private List<DeliveryTimeList> mTimingList = new ArrayList<>();
    private List<UserAddress> mAddressList = new ArrayList<>();
    private double Amount_show;

    private Intent timerServiceIntent;

    private int flag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_purchase_ticket);

        try {
 /*
          Preload payment resources
         */
            // Checkout.preload(getApplicationContext());

            initializeData();

            getIntentData();

            findViewById();

            setupActionBar();

            showHideForm();

            setOnClickEvent();

            Country country = Country.getCountryByISO(Constant.DEFAULT_COUNTRY_REGION);
            if (Validate.isNotNull(mLogin.getCountryCode())) {
                etCountryCode.setText(mLogin.getCountryCode());
            } else {
                etCountryCode.setText(country.getDialCode());
            }

            SpannableString styledString = new SpannableString("By Placing the order, you agree to the Terms-and-conditions.");
            styledString.setSpan(new ForegroundColorSpan(Color.parseColor("#F53033")), 39, 60, 0);
            styledString.setSpan(new TextViewUtils.MyClickableSpan() {
                @Override
                public void onClick(@NonNull View widget) {
                    Utility.hideKeyboard(PurchaseTicketActivity.this);
                    if (CC.isOnline()) {
                        Intent iterms_condition = new Intent(getApplicationContext(), TermsAndConditionActivity.class);
                        startActivity(iterms_condition);
                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                }
            }, 39, 60, 0);
            tv_terms_and_condition.setMovementMethod(LinkMovementMethod.getInstance());
            tv_terms_and_condition.setText(styledString);

            invalidateData();

        /*MyEvent myEvent = new MyEvent();
        myEvent.event_id = eventSessionData.getId();
        myEvent.Poster_Image_Id = eventSessionData.getImageId();
        myEvent.poster_image_url = eventSessionData.getImageUrl();

        Intent i = new Intent(this, TimerService.class);
        i.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
        startService(i);*/

            startTimerService();

            progressBar.setProgress(100);

        /*mTimer = new CountDownTimer(TimeUnit.MINUTES.toMillis(Constant.PAYMENT_SESSION_TIMEOUT), TimeUnit.SECONDS.toMillis(1)) {
            public void onTick(long millisUntilFinished) {
                progressBar.setProgress(((int) millisUntilFinished / 1000 * 100) / (Constant.PAYMENT_SESSION_TIMEOUT * 60));

                if (tvTimerCountDown != null) {
                    tvTimerCountDown.setText(String.format(Locale.getDefault(), Constant.TIMER_FORMAT,
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                    TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                }
            }

            public void onFinish() {
                try {
                    Utility.hideKeyboard(PurchaseTicketActivity.this);
                    tvTimerCountDown.setText(R.string.label_done);
                    CC.showAlert(R.string.purchase_ticket_msg_session_timeout, () -> {

                        MyEvent myEvent = new MyEvent();
                        myEvent.event_id = eventSessionData.getId();
                        myEvent.Poster_Image_Id = eventSessionData.getImageId();
                        myEvent.poster_image_url = eventSessionData.getImageUrl();

                        Intent i1 = new Intent(PurchaseTicketActivity.this, EventDetailActivity.class);
                        i1.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                        i1.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_PAYMENT);
                        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i1);
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();*/

            wsGetDataForCOD();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getIntentData() {
        try {
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_SESSION_ID)) {
                eventSessionId = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_SESSION_ID);
            }
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_SESSION_DATA)) {
                eventSessionData = (TicketSessionData) getIntent().getSerializableExtra(Constant.ScreenExtras.EVENT_SESSION_DATA);
            }
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_NOTE_LIST)) {
                eventPaymentNoteList = (ArrayList<PaymentNoteList>) getIntent().getSerializableExtra(Constant.ScreenExtras.EVENT_NOTE_LIST);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            mLogin = new LoginUtils(this);
            CC = new CommonClass(this);
            mLoader = new SRKLoaderDialog(this);
            otpVerificationDialog = new OTPVerificationDialog(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setOnClickEvent() {
        try {

            rb_cod.setOnClickListener(v -> showHideForm());

            rb_online.setOnClickListener(v -> showHideForm());

            etCountryCode.setOnClickListener(v -> {
                try {
                    CountryPicker picker = CountryPicker.newInstance("Select country");  // dialog title
                    picker.setListener((name, code, dialCode, flagDrawableResID) -> {

                        etCountryCode.setText(dialCode);

                        Utility.hideKeyboard(PurchaseTicketActivity.this, picker.getView());

                        picker.dismiss();

                        //  tv_country.setCompoundDrawables(flagDrawableResID,null,null,null);
                    });


                    picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });

            btn_paytm.setOnClickListener(view -> {
                try {

                    if (CC.isOnline()) {
                        //startPaytm();
                        String email = etEmail.getText().toString().trim();
                        String countryCode = etCountryCode.getText().toString().trim();
                        String mobileNo = etMobile.getText().toString().trim();
                        String sessionId = eventSessionId;
                        String txnAmount = eventSessionData.getAmount() + "";
                        //String txnAmount = "1";

                        try {
                            if (Validate.isNotNull(countryCode) && Validate.isNotNull(countryCode)) {
                                Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobileNo, Constant.DEFAULT_COUNTRY_REGION);
                                //countryCode = "+" + phoneNumber.getCountryCode();
                                mobileNo = phoneNumber.getNationalNumber() + "";
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Amount_show = eventSessionData.getAmount();
                        wsGeneratePaytmChecksum(mLogin.getUserId(), sessionId, txnAmount, email, mobileNo);

                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });

            btn_continue.setOnClickListener(v -> {

                try {
                    if (CC.isOnline()) {
                        if (Constant.Code.ONE == eventSessionData.isRSVP() || Constant.Code.ONE == eventSessionData.isInviteRSVP()) {
                            if (checkValidationMobileEmail()) {
                                rsvpEvent();
                            }

                        } else if (rb_cod.isChecked()) {
                            arePermissionGranted();

                        } else if (rb_online.isChecked()) {
                            if (checkValidation()) {
                                String name = mLogin.getName() + "";
                                String email = etEmail.getText().toString().trim();
                                String countryCode = etCountryCode.getText().toString().trim();
                                String mobile = etMobile.getText().toString().trim();
                                try {
                                    if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                                        Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile, Constant.DEFAULT_COUNTRY_REGION);
                                        countryCode = "+" + phoneNumber.getCountryCode();
                                        mobile = phoneNumber.getNationalNumber() + "";
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }

                                Amount_show = eventSessionData.getAmount();
                                startPayment(eventSessionData.getAmount() * 100, eventSessionData.getEvent_name(), name, email, mobile, countryCode);
                                //startPayment(1 * 100, eventSessionData.getEvent_name(), email, mobile);
                            }
                        }

                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            });

            sp_delivery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                /*btn_continue.setFocusable(true);
                btn_continue.setFocusableInTouchMode(true);*/
                    btn_continue.requestFocus();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            sp_address.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                    try {

                        //etAdrsFullName.setFocusable(true);
                        //etAdrsFullName.setFocusableInTouchMode(true);
                        etAdrsFullName.requestFocus();

                        if (mAddressList.get(position).Add_Type.equalsIgnoreCase("Add New Address")) {
                            selectedAddressId = 0;
                            enable();

                        } else {
                            UserAddress address = mAddressList.get(position);
                            selectedAddressId = address.Address_Id;
                            etAdrsFullName.setText(Validate.isNotNull(address.getFullName()) ? address.getFullName() : "");
                            etAdrsLine1.setText(Validate.isNotNull(address.getAline1()) ? address.getAline1() : "");
                            etAdrsLine2.setText(Validate.isNotNull(address.getAline2()) ? address.getAline2() : "");
                            etAdrsPinCode.setText(Validate.isNotNull(address.getPincode()) ? address.getPincode() : "");
                            etAdrsCity.setText(Validate.isNotNull(address.getCity()) ? address.getCity() : "");
                            etAdrsState.setText(Validate.isNotNull(address.getState()) ? address.getState() : "");
                            etAdrsMobile.setText(Validate.isNotNull(address.getMobileNo()) ? address.getMobileNo() : "");
                            etAdrsType.setText(Validate.isNotNull(address.getAddressType()) ? address.getAddressType() : "");

                            disable();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(String.format("%s", "PURCHASE TICKET"));
                Utility.convertToLowerCase(tvTitle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findViewById() {
        try {
            progressBar = findViewById(R.id.progressBar);
            btn_continue = findViewById(R.id.btn_continue);
            btn_paytm = findViewById(R.id.btn_paytm);
            sp_address = findViewById(R.id.sp_address);
            sp_delivery = findViewById(R.id.sp_delivery);
            tvTimerCountDown = findViewById(R.id.tv_count_down);
            tv_terms_and_condition = findViewById(R.id.tv_terms_and_condition);
            tv_rule = findViewById(R.id.tv_rule);
            rb_online = findViewById(R.id.rb_online);
            rb_cod = findViewById(R.id.rb_cod);
            ll_cod = findViewById(R.id.ll_cod);
            etEmail = findViewById(R.id.et_email);
            etMobile = findViewById(R.id.et_mobile_no);
            etAdrsFullName = findViewById(R.id.etAdrsFullName);
            etAdrsLine1 = findViewById(R.id.etAdrsLine1);
            etAdrsLine2 = findViewById(R.id.etAdrsLine2);
            etAdrsPinCode = findViewById(R.id.etAdrsPinCode);
            etAdrsCity = findViewById(R.id.etAdrsCity);
            etAdrsState = findViewById(R.id.etAdrsState);
            etAdrsMobile = findViewById(R.id.etAdrsMobile);
            etAdrsType = findViewById(R.id.etAdrsType);
            etCountryCode = findViewById(R.id.et_countrycode);
            etCountryCode.setInputType(InputType.TYPE_NULL);

            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);

            etEmail.setTypeface(tfMedium);
            etMobile.setTypeface(tfMedium);
            etCountryCode.setTypeface(tfMedium);
            etAdrsFullName.setTypeface(tfMedium);
            etAdrsLine1.setTypeface(tfMedium);
            etAdrsLine2.setTypeface(tfMedium);
            etAdrsPinCode.setTypeface(tfMedium);
            etAdrsCity.setTypeface(tfMedium);
            etAdrsState.setTypeface(tfMedium);
            etAdrsMobile.setTypeface(tfMedium);
            etAdrsType.setTypeface(tfMedium);

            Utility.setEditTextSingleLine(etEmail);
            Utility.setEditTextSingleLine(etMobile);
            Utility.setEditTextSingleLine(etAdrsFullName);
            Utility.setEditTextSingleLine(etAdrsLine1);
            Utility.setEditTextSingleLine(etAdrsLine2);
            Utility.setEditTextSingleLine(etAdrsPinCode);
            Utility.setEditTextSingleLine(etAdrsCity);
            Utility.setEditTextSingleLine(etAdrsState);
            Utility.setEditTextSingleLine(etAdrsMobile);
            Utility.setEditTextSingleLine(etAdrsType);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void invalidateData() {
        try {

            if (mLogin.isLoggedIn()) {
                etEmail.setText(Validate.isNotNull(mLogin.getEmail()) ? mLogin.getEmail() : "");
                etMobile.setText(Validate.isNotNull(mLogin.getMobile()) ? mLogin.getMobile() : "");
                etAdrsFullName.setText(Validate.isNotNull(mLogin.getName()) ? mLogin.getName() : "");
                etAdrsMobile.setText(Validate.isNotNull(mLogin.getMobile()) ? mLogin.getMobile() : "");
            }

            if (eventPaymentNoteList != null && eventPaymentNoteList.size() > 0) {
                List<String> noteList = new ArrayList<>();
                for (int i = 0; i < eventPaymentNoteList.size(); i++)
                    noteList.add((i + 1) + ". " + eventPaymentNoteList.get(i));
                tv_rule.setText(TextUtils.join("\n", noteList));
            } else {
                tv_rule.setVisibility(View.GONE);
            }

            //btn_paytm.setVisibility((eventSessionData.isPaytmPaymentAllow() && (!eventSessionData.isRSVP() && !eventSessionData.isInviteRSVP())) ? View.VISIBLE : View.GONE);
            // btn_continue.setText((eventSessionData.isRazorpayPaymentAllow() && (eventSessionData.isRSVP() || eventSessionData.isInviteRSVP())) ? R.string.event_btn_rsvp : R.string.btn_pay_now);
            btn_paytm.setVisibility((eventSessionData.isPaytmPaymentAllow() && (Constant.Code.ONE == eventSessionData.isRSVP() && Constant.Code.ONE == eventSessionData.isInviteRSVP())) ? View.VISIBLE : View.GONE);
            btn_continue.setText((eventSessionData.isRazorpayPaymentAllow() && (Constant.Code.ONE == eventSessionData.isRSVP() || Constant.Code.ONE == eventSessionData.isInviteRSVP())) ? R.string.event_btn_rsvp : R.string.btn_pay_now);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showHideForm() {
        try {
            if (rb_cod.isChecked()) {
                ll_cod.setVisibility(View.VISIBLE);
                btn_continue.setText(R.string.btn_continue);
            } else {
                ll_cod.setVisibility(View.GONE);
                btn_continue.setText(R.string.btn_pay_now);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //................................VALIDATIONS...........................//
    private boolean checkValidation() {
        String email = etEmail.getText().toString().trim();
        String mobile = etMobile.getText().toString().trim();
        String countryCode = etCountryCode.getText().toString().trim();

        if (Validate.isNull(email)) {
            CC.showToast(R.string.common_msg_enter_email);
            etEmail.setFocusable(true);
            etEmail.requestFocus();
            return false;

        } else if (!Validate.checkEmail(email)) {
            CC.showToast(R.string.common_msg_enter_email_valid);
            etEmail.setFocusable(true);
            etEmail.requestFocus();
            return false;

        } else if (Validate.isNull(countryCode)) {
            CC.showToast(R.string.common_msg_enter_country_code);
            etCountryCode.setFocusable(true);
            etCountryCode.requestFocus();
            return false;

        } else if (Validate.isNull(mobile)) {
            CC.showToast(R.string.common_msg_enter_mobile);
            etMobile.setFocusable(true);
            etMobile.requestFocus();
            return false;

        /*} else if (mobile.length() != 10) {
            CC.showToast(R.string.common_msg_enter_mobile_valid);
            etMobile.setFocusable(true);
            etMobile.requestFocus();
            return false;*/

        /*} else if (!isValidPhoneNumber(mobile)) {
            CC.showToast(R.string.common_msg_enter_mobile_valid);
            etMobile.requestFocus();
            return false;*/

        } else if (!Validate.checkPhone_libPhoneNumber(countryCode, mobile)) {
            CC.showToast(R.string.common_msg_enter_mobile_valid);
            etMobile.requestFocus();
            return false;

        } else {
            return true;
        }

    }

    public boolean checkValidationMobileEmail() {
        String email = etEmail.getText().toString().trim();
        String mobile = etMobile.getText().toString().trim();
        String countryCode = etCountryCode.getText().toString().trim();

        if (Validate.isNull(email)) {
            CC.showToast(R.string.login_msg_enter_email);
            etEmail.requestFocus();
            return false;

        } else if (!Validate.checkEmail(email)) {
            CC.showToast(R.string.login_msg_enter_email_valid);
            etEmail.requestFocus();
            return false;

        } else if (Validate.isNull(countryCode)) {
            CC.showToast(R.string.common_msg_enter_country_code);
            etCountryCode.requestFocus();
            return false;

        } else if (Validate.isNull(mobile)) {
            CC.showToast(R.string.login_msg_enter_mobile);
            etMobile.requestFocus();
            return false;

        /*} else if (mobile.length() != 10) {
            CC.showToast(R.string.common_msg_enter_mobile_valid);
            etMobile.requestFocus();
            return false;*/

        /*} else if (!isValidPhoneNumber(mobile)) {
            CC.showToast(R.string.common_msg_enter_mobile_valid);
            etMobile.requestFocus();
            return false;*/

        } else if (!Validate.checkPhone_libPhoneNumber(countryCode, mobile)) {
            CC.showToast(R.string.common_msg_enter_mobile_valid);
            etMobile.requestFocus();
            return false;

        } else {
            return true;
        }
    }

    public boolean checkValidationCOD() {
        String fullName = etAdrsFullName.getText().toString().trim();
        String address1 = etAdrsLine1.getText().toString().trim();
        //String address2 = etAdrsLine2.getText().toString().trim();
        String pinCode = etAdrsPinCode.getText().toString().trim();
        String city = etAdrsCity.getText().toString().trim();
        String state = etAdrsState.getText().toString().trim();
        String mobileNo = etAdrsMobile.getText().toString().trim();
        String addressType = etAdrsType.getText().toString().trim();

        if (Validate.isNull(fullName)) {
            CC.showToast(R.string.address_msg_enter_full_name);
            //etAdrsFullName.setFocusable(true);
            etAdrsFullName.requestFocus();
            return false;

        } else if (Validate.isNull(address1)) {
            CC.showToast(R.string.address_msg_enter_address1);
            //etAdrsLine1.setFocusable(true);
            etAdrsLine1.requestFocus();
            return false;

        } else if (Validate.isNull(pinCode)) {
            CC.showToast(R.string.address_msg_enter_pincode);
            //etAdrsPinCode.setFocusable(true);
            etAdrsPinCode.requestFocus();
            return false;

        } else if (pinCode.length() != 6) {
            CC.showToast(R.string.address_msg_enter_pincode_valid);
            //etAdrsPinCode.setFocusable(true);
            etAdrsPinCode.requestFocus();
            return false;

        } else if (Validate.isNull(city)) {
            CC.showToast(R.string.address_msg_enter_city);
            //etAdrsCity.setFocusable(true);
            etAdrsCity.requestFocus();
            return false;

        } else if (Validate.isNull(state)) {
            CC.showToast(R.string.address_msg_enter_state);
            //etAdrsState.setFocusable(true);
            etAdrsState.requestFocus();
            return false;

        } else if (Validate.isNull(mobileNo)) {
            CC.showToast(R.string.address_msg_enter_mobile);
            //etAdrsMobile.setFocusable(true);
            etAdrsMobile.requestFocus();
            return false;

        } else if (mobileNo.length() != 10) {
            CC.showToast(R.string.address_msg_enter_mobile_valid);
            etAdrsMobile.requestFocus();
            return false;

        } else if (Validate.isNull(addressType)) {
            CC.showToast(R.string.address_msg_enter_address_type);
            //etAdrsMobile.setFocusable(true);
            etAdrsMobile.requestFocus();
            return false;

        } else {
            return true;
        }
    }

    public void enable() {
        try {
            etAdrsFullName.setEnabled(true);
            etAdrsLine1.setEnabled(true);
            etAdrsLine2.setEnabled(true);
            etAdrsPinCode.setEnabled(true);
            etAdrsCity.setEnabled(true);
            etAdrsState.setEnabled(true);
            //etAdrsMobile.setEnabled(true);
            etAdrsType.setEnabled(true);

            etAdrsFullName.setText("");
            etAdrsLine1.setText("");
            etAdrsLine2.setText("");
            etAdrsPinCode.setText("");
            etAdrsCity.setText("");
            etAdrsState.setText("");
            etAdrsFullName.setText(mLogin != null && Validate.isNotNull(mLogin.getName()) ? mLogin.getName() : "");
            etAdrsMobile.setText(mLogin != null && Validate.isNotNull(mLogin.getMobile()) ? mLogin.getMobile() : "");
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    public void disable() {
        try {
            etAdrsFullName.setEnabled(false);
            etAdrsLine1.setEnabled(false);
            etAdrsLine2.setEnabled(false);
            etAdrsPinCode.setEnabled(false);
            etAdrsCity.setEnabled(false);
            etAdrsState.setEnabled(false);
            //etAdrsMobile.setEnabled(false);
            etAdrsType.setEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {
        try {

            Utility.hideKeyboard(this);
            //stopService(new Intent(PurchaseTicketActivity.this, TimerService.class));
            super.onBackPressed();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        try {

            if (mTimer != null) {
                mTimer.cancel();
                //mTimer.purge();
                mTimer = null;
            }
            super.onDestroy();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void startPayment(double amount, String eventName, String name, String email, String mobile, String countryCode) {
        Log.e(TAG, "amount=" + amount + "  eventName=" + eventName + "  name=" + name + "  email=" + email + "  mobile=" + mobile + "  countryCode=" + countryCode);
        //
        //* Instantiate Checkout
        //
        //Checkout checkout = new Checkout();
        // checkout.setFullScreenDisable(true);
        /*
         * Set your logo here
         */
        // checkout.setImage(R.drawable.ic_launcher);

        /*
         * Reference to current activity
         */

        /*
         * Pass your payment options to the Razorpay Checkout as a JSONObject
         */
        try {
            JSONObject options = new JSONObject();
            options.put("categoryName", "Ticket Ninja");
            options.put("description", "" + eventName);
            //options.put("notes", "App_Android_Session_Id=" + eventSessionData.getId());
            options.put("currency", "INR");
            options.put("amount", "" + amount);
            options.put("receipt", "TicketNinja-" + eventSessionData.getId());

            JSONObject externals = new JSONObject();
            externals.put("wallets", new String[]{"paytm"});
            options.put("external", externals);

            JSONObject notes = new JSONObject();
            notes.put("App_Android_Session_Id", "" + eventSessionId);
            options.put("notes", notes);

            JSONObject preFill = new JSONObject();
            preFill.put("name", "" + name);
            preFill.put("email", "" + email);
            preFill.put("contact", "" + mobile);
            //preFill.put("country_code", "" + countryCode);
            options.put("preFill", preFill);

            JSONObject theme = new JSONObject();
            theme.put("color", "#F53033");
            options.put("theme", theme);

            // checkout.open(activity, options);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    //...................WS FOR ADDRESS_CODE LIST AND TIME SLOT ...........//

    // WS - GET COD ADDRESS_CODE & TIME SLOTS
    private void wsGetDataForCOD() {
        try {

            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestPaymentCODdate request = new RequestPaymentCODdate();
            request.user_id = String.valueOf(mLogin.getUserId());

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackPaymentCODdate> call = tnAPI.getPaymentCODdate(request);
            call.enqueue(new Callback<CallbackPaymentCODdate>() {
                @Override
                public void onResponse(@NonNull Call<CallbackPaymentCODdate> call, @NonNull Response<CallbackPaymentCODdate> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {

                        mTimingList = Objects.requireNonNull(response.body()).data.Delivery_Time_Slot;
                        mAddressList = new ArrayList<>();
                        List<UserAddress> addressList = Objects.requireNonNull(response.body()).data.User_Address;

                        SpinnerCODTiming spinnerCOD = new SpinnerCODTiming(PurchaseTicketActivity.this, mTimingList);
                        sp_delivery.setAdapter(spinnerCOD);

                        mAddressList.add(new UserAddress(mLogin.getUserId(), "Add New Address"));
                        mAddressList.addAll(addressList);
                        SpinnerAddress spinnerAddress = new SpinnerAddress(PurchaseTicketActivity.this, mAddressList);
                        sp_address.setAdapter(spinnerAddress);

                    } else {
                        CC.showAlert(R.string.msg_something_wrong);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackPaymentCODdate> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }


    }

    // WS - Send COD OTP
    private void wsCOD_OTP_Send(String countryCode, String mobile, String email, String sessionId, String currency) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();
            otpVerificationDialog = new OTPVerificationDialog(this);

            RequestCOD_OTP_Send request = new RequestCOD_OTP_Send(countryCode, mobile, email, sessionId, currency);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackCOD_OTP_Send> call = tnAPI.COD_OTP_Send(request);
            call.enqueue(new Callback<CallbackCOD_OTP_Send>() {
                @Override
                public void onResponse(@NonNull Call<CallbackCOD_OTP_Send> call, @NonNull Response<CallbackCOD_OTP_Send> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                            if (otpVerificationDialog != null) {
                                otpVerificationDialog.setOnOTPVerificationListener(new OTPVerificationDialog.OnOTPVerificationListener() {
                                    @Override
                                    public void onSuccess(@NonNull String otp) {
                                        if (CC.isOnline()) {
                                            //wsCOD_OTP_Check(eventSessionId, otp);
                                            String fullName = etAdrsFullName.getText().toString().trim();
                                            String address1 = etAdrsLine1.getText().toString().trim();
                                            String address2 = etAdrsLine2.getText().toString().trim();
                                            String pinCode = etAdrsPinCode.getText().toString().trim();
                                            String city = etAdrsCity.getText().toString().trim();
                                            String country = "";
                                            String state = etAdrsState.getText().toString().trim();
                                            String mobileNo = etAdrsMobile.getText().toString().trim();
                                            String addressType = etAdrsType.getText().toString().trim();

                                        /*wsUserAddressInsertUpdate(mLogin.getUserId(), selectedAddressId, fullName, address1,
                                                address2, city, state, country, pinCode, mobileNo, add_Type);*/

                                            UserAddress userAddress = new UserAddress(mLogin.getUserId(), selectedAddressId, addressType,
                                                    fullName, address1, address2, city, state, country, pinCode, mobileNo,
                                                    selectedAddressId > 0 ? Constant.RegisterMode.UPDATE : Constant.RegisterMode.INSERT);

                                            String email = etEmail.getText().toString().trim();
                                            String countryCode = etCountryCode.getText().toString().trim();
                                            String mobile = etMobile.getText().toString().trim();
                                            String sessionId = eventSessionId;
                                            String paymentId = "0";
                                            String slotTime = mTimingList.get(sp_delivery.getSelectedItemPosition()).D_Time;
                                            try {
                                                if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                                                    Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile, Constant.DEFAULT_COUNTRY_REGION);
                                                    countryCode = "+" + phoneNumber.getCountryCode();
                                                    mobile = phoneNumber.getNationalNumber() + "";
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            wsInsertPlaceOrder(mLogin.getUserId(), sessionId, Constant.PaymentType.COD,
                                                    paymentId, selectedAddressId, slotTime, countryCode, mobile, email, otp, userAddress);
                                        } else {
                                            CC.showToast(R.string.msg_no_internet);
                                        }
                                    }

                                    @Override
                                    public void onFail() {
                                        Utility.hideKeyboard(PurchaseTicketActivity.this);
                                    }

                                    @Override
                                    public void onCancel() {
                                        Utility.hideKeyboard(PurchaseTicketActivity.this);
                                    }

                                    @Override
                                    public void onResendClick() {
                                        if (CC.isOnline()) {
                                            String email = etEmail.getText().toString().trim();
                                            String countryCode = etCountryCode.getText().toString().trim();
                                            String mobile = etMobile.getText().toString().trim();
                                            String sessionId = eventSessionId;
                                            String currency = "INR";
                                            try {
                                                if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                                                    Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile, Constant.DEFAULT_COUNTRY_REGION);
                                                    countryCode = "+" + phoneNumber.getCountryCode();
                                                    mobile = phoneNumber.getNationalNumber() + "";
                                                }
                                            } catch (Exception e) {
                                                e.printStackTrace();
                                            }
                                            wsCOD_OTP_Send(countryCode, mobile, email, sessionId, currency);

                                        } else {
                                            CC.showToast(R.string.msg_no_internet);
                                        }
                                    }
                                });
                                otpVerificationDialog.show();
                            }
                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                            CC.showAlert(Objects.requireNonNull(response.body()).getError_description());
                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackCOD_OTP_Send> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });

        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }


    }

    private void rsvpEvent() {
        try {
            if (CC.isOnline()) {

                String email = etEmail.getText().toString().trim();
                String countryCode = etCountryCode.getText().toString().trim();
                String mobile = etMobile.getText().toString().trim();
                String sessionId = eventSessionId;
                try {
                    if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                        Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile, Constant.DEFAULT_COUNTRY_REGION);
                        countryCode = "+" + phoneNumber.getCountryCode();
                        mobile = phoneNumber.getNationalNumber() + "";
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                wsInsertPlaceOrder(mLogin.getUserId(), sessionId, Constant.PaymentType.RSVP,
                        "", 0, "", countryCode, mobile, email, "", null);
            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // WS - Insert Place Order
    private void wsInsertPlaceOrder(String user_Id, String session_Id, @Constant.PaymentWSType String payment_Type,
                                    String payment_Id, long address_Id, String d_Time_Slot,
                                    String countryCode, String mobileNo, String email, String codOTP, UserAddress userAddress) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestInsertPlaceOrder request;
            if (payment_Type.toLowerCase().equals(Constant.PaymentType.RSVP.toLowerCase())) {
                request = new RequestInsertPlaceOrder(String.valueOf(user_Id), session_Id, payment_Type, countryCode, mobileNo, email);
            } else {
                request = new RequestInsertPlaceOrder(String.valueOf(user_Id), session_Id, payment_Type,
                        payment_Id, address_Id, d_Time_Slot, countryCode, mobileNo, email, codOTP, userAddress);
            }

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackInsertPlaceOrder> call = tnAPI.InsertPlaceOrder(request);
            call.enqueue(new Callback<CallbackInsertPlaceOrder>() {
                @Override
                public void onResponse(@NonNull Call<CallbackInsertPlaceOrder> call, @NonNull Response<CallbackInsertPlaceOrder> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            if (otpVerificationDialog != null) otpVerificationDialog.dismiss();

                            stopTimerService();

                            long transId = Objects.requireNonNull(response.body()).data().getTransId();

                            Intent intConfirm = new Intent(getApplicationContext(), BookingConfirmationActivity.class);
                            intConfirm.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE, payment_Type);
                            intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, transId);
                            startActivity(intConfirm);

                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).data().message())) {
                            if (otpVerificationDialog != null && otpVerificationDialog.isShowing())
                                otpVerificationDialog.reset();
                            CC.showAlert(Objects.requireNonNull(response.body()).getError_description());

                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackInsertPlaceOrder> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }


    }

    private void wsInsertPlaceOrder(String user_Id, String session_Id, String orderId, @Constant.PaymentWSType String payment_Type,
                                    @Constant.MerchantWSType String merchant,
                                    String payment_Id, String countryCode, String mobileNo, String email) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestInsertPlaceOrder request = new RequestInsertPlaceOrder(String.valueOf(user_Id), session_Id, orderId,
                    payment_Type, merchant, payment_Id, countryCode, mobileNo, email);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackInsertPlaceOrder> call = tnAPI.InsertPlaceOrder(request);
            call.enqueue(new Callback<CallbackInsertPlaceOrder>() {
                @Override
                public void onResponse(@NonNull Call<CallbackInsertPlaceOrder> call, @NonNull Response<CallbackInsertPlaceOrder> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            if (otpVerificationDialog != null) otpVerificationDialog.dismiss();

                            long transId = Objects.requireNonNull(response.body()).data().getTransId();

                            stopTimerService();
                            //mTimer.cancel();

                            PaymentSuccessDialog successDialog = new PaymentSuccessDialog(PurchaseTicketActivity.this);
                            successDialog.setTransactionId(String.valueOf(transId));
                            if (Amount_show == 0) {
                                successDialog.setAmount(R.string.str_price_free);
                            } else {
                                successDialog.setAmount(String.format(Locale.getDefault(), "₹ %.2f", Amount_show));
                            }
                            successDialog.setOnOkClickListener(() -> {
                                Intent intConfirm = new Intent(getApplicationContext(), BookingConfirmationActivity.class);
                                intConfirm.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE, payment_Type);
                                intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, transId);
                                startActivity(intConfirm);

                            });

                            if (!isFinishing()) successDialog.show();

                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                            if (otpVerificationDialog != null && otpVerificationDialog.isShowing())
                                otpVerificationDialog.reset();
                            CC.showAlert(Objects.requireNonNull(response.body()).getError_description());

                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackInsertPlaceOrder> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }


    }
/*
    @Override
    public void onPaymentSuccess(String razorPayPaymentID, PaymentData data) {
        *//*String paymentId = data.getPaymentId();
        String signature = data.getData().toString();
        String orderId = data.getOrderId();*//*

        try {
            Answers.getInstance().logPurchase(new PurchaseEvent()
                    .putItemPrice(BigDecimal.valueOf(eventSessionData.getAmount()))
                    .putCurrency(Currency.getInstance("INR"))
                    .putItemName(eventSessionData.getEvent_name())
                    .putItemType(eventSessionData.eventType())
                    .putItemId("" + eventSessionData.getId())
                    .putCustomAttribute("paymentID", "" + razorPayPaymentID)
                    .putSuccess(true));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (CC.isOnline()) {
            String email = etEmail.getText().toString().trim();
            String countryCode = etCountryCode.getText().toString().trim();
            String mobile = etMobile.getText().toString().trim();
            String sessionId = eventSessionId;
            try {
                if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                    Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile, Constant.DEFAULT_COUNTRY_REGION);
                    countryCode = "+" + phoneNumber.getCountryCode();
                    mobile = phoneNumber.getNationalNumber() + "";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            wsInsertPlaceOrder(mLogin.getUserId(), sessionId, eventSessionId, Constant.PaymentType.ONLINE,
                    Constant.MerchantType.RAZORPAY,
                    razorPayPaymentID, countryCode, mobile, email);

        } else {
            CC.showToast(R.string.msg_no_internet);
        }
    }

    @Override
    public void onPaymentError(int code, String response, PaymentData data) {
        try {
            if (code > 0) CC.showAlert("Payment Failure :", response);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Answers.getInstance().logPurchase(new PurchaseEvent()
                    .putItemPrice(BigDecimal.valueOf(eventSessionData.getAmount()))
                    .putCurrency(Currency.getInstance("INR"))
                    .putItemName(eventSessionData.getEvent_name())
                    .putItemType(eventSessionData.eventType())
                    .putItemId("" + eventSessionData.getId())
                    .putCustomAttribute("errorCode", "" + code)
                    .putCustomAttribute("errorMessage", "" + response)
                    .putSuccess(false));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }*/

    private void startTimerService() {
        try {
            // start service
            MyEvent myEvent = new MyEvent();
            myEvent.event_id = eventSessionData.getId();
            myEvent.Poster_Image_Id = eventSessionData.getImageId();
            myEvent.poster_image_url = eventSessionData.getImageUrl();

            timerServiceIntent = new Intent(this, TimerService.class);
            timerServiceIntent.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
            startService(timerServiceIntent);
            registerReceiver(timerBroadcastReceiver, new IntentFilter(TimerService.BROADCAST_ACTION));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopTimerService() {
        try {
            if (timerBroadcastReceiver != null) {
                unregisterReceiver(timerBroadcastReceiver);
                timerBroadcastReceiver = null;
            }
            if (timerServiceIntent != null) {
                stopService(timerServiceIntent);
                timerServiceIntent = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onStopTimerService() {
        try {
            progressBar.setProgress(0);
            tvTimerCountDown.setText(R.string.label_done);
            Log.d(TAG, "Finished ");
            CC.showAlert(R.string.purchase_ticket_msg_session_timeout, () -> {

                MyEvent myEvent = new MyEvent();
                myEvent.event_id = eventSessionData.getId();
                myEvent.Poster_Image_Id = eventSessionData.getImageId();
                myEvent.poster_image_url = eventSessionData.getImageUrl();

                Intent i1 = new Intent(PurchaseTicketActivity.this, EventDetailActivity.class);
                i1.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                i1.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_PAYMENT);
                i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(i1);

            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private BroadcastReceiver timerBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            try {
                long counter = intent.getLongExtra("countdown", 0);
                int percentage = intent.getIntExtra("percentage", 0);
                boolean in_progress = intent.getBooleanExtra("in_progress", true);
                String string_to_show = intent.getStringExtra("string_to_show");

                if (in_progress) {
                    progressBar.setProgress(percentage);
                    tvTimerCountDown.setText(string_to_show);
                } else {
                    onStopTimerService();
                }
                Log.d(TAG, "---------------------------------------");
                Log.d(TAG, "counter == " + counter);
                Log.d(TAG, "percentage == " + percentage);
                Log.d(TAG, "in_progress == " + in_progress);
                Log.d(TAG, "string_to_show == " + string_to_show);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    };



    /*private BroadcastReceiver SMSReceiver = new BroadcastReceiver() {
        private static final String TAG = "SMSListener";

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("android.provider.Telephony.SMS_RECEIVED")) {
                Bundle bundle = intent.getExtras();

                SmsMessage[] msgs;
                //---retrieve the SMS message received---
                try {
                    Object[] pdus = (Object[]) bundle.get("pdus");
                    msgs = new SmsMessage[pdus.length];
                    for (int i = 0; i < msgs.length; i++) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String format = bundle.getString("format");
                            msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                        } else {
                            msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        }

                        String msg_from = msgs[i].getOriginatingAddress();
                        if (msg_from.contains("TNINJA")) {
                            String msgBody = msgs[i].getMessageBody();
                            if (msgBody.contains("Your Ticket Ninja Verification code is ")) {
                                String replace = msgBody.replace("Your Ticket Ninja Verification code is ", "");
                                if (otpVerificationDialog != null && otpVerificationDialog.isShowing()) {
                                    otpVerificationDialog.setOTP(String.format("%s", replace));
                                    otpVerificationDialog.performSubmitButton();
                                }
                            }
                            break;
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
//                            Log.d("Exception caught",e.getMessage());
                }
            }
        }
    };*/

    @Override
    protected void onResume() {
        try {
            // TIMER SERVICE
            if (timerServiceIntent != null && !Utility.isMyServiceRunning(this, TimerService.class)) {
                try {
                    timerBroadcastReceiver = null;

                    onStopTimerService();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (timerBroadcastReceiver != null && Utility.isMyServiceRunning(this, TimerService.class))
                registerReceiver(timerBroadcastReceiver, new IntentFilter(TimerService.BROADCAST_ACTION));

            // SMS SERVICE
            if (ContextCompat.checkSelfPermission(PurchaseTicketActivity.this,
                    Manifest.permission.READ_SMS) == PackageManager.PERMISSION_GRANTED) {

                flag = 1;
                // registerReceiver(SMSReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
            } /*else {
            ActivityCompat.requestPermissions(RegisterActivityPart1.this,
                    new String[]{Manifest.permission.READ_SMS},
                    Constant.PERMISSION_READ_SMS);
        }*/
        } catch (Exception e) {
            e.printStackTrace();
        }


        super.onResume();
    }

    @Override
    public void onStop() {
        Log.d(TAG, "onStop");
        try {
            if (timerBroadcastReceiver != null) unregisterReceiver(timerBroadcastReceiver);
            super.onStop();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    protected void onPause() {
        super.onPause();
        try {

            //if (timerBroadcastReceiver != null) unregisterReceiver(timerBroadcastReceiver);
            if (flag == 1) {
                // unregisterReceiver(SMSReceiver);
                flag = 0;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void arePermissionGranted() {
        try {
            flag = 1;
            //registerReceiver(SMSReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
            if (CC.isOnline()) {
                if (checkValidationMobileEmail() && checkValidationCOD()) {
                    String email = etEmail.getText().toString().trim();
                    String countryCode = etCountryCode.getText().toString().trim();
                    String mobile = etMobile.getText().toString().trim();
                    try {
                        if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                            Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile, Constant.DEFAULT_COUNTRY_REGION);
                            countryCode = "+" + phoneNumber.getCountryCode();
                            mobile = phoneNumber.getNationalNumber() + "";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String sessionId = eventSessionId;
                    String currency = "INR";

                    wsCOD_OTP_Send(countryCode, mobile, email, sessionId, currency);

                }

            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


   /* private void arePermissionGranted() {
        if (ContextCompat.checkSelfPermission(PurchaseTicketActivity.this,
                Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED) {
           *//* ActivityCompat.requestPermissions(PurchaseTicketActivity.this,
                    new String[]{android.Manifest.permission.READ_SMS},
                    Constant.PERMISSION_READ_SMS);*//*
            ActivityCompat.requestPermissions(PurchaseTicketActivity.this,
                    new String[]{Manifest.permission.READ_SMS},
                    Utility.REQUEST_PERMISSION_CALL_SMS);
        } else {
            flag = 1;
            registerReceiver(SMSReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));

            if (CC.isOnline()) {
                if (checkValidationMobileEmail() && checkValidationCOD()) {
                    String email = etEmail.getText().toString().trim();
                    String countryCode = etCountryCode.getText().toString().trim();
                    String mobile = etMobile.getText().toString().trim();
                    try {
                        if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                            Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile, Constant.DEFAULT_COUNTRY_REGION);
                            countryCode = "+" + phoneNumber.getCountryCode();
                            mobile = phoneNumber.getNationalNumber() + "";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    String sessionId = eventSessionId;
                    String currency = "INR";

                    wsCOD_OTP_Send(countryCode, mobile, email, sessionId, currency);

                }

            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        }

    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        /*
        if (requestCode == Utility.REQUEST_PERMISSION_CALL_SMS) {
            if (grantResults.length > 0) {
                registerReceiver(SMSReceiver, new IntentFilter("android.provider.Telephony.SMS_RECEIVED"));
            }

            if (CC.isOnline()) {
                if (checkValidationMobileEmail() && checkValidationCOD()) {
                    String email = etEmail.getText().toString().trim();
                    String countryCode = etCountryCode.getText().toString().trim();
                    String mobile = etMobile.getText().toString().trim();
                    String sessionId = eventSessionId;
                    String currency = "INR";
                    try {
                        if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                            Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile, Constant.DEFAULT_COUNTRY_REGION);
                            countryCode = "+" + phoneNumber.getCountryCode();
                            mobile = phoneNumber.getNationalNumber() + "";
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    wsCOD_OTP_Send(countryCode, mobile, email, sessionId, currency);
                }
            } else {
                CC.showToast(R.string.msg_no_internet);
            }

        }*/
    }

    private void startPaytm(String mid, String orderId, String custId, String industryTypeId,
                            String channelId, String website, String amount, String paytmChecksum,
                            String callbackUrl, String email, String mobile, boolean isPaytmLive) {

        try {
            HashMap<String, String> paramMap = new HashMap<>();
            paramMap.put("MID", mid);
            paramMap.put("ORDER_ID", orderId + "");
            paramMap.put("CUST_ID", custId + "");
            paramMap.put("INDUSTRY_TYPE_ID", industryTypeId);
            paramMap.put("CHANNEL_ID", channelId);
            paramMap.put("TXN_AMOUNT", amount);
            paramMap.put("WEBSITE", website);
            paramMap.put("CALLBACK_URL", callbackUrl);
            paramMap.put("EMAIL_CODE", email);
            paramMap.put("MOBILE_NO_CODE", mobile);
            paramMap.put("CHECKSUMHASH", paytmChecksum);

            PaytmOrder Order = new PaytmOrder(paramMap);
            PaytmPGService service = PaytmPGService.getStagingService();
            if (isPaytmLive) service = PaytmPGService.getProductionService();

            service.enableLog(this);
            service.initialize(Order, null);
            service.startPaymentTransaction(this, true, false, new PaytmPaymentTransactionCallback() {
                @Override
                public void onTransactionResponse(Bundle inResponse) {
                    Log.d(TAG, "Paytm Payment Transaction : " + inResponse);
                    //Toast.makeText(getApplicationContext(), "Payment Transaction response " + inResponse.toString(), Toast.LENGTH_LONG).show();

                    String status = inResponse.getString("STATUS", "");
                    if (status.equalsIgnoreCase("TXN_SUCCESS")) {
                    /*long userId = mLogin.getUserId();
                    String payStatus = Constant.PAYMENT_SUCCESS_TITLE;
                    String txnamnt = inResponse.getString("TXNAMOUNT", "");
                    String txndate = inResponse.getString("TXNDATE", "");
                    String MID = inResponse.getString("MID", "");
                    String respcode = inResponse.getString("RESPCODE", "");
                    String bankName = inResponse.getString("BANKNAME", "");
                    String paymentmode = inResponse.getString("PAYMENTMODE", "");
                    String banktxnid = inResponse.getString("BANKTXNID", "");
                    String currency = inResponse.getString("CURRENCY", "INR");
                    String gatwayname = inResponse.getString("GATEWAYNAME", "");
                    String IS_CHECKSUM_VALID = inResponse.getString("IS_CHECKSUM_VALID", "Y");
                    String respmsg = inResponse.getString("RESPMSG", "");*/

                        String txnid = inResponse.getString("TXNID", "");
                        String orderId = inResponse.getString("ORDERID", "");

                        if (CC.isOnline()) {
                            String email = etEmail.getText().toString().trim();
                            String countryCode = etCountryCode.getText().toString().trim();
                            String mobile = etMobile.getText().toString().trim();
                            try {
                                if (Validate.isNotNull(countryCode) && Validate.isNotNull(mobile)) {
                                    Phonenumber.PhoneNumber phoneNumber = phoneNumberUtil.parse(countryCode + mobile,
                                            Constant.DEFAULT_COUNTRY_REGION);
                                    countryCode = "+" + phoneNumber.getCountryCode();
                                    mobile = phoneNumber.getNationalNumber() + "";
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            wsInsertPlaceOrder(mLogin.getUserId(), eventSessionId, orderId, Constant.PaymentType.ONLINE,
                                    Constant.MerchantType.PAYTM, txnid, countryCode, mobile, email);
                       /* callPaymentRespondWS(userId, orderId, bankName, payStatus,
                                txnamnt, txndate, txnid, respcode, paymentmode,
                                banktxnid, currency, gatwayname, respmsg, status, IS_CHECKSUM_VALID);*/
                        } else {
                            CC.showAlert(R.string.payment_msg_success_no_internet,
                                    () -> {
                                        Intent intSuccess = new Intent(getApplicationContext(), LandingActivity.class);
                                        intSuccess.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK
                                                | Intent.FLAG_ACTIVITY_CLEAR_TOP
                                                | Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intSuccess);
                                    });
                        }

                        // After successful transaction this method gets called.
                        // // Response bundle contains the merchant response
                        // parameters.
                        Log.e(TAG, "Paytm Payment Transaction is successful " + inResponse);
                    } else {
                    /*long userId = mLogin.getUserId();
                    String orderId = inResponse.getString("ORDERID", "");
                    String txnamnt = inResponse.getString("TXNAMOUNT", "");
                    String txndate = inResponse.getString("TXNDATE", "");
                    String MID = inResponse.getString("MID", "");
                    String respcode = inResponse.getString("RESPCODE", "");
                    String bankName = inResponse.getString("BANKNAME", "");
                    String paymentmode = inResponse.getString("PAYMENTMODE", "");
                    String txnid = inResponse.getString("TXNID", "");
                    String banktxnid = inResponse.getString("BANKTXNID", "");
                    String currency = inResponse.getString("CURRENCY", "INR");
                    String gatwayname = inResponse.getString("GATEWAYNAME", "");
                    String IS_CHECKSUM_VALID = inResponse.getString("IS_CHECKSUM_VALID", "Y");*/

                        //String payStatus = Constant.PAYMENT_FAILED_TITLE;
                        String respMsg = inResponse.getString("RESPMSG", "");

                        if (Validate.isNotNull(respMsg)) {
                            CC.showAlert(respMsg);
                        }

                   /* if (status.equalsIgnoreCase("PENDING")) {
                        payStatus = Constant.PAYMENT_PENDING_TITLE;
                    }*/

                    /*callPaymentRespondWS(userId, orderId, bankName, payStatus,
                            txnamnt, txndate, txnid, respcode, paymentmode,
                            banktxnid, currency, gatwayname, respmsg, status, IS_CHECKSUM_VALID);*/

                        //Log.e(TAG, "Payment Transaction Failed " + inErrorMessage);
                        Log.e(TAG, "Paytm Payment Transaction Failed " + inResponse.toString());
                    }
                }

                @Override
                public void networkNotAvailable() {
                    Log.e(TAG, "Paytm networkNotAvailable ");
                    CC.showToast(R.string.payment_msg_no_internet);
                }

                @Override
                public void clientAuthenticationFailed(String inErrorMessage) {
                    Log.e(TAG, "Paytm clientAuthenticationFailed " + inErrorMessage);
                    CC.showToast(R.string.payment_msg_error);
                }

                @Override
                public void someUIErrorOccurred(String inErrorMessage) {
                    Log.e(TAG, "Paytm someUIErrorOccurred = " + inErrorMessage);
                    CC.showToast(R.string.payment_msg_error);
                }

                @Override
                public void onErrorLoadingWebPage(int iniErrorCode, String inErrorMessage, String inFailingUrl) {
                    Log.e(TAG, "Paytm onErrorLoadingWebPage " + iniErrorCode);
                    Log.e(TAG, "Paytm onErrorLoadingWebPage " + inErrorMessage);
                    Log.e(TAG, "Paytm onErrorLoadingWebPage " + inFailingUrl);
                    CC.showToast(R.string.payment_msg_error);
                }

                @Override
                public void onBackPressedCancelTransaction() {
                    Log.e(TAG, "Paytm onBackPressedCancelTransaction");
                    //callPaymentStatusUpdateWS(data.getOrderId(), Constant.PAYMENT_CANCELED_TITLE);
                    CC.showToast(R.string.payment_msg_cancel);
                }

                @Override
                public void onTransactionCancel(String s, Bundle bundle) {
                    Log.e(TAG, "Paytm onTransactionCancel");
                    //callPaymentStatusUpdateWS(data.getOrderId(), Constant.PAYMENT_CANCELED_TITLE);
                    CC.showToast(R.string.payment_msg_cancel);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    // WS - Generate Paytm Checksum
    private void wsGeneratePaytmChecksum(String userId, String sessionId, String txnAmount, String email, String mobileNo) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestGeneratePaytmChecksum request = new RequestGeneratePaytmChecksum(String.valueOf(userId), sessionId, txnAmount, email, mobileNo);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackGeneratePaytmChecksum> call = tnAPI.GeneratePaytmChecksum(request);
            call.enqueue(new Callback<CallbackGeneratePaytmChecksum>() {
                @SuppressWarnings("ConstantConditions")
                @Override
                public void onResponse(@NonNull Call<CallbackGeneratePaytmChecksum> call, @NonNull Response<CallbackGeneratePaytmChecksum> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (response.body().data().isSuccess()) {

                                String mid = response.body().data().merchantID();
                                String orderId = response.body().data().orderId();
                                String custId = response.body().data().custID();
                                String industryTypeId = response.body().data().industryTypeId();
                                String channelId = response.body().data().channelID();
                                String website = response.body().data().website();
                                String amount = response.body().data().txnAmount();
                                // String paytmChecksum = response.body().data().paytmChecksum();
                                String paytmChecksum = response.body().data().getCHECKSUMHASH();
                                String callbackUrl = response.body().data().callbackUrl();
                                String email = response.body().data().email();
                                String mobile = response.body().data().mobileNo();
                                boolean isPaytmLive = response.body().data().isPaytmLive();

                                startPaytm(mid, orderId, custId, industryTypeId, channelId, website, amount, paytmChecksum, callbackUrl, email, mobile, isPaytmLive);

                            } else if (Validate.isNotNull(response.body().data().message())) {
                                CC.showAlert(response.body().data().message());
                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        } else {
                            CC.showToast(R.string.msg_no_response);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CC.showToast(R.string.msg_something_wrong);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackGeneratePaytmChecksum> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }


}