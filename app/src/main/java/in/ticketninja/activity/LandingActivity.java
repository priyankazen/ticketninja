/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.util.Log;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;

import java.util.ArrayList;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.database.DatabaseHelper;
import in.ticketninja.fragments.CouponCodesFragment;
import in.ticketninja.fragments.HomeFragment;
import in.ticketninja.fragments.MyProfileFragment;
import in.ticketninja.fragments.MyTicketFragment;
import in.ticketninja.fragments.SearchFragment;
import in.ticketninja.fragments.TabFragment;
import in.ticketninja.objects.UserCouponCode;
import in.ticketninja.objects.User_Ticket_Event;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestUserTicket;
import in.ticketninja.ws.response.CallbackUserTicket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LandingActivity extends AppCompatActivity {

    private static final String TAG = LandingActivity.class.getSimpleName();
    private final int TAB_HOME = 0;
    private final int TAB_SEARCH = 1;
    private final int TAB_PROFILE = 3;
    private final int TAB_MYTICKET = 2;
    private CommonClass CC;
    private DatabaseHelper DB;
    private LoginUtils mLogin;
    private int currentTab = 0;
    private AHBottomNavigation mBottomNavigationView;
    private boolean isOpenMyTickets = false;
    //private boolean isDashboardDisplayFromCache = true;
    private boolean doubleBackToExitPressedOnce = false;
    private PreferencesUtils mPref;
    private boolean bBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        CC = new CommonClass(this);
        DB = new DatabaseHelper(this);
        //DB.deleteDatabase1(LandingActivity.this);
        mLogin = new LoginUtils(this);
        mPref = new PreferencesUtils(this);

        mBottomNavigationView = findViewById(R.id.bottom_navigation);

        if (getIntent() != null) {
            if (getIntent().hasExtra("deepLink")) {
                bBack = getIntent().getBooleanExtra("deepLink", false);
                Log.e(TAG, "bBack : " + bBack);
            }
        }

        setupBottomNavigation();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        }

        if (savedInstanceState == null) {
            loadHomeFragment();
        }

        syncBookingHistory();
    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboard(this);

        if (bBack) {
            super.onBackPressed();
            return;
        }

        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        CC.showToast(R.string.msg_double_time_back);

        new Handler().postDelayed(() -> doubleBackToExitPressedOnce = false, 2000);
    }

    //p
    /*private void checkApiType() {
        Log.e(TAG, "BuildConfig_isDashboardDisplayFromCache=" + BuildConfig.IS_UPDATE);
        Log.e(TAG, "getIsUpdate =" + mPref.getIsUpdate());

        if (BuildConfig.IS_UPDATE){
            if (mPref.getIsUpdate()) {
                mPref.setIsUpdate(false);
                isDashboardDisplayFromCache = false;
            } else {
                isDashboardDisplayFromCache = true;
            }
        }else {
            if (mPref.getIsUpdate()) {
                isDashboardDisplayFromCache = true;
            } else {
                mPref.setIsUpdate(true);
                isDashboardDisplayFromCache = false;

            }
        }
        Log.e(TAG, "isDashboardDisplayFromCache =" + isDashboardDisplayFromCache);
    }*/


    private void setupBottomNavigation() {
        mBottomNavigationView.setTranslucentNavigationEnabled(false);
        mBottomNavigationView.setBehaviorTranslationEnabled(false);

        AHBottomNavigationItem item1 = new AHBottomNavigationItem(R.string.str_home, R.drawable.selecter_home, R.color.colorPrimary);
        AHBottomNavigationItem item2 = new AHBottomNavigationItem(R.string.str_search, R.drawable.ic_search_selected, R.color.colorPrimary);
        AHBottomNavigationItem item3 = new AHBottomNavigationItem(R.string.str_profile, R.drawable.ic_profile_unselect, R.color.colorPrimary);
        AHBottomNavigationItem item4 = new AHBottomNavigationItem(R.string.str_myticket, R.drawable.ic_myticket_unselected, R.color.colorPrimary);
        //AHBottomNavigationItem item5 = new AHBottomNavigationItem(R.string.str_more, R.drawable.ic_more_selected, R.color.colorPrimary);

        mBottomNavigationView.addItem(item1);
        mBottomNavigationView.addItem(item2);
        mBottomNavigationView.addItem(item4);
        mBottomNavigationView.addItem(item3);
        // mBottomNavigationView.addItem(item5);

        currentTab = TAB_HOME;
        mBottomNavigationView.setAccentColor(getResources().getColor(R.color.colorPrimary));
        mBottomNavigationView.setTitleState(AHBottomNavigation.TitleState.ALWAYS_SHOW);
        mBottomNavigationView.setCurrentItem(TAB_HOME);
        mBottomNavigationView.getItem(TAB_HOME).setDrawable(R.drawable.ic_home_selected);
        mBottomNavigationView.getItem(TAB_HOME).setColor(getResources().getColor(R.color.colorPrimary));

        if (mLogin.getNotifications() > 0) {
            mBottomNavigationView.setNotificationBackgroundColor(Color.parseColor("#F53033"));
            mBottomNavigationView.setNotification("" + LoginUtils.getNotifications(LandingActivity.this), TAB_PROFILE);
        } else {
            mBottomNavigationView.setNotificationBackgroundColor(Color.parseColor("#ffffff"));
        }


        mBottomNavigationView.setOnTabSelectedListener((position, wasSelected) -> {
            // Do something cool here...
            isOpenMyTickets = false;
            //isDashboardDisplayFromCache = currentTab != TAB_HOME;
            currentTab = position;
            invalidateNotificationCount();
            switch (position) {
                case TAB_HOME: {
                    mBottomNavigationView.getItem(TAB_HOME).setDrawable(R.drawable.ic_home_selected);
                    mBottomNavigationView.getItem(TAB_PROFILE).setDrawable(R.drawable.ic_profile_unselect);
                    loadHomeFragment();
                    return true;
                }

                case TAB_SEARCH: {
                    mBottomNavigationView.getItem(TAB_HOME).setDrawable(R.drawable.ic_home_unselect);
                    mBottomNavigationView.getItem(TAB_PROFILE).setDrawable(R.drawable.ic_profile_unselect);
                    loadSearch();
                    return true;
                }

                case TAB_PROFILE: {
                    mBottomNavigationView.getItem(TAB_HOME).setDrawable(R.drawable.ic_home_unselect);
                    mBottomNavigationView.getItem(TAB_PROFILE).setDrawable(R.drawable.ic_profile_selected);
                    loadProfile();
                    return true;
                }

                case TAB_MYTICKET: {
                    mLogin = new LoginUtils(LandingActivity.this);
                    if (!mLogin.getUserId().equals("")) {
                        mBottomNavigationView.getItem(TAB_HOME).setDrawable(R.drawable.ic_home_unselect);
                        mBottomNavigationView.getItem(TAB_PROFILE).setDrawable(R.drawable.ic_profile_unselect);
                        myTicketFragment();
                        return true;

                    } else {
                        if (CC.isOnline()) {
                            isOpenMyTickets = true;
                            openLogin("");
                            return false;
                        } else {
                            CC.showToast(R.string.msg_no_internet);
                            return false;
                        }
                    }
                }
            }
            return false;
        });
    }

    private void invalidateNotificationCount() {
        if (currentTab == TAB_PROFILE) {
            mBottomNavigationView.setNotification("", TAB_PROFILE);
        } else {
            int count = LoginUtils.getNotifications(LandingActivity.this);
            mBottomNavigationView.setNotification(count <= 0 ? "" : "" + count, TAB_PROFILE);
        }
    }

    private void loadHomeFragment() {
        //HomeFragment fragment = HomeFragment.newInstance(isDashboardDisplayFromCache);
        HomeFragment fragment = HomeFragment.newInstance(false);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, fragment);
        ft.commit();
    }

    private void myTicketFragment() {
      /*  Intent i = new Intent(YouTubeActivity.this,MyTicketsActivity.class);
        startActivity(i);*/
        Fragment fragment;
        boolean isUserTicketTab = mPref.getUserTickets();
        boolean isUserCodeTab = mPref.getUserCode();

        if (isUserCodeTab && isUserTicketTab) {
            fragment = TabFragment.newInstance();
        }else if(isUserCodeTab){
            fragment = CouponCodesFragment.newInstance(true);
        }else {
            fragment = MyTicketFragment.newInstance(true);
        }
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, fragment);
        ft.commit();
    }

    private void loadProfile() {
        MyProfileFragment fragment = MyProfileFragment.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, fragment);
        ft.commit();
    }

    private void loadSearch() {
        SearchFragment fragment = SearchFragment.newInstance();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_frame, fragment);
        ft.commit();
    }

    private void syncBookingHistory() {
        if (CC.isOnline() && mLogin.isLoggedIn()) {
            if (ContextCompat.checkSelfPermission(LandingActivity.this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(LandingActivity.this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(LandingActivity.this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE);
            } else {
                wsGetBookingHistory(mLogin.getUserId());
            }
        }
    }

    //..............................WS CALL FOR GET BOOKING HISTORY..........//
    /*private void wsGetBookingHistory(long userId) {
        RequestBookingHistory request = new RequestBookingHistory();
        request.user_id = userId;
        request.h_type = Constant.HistoryType.FUTURE;

        TicketNinjaAPI tnAPI = RestApi.createAPI();
        Call<CallbackBookingHistory> call = tnAPI.GetBookingHistory(request);
        call.enqueue(new Callback<CallbackBookingHistory>() {
            @Override
            public void onResponse(Call<CallbackBookingHistory> call, @NonNull Response<CallbackBookingHistory> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().data().isSuccess()) {

                            DB.clearBookingHistoryTable(userId);
                            DB.clearBookingTicketListTable(userId);

                            ArrayList<BookingHistory> BookinHistoryList = response.body().data().booking_history;
                            if (BookinHistoryList != null && BookinHistoryList.size() > 0) {

                                for (int i = 0; i < BookinHistoryList.size(); i++) {
                                    DB.addBookingHistory(BookinHistoryList.get(i), userId);
                                }

                                DB.startDownloadingBookingHistoryInBackground();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CallbackBookingHistory> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }*/

    //..............................WS CALL FOR GET BOOKING HISTORY..........//
    private void wsGetBookingHistory(String userId) {

        RequestUserTicket request = new RequestUserTicket(mLogin.getUserId());

        TicketNinjaAPI tnAPI = RestApi.createAPI();
        Call<CallbackUserTicket> call = tnAPI.UserTicket(request);
        call.enqueue(new Callback<CallbackUserTicket>() {
            @Override
            public void onResponse(@NonNull Call<CallbackUserTicket> call, @NonNull Response<CallbackUserTicket> response) {
                try {
                    if (response.isSuccessful()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                            try {
                                if (DB.doesDatabaseExist(LandingActivity.this)) {
                                    DB.clearMyTicketTable(mLogin.getUserId());
                                    DB.clearMyTicketListTable(mLogin.getUserId());
                                } else {
                                    DB.reCreteDatabase(LandingActivity.this);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            ArrayList<User_Ticket_Event> ticketList = Objects.requireNonNull(response.body()).data().userTicketEvents();
                            if (ticketList != null && ticketList.size() > 0) {
                                for (int i = 0; i < ticketList.size(); i++) {
                                    DB.addMyTickets(ticketList.get(i), userId);
                                }
                                DB.startDownloadingMyTicketsInBackground();
                                mPref.setUserTickets(LandingActivity.this, true);
                            }else {
                                mPref.setUserTickets(LandingActivity.this, false);
                            }

                            ArrayList<UserCouponCode> list = response.body().data().getUser_coupon_code();
                            if (list != null && list.size() > 0) {
                                mPref.setUserCode(LandingActivity.this, true);
                            } else {
                                mPref.setUserCode(LandingActivity.this, false);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(@NonNull Call<CallbackUserTicket> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                syncBookingHistory();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e(TAG, "requestCode:" + requestCode + " ,  resultCode: " + resultCode);

        if (requestCode == Constant.ActivityForResult.NOTIFICATION) {
            invalidateNotificationCount();

        } else if (requestCode == Constant.ActivityForResult.LOGIN && resultCode == Activity.RESULT_OK) {
            int intExtra = data.getIntExtra(Constant.ScreenExtras.DATA, 0);
            if (intExtra == Constant.ScreenExtras.PRESS_SIGNUP) {
                openRegister();
            } else {
                mLogin = new LoginUtils(this);
                if (isOpenMyTickets) mBottomNavigationView.setCurrentItem(TAB_MYTICKET);
                syncBookingHistory();
            }

        } else if (requestCode == Constant.ActivityForResult.REGISTER && resultCode == Activity.RESULT_OK) {
            int intExtra = data.getIntExtra(Constant.ScreenExtras.DATA, 0);
            if (intExtra == Constant.ScreenExtras.PRESS_LOGIN) {
                String mailExtra = "";
                if (data.hasExtra(Constant.ScreenExtras.EMAIL))
                    mailExtra = data.getStringExtra(Constant.ScreenExtras.EMAIL);
                openLogin(mailExtra);
            } else {
                mLogin = new LoginUtils(this);
                if (isOpenMyTickets) mBottomNavigationView.setCurrentItem(TAB_MYTICKET);
                syncBookingHistory();
            }
        } else if (requestCode == 1 && resultCode == 4) {
            Log.e(TAG, "landing");
            bBack = true;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void openLogin(String email) {
        Intent intLogin = new Intent(LandingActivity.this, LoginActivity.class);
        intLogin.putExtra(Constant.ScreenExtras.EMAIL, email);
        //intLogin.putExtra(Constant.ScreenExtras.LOGIN_REQUIRED, mPref.getPrefLoginRequired());
        startActivityForResult(intLogin, Constant.ActivityForResult.LOGIN);
    }

    private void openRegister() {
        Intent intRegister = new Intent(getApplicationContext(), RegisterActivity.class);
        intRegister.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_MY_PROFILE);
        startActivityForResult(intRegister, Constant.ActivityForResult.REGISTER);
    }

}
