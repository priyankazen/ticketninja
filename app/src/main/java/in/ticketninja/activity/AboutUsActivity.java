/*
 * Copyright (c) 2017. suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.os.Build;
import android.os.Bundle;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import in.ticketninja.R;
import in.ticketninja.common.Constant;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;

public class AboutUsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_about_us);

            String from = getIntent().getStringExtra(Constant.ScreenExtras.PRIVACY);

            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                if (Validate.isNotNull(from)) {
                    tvTitle.setText(R.string.title_activity_privacy_policy);
                } else {
                    tvTitle.setText(R.string.title_activity_about_us);
                }
                Utility.convertToLowerCase(tvTitle);

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }

            TextView tv_about_us = findViewById(R.id.tv_about_us);

            if (Validate.isNotNull(from)) {
                tv_about_us.setText(getResources().getString(R.string.str_privacy_policy_new));
            } else {
                tv_about_us.setText(getResources().getString(R.string.str_about_us));
            }
        }catch (Exception e){
            e.printStackTrace();

        }
    }

}
