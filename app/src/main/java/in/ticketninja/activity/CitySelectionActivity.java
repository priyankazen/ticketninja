/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.ticketninja.R;
import in.ticketninja.adapters.CityAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.DividerItemDecoration;
import in.ticketninja.common.GPSTracker;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.CityList;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestCityList;
import in.ticketninja.ws.response.CallbackCityList;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CitySelectionActivity extends AppCompatActivity {

    private CommonClass CC;
    private GPSTracker gps;
    private PreferencesUtils pref;
    private SRKLoaderDialog mLoader;

    private RecyclerView recyclerView;
    private RecyclerView rvPopularCity;
    private RecyclerView rvOtherCity;

    private double lattitude, longitude;

    private EditText et_search;

    private static List<CityList> city_data = new ArrayList<>();
    private static List<CityList> dictionaryWords = new ArrayList<>();

    private CityAdapter mAdapter;
    public TextView tv_detect_location;

    public static int IsFromLocation = 0;
    private LinearLayout llRemoveLocation;
    private LinearLayout llOtherCity;
    private LinearLayout llPopularCity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city_selection);

        try {
            initializeData();

            setupActionbar();

            findViewById();

            TextView tvRemoveLocation = findViewById(R.id.tvRemoveLocation);
            tvRemoveLocation.setOnClickListener(view -> CC.showAlert(R.string.alert_remove_current_location, android.R.string.yes,
                    android.R.string.no, () -> {
                        removeCurrentLocation();
//                        invalidateCityList();
                        Intent iCart = new Intent(getBaseContext(), LandingActivity.class);
                        iCart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(iCart);
                    }));

            tv_detect_location.setOnClickListener(v -> {
                Utility.hideKeyboard(CitySelectionActivity.this);
                arePermissionGranted();
            });

            //arePermissionGranted();

            Log.e("latitude1==>", "" + lattitude);
            Log.e("longitude1==>", "" + longitude);

            //CurrentCity = getCountryName(CitySelectionActivity.this, lattitude, longitude);

            if (CC.isOnline()) {
                wsCityList();
            } else {
                CC.showAlert(R.string.msg_no_internet, this::onBackPressed);
            }
            Utility.setEditTextSingleLine(et_search);
            et_search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (!TextUtils.isEmpty(s.toString())) {
                        if (mAdapter != null) {
                            mAdapter.getFilter().filter(s.toString());
                        }
                        showLayout();
                    }else {
                        hideLayout();
                    }
                }
                @Override
                public void afterTextChanged(Editable s) {
                }
            });

            et_search.setOnEditorActionListener((v1, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    Utility.hideKeyboard(CitySelectionActivity.this);
                    if (!TextUtils.isEmpty(et_search.getText().toString().trim())) {
                        if (mAdapter != null && et_search != null) {
                            mAdapter.getFilter().filter(et_search.getText().toString().trim());
                        }
                        showLayout();
                    }else {
                        hideLayout();
                    }
                    return true;
                }
                return false;
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void showLayout(){
        recyclerView.setVisibility(View.VISIBLE);
        llPopularCity.setVisibility(View.GONE);
        llOtherCity.setVisibility(View.GONE);
    }

    private void hideLayout(){
        llPopularCity.setVisibility(View.VISIBLE);
        llOtherCity.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }
    private void findViewById() {
        try {

            recyclerView = findViewById(R.id.recyclerView);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.addItemDecoration(new DividerItemDecoration(this));
            et_search = findViewById(R.id.et_search);
            llRemoveLocation = findViewById(R.id.llRemoveLocation);
            if (Validate.isNotNull(pref.getCityName())) {
                llRemoveLocation.setVisibility(View.VISIBLE);
            } else {
                llRemoveLocation.setVisibility(View.GONE);
            }
            tv_detect_location = findViewById(R.id.tv_detect_location);
            //recyclerView.addItemDecoration(new DividerItemDecoration(this));

            rvPopularCity = findViewById(R.id.rvPopularCity);
            rvPopularCity.addItemDecoration(new DividerItemDecoration(this));
            rvOtherCity = findViewById(R.id.rvOtherCity);
            rvOtherCity.addItemDecoration(new DividerItemDecoration(this));

            llPopularCity = findViewById(R.id.llPopularCity);
            llOtherCity = findViewById(R.id.llOtherCity);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setupActionbar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                //getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);

                if (Validate.isNotNull(pref.getCityName())) {
                    tvTitle.setText(pref.getCityName());
                } else {
                    tvTitle.setText(R.string.title_activity_city_selection);
                }
                Utility.convertToLowerCase(tvTitle);
                //tvTitle.setTypeface(tfMedium);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {

            CC = new CommonClass(this);
            gps = new GPSTracker(this);
            pref = new PreferencesUtils(this);
            mLoader = new SRKLoaderDialog(this);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void removeCurrentLocation() {
        try {
            pref.setCityName("");
            pref.setCityId(0);
            llRemoveLocation.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void getCurrentCity() {
        try {
            Utility.hideKeyboard(this);
        /*lattitude = 23.022505;
        longitude = 72.5713621;*/
            String currentCity = getCountryname(CitySelectionActivity.this, lattitude, longitude);
            if (Validate.isNotNull(currentCity)) {
                for (int i = 0; i < city_data.size(); i++) {

                    if (city_data.get(i).name().equalsIgnoreCase(currentCity)) {
                        pref.setCityId(city_data.get(i).id());
                        pref.setCityName(city_data.get(i).name());
                    } else {
                        pref.setCityName(currentCity);
                    }
                }
                MessageUtils.showToast(CitySelectionActivity.this, "You are in " + currentCity);

                Intent iCart = new Intent(getBaseContext(), LandingActivity.class);
                iCart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(iCart);
                overridePendingTransition(R.anim.slide_in_up, R.anim.stay);

            } else {
                MessageUtils.showToast(CitySelectionActivity.this, R.string.msg_something_wrong);

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        try {
            Utility.hideKeyboard(this);
            super.onBackPressed();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // WS - city List
    private void wsCityList() {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestCityList request = new RequestCityList();
            request.keyword = "";

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackCityList> call = tnAPI.cityList(request);
            call.enqueue(new Callback<CallbackCityList>() {
                @SuppressWarnings("ConstantConditions")
                @Override
                public void onResponse(@NonNull Call<CallbackCityList> call, @NonNull Response<CallbackCityList> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        // if (response.body().data().isSuccess()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(response.body().getError_code())) {

                           /* dictionaryWords.clear();
                            city_data = response.body().data().city_list;
                            dictionaryWords.addAll(city_data);
                            invalidateCityList();*/

                            //new code 30/dec/2019
                            List<CityList> popularList = response.body().data().top_city_list;
                            invalidatePopularCityList(popularList);
                            List<CityList> otherList = response.body().data().other_city_list;
                            invalidateOtherCityList(otherList);

                            dictionaryWords.clear();
                            city_data.clear();
                            city_data.addAll(popularList);
                            city_data.addAll(otherList);
                            dictionaryWords.addAll(city_data);
                            invalidateCityList();

                        } else if (Validate.isNotNull(response.body().data().message())) {
                            CC.showAlert(response.body().data().message(), () -> onBackPressed());
                        } else {
                            CC.showAlert(R.string.msg_something_wrong, () -> onBackPressed());
                        }
                    } else {
                        CC.showAlert(R.string.msg_no_response, () -> onBackPressed());
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackCityList> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showAlert(R.string.msg_something_wrong, () -> onBackPressed());
                    t.printStackTrace();
                }
            });
        }catch (Exception e){
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }
    }

    private void invalidateCityList() {
        try {
            mAdapter = new CityAdapter(dictionaryWords, pref.getCityName(), CitySelectionActivity.this);
            mAdapter.setOnItemClickListener((position, city) -> {
                Utility.hideKeyboard(CitySelectionActivity.this);
                MessageUtils.showToast(CitySelectionActivity.this, "You are in " + city.name());
                pref.setCityName(city.name());
                pref.setCityId(city.id());

                Intent iCart = new Intent(getBaseContext(), LandingActivity.class);
                iCart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(iCart);
                // overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
            });
            recyclerView.setAdapter(mAdapter);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void invalidatePopularCityList(List<CityList> popularCityList) {
        try {
            if (popularCityList != null && popularCityList.size() > 0) {
                llPopularCity.setVisibility(View.VISIBLE);
                CityAdapter  adapter = new CityAdapter(popularCityList, pref.getCityName(), CitySelectionActivity.this);
                adapter.setOnItemClickListener((position, city) -> {
                    Utility.hideKeyboard(CitySelectionActivity.this);
                    MessageUtils.showToast(CitySelectionActivity.this, "You are in " + city.name());
                    pref.setCityName(city.name());
                    pref.setCityId(city.id());

                    Intent iCart = new Intent(getBaseContext(), LandingActivity.class);
                    iCart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(iCart);
                    // overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
                });
                rvPopularCity.setAdapter(adapter);
                recyclerView.setVisibility(View.GONE);
            } else {
                llPopularCity.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void invalidateOtherCityList(List<CityList> otherCityList) {
        try {
            if (otherCityList != null && otherCityList.size() > 0) {
                llOtherCity.setVisibility(View.VISIBLE);
                CityAdapter adapter = new CityAdapter(otherCityList, pref.getCityName(), CitySelectionActivity.this);
                adapter.setOnItemClickListener((position, city) -> {
                    Utility.hideKeyboard(CitySelectionActivity.this);
                    MessageUtils.showToast(CitySelectionActivity.this, "You are in " + city.name());
                    pref.setCityName(city.name());
                    pref.setCityId(city.id());

                    Intent iCart = new Intent(getBaseContext(), LandingActivity.class);
                    iCart.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(iCart);
                    // overridePendingTransition(R.anim.fadein, R.anim.fadeout);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.stay);
                });
                rvOtherCity.setAdapter(adapter);
                recyclerView.setVisibility(View.GONE);
            } else {
                llOtherCity.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getCountryname(Context context, double latitude, double longitude) {
        try {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses;
            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1);
                Log.e("addresses==>", "" + addresses);

                if (addresses != null && !addresses.isEmpty()) {
                    return addresses.get(0).getLocality();

                }
                return null;
            } catch (IOException ignored) {
                return null;
            }
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    private void arePermissionGranted() {
        try {
            if ((ContextCompat.checkSelfPermission(CitySelectionActivity.this,
                    android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) ||
                    ContextCompat.checkSelfPermission(CitySelectionActivity.this,
                            Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                //Permission not Granted...
                ActivityCompat.requestPermissions(CitySelectionActivity.this,
                        new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION,
                                android.Manifest.permission.ACCESS_COARSE_LOCATION},
                        Utility.REQUEST_PERMISSION_ACCESS_FINE_LOCATION);
            } else {
                gps = new GPSTracker(CitySelectionActivity.this);
                if (gps.canGetLocation()) {
                    lattitude = gps.getLatitude();
                    longitude = gps.getLongitude();
                    Log.e("latitude==>", "" + lattitude);
                    Log.e("longitude==>", "" + longitude);
                    getCurrentCity();
                } else {
                    gps.showSettingsAlert();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        try {
            if (requestCode == Utility.REQUEST_PERMISSION_ACCESS_FINE_LOCATION) {

                if (grantResults.length > 0) {
                    Log.e("grantResults size===>", "" + grantResults.length);
                    if (PackageManager.PERMISSION_GRANTED == grantResults[0] && PackageManager.PERMISSION_GRANTED == grantResults[1]) {

                        Log.e("Shabbir===>", "" + "Shabbir");

                        gps = new GPSTracker(CitySelectionActivity.this);
                        if (gps.canGetLocation()) {
                            lattitude = gps.getLatitude();
                            longitude = gps.getLongitude();
                            Log.e("latitude==>", "" + lattitude);
                            Log.e("longitude==>", "" + longitude);
                            getCurrentCity();

                        } else {

                            gps.showSettingsAlert();
                        }
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static class CustomFilter extends Filter {
        private CityAdapter mAdapter;

        public CustomFilter(CityAdapter mAdapter) {
            super();
            this.mAdapter = mAdapter;
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            dictionaryWords.clear();
            final FilterResults results = new FilterResults();
            try {
                if (constraint.length() == 0) {
                    dictionaryWords.addAll(city_data);
                } else {
                    final String filterPattern = constraint.toString().toLowerCase().trim();

                    for (int i = 0; i < city_data.size(); i++) {
                        if (city_data.get(i).name().toLowerCase().contains(filterPattern)) {
                            dictionaryWords.add(new CityList(city_data.get(i).id(), city_data.get(i).name(), city_data.get(i).isChecked()));
                        }
                    }

               /* for (final CityList mWords : city_list) {
                    if (mWords.categoryName().startsWith(filterPattern)) {
                        dictionaryWords.add(mWords);
                    }
                }*/
                }
                System.out.println("Count Number " + dictionaryWords.size());
                results.values = dictionaryWords;
                results.count = dictionaryWords.size();
            }catch (Exception e){
                e.printStackTrace();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            //System.out.println("Count Number 2 " + ((List<CityList>) results.values).size());
            this.mAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        try {
            if (IsFromLocation == 1) {
                if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

                Handler handler = new Handler();
                handler.postDelayed(() -> {
                    //Do something after 100ms
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();

                    gps = new GPSTracker(CitySelectionActivity.this);
                    if (gps.canGetLocation()) {
                        lattitude = gps.getLatitude();
                        longitude = gps.getLongitude();
                        Log.e("latitude==>", "" + lattitude);
                        Log.e("longitude==>", "" + longitude);
                        getCurrentCity();
                    } /*else {
                    gps.showSettingsAlert();
                }*/
                }, 2000);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
