package in.ticketninja.activity;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;

import com.google.android.material.appbar.AppBarLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import in.ticketninja.R;
import in.ticketninja.common.Constant;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.view.TouchImageView;
import in.ticketninja.widget.SRKLoaderDialog;

public class FullScreenImageActivity extends AppCompatActivity {

    private final String TAG = "FullScreenImage";
    private String imageUri;
    private TouchImageView ivFullImage;
    private String mTitle;
    private SRKLoaderDialog mLoader;
    private AppBarLayout appBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_full_screen_image);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                getWindow().setStatusBarColor(Color.BLACK);

            mLoader = new SRKLoaderDialog(this);

            getIntentData();

            setupActionbar();

            ivFullImage = findViewById(R.id.ivFullImage);
            ivFullImage.setOnClickListener(view -> {
                if (appBarLayout.getVisibility() == View.VISIBLE) {
                    appBarLayout.setVisibility(View.GONE);
                } else
                    appBarLayout.setVisibility(View.VISIBLE);
            });


        /*if (Validate.isNotNull(imageUri)) {
            Glide.with(this)
                    .load(imageUri)
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(ivFullImage);
        }*/
            if (Validate.isNotNull(imageUri)) {
                if (!mLoader.isShowing() && !isFinishing()) mLoader.show();
                // int widthDP = Utility.intToDP(getContext(), 250);
                Log.e(TAG, "imageUri=" + imageUri);
                Glide.with(this)
                        .load(imageUri)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                Log.e(TAG, "onException");
                                if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                                return true;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                Log.e(TAG, "onResourceReady");
                                if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                                ivFullImage.setImageDrawable(resource);
                                return true;
                            }
                        })
                        .placeholder(R.drawable.ic_placeholder)
                        .error(R.drawable.ic_placeholder)
                        .into(ivFullImage);
            } else {
                Glide.with(this)
                        .load(R.drawable.ic_placeholder)
                        .into(ivFullImage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getIntentData() {
        try {
            if (getIntent().hasExtra(Constant.ScreenExtras.IMAGE_URI)) {
                imageUri = getIntent().getStringExtra(Constant.ScreenExtras.IMAGE_URI);
            }
            if (getIntent().hasExtra(Constant.ScreenExtras.KEY_IMAGE_TITLE)) {
                mTitle = getIntent().getStringExtra(Constant.ScreenExtras.KEY_IMAGE_TITLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setupActionbar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            appBarLayout = findViewById(R.id.appBarLayout);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(mTitle);
                Utility.convertToLowerCase(tvTitle);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        try {
            // Respond to the action bar's Up/Home button
            if (item.getItemId() == android.R.id.home) {
                supportFinishAfterTransition();
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.onOptionsItemSelected(item);
    }
}
