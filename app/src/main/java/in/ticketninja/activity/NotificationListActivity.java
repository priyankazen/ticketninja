/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.ItemTouchHelper;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.NotificationListAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DividerItemDecoration;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.MyEvent;
import in.ticketninja.objects.Notifications;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestNotificationDelete;
import in.ticketninja.ws.request.RequestNotificationList;
import in.ticketninja.ws.request.RequestNotificationRead;
import in.ticketninja.ws.response.CallbackNotificationDelete;
import in.ticketninja.ws.response.CallbackNotificationList;
import in.ticketninja.ws.response.CallbackNotificationRead;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationListActivity extends AppCompatActivity {

    private CommonClass CC;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;
    private RecyclerView recyclerView;
    private LinearLayout llErrorLayout;
    private ImageView ivErrorImage;
    private TextView tvErrorMessage, tvErrorButton;
    private SwipeRefreshLayout swipeRefreshLayout;
    private NotificationListAdapter mAdapter;
    private Paint p = new Paint();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_list);

        try {
            initializeData();

            setupActionBar();

            findViewById();

            swipeRefreshLayout.setOnRefreshListener(() -> {
                if (CC.isOnline()) {
                    wsNotificationList(mLogin.getUserId());
                } else {
                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            CC = new CommonClass(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void findViewById() {
        try {
            recyclerView = findViewById(R.id.recyclerView);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);
            recyclerView.addItemDecoration(new DividerItemDecoration(this));

            swipeRefreshLayout = findViewById(R.id.swipe_refresh_layout);

            llErrorLayout = findViewById(R.id.llErrorLayout);
            ivErrorImage = findViewById(R.id.ivErrorImage);
            tvErrorMessage = findViewById(R.id.tvErrorMessage);
            tvErrorButton = findViewById(R.id.tvErrorButton);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showNoInternet() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_connection_lost)
                    .placeholder(R.drawable.ic_connection_lost_static)
                    .into(ivErrorImage);
            tvErrorMessage.setText(R.string.msg_no_internet);
            recyclerView.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.VISIBLE);
            tvErrorButton.setText(R.string.btn_try_again);
            tvErrorButton.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    wsNotificationList(mLogin.getUserId());
                } else {
                    showNoInternet();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showError() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_no_result_found)
                    .placeholder(R.drawable.ic_no_result_found)
                    .into(ivErrorImage);

            //swipeRefreshLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void initSwipe() {
        try {
            ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

                @Override
                public boolean onMove(@NonNull RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                    final int fromPosition = viewHolder.getAdapterPosition();
                    final int toPosition = target.getAdapterPosition();

                /*ItemObject prev = posts.remove(fromPosition);
                posts.add(toPosition > fromPosition ? toPosition - 1 : toPosition, prev);*/
                    mAdapter.notifyItemMoved(fromPosition, toPosition);

                    mAdapter.notifyItemMoved(fromPosition, toPosition);
                    return true;
                }

                @Override
                public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                    int position = viewHolder.getAdapterPosition();

                    if (mAdapter != null) {
                        if (direction == ItemTouchHelper.LEFT) {
                            mAdapter.removeItem(position);
                        } else {
                            MessageUtils.showToast(NotificationListActivity.this, "Marked as Read");
                            mAdapter.removeItem(position);
                        }
                    }
                }

                @Override
                public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

                    Bitmap icon;
                    if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {

                        View itemView = viewHolder.itemView;
                        float height = (float) itemView.getBottom() - (float) itemView.getTop();
                        float width = height / 3;

                        if (dX > 0) {
                            p.setColor(Color.parseColor("#388E3C"));
                            RectF background = new RectF((float) itemView.getLeft(), (float) itemView.getTop(), dX, (float) itemView.getBottom());
                            c.drawRect(background, p);
                            icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_logo);
                            RectF icon_dest = new RectF((float) itemView.getLeft() + width, (float) itemView.getTop() + width, (float) itemView.getLeft() + 2 * width, (float) itemView.getBottom() - width);
                            c.drawBitmap(icon, null, icon_dest, p);
                        } else {
                            p.setColor(Color.parseColor("#D32F2F"));
                            RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(), (float) itemView.getRight(), (float) itemView.getBottom());
                            c.drawRect(background, p);
                            icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_download);
                            RectF icon_dest = new RectF((float) itemView.getRight() - 2 * width, (float) itemView.getTop() + width, (float) itemView.getRight() - width, (float) itemView.getBottom() - width);
                            c.drawBitmap(icon, null, icon_dest, p);
                        }
                    }
                    super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
                }
            };
            ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
            itemTouchHelper.attachToRecyclerView(recyclerView);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        try {
            Utility.hideKeyboard(this);
            // super.onBackPressed();
            Intent returnIntent = new Intent();
            //returnIntent.putExtra("result", result);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // WS - NOTIFICATION LIST
    private void wsNotificationList(String userId) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestNotificationList request = new RequestNotificationList(String.valueOf(userId));

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackNotificationList> call = tnAPI.notificationList(request);
            call.enqueue(new Callback<CallbackNotificationList>() {
                @Override
                public void onResponse(@NonNull Call<CallbackNotificationList> call, @NonNull Response<CallbackNotificationList> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                                List<Notifications> myNotification = Objects.requireNonNull(response.body()).data().notification_list;

                            /*List<Notifications> myNotification =new ArrayList<>();
                            List<CallbackNotificationList.NotificationData> myNotification1 = Objects.requireNonNull(response.body()).data;
                            for (int i = 0; i <myNotification1.size() ; i++) {
                                CallbackNotificationList.NotificationData notificationData= myNotification1.get(i);
                                Notifications notifications =new Notifications();
                                notifications.setEvent_id(notificationData.event_id);
                                notifications.setIs_read(notificationData.is_read);
                                notifications.setMessage(notificationData.message);
                                notifications.setN_type(notificationData.n_type);
                                notifications.setNotification_date(notificationData.notification_date);
                                notifications.setNotification_id(notificationData.notification_id);
                                notifications.setPoster_image_id(notificationData.poster_image_id);
                                notifications.setPoster_image_url(notificationData.poster_image_id);
                                notifications.setReference_id(notificationData.reference_id);
                                notifications.setStatus(notificationData.status);
                              // notifications.setUser_id(notificationData.user_id);
                                notifications.setType(notificationData.type);
                                myNotification.add(notifications);
                            }*/
                                Log.e("myNotification", "" + myNotification);

                                if (myNotification != null && myNotification.size() > 0) {
                                    mAdapter = new NotificationListAdapter(NotificationListActivity.this, myNotification);
                                    mAdapter.setOnItemClickListener(new NotificationListAdapter.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(int position, Notifications notification) {

                                            wsNotificationRead(notification.getUserId(), notification.getNotificationId(),
                                                    position, false);

                                            if (notification.getType() == Constant.NotificationType.EVENT) {
                                                MyEvent myEvent = new MyEvent();
                                                myEvent.event_id = notification.getEventId();
                                                myEvent.Poster_Image_Id = notification.getPosterImageId();
                                                myEvent.poster_image_url = notification.getPosterImageURL();
                                                Intent i1 = new Intent(NotificationListActivity.this, EventDetailActivity.class);
                                                i1.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                                                startActivity(i1);

                                            } else if (notification.getType() == Constant.NotificationType.TICKET) {
                                                Intent i1 = new Intent(NotificationListActivity.this, MyTicketsActivity.class);
                                                startActivity(i1);

                                            } else {
                                                CC.showToast("Undefined Notification type.");
                                            }


                                      /*  MyEvent myEvent = new MyEvent();
                                        myEvent.event_id = notification.getEventId();
                                        myEvent.Poster_Image_Id = notification.getPosterImageId();
                                        myEvent.poster_image_url = notification.getPosterImageURL();
                                        Intent i1 = new Intent(NotificationListActivity.this, EventDetailActivity.class);
                                        i1.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                                       // i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(i1);*/
                                        }

                                        @Override
                                        public void onItemMarkAsRead(int position, Notifications notification) {
                                            if (CC.isOnline()) {
                                                wsNotificationRead(notification.getUserId(), notification.getNotificationId(),
                                                        position, true);
                                            } else {
                                                CC.showToast(R.string.msg_no_internet);
                                            }
                                        }

                                        @Override
                                        public void onItemDelete(int position, Notifications notification) {

                                            if (CC.isOnline()) {
                                                wsNotificationDelete(notification.getNotificationId(), position);
                                            } else {
                                                MessageUtils.showToast(NotificationListActivity.this, R.string.msg_no_internet);
                                            }


                                            MessageUtils.showToast(NotificationListActivity.this, "Delete = " + position);
                                        }
                                    });
                                    recyclerView.setAdapter(mAdapter);

                                    initSwipe();
                                    recyclerView.setVisibility(View.VISIBLE);
                                    llErrorLayout.setVisibility(View.GONE);

                                } else {
                                    tvErrorMessage.setText(R.string.msg_no_notification_found);
                                    showError();
                                }

                            } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                                tvErrorMessage.setText(Objects.requireNonNull(response.body()).getError_description());
                                showError();

                            } else {
                                tvErrorMessage.setText(R.string.msg_something_wrong);
                                showError();
                            }
                        } else {
                            tvErrorMessage.setText(R.string.msg_no_response);
                            showError();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        tvErrorMessage.setText(R.string.msg_something_wrong);
                        showError();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackNotificationList> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing()) {
                        swipeRefreshLayout.setRefreshing(false);
                    }
                    tvErrorMessage.setText(R.string.msg_something_wrong);
                    showError();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }
    }

    // WS - NOTIFICATION READ
    private void wsNotificationRead(long userId, long notificationId, int position, boolean isLoaderShow) {
        try {
            if (!mLoader.isShowing() && !isFinishing() && isLoaderShow) mLoader.show();

            RequestNotificationRead request = new RequestNotificationRead(String.valueOf(userId), notificationId);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackNotificationRead> call = tnAPI.notificationRead(request);
            call.enqueue(new Callback<CallbackNotificationRead>() {
                @Override
                public void onResponse(@NonNull Call<CallbackNotificationRead> call, @NonNull Response<CallbackNotificationRead> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                        swipeRefreshLayout.setRefreshing(false);
                    try {
                        if (response.isSuccessful() && response.body() != null) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                mLogin.setNotifications(Objects.requireNonNull(response.body()).data().getNotificationUnreadCount());
                                if (mAdapter != null)
                                    mAdapter.notifyItemChanged(position);

                            } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                                CC.showToast(Objects.requireNonNull(response.body()).getError_description());
                                showError();

                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                                showError();
                            }
                        } else {
                            CC.showToast(R.string.msg_no_response);
                            showError();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CC.showToast(R.string.msg_something_wrong);
                        showError();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackNotificationRead> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                        swipeRefreshLayout.setRefreshing(false);
                    CC.showToast(R.string.msg_something_wrong);
                    showError();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }


    }

    // WS - NOTIFICATION DELETE
    private void wsNotificationDelete(long notificationId, int position) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestNotificationDelete request = new RequestNotificationDelete(notificationId);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackNotificationDelete> call = tnAPI.notificationDelete(request);
            call.enqueue(new Callback<CallbackNotificationDelete>() {
                @Override
                public void onResponse(@NonNull Call<CallbackNotificationDelete> call, @NonNull Response<CallbackNotificationDelete> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                        swipeRefreshLayout.setRefreshing(false);
                    try {
                        if (response.isSuccessful() && response.body() != null) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                           /* mLogin.setNotifications(response.body().data().getNotificationUnreadCount());
                            if (mAdapter != null)
                                mAdapter.notifyItemChanged(position);*/

                                if (mAdapter != null) {
                                    mAdapter.notifyItemRemoved(position);
                                    mAdapter.notifyItemChanged(position);
                                }


                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                                showError();
                            }
                        } else {
                            CC.showToast(R.string.msg_no_response);
                            showError();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CC.showToast(R.string.msg_something_wrong);
                        showError();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackNotificationDelete> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (swipeRefreshLayout != null && swipeRefreshLayout.isRefreshing())
                        swipeRefreshLayout.setRefreshing(false);
                    CC.showToast(R.string.msg_something_wrong);
                    showError();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }


    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (CC.isOnline()) {
                wsNotificationList(mLogin.getUserId());
            } else {
                showNoInternet();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
