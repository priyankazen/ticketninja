/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.io.InputStream;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.CustomTypefaceSpan;
import in.ticketninja.common.FilesUtils;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.TextViewUtils;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestCustomerCare;
import in.ticketninja.ws.response.CallbackCustomerCare;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomerCareActivity extends AppCompatActivity {

    //private final String TAG = CustomerCareActivity.class.getSimpleName();
    private CommonClass CC;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;

    private EditText etEmail, etSubject, etDescription, et_mo_no;
    private Button btn_addfile, btn_submit;
    private String mSelectedFileName, mBytesData;
    private String type;
    private TextView tvSelectedFile;
    private TextView tv_time, tv_customer_care, tv_hotline, tv_email;

    private boolean mDoubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_care);

        initializedData();

        // TYPEFACE
        Typeface tfBold = ResourcesCompat.getFont(this, R.font.helvetica_neue_bold);

        // ACTIONBAR
        setupActionBar();

        findViewById();

        if (mLogin.isLoggedIn()) {
            etEmail.setText(Validate.isNotNull(mLogin.getEmail()) ? mLogin.getEmail() : "");
            et_mo_no.setText(Validate.isNotNull(mLogin.getMobile()) ? mLogin.getMobile() : "");
        }

        SpannableString styledString = new SpannableString(getString(R.string.str_opration_hour));
        styledString.setSpan(new CustomTypefaceSpan("", tfBold), 0, 16, 0);
        styledString.setSpan(new ForegroundColorSpan(Color.parseColor("#4A4A4A")), 0, 16, 0);
        styledString.setSpan(new TextViewUtils.MyClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Utility.hideKeyboard(CustomerCareActivity.this);


            }
        }, 0, 16, 0);
        tv_time.setMovementMethod(LinkMovementMethod.getInstance());
        tv_time.setText(styledString);


        styledString = new SpannableString(getString(R.string.str_customer_care_hotline));
        styledString.setSpan(new CustomTypefaceSpan("", tfBold), 16, 29, 0);
        styledString.setSpan(new ForegroundColorSpan(Color.parseColor("#4A4A4A")), 16, 29, 0);
        styledString.setSpan(new TextViewUtils.MyClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Utility.hideKeyboard(CustomerCareActivity.this);

                Utility.callPhone(CustomerCareActivity.this, "1800 241 7777");

            }
        }, 16, 29, 0);
        tv_customer_care.setMovementMethod(LinkMovementMethod.getInstance());
        tv_customer_care.setText(styledString);


        styledString = new SpannableString(getString(R.string.str_tr_hotline));
        styledString.setSpan(new CustomTypefaceSpan("", tfBold), 21, 34, 0);
        styledString.setSpan(new ForegroundColorSpan(Color.parseColor("#4A4A4A")), 21, 34, 0);
        styledString.setSpan(new TextViewUtils.MyClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Utility.hideKeyboard(CustomerCareActivity.this);

                Utility.callPhone(CustomerCareActivity.this, "1800 241 8888");
            }
        }, 21, 34, 0);
        tv_hotline.setMovementMethod(LinkMovementMethod.getInstance());
        tv_hotline.setText(styledString);


        styledString = new SpannableString(getString(R.string.str_cc_email_id));
        styledString.setSpan(new CustomTypefaceSpan("", tfBold), 0, 17, 0);
        styledString.setSpan(new ForegroundColorSpan(Color.parseColor("#F53033")), 0, 17, 0);
        styledString.setSpan(new TextViewUtils.MyClickableSpan() {
            @Override
            public void onClick(@NonNull View widget) {
                Utility.hideKeyboard(CustomerCareActivity.this);

                Intent i = new Intent(Intent.ACTION_SENDTO);
                //i.setType("message/rfc822");
                // i.putExtra(Intent.EXTRA_EMAIL, new String[]{"cs@ticketninja.in"});
                i.setData(Uri.parse("mailto:cs@ticketninja.in"));

                try {
                    startActivity(i);
                } catch (android.content.ActivityNotFoundException ex) {
                    MessageUtils.showToast(CustomerCareActivity.this, R.string.msg_no_email_client_found);
                }
            }
        }, 0, 17, 0);
        tv_email.setMovementMethod(LinkMovementMethod.getInstance());
        tv_email.setText(styledString);

        switchFileNameVisibility();

        setOnClickEvent();


    }

    private void setOnClickEvent() {
        try {
            btn_addfile.setOnClickListener(v -> {
                if (ContextCompat.checkSelfPermission(CustomerCareActivity.this,
                        android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(CustomerCareActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                            Constant.RequestPermissions.READ_EXTERNAL_STORAGE);
                } else {
                    if (CC.isOnline()) {
                        uploadFile();
                    } else {
                        MessageUtils.showToast(CustomerCareActivity.this, R.string.msg_no_internet);
                    }

                }
            });

            btn_submit.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    if (validate()) {
                        wsCustomerCare();
                    }
                } else {

                    MessageUtils.showToast(CustomerCareActivity.this, R.string.msg_no_internet);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        try {
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                //getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(R.string.title_activity_customer_care);
                tvTitle.setTypeface(tfMedium);
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findViewById() {
        try {
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);
            etEmail = findViewById(R.id.et_email);
            etSubject = findViewById(R.id.et_subject);
            etDescription = findViewById(R.id.et_discription);
            tvSelectedFile = findViewById(R.id.tv_selected_file);
            et_mo_no = findViewById(R.id.et_mo_no);
            btn_submit = findViewById(R.id.btn_submit);
            btn_addfile = findViewById(R.id.btn_addfile);

            tv_time = findViewById(R.id.tv_time);
            tv_customer_care = findViewById(R.id.tv_customer_care);
            tv_hotline = findViewById(R.id.tv_hotline);
            tv_email = findViewById(R.id.tv_email);
            TextView tv_address_title = findViewById(R.id.tv_address_title);
            TextView tv_address = findViewById(R.id.tv_address);
            TextView tvCIN = findViewById(R.id.tvCIN);

            etEmail.setTypeface(tfMedium);
            etSubject.setTypeface(tfMedium);
            etDescription.setTypeface(tfMedium);
            tvSelectedFile.setTypeface(tfMedium);
            btn_submit.setTypeface(tfMedium);
            btn_addfile.setTypeface(tfMedium);

            tv_address_title.setTypeface(tfMedium);
            tv_address.setTypeface(tfMedium);
            tvCIN.setTypeface(tfMedium);
            tv_time.setTypeface(tfMedium);
            tv_customer_care.setTypeface(tfMedium);
            tv_hotline.setTypeface(tfMedium);
            tv_email.setTypeface(tfMedium);

            Utility.setEditTextSingleLine(etEmail);
            Utility.setEditTextSingleLine(etSubject);
            Utility.setEditTextSingleLine(etDescription);
            Utility.setEditTextSingleLine(et_mo_no);
            etDescription.setMinLines(4);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializedData() {
        try {
            CC = new CommonClass(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
            if (mLogin.isLoggedIn()) {
                etEmail.setText(Validate.isNotNull(mLogin.getEmail()) ? mLogin.getEmail() : "");
                et_mo_no.setText(Validate.isNotNull(mLogin.getMobile()) ? mLogin.getMobile() : "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean validate() {
        if (Validate.isNull(etEmail.getText().toString().trim())) {
            CC.showToast(R.string.common_msg_enter_email);
            etEmail.setFocusable(true);
            etEmail.requestFocus();
            return false;

        } else if (!Validate.checkEmail(etEmail.getText().toString().trim())) {
            CC.showToast(R.string.common_msg_enter_email_valid);
            etEmail.setFocusable(true);
            etEmail.requestFocus();
            return false;

        } else if (Validate.isNull(etSubject.getText().toString().trim())) {
            CC.showToast(R.string.common_msg_enter_subject);
            etSubject.setFocusable(true);
            etSubject.requestFocus();
            return false;

        } else if (Validate.isNull(etDescription.getText().toString().trim())) {
            CC.showToast(R.string.common_msg_enter_description);
            etDescription.setFocusable(true);
            etDescription.requestFocus();
            return false;

        } else {
            return true;
        }


    }

    //..........................Customer Care WS .............................//

    @Override
    public void onBackPressed() {
        try {
            Utility.hideKeyboard(this);
            if (!mDoubleBackToExitPressedOnce && isDataChanged()) {
                AlertDialog.Builder alert = new AlertDialog.Builder(this)
                        .setMessage(R.string.msg_confirm_exit_without_submit)
                        .setPositiveButton(android.R.string.yes, (dialogInterface, i) -> {
                            dialogInterface.dismiss();
                            mDoubleBackToExitPressedOnce = true;
                            onBackPressed();
                        })
                        .setNegativeButton(android.R.string.cancel, (dialogInterface, i) -> dialogInterface.dismiss());
                AlertDialog dialog = alert.create();
                if (!isFinishing()) dialog.show();
            } else {
                super.onBackPressed();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private boolean isDataChanged() {
        String email = etEmail.getText().toString().trim();
        String subject = etSubject.getText().toString().trim();
        String description = etDescription.getText().toString().trim();

        return (!mLogin.isLoggedIn() && Validate.isNotNull(email))
                || Validate.isNotNull(subject)
                || Validate.isNotNull(description)
                || Validate.isNotNull(mSelectedFileName)
                || (mLogin.isLoggedIn() && !email.equals(mLogin.getEmail()));
    }

    private void wsCustomerCare() {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestCustomerCare request = new RequestCustomerCare();
            request.email = etEmail.getText().toString().trim();
            request.description = etDescription.getText().toString().trim();
            request.subject = etSubject.getText().toString().trim();
            request.mobileno = et_mo_no.getText().toString().trim();

            /*if (Validate.isNotNull(mBytesData)) {
                request.imgData = mBytesData;
                request.fileformat = type;
            } else {
                request.imgData = "";
                request.fileformat = "";
            }*/
            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackCustomerCare> call = tnAPI.Customercare(request);
            call.enqueue(new Callback<CallbackCustomerCare>() {
                @Override
                public void onResponse(@NonNull Call<CallbackCustomerCare> call, @NonNull Response<CallbackCustomerCare> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                mDoubleBackToExitPressedOnce = true;
                                CC.showAlert(Objects.requireNonNull(response.body()).getError_description(), () -> onBackPressed());

                            } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                                CC.showToast(Objects.requireNonNull(response.body()).getError_description());

                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        } else {
                            CC.showToast(R.string.msg_no_response);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CC.showToast(R.string.msg_something_wrong);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackCustomerCare> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    private void uploadFile() {
        try {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            String[] mimeTypes = {"image/*", "application/pdf", "application/msword", "text/*",
                    "application/vnd.ms-excel",
                    "application/vnd.openxmlformats-officedocument.wordprocessingml.document"};
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
            // intent.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
            // intent.setType("image/*|application/pdf|application/msword/*|text/*|application/vnd.ms-excel/*");
            intent.addCategory(Intent.CATEGORY_OPENABLE);

            try {
                startActivityForResult(Intent.createChooser(intent, "Select a File to Upload"),
                        Constant.ActivityForResult.FILE_SELECT);
            } catch (ActivityNotFoundException ex) {
                CC.showToast(R.string.common_file_upload_file_manager_not_found);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == Constant.ActivityForResult.FILE_SELECT) {
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = data.getData();
                    //Log.d(TAG, "File Uri: " + uri.toString());
                    // Get the path
                    String path = null;
                    try {
                        ContentResolver cR = getContentResolver();
                        MimeTypeMap mime = MimeTypeMap.getSingleton();
                        type = mime.getExtensionFromMimeType(cR.getType(Objects.requireNonNull(uri)));
                        if (!Validate.isNotNull(type)) {
                            type = MimeTypeMap.getFileExtensionFromUrl(uri.toString());
                        }

                        int isMatch = -1;
                        String[] extension = getResources().getStringArray(R.array.resume_attach_extensions);
                        for (int i = 0; i < extension.length; i++) {
                            if (type.equals(extension[i])) {
                                isMatch = i;
                            }
                        }
                        if (isMatch > -1) {
                            String fileName = FilesUtils.getContentResolverFileName(this, uri);
                            if (!Validate.isNotNull(fileName)) {
                                File file = new File(Objects.requireNonNull(uri.getPath()));
                                fileName = file.getName();
                            }
                            InputStream inputStream = getContentResolver().openInputStream(uri);
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                                path = FilesUtils.createTemporaryFileRefer(this, Objects.requireNonNull(inputStream), fileName, type).getPath();
                            }
                            if (Validate.isNotNull(path)) {
                                File file = new File(path);

                                if (file.exists() && file.isFile()) {
                                    String file_name = file.getName();

                                    mBytesData = FilesUtils.fileToBase64(file);
                                    if ((file.length() / 1024) < Constant.MAXIMUM_UPLOAD_FILE_SIZE) {
                                        if (Validate.isNotNull(mBytesData)) {
                                            mSelectedFileName = file_name;
                                            switchFileNameVisibility();
                                        }
                                    } else {
                                        CC.showAlert(R.string.upload_file_size_large_msg);
                                    }
                                } else {
                                    CC.showAlert(R.string.msg_invalid_file);
                                }
                            } else {
                                CC.showToast(R.string.missing_file_from_device);
                            }
                        } else {
                            CC.showAlert(R.string.msg_invalid_file);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CC.showToast(R.string.msg_something_wrong);
                    }
                    //Log.d(TAG, "File Path: " + path);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void switchFileNameVisibility() {
        try {
            if (Validate.isNotNull(mBytesData)) {
                tvSelectedFile.setText(String.format("%s", mSelectedFileName));
                tvSelectedFile.setVisibility(Validate.isNotNull(mSelectedFileName) ? View.VISIBLE : View.GONE);
                btn_addfile.setText(R.string.btn_change_file);
            } else {
                tvSelectedFile.setVisibility(View.GONE);
                btn_addfile.setText(R.string.btn_add_file);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /*public void cleanView() {
        etDescription.setText("");
        etSubject.setText("");
        etEmail.setText("");
        mBytesData = "";
        type = "";

    }*/

}



