/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestChangePassword;
import in.ticketninja.ws.response.CallbackChangePassword;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {

    private EditText et_currPassword, et_newPassword, et_confirmPassword;
    private CommonClass CC;
    private SRKLoaderDialog mLoader;
    private LoginUtils mLogin;
    private  Button btn_change_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_change_password);

            CC = new CommonClass(this);
            mLoader = new SRKLoaderDialog(this);
            mLogin = new LoginUtils(this);

            setupActionbar();

            findViewById();

            btn_change_password.setOnClickListener(v -> {
                Utility.hideKeyboard(ChangePasswordActivity.this);
                if (CC.isOnline()) {
                    if (checkValidation()) {
                        wsUpdatePassword();
                    }
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void findViewById() {
        try {
            // TYPEFACE
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);
            et_confirmPassword = findViewById(R.id.et_confirmpassword);
            et_newPassword = findViewById(R.id.et_newpassword);
            et_currPassword = findViewById(R.id.et_currpassword);
            et_confirmPassword.setTypeface(tfMedium);
            et_newPassword.setTypeface(tfMedium);
            et_currPassword.setTypeface(tfMedium);
            btn_change_password = findViewById(R.id.btn_change_password);
            btn_change_password.setTypeface(tfMedium);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionbar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkValidation() {
        try {
            et_currPassword.setError(null);
            et_newPassword.setError(null);
            et_confirmPassword.setError(null);

            if (et_currPassword.getText().toString().trim().length() <= 0) {
                MessageUtils.showToast(ChangePasswordActivity.this, R.string.msg_enter_current_password);
                et_currPassword.requestFocus();
                return false;

            } else if (et_currPassword.getText().toString().trim().length() < 6
                    || et_currPassword.getText().toString().trim().length() > 16) {
                MessageUtils.showToast(ChangePasswordActivity.this, R.string.msg_enter_valid_current_password);
                et_currPassword.requestFocus();
                return false;

            } else if (et_newPassword.getText().toString().trim().length() <= 0) {
                MessageUtils.showToast(ChangePasswordActivity.this, R.string.msg_enter_new_password);
                et_newPassword.requestFocus();
                return false;

            } else if (et_newPassword.getText().toString().trim().length() < 6
                    || et_newPassword.getText().toString().trim().length() > 16) {
                MessageUtils.showToast(ChangePasswordActivity.this, R.string.msg_enter_valid_new_password);
                et_newPassword.requestFocus();
                return false;

            } else if (et_currPassword.getText().toString().trim()
                    .equals(et_newPassword.getText().toString().trim())) {
                MessageUtils.showToast(ChangePasswordActivity.this, R.string.msg_enter_current_and_new_password_same_not_allow);
                et_newPassword.requestFocus();
                return false;

            } else if (et_confirmPassword.getText().toString().trim().length() <= 0) {
                MessageUtils.showToast(ChangePasswordActivity.this, R.string.msg_confirm_password);
                et_confirmPassword.requestFocus();
                return false;

            } else if (et_confirmPassword.getText().toString().trim().length() < 6
                    || et_confirmPassword.getText().toString().trim().length() > 16) {
                MessageUtils.showToast(ChangePasswordActivity.this, R.string.msg_enter_valid_confirm_password);
                et_confirmPassword.requestFocus();
                return false;

            } else if (!et_confirmPassword.getText().toString().trim()
                    .equals(et_newPassword.getText().toString().trim())) {
                MessageUtils.showToast(ChangePasswordActivity.this, R.string.msg_enter_new_and_confirm_password_not_match);
                et_confirmPassword.requestFocus();
                return false;

            } else {
                return true;

            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    // WS - Forgot password
    private void wsUpdatePassword() {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestChangePassword request = new RequestChangePassword();
            request.new_password = et_newPassword.getText().toString().trim();
            request.old_password = et_currPassword.getText().toString().trim();
            request.user_id = String.valueOf(mLogin.getUserId());
            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackChangePassword> call = tnAPI.changePassword(request);
            call.enqueue(new Callback<CallbackChangePassword>() {
                @Override
                public void onResponse(@NonNull Call<CallbackChangePassword> call, @NonNull Response<CallbackChangePassword> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {

                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            CC.showAlert(response.body().getError_description(), () -> onBackPressed());

                        } else if (Validate.isNotNull(response.body().getError_description())) {
                            CC.showAlert(response.body().getError_description());

                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }

                    /*if (response.body().data().isSuccess()) {
                        CC.showAlert(response.body().data().message(), () -> onBackPressed());

                    } else if (Validate.isNotNull(response.body().data().message())) {
                        CC.showAlert(response.body().data().message());

                    } else {
                        CC.showToast(R.string.msg_something_wrong);
                    }*/
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackChangePassword> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {
        try {
            Utility.hideKeyboard(this);
            super.onBackPressed();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
