/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.PastTicketAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.BookingHistory;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestBookingHistory;
import in.ticketninja.ws.response.CallbackBookingHistory;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PastTicketActivity extends AppCompatActivity {

    private CommonClass CC;
    private SRKLoaderDialog mLoader;
    private LoginUtils mLogin;

    private List<BookingHistory> historyList = new ArrayList<>();

    private RecyclerView recyclerView;
    private LinearLayout llErrorLayout;
    private ImageView ivErrorImage;
    private TextView tvErrorMessage, tvErrorButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_booking_history);

            initializeData();

            setupActionBar();

            findViewById();

            if (CC.isOnline()) {
                wsGetBookingHistory();
            } else {
                showNoInternet();
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            CC = new CommonClass(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findViewById() {
        try {
            recyclerView = findViewById(R.id.recyclerView);
            recyclerView.setHasFixedSize(true);
            recyclerView.setNestedScrollingEnabled(false);

            llErrorLayout = findViewById(R.id.llErrorLayout);
            ivErrorImage = findViewById(R.id.ivErrorImage);
            tvErrorMessage = findViewById(R.id.tvErrorMessage);
            tvErrorButton = findViewById(R.id.tvErrorButton);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void showNoInternet() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_connection_lost)
                    .placeholder(R.drawable.ic_connection_lost_static)
                    .into(ivErrorImage);
            tvErrorMessage.setText(R.string.msg_no_internet);
            recyclerView.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.VISIBLE);
            tvErrorButton.setText(R.string.btn_try_again);
            tvErrorButton.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    recyclerView.setVisibility(View.VISIBLE);
                    llErrorLayout.setVisibility(View.GONE);
                    wsGetBookingHistory();
                } else {
                    showNoInternet();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void showError() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_no_result_found)
                    .placeholder(R.drawable.ic_no_result_found)
                    .into(ivErrorImage);
            recyclerView.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        Utility.hideKeyboard(this);
        super.onBackPressed();
    }

    //..............................WS CALL FOR GET BOOKING HISTORY..........//

    private void wsGetBookingHistory() {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestBookingHistory request = new RequestBookingHistory();

            request.user_id = String.valueOf(mLogin.getUserId());
            request.h_type = Constant.HistoryType.PAST;

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackBookingHistory> call = tnAPI.GetBookingHistory(request);
            call.enqueue(new Callback<CallbackBookingHistory>() {
                @Override
                public void onResponse(@NonNull Call<CallbackBookingHistory> call, @NonNull Response<CallbackBookingHistory> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                historyList = response.body().data().booking_history;
                                if (historyList != null && historyList.size() > 0) {
                                    invalidateData();
                                } else {
                                    tvErrorMessage.setText(R.string.msg_no_past_records_found);
                                    showError();
                                }

                            } else if (Validate.isNotNull(response.body().data().message())) {
                                tvErrorMessage.setText(response.body().data().message());
                                showError();

                            } else {
                                tvErrorMessage.setText(R.string.msg_something_wrong);
                                showError();
                            }
                        } else {
                            tvErrorMessage.setText(R.string.msg_no_response);
                            showError();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        tvErrorMessage.setText(R.string.msg_something_wrong);
                        showError();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackBookingHistory> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    tvErrorMessage.setText(R.string.msg_something_wrong);
                    showError();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }
    }

    private void invalidateData() {
        try {
            PastTicketAdapter adapter = new PastTicketAdapter(PastTicketActivity.this, historyList, 2);
            adapter.setOnItemClickListener((position, history) -> {
                if (CC.isOnline()) {
                    Intent intConfirm = new Intent(getApplicationContext(), BookingHistoryDetailActivity.class);
                    //intConfirm.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    intConfirm.putExtra(Constant.ScreenExtras.EVENT_HISTORY_TYPE, Constant.HistoryType.PAST);
                    intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, history.trans_id);
                    intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE, history.payment_type);
                    intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TICKET_LIVE, history.isIs_live_ticket());
                    intConfirm.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_APP_TICKET, history.isIs_app_ticket());
                    startActivity(intConfirm);
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });
            recyclerView.setAdapter(adapter);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
