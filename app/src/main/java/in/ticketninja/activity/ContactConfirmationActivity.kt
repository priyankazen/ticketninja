/*
 * Copyright (c) 2018. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package `in`.ticketninja.activity

import `in`.ticketninja.R
import `in`.ticketninja.common.*
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.core.view.ViewCompat
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.mukesh.countrypicker.Country
import com.mukesh.countrypicker.CountryPicker

class ContactConfirmationActivity : AppCompatActivity() {

    private var cc: CommonClass? = null

    private var etEmail: EditText? = null
    private var etMobileNo: EditText? = null
    private var etCountryCode: EditText? = null
    private var btnDone: Button? = null

    private var mPref: PreferencesUtils? = null

    private var isForUpdate: Boolean = false
    private var eventSessionId: String? = null
    private var usetInfoData: String? = null
    private var seatType: String? = null

    companion object {
        private val TAG = ContactConfirmationActivity::class.java.simpleName
        fun start(context: Context, sessionID: String?, flagAction: String, seatType: String?, user_identification_list: String?) {
            Log.e(TAG, "flagAction : $flagAction")
            val starter = Intent(context, ContactConfirmationActivity::class.java)
            starter.putExtra(Constant.ScreenExtras.EVENT_SESSION_ID, sessionID)
            starter.putExtra(Constant.ScreenExtras.TYPE, seatType)
            starter.putExtra(Constant.ScreenExtras.USER_INFO_DATA, user_identification_list)
            context.startActivity(starter)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        try {

            setContentView(R.layout.activity_contact_confirmation)

            getIntentData()

            cc = CommonClass(this)
            mPref = PreferencesUtils(this)

            setToolBarLayout()
            bindViews()
            showContactDetail()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun getIntentData() {
        try {
            isForUpdate = false

            if (intent.hasExtra(Constant.ScreenExtras.CONTACT_ACTION) &&
                    intent.getStringExtra(Constant.ScreenExtras.CONTACT_ACTION) == Constant.ContactActions.EDIT) {
                isForUpdate = true
            }

            if (intent.hasExtra(Constant.ScreenExtras.EVENT_SESSION_ID)) {
                eventSessionId = intent.getStringExtra(Constant.ScreenExtras.EVENT_SESSION_ID)
            }

            if (intent.hasExtra(Constant.ScreenExtras.TYPE)) {
                seatType = intent.getStringExtra(Constant.ScreenExtras.TYPE)
            }

            if (intent.hasExtra(Constant.ScreenExtras.USER_INFO_DATA)) {
                usetInfoData = intent.getStringExtra(Constant.ScreenExtras.USER_INFO_DATA)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun setToolBarLayout() {
        try {
            // ACTIONBAR
            val toolbar = findViewById<Toolbar>(R.id.toolbar)
            setSupportActionBar(toolbar)
            if (supportActionBar != null) {
                supportActionBar!!.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
                supportActionBar!!.setDisplayHomeAsUpEnabled(false)
                supportActionBar!!.setDisplayShowHomeEnabled(false)
                ViewCompat.setElevation(toolbar, 10f)

                val ivActionBack = toolbar.findViewById<ImageView>(R.id.ivActionBack)
                ivActionBack.setOnClickListener { onBackPressed() }
                val tvTitle = toolbar.findViewById<TextView>(R.id.tvTitle)
                tvTitle.text = title
                Utility.convertToLowerCase(tvTitle)
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById<View>(R.id.toolbarBottomDivider).visibility = View.VISIBLE
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun bindViews() {
        try {
            etEmail = findViewById(R.id.etEmail)
            etMobileNo = findViewById(R.id.etMobileNo)
            etCountryCode = findViewById(R.id.etCountryCode)
            btnDone = findViewById(R.id.btnDone)

            btnDone?.setOnClickListener { p0 ->
                if (isValidEmail() && isValidMobileNo()) {
                    mPref!!.userEmailId = etEmail?.text.toString().trim()
                    mPref!!.userPhoneNumber = etMobileNo?.text.toString().trim()
                    mPref!!.countryCode = etCountryCode?.text.toString().trim()

                    val mLogin = LoginUtils(this@ContactConfirmationActivity)
                    mLogin.email = etEmail?.text.toString().trim()
                    mLogin.mobile = etMobileNo?.text.toString().trim()
                    Log.e(TAG, "isForUpdate : $isForUpdate")
                    Utility.hideKeyboard(applicationContext, p0)
                    if (isForUpdate) {
                        setResult(Activity.RESULT_OK)
                        finish()
                    } else {
//                        val starter = Intent(this@ContactConfirmationActivity, OrderSummaryActivity::class.java)
//                        starter.putExtra(Constant.ScreenExtras.EVENT_SESSION_ID, sessionID)
//                        startActivity(starter)
                        // OrderSummaryActivity.start(this@ContactConfirmationActivity, eventSessionId, "", "")
                        OrderSummaryActivity.start(this@ContactConfirmationActivity, eventSessionId, seatType, usetInfoData)
                        finish()
                    }
                }
            }

            etCountryCode!!.setOnClickListener { pickCountry() }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun showContactDetail() {
        try {
            Log.e(TAG, "userEmailId : " + mPref?.userEmailId)
            Log.e(TAG, "userPhoneNumber: " + mPref?.userPhoneNumber)
            etEmail?.setText(mPref?.userEmailId)
            etMobileNo?.setText(mPref?.userPhoneNumber)
            if (Validate.isNotNull(mPref!!.countryCode)) {
                etCountryCode?.setText(mPref!!.countryCode)
            } else {
                val country = Country.getCountryByISO(Constant.DEFAULT_COUNTRY_REGION)
                etCountryCode?.setText(country.dialCode)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun pickCountry() {
        try {
            val picker = CountryPicker.newInstance("Select country")
            picker.setListener { name, code, dialCode, flagDrawableResID ->
                Log.e(TAG, "name : $name")
                Log.e(TAG, "code : $code")
                Log.e(TAG, "flagDrawableResID : $flagDrawableResID")
                etCountryCode!!.setText(dialCode)
                Utility.hideKeyboard(this@ContactConfirmationActivity, picker.view)
                picker.dismiss()
            }
            picker.show(supportFragmentManager, "COUNTRY_PICKER")
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    private fun isValidMobileNo(): Boolean {
        try {
            val countryCode = etCountryCode!!.text.toString().trim()
            val mobile = etMobileNo!!.text.toString().trim()

            return if (Validate.isNull(mobile)) {
                cc!!.showToast(R.string.login_msg_enter_mobile)
                etMobileNo!!.requestFocus()
                false

            } else if (Validate.isNotNull(countryCode) && !Validate.checkPhone_libPhoneNumber(countryCode, mobile)) {
                cc!!.showToast(R.string.common_msg_enter_mobile_valid)
                etMobileNo!!.requestFocus()
                false
            } else {
                true
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return false
        }

    }

    private fun isValidEmail(): Boolean {
        try {
            val email = etEmail?.text.toString().trim()

            return if (Validate.isNull(email)) {
                cc?.showToast(R.string.login_msg_enter_email)
                etEmail?.requestFocus()
                false

            } else if (!Validate.checkEmail(email)) {
                cc?.showToast(R.string.login_msg_enter_email_valid)
                etEmail?.requestFocus()
                false

            } else {
                true
            }
        } catch (e: Exception) {
            e.printStackTrace()
            return  false
        }
    }

    override fun onBackPressed() {
        try {
            Utility.hideKeyboard(this)
            super.onBackPressed()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }
}
