
package in.ticketninja.activity;

import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.uncopt.android.widget.text.justify.JustifiedTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.Validate;
import in.ticketninja.widget.MyDatePickerDialog;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestEventRegister;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EventRegisterActivity extends AppCompatActivity {
    private final String TAG = EventRegisterActivity.class.getSimpleName();
    private SRKLoaderDialog mLoader;
    private TextInputEditText edtName, edtMobileNo, edtEmail, edtTicket, edtDate;
    private CheckBox cbCBooking;
    private long eventId;
    private CommonClass commonClass;
    private MyDatePickerDialog datePickerDialog;
    private   Button btnReggi;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_corporate_booking);

        initializeData();

        setUpDatePickerDialog();

        findViewById();

        getIntentData();

        // ACTIONBAR
        setupActionBar();

        setOnClickEvent();

    }

    private void getIntentData() {
        try {
            eventId = getIntent().getLongExtra(Constant.ScreenExtras.EVENT_ID, 0);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            mLoader = new SRKLoaderDialog(this);
            commonClass = new CommonClass(this);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setOnClickEvent() {
        try {
            edtDate.setOnClickListener(v -> {
                Log.e(TAG, "click");
                datePickerDialog.show();
            });

            btnReggi.setOnClickListener(view -> wsEventReg());
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void findViewById() {
        try {
            edtTicket = findViewById(R.id.edtTicket);
            edtTicket.setInputType(InputType.TYPE_CLASS_TEXT);
            TextInputLayout textTicketInputLayout = findViewById(R.id.txtInputTicket);
            textTicketInputLayout.setHint(getResources().getString(R.string.city));
            cbCBooking = findViewById(R.id.cbCBooking);
            JustifiedTextView txtTerm = findViewById(R.id.txtTermC);
            txtTerm.setText(getResources().getString(R.string.terms_condition1));
            btnReggi = findViewById(R.id.btnRegi);
            LinearLayout llDate = findViewById(R.id.llDate);
            llDate.setVisibility(View.VISIBLE);
            edtDate = findViewById(R.id.edtDate);
            edtName = findViewById(R.id.edtName);
            TextInputLayout textNameInputLayout = findViewById(R.id.txtInputName);
            textNameInputLayout.setHint(getResources().getString(R.string.full_name));
            edtMobileNo = findViewById(R.id.edtMobileNo);
            edtEmail = findViewById(R.id.edtEmail);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        try {
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(R.string.register);

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpDatePickerDialog() {

        try {
            final Calendar newCalendar = Calendar.getInstance();
            newCalendar.set(Calendar.HOUR_OF_DAY, 0);
            newCalendar.set(Calendar.MINUTE, 0);
            newCalendar.set(Calendar.SECOND, 5);

            datePickerDialog = new MyDatePickerDialog(EventRegisterActivity.this, R.style.datepicker,
                    (view, year, monthOfYear, dayOfMonth) -> {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        String selectedDate = String.format(Locale.getDefault(), "%d-%02d-%02d", dayOfMonth, monthOfYear + 1, year);
                        Log.e(TAG, "selectedDate :" + selectedDate);
                        edtDate.setText(selectedDate);
                    }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

            datePickerDialog.getDatePicker().setLayoutMode(1);
            Log.e(TAG, "getMinDate :" + DateTimeUtils.getMinDate(0));
            datePickerDialog.getDatePicker().setCalendarViewShown(false);
            datePickerDialog.getDatePicker().setMinDate(DateTimeUtils.getMinDate(0));
            //DatePickerDialog.setTitle("Select your event_date");
            datePickerDialog.setTitle(getResources().getString(R.string.select_date));
            datePickerDialog.setDisableDate(null);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        finish();
        return super.onTouchEvent(event);
    }

    // WS - Get Brand Association
    //Insert_Corporate_Booking_Inquiry(string event_id, string event_name, string mobileno, string email, string category_name, string Tickets_Looking)
    private void wsEventReg() {
        try {
            String name = Objects.requireNonNull(edtName.getText()).toString().trim();
            String mobile_no = Objects.requireNonNull(edtMobileNo.getText()).toString().trim();
            String email = Objects.requireNonNull(edtEmail.getText()).toString().trim();
            String date = Objects.requireNonNull(edtDate.getText()).toString().trim();
            String city = Objects.requireNonNull(edtTicket.getText()).toString().trim();

            Log.e(TAG, "name=" + name);
            Log.e(TAG, "mobile_no=" + mobile_no);
            Log.e(TAG, "email=" + email);
            Log.e(TAG, "city=" + city);
            Log.e(TAG, "event_date=" + date);
            Log.e(TAG, "eventId=" + eventId);
            Log.e(TAG, "cb=" + cbCBooking.isChecked());


            if (TextUtils.isEmpty(name)) {
                showError(getString(R.string.please_enter_name));
                return;
            } else if (TextUtils.isEmpty(mobile_no)) {
                showError(getString(R.string.please_enter_mobile_no));
                return;
            } else if (!TextUtils.isEmpty(mobile_no) && mobile_no.length() != 10) {
                showError(getString(R.string.common_msg_enter_mobile_valid));
                return;
            } else if (TextUtils.isEmpty(email)) {
                showError(getString(R.string.please_enter_email));
                return;
            } else if (!TextUtils.isEmpty(email) && !Validate.checkEmail(email)) {
                showError(getString(R.string.common_msg_enter_email_valid));
                return;
            } else if (TextUtils.isEmpty(date)) {
                showError(getString(R.string.please_enter_date));
                return;
            } else if (TextUtils.isEmpty(city)) {
                showError(getString(R.string.please_enter_city));
                return;
            } else if ((!cbCBooking.isChecked())) {
                showError(getString(R.string.please_select_terms_condition));
                return;
            }

            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestEventRegister request = new RequestEventRegister();
            request.event_id = String.valueOf(eventId);
            request.email = email;
            request.mobileno = mobile_no;
            request.fullname = name;
            request.date = date;
            request.city = city;

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<ResponseBody> call = tnAPI.getEventReg(request);
            call.enqueue(new Callback<ResponseBody>() {
                @SuppressWarnings("ConstantConditions")
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        String myData = response.body().string();
                        Log.e(TAG, "data =" + myData);
                        if (response.isSuccessful()) {
                            Log.e(TAG, "if");
                            try {
                                JSONObject jo = new JSONObject(myData);
                                //JSONObject jo1 = jo.getJSONObject("Insert_Register");
                                String errorCode = jo.getString("error_code");
                                String errorMessage = jo.getString("error_description");
                                if (errorCode.equals("1")) {
                                    commonClass.showAlert(errorMessage, () -> onBackPressed());
                                } else {
                                    commonClass.showAlert(errorMessage, (MessageUtils.OnOkClickListener) null);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {
                            showError("");
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    showError("");
                }

            });
        }catch (Exception e){
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }


    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void showError(String msg) {
        try {
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
