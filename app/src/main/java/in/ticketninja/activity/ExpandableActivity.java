package in.ticketninja.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.CustomExpandableListAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.FilesUtils;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.PathUtil;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.UserIdentificationCode;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.EventPriceCategory;
import in.ticketninja.objects.UserIdentificationDetails;
import in.ticketninja.objects.UsrIdentificationData;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestInsertTicketSession;
import in.ticketninja.ws.request.RequestUserIdentification;
import in.ticketninja.ws.response.CallbackInsertTicketSession;
import in.ticketninja.ws.response.CbUserIdentification;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static in.ticketninja.common.UserIdentificationCode.ID_CARD;
import static in.ticketninja.common.UserIdentificationCode.ID_CARD_CODE;
import static in.ticketninja.common.UserIdentificationCode.ID_NUMBER_CODE;
import static in.ticketninja.common.UserIdentificationCode.PROFESSION_CODE;

public class ExpandableActivity extends AppCompatActivity implements View.OnClickListener {
    private static final String TAG = "ExpandableActivity";
    private CustomExpandableListAdapter expandableListAdapter;
    private ExpandableListView expandableListView;
    private SRKLoaderDialog mLoader;
    private long eventId = 0;
    private long categoryId = 0;
    private int noOfTicket;
    private String eventDate, eventTime, seatType,eventName,eventCity,eventAddress;
    private EventPriceCategory selectedCategory;
    private ArrayList<EventPriceCategory> selectedDataList = new ArrayList<>();
    private CommonClass CC;
    private LoginUtils mLogin;
    private PreferencesUtils mPref;
    private ArrayList<UserIdentificationDetails> integersArrayList;
    private ArrayList<UserIdentificationDetails> useIndData = new ArrayList<>();
    public static int childSize = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exp);
        try {

            //getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
            userCodeArray();

            initializeData();

            // ACTIONBAR
            setUpActionBar();

            getUserIdentification();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            getIntentData();
            CC = new CommonClass(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
            mPref = new PreferencesUtils(this);
            expandableListView = findViewById(R.id.expandableListView);
            expandableListView.setIndicatorBounds(Utility.getWidth(this) - Utility.GetDipsFromPixel(this, 50), Utility.getWidth(this) - Utility.GetDipsFromPixel(this, 25));
            //expandableListView.addItemDecoration(new ItemDecoratorBorderListView(getResources().getDimensionPixelSize(R.dimen.stroke_size), Color.GRAY));
            Button btn = findViewById(R.id.btn);
            btn.setOnClickListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setUpActionBar() {
        try {
            androidx.appcompat.widget.Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(R.string.customer_info);
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getIntentData() {

        try {
            Intent intent = getIntent();

            if (intent.hasExtra(Constant.ScreenExtras.EVENT_ID)) {
                eventId = intent.getLongExtra(Constant.ScreenExtras.EVENT_ID, 0);
            }

            if (intent.hasExtra(Constant.ScreenExtras.EVENT_NAME)) {
                eventName = intent.getStringExtra(Constant.ScreenExtras.EVENT_NAME);
            }

            if (intent.hasExtra(Constant.ScreenExtras.PLAN_CATEGORY_ID)) {
                categoryId = intent.getLongExtra(Constant.ScreenExtras.PLAN_CATEGORY_ID, 0);
            }

            if (intent.hasExtra(Constant.ScreenExtras.SEAT_COUNT)) {
                noOfTicket = intent.getIntExtra(Constant.ScreenExtras.SEAT_COUNT, 0);
            }
            if (intent.hasExtra(Constant.ScreenExtras.EVENT_DATE)) {
                eventDate = intent.getStringExtra(Constant.ScreenExtras.EVENT_DATE);
            }

            if (intent.hasExtra(Constant.ScreenExtras.EVENT_TIME)) {
                eventTime = intent.getStringExtra(Constant.ScreenExtras.EVENT_TIME);
            }

            if (intent.hasExtra(Constant.ScreenExtras.EVENT_DATA)) {
                selectedCategory = (EventPriceCategory) intent.getSerializableExtra(Constant.ScreenExtras.EVENT_DATA);
            }

            if (intent.hasExtra(Constant.ScreenExtras.EVENT_DATA1)) {
                selectedDataList = (ArrayList<EventPriceCategory>) intent.getSerializableExtra(Constant.ScreenExtras.EVENT_DATA1);
            }

            if (intent.hasExtra(Constant.ScreenExtras.TYPE)) {
                seatType = intent.getStringExtra(Constant.ScreenExtras.TYPE);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_CITY)) {
                eventCity = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_CITY);
                Log.e(TAG, "eventCity: " + eventCity);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_ADDRESS)) {
                eventAddress = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_ADDRESS);
                Log.e(TAG, "eventAddress: " + eventAddress);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        // eventId = "1027";
        //categoryId = "1038";
    }

    private void setAdapter(ArrayList<UsrIdentificationData> list) {
        try {
            if (list != null && list.size() > 0) {
                try {
                    expandableListAdapter = new CustomExpandableListAdapter(this, list);

                    runOnUiThread(() -> {
                        expandableListView.setAdapter(expandableListAdapter);
                        expandableListView.expandGroup(0);
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void getUserIdentification() {
        //{"Get_Event_User_Identification_List":{"error_code":1,"error_description":"Sucess",
        // "ServiceName":"Get_Event_User_Identification_List",
        // "User_Identification_List":[{"Proof_Id":1000,"Proof_Name":"Customer Photo"},
        // {"Proof_Id":1001,"Proof_Name":"Full event_name"},{"Proof_Id":1002,"Proof_Name":"gender"},
        // {"Proof_Id":1003,"Proof_Name":"birthdate"},{"Proof_Id":1004,"Proof_Name":"Address"},
        // {"Proof_Id":1005,"Proof_Name":"city"},{"Proof_Id":1006,"Proof_Name":"Organization event_name"},
        // {"Proof_Id":1007,"Proof_Name":"mobileno"},{"Proof_Id":1008,"Proof_Name":"email"},
        // {"Proof_Id":1009,"Proof_Name":"ID card"}]}}


        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestUserIdentification request = new RequestUserIdentification();
            request.event_id = "" + eventId;
            request.category_id = "" + categoryId;

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CbUserIdentification> call = tnAPI.getUserIdentification(request);
            call.enqueue(new Callback<CbUserIdentification>() {
                @SuppressWarnings("ConstantConditions")
                @Override
                public void onResponse(@NonNull Call<CbUserIdentification> call, @NonNull Response<CbUserIdentification> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();

                    Log.e(TAG, "response: " + response.body());
                    //Log.e(TAG, "response: " + response.body().data());

                    new Thread(() -> {

                        if (response.isSuccessful()) {

                            if (response.body().getError_code().equals("0")) {
                                Toast.makeText(ExpandableActivity.this, response.body().getError_description(), Toast.LENGTH_LONG).show();
                                return;
                            }
                            ArrayList<Integer> apiUserCodeArrayList = new ArrayList<>();

                            ArrayList<UserIdentificationDetails> userCodeArray = response.body().getData().getUserInfo();

                            for (int k = 0; k < userCodeArray.size(); k++) {
                                apiUserCodeArrayList.add(userCodeArray.get(k).getProof_id());
                                useIndData.add(userCodeArray.get(k));

                           /* if (UserIdentificationCode.ID_CARD_CODE == userCodeArray.get(k).getProof_id()) {
                                apiUserCodeArrayList.add(UserIdentificationCode.ID_NUMBER_CODE);
                                UserIdentificationDetails usrIdentificationData = new UserIdentificationDetails();
                                usrIdentificationData.setProof_name(UserIdentificationCode.ID_NUMBER);
                                ExpandableActivity.childSize = ExpandableActivity.childSize + 1;
                                usrIdentificationData.setProof_id(ID_NUMBER_CODE);
                                usrIdentificationData.setText("");
                                 useIndData.add(usrIdentificationData);
                                Log.e(TAG, "useIndData.if");
                            }*/
                            }
                            Log.e(TAG, "useIndData.size: " + useIndData.size() + ", " + useIndData);
                            childSize = useIndData.size();
                            emptyArray(apiUserCodeArrayList);

                            ArrayList<UsrIdentificationData> arrayList = new ArrayList<>();
                            if (response.body().getData() != null) {

                    /*for (int i = 0; i < userCount; i++) {
                        UsrIdentificationData userIdentification = new UsrIdentificationData();
                        userIdentification.setGroup_title("Details of Visitor " + i);
                        userIdentification.setInfo(response.body().getData().getUserInfo());
                        arrayList.add(userIdentification);
                    }*/
                                if (response.body().getData().getUserInfo() != null && !response.body().getData().getUserInfo().isEmpty() && response.body().getData().getUserInfo().size() > 0) {
                                    for (int i = 0; i < noOfTicket; i++) {
                                        UsrIdentificationData userIdentification = new UsrIdentificationData();
                                        userIdentification.setGroup_title("Details of Visitor " + (i + 1));
                                        userIdentification.setPos(i);

                                        ArrayList<UserIdentificationDetails> info = new ArrayList<>();
                                        // ArrayList<UserIdentificationDetails> apiArray = response.body().getData().getUserInfo();
                                        ArrayList<UserIdentificationDetails> apiArray = useIndData;

                                        for (int j = 0; j < apiArray.size(); j++) {
                                            UserIdentificationDetails childInfo = new UserIdentificationDetails();
                                            childInfo.setProof_id(apiArray.get(j).getProof_id());
                                            childInfo.setProof_name(apiArray.get(j).getProof_name());
                                            childInfo.setProof_type(apiArray.get(j).getProof_type());
                                            childInfo.setProof_value(apiArray.get(j).getProof_value());
                                            info.add(childInfo);
                                        }
                                        userIdentification.setInfo(info);
                                        arrayList.add(userIdentification);
                                    }

                                    setAdapter(arrayList);
                                }
                            }
                        }
                    }).start();


                }

                @Override
                public void onFailure(@NonNull Call<CbUserIdentification> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    private String convertToJson(ArrayList<UsrIdentificationData> list) {
        try {
            JSONArray ja = new JSONArray();
            if (list != null && list.size() > 0) {
                for (int i = 0; i < list.size(); i++) {
                    ArrayList<UserIdentificationDetails> info = list.get(i).getUserInfo();
                    Log.e(TAG, "convertToJson info = " + info.size());
                    Log.e(TAG, "convert info = " + info);
                    for (int j = 0; j < info.size(); j++) {
                        UserIdentificationDetails details = info.get(j);
                        JSONObject jsonObject = new JSONObject();
                        if (!TextUtils.isEmpty(details.getText()) && details.getProof_id() != 0) {
                            if (!TextUtils.isEmpty(details.getImagePath())) {
                                if (UserIdentificationCode.FILE.equals(details.getProof_type())) {
                                    if (Utility.GALLERY.equals(details.getText())) {
                                        jsonObject.put(UserIdentificationCode.PROOF_VALUE, Utility.getImageUrlData(details.getImageType()).concat(details.getImageBase64()));
                                    } else {
                                        jsonObject.put(UserIdentificationCode.PROOF_VALUE, Utility.getImageUrlData(details.getImageType()).concat(details.getImagePath()));
                                    }

                                    jsonObject.put(UserIdentificationCode.PROOF_ID, details.getProof_id());
                                    jsonObject.put(UserIdentificationCode.PROOF_TYPE, details.getProof_type());
                                    jsonObject.put(UserIdentificationCode.SR_NO, "" + (i + 1));
                                    ja.put(jsonObject);
                                }
                            } else {
                                jsonObject.put(UserIdentificationCode.PROOF_VALUE, details.getText());
                                jsonObject.put(UserIdentificationCode.PROOF_ID, details.getProof_id());
                                jsonObject.put(UserIdentificationCode.PROOF_TYPE, details.getProof_type());
                                jsonObject.put(UserIdentificationCode.SR_NO, "" + (i + 1));
                                ja.put(jsonObject);
                            }
                        }
                    }
                }
                //jo.put("User_Info_List", ja);
                Log.e(TAG, "convertToJson = " + ja.toString());
            }
            return ja.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
        //json = [{"Photo_Id":"85471728-ea24-4e82-a781-a348afc96d78.jpg","Full_Name":"name","gender":"Male","Birth_Date":"2018-09-29","Address":"j","city":"hu","Organization_Name":"yu","mobileno":"12345688","email":"vb","Id_Type":"Aadhar Card","Id_Number":"tt"}]
        // json = [{"Photo_Id":"https:\/\/demo.ticketninja.in:4430\/Admin\/Document\/3301c0e6-e1c8-49f0-a434-805b0a5ac321.jpg","Full_Name":"bcjdjd","gender":"Male","Birth_Date":"2018-09-27","Address":"jdjdjd","city":"e7e7","Organization_Name":"eue7","mobileno":"6535","email":"yeye","Id_Type":"Aadhar Card","Id_Number":"eyyeeu"}]
        // json = [{"Full_Name":"zen","city":"Ahmedabad","Photo_Id":"","gender":"","Birth_Date":"1900-01-01","Address":"","Organization_Name":"","mobileno":"","email":"","Id_Type":"","Id_Number":""}]
    }

    /*private String convertToJson1(ArrayList<UsrIdentificationData> list) {

        try {
            //JSONObject jo = new JSONObject();
            JSONArray ja = new JSONArray();
            for (int i = 0; i < list.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                for (int j = 0; j < list.get(i).getUserInfo().size(); j++) {
                    UserIdentificationDetails details = list.get(i).getUserInfo().get(j);
                    if (details.getProof_name().equalsIgnoreCase(ID_CARD)) {
                        jsonObject.put(ID_TYPE, details.getText());
                        jsonObject.put(_ID_NUMBER, details.getText());
                    } else if (details.getProof_name().equalsIgnoreCase(ID_NUMBER)) {
                        jsonObject.put(_ID_NUMBER, details.getText());
                    } else if (details.getProof_name().equalsIgnoreCase(FULL_NAME)) {
                        jsonObject.put(_FULL_NAME, details.getText());
                    } else if (details.getProof_name().equalsIgnoreCase(BIRTH_DATE)) {
                        if (Validate.isNotNull(details.getText())) {
                            Date date = DateTimeUtils.stringToDate(details.getText());
                            String strDate = DateTimeUtils.dateToString(date, DateTimeUtils.SERVER_FORMAT_DATE);
                            jsonObject.put(_BIRTH_DATE, strDate);
                        } else {
                            jsonObject.put(_BIRTH_DATE, STR_BIRTH_DATE);
                        }
                    } else if (details.getProof_name().equalsIgnoreCase(ORGANIZATION_NAME)) {
                        jsonObject.put(_ORGANIZATION_NAME, details.getText());
                    } else if (details.getProof_name().equalsIgnoreCase(CUSTOMER_PHOTO)) {
                        jsonObject.put(PHOTO_ID, details.getText());
                    } else {
                        jsonObject.put(details.getProof_name(), details.getText());
                    }
                }
                ja.put(i, jsonObject);
            }
            //jo.put("User_Info_List", ja);
            Log.e(TAG, "json = " + ja.toString());
            return ja.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
        //json = [{"Photo_Id":"85471728-ea24-4e82-a781-a348afc96d78.jpg","Full_Name":"name","gender":"Male","Birth_Date":"2018-09-29","Address":"j","city":"hu","Organization_Name":"yu","mobileno":"12345688","email":"vb","Id_Type":"Aadhar Card","Id_Number":"tt"}]
        // json = [{"Photo_Id":"https:\/\/demo.ticketninja.in:4430\/Admin\/Document\/3301c0e6-e1c8-49f0-a434-805b0a5ac321.jpg","Full_Name":"bcjdjd","gender":"Male","Birth_Date":"2018-09-27","Address":"jdjdjd","city":"e7e7","Organization_Name":"eue7","mobileno":"6535","email":"yeye","Id_Type":"Aadhar Card","Id_Number":"eyyeeu"}]
        // json = [{"Full_Name":"zen","city":"Ahmedabad","Photo_Id":"","gender":"","Birth_Date":"1900-01-01","Address":"","Organization_Name":"","mobileno":"","email":"","Id_Type":"","Id_Number":""}]
    }
*/

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == Utility.REQUEST_CAMERA) {
                if (CC.isOnline()) {
                    if (data != null) {
                        Log.e(TAG, "Data: " + data.getData());
                        Bitmap thumbnail = (Bitmap) Objects.requireNonNull(data.getExtras()).get("data");
                        if (thumbnail != null) {
                            long fileSize = Utility.getBitmapSize(thumbnail);
                            //Log.e(TAG, "fileSize: " + fileSize);
                            if (fileSize <= 1) {
                                expandableListAdapter.getIv().setImageBitmap(thumbnail);
                                String imageData = FilesUtils.bitmapToBase64(thumbnail);
                                //Log.e(TAG, "imageData: " + imageData);
                                expandableListAdapter.getIv().setImageBitmap(thumbnail);
                                expandableListAdapter.setPic(imageData, Utility.CAMERA, Utility.getImageType(thumbnail), imageData);
                                Log.e(TAG, "type: " + Utility.getImageType(thumbnail));
                                //String extension = ".jpg";
                                //wsUploadProfileImage(imageData, extension);
                            } else {
                                expandableListAdapter.setPic("", "", "", "");
                                Toast.makeText(this, R.string.plese_select_photo, Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            } else if (requestCode == Utility.SELECT_FILE) {
                if (CC.isOnline()) {
                    if (data != null) {
                        Uri mImageUri = data.getData();
                        Log.e(TAG, "mImageUri: " + mImageUri);
                        if (mImageUri != null) {
                            try {
                                String path = PathUtil.getPath(this, mImageUri);
                                long fileSize = Utility.getFileSize(path);
                                if (fileSize <= 1) {
                                    Log.e(TAG, "ifffff");
                                    //String extension = path.substring(path.lastIndexOf("."));
                                    //Log.e(TAG, "extension: " + extension);
                                    Bitmap bm = Utility.getBitmap(path);
                                    expandableListAdapter.getIv().setImageBitmap(bm);

                                    String imageData = FilesUtils.bitmapToBase64(Objects.requireNonNull(bm));

                                    expandableListAdapter.setPic(path, Utility.GALLERY, Utility.getImageType(Objects.requireNonNull(bm)), imageData);
                                    //new FilesUtils.GetBase64FromUri1(this, imageData -> wsUploadProfileImage(imageData, extension)).execute(mImageUri);
                                } else {
                                    expandableListAdapter.setPic("", "", "", "");
                                    Toast.makeText(this, R.string.plese_select_photo, Toast.LENGTH_LONG).show();
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // WS - UPLOAD PROFILE IMAGE
   /* private void wsUploadProfileImage(String imgStr, String extension) {
        if (!mLoader.isShowing() && !isFinishing()) mLoader.show();
        RequestUploadImage request = new RequestUploadImage(imgStr, extension);
        TicketNinjaAPI tnAPI = RestApi.createAPI();
        Call<CallbackUploadImage> call = tnAPI.uploadImage(request);
        call.enqueue(new Callback<CallbackUploadImage>() {
            @Override
            public void onResponse(@NonNull Call<CallbackUploadImage> call, @NonNull Response<CallbackUploadImage> response) {
                if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                try {
                    if (response.isSuccessful()) {

                        if (response.body() != null) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                                if (Validate.isNotNull(Objects.requireNonNull(response.body()).getData().getImage_URL())) {
                                    Glide.with(ExpandableActivity.this)
                                            .load(Objects.requireNonNull(response.body()).getData().getImage_URL())
                                            .centerCrop()
                                            .listener(new RequestListener<String, GlideDrawable>() {
                                                @Override
                                                public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                                    return false;
                                                }

                                                @Override
                                                public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                                    expandableListAdapter.getIv().setImageDrawable(resource);
                                                    String imageId = Objects.requireNonNull(response.body()).getData().getImage_Id();
                                                    expandableListAdapter.setPic(Objects.requireNonNull(response.body()).getData().getImage_URL(), imageId, "","");
                                                    return true;
                                                }
                                            })
                                            .skipMemoryCache(false)
                                            .diskCacheStrategy(DiskCacheStrategy.NONE)
                                            .placeholder(R.drawable.ic_palceholder_edit_profile)
                                            .error(R.drawable.ic_palceholder_edit_profile)
                                            .into(expandableListAdapter.getIv());
                                } else {
                                    Glide.with(ExpandableActivity.this)
                                            .load(R.drawable.ic_palceholder_edit_profile)
                                            .into(expandableListAdapter.getIv());
                                }

                                Toast.makeText(ExpandableActivity.this, Objects.requireNonNull(response.body()).getData().getErrorMessage(), Toast.LENGTH_LONG).show();

                            } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                                CC.showAlert(Objects.requireNonNull(response.body()).getError_description());
                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CC.showToast(R.string.msg_something_wrong);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CallbackUploadImage> call, @NonNull Throwable t) {
                if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                CC.showToast(R.string.msg_something_wrong);
                t.printStackTrace();
            }
        });
    }*/


    @Override
    public void onClick(View view) {
        try {
            if (expandableListAdapter != null) {
                ArrayList<UsrIdentificationData> arrayList = expandableListAdapter.getArrayList();
                if (arrayList != null && arrayList.size() > 0) {
                    for (int i = 0; i < arrayList.size(); i++) {

                        for (int j = 0; j < arrayList.get(i).getUserInfo().size(); j++) {
                            UserIdentificationDetails details = arrayList.get(i).getUserInfo().get(j);

                            if (details.getProof_id() != 0) {

                                if (Validate.isNull(details.getText())) {
                                    expandableListView.expandGroup(i);
                                    for (int k = 0; k < arrayList.size(); k++) {
                                        if (k != i) {
                                            expandableListView.collapseGroup(k);
                                        }
                                    }
                                    if (UserIdentificationCode.CUSTOMER_PHOTO_CODE == details.getProof_id()) {
                                        if (Validate.isNull(details.getImagePath())) {
                                            Toast.makeText(this, getString(R.string.please_select) + " " + details.getProof_name(), Toast.LENGTH_LONG).show();
                                            return;
                                        }
                                    } else {
                                        expandableListAdapter.updateItem(i, j);
                                        expandableListView.setSelection(j);
                                        Toast.makeText(this, getString(R.string.please_enter) + " " + details.getProof_name(), Toast.LENGTH_LONG).show();
                                        return;
                                    }
                                    //return;
                                } else if (UserIdentificationCode.EMAIL_CODE == details.getProof_id()) {
                                    if (Validate.isNotNull(details.getText()) && !Validate.checkEmail(details.getText())) {
                                        Toast.makeText(this, getString(R.string.login_msg_enter_email_valid), Toast.LENGTH_LONG).show();
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }

                String userInfo = convertToJson(Objects.requireNonNull(arrayList));

                try {
                    Log.e(TAG, "seatType123456: " + seatType);
                    if (Constant.SEAT_TYPE_ARRANGE_SEATING.equalsIgnoreCase(seatType)) {
                        Intent intent = new Intent(getApplicationContext(), SeatSelectionActivity.class);
                        intent.putExtra(Constant.ScreenExtras.EVENT_ID, eventId);
                        intent.putExtra(Constant.ScreenExtras.SHOW_ID, selectedCategory.showId());
                        intent.putExtra(Constant.ScreenExtras.PLAN_CATEGORY_ID, selectedCategory.id());
                        intent.putExtra(Constant.ScreenExtras.SEAT_COUNT, selectedCategory.noOfTicket());
                        intent.putExtra(Constant.ScreenExtras.IDENTIFICATION, selectedCategory.isIdentification());
                        intent.putExtra(Constant.ScreenExtras.USER_IDENTIFICATION_LIST, userInfo);
                        intent.putExtra(Constant.ScreenExtras.TYPE, selectedCategory.seatType());

                        intent.putExtra(Constant.ScreenExtras.EVENT_DATE, selectedCategory.eventDate());
                        intent.putExtra(Constant.ScreenExtras.EVENT_TIME, selectedCategory.time());
                        intent.putExtra(Constant.ScreenExtras.EVENT_NAME, eventName);
                        intent.putExtra(Constant.ScreenExtras.EVENT_CITY, eventCity);
                        intent.putExtra(Constant.ScreenExtras.EVENT_ADDRESS, eventAddress);
                        startActivity(intent);
                        finish();
                    } else {
                        //wsInsertTicketSession(eventId, eventDate, eventTime, mLogin.getUserId(), selectedCategory, userInfo);
                        wsInsertTicketSession(eventId, eventDate, eventTime, mLogin.getUserId(), selectedDataList, userInfo);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void userCodeArray() {
        try {
            integersArrayList = new ArrayList<>();

            UserIdentificationDetails usr1 = new UserIdentificationDetails();
            usr1.setProof_name(UserIdentificationCode.CUSTOMER_PHOTO);
            usr1.setProof_id(UserIdentificationCode.CUSTOMER_PHOTO_CODE);
            integersArrayList.add(usr1);


            UserIdentificationDetails usr2 = new UserIdentificationDetails();
            usr2.setProof_name(UserIdentificationCode.FULL_NAME);
            usr2.setProof_id(UserIdentificationCode.FULL_NAME_CODE);
            integersArrayList.add(usr2);

            UserIdentificationDetails usr3 = new UserIdentificationDetails();
            usr3.setProof_name(UserIdentificationCode.GENDER);
            usr3.setProof_id(UserIdentificationCode.GENDER_CODE);
            integersArrayList.add(usr3);


            UserIdentificationDetails usr4 = new UserIdentificationDetails();
            usr4.setProof_name(UserIdentificationCode.BIRTH_DATE);
            usr4.setProof_id(UserIdentificationCode.BIRTH_DATE_CODE);
            integersArrayList.add(usr4);


            UserIdentificationDetails usr5 = new UserIdentificationDetails();
            usr5.setProof_name(UserIdentificationCode.ADDRESS);
            usr5.setProof_id(UserIdentificationCode.ADDRESS_CODE);
            integersArrayList.add(usr5);


            UserIdentificationDetails usr6 = new UserIdentificationDetails();
            usr6.setProof_name(UserIdentificationCode.CITY);
            usr6.setProof_id(UserIdentificationCode.CITY_CODE);
            integersArrayList.add(usr6);


            UserIdentificationDetails usr7 = new UserIdentificationDetails();
            usr7.setProof_name(UserIdentificationCode.ORGANIZATION_NAME);
            usr7.setProof_id(UserIdentificationCode.ORGANIZATION_NAME_CODE);
            integersArrayList.add(usr7);

            UserIdentificationDetails usr8 = new UserIdentificationDetails();
            usr8.setProof_name(UserIdentificationCode.MOBILE_NO);
            usr8.setProof_id(UserIdentificationCode.MOBILE_NO_CODE);
            integersArrayList.add(usr8);

            UserIdentificationDetails usr9 = new UserIdentificationDetails();
            usr9.setProof_name(UserIdentificationCode.EMAIL);
            usr9.setProof_id(UserIdentificationCode.EMAIL_CODE);
            integersArrayList.add(usr9);

            UserIdentificationDetails usr10 = new UserIdentificationDetails();
            usr10.setProof_name(ID_CARD);
            usr10.setProof_id(ID_CARD_CODE);
            integersArrayList.add(usr10);

            // UserIdentificationDetails usr11 = new UserIdentificationDetails();
            // usr11.setProof_name(UserIdentificationCode.ID_NUMBER);
            // usr11.setProof_id(ID_NUMBER_CODE);
            //integersArrayList.add(usr11);

            UserIdentificationDetails usr12 = new UserIdentificationDetails();
            usr12.setProof_name(UserIdentificationCode.PROFESSION);
            usr12.setProof_id(PROFESSION_CODE);
            integersArrayList.add(usr12);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    private void emptyArray(ArrayList<Integer> apiUserCodeArrayList) {
        try {
            if (integersArrayList != null && integersArrayList.size() > 0) {
                for (int i = 0; i < integersArrayList.size(); i++) {
                    if (!apiUserCodeArrayList.contains(integersArrayList.get(i).getProof_id())) {
                        UserIdentificationDetails usrIdentificationData = new UserIdentificationDetails();
                        usrIdentificationData.setProof_name(integersArrayList.get(i).getProof_name());

                        if (integersArrayList.get(i).getProof_id() == ID_NUMBER_CODE) {
                            if (apiUserCodeArrayList.contains(ID_CARD_CODE)) {
                                ExpandableActivity.childSize = ExpandableActivity.childSize + 1;
                                usrIdentificationData.setProof_id(ID_NUMBER_CODE);
                            }
                        } else {
                            usrIdentificationData.setProof_id(0);
                        }

                        usrIdentificationData.setText("");
                        useIndData.add(usrIdentificationData);
                    }
                }
            }
            // Log.e(TAG, "useIndData: " + useIndData);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // WS - Insert Ticket Session
    //private void wsInsertTicketSession(long eventId, String eventDate, String timeSlot, String userId, EventPriceCategory categoryList, String User_Info_List) {
    private void wsInsertTicketSession(long eventId, String eventDate, String timeSlot, String userId, List<EventPriceCategory> categoryList, String User_Info_List) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestInsertTicketSession request = new RequestInsertTicketSession(eventId, eventDate, timeSlot, String.valueOf(userId), categoryList, User_Info_List);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackInsertTicketSession> call = tnAPI.insertTicketSession(request);
            call.enqueue(new Callback<CallbackInsertTicketSession>() {
                @Override
                public void onResponse(@NonNull Call<CallbackInsertTicketSession> call, @NonNull Response<CallbackInsertTicketSession> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                String sessionID = response.body().data().sessionID();
                                finish();
                                if (Validate.isNull(mPref.getUserEmailId()) || Validate.isNull(mPref.getUserPhoneNumber()) ||
                                        Validate.isNull(mPref.getCountryCode())) {

                                    // TODO: 29/3/18 SHREYA 1 : Contact Detail Flow
//                                startActivity(new Intent(EventPricingMultidayActivity.this, ContactConfirmationActivity.class));

                                    //ContactConfirmationActivity.Companion.start(ExpandableActivity.this, sessionID, Constant.ContactActions.EDIT,"");
                                    ContactConfirmationActivity.Companion.start(ExpandableActivity.this, sessionID, Constant.ContactActions.EDIT, seatType, User_Info_List);

                                } else {
                                    // OrderSummaryActivity.start(ExpandableActivity.this, sessionID, "", "");
                                    OrderSummaryActivity.start(ExpandableActivity.this, sessionID, seatType, User_Info_List);
                                }
                            } else if (Validate.isNotNull(response.body().data().message())) {
                                CC.showAlert(response.body().data().message());
                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        } else {
                            CC.showToast(R.string.msg_no_response);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CC.showToast(R.string.msg_something_wrong);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackInsertTicketSession> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }
    }
    //{"Get_Event_User_Identification_List":{"error_code":1,"error_description":"Sucess","ServiceName":"Get_Event_User_Identification_List","User_Identification_List":[{"Proof_Id":1001,"Proof_Name":"Full event_name"},{"Proof_Id":1002,"Proof_Name":"gender"},{"Proof_Id":1003,"Proof_Name":"birthdate"},{"Proof_Id":1004,"Proof_Name":"Address"},{"Proof_Id":1005,"Proof_Name":"city"}]}}

}
