/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.BookingCategoryAdapter;
import in.ticketninja.common.CacheHelper;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.FilesUtils;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.database.DatabaseHelper;
import in.ticketninja.objects.BookingHistory;
import in.ticketninja.objects.BookingTicketList;
import in.ticketninja.service.TimerService;
import in.ticketninja.widget.BarCodeDialog;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestDownloadBooking;
import in.ticketninja.ws.request.RequestGetBookingDetailForCOD;
import in.ticketninja.ws.request.RequestGetBookingDetailForOnline;
import in.ticketninja.ws.response.CallbackDownloadBooking;
import in.ticketninja.ws.response.CallbackGetBookingDetailForCOD;
import in.ticketninja.ws.response.CallbackGetBookingDetailForOnline;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingConfirmationActivity extends AppCompatActivity {

    private final String TAG = BookingConfirmationActivity.class.getSimpleName();
    private CommonClass CC;
    private DatabaseHelper databaseHelper;
    private SRKLoaderDialog mLoader;
    @Constant.PaymentWSType
    // private String transactionType = Constant.PaymentType.COD;
    // private long transactionId = 1100;
    private String transactionType = Constant.PaymentType.ONLINE;
    private long transactionId = 2058;

    private View llMainLayout;
    private View llErrorLayout;
    private ImageView ivErrorImage;
    private TextView tvErrorMessage, tvErrorButton;

    private ImageView ivEventPoster;
    private ImageView ivBookingBarcode;
    private TextView tvBookingId;
    private TextView tvEventTitle;
    private TextView tvEventDate;
    private TextView tvEventTime;
    private TextView tvBookingTicketCount;
    private TextView tvBookingVenue;
    private RecyclerView rvBookingCategory;
    private TextView tvBookingLabelPayAmount;
    private TextView tvBookingPayAmount;
    private TextView tvBookingPaymentType;
    private TextView tvBookingBookingDate;
    private TextView tvBookingDeliveryAddress;
    private TextView tvBookingDeliveryMobile;
    private TextView tvBookingDeliveryTime;
    private TextView tvBookingSubTotal;
    private TextView tvBookingDiscount;
    private View llFooter;
    private View llViewTicket;
    private ImageView btnDownload;

    private View bookingRowAddress, bookingRowAddressDivider;

    private LoginUtils mLogin;
    private LinearLayout llSubTotalDiscount;
    private TextView tvBookingLabelDiscount;
    private TextView tvBookingLabelSubTotal;

    private TextView tvTotalProcessingFee, tvPFee1, tvPFee2, tvLabelPFee2, tvLabelProcessingFee;
    private LinearLayout llProcessingFee, llProcessing, llDis, llSubTtl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_booking_confirmation);

            initializeData();

            getIntentData();

            setupActionbar();

            stopService(new Intent(BookingConfirmationActivity.this, TimerService.class));
            Utility.hideKeyboard(BookingConfirmationActivity.this);

            findViewById();

            tvLabelProcessingFee.setOnClickListener(view -> {
                if (llProcessing.getVisibility() == View.VISIBLE) {
                    tvLabelProcessingFee.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_booking_rupees, 0, R.drawable.ic_share_arrow_down, 0);
                    llProcessing.setVisibility(View.GONE);
                } else {
                    tvLabelProcessingFee.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_booking_rupees, 0, R.drawable.ic_share_arrow_up, 0);
                    llProcessing.setVisibility(View.VISIBLE);
                }
            });

            // REMOVE CACHED DASHBOARD DATA
            CacheHelper mCacheHelper = new CacheHelper(this);
            mCacheHelper.remove(CacheHelper.KEY_DASHBOARD);

            if (CC.isOnline()) {
                if (Validate.isNotNull(transactionType) && transactionType.toLowerCase().equals(Constant.PaymentType.ONLINE.toLowerCase())) {
                    wsGetBookingDetailForOnline(transactionId);
                } else if (Validate.isNotNull(transactionType) && transactionType.toLowerCase().equals(Constant.PaymentType.COD.toLowerCase())) {
                    wsGetBookingDetailForCOD(transactionId);
                } else if (Validate.isNotNull(transactionType) && transactionType.toLowerCase().equals(Constant.PaymentType.COMPLEMENTARY.toLowerCase())) {
                    wsGetBookingDetailForOnline(transactionId);
                } else {
                    tvErrorMessage.setText(R.string.msg_something_wrong);
                    showError();
                }
            } else {
                showNoInternet();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void initializeData() {
        try {
            CC = new CommonClass(this);
            databaseHelper = new DatabaseHelper(this);
            mLoader = new SRKLoaderDialog(this);
            mLogin = new LoginUtils(this);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        try {
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID)) {
                transactionId = getIntent().getLongExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, 0);
            }
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE)) {
                transactionType = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setupActionbar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
                findViewById(R.id.ivActionLogo).setVisibility(View.VISIBLE);
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void findViewById() {
        try {
            // TYPEFACE
            Typeface tfRegular = TypefaceUtils.HelveticaRegular(this);
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);


            llMainLayout = findViewById(R.id.llMainLayout);
            llMainLayout.setVisibility(View.GONE);
            llErrorLayout = findViewById(R.id.llErrorLayout);
            ivErrorImage = findViewById(R.id.ivErrorImage);
            tvErrorMessage = findViewById(R.id.tvErrorMessage);
            tvErrorButton = findViewById(R.id.tvErrorButton);

            ivEventPoster = findViewById(R.id.ivEventPoster);
            ivBookingBarcode = findViewById(R.id.ivBookingBarcode);
            tvBookingId = findViewById(R.id.tvBookingId);
            tvBookingId.setTypeface(tfMedium);
            tvEventTitle = findViewById(R.id.tvEventTitle);
            tvEventTitle.setTypeface(tfMedium);
            tvEventDate = findViewById(R.id.tvEventDate);
            tvEventDate.setTypeface(tfRegular);
            tvEventTime = findViewById(R.id.tvEventTime);
            tvEventTime.setTypeface(tfRegular);
            tvBookingTicketCount = findViewById(R.id.tvBookingTicketCount);
            tvBookingTicketCount.setTypeface(tfRegular);
            tvBookingVenue = findViewById(R.id.tvBookingVenue);
            tvBookingVenue.setTypeface(tfRegular);
            tvBookingLabelPayAmount = findViewById(R.id.tvBookingLabelPayAmount);
            tvBookingLabelPayAmount.setTypeface(tfRegular);
            tvBookingLabelDiscount = findViewById(R.id.tvBookingLabelDiscount);
            tvBookingLabelDiscount.setTypeface(tfRegular);
            tvBookingLabelSubTotal = findViewById(R.id.tvBookingLabelSubTotal);
            tvBookingLabelSubTotal.setTypeface(tfRegular);
            tvBookingSubTotal = findViewById(R.id.tvBookingSubTotal);
            tvBookingSubTotal.setTypeface(tfMedium);
            tvBookingDiscount = findViewById(R.id.tvBookingDiscount);
            tvBookingDiscount.setTypeface(tfMedium);
            tvBookingPayAmount = findViewById(R.id.tvBookingPayAmount);
            tvBookingPayAmount.setTypeface(tfMedium);
            llSubTotalDiscount = findViewById(R.id.llSubTotalDiscount);


            TextView tvBookingLabelPaymentType = findViewById(R.id.tvBookingLabelPaymentType);
            tvBookingLabelPaymentType.setTypeface(tfRegular);
            tvBookingPaymentType = findViewById(R.id.tvBookingPaymentType);
            tvBookingPaymentType.setTypeface(tfMedium);
            TextView tvBookingLabelBookingDate = findViewById(R.id.tvBookingLabelBookingDate);
            tvBookingLabelBookingDate.setTypeface(tfRegular);
            tvBookingBookingDate = findViewById(R.id.tvBookingBookingDate);
            tvBookingBookingDate.setTypeface(tfRegular);
            tvBookingDeliveryAddress = findViewById(R.id.tvBookingDeliveryAddress);
            tvBookingDeliveryAddress.setTypeface(tfMedium);
            tvBookingDeliveryMobile = findViewById(R.id.tvBookingDeliveryMobile);
            tvBookingDeliveryMobile.setTypeface(tfMedium);
            tvBookingDeliveryTime = findViewById(R.id.tvBookingDeliveryTime);
            tvBookingDeliveryTime.setTypeface(tfMedium);
            llFooter = findViewById(R.id.llFooter);
            llViewTicket = findViewById(R.id.llViewTicket);
            Button btnViewTicket = findViewById(R.id.btnViewTicket);
            btnViewTicket.setTypeface(tfMedium);
            btnDownload = findViewById(R.id.btnDownload);

            bookingRowAddress = findViewById(R.id.bookingRowAddress);
            bookingRowAddressDivider = findViewById(R.id.bookingRowAddressDivider);

            rvBookingCategory = findViewById(R.id.rvBookingCategory);
            rvBookingCategory.setHasFixedSize(true);
            rvBookingCategory.setNestedScrollingEnabled(false);

            tvTotalProcessingFee = findViewById(R.id.tvTotalProcessingFee);
            tvTotalProcessingFee.setTypeface(tfMedium);
            tvPFee1 = findViewById(R.id.tvPFee1);
            tvPFee2 = findViewById(R.id.tvPFee2);
            tvLabelPFee2 = findViewById(R.id.tvLabelPFee2);
            llProcessingFee = findViewById(R.id.llProcessingFee);
            tvLabelProcessingFee = findViewById(R.id.tvLabelProcessingFee);
            tvLabelProcessingFee.setTypeface(tfRegular);
            llProcessing = findViewById(R.id.llProcessing);
            llSubTtl = findViewById(R.id.llSubTtl);
            llDis = findViewById(R.id.llDis);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void invalidateData(BookingHistory data) {
        try {
            if (Validate.isNotNull(data.getImageUrl())) {
                Glide.with(this)
                        .load(data.getImageUrl())
                        //.centerCrop()
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(ivEventPoster);
                ivEventPoster.setVisibility(View.VISIBLE);
            } else {
                ivEventPoster.setVisibility(View.GONE);
            }

            if (Validate.isNotNull(data.getQr_code_url())) {
                Glide.with(this)
                        .load(Constant.generateQRCodeURL1(data.getQr_code_url()))
                        .centerCrop()
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .skipMemoryCache(false)
                        .into(ivBookingBarcode);
            } else {
                Glide.with(this)
                        .load(Constant.generateQRCodeURL(data.getQRCode()))
                        .centerCrop()
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .skipMemoryCache(false)
                        .into(ivBookingBarcode);
            }

            ivBookingBarcode.setOnClickListener(v -> {
                BarCodeDialog dialog = new BarCodeDialog(BookingConfirmationActivity.this, R.style.transparent_dialog_borderless);
                dialog.setBookingId(data.getBookingId());
                if (Validate.isNotNull(data.getQr_code_url())) {
                    dialog.setImageUrl(Constant.generateQRCodeURL1(data.getQr_code_url()), data.qr_code);
                } else {
                    dialog.setImageUrl(Constant.generateQRCodeURL(data.getQRCode()), data.qr_code);
                }
                dialog.show();
            });

            tvBookingId.setText(Validate.isNotNull(data.getBookingId()) ? data.getBookingId() : "");
            tvEventTitle.setText(Validate.isNotNull(data.getName()) ? String.format("%s", data.getName()) : "");

            if (Validate.isNotNull(data.getMDDate())) {
                if ((data.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY)
                        && data.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW))
                        && Constant.Code.ONE == data.isSeasonPass()) {
                    tvEventDate.setText(Validate.isNotNull(data.getMDDate()) ? String.format("%s, %s", data.getMDDate(), data.getYear()) : "");
                } else {
                    tvEventDate.setText(Validate.isNotNull(data.getMDDate()) ? String.format("%s, %s\n%s", data.getMDDate(), data.getYear(), data.getDay()) : "");
                }
            } else {
                tvEventDate.setText("-");
            }

            if (data.getDayType().equalsIgnoreCase(Constant.EventDayType.SINGLE_DAY)) {
                tvEventTime.setText(String.format("%s %s", data.getEvent_time(), Constant.ONWARDS));
            } else if (data.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY)
                    || data.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW)) {
                tvEventTime.setText(String.format("%s %s", data.getEvent_time(), Constant.ONWARDS));
            } else if (data.getDayType().equalsIgnoreCase(Constant.EventDayType.PERPETUAL)) {
                tvEventTime.setText(String.format("%s", data.getEvent_time()));
            } else {
                tvEventTime.setText(String.format("%s", data.getEvent_time()));
            }

            tvBookingVenue.setText(Validate.isNotNull(data.getVenueName()) ? String.format("%s", data.getVenueName()) : "");
            tvBookingVenue.setOnClickListener(v -> CommonClass.openMap(v.getContext(), data.getVenueName(), data.getLatitude(), data.getLongitude()));

            tvBookingTicketCount.setText(String.format(Locale.getDefault(), data.getTotalTicket() > 1 ? "%d\nTickets" : "%d\nTicket", data.getTotalTicket()));

            if (data.getTicketList() != null) {
                List<BookingTicketList> bookingTicketLists = CommonClass.groupTicketList(data.getTicketList());
                BookingCategoryAdapter mAdapter = new BookingCategoryAdapter(bookingTicketLists);
                rvBookingCategory.setAdapter(mAdapter);
            }

            Log.e(TAG, "data.getTotal_processingfee()=" + data.getTota_ProcessingFee());
            if (data.getTota_ProcessingFee() > 0) {
                llSubTotalDiscount.setVisibility(View.VISIBLE);
                tvBookingSubTotal.setVisibility(View.GONE);
                tvBookingDiscount.setVisibility(View.GONE);
                tvBookingLabelSubTotal.setVisibility(View.GONE);
                tvBookingLabelDiscount.setVisibility(View.GONE);

                llProcessingFee.setVisibility(View.VISIBLE);
                // tvBookingSubTotal.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getAmount()));
                tvTotalProcessingFee.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getTota_ProcessingFee()));
                tvPFee1.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getProcessingfee()));
                tvPFee2.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getTax()));
                tvLabelPFee2.setText(data.getTax_type());
            } else {
                llProcessingFee.setVisibility(View.GONE);
            }

            if (Validate.isNotNull(data.getCouponCode())) {
                llSubTotalDiscount.setVisibility(View.VISIBLE);

                llSubTtl.setVisibility(View.VISIBLE);
                tvBookingSubTotal.setVisibility(View.VISIBLE);
                tvBookingDiscount.setVisibility(View.VISIBLE);
                llDis.setVisibility(View.VISIBLE);
                tvBookingLabelSubTotal.setVisibility(View.VISIBLE);
                tvBookingLabelDiscount.setVisibility(View.VISIBLE);

                tvBookingSubTotal.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getAmount()));
                tvBookingDiscount.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getDiscount()));
                Log.e(TAG, "data.getAmount()_1:" + data.getAmount());
                if (data.getNetamount() == 0) {
                    tvBookingPayAmount.setText(R.string.str_price_free);
                } else {
                    tvBookingPayAmount.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getNetamount()));
                }
            } else {
                if (llProcessingFee.getVisibility() == View.GONE) {
                    llSubTotalDiscount.setVisibility(View.GONE);
                }
                llSubTtl.setVisibility(View.GONE);
                llDis.setVisibility(View.GONE);
                Log.e(TAG, "data.getAmount()_2:" + data.getAmount());
                if (data.getAmount() == 0) {
                    tvBookingPayAmount.setText(R.string.str_price_free);
                } else {
                    //tvBookingPayAmount.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getAmount()));
                    tvBookingPayAmount.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getNetamount()));
                }
            }


            //tvBookingPaymentType.setText(Validate.isNotNull(data.getPaymentType()) ? data.getPaymentType() : "");
            tvBookingPaymentType.setText(Validate.isNotNull(Utility.strSmallCap(data.getPaymentType())) ? Utility.strSmallCap(data.getPaymentType()) : "");

            tvBookingBookingDate.setText(Validate.isNotNull(data.getTransDate()) ? data.getTransDate() : "");

            tvBookingDeliveryAddress.setText(Validate.isNotNull(data.getP_d_address()) ? "Delivery Address : " + data.getP_d_address() : "");
            tvBookingDeliveryMobile.setText(Validate.isNotNull(data.getMobileno()) ? "Phone : " + data.getMobileno() : "");
            tvBookingDeliveryTime.setText(Validate.isNotNull(data.getDeliveryTimeSlot()) ? "event_time : " + data.getDeliveryTimeSlot() : "");

       /* if (Validate.isNotNull(transactionType) && (transactionType.toLowerCase().equals(Constant.PaymentType.ONLINE.toLowerCase()) || transactionType.equals(Constant.PaymentType.RSVP))) {
            llFooter.setVisibility(View.VISIBLE);
            bookingRowAddress.setVisibility(View.GONE);
            bookingRowAddressDivider.setVisibility(View.GONE);

            //tvBookingLabelPayAmount.setText(R.string.booking_confirmation_amount_paid);

            *//*SpannableString spannable = new SpannableString(getString(R.string.booking_confirmation_amount_paid));
            spannable.setSpan(new StrikethroughSpan(), 0, 4, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            tvBookingLabelPayAmount.setText(spannable);*//*

            //tvBookingLabelPayAmount.setPaintFlags(tvBookingLabelPayAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

            llViewTicket.setOnClickListener(v -> {
                Intent intViewTicket = new Intent(getApplicationContext(), MTicketsActivity.class);
                intViewTicket.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, data.getTrans_id());
                intViewTicket.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE, data.getPaymentType());
                intViewTicket.putExtra(Constant.ScreenExtras.BOOKING_DATA, data);
                startActivityForResult(intViewTicket, Constant.ActivityForResult.FROM_MY_TICKETS);
                //finish();
            });

            btnDownload.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    if (ContextCompat.checkSelfPermission(BookingConfirmationActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || ContextCompat.checkSelfPermission(BookingConfirmationActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(BookingConfirmationActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                                Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE);
                    } else {
                        wsDownloadTicket(transactionId, data.getBookingId());
                    }

                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });

        } else {
            llFooter.setVisibility(View.GONE);

            bookingRowAddress.setVisibility(View.VISIBLE);
            bookingRowAddressDivider.setVisibility(View.VISIBLE);

            tvBookingLabelPayAmount.setText(R.string.booking_confirmation_amount_payable);
        }*/


            if (Validate.isNotNull(transactionType) && (transactionType.toLowerCase().equals(Constant.PaymentType.COD.toLowerCase()))) {
                llFooter.setVisibility(View.GONE);
                bookingRowAddress.setVisibility(View.VISIBLE);
                bookingRowAddressDivider.setVisibility(View.VISIBLE);

                tvBookingLabelPayAmount.setText(R.string.booking_confirmation_amount_payable);
            } else {

                llFooter.setVisibility(View.VISIBLE);
                bookingRowAddress.setVisibility(View.GONE);
                bookingRowAddressDivider.setVisibility(View.GONE);

                //tvBookingLabelPayAmount.setText(R.string.booking_confirmation_amount_paid);

            /*SpannableString spannable = new SpannableString(getString(R.string.booking_confirmation_amount_paid));
            spannable.setSpan(new StrikethroughSpan(), 0, 4, Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
            tvBookingLabelPayAmount.setText(spannable);*/

                //tvBookingLabelPayAmount.setPaintFlags(tvBookingLabelPayAmount.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

                llViewTicket.setOnClickListener(v -> {
                    Intent intViewTicket = new Intent(getApplicationContext(), MTicketsActivity.class);
                    intViewTicket.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, data.getTrans_id());
                    intViewTicket.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE, data.getPaymentType());
                    intViewTicket.putExtra(Constant.ScreenExtras.BOOKING_DATA, data);
                    startActivityForResult(intViewTicket, Constant.ActivityForResult.FROM_MY_TICKETS);
                    //finish();
                });

                btnDownload.setOnClickListener(v -> {
                    if (CC.isOnline()) {
                        if (ContextCompat.checkSelfPermission(BookingConfirmationActivity.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                                || ContextCompat.checkSelfPermission(BookingConfirmationActivity.this,
                                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(BookingConfirmationActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                                    Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE);
                        } else {
                            wsDownloadTicket(transactionId, data.getBookingId());
                        }

                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                });


            }


            if (Constant.Code.ONE == data.isIs_live_ticket()||Constant.Code.ONE == data.isIs_app_ticket()) {
                ImageView ivActionRing = findViewById(R.id.ivActionRing);
                ivActionRing.setVisibility(View.VISIBLE);
                Utility.blinkBorder(ivActionRing);
                btnDownload.setVisibility(View.GONE);
            }

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void showNoInternet() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_connection_lost)
                    .placeholder(R.drawable.ic_connection_lost_static)
                    .into(ivErrorImage);
            tvErrorMessage.setText(R.string.msg_no_internet);
            llMainLayout.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.VISIBLE);
            tvErrorButton.setText(R.string.btn_try_again);
            tvErrorButton.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    wsGetBookingDetailForCOD(transactionId);
                } else {
                    showNoInternet();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void showError() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_no_result_found)
                    .placeholder(R.drawable.ic_no_result_found)
                    .into(ivErrorImage);
            llMainLayout.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        try {
            Utility.hideKeyboard(this);
            Intent i1 = new Intent(getApplicationContext(), LandingActivity.class);
            i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i1);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    // WS - Get Booking Detail For Online
    private void wsGetBookingDetailForOnline(long txnId) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestGetBookingDetailForOnline request = new RequestGetBookingDetailForOnline(txnId,"");

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackGetBookingDetailForOnline> call = tnAPI.getBookingDetailForOnline(request);
            call.enqueue(new Callback<CallbackGetBookingDetailForOnline>() {
                @Override
                public void onResponse(@NonNull Call<CallbackGetBookingDetailForOnline> call, @NonNull Response<CallbackGetBookingDetailForOnline> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            BookingHistory data = response.body().data();
                            databaseHelper.addBookingHistory(data, mLogin.getUserId());
                            if (ContextCompat.checkSelfPermission(BookingConfirmationActivity.this,
                                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                                    || ContextCompat.checkSelfPermission(BookingConfirmationActivity.this,
                                    android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(BookingConfirmationActivity.this,
                                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                                        Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE_FOR_BGDWNLD);
                            } else {
                                databaseHelper.startDownloadingBookingHistoryInBackground();
                            }
                            invalidateData(data);

                            llMainLayout.setVisibility(View.VISIBLE);
                            llErrorLayout.setVisibility(View.GONE);

                        } else if (Validate.isNotNull(response.body().getError_description())) {
                            tvErrorMessage.setText(response.body().getError_description());
                            showError();

                        } else {
                            tvErrorMessage.setText(R.string.msg_something_wrong);
                            showError();
                        }
                    } else {
                        tvErrorMessage.setText(R.string.msg_no_response);
                        showError();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackGetBookingDetailForOnline> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    tvErrorMessage.setText(R.string.msg_something_wrong);
                    showError();
                }
            });
        }catch (Exception e){
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    // WS - Get Booking Detail For COD
    private void wsGetBookingDetailForCOD(long regId) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestGetBookingDetailForCOD request = new RequestGetBookingDetailForCOD(regId);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackGetBookingDetailForCOD> call = tnAPI.getBookingDetailForCOD(request);
            call.enqueue(new Callback<CallbackGetBookingDetailForCOD>() {
                @Override
                public void onResponse(@NonNull Call<CallbackGetBookingDetailForCOD> call, @NonNull Response<CallbackGetBookingDetailForCOD> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            BookingHistory data = response.body().data();
                            invalidateData(data);

                            llMainLayout.setVisibility(View.VISIBLE);
                            llErrorLayout.setVisibility(View.GONE);

                        } else if (Validate.isNotNull(response.body().getError_description())) {
                            tvErrorMessage.setText(response.body().getError_description());
                            showError();

                        } else {
                            tvErrorMessage.setText(R.string.msg_something_wrong);
                            showError();
                        }
                    } else {
                        tvErrorMessage.setText(R.string.msg_no_response);
                        showError();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackGetBookingDetailForCOD> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    tvErrorMessage.setText(R.string.msg_something_wrong);
                    showError();
                }
            });
        }catch (Exception e){
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }


    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        try {

            switch (requestCode) {
                case Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE:
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        btnDownload.performClick();
                    }
                case Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE_FOR_BGDWNLD:
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //DB.startDownloadingBookingHistoryInBackground();
                        databaseHelper.startDownloadingMyTicketsInBackground();
                    }
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    // WS - DOWNLOAD TICKET
    @RequiresPermission(allOf = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE})
    private void wsDownloadTicket(long transId, String bookingId) {

        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestDownloadBooking request = new RequestDownloadBooking(transId);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackDownloadBooking> call = tnAPI.downloadBooking(request);
            call.enqueue(new Callback<CallbackDownloadBooking>() {
                @Override
                public void onResponse(@NonNull Call<CallbackDownloadBooking> call, @NonNull Response<CallbackDownloadBooking> response) {
                    try {
                        if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                                String imageData = response.body().data().getImageData();
                                File fileDir = new File(Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_PICTURES, "TicketNinja");
                                if (!fileDir.exists()) if (fileDir.mkdirs()) Log.e(TAG, "");
                                File file = FilesUtils.base64toAnyFile(BookingConfirmationActivity.this, imageData, new File(fileDir, bookingId + ".png"));

                                FilesUtils.refreshGallery(BookingConfirmationActivity.this, file);

                                CC.showAlert(R.string.booking_confirmation_download_success);

                                llMainLayout.setVisibility(View.VISIBLE);
                                llErrorLayout.setVisibility(View.GONE);

                            } else if (Validate.isNotNull(response.body().getError_description())) {
                                tvErrorMessage.setText(response.body().getError_description());
                                showError();

                            } else {
                                tvErrorMessage.setText(R.string.msg_something_wrong);
                                showError();
                            }
                        } else {
                            tvErrorMessage.setText(R.string.msg_no_response);
                            showError();
                        }
                    } catch (Exception e){
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(@NonNull Call<CallbackDownloadBooking> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    tvErrorMessage.setText(R.string.msg_something_wrong);
                    showError();
                }
            });
        }catch (Exception e){
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {

            if (requestCode == Constant.ActivityForResult.FROM_MY_TICKETS) {
                try {
                    BookingHistory history = databaseHelper.getBookingHistoryById(transactionId, mLogin.getUserId());
                    Log.e(TAG,"history:" +history);
                    invalidateData(history);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
