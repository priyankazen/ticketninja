/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;

import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import in.ticketninja.R;
import in.ticketninja.adapters.ExpandebleAdapter;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Utility;

public class TermsAndConditionActivity extends AppCompatActivity {

    List<String> listDataHeader;
    HashMap<String, String> listDataChild;
    ExpandableListView expListView;
    ExpandebleAdapter listAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            setContentView(R.layout.activity_terms_and_condition);

            expListView = findViewById(R.id.lvExp);
            expListView.setGroupIndicator(null);
            expListView.setChildIndicator(null);
            expListView.setChildDivider(getResources().getDrawable(R.color.transparent));
            expListView.setGroupIndicator(getResources().getDrawable(R.color.transparent));

            setupActionbar();

            prepareData();

            listAdapter = new ExpandebleAdapter(this, listDataHeader, listDataChild);

            // setting list adapter
            expListView.setAdapter(listAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setupActionbar() {
        try {
            // TYPEFACE
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);

            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                //getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                tvTitle.setTypeface(tfMedium);
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void prepareData() {
        try {
            listDataHeader = new ArrayList<>();
            listDataChild = new HashMap<>();

            // Adding child data
            listDataHeader.add("Acceptance of terms");
            listDataHeader.add("Modification of terms");
            listDataHeader.add("Privacy policy");
            listDataHeader.add("Limited user");
            listDataHeader.add("Prohibition against unlawful use");
            listDataHeader.add("Termination/Access restriction");
            listDataHeader.add("Fees payment");
            listDataHeader.add("User's obligation and user account");
            listDataHeader.add("Breach");
            listDataHeader.add("Proprietary rights");
            listDataHeader.add("Relationship");
            listDataHeader.add("Headings");
            listDataHeader.add("Termination of agreement and services");
            listDataHeader.add("Governing law");
            listDataHeader.add("Purchase Policy");
            listDataHeader.add("Terms and Conditions for Local Deliveries");

            listDataChild.put(listDataHeader.get(0), getResources().getString(R.string.acceptance_terms));
            listDataChild.put(listDataHeader.get(1), getResources().getString(R.string.modificationof_terms));
            listDataChild.put(listDataHeader.get(2), getResources().getString(R.string.privacypolicy));
            listDataChild.put(listDataHeader.get(3), getResources().getString(R.string.limiteduser));
            listDataChild.put(listDataHeader.get(4), getResources().getString(R.string.prohibition_against_unlawful_use));
            listDataChild.put(listDataHeader.get(5), getResources().getString(R.string.termination));
            listDataChild.put(listDataHeader.get(6), getResources().getString(R.string.feespayment));
            listDataChild.put(listDataHeader.get(7), getResources().getString(R.string.user_obligation_and_user_account));
            listDataChild.put(listDataHeader.get(8), getResources().getString(R.string.breach));
            listDataChild.put(listDataHeader.get(9), getResources().getString(R.string.proprietary_rights));
            listDataChild.put(listDataHeader.get(10), getResources().getString(R.string.relationship));
            listDataChild.put(listDataHeader.get(11), getResources().getString(R.string.headings));
            listDataChild.put(listDataHeader.get(12), getResources().getString(R.string.terminationo_of_agreemnet_andservice));
            listDataChild.put(listDataHeader.get(13), getResources().getString(R.string.governing_law));
            listDataChild.put(listDataHeader.get(14), getResources().getString(R.string.purchasepolicy));
            listDataChild.put(listDataHeader.get(15), getResources().getString(R.string.terms_n_condition_localdelivery));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
