/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresPermission;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.BookingCategoryAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.common.FilesUtils;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.database.DatabaseHelper;
import in.ticketninja.objects.BookingHistory;
import in.ticketninja.objects.BookingTicketList;
import in.ticketninja.service.TimerService;
import in.ticketninja.widget.BarCodeDialog;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestDownloadBooking;
import in.ticketninja.ws.request.RequestGetBookingDetailForCOD;
import in.ticketninja.ws.request.RequestGetBookingDetailForOnline;
import in.ticketninja.ws.request.RequestTransNo;
import in.ticketninja.ws.response.CallbackDownloadBooking;
import in.ticketninja.ws.response.CallbackGetBookingDetailForCOD;
import in.ticketninja.ws.response.CallbackGetBookingDetailForOnline;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BookingHistoryDetailActivity extends AppCompatActivity {

    private final String TAG = "BookingHistoryDetail";
    private CommonClass CC;
    private DatabaseHelper DB;
    private SRKLoaderDialog mLoader;
    private String transactionType = "";
    // private long transactionId = 1100;
    @Constant.PaymentWSType
    // private String transactionType = Constant.PaymentType.COD;
    private long transactionId = 0;
    private String transactionNo = "";
    private boolean isPastTicket = true;
    private int ticketLive, appTicket;

    private View llMainLayout;
    private View llErrorLayout;
    private ImageView ivErrorImage;
    private TextView tvErrorMessage, tvErrorButton;

    private ImageView ivEventPoster;
    private ImageView ivBookingBarcode;
    private TextView tvBookingId;
    private TextView tvEventTitle;
    private TextView tvEventType;
    private ImageView ivEventTypeIcon;
    private TextView tvBookingTicketCount;
    private TextView tvBookingVenue;
    private TextView tvEventDateTime;
    private RecyclerView rvBookingCategory;
    private TextView tvBookingLabelPayAmount;
    private TextView tvBookingPayAmount;
    private TextView tvBookingPaymentType;
    private TextView tvBookingPaymentStatus;
    private TextView tvBookingBookingDate;
    private TextView tvBookingDeliveryAddress;
    private TextView tvBookingDeliveryMobile;
    private TextView tvBookingDeliveryTime;
    private View llFooter;
    private View llViewTicket;
    private ImageView btnDownload;

    private View bookingRowAddress, bookingRowAddressDivider;
    private LoginUtils mLogin;
    private LinearLayout llSubTotalDiscount;
    private TextView tvBookingSubTotal;
    private TextView tvBookingDiscount;
    private Uri uri;

    private TextView tvTotalProcessingFee, tvPFee1, tvPFee2, tvLabelPFee2, tvLabelProcessingFee, tvBookingLabelSubTotal, tvBookingLabelDiscount;
    private LinearLayout llProcessingFee, llProcessing, llDis, llSubTtl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_hisotry_detail);

        try {
            initializeData();

            btnDownload = findViewById(R.id.btnDownload);
            View td = findViewById(R.id.td);

            getIntentData();

            setUpActionBar(td);

            stopService(new Intent(BookingHistoryDetailActivity.this, TimerService.class));
            Utility.hideKeyboard(BookingHistoryDetailActivity.this);

            String from_where = getIntent().getStringExtra("From");

            findViewById();

            tvLabelProcessingFee.setOnClickListener(view -> {
                if (llProcessing.getVisibility() == View.VISIBLE) {
                    tvLabelProcessingFee.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_booking_rupees, 0, R.drawable.ic_share_arrow_down, 0);
                    llProcessing.setVisibility(View.GONE);
                } else {
                    tvLabelProcessingFee.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_booking_rupees, 0, R.drawable.ic_share_arrow_up, 0);
                    llProcessing.setVisibility(View.VISIBLE);
                }
            });

            uri = handleIntent();
            Log.e(TAG, "uri=" + uri);

            BookingHistory bookingHistoryData = DB.getBookingHistoryById(transactionId, mLogin.getUserId());
            if (uri != null) {
                bookingHistoryData = DB.getBookingHistoryByTransNo(uri.getLastPathSegment(), mLogin.getUserId());
                Log.e(TAG, "if_bookingHistory = " + bookingHistoryData);
                isPastTicket = false;
                transactionNo = uri.getLastPathSegment();
                Log.e(TAG, "if_transactionNo = " + transactionNo);
                //transactionType = bookingHistoryData.getPaymentType();
                //Log.e(TAG, "if_transactionType = " + transactionType);
            }

            if (!Validate.isNotNull(from_where)) {

                if (bookingHistoryData != null && Validate.isNotNull(bookingHistoryData.getName())) {
                    Log.e(TAG, "if");
                    transactionType = bookingHistoryData.getPaymentType();
                    invalidateData(bookingHistoryData);
                    llMainLayout.setVisibility(View.VISIBLE);
                    llErrorLayout.setVisibility(View.GONE);
                } else {
                    Log.e(TAG, "else_transactionType: " + transactionType);
                    if (CC.isOnline()) {
                        /*if (Validate.isNotNull(transactionType) && transactionType.toLowerCase().equals(Constant.PaymentType.ONLINE.toLowerCase())) {
                            wsGetBookingDetailForOnline(transactionId, "");
                        } else if (uri != null) {
                            wsGetBookingDetailForOnline(0, transactionNo);
                        } else if (Validate.isNotNull(transactionType) && transactionType.toLowerCase().equals(Constant.PaymentType.COD.toLowerCase())) {
                            wsGetBookingDetailForCOD(transactionId);
                        } else {
                            tvErrorMessage.setText(R.string.msg_something_wrong);
                            showError();
                        }*/

                        if (uri != null) {
                            wsGetBookingDetailForOnline(0, transactionNo);
                        }else {
                            wsGetBookingDetailForOnline(transactionId, "");
                        }

                    } else {
                        showNoInternet();
                    }
                }
            }



       /* if (CC.isOnline()) {
            if (Validate.isNotNull(transactionType) && transactionType.equals(Constant.PaymentType.ONLINE)) {
                wsGetBookingDetailForOnline(transactionId);
            } else if (Validate.isNotNull(transactionType) && transactionType.equals(Constant.PaymentType.COD)) {
                wsGetBookingDetailForCOD(transactionId);
            } else {
                tvErrorMessage.setText(R.string.msg_something_wrong);
                showError();
            }
        } else {
            showNoInternet();
        }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void findViewById() {
        try {
            // TYPEFACE
            //Typeface tfRegular = TypefaceUtils.HelveticaRegular(this);
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);

            llMainLayout = findViewById(R.id.llMainLayout);
            llMainLayout.setVisibility(View.GONE);
            llErrorLayout = findViewById(R.id.llErrorLayout);
            ivErrorImage = findViewById(R.id.ivErrorImage);
            tvErrorMessage = findViewById(R.id.tvErrorMessage);
            tvErrorButton = findViewById(R.id.tvErrorButton);

            ivEventPoster = findViewById(R.id.ivEventPoster);
            ivBookingBarcode = findViewById(R.id.ivBookingBarcode);
            tvBookingId = findViewById(R.id.tvBookingId);
            tvBookingId.setTypeface(tfMedium);
            tvEventTitle = findViewById(R.id.tvEventTitle);
            tvEventTitle.setTypeface(tfMedium);
            tvEventType = findViewById(R.id.tvEventType);
            ivEventTypeIcon = findViewById(R.id.ivEventTypeIcon);
            tvBookingTicketCount = findViewById(R.id.tvBookingTicketCount);
            tvBookingVenue = findViewById(R.id.tvBookingVenue);
            tvBookingVenue.setTypeface(tfMedium);
            tvEventDateTime = findViewById(R.id.tvEventDateTime);
            tvEventDateTime.setTypeface(tfMedium);
            tvBookingLabelPayAmount = findViewById(R.id.tvBookingLabelPayAmount);
            llSubTotalDiscount = findViewById(R.id.llSubTotalDiscount);
            tvBookingSubTotal = findViewById(R.id.tvBookingSubTotal);
            tvBookingDiscount = findViewById(R.id.tvBookingDiscount);
            tvBookingPayAmount = findViewById(R.id.tvBookingPayAmount);
            tvBookingPayAmount.setTypeface(tfMedium);
            tvBookingPaymentType = findViewById(R.id.tvBookingPaymentType);
            tvBookingPaymentType.setTypeface(tfMedium);
            tvBookingPaymentStatus = findViewById(R.id.tvBookingPaymentStatus);
            tvBookingPaymentStatus.setTypeface(tfMedium);
            tvBookingBookingDate = findViewById(R.id.tvBookingBookingDate);
            tvBookingBookingDate.setTypeface(tfMedium);
            tvBookingDeliveryAddress = findViewById(R.id.tvBookingDeliveryAddress);
            tvBookingDeliveryAddress.setTypeface(tfMedium);
            tvBookingDeliveryMobile = findViewById(R.id.tvBookingDeliveryMobile);
            tvBookingDeliveryMobile.setTypeface(tfMedium);
            tvBookingDeliveryTime = findViewById(R.id.tvBookingDeliveryTime);
            tvBookingDeliveryTime.setTypeface(tfMedium);
            llFooter = findViewById(R.id.llFooter);
            llViewTicket = findViewById(R.id.llViewTicket);


            bookingRowAddress = findViewById(R.id.bookingRowAddress);
            bookingRowAddressDivider = findViewById(R.id.bookingRowAddressDivider);

            rvBookingCategory = findViewById(R.id.rvBookingCategory);
            rvBookingCategory.setHasFixedSize(true);
            rvBookingCategory.setNestedScrollingEnabled(false);


            tvTotalProcessingFee = findViewById(R.id.tvTotalProcessingFee);
            //tvTotalProcessingFee.setTypeface(tfMedium);
            tvPFee1 = findViewById(R.id.tvPFee1);
            tvPFee2 = findViewById(R.id.tvPFee2);
            tvLabelPFee2 = findViewById(R.id.tvLabelPFee2);
            llProcessingFee = findViewById(R.id.llProcessingFee);
            tvLabelProcessingFee = findViewById(R.id.tvLabelProcessingFee);
            //tvLabelProcessingFee.setTypeface(tfRegular);
            llProcessing = findViewById(R.id.llProcessing);
            llSubTtl = findViewById(R.id.llSubTtl);
            llDis = findViewById(R.id.llDis);
            tvBookingLabelSubTotal = findViewById(R.id.tvBookingLabelSubTotal);
            tvBookingLabelDiscount = findViewById(R.id.tvBookingLabelDiscount);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            CC = new CommonClass(this);
            DB = new DatabaseHelper(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpActionBar(View td) {
        try {
            // TYPEFACE
            //Typeface tfRegular = TypefaceUtils.HelveticaRegular(this);
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(this);

            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                //getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                tvTitle.setTypeface(tfMedium);
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
                findViewById(R.id.ivActionLogo).setVisibility(View.VISIBLE);
                if (Constant.Code.ONE == ticketLive || Constant.Code.ONE == appTicket) {
                    ImageView ivActionRing = findViewById(R.id.ivActionRing);
                    ivActionRing.setVisibility(View.VISIBLE);
                    Utility.blinkBorder(ivActionRing);
                    btnDownload.setVisibility(View.GONE);
                    td.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        try {
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID))
                transactionId = getIntent().getLongExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, 0);
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE))
                transactionType = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE);
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TICKET_LIVE))
                ticketLive = getIntent().getIntExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TICKET_LIVE, 0);
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_TRANSACTION_APP_TICKET))
                appTicket = getIntent().getIntExtra(Constant.ScreenExtras.EVENT_TRANSACTION_APP_TICKET, 0);

            String historyType = Constant.HistoryType.FUTURE;
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_HISTORY_TYPE))
                historyType = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_HISTORY_TYPE);

            if (Validate.isNotNull(historyType) && historyType.equals(Constant.HistoryType.FUTURE)) {
                isPastTicket = false;
            }
            Log.e(TAG, "transactionType=" + transactionType + " , transactionId = " + transactionId);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void invalidateData(BookingHistory data) {
        try {

            if (data == null) {
                return;
            }

            if (Validate.isNotNull(data.getImageUrl())) {

                //File f = new File(Constant.IMAGE_DOWNLOAD_PATH + data.getImageId());
                File f = new File("");

                if (f.exists()) {
                    Glide.with(BookingHistoryDetailActivity.this)
                            .load(f)
                            //.centerCrop()
                            .skipMemoryCache(false)
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(ivEventPoster);

                } else {
                    Glide.with(BookingHistoryDetailActivity.this)
                            .load(data.getImageUrl())
                            //.centerCrop()
                            .skipMemoryCache(false)
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .into(ivEventPoster);
                }

                ivEventPoster.setVisibility(View.VISIBLE);
            } else {
                ivEventPoster.setVisibility(View.GONE);
            }

            //File f = new File(Constant.IMAGE_DOWNLOAD_PATH + data.getQRCode() + ".png");
            File f = new File("");

            if (f.exists()) {
                Glide.with(BookingHistoryDetailActivity.this)
                        .load(f)
                        .centerCrop()
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .skipMemoryCache(false)
                        .into(ivBookingBarcode);
            } else {
                if (Validate.isNotNull(data.getQr_code_url())) {
                    Glide.with(BookingHistoryDetailActivity.this)
                            .load(Constant.generateQRCodeURL1(data.getQr_code_url()))
                            .centerCrop()
                            .skipMemoryCache(false)
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .skipMemoryCache(false)
                            .into(ivBookingBarcode);
                } else {
                    Glide.with(BookingHistoryDetailActivity.this)
                            .load(Constant.generateQRCodeURL(data.getQRCode()))
                            .centerCrop()
                            .skipMemoryCache(false)
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .skipMemoryCache(false)
                            .into(ivBookingBarcode);

                }

            }

            ivBookingBarcode.setOnClickListener(v -> {
                BarCodeDialog dialog = new BarCodeDialog(BookingHistoryDetailActivity.this, R.style.transparent_dialog_borderless);
                dialog.setBookingId(data.getBookingId());
                if (Validate.isNotNull(data.getQr_code_url())) {
                    dialog.setImageUrl(Constant.generateQRCodeURL1(data.getQr_code_url()), data.getQRCode());
                } else {
                    dialog.setImageUrl(Constant.generateQRCodeURL(data.getQRCode()), data.getQRCode());
                }

                dialog.show();
            });

            tvBookingId.setText(Validate.isNotNull(data.getBookingId()) ? data.getBookingId() : "");
            tvEventTitle.setText(Validate.isNotNull(data.getName()) ? String.format("%s", data.getName()) : "");

            tvEventType.setText(Validate.isNotNull(data.getType()) ? String.format("%s", data.getType()) : "");

            if (Validate.isNotNull(data.getTypeIcon())) {
            /*Glide.with(this)
                    .load(RestApi.ICON_RED_IMAGE + data.getTypeIcon())
                    .into(ivEventTypeIcon);*/

                Glide.with(this)
                        .load(data.getTypeIcon())
                        .into(ivEventTypeIcon);
            } else {
                Glide.with(this).load(R.drawable.ic_event_detail_type).into(ivEventTypeIcon);
            }


            try {
                if ((data.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY)
                        || data.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW))
                        && Constant.Code.ONE == data.isSeasonPass()) {
                    tvEventDateTime.setText(String.format("%s, %s \n%s %s", data.getMDDate(), data.getYear(), data.getEvent_time(), Constant.ONWARDS));
                } else if (data.getDayType().equalsIgnoreCase(Constant.EventDayType.PERPETUAL)) {
                    tvEventDateTime.setText(String.format("%s, %s, %s \n%s", data.getDay(), data.getMDDate(), data.getYear(), data.getEvent_time()));
                } else if (Validate.isNotNull(data.getMDDate())) {
                    tvEventDateTime.setText(String.format("%s, %s, %s \n%s %s", data.getDay(), data.getMDDate(), data.getYear(), data.getEvent_time(), Constant.ONWARDS));
                } else if (Validate.isDateNotNull(data.getEvent_date())) {
                    String dateTime = DateTimeUtils.changeDateTimeFormat(data.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, DateTimeUtils.OUTPUT_FORMAT_WITH_DAY_FULL);
                    tvEventDateTime.setText(String.format("%s %s",
                            Validate.isNotNull(dateTime) ? dateTime : String.format("%s", data.getEvent_date()),
                            Validate.isNotNull(data.getEvent_time()) ? String.format("%s", data.getEvent_time()) : ""));
                } else {
                    tvEventDateTime.setText("-");
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            tvBookingVenue.setText(Validate.isNotNull(data.getVenueName()) ? String.format("%s", data.getVenueName()) : "");
            tvBookingVenue.setOnClickListener(v -> CommonClass.openMap(v.getContext(), data.getVenueName(), data.getLatitude(), data.getLongitude()));

            tvBookingTicketCount.setText(String.format(Locale.getDefault(), data.getTotalTicket() > 1 ? "%d Tickets" : "%d Ticket", data.getTotalTicket()));

            if (data.getTicketList() != null) {

                Collections.reverse(data.getTicketList());

                List<BookingTicketList> bookingTicketLists = CommonClass.groupTicketList(data.getTicketList());
                BookingCategoryAdapter mAdapter = new BookingCategoryAdapter(bookingTicketLists);
                rvBookingCategory.setAdapter(mAdapter);
            }


            // data.Ticket_List = DB.getBookingTicketList(null, data.getBookingId());
            Log.e(TAG, "data.getAmount():" + data.getAmount());
            Log.e(TAG, "data.getNetamount():" + data.getNetamount());
            Log.e(TAG, "data.getCouponCode():" + data.getCouponCode());

            Log.e(TAG, "data.getTotal_processingfee()=" + data.getTota_ProcessingFee());
            if (data.getTota_ProcessingFee() > 0) {
                llSubTotalDiscount.setVisibility(View.VISIBLE);
                tvBookingSubTotal.setVisibility(View.GONE);
                tvBookingDiscount.setVisibility(View.GONE);
                tvBookingLabelSubTotal.setVisibility(View.GONE);
                tvBookingLabelDiscount.setVisibility(View.GONE);

                llProcessingFee.setVisibility(View.VISIBLE);
                tvTotalProcessingFee.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getTota_ProcessingFee()));
                tvPFee1.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getProcessingfee()));
                tvPFee2.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getTax()));
                tvLabelPFee2.setText(data.getTax_type());
            } else {
                llProcessingFee.setVisibility(View.GONE);
            }


//
//
//            if (data.getAmount() == 0) {
//                tvBookingPayAmount.setText(R.string.str_price_free);
//            } else {
//                tvBookingPayAmount.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getAmount()));
//            }
            if (Validate.isNotNull(data.getCouponCode())) {
                llSubTotalDiscount.setVisibility(View.VISIBLE);

                llSubTtl.setVisibility(View.VISIBLE);
                tvBookingSubTotal.setVisibility(View.VISIBLE);
                tvBookingDiscount.setVisibility(View.VISIBLE);
                llDis.setVisibility(View.VISIBLE);
                tvBookingLabelSubTotal.setVisibility(View.VISIBLE);
                tvBookingLabelDiscount.setVisibility(View.VISIBLE);

                tvBookingSubTotal.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getAmount()));
                tvBookingDiscount.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getDiscount()));
                if (data.getAmount() == 0) {
                    tvBookingPayAmount.setText(R.string.str_price_free);
                } else {
                    tvBookingPayAmount.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getNetamount()));
                }
            } else {
                if (llProcessingFee.getVisibility() == View.GONE) {
                    llSubTotalDiscount.setVisibility(View.GONE);
                }
                llSubTtl.setVisibility(View.GONE);
                llDis.setVisibility(View.GONE);

                if (data.getAmount() == 0) {
                    tvBookingPayAmount.setText(R.string.str_price_free);
                } else {
                    //tvBookingPayAmount.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getAmount()));
                    tvBookingPayAmount.setText(String.format(Locale.getDefault(), "₹ %.2f", data.getNetamount()));
                }
            }

            tvBookingPaymentType.setText(Validate.isNotNull(Utility.strSmallCap(data.getPaymentType())) ? Utility.strSmallCap(data.getPaymentType()) : "");
            tvBookingPaymentStatus.setText(Validate.isNotNull(data.getPaymentStatus()) ? data.getPaymentStatus() : "");
            tvBookingPaymentStatus.setTextColor(Validate.isNotNull(data.getPaymentStatus()) && data.getPaymentStatus().equalsIgnoreCase("Received")
                    ? Color.parseColor("#75BF7A") : ContextCompat.getColor(this, R.color.themeRed));
            tvBookingBookingDate.setText(Validate.isNotNull(data.getTransDate()) ? data.getTransDate() : "");

            tvBookingDeliveryAddress.setText(Validate.isNotNull(data.getP_d_address()) ? "Delivery Address : " + data.getP_d_address() : "");
            tvBookingDeliveryMobile.setText(Validate.isNotNull(data.getMobileno()) ? "Phone : " + data.getMobileno() : "");
            tvBookingDeliveryTime.setText(Validate.isNotNull(data.getDeliveryTimeSlot()) ? "event_time : " + data.getDeliveryTimeSlot() : "");

            Log.e(TAG, "transactionType=" + transactionType);
       /* if (Validate.isNotNull(transactionType) && (transactionType.toLowerCase().equals(Constant.PaymentType.ONLINE.toLowerCase()) || transactionType.equals(Constant.PaymentType.COMPLEMENTARY))) {
            llFooter.setVisibility(View.VISIBLE);

            bookingRowAddress.setVisibility(View.GONE);
            bookingRowAddressDivider.setVisibility(View.GONE);

            tvBookingLabelPayAmount.setText(R.string.booking_confirmation_amount_paid);

            llViewTicket.setOnClickListener(v -> {
                Intent intViewTicket = new Intent(getApplicationContext(), MTicketsActivity.class);
                intViewTicket.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, data.getTrans_id());
                intViewTicket.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE, data.getPaymentType());
                intViewTicket.putExtra(Constant.ScreenExtras.BOOKING_DATA, data);
                startActivityForResult(intViewTicket, Constant.ActivityForResult.FROM_MY_TICKETS);
                //finish();
            });

            btnDownload.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    if (ContextCompat.checkSelfPermission(BookingHistoryDetailActivity.this,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                            || ContextCompat.checkSelfPermission(BookingHistoryDetailActivity.this,
                            Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(BookingHistoryDetailActivity.this,
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                                Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE);
                    } else {
                        wsDownloadTicket(transactionId, data.getBookingId());
                    }

                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });

        } else {
            llFooter.setVisibility(View.GONE);

            bookingRowAddress.setVisibility(View.VISIBLE);
            bookingRowAddressDivider.setVisibility(View.VISIBLE);

            tvBookingLabelPayAmount.setText(R.string.booking_confirmation_amount_payable);
        }*/

            if (Validate.isNotNull(transactionType) && (transactionType.toLowerCase().equals(Constant.PaymentType.COD.toLowerCase()))) {
                llFooter.setVisibility(View.GONE);
                bookingRowAddress.setVisibility(View.VISIBLE);
                bookingRowAddressDivider.setVisibility(View.VISIBLE);
                tvBookingLabelPayAmount.setText(R.string.booking_confirmation_amount_payable);

            } else {
                llFooter.setVisibility(View.VISIBLE);

                bookingRowAddress.setVisibility(View.GONE);
                bookingRowAddressDivider.setVisibility(View.GONE);

                tvBookingLabelPayAmount.setText(R.string.booking_confirmation_amount_paid);

                llViewTicket.setOnClickListener(v -> {
                    Intent intViewTicket = new Intent(getApplicationContext(), MTicketsActivity.class);
                    intViewTicket.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_ID, data.getTrans_id());
                    intViewTicket.putExtra(Constant.ScreenExtras.EVENT_TRANSACTION_TYPE, data.getPaymentType());
                    intViewTicket.putExtra(Constant.ScreenExtras.BOOKING_DATA, data);
                    startActivityForResult(intViewTicket, Constant.ActivityForResult.FROM_MY_TICKETS);
                    //finish();
                });

                btnDownload.setOnClickListener(v -> {
                    if (CC.isOnline()) {
                        if (ContextCompat.checkSelfPermission(BookingHistoryDetailActivity.this,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                                || ContextCompat.checkSelfPermission(BookingHistoryDetailActivity.this,
                                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(BookingHistoryDetailActivity.this,
                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE},
                                    Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE);
                        } else {
                            wsDownloadTicket(transactionId, data.getBookingId());
                        }

                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                });
            }

            if (uri != null) {
                llFooter.setVisibility(View.VISIBLE);
                bookingRowAddress.setVisibility(View.GONE);
                bookingRowAddressDivider.setVisibility(View.GONE);
            }

            if (isPastTicket) llFooter.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showNoInternet() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_connection_lost)
                    .placeholder(R.drawable.ic_connection_lost_static)
                    .into(ivErrorImage);
            tvErrorMessage.setText(R.string.msg_no_internet);
            llMainLayout.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.VISIBLE);
            tvErrorButton.setText(R.string.btn_try_again);
            tvErrorButton.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    wsGetBookingDetailForCOD(transactionId);
                } else {
                    showNoInternet();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void showError() {
        try {
            Glide.with(this)
                    .load(R.drawable.ic_no_result_found)
                    .placeholder(R.drawable.ic_no_result_found)
                    .into(ivErrorImage);
            llMainLayout.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // WS - Get Booking Detail For Online
    private void wsGetBookingDetailForOnline(long txnId, String tansNo) {
        Log.e(TAG, "call api");
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestGetBookingDetailForOnline request = new RequestGetBookingDetailForOnline(txnId, tansNo);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackGetBookingDetailForOnline> call = tnAPI.getBookingDetailForOnline(request);
            call.enqueue(new Callback<CallbackGetBookingDetailForOnline>() {
                @Override
                public void onResponse(@NonNull Call<CallbackGetBookingDetailForOnline> call, @NonNull Response<CallbackGetBookingDetailForOnline> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        //  if (response.body().data().isSuccess()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            BookingHistory data = response.body().data();
                            DB.addBookingHistory(data, mLogin.getUserId());
                            invalidateData(data);
                            llMainLayout.setVisibility(View.VISIBLE);
                            llErrorLayout.setVisibility(View.GONE);

                        } else if (Validate.isNotNull(response.body().data().message())) {
                            tvErrorMessage.setText(response.body().data().message());
                            showError();

                        } else {
                            tvErrorMessage.setText(R.string.msg_something_wrong);
                            showError();
                        }
                    } else {
                        tvErrorMessage.setText(R.string.msg_no_response);
                        showError();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackGetBookingDetailForOnline> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    tvErrorMessage.setText(R.string.msg_something_wrong);
                    showError();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
        }

    }

    // WS - Get Booking Detail For COD
    private void wsGetBookingDetailForCOD(long regId) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestGetBookingDetailForCOD request = new RequestGetBookingDetailForCOD(regId);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackGetBookingDetailForCOD> call = tnAPI.getBookingDetailForCOD(request);
            call.enqueue(new Callback<CallbackGetBookingDetailForCOD>() {
                @SuppressWarnings("ConstantConditions")
                @Override
                public void onResponse(@NonNull Call<CallbackGetBookingDetailForCOD> call, @NonNull Response<CallbackGetBookingDetailForCOD> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (response.body().data().isSuccess()) {
                            BookingHistory data = response.body().data();
                            invalidateData(data);

                            llMainLayout.setVisibility(View.VISIBLE);
                            llErrorLayout.setVisibility(View.GONE);

                        } else if (Validate.isNotNull(response.body().data().message())) {
                            tvErrorMessage.setText(response.body().data().message());
                            showError();

                        } else {
                            tvErrorMessage.setText(R.string.msg_something_wrong);
                            showError();
                        }
                    } else {
                        tvErrorMessage.setText(R.string.msg_no_response);
                        showError();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackGetBookingDetailForCOD> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    tvErrorMessage.setText(R.string.msg_something_wrong);
                    showError();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
        }

    }

    // WS - DOWNLOAD TICKET
    @RequiresPermission(allOf = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE})
    private void wsDownloadTicket(long transId, String bookingId) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestDownloadBooking request = new RequestDownloadBooking(transId);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackDownloadBooking> call = tnAPI.downloadBooking(request);
            call.enqueue(new Callback<CallbackDownloadBooking>() {
                @Override
                public void onResponse(@NonNull Call<CallbackDownloadBooking> call, @NonNull Response<CallbackDownloadBooking> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        //if (response.body().data().isSuccess()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            String imageData = response.body().data().getImageData();
                            File fileDir = new File(Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_PICTURES, "TicketNinja");
                            if (!fileDir.exists()) if (fileDir.mkdirs()) Log.e(TAG, "");
                            File file = FilesUtils.base64toAnyFile(BookingHistoryDetailActivity.this, imageData, new File(fileDir, bookingId + ".png"));

                            FilesUtils.refreshGallery(BookingHistoryDetailActivity.this, file);

                            CC.showAlert(R.string.booking_confirmation_download_success);

                            llMainLayout.setVisibility(View.VISIBLE);
                            llErrorLayout.setVisibility(View.GONE);

                        } else if (Validate.isNotNull(response.body().data().message())) {
                            tvErrorMessage.setText(response.body().data().message());
                            //showError();
                            CC.showAlert(R.string.msg_something_wrong);

                        } else {
                            tvErrorMessage.setText(R.string.msg_something_wrong);
                            //showError();
                            CC.showAlert(R.string.msg_something_wrong);
                        }
                    } else {
                        tvErrorMessage.setText(R.string.msg_no_response);
                        //showError();
                        CC.showAlert(R.string.msg_something_wrong);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackDownloadBooking> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showAlert(R.string.msg_something_wrong);
                    //tvErrorMessage.setText(R.string.msg_something_wrong);
                    //showError();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        try {
            if (requestCode == Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE) {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    btnDownload.performClick();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == Constant.ActivityForResult.FROM_MY_TICKETS) {

                Log.e(TAG, "appLinkAction =" + uri);
                if (uri != null) {
                    wsGetBookingDetailForOnline(0, uri.getLastPathSegment());
                    // history = DB.getBookingHistoryByTransNo(uri.getLastPathSegment(), mLogin.getUserId());
                } else {
                    BookingHistory history = DB.getBookingHistoryById(transactionId, mLogin.getUserId());
                    Log.e(TAG, "history =" + history);
                    invalidateData(history);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {

        try {
            if (uri != null) {
                Intent intent = new Intent(this, LandingActivity.class);
                intent.putExtra(Constant.ScreenExtras.DEEP_LINK, true);
                startActivity(intent);
                finish();
                return;
            }

            Utility.hideKeyboard(this);
            super.onBackPressed();
        /*Intent i1 = new Intent(getApplicationContext(), YouTubeActivity.class);
        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i1);*/
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // WS - Get Booking Detail For Online
    private void wsGetTransId(String transNo) {
        try {
            Log.e(TAG, "transNo: " + transNo);
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestTransNo request = new RequestTransNo();
            request.trans_no = transNo;

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<ResponseBody> call = tnAPI.getTransId(request);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (response.body() != null) {
                            try {
                                //{"Get_TransId_byTrans_No":{"error_code":1,"error_description":"success","ServiceName":"Get_TransId_byTrans_No","Data":"8089"}}
                                //{"data":{"trans_id":70569},"error_code":"1","error_description":"Sucess"}
                                String myData = response.body().string();
                                Log.e(TAG, "data =" + myData);
                                try {
                                    JSONObject jo = new JSONObject(myData);
                                    //String errorMessage = jo.getString("error_description");
                                    if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(jo.getString("error_code")))) {
                                        JSONObject joData = jo.getJSONObject("data");
                                        String transId = joData.getString("trans_id");
                                        Log.e(TAG, "transId =" + transId);
                                        transactionId = Long.valueOf(transId);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                            }

                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
        }

    }

    private Uri handleIntent() {
        try {
            // ATTENTION: This was auto-generated to handle app links.
            Intent appLinkIntent = getIntent();
            String appLinkAction = appLinkIntent.getAction();
            Log.e(TAG, "appLinkAction =" + appLinkAction);
            Uri appLinkData = appLinkIntent.getData();
            Log.e(TAG, "appLinkData =" + appLinkData);
            Log.e(TAG, "appLinkScheme =" + appLinkIntent.getScheme());

            if (Intent.ACTION_VIEW.equals(appLinkAction) && appLinkData != null) {
                Log.e(TAG, "appLinkPath =" + appLinkData.getPath());
                String recipeId = appLinkData.getLastPathSegment();
                Log.e(TAG, "recipeId =" + recipeId);
                wsGetTransId(recipeId);
            } else {
                if (getIntent().hasExtra(Constant.ScreenExtras.DEEP_LINK_URI)) {
                    String url = getIntent().getStringExtra(Constant.ScreenExtras.DEEP_LINK_URI);
                    Log.e(TAG, "url=" + url);
                    appLinkData = Uri.parse(url);
                    String recipeId = appLinkData.getLastPathSegment();
                    Log.e(TAG, "else_recipeId =" + recipeId);
                    wsGetTransId(recipeId);
                }
            }

            return appLinkData;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    protected void onDestroy() {
        try {
            dismissProgressDialog();
            super.onDestroy();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void dismissProgressDialog() {
        try{
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
