package in.ticketninja.activity;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;

import java.util.Collection;
import java.util.HashSet;

public class AvailableDaysDecorator implements DayViewDecorator {

    private final int color;
    private final HashSet<CalendarDay> dates;

    AvailableDaysDecorator(int color, Collection<CalendarDay> dates) {
        this.color = color;
        this.dates = new HashSet<>(dates);
    }

    @Override
    public boolean shouldDecorate(CalendarDay day) {
        return dates.contains(day); //decorate only available days
    }

    @Override
    public void decorate(DayViewFacade view) {
        view.setDaysDisabled(true); ///important to enable day
    }
}