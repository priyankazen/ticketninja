/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.activity;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mukesh.countrypicker.CountryPicker;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.adapters.SelectTicketAdapter1;
import in.ticketninja.adapters.SpinnerTimeSlot;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.EventPriceCategory;
import in.ticketninja.objects.EventTimeSlot;
import in.ticketninja.widget.MyDatePickerDialog;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestAttraction;
import in.ticketninja.ws.request.RequestInsertTicketSession;
import in.ticketninja.ws.response.CallbackGetAttraction;
import in.ticketninja.ws.response.CallbackInsertTicketSession;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectTicketActivity extends AppCompatActivity {

    private final String TAG = SelectTicketActivity.class.getSimpleName();
    private CommonClass CC;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;

    private long eventId;
    private String eventName = "";
    private int multiCategorySelect = 0;

    private Spinner sp_time_slot;
    private TextView tv_titleDate, tv_country, tvTotalPrice, tvDiscount;
    private RelativeLayout rvDiscount;
    private String country_name, selected_date, selectedTimeSlot, country_code;
    private ImageView iv_country;
    private RecyclerView rv_ticketList;
    private SelectTicketAdapter1 mPricingAdapter;
    private LinearLayout llBuy;

    private ArrayList<EventPriceCategory> categoryLists = new ArrayList<>();
    private ArrayList<EventTimeSlot> TimeSlotList = new ArrayList<>();

    private MyDatePickerDialog datePickerDialog;

    private String prev_time;
    private Button btnBuy;
    private String extraDiscountLabel = "";
    private double extraDiscountPer = 0;
    private int per_trans_limit = 0, advance_days = 0, hours = 0;
    private PreferencesUtils mPref;
    ArrayList<String> exceptionDate = new ArrayList<>();
    private int img_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_ticket_activity);
        Log.e(TAG, "is calledddd..............");

        try {
            initializeData();

            getIntentData();

            setupActionBar();

            findViewById();


            mPricingAdapter = new SelectTicketAdapter1(this, categoryLists, per_trans_limit, multiCategorySelect);
            mPricingAdapter.setOnItemClickListener(position -> setBuyNowStatus());
            rv_ticketList.setAdapter(mPricingAdapter);
            setBuyNowStatus();

            if (Validate.isNotNull(extraDiscountLabel)) {
                rvDiscount.setVisibility(View.VISIBLE);
                tvDiscount.setText(String.format(Locale.getDefault(), "%s %d%% off", extraDiscountLabel, extraDiscountPer));
            } else {
                rvDiscount.setVisibility(View.GONE);
            }

            SpinnerTimeSlot spinnerTimeSlot = new SpinnerTimeSlot(SelectTicketActivity.this, TimeSlotList, 1);
            sp_time_slot.setAdapter(spinnerTimeSlot);
            sp_time_slot.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    selectSpinnerItemByValue(sp_time_slot, prev_time, 1, position);
                    prev_time = TimeSlotList.get(position).timeSlot();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            selectSpinnerItemByValue(sp_time_slot, selectedTimeSlot, 0, 0);

            String showSelectedDate = DateTimeUtils.changeDateTimeFormat(selected_date, DateTimeUtils.SERVER_FORMAT_DATE, DateTimeUtils.DEMO);
            tv_titleDate.setText(showSelectedDate);

            btnBuy.setOnClickListener(v -> {
                try {
                    if (mPricingAdapter.getSelectedList().size() > 0) {
                        if (sp_time_slot.getSelectedItemPosition() > 0) {

                    /*for (int i = 0; i <mPricingAdapter.getSelectedList().size() ; i++) {
                        EventPriceCategory category = mPricingAdapter.getSelectedList().get(i);
                        if (category.isIdentification()){
                            Intent intent = new Intent(getApplicationContext(), ExpandableActivity.class);
                            intent.putExtra(Constant.ScreenExtras.EVENT_ID, eventId);
                            intent.putExtra(Constant.ScreenExtras.PLAN_CATEGORY_ID, category.getCategory_Id());
                            intent.putExtra(Constant.ScreenExtras.SEAT_COUNT, category.noOfTicket());
                            intent.putExtra(Constant.ScreenExtras.EVENT_DATE, category.eventDate());
                            intent.putExtra(Constant.ScreenExtras.EVENT_TIME, category.time());
                            intent.putExtra(Constant.ScreenExtras.EVENT_DATA, category);
                            startActivity(intent);
                        }
                    }*/

                            wsInsertTicketSession(eventId, selected_date, selectedTimeSlot, country_code, mLogin.getUserId(), mPricingAdapter.getSelectedList());
                        } else {
                            CC.showToast(R.string.event_detail_msg_select_time_slot);
                        }
                    } else {
                        CC.showToast(R.string.event_detail_msg_select_ticket);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });

            tv_country.setOnClickListener(v -> {
                try {
                    CountryPicker picker = CountryPicker.newInstance("Select country");  // dialog title
                    picker.setListener((name, code, dialCode, flagDrawableResID) -> {

                        tv_country.setText(name);
                        iv_country.setImageResource(flagDrawableResID);

                        country_name = name;

                        country_code = code;

                        wsGetAttractionTimeSlotCategory(eventId, code, selected_date);

                        Utility.hideKeyboard(SelectTicketActivity.this, picker.getView());
                        picker.dismiss();


                        //  tv_country.setCompoundDrawables(flagDrawableResID,null,null,null);
                    });
                    picker.onCancel(new DialogInterface() {
                        @Override
                        public void cancel() {
                        }

                        @Override
                        public void dismiss() {
                            Utility.hideKeyboard(SelectTicketActivity.this, tv_country);
                        }
                    });
                    picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                } catch (Exception e) {
                    e.printStackTrace();
                }

            });

            tv_titleDate.setOnClickListener(v -> datePickerDialog.show());

            setDateTimeField();

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void findViewById() {
        try {
            TextView tvEventTitle = findViewById(R.id.tvEventTitle);
            tvEventTitle.setText(Validate.isNotNull(eventName) ? eventName : "");

            tv_titleDate = findViewById(R.id.tv_titledate);
            sp_time_slot = findViewById(R.id.sp_time_slot);
            iv_country = findViewById(R.id.iv_country);
            tv_country = findViewById(R.id.tv_country);
            btnBuy = findViewById(R.id.btnBuy);
            llBuy = findViewById(R.id.llBuy);
            tvTotalPrice = findViewById(R.id.tvTotalPrice);
            tvDiscount = findViewById(R.id.tv_discount);
            rvDiscount = findViewById(R.id.rv_discount);
            iv_country.setImageResource(img_id);
            tv_country.setText(country_name);
            rv_ticketList = findViewById(R.id.rv_ticketlist);
            rv_ticketList.setHasFixedSize(true);
            rv_ticketList.setNestedScrollingEnabled(false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            mPref = new PreferencesUtils(this);
            CC = new CommonClass(this);
            mLogin = new LoginUtils(this);
            mLoader = new SRKLoaderDialog(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionBar() {
        try {
            // ACTIONBAR
            Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                getSupportActionBar().setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);

                ImageView ivActionBack = toolbar.findViewById(R.id.ivActionBack);
                ivActionBack.setOnClickListener(v -> onBackPressed());
                TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
                tvTitle.setText(getTitle());
                Utility.convertToLowerCase(tvTitle);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getIntentData() {
        try {
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_ID))
                eventId = getIntent().getLongExtra(Constant.ScreenExtras.EVENT_ID, 0);
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_NAME))
                eventName = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_NAME);
            if (getIntent().hasExtra(Constant.SelectTicket.CATEGORYLIST))
                categoryLists = (ArrayList<EventPriceCategory>) getIntent().getSerializableExtra(Constant.SelectTicket.CATEGORYLIST);
            if (getIntent().hasExtra(Constant.SelectTicket.TIME_LIST))
                TimeSlotList = (ArrayList<EventTimeSlot>) getIntent().getSerializableExtra(Constant.SelectTicket.TIME_LIST);
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_DISCOUNT_LABEL))
                extraDiscountLabel = getIntent().getStringExtra(Constant.ScreenExtras.EVENT_DISCOUNT_LABEL);
            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_DISCOUNT_PER))
                extraDiscountPer = getIntent().getDoubleExtra(Constant.ScreenExtras.EVENT_DISCOUNT_PER, 0);

            if (getIntent().hasExtra(Constant.ScreenExtras.MULTI_CATEGORY_SELECT))
                multiCategorySelect = getIntent().getIntExtra(Constant.ScreenExtras.MULTI_CATEGORY_SELECT, 0);

            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_TOTAL_TICKET)) {
                per_trans_limit = getIntent().getIntExtra(Constant.ScreenExtras.EVENT_TOTAL_TICKET, 0);
                Log.e(TAG, "per_trans_limit = " + per_trans_limit);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.EVENT_ADVANCE_DAYS)) {
                advance_days = getIntent().getIntExtra(Constant.ScreenExtras.EVENT_ADVANCE_DAYS, 0);
                Log.e(TAG, "advance_days = " + advance_days);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.HOURS)) {
                hours = getIntent().getIntExtra(Constant.ScreenExtras.HOURS, 0);
                Log.e(TAG, "advance_days = " + hours);
            }

            if (getIntent().hasExtra(Constant.ScreenExtras.EXCEPTION_DATES)) {
                exceptionDate = getIntent().getStringArrayListExtra(Constant.ScreenExtras.EXCEPTION_DATES);
                Log.e(TAG, "exceptionDate = " + exceptionDate);
            }


            country_name = getIntent().getStringExtra(Constant.SelectTicket.COUNTRYNAME);
            selected_date = getIntent().getStringExtra(Constant.SelectTicket.SELECTED_DATE);
            img_id = getIntent().getIntExtra(Constant.SelectTicket.IMAGE_ID, 0);
            selectedTimeSlot = getIntent().getStringExtra(Constant.SelectTicket.TIME);
            prev_time = getIntent().getStringExtra(Constant.SelectTicket.TIME);
            country_code = getIntent().getStringExtra(Constant.SelectTicket.COUNTRY_CODE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (resultCode == RESULT_OK && requestCode == Constant.REQUEST_CODE_CONFIRM_CONTACT_DETAIL) {
//            if (data != null && data.hasExtra(Constant.ScreenExtras.EVENT_SESSION_ID)) {
//                mPref = new PreferencesUtils(this);
//                if (Validate.isNotNull(mPref.getUserEmailId()) && Validate.isNotNull(mPref.getUserPhoneNumber()) &&
//                        Validate.isNotNull(mPref.getCountryCode())) {
//                    OrderSummaryActivity.start(SelectTicketActivity.this, data.getStringExtra(Constant.ScreenExtras.EVENT_SESSION_ID));
//                }
//            }
//        }
//    }


    @Override
    protected void onPostResume() {
        super.onPostResume();
        try {

            for (int i = 0; i < categoryLists.size(); i++) {
                categoryLists.get(i).noOfTicket(0);
            }
            mPricingAdapter.notifyDataSetChanged();
            setBuyNowStatus();
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    @Override
    protected void onStop() {
        super.onStop();
        try {
            Log.e(TAG, "onStop");
            mPricingAdapter.updateCount(per_trans_limit);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // WS - Insert Ticket Session
    private void wsInsertTicketSession(long eventId, String eventDate, String timeSlot, String currency, String userId, List<EventPriceCategory> categoryList) {
        try {

            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestInsertTicketSession request = new RequestInsertTicketSession(eventId, eventDate, timeSlot, currency, String.valueOf(userId), categoryList);

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackInsertTicketSession> call = tnAPI.insertTicketSession(request);
            call.enqueue(new Callback<CallbackInsertTicketSession>() {
                @Override
                public void onResponse(@NonNull Call<CallbackInsertTicketSession> call, @NonNull Response<CallbackInsertTicketSession> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                            String sessionID = response.body().data().sessionID();

                            if (Validate.isNull(mPref.getUserEmailId()) || Validate.isNull(mPref.getUserPhoneNumber()) ||
                                    Validate.isNull(mPref.getCountryCode())) {
                                // TODO: 29/3/18 SHREYA 1 : Contact Detail Flow
//                            startActivity(new Intent(SelectTicketActivity.this, ContactConfirmationActivity.class)
//                                            .putExtra(Constant.ScreenExtras.EVENT_SESSION_ID, sessionID));
//                            );

                                ContactConfirmationActivity.Companion.start(SelectTicketActivity.this, sessionID, Constant.ContactActions.EDIT, "", "");

                            } else {
                                OrderSummaryActivity.start(SelectTicketActivity.this, sessionID, "", "");
                            }

                        } else if (Validate.isNotNull(response.body().data().message())) {
                            CC.showAlert(response.body().data().message());
                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_no_response);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackInsertTicketSession> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    // WS - GET ATTRACTION CATEGORY & TIME SLOT
    private void wsGetAttractionTimeSlotCategory(long eventId, String currency, String selectedDate) {
        try {
            if (!mLoader.isShowing() && !isFinishing()) mLoader.show();

            RequestAttraction request = new RequestAttraction();

            request.event_id = eventId;
            request.country_code = currency;
            request.select_date = selectedDate;
            request.engine_type = Constant.ENGINE_TYPE;

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackGetAttraction> call = tnAPI.getAttraction(request);
            call.enqueue(new Callback<CallbackGetAttraction>() {
                @SuppressWarnings("ConstantConditions")
                @Override
                public void onResponse(@NonNull Call<CallbackGetAttraction> call, @NonNull Response<CallbackGetAttraction> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (response.body().data().isSuccess()) {
                                categoryLists = response.body().data().category_list;
                                if (response.body().data().time_slot_list.size() > 0 && categoryLists.size() > 0) {
                                    TimeSlotList = new ArrayList<>();
                                    TimeSlotList.add(0, new EventTimeSlot(getResources().getString(R.string.select_time_slot), 0));
                                    TimeSlotList.addAll(response.body().data().time_slot_list);

                                    SpinnerTimeSlot spinnerTimeSlot = new SpinnerTimeSlot(SelectTicketActivity.this, TimeSlotList, 1);
                                    sp_time_slot.setAdapter(spinnerTimeSlot);

                                    selectSpinnerItemByValue(sp_time_slot, selectedTimeSlot, 0, 0);

                                    mPricingAdapter = new SelectTicketAdapter1(SelectTicketActivity.this, categoryLists, per_trans_limit, multiCategorySelect);
                                    mPricingAdapter.setOnItemClickListener(position -> setBuyNowStatus());
                                    rv_ticketList.setAdapter(mPricingAdapter);
                                    mPricingAdapter.notifyDataSetChanged();
                                    setBuyNowStatus();

                                    Log.e(TAG, "selected_date : " + selected_date);
                                    Log.e(TAG, "selectedDate : " + selectedDate);

                                    selected_date = selectedDate;
                                    tv_titleDate.setText(DateTimeUtils.changeDateTimeFormat(selectedDate, DateTimeUtils.SERVER_FORMAT_DATE, DateTimeUtils.DEMO));
                                } else {
                                    selected_date = getIntent().getStringExtra(Constant.SelectTicket.SELECTED_DATE);
                                    Log.e(TAG, "selected_date : " + selected_date);
                                    Log.e(TAG, "selectedDate : " + selectedDate);

                                    tv_titleDate.setText(DateTimeUtils.changeDateTimeFormat(selected_date, DateTimeUtils.SERVER_FORMAT_DATE, DateTimeUtils.DEMO));
                                    // TODO: 4/4/18 SHREYA  check for data and change
                                    MessageUtils.showAlert(SelectTicketActivity.this, R.string.event_detail_msg_no_time_slot_found);
                                }

                            } else if (Validate.isNotNull(response.body().data().message())) {
                                CC.showToast(response.body().data().message());

                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        } else {
                            CC.showToast(R.string.msg_no_response);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        CC.showToast(R.string.msg_something_wrong);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackGetAttraction> call, @NonNull Throwable t) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                    t.printStackTrace();
                }
            });
        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    private void setBuyNowStatus() {
        try {
            String seatType = "";
            int totalTicketCount = 0;
            if (mPricingAdapter != null && mPricingAdapter.getSelectedList().size() > 0) {
                double totalAmount = 0.0;

                for (EventPriceCategory category : mPricingAdapter.getSelectedList()) {
                    totalTicketCount = totalTicketCount + category.noOfTicket();
                    Log.e(TAG, "totalTicketCount: " + totalTicketCount);
                    totalAmount += category.amount();
                    seatType = category.seatType();
                }

                if (totalAmount > 0) {
                    tvTotalPrice.setVisibility(View.VISIBLE);
                    //tvTotalPrice.setText(getString(R.string.total).concat("    ₹ ").concat("" + totalAmount));

                    String s1 = "₹ ".concat(String.format("%.2f", totalAmount));
                    SpannableString ss1 = new SpannableString(s1);
                    ss1.setSpan(new RelativeSizeSpan(1f), 0, s1.length(), 0);

                    String s2 = totalTicketCount + " Tickets";// / set size
                    SpannableString ss2 = new SpannableString(s2);
                    ss2.setSpan(new RelativeSizeSpan(0.7f), 0, s2.length(), 0);

                    CharSequence finalText = TextUtils.concat(ss1, "\n", ss2);// / set size
                    tvTotalPrice.setText(finalText);

                    llBuy.setEnabled(true);
                    btnBuy.setGravity(Gravity.END | Gravity.CENTER);
                    btnBuy.setEnabled(true);

                } else {
                    //old code
                   /* tvTotalPrice.setText("");
                    tvTotalPrice.setVisibility(View.GONE);
                    llBuy.setEnabled(false);
                    btnBuy.setGravity(Gravity.CENTER);
                    btnBuy.setEnabled(false);*/

                    //my code change 14/10/2020
                    if (Constant.SEAT_TYPE_FREE_SEATING.equals(seatType) || Constant.EVENT_TYPE_ENTRANCE.equals(seatType)) { // change 2/8/2021
                        llBuy.setEnabled(true);
                        btnBuy.setGravity(Gravity.CENTER);
                        btnBuy.setEnabled(true);
                    } else {
                        tvTotalPrice.setText("");
                        tvTotalPrice.setVisibility(View.GONE);
                        llBuy.setEnabled(false);
                        btnBuy.setGravity(Gravity.CENTER);
                        btnBuy.setEnabled(false);
                    }
                }
            } else {
                tvTotalPrice.setText("");
                tvTotalPrice.setVisibility(View.GONE);
                llBuy.setEnabled(false);
                btnBuy.setEnabled(false);
                btnBuy.setGravity(Gravity.CENTER);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setDateTimeField() {
        try {
            final Calendar newCalendar = Calendar.getInstance();
            newCalendar.set(Calendar.HOUR_OF_DAY, 0);
            newCalendar.set(Calendar.MINUTE, 0);
            newCalendar.set(Calendar.SECOND, 5);

            String dobDefault = tv_titleDate.getText().toString().trim();
            if (Validate.isDateNotNull(dobDefault)) {
                Date date = DateTimeUtils.stringToDate(dobDefault, DateTimeUtils.DEMO);

                if (date != null) {
                    newCalendar.setTime(date);
                    newCalendar.set(Calendar.HOUR_OF_DAY, 0);
                    newCalendar.set(Calendar.MINUTE, 0);
                    newCalendar.set(Calendar.SECOND, 5);
                }
            }

            datePickerDialog = new MyDatePickerDialog(SelectTicketActivity.this, R.style.datepicker,
                    (view, year, monthOfYear, dayOfMonth) -> {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year, monthOfYear, dayOfMonth);
                        //  String selecteddate = year+"-"+monthOfYear+1+"-"+dayOfMonth;
                        String selecteddate = String.format(Locale.getDefault(), "%d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
                        selected_date = selecteddate;

                        wsGetAttractionTimeSlotCategory(eventId, country_code, selecteddate);

                    }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
            Calendar calMax = Calendar.getInstance();
            calMax.add(Calendar.DATE, advance_days);
            calMax.set(Calendar.HOUR_OF_DAY, 0);
            calMax.set(Calendar.MINUTE, 0);
            calMax.set(Calendar.SECOND, 0);
            datePickerDialog.getDatePicker().setMaxDate(calMax.getTimeInMillis());

            Calendar calMin = Calendar.getInstance();
            calMin.set(Calendar.HOUR_OF_DAY, 0);
            calMin.set(Calendar.MINUTE, 0);
            calMin.set(Calendar.SECOND, 0);

            //DatePickerDialog.getDatePicker().mode(false);
            if (Build.VERSION_CODES.JELLY_BEAN_MR2 <= Build.VERSION.SDK_INT) {
                datePickerDialog.getDatePicker().setLayoutMode(1);
            }
            datePickerDialog.getDatePicker().setCalendarViewShown(false);

            //DatePickerDialog.getDatePicker().setMinDate(calMin.getTimeInMillis());
            datePickerDialog.getDatePicker().setMinDate(DateTimeUtils.getMinDate(hours));
            //datePickerDialog.setTitle("Select your event_date");
            datePickerDialog.setTitle(getResources().getString(R.string.select_date));
            datePickerDialog.setDisableDate(exceptionDate);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void selectSpinnerItemByValue(Spinner spinner, String value, int fromSelection, int pos) {
        try {
            if (fromSelection == 1) {
                spinner.setSelection(pos);
                selectedTimeSlot = TimeSlotList.get(pos).timeSlot();
            } else {
                for (int position = 0; position < TimeSlotList.size(); position++) {
                    if (TimeSlotList.get(position).timeSlot().equalsIgnoreCase(value)) {
                        spinner.setSelection(position);
                        selectedTimeSlot = TimeSlotList.get(position).timeSlot();
                        break;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
