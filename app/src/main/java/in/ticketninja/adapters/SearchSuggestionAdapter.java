/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.objects.MyEvent;

/**
 * TicketNinja_Working(in.ticketninja.objects) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 9/6/17.
 *
 * @author Suthar Rohit
 */
public class SearchSuggestionAdapter extends RecyclerView.Adapter {

    private List<MyEvent> eventList;
    private OnItemClickListener clickListener;

    public SearchSuggestionAdapter(List<MyEvent> eventList) {
        this.eventList = eventList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_search_suggestion, viewGroup, false);
        return new EventViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        try {
            EventViewHolder h = (EventViewHolder) holder;
            MyEvent item = eventList.get(i);

            h.tvTitle.setText(item.getEvent_name());
            h.tvTitle.setOnClickListener(v -> {
                if (clickListener != null)
                    clickListener.onItemClick(item.getEvent_name(), item);
            });


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    private class EventViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle;

        EventViewHolder(View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tv_txt);
        }
    }


    public interface OnItemClickListener {
        void onItemClick(String name, MyEvent item);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.clickListener = listener;
    }

}
