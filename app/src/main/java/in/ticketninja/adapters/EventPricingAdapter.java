/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.graphics.Paint;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.ticketninja.R;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.EventPriceCategory;

public class EventPricingAdapter extends RecyclerView.Adapter {

    //private final String TAG = EventPricingAdapter.class.getSimpleName();
    private List<EventPriceCategory> dataList;
    private ArrayList<EventPriceCategory> selectedDataList;
    private OnItemClickListener clickListener;

    private int currentExpand = 0;
    private int currentTicket = 0;

    public EventPricingAdapter(List<EventPriceCategory> dataList) {
        this.dataList = dataList;
        this.selectedDataList = new ArrayList<>(dataList.size());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_event_pricing, parent, false);
        return new PricingViewHolder(v);
    }

    public ArrayList<EventPriceCategory> getSelectedList() {
        selectedDataList.clear();
        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).noOfTicket() >= 1) {
                EventPriceCategory category = dataList.get(i);
                //category.setMrp(category.getMrp() * category.noOfTicket());
                selectedDataList.add(category);
            }
        }
        return selectedDataList;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        try {
            if (viewHolder instanceof PricingViewHolder) {
                EventPriceCategory category = dataList.get(position);

                PricingViewHolder h = (PricingViewHolder) viewHolder;
                h.tvItemName.setText(category.name());
                h.tvItemName.setVisibility(Validate.isNotNull(category.name()) ? View.VISIBLE : View.GONE);
                if (category.rate() == 0) {
                    h.tvItemPrice.setText(R.string.str_price_free);
                } else {
                    h.tvItemPrice.setText(String.format(Locale.getDefault(), "₹ %.2f", category.rate()));
                }
                h.tvItemDesc.setText(category.description());
                h.tvItemDesc.setVisibility(Validate.isNotNull(category.description()) ? View.VISIBLE : View.GONE);

                if (category.stock() <= 0 && Validate.isNull(category.description())) {
                    h.tvItemDesc.setText(R.string.str_out_of_stock);
                    h.tvItemDesc.setVisibility(View.VISIBLE);
                } else {
                    //h.tvItemDesc.setVisibility(View.GONE);
                    if (category.stock() <= 0) {
                        h.tvItemDesc.setText(String.format("%s\n%s" , "Sold Out" , category.description()));
                    } else {
                        h.tvItemDesc.setText(category.description());
                    }
                    // h.tvItemDesc.setText(category.description());
                }

                EventPricingCountAdapter mCountAdapter = new EventPricingCountAdapter(category.stock());
                mCountAdapter.setOnItemClickListener((position1, isChecked) -> {
                    currentTicket = isChecked ? position1 + 1 : 0;
                    EventPriceCategory category1 = (currentExpand >= 0 && currentExpand == h.getAdapterPosition()) ? dataList.get(currentExpand) : null;
                    if (clickListener != null)
                        clickListener.onItemClick(currentExpand, currentTicket, category1);
                });
                h.rvTicketCount.setAdapter(mCountAdapter);

                h.llTicketCount.setVisibility(currentExpand == h.getAdapterPosition() ? View.VISIBLE : View.GONE);
                h.ivItemPrice.setImageResource(currentExpand == h.getAdapterPosition() ? R.drawable.ic_event_pricing_collapse : R.drawable.ic_event_pricing_expand);
                h.llPricingHeader.setOnClickListener(v -> {
                    currentTicket = 0;
                    currentExpand = currentExpand == h.getAdapterPosition() ? -1 : h.getAdapterPosition();
                    EventPriceCategory category1 = null;
                    if (h.getAdapterPosition() > -1)
                        category1 = currentExpand == h.getAdapterPosition() ? dataList.get(currentExpand) : null;
                    if (clickListener != null)
                        clickListener.onItemClick(currentExpand, currentTicket, category1);

                    notifyDataSetChanged();
                });


                if (category.getMrp() <= category.rate()) {
                    h.tv_mrp.setVisibility(View.GONE);
                } else {
                    if (category.rate() == 0) {
                        h.tv_mrp.setText(R.string.str_price_free);
                    } else {
                        h.tv_mrp.setText(String.format(Locale.getDefault(), "₹ %.2f", category.getMrp()));
                    }
                    h.tv_mrp.setPaintFlags(h.tv_mrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }

                //h.llItemPrice.getLayoutParams().height = h.llPricingHeader.getHeight();
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private static class PricingViewHolder extends RecyclerView.ViewHolder {

        View llPricingHeader, llTicketCount;
        TextView tvItemName;
        LinearLayout llItemPrice;
        TextView tvItemDesc;
        ImageView ivItemPrice;
        TextView tvItemPrice;
        TextView tv_mrp;
        RecyclerView rvTicketCount;

        PricingViewHolder(@NonNull View itemView) {
            super(itemView);
            try {
                llPricingHeader = itemView.findViewById(R.id.llPricingHeader);
                llTicketCount = itemView.findViewById(R.id.llTicketCount);
                tvItemName = itemView.findViewById(R.id.tvItemName);
                tvItemDesc = itemView.findViewById(R.id.tvItemDesc);
                tv_mrp = itemView.findViewById(R.id.tv_mrp);
                tvItemPrice = itemView.findViewById(R.id.tvItemPrice);
                llItemPrice = itemView.findViewById(R.id.llItemPrice);
                ivItemPrice = itemView.findViewById(R.id.ivItemPrice);
                rvTicketCount = itemView.findViewById(R.id.rvTicketCount);

                Typeface tfMedium = TypefaceUtils.HelveticaMedium(itemView.getContext());
                tvItemName.setTypeface(tfMedium);
                tvItemDesc.setTypeface(tfMedium);
                tvItemPrice.setTypeface(tfMedium);
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    public void setDataList(List<EventPriceCategory> dataList) {
        this.dataList = dataList;
        currentExpand = 0;
        currentTicket = 0;
    }

    // LOAD MORE LISTENER
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.clickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, int count, EventPriceCategory priceCategory);
    }

}
