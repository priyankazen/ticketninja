/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.objects.DeliveryTimeList;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 29-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class SpinnerCODTiming extends BaseAdapter {

    private LayoutInflater mInflater;

    private List<DeliveryTimeList> dataList;

    public SpinnerCODTiming(Context context, List<DeliveryTimeList> dataList) {
        mInflater = LayoutInflater.from(context);
        this.dataList = dataList;
    }


    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private static class ViewHolder {
        TextView tvItemText;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder h;
        if (convertView == null) {

            convertView = mInflater.inflate(R.layout.custom_sp_address, parent, false);

            h = new ViewHolder();
            h.tvItemText = convertView.findViewById(R.id.tvItemText);
            convertView.setTag(h);

        } else {
            h = (ViewHolder) convertView.getTag();
        }
        h.tvItemText.setText(dataList.get(position).D_Time);
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder h;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.custom_sp_dropdown, parent, false);
            h = new ViewHolder();
            h.tvItemText = convertView.findViewById(R.id.tvItemText);
            convertView.setTag(h);

        } else {
            h = (ViewHolder) convertView.getTag();
        }
        h.tvItemText.setText(dataList.get(position).D_Time);
        return convertView;
    }

}
