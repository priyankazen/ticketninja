/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.objects.MyEventTimeList;

/**
 * TicketNinja_Working(in.ticketninja.objects) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 9/6/17.
 *
 * @author Suthar Rohit
 */
public class EventPricingTimeChooserAdapter extends RecyclerView.Adapter {

    private List<MyEventTimeList> daysList;
    private OnTimeSelectedListener listener;

    private int currentSelection = 0;

    public EventPricingTimeChooserAdapter(List<MyEventTimeList> daysList) {
        this.daysList = daysList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_event_pricing_time_chooser, viewGroup, false);
        return new EventViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        try {
            EventViewHolder h = (EventViewHolder) holder;
            MyEventTimeList item = daysList.get(i);

            h.tvEventTime.setText(item.getTime());

            h.itemView.setSelected(currentSelection == h.getAdapterPosition());
            h.tvEventTime.setSelected(currentSelection == h.getAdapterPosition());
            h.tvEventTime.setCompoundDrawablesWithIntrinsicBounds(currentSelection == h.getAdapterPosition() ?
                    R.drawable.ic_access_time_white : R.drawable.ic_access_time_red, 0, 0, 0);

            h.itemView.setOnClickListener(view -> {
                //currentSelection = currentSelection == h.getAdapterPosition() ? -1 : h.getAdapterPosition()
                if (currentSelection != h.getAdapterPosition()) {
                    currentSelection = h.getAdapterPosition();
                    if (listener != null) {
                        listener.onTimeSelected(h.getLayoutPosition(), item);
                    }
                    notifyDataSetChanged();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Override
    public int getItemCount() {
        return daysList.size();
    }

    private class EventViewHolder extends RecyclerView.ViewHolder {
        TextView tvEventTime;

        EventViewHolder(View itemView) {
            super(itemView);
            tvEventTime = itemView.findViewById(R.id.tvEventTime);
        }
    }

    public void setOnTimeSelectedListener(OnTimeSelectedListener listener) {
        this.listener = listener;
    }

    public interface OnTimeSelectedListener {

        void onTimeSelected(int position, MyEventTimeList time);

    }
}
