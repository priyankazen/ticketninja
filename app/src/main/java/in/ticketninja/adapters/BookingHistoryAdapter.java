/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import in.ticketninja.activity.PastTicketActivity;
import in.ticketninja.R;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.BookingHistory;

import static android.view.View.GONE;

/**
 * TicketNinja_Working(in.ticketninja.adapters) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 16/8/17.
 *
 * @author Suthar Rohit
 */
public class BookingHistoryAdapter extends RecyclerView.Adapter {

    //private static String TAG = BookingHistoryAdapter.class.getSimpleName();
    private OnItemClickListener clickListener;
    private Context mContext;

    private static List<BookingHistory> eventList = new ArrayList<>();

    private static final int VIEW_TYPE_FOOTER = 1;
    private static final int VIEW_TYPE_CELL = 2;

    private int from_where;

    public BookingHistoryAdapter(Context context, List<BookingHistory> eventList, int from) {
        this.mContext = context;
        BookingHistoryAdapter.eventList = eventList;
        from_where = from;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder holder;
        if (i == VIEW_TYPE_FOOTER && from_where == 1) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_booking_history_past_ticket, viewGroup, false);
            holder = new DataViewHolderHeader(v);
        } else {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.item_booking_history_ticket, viewGroup, false);
            holder = new EventViewHolder(v);
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        try {
            if (holder instanceof EventViewHolder) {
                EventViewHolder h = (EventViewHolder) holder;

                BookingHistory item = eventList.get(i);

                if (item.type.equalsIgnoreCase(Constant.EVENT_TYPE_ATTRACTION)) {
                    h.tv_seatno.setText("");

                } else if (Validate.isNotNull(item.getSeat_No())) {
                    SpannableString styledString = new SpannableString(mContext.getString(R.string.str_seat_no) + " " + item.getSeat_No());
                    styledString.setSpan(new ForegroundColorSpan(Color.parseColor("#979797")), 0, 9, 0);
                    h.tv_seatno.setText(styledString);

                } else if (Validate.isNotNull(item.getSeatType())) {
                    h.tv_seatno.setText(item.getSeatType());

                } else {
                    h.tv_seatno.setText(Constant.SEAT_TYPE_FREE_SEATING);
                }

                h.tvEventTitle.setText(item.getName());
                h.tvEventLocation.setText(item.getVenueName());

                // if ((item.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY) || item.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW)) && Constant.Code.ONE == item.isSeasonPass()) {
                if (!item.getEvent_date().equalsIgnoreCase(item.getDateTo()) && Constant.Code.ONE == item.isSeasonPass()) {
                    SpannableStringBuilder dateBuilder = new SpannableStringBuilder();
                    if (Validate.isDateNotNull(item.getEvent_date())) {
                        String monthFrom = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "MMM");
                        String dateFrom = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "dd");
                        dateBuilder.append(monthFrom.toUpperCase()).append(" ").append(dateFrom).append("\nto\n");
                        dateBuilder.setSpan(new ForegroundColorSpan(Color.RED), 0, monthFrom.length(), 0);
                    }
                    if (Validate.isDateNotNull(item.getDateTo())) {
                        int toStartPos = dateBuilder.length();
                        String monthTo = DateTimeUtils.changeDateTimeFormat(item.getDateTo(), DateTimeUtils.SERVER_FORMAT_DATE, "MMM");
                        String dateTo = DateTimeUtils.changeDateTimeFormat(item.getDateTo(), DateTimeUtils.SERVER_FORMAT_DATE, "dd");
                        dateBuilder.append(monthTo.toUpperCase()).append(" ").append(dateTo);
                        dateBuilder.setSpan(new ForegroundColorSpan(Color.RED), toStartPos, toStartPos + monthTo.length(), 0);
                    }

                    h.tvEventDate.setText(dateBuilder);
                    h.tvEventDate.setVisibility(View.VISIBLE);

                } else if (Validate.isDateNotNull(item.getEvent_date())) {
                    String month = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "MMM").toUpperCase();
                    String date = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "dd");
                    String day = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "EEE").toUpperCase();
                    SpannableStringBuilder dateBuilder = new SpannableStringBuilder();
                    dateBuilder.append(month).append("\n").append(date).append("\n").append(day);
                    dateBuilder.setSpan(new ForegroundColorSpan(Color.RED), 0, month.length(), 0);
                    dateBuilder.setSpan(new RelativeSizeSpan(1f), 0, month.length(), 0);
                    dateBuilder.setSpan(new RelativeSizeSpan(0.9f), month.length(), month.length() + day.length() + 1, 0);
                    dateBuilder.setSpan(new RelativeSizeSpan(0.85f), month.length() + day.length() + 1, dateBuilder.length(), 0);

                    h.tvEventDate.setText(dateBuilder);
                    h.tvEventDate.setVisibility(View.VISIBLE);

                } else {
                    h.tvEventDate.setVisibility(View.GONE);
                }

                if (Validate.isNotNull(item.getEvent_time()))
                    h.tv_time.setText(item.getDayType().equalsIgnoreCase(Constant.EventDayType.PERPETUAL)
                            ? item.getEvent_time() : String.format("%s %s", item.getEvent_time(), Constant.ONWARDS));

                h.tvBookingId.setText(item.getBookingId());
                h.tvEventType.setText(item.getType());
                h.tvEventCategory.setVisibility(Validate.isNotNull(item.getType()) ? View.VISIBLE : GONE);
                h.tvEventCategory.setText(item.getCategory());
                h.tvEventCategory.setVisibility(Validate.isNotNull(item.getCategory()) ? View.VISIBLE : GONE);
                h.tvEventLanguage.setText(item.getLanguage());
                h.tvEventLanguage.setVisibility(Validate.isNotNull(item.getLanguage()) ? View.VISIBLE : GONE);

                h.itemView.setOnClickListener(v -> {
                    if (clickListener != null)
                        clickListener.onItemClick(h.getAdapterPosition(), eventList.get(h.getAdapterPosition()));
                });
            } else {
                DataViewHolderHeader h = (DataViewHolderHeader) holder;
                h.btn_past_ticket.setOnClickListener(v -> {
                    Intent i1 = new Intent(mContext, PastTicketActivity.class);
                    mContext.startActivity(i1);
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemViewType(int position) {
        return (position == eventList.size()) ? VIEW_TYPE_FOOTER : VIEW_TYPE_CELL;
    }

    @Override
    public int getItemCount() {
        int position;
        if (from_where == 1) {
            position = eventList.size() + 1;
        } else {
            position = eventList.size();
        }
        return position;
    }


    private class EventViewHolder extends RecyclerView.ViewHolder {

        TextView tvEventTitle, tvEventLocation, tvEventDate, tv_seatno, tvEventCategory, tvEventLanguage, tv_time, tvEventType;
        TextView tvBookingId;

        EventViewHolder(View itemView) {
            super(itemView);
            try {
                Typeface tfRegular = TypefaceUtils.HelveticaRegular(itemView.getContext());
                Typeface tfMedium = TypefaceUtils.HelveticaMedium(itemView.getContext());
                //Typeface tfLight = TypefaceUtils.HelveticaLight(itemView.getContext());

                tvEventTitle = itemView.findViewById(R.id.tvEventTitle);
                tvEventTitle.setTypeface(tfMedium);
                tvEventLocation = itemView.findViewById(R.id.tvEventLocation);
                tvEventLocation.setTypeface(tfRegular);
                tvEventDate = itemView.findViewById(R.id.tvEventDate);
                tv_seatno = itemView.findViewById(R.id.tv_seatno);
                tvEventCategory = itemView.findViewById(R.id.tvEventCategory);
                tvEventLanguage = itemView.findViewById(R.id.tvEventLanguage);
                tv_time = itemView.findViewById(R.id.tv_time);
                tvEventType = itemView.findViewById(R.id.tvEventType);
                TextView tv_bid = itemView.findViewById(R.id.tv_bid);
                tv_bid.setTypeface(tfMedium);
                tvBookingId = itemView.findViewById(R.id.tvBookingId);
                tvBookingId.setTypeface(tfMedium);

                if (tvEventTitle != null) {
                    tvEventTitle.setTypeface(tfMedium);
                    tvEventLocation.setTypeface(tfRegular);
                    tvEventDate.setTypeface(tfMedium);
                }
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    private class DataViewHolderHeader extends RecyclerView.ViewHolder {

        Button btn_past_ticket;

        DataViewHolderHeader(View view) {
            super(view);
            btn_past_ticket = view.findViewById(R.id.btn_past_ticket);
        }
    }


    // LOAD MORE LISTENER
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.clickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, BookingHistory history);
    }

}
