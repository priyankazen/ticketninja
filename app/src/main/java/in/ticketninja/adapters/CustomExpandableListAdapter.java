package in.ticketninja.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Build;
import android.provider.MediaStore;
import androidx.annotation.RequiresApi;
import com.google.android.material.textfield.TextInputEditText;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import in.ticketninja.R;
import in.ticketninja.activity.ExpandableActivity;
import in.ticketninja.common.FilesUtils;
import in.ticketninja.common.UserIdentificationCode;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.UserIdentificationDetails;
import in.ticketninja.objects.UsrIdentificationData;
import in.ticketninja.widget.MyDatePickerDialog;

import static in.ticketninja.common.Utility.REQUEST_CAMERA;
import static in.ticketninja.common.Utility.SELECT_FILE;

///http://android.amberfog.com/?p=296
public class CustomExpandableListAdapter extends BaseExpandableListAdapter implements DialogClickEvent {

    private static final String TAG = CustomExpandableListAdapter.class.getSimpleName();
    private Context context;
    private ArrayList<UsrIdentificationData> arrayList;
    private CircleImageView imgCustomerPhoto;
    private int groupPos, childPos;


    public CustomExpandableListAdapter(Context context, ArrayList<UsrIdentificationData> expandableListTitle) {
        this.context = context;
        this.arrayList = expandableListTitle;
    }

    public void updateItem(int groupPosition, int childPosition) {
        //Log.e(TAG, "groupPosition " + groupPosition + ",childPosition: " + childPosition);

        for (int i = 0; i < arrayList.get(groupPosition).getUserInfo().size(); i++) {
            arrayList.get(groupPosition).getUserInfo().get(i).setBlinkCursor(false);
        }

        arrayList.get(groupPosition).getUserInfo().get(childPosition).setBlinkCursor(true);
        notifyDataSetChanged();
    }

    public ArrayList<UsrIdentificationData> getArrayList() {
        return arrayList;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return arrayList.get(groupPosition).getUserInfo().size();
    }

    @Override
    public UserIdentificationDetails getChild(int groupPosition, int childPosition) {
        return arrayList.get(groupPosition).getUserInfo().get(childPosition);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final UserIdentificationDetails child = arrayList.get(groupPosition).getUserInfo().get(childPosition);
        //Log.e(TAG, "groupPosition " + groupPosition + " " + convertView);
        // int childrenCount = arrayList.get(groupPosition).getUserInfo().size();

        ChildViewHolder holder;
        if (convertView == null) {
            holder = new ChildViewHolder();
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (layoutInflater != null) {
                convertView = layoutInflater.inflate(R.layout.item_ex_child_view, parent, false);
                holder.linearLayout = convertView.findViewById(R.id.linearlayout);
                convertView.setTag(holder);
            }
        } else {
            holder = (ChildViewHolder) convertView.getTag();
        }

        // Log.e(TAG, "childPosition " + childPosition + ",," + (ExpandableActivity.childSize - 1));

        holder.linearLayout.removeAllViews();

        if (childPosition == (ExpandableActivity.childSize - 1)) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, 40);
            holder.linearLayout.setLayoutParams(params);
            if (convertView != null) {
                convertView.setBackground(context.getResources().getDrawable(R.drawable.lnormalchild));
            }
        } else if (childPosition < ExpandableActivity.childSize) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, 0);
            holder.linearLayout.setLayoutParams(params);
            if (convertView != null) {
                convertView.setBackground(context.getResources().getDrawable(R.drawable.normalchild));
            }
        } else {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            params.setMargins(0, 0, 0, 0);
            holder.linearLayout.setLayoutParams(params);
            if (convertView != null) {
                convertView.setBackground(null);
            }
        }

        /*if (UserIdentificationCode.GENDER_CODE == child.getProof_id()) {
            holder.linearLayout.addView(getTitle(child));
            holder.linearLayout.addView(addRadioButtons(child, groupPosition, childPosition));

        } else if (UserIdentificationCode.CUSTOMER_PHOTO_CODE == child.getProof_id()) {
            groupPos = groupPosition;
            childPos = childPosition;
            holder.linearLayout.addView(addPhoto(child));

        } else if (UserIdentificationCode.ID_CARD_CODE == child.getProof_id()) {
            holder.linearLayout.addView(getTitle(child));
            holder.linearLayout.addView(addSpinner(groupPosition, childPosition));

        } else if (UserIdentificationCode.BIRTH_DATE_CODE == child.getProof_id()) {
            holder.linearLayout.addView(getText(child, groupPosition, childPosition));
            holder.linearLayout.addView(addLine());
        } else {
            if (child.getProof_id() != 0) {
                holder.linearLayout.addView(addEdt(child, childPosition, groupPosition));
            }
        }*/


        if (UserIdentificationCode.RADIO.equals(child.getProof_type())) {
            holder.linearLayout.addView(getTitle(child));
            holder.linearLayout.addView(addRadioButtons(child, groupPosition, childPosition));

        } else if (UserIdentificationCode.FILE.equals(child.getProof_type())) {
            groupPos = groupPosition;
            childPos = childPosition;
            holder.linearLayout.addView(addPhoto(child));

        } else if (UserIdentificationCode.DROP_DOWN.equals(child.getProof_type())) {
            holder.linearLayout.addView(getTitle(child));
            holder.linearLayout.addView(addSpinner(child, groupPosition, childPosition));

        } else if (UserIdentificationCode.BIRTHDAY.equals(child.getProof_type())) {
            holder.linearLayout.addView(getText(child, groupPosition, childPosition));
            holder.linearLayout.addView(addLine());
        } else {
            if (UserIdentificationCode.TEXT.equals(child.getProof_type()) || UserIdentificationCode.EMAIL_TYPE.equals(child.getProof_type()) ||
                    UserIdentificationCode.MOBILE_NO_TYPE.equals(child.getProof_type())) {
                holder.linearLayout.addView(addEdt(child, childPosition, groupPosition));
            }
        }
        return convertView;
    }


    @Override
    public UsrIdentificationData getGroup(int groupPosition) {
        return arrayList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return arrayList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        UsrIdentificationData listTitle = arrayList.get(groupPosition);
        GroupViewHolder holder;
        try {
            if (convertView == null) {
                holder = new GroupViewHolder();
                LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                if (layoutInflater != null) {
                    convertView = layoutInflater.inflate(R.layout.item_ex_group_view, parent, false);
                    holder.ll = convertView.findViewById(R.id.ll);
                    holder.txt = convertView.findViewById(R.id.txt);
                    holder.listTitleTextView = convertView.findViewById(R.id.listTitle);
                    holder.listTitleTextView.setTypeface(null, Typeface.BOLD);
                    convertView.setTag(holder);
                }
            } else {
                holder = (GroupViewHolder) convertView.getTag();
            }

            if (isExpanded) {
                holder.txt.setVisibility(View.VISIBLE);
                holder.ll.setBackground(context.getResources().getDrawable(R.drawable.groupbg));
                Utility.showKeyboard(context, convertView);
            } else {
                holder.txt.setVisibility(View.GONE);
                holder.ll.setBackground(null);
                // Utility.hideKeyboard(context,convertView);
            }
            //Log.e(TAG, "groupPosition = " + groupPosition);
            holder.listTitleTextView.setText(listTitle.getGroup_title());
        }catch (Exception e){
            e.printStackTrace();
        }


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public void openImagesView(String type) {
        if (context.getResources().getString(R.string.take_photo).equals(type)) {
            cameraIntent();
        } else if (context.getResources().getString(R.string.choose_from_library).equals(type)) {
            galleryIntent();
        }
    }

    private class GroupViewHolder {
        TextView listTitleTextView, txt;
        LinearLayout ll;
    }

    private class ChildViewHolder {
        LinearLayout linearLayout;
    }

    private Spinner addSpinner(UserIdentificationDetails child, int groupPosition, int childPosition) {

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        Spinner spinner = new Spinner(context);
        spinner.setLayoutParams(params);

        spinner.setBackground(context.getResources().getDrawable(R.drawable.gray_border_line));

        ArrayList<String> spinArrayList = new ArrayList<>();
        /*spinArrayList.add(AADHAR_CARD);
        spinArrayList.add(DRIVING_LICENSE);
        spinArrayList.add(PASSPORT);
        spinArrayList.add(STUDENT_ID_CARD);
        spinArrayList.add(VOTER_CARD);*/

        String spinValue = child.getProof_value();
        String[] spinArray = spinValue.split(UserIdentificationCode.SPLIT);
        Collections.addAll(spinArrayList, spinArray);

        if (spinArrayList.size() > 0) {
            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, 0, spinArrayList);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(arrayAdapter);
            spinner.setPadding(5, 25, 10, 25);
            arrayList.get(groupPosition).getUserInfo().get(childPosition).setText(spinArrayList.get(0));
            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    arrayList.get(groupPosition).getUserInfo().get(childPosition).setText(adapterView.getSelectedItem().toString());
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        }
        //spinner.setBackground(context.getResources().getDrawable(R.drawable.red_border_line));
        return spinner;
    }

    private View addPhoto(UserIdentificationDetails details) {

        LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View v = layoutInflater != null ?
                layoutInflater.inflate(R.layout.item_ex_child_customer_photo, null) : null;
        if (v != null) {

            try {
                TextView txt = v.findViewById(R.id.txtPhotoUpload);
                txt.setOnClickListener(view -> {
                    Log.e(TAG, "id = " + view.getId());
                    Log.e(TAG, "id = " + view.getId());
                });

                imgCustomerPhoto = v.findViewById(R.id.imgCustomerPhoto);
                ImageView imageView = v.findViewById(R.id.imgEdit);
                imageView.setOnClickListener(view -> {
                    Log.e(TAG, "id = " + view.getId());
                    Utility.selectImage(context, CustomExpandableListAdapter.this);
                });

                if (Validate.isNotNull(details.getText())) {
                    if (Utility.CAMERA.equals(details.getText())) {
                        // byte[] data = Base64.decode(details.getImagePath(), Base64.NO_WRAP);
                        Bitmap bm = FilesUtils.base64ToBitmap(details.getImagePath());
                        imgCustomerPhoto.setImageBitmap(bm);

                    /*Glide.with(context)
                            .load(data)
                            .centerCrop()
                            .skipMemoryCache(false)
                            .diskCacheStrategy(DiskCacheStrategy.ALL)
                            .placeholder(R.drawable.ic_palceholder_edit_profile)
                            .error(R.drawable.ic_palceholder_edit_profile)
                            .into(imgCustomerPhoto);*/
                    } else if (Utility.GALLERY.equals(details.getText())) {
                   /* if (details.getImagePath().contains("storage")){
                        Bitmap bm = FilesUtils.base64ToBitmap(details.getImagePath());
                        imgCustomerPhoto.setImageBitmap(bm);
                    }else {*/
                        Glide.with(context)
                                .load(details.getImagePath())
                                .centerCrop()
                                .listener(new RequestListener<String, GlideDrawable>() {
                                    @Override
                                    public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                        return false;
                                    }

                                    @Override
                                    public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                        imgCustomerPhoto.setImageDrawable(resource);
                                        return true;
                                    }
                                })
                                .skipMemoryCache(false)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)
                                .placeholder(R.drawable.ic_palceholder_edit_profile)
                                .error(R.drawable.ic_palceholder_edit_profile)
                                .into(imgCustomerPhoto);
                        // }
                    } else {
                        imgCustomerPhoto.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_palceholder_edit_profile));
                    }
                } else {
                    imgCustomerPhoto.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_palceholder_edit_profile));
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
        return v;
    }

    public void setPic(String path, String id,String imgType,String imageBase64) {
        arrayList.get(groupPos).getUserInfo().get(childPos).setImageType(imgType);
        arrayList.get(groupPos).getUserInfo().get(childPos).setText(id);
        arrayList.get(groupPos).getUserInfo().get(childPos).setImagePath(path);
        arrayList.get(groupPos).getUserInfo().get(childPos).setImageBase64(imageBase64);
    }

    public ImageView getIv() {
        return imgCustomerPhoto;
    }

    private View addEdt(UserIdentificationDetails details, int childPosition, int groupPosition) {
        //Log.e(TAG, "click_groupPosition11= " + groupPosition + " , childPosition: " + childPosition);
        LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View v = layoutInflater != null ?
                layoutInflater.inflate(R.layout.edittext_view, null) : null;

        if (v != null) {

            //TextInputLayout textInputLayout = v.findViewById(R.id.inputL);
            //textInputLayout.setHint(details.getProof_name());

            TextInputEditText edt = v.findViewById(R.id.edt);

            edt.setHint(details.getProof_name());
            edt.setText(details.getText());
            edt.setSelection(Objects.requireNonNull(edt.getText()).length());

            if (details.isBlinkCursor()) {
                edt.requestFocus();
            }

            //Log.e(TAG, "groupPosition1 " + groupPosition + ",childPosition1: " + childPosition);
           /* if (TextUtils.isEmpty(details.getText()) && details.getProof_id() != UserIdentificationCode.CUSTOMER_PHOTO_CODE && childPosition == 0) {
                // Request focus and show soft keyboard automatically
                edt.requestFocus();
            }else if (details.getProof_id() == UserIdentificationCode.FULL_NAME_CODE && !TextUtils.isEmpty(details.getText())){
                edt.clearFocus();
            }*/

            if (UserIdentificationCode.MOBILE_NO_CODE == details.getProof_id()) {
                edt.setInputType(InputType.TYPE_CLASS_PHONE);
                edt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
            } else if (UserIdentificationCode.EMAIL_CODE == details.getProof_id()) {
                edt.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            }

            /*edt.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    Log.e(TAG, "edtsetOnClickListener groupPosition1 " + groupPosition + ",childPosition1: " + childPosition);

                    return false;
                }
            });*/


            edt.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    arrayList.get(groupPosition).getUserInfo().get(childPosition).setText(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }

        return v;
    }

    private RadioGroup addRadioButtons(UserIdentificationDetails child, int groupPosition, int childPosition) {

        //for (int row = 0; row < 1; row++) {
        RadioGroup rg = new RadioGroup(context);
        rg.setOrientation(LinearLayout.HORIZONTAL);

        if (!TextUtils.isEmpty(child.getProof_value())) {
            String radioValue = child.getProof_value();
            String[] radioArray = radioValue.split(UserIdentificationCode.SPLIT);

            for (int i = 0; i < radioArray.length; i++) {
                RadioButton rdBtn = new RadioButton(context);
                rdBtn.setId(i);

                rdBtn.setText(radioArray[i]);
                if (radioArray[i].equals(context.getResources().getString(R.string.str_male))) {
                    if (Validate.isNull(child.getText())) {
                        rdBtn.setChecked(true);
                        arrayList.get(groupPosition).getUserInfo().get(childPosition).setText(context.getResources().getString(R.string.male));
                    }
                }

               /* if (i == 0) {
                    rdBtn.setText(R.string.male);
                    if (Validate.isNull(child.getText())) {
                        rdBtn.setChecked(true);
                        arrayList.get(groupPosition).getUserInfo().get(childPosition).setText(context.getResources().getString(R.string.male));
                    }
                } else if (i == 1) {
                    rdBtn.setText(R.string.female);
                } else {
                    rdBtn.setText(R.string.other);
                }*/

                if (rdBtn.getText().toString().equals(child.getText())) {
                    // Log.e(TAG, "child.getText(): " + child.getText());
                    rdBtn.setChecked(true);
                }

                rg.addView(rdBtn);

                rg.setOnCheckedChangeListener((radioGroup, i1) -> {
                    // get selected radio button from radioGroup
                    int selectedId = radioGroup.getCheckedRadioButtonId();

                    // find the radiobutton by returned id
                    RadioButton radioButton = rg.findViewById(selectedId);

                    if (radioButton != null) {
                        // Log.e(TAG, "radioText: " + radioButton.getText());
                        arrayList.get(groupPosition).getUserInfo().get(childPosition).setText(radioButton.getText().toString());
                    }
                });
            }

            return rg;
        } else {
            return null;
        }
    }


    private TextView getTitle(UserIdentificationDetails details) {
        TextView txt = new TextView(context);

        if (UserIdentificationCode.ID_CARD_CODE == details.getProof_id()) {
            txt.setPadding(10, 25, 0, 25);
            //txt.setText(R.string.proof_id);
            txt.setText(details.getProof_name());
        } else {
            txt.setPadding(5, 0, 0, 0);
            //txt.setText(R.string.gender);
            txt.setText(details.getProof_name());
        }

        return txt;
    }

    private TextView getText(UserIdentificationDetails details, int groupPosition, int childPosition) {
        TextView textView = new TextView(context);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.setMargins(15, 0, 0, 0);
        textView.setLayoutParams(params);

        textView.setTextSize(16);
        textView.setTextColor(context.getResources().getColor(R.color.themeGrayDark));
        textView.setPadding(0, 30, 5, 5);
        if (Validate.isNotNull(details.getText())) {
            textView.setText(details.getText());
        } else {
            textView.setText(details.getProof_name());
        }

        MyDatePickerDialog DatePickerDialog = new MyDatePickerDialog(context, R.style.datepicker,
                (view, year, monthOfYear, dayOfMonth) -> {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    //String selectedDate = String.format(Locale.getDefault(), "%d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
                    String selectedDate = String.format(Locale.getDefault(), "%02d-%02d-%02d", dayOfMonth, monthOfYear + 1, year);
                    Log.e(TAG, "selectedDate: " + selectedDate);
                    textView.setText(selectedDate);
                    arrayList.get(groupPosition).getUserInfo().get(childPosition).setText(selectedDate);
                    notifyDataSetChanged();
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        // DatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        DatePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

        textView.setOnClickListener(view -> {
            Log.e(TAG, "name: " + details.getProof_name());
//
            DatePickerDialog.show();
        });
        return textView;
    }

    private View addLine() {
        View v = new View(context);
        LinearLayout.LayoutParams linLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 3);
        linLayoutParams.setMargins(15, 0, 0, 15);
        v.setLayoutParams(linLayoutParams);
        v.setBackgroundColor(context.getResources().getColor(R.color.themeGrayDark));
        return v;
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        ((Activity) context).startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    private void cameraIntent() {
        /*// Define the file-categoryName to save photo taken by Camera activity
        String fileName = "TicketNinja_" + DateTimeUtils.getCurrentDateTimeMix() + ".jpg";

        // Create parameters for Intent with filename
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, fileName);
        values.put(MediaStore.Images.Media.DESCRIPTION, "Image captured by Camera on TicketNinja App.");

        // mImageUri is the current activity attribute, define and save it for later usage
        Uri mImageUri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
*/
        // Standard Intent action that can be sent to have the camera application capture an image and return it.
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageUri);
        ((Activity) context).startActivityForResult(intent, REQUEST_CAMERA);
    }

}