/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.objects.EventTerms;

public class EventTermsAdapter extends RecyclerView.Adapter {

    private List<EventTerms> dataList;

    public EventTermsAdapter(List<EventTerms> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_event_terms, parent, false);
        return new EventViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        try {
            if (viewHolder instanceof EventViewHolder) {
                EventViewHolder h = (EventViewHolder) viewHolder;
                h.tvItemDesc.setText(dataList.get(position).toString());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private static class EventViewHolder extends RecyclerView.ViewHolder {

        TextView tvItemBullet;
        TextView tvItemDesc;

        EventViewHolder(@NonNull View itemView) {
            super(itemView);
            tvItemBullet = itemView.findViewById(R.id.tvItemBullet);
            tvItemDesc = itemView.findViewById(R.id.tvItemDesc);
        }
    }

}
