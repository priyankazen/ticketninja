/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.ticketninja.R;
import in.ticketninja.common.Constant;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.EventPriceCategory;

/**
 * TicketNinja_Working(in.ticketninja.adapters) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 12/8/17.
 *
 * @author Suthar Rohit
 */
public class SelectTicketAdapter1 extends RecyclerView.Adapter {
    private final String TAG = SelectTicketAdapter1.class.getSimpleName();
    private static final int MAXIMUM_TICKET = 10;

    private List<EventPriceCategory> dataList;
    private ArrayList<EventPriceCategory> selectedDataList;
    private OnTicketValueChangeListener changeListener;
    private Context context;
    private int multiCategorySelect;
    private int previousIndex = 0;
    private int per_trans_limit;
    private int totalTicketCount = 0;
    //private int totalQty = 0;


    public SelectTicketAdapter1(Context ctx, List<EventPriceCategory> dataList, int perTransLimit, int multiCategorySelect) {
        this.dataList = dataList;
        selectedDataList = new ArrayList<>(dataList.size());
        this.per_trans_limit = perTransLimit;
        this.context = ctx;
        this.multiCategorySelect = 0;
        this.multiCategorySelect = multiCategorySelect;
    }

    public ArrayList<EventPriceCategory> getSelectedList() {
        selectedDataList.clear();
        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).noOfTicket() >= 1) {
                EventPriceCategory category = dataList.get(i);
                //category.setMrp(category.getMrp() * category.noOfTicket());
                selectedDataList.add(category);
            }
        }
        return selectedDataList;
    }

    public void updateCount(int per_trans_limit) {
        this.per_trans_limit = 10;
        this.totalTicketCount = 0;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_selectdate_and_time1, viewGroup, false);
        return new EventViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        try {
            EventViewHolder h = (EventViewHolder) holder;
            EventPriceCategory item = dataList.get(i);


            h.tv_category.setText(item.name());

            h.tv_dec.setVisibility(Validate.isNotNull(item.description()) ? View.VISIBLE : View.GONE);
            h.tv_dec.setText(item.description());

            h.tvStock.setText(context.getResources().getString(R.string.add));   //add 14 octo 2019

            if (item.stock() <= 0) {
                h.tvStock.setEnabled(false);
                h.tvStock.setTextColor(context.getResources().getColor(R.color.coloGray));
                h.tvStock.setText(R.string.solde_out);
                hideShow1(h, View.VISIBLE);
                hideShow(h, View.GONE); //add 14 octo 2019
            } else {
                if (item.noOfTicket() > 0) {
                    hideShow(h, View.VISIBLE);
                    hideShow1(h, View.GONE);
                } else {
                    hideShow(h, View.GONE);
                    hideShow1(h, View.VISIBLE);
                }
            }

            h.tv_qty.setText(String.valueOf(item.noOfTicket()));

            if (item.noOfTicket() <= 0) {
                if (item.rate() == 0) {
                    h.tv_price.setText(R.string.str_price_free);
                } else {
                    h.tv_price.setText(String.format(Locale.getDefault(), "₹ %.2f", item.rate()));
                }
                ///h.tv_total.setVisibility(View.GONE);

            } else {

                // h.tv_total.setVisibility(View.VISIBLE);

                //h.tv_price.setText(String.format(Locale.getDefault(), " x ₹ %.2f = ₹ %.2f", item.rate(), item.noOfTicket() * item.rate()));
                if (item.rate() == 0) {
                    h.tv_price.setText(R.string.str_price_free);
                } else {
                    h.tv_price.setText(String.format(Locale.getDefault(), "₹ %.2f", item.rate()));
                }
          /*  if ((item.noOfTicket() * item.rate()) == 0) {
                // h.tv_total.setText(String.format(" = %s", h.tv_total.getContext().getString(R.string.str_price_free)));
            } else {
                // h.tv_total.setText(String.format(Locale.getDefault(), " = ₹ %.2f", item.noOfTicket() * item.rate()));
            }*/
            }


            if (item.getMrp() <= item.rate()) {
                h.tvmrp.setVisibility(View.GONE);

            } else {
                if (item.getMrp() == 0) {
                    h.tvmrp.setText(R.string.str_price_free);
                } else {
                    h.tvmrp.setText(String.format(Locale.getDefault(), "₹ %.2f", item.getMrp()));
                }
                h.tvmrp.setPaintFlags(h.tvmrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }

            if (h.tvStock.getText().equals(context.getResources().getString(R.string.add))) {
                h.tvStock.setEnabled(true);
                h.tvStock.setTextColor(context.getResources().getColor(R.color.themeRed));
                h.tvStock.setOnClickListener(v -> {
                    try {
                        Log.e(TAG, "previousIndex:" + previousIndex);

                    /*if (totalQty == 10) {
                        Toast.makeText(context.getApplicationContext(), R.string.stock_msg_error, Toast.LENGTH_LONG).show();
                        return;
                    }*/

                        if (totalTicketCount == per_trans_limit) {
                            Toast.makeText(v.getContext(), R.string.stock_msg_error, Toast.LENGTH_LONG).show();
                            return;
                        }

                        if (Constant.Code.ZERO == multiCategorySelect) {
                            dataList.get(previousIndex).noOfTicket(0);
                            totalTicketCount = 0;
                        }

                        hideShow(h, View.VISIBLE);
                        hideShow1(h, View.GONE);
                        int noTicket = item.noOfTicket() + 1;
                        totalTicketCount += 1;
                        item.noOfTicket(noTicket);
                        item.amount(noTicket * item.rate());
                        item.setTotalMRP(noTicket * item.getMrp());
                        Log.e(TAG, "previousIndex1:" + h.getAdapterPosition());
                        previousIndex = h.getAdapterPosition();
                        //totalQty = totalQty + 1;
                        notifyDataSetChanged();
                        if (changeListener != null)
                            changeListener.onTicketValueChange(h.getAdapterPosition());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                });
            }

            h.iv_plus.setOnClickListener(v -> {

                Log.e(TAG, "item.stock()=" + item.stock());
                Log.e(TAG, "item.noOfTicket()=" + item.noOfTicket());
                Log.e(TAG, "totalTicketCount=" + totalTicketCount);
                Log.e(TAG, "apiTotalCount=" + per_trans_limit);

           /* if (totalQty == 10) {
                Toast.makeText(context.getApplicationContext(), R.string.stock_msg_error, Toast.LENGTH_LONG).show();
                return;
            }*/

                if (totalTicketCount == per_trans_limit) {
                    Toast.makeText(v.getContext(), R.string.stock_msg_error, Toast.LENGTH_LONG).show();
                    return;
                }

                if (item.stock() <= item.noOfTicket()) {
                    Toast.makeText(v.getContext(), R.string.stock_msg_error, Toast.LENGTH_LONG).show();
                    return;
                }

                if (item.noOfTicket() >= 0 && item.noOfTicket() < MAXIMUM_TICKET) {
                    int noTicket = item.noOfTicket() + 1;
                    totalTicketCount += 1;
                    item.noOfTicket(noTicket);
                    item.amount(noTicket * item.rate());
                    item.setTotalMRP(noTicket * item.getMrp());
                    //totalQty = totalQty + 1;
                    notifyDataSetChanged();

                    if (changeListener != null)
                        changeListener.onTicketValueChange(h.getAdapterPosition());
                }
            });

            h.iv_minus.setOnClickListener(v -> {

                if (item.noOfTicket() > 0) {
                    if (item.noOfTicket() == 1) {
                        hideShow(h, View.GONE);
                        hideShow1(h, View.VISIBLE);
                    }

                    int noTicket = item.noOfTicket() - 1;
                    totalTicketCount -= 1;
                    item.noOfTicket(noTicket);
                    item.amount(item.noOfTicket() * item.rate());
                    item.setTotalMRP(noTicket * item.getMrp());
                    //totalQty = totalQty - 1;
                    notifyDataSetChanged();

                    if (changeListener != null)
                        changeListener.onTicketValueChange(h.getAdapterPosition());
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    private void hideShow(SelectTicketAdapter1.EventViewHolder h, int b) {
        h.iv_plus.setVisibility(b);
        h.iv_minus.setVisibility(b);
        h.tv_qty.setVisibility(b);
    }

    private void hideShow1(SelectTicketAdapter1.EventViewHolder h, int b) {
        h.tvStock.setVisibility(b);
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private class EventViewHolder extends RecyclerView.ViewHolder {

        TextView tv_price, tv_category, tvmrp, tvStock, tv_qty;
        TextView tv_dec;
        LinearLayout ll_price;
        ImageView iv_plus, iv_minus;

        EventViewHolder(View itemView) {
            super(itemView);
            try {
                tv_price = itemView.findViewById(R.id.tv_price);
                tv_category = itemView.findViewById(R.id.tv_category);
                tv_dec = itemView.findViewById(R.id.tv_dec);
                iv_plus = itemView.findViewById(R.id.iv_plus);
                iv_minus = itemView.findViewById(R.id.iv_minus);
                tvmrp = itemView.findViewById(R.id.tvmrp);
                ll_price = itemView.findViewById(R.id.ll_price);
                tvStock = itemView.findViewById(R.id.tvStock);
                tv_qty = itemView.findViewById(R.id.tv_qty);
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    // LOAD MORE LISTENER
    public void setOnItemClickListener(OnTicketValueChangeListener listener) {
        this.changeListener = listener;
    }

    public interface OnTicketValueChangeListener {
        void onTicketValueChange(int position);
    }

}
