/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.common.CommonClass;
import in.ticketninja.objects.MyEvent;

/**
 * TicketNinja_Working(in.ticketninja.objects) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 9/6/17.
 *
 * @author Suthar Rohit
 */
public class FeaturedEventsAdapter extends RecyclerView.Adapter {
    private Context mContext;
    private List<MyEvent> EventList;

    public FeaturedEventsAdapter(Context context, List<MyEvent> eventlist) {
        this.mContext = context;
        this.EventList = eventlist;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder holder;
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_event_list, viewGroup, false);
        holder = new FeaturedEventsAdapter.Viewholder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder itemview, int i) {
        try {
            final Viewholder holder = (Viewholder) itemview;

            MyEvent item = EventList.get(i);


            Glide.with(mContext)
                    .load(item.getPosterImageUrl())
                    .centerCrop()
                    .into(holder.iv_poster);

            holder.tv_name.setText(item.getEvent_name());
            holder.tv_date.setText(item.getEvent_date());
            holder.tv_category.setText(item.getCategory());
            holder.tv_language.setText(item.getLanguage());
            holder.tv_city.setText(item.getCity());

            holder.tv_city.setOnClickListener(v -> CommonClass.openMap(mContext, item.getEvent_name(), item.getLatitude(), item.getLongitude()));


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return EventList.size();
    }


    private class Viewholder extends RecyclerView.ViewHolder {
        private ImageView iv_poster;
        private TextView tv_name, tv_date, tv_city, tv_category, tv_language;

        private Viewholder(View itemView) {
            super(itemView);
            try {
                iv_poster = itemView.findViewById(R.id.ivEventPoster);
                tv_name = itemView.findViewById(R.id.tvEventTitle);
                tv_date = itemView.findViewById(R.id.tvEventDate);
                tv_city = itemView.findViewById(R.id.tvEventLocation);
                tv_category = itemView.findViewById(R.id.tvEventCategory);
                tv_language = itemView.findViewById(R.id.tvEventLanguage);

            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

}
