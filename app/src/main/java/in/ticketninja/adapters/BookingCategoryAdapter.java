/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.graphics.Typeface;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;
import java.util.Locale;

import in.ticketninja.R;
import in.ticketninja.common.Constant;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.BookingTicketList;

/**
 * TicketNinja_Working(in.ticketninja.objects) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 9/6/17.
 *
 * @author Suthar Rohit
 */
public class BookingCategoryAdapter extends RecyclerView.Adapter {

    private List<BookingTicketList> dataList;

    public BookingCategoryAdapter(List<BookingTicketList> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_order_summary_category, viewGroup, false);
        return new EventViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {

        try {
            EventViewHolder h = (EventViewHolder) holder;

            BookingTicketList item = dataList.get(i);

            h.tvItemName.setText(String.format(Locale.getDefault(), "%d x %s", item.noOfTicket(), item.categoryName()));
            if (item.rate() == 0) {
                h.tvPrice.setText(R.string.str_price_free);
            } else {
                h.tvPrice.setText(String.format(Locale.getDefault(), "₹ %.2f", item.rate()));
            }

            if (Validate.isNotNull(item.getEventType()) && item.getEventType().equalsIgnoreCase(Constant.EVENT_TYPE_ATTRACTION)) {
                h.tvSeatNoLabel.setVisibility(View.GONE);
                h.tvSeatNo.setVisibility(View.GONE);
            } else {
                if (Validate.isNotNull(item.seatNo())
                        && item.seatType().equalsIgnoreCase(Constant.SEAT_TYPE_ARRANGE_SEATING)) {
                    h.tvSeatNoLabel.setVisibility(View.VISIBLE);
                    h.tvSeatNo.setVisibility(View.VISIBLE);

                } else if (Validate.isNotNull(item.seatType())) {
                    h.tvSeatNoLabel.setVisibility(View.GONE);
                    h.tvSeatNo.setVisibility(View.VISIBLE);

                } else {
                    h.tvSeatNoLabel.setVisibility(View.GONE);
                    h.tvSeatNo.setVisibility(View.GONE);
                }
            }

            if (Validate.isNotNull(item.seatNo())) {
                h.tvSeatNo.setText(item.seatNo());
            } else if (Validate.isNotNull(item.seatType())) {
                h.tvSeatNo.setText(item.seatType());
            } else {
                h.tvSeatNo.setText("");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private class EventViewHolder extends RecyclerView.ViewHolder {
        TextView tvItemName, tvPrice, tvSeatNoLabel, tvSeatNo;

        EventViewHolder(View itemView) {
            super(itemView);

            try {
                Typeface tfRegular = TypefaceUtils.HelveticaRegular(itemView.getContext());
                Typeface tfMedium = TypefaceUtils.HelveticaMedium(itemView.getContext());

                tvItemName = itemView.findViewById(R.id.tvOrderCategoryItemName);
                tvPrice = itemView.findViewById(R.id.tvOrderCategoryItemPrice);
                tvSeatNoLabel = itemView.findViewById(R.id.tvOrderCategorySeatNoLabel);
                tvSeatNo = itemView.findViewById(R.id.tvOrderCategorySeatNo);

                tvItemName.setTypeface(tfMedium);
                tvPrice.setTypeface(tfMedium);
                tvSeatNoLabel.setTypeface(tfRegular);
                tvSeatNo.setTypeface(tfRegular);
            }catch (Exception e){
                e.printStackTrace();
            }


        }
    }

}
