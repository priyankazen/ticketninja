/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.User_Ticket_Event;

import static android.view.View.GONE;

public class MyTicketsAdapter extends RecyclerView.Adapter {
    //private final String TAG = MyTicketsAdapter.class.getSimpleName();
    private OnItemClickListener clickListener;
    private Context mContext;

    private List<User_Ticket_Event> eventList;

    public MyTicketsAdapter(Context context, List<User_Ticket_Event> eventList) {
        this.mContext = context;
        this.eventList = eventList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {

        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_booking_history_ticket1,
                        viewGroup, false);

        return new EventViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        try {
            EventViewHolder h = (EventViewHolder) holder;

            User_Ticket_Event item = eventList.get(i);

            h.tvEventTitle.setText(item.getEvent_name());
            h.tvEventLocation.setText(item.getVenueName());

            //if (item.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY) || item.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW)) {
            if (!item.getEvent_date().equalsIgnoreCase(item.getDateTo())) {
                SpannableStringBuilder dateBuilder = new SpannableStringBuilder();
                if (Validate.isDateNotNull(item.getEvent_date())) {
                    String monthFrom = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "MMM");
                    String dateFrom = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "dd");
                    //dateBuilder.append(monthFrom.toUpperCase()).append(" ").append(dateFrom).append("\nto\n");
                    // dateBuilder.setSpan(new ForegroundColorSpan(Color.RED), 0, monthFrom.length(), 0);
                    dateBuilder.append(monthFrom.toUpperCase()).append(" ").append(dateFrom).append(" to ");
                }
                if (Validate.isDateNotNull(item.getDateTo())) {
                    int toStartPos = dateBuilder.length();
                    String monthTo = DateTimeUtils.changeDateTimeFormat(item.getDateTo(), DateTimeUtils.SERVER_FORMAT_DATE, "MMM");
                    String dateTo = DateTimeUtils.changeDateTimeFormat(item.getDateTo(), DateTimeUtils.SERVER_FORMAT_DATE, "dd");
                    dateBuilder.append(monthTo.toUpperCase()).append(" ").append(dateTo);
                    // dateBuilder.setSpan(new ForegroundColorSpan(Color.RED), toStartPos, toStartPos + monthTo.length(), 0);
                }

                h.tvEventDate.setText(dateBuilder);
                h.tvEventDate.setVisibility(View.VISIBLE);

            } else if (Validate.isDateNotNull(item.getEvent_date())) {
                String month = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "MMM").toUpperCase();
                String date = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "dd");
                String day = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "EEE").toUpperCase();
                SpannableStringBuilder dateBuilder = new SpannableStringBuilder();

            /*dateBuilder.append(month).append("\n").append(date).append("\n").append(day);
            dateBuilder.setSpan(new ForegroundColorSpan(Color.RED), 0, month.length(), 0);
            dateBuilder.setSpan(new RelativeSizeSpan(1f), 0, month.length(), 0);
            dateBuilder.setSpan(new RelativeSizeSpan(0.9f), month.length(), month.length() + day.length() + 1, 0);
            dateBuilder.setSpan(new RelativeSizeSpan(0.85f), month.length() + day.length() + 1, dateBuilder.length(), 0);
*/
                String year = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "yyyy").toUpperCase();
                dateBuilder.append(month).append(" ").append(date).append(", ").append(year);

                h.tvEventDate.setText(dateBuilder);
                h.tvEventDate.setVisibility(View.VISIBLE);

            } else {
                h.tvEventDate.setVisibility(View.GONE);
            }

            if (item.getType().equalsIgnoreCase(Constant.EVENT_TYPE_ATTRACTION)) {
                h.tv_seatno.setText("");

            } else if (Validate.isNotNull(item.getSeatNo())) {
                SpannableString styledString = new SpannableString(mContext.getString(R.string.str_seat_no) + " " + item.getSeatNo());
                styledString.setSpan(new ForegroundColorSpan(Color.parseColor("#979797")), 0, 9, 0);
                h.tv_seatno.setText(styledString);

            } else if (Validate.isNotNull(item.getSeat_Type())) {
                h.tv_seatno.setText(item.getSeat_Type());

            } else {
                h.tv_seatno.setText(Constant.SEAT_TYPE_FREE_SEATING);
            }

            if (Validate.isNotNull(item.getEvent_time())) {
                h.tv_time.setVisibility(View.GONE);
                h.tv_time.setText(item.getDayType().equalsIgnoreCase(Constant.EventDayType.PERPETUAL)
                        ? item.getEvent_time() : String.format("%s %s", item.getEvent_time(), Constant.ONWARDS));
            } else
                h.tv_time.setVisibility(View.GONE);

            h.tvEventType.setText(item.getType());
            h.tvEventCategory.setVisibility(Validate.isNotNull(item.getType()) ? View.VISIBLE : GONE);
            h.tvEventCategory.setText(item.getCategory());
            h.tvEventCategory.setVisibility(Validate.isNotNull(item.getCategory()) ? View.VISIBLE : GONE);
            h.tvEventLanguage.setText(item.getLanguage());
            h.tvEventLanguage.setVisibility(Validate.isNotNull(item.getLanguage()) ? View.VISIBLE : GONE);

            if (h.iv != null) {
                Glide.with(mContext)
                        .load(item.getImageURL())
                        .centerCrop()
                        .into(h.iv);
            }

            if (h.tv_ticketCount != null) {
                h.tv_ticketCount.setText("" + item.getTicketList().size());
            }

            h.itemView.setOnClickListener(v -> {
                if (clickListener != null)
                    clickListener.onItemClick(h.getAdapterPosition(), eventList.get(h.getAdapterPosition()));
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }

    private static class EventViewHolder extends RecyclerView.ViewHolder {

        TextView tvEventTitle;
        TextView tvEventLocation;
        TextView tvEventDate;
        TextView tv_seatno;
        TextView tvEventCategory;
        TextView tvEventLanguage;
        TextView tv_time;
        TextView tvEventType;
        TextView tv_ticketCount;
        ImageView iv;

        EventViewHolder(View itemView) {
            super(itemView);
            try {
                Typeface tfRegular = TypefaceUtils.HelveticaRegular(itemView.getContext());
                Typeface tfMedium = TypefaceUtils.HelveticaMedium(itemView.getContext());
                //Typeface tfLight = TypefaceUtils.HelveticaLight(itemView.getContext());

                iv = itemView.findViewById(R.id.iv);
                tv_ticketCount = itemView.findViewById(R.id.tv_ticketCount);

                tvEventTitle = itemView.findViewById(R.id.tvEventTitle);
                tvEventTitle.setTypeface(tfMedium);
                tvEventLocation = itemView.findViewById(R.id.tvEventLocation);
                tvEventLocation.setTypeface(tfRegular);
                tvEventDate = itemView.findViewById(R.id.tvEventDate);
                tv_seatno = itemView.findViewById(R.id.tv_seatno);
                tvEventCategory = itemView.findViewById(R.id.tvEventCategory);
                tvEventLanguage = itemView.findViewById(R.id.tvEventLanguage);
                tv_time = itemView.findViewById(R.id.tv_time);
                tvEventType = itemView.findViewById(R.id.tvEventType);
                itemView.findViewById(R.id.tv_bid).setVisibility(View.INVISIBLE);
                //itemView.findViewById(R.id.tvBookingId).setVisibility(View.INVISIBLE);

                if (tvEventTitle != null) {
                    tvEventTitle.setTypeface(tfMedium);
                    tvEventLocation.setTypeface(tfRegular);
                    tvEventDate.setTypeface(tfMedium);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // LOAD MORE LISTENER
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.clickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, User_Ticket_Event history);
    }
}
