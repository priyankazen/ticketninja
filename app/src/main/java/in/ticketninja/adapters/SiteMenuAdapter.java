/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.SiteMenu;


/**
 * TicketNinja_Working(in.ticketninja.objects) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 9/6/17.
 *
 * @author Suthar Rohit
 */
public class SiteMenuAdapter extends RecyclerView.Adapter {

    private Context mContext;
    private OnItemClickListener clickListener;

    private List<SiteMenu> dataList;

    public SiteMenuAdapter(List<SiteMenu> dataList, Context context) {
        this.mContext = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_slide_menu, viewGroup, false);
        return new MenuViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        try {
            MenuViewHolder h = (MenuViewHolder) holder;
            SiteMenu item = dataList.get(i);

            h.ivMenuName.setText(item.name());
            if (Validate.isNotNull(item.image())) {
                Glide.with(mContext)
                        //.load(RestApi.ICON_IMAGE + item.image())
                        .load(item.image())
                        .into(h.ivMenuIcon);
            }

            h.itemView.setOnClickListener(v -> {
                if (clickListener != null)
                    clickListener.onItemClick(h.getAdapterPosition(), dataList.get(h.getAdapterPosition()));
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private static class MenuViewHolder extends RecyclerView.ViewHolder {

        ImageView ivMenuIcon;
        TextView ivMenuName;

        MenuViewHolder(View itemView) {
            super(itemView);
            ivMenuIcon = itemView.findViewById(R.id.iv_menuicon);
            ivMenuName = itemView.findViewById(R.id.menu_name);
        }
    }

    // LOAD MORE LISTENER
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.clickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, SiteMenu siteMenu);
    }

}
