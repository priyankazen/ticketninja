/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.objects.MoreMenu;

public class MoreMenuAdapter extends RecyclerView.Adapter {

    private List<MoreMenu> dataList;
    private OnItemClickListener clickListener;

    public MoreMenuAdapter(List<MoreMenu> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_more_menu, parent, false);
        return new MenuHolder(v);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        try {
            if (viewHolder instanceof MenuHolder) {
                MenuHolder h = (MenuHolder) viewHolder;
                h.ivItemIcon.setImageResource(dataList.get(position).getIcon());
                h.tvItemText.setText(dataList.get(position).getText());
                h.itemView.setOnClickListener(v -> {
                    if (clickListener != null)
                        clickListener.onItemClick(h.getAdapterPosition(), dataList.get(h.getAdapterPosition()));
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private class MenuHolder extends RecyclerView.ViewHolder {

        ImageView ivItemIcon;
        TextView tvItemText;

        MenuHolder(@NonNull View itemView) {
            super(itemView);
            ivItemIcon = itemView.findViewById(R.id.ivItemIcon);
            tvItemText = itemView.findViewById(R.id.tvItemText);
        }
    }

    // LOAD MORE LISTENER
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.clickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, MoreMenu menu);
    }

}
