/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import android.graphics.Paint;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.ticketninja.R;
import in.ticketninja.common.Constant;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.EventPriceCategory;

public class EventPricingAdapter1 extends RecyclerView.Adapter {

    private final String TAG = EventPricingAdapter.class.getSimpleName();
    private List<EventPriceCategory> dataList;
    private ArrayList<EventPriceCategory> selectedDataList;
    private OnItemClickListener clickListener;
    private double totalPrice = 0;
    private Context context;
    private int multiCategorySelect;
    private int previousIndex = 0;
    private int per_trans_limit;
    private int totalQty = 0;

    public EventPricingAdapter1(Context ctx, List<EventPriceCategory> dataList, int multiCategorySelect, int maxTicketNo) {
        this.dataList = dataList;
        this.context = ctx;
        this.multiCategorySelect = multiCategorySelect;
        this.selectedDataList = new ArrayList<>(dataList.size());
        this.per_trans_limit = maxTicketNo;
    }

    public ArrayList<EventPriceCategory> getSelectedList() {
        selectedDataList.clear();
        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).noOfTicket() >= 1) {
                EventPriceCategory category = dataList.get(i);
                //category.setMrp(category.getMrp() * category.noOfTicket());
                selectedDataList.add(category);
            }
        }
        return selectedDataList;
    }

    public void updateCount(int limit) {
        this.per_trans_limit = limit;
        this.totalQty = 0;
        this.totalPrice = 0;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_event_pricing1, parent, false);
        return new PricingViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        try {
            if (viewHolder instanceof PricingViewHolder) {
                EventPriceCategory category = dataList.get(position);

                PricingViewHolder h = (PricingViewHolder) viewHolder;

                if (category.noOfTicket() > 0) {
                    hideShow(h, View.VISIBLE);
                    hideShow1(h, View.GONE);
                } else {
                    hideShow(h, View.GONE);
                    hideShow1(h, View.VISIBLE);
                }

                h.tv_qty.setText(String.valueOf(category.noOfTicket()));

                h.tvItemName.setText(category.name());
                h.tvItemName.setVisibility(Validate.isNotNull(category.name()) ? View.VISIBLE : View.GONE);
                if (category.rate() == 0) {
                    h.tvItemPrice.setText(R.string.str_price_free);
                } else {
                    h.tvItemPrice.setText(String.format(Locale.getDefault(), "₹ %.2f", category.rate()));
                }
                h.tvItemDesc.setText(category.description());
                h.tvItemDesc.setVisibility(Validate.isNotNull(category.description()) ? View.VISIBLE : View.GONE);

                h.tv_add.setText(context.getResources().getString(R.string.add));

                if (category.stock() <= 0 && Validate.isNull(category.description())) {
                    h.tv_add.setEnabled(false);
                    h.tv_add.setTextColor(context.getResources().getColor(R.color.coloGray));
                    h.tv_add.setText(R.string.str_out_of_stock);
                    h.tvItemDesc.setVisibility(View.GONE);
                } else {
                    //h.tvItemDesc.setVisibility(View.GONE);
                    if (category.stock() <= 0) {
                        // h.tv_add.setText(String.format("%s\n%s", "Sold Out", category.description()));
                        h.tv_add.setEnabled(false);
                        h.tv_add.setTextColor(context.getResources().getColor(R.color.coloGray));
                        h.tv_add.setText(R.string.sold_out);
                    } else {
                        if (Validate.isNotNull(category.description())) {
                            h.tvItemDesc.setVisibility(View.VISIBLE);
                            h.tvItemDesc.setText(category.description());
                        }
                    }
                    // h.tvItemDesc.setText(category.description());
                }


                if (category.getMrp() <= category.rate()) {
                    h.tv_mrp.setVisibility(View.GONE);
                } else {
                    if (category.rate() == 0) {
                        h.tv_mrp.setText(R.string.str_price_free);
                    } else {
                        h.tv_mrp.setText(String.format(Locale.getDefault(), "₹ %.2f", category.getMrp()));
                    }
                    h.tv_mrp.setPaintFlags(h.tv_mrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                }

                if (h.tv_add.getText().equals(context.getResources().getString(R.string.add))) {
                    h.tv_add.setEnabled(true);
                    h.tv_add.setTextColor(context.getResources().getColor(R.color.themeRed));
                    h.tv_add.setOnClickListener(v -> {
                        try {
                            // Log.e(TAG, "previousIndex:" + previousIndex);
                            //Log.e(TAG, "multiCategorySelect:"  + multiCategorySelect);
                            Log.e(TAG, "per_trans_limit:" + per_trans_limit);
                            Log.e(TAG, "totalQty:" + totalQty);

                            if (totalQty == per_trans_limit) {
                                Toast.makeText(context.getApplicationContext(), R.string.stock_msg_error, Toast.LENGTH_LONG).show();
                                return;
                            }

                            if (Constant.Code.ZERO == multiCategorySelect) {
                                dataList.get(previousIndex).noOfTicket(0);
                                totalQty = 0;
                                totalPrice = 0;
                            }

                            hideShow(h, View.VISIBLE);
                            hideShow1(h, View.GONE);
                            category.noOfTicket(1);
                            h.tv_qty.setText("1");
                            // Log.e("totalQty", "totalQty: " + totalQty);
                            totalQty = totalQty + 1;
                            totalPrice = totalPrice + category.rate();
                            category.setTotalRate(totalPrice);
                            // Log.e(TAG, "size:" + dataList.size());
                            previousIndex = h.getAdapterPosition();
                            // Log.e(TAG, "currentIndex:" + previousIndex);
                            if (clickListener != null) {
                                clickListener.onItemClick(position, totalQty, dataList.get(h.getAdapterPosition()));
                            }
                            notifyDataSetChanged();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    });
                }

                h.tv_plus.setOnClickListener(v -> {

                    if (totalQty == per_trans_limit) {
                        Toast.makeText(context.getApplicationContext(), R.string.stock_msg_error, Toast.LENGTH_LONG).show();
                        return;
                    }

                    if (category.noOfTicket() < 10) {
                        int qty = category.noOfTicket() + 1;
                        //Log.e("totalQty", "plustotalQty: " + totalQty);
                        category.noOfTicket(qty);
                        h.tv_qty.setText(String.valueOf(qty));
                        totalQty = totalQty + 1;
                        totalPrice = totalPrice + category.rate();
                        category.setTotalRate(totalPrice);
                        if (clickListener != null)
                            clickListener.onItemClick(position, totalQty, dataList.get(h.getAdapterPosition()));
                    }
                });

                h.tv_minus.setOnClickListener(v -> {
                    if (category.noOfTicket() == 1) {
                        hideShow(h, View.GONE);
                        hideShow1(h, View.VISIBLE);
                    }
                    if (category.noOfTicket() > 0) {
                        int qty = category.noOfTicket() - 1;
                        category.noOfTicket(qty);
                        h.tv_qty.setText(String.valueOf(qty));
                        totalQty = totalQty - 1;
                        totalPrice = totalPrice - category.rate();
                        category.setTotalRate(totalPrice);
                        if (clickListener != null)
                            clickListener.onItemClick(position, totalQty, dataList.get(h.getAdapterPosition()));
                    }
                });

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void hideShow(PricingViewHolder h, int b) {
        h.tv_plus.setVisibility(b);
        h.tv_minus.setVisibility(b);
        h.tv_qty.setVisibility(b);
    }

    private void hideShow1(PricingViewHolder h, int b) {
        h.tv_add.setVisibility(b);
    }

    private static class PricingViewHolder extends RecyclerView.ViewHolder {

        TextView tvItemName;
        TextView tvItemDesc;
        TextView tvItemPrice;
        TextView tv_mrp;
        ImageView tv_plus;
        ImageView tv_minus;
        TextView tv_add;
        TextView tv_qty;

        PricingViewHolder(@NonNull View itemView) {
            super(itemView);
            try {
                tvItemName = itemView.findViewById(R.id.tvItemName);
                tvItemDesc = itemView.findViewById(R.id.tvItemDesc);
                tv_mrp = itemView.findViewById(R.id.tv_mrp);
                tvItemPrice = itemView.findViewById(R.id.tvItemPrice);
                tv_plus = itemView.findViewById(R.id.tv_plus);
                tv_minus = itemView.findViewById(R.id.tv_minus);
                tv_add = itemView.findViewById(R.id.tv_add);
                tv_qty = itemView.findViewById(R.id.tv_qty);

                // Typeface tfMedium = TypefaceUtils.HelveticaMedium(itemView.getContext());
                // tvItemName.setTypeface(tfMedium);
                // tvItemDesc.setTypeface(tfMedium);
                //tvItemPrice.setTypeface(tfMedium);
                //tv_plus.setTypeface(tfMedium);
                //tv_minus.setTypeface(tfMedium);
                //tv_add.setTypeface(tfMedium);
                //tv_qty.setTypeface(tfMedium);
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    public void setDataList(List<EventPriceCategory> dataList) {
        this.dataList = dataList;
        totalQty = 0;
        totalPrice = 0;
        previousIndex = 0;
    }

    // LOAD MORE LISTENER
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.clickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, int count, EventPriceCategory priceCategory);
    }

}
