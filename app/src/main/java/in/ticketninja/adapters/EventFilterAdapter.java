/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import in.ticketninja.R;
import in.ticketninja.objects.MyEvent;

/**
 * TicketNinja_Working(in.ticketninja.objects) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 9/6/17.
 *
 * @author Suthar Rohit
 */
public class EventFilterAdapter extends RecyclerView.Adapter {

    private ArrayList<MyEvent> daysList;
    private OnEventSelectedListener listener;
    private int currentSelection = -1;

    public EventFilterAdapter(ArrayList<MyEvent> daysList) {
        this.daysList = daysList;
    }

    public void updatePosition(){
        try {
            currentSelection = -1;
            for (MyEvent s : daysList) {
                s.isSelected = false;
            }
            notifyDataSetChanged();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public ArrayList<MyEvent> getEventArray(){
        return daysList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_event_filter_view, viewGroup, false);
        return new EventViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        try {
            EventViewHolder h = (EventViewHolder) holder;
            MyEvent myEvent = daysList.get(i);
            String item = myEvent.getCollection();
            if (!TextUtils.isEmpty(item.trim())){
                h.llEventFilter.setVisibility(View.VISIBLE);
                h.tvEventCollection.setText(item);
               // h.itemView.setSelected(currentSelection == h.getAdapterPosition());
                //h.tvEventCollection.setSelected(currentSelection == h.getAdapterPosition());

                h.itemView.setSelected(myEvent.isSelected());
                h.tvEventCollection.setSelected(myEvent.isSelected());

                h.itemView.setOnClickListener(view -> {

                    try {
                       // if (currentSelection != h.getAdapterPosition()) {
                       if (!daysList.get(h.getAdapterPosition()).isSelected()) {
                            //currentSelection = h.getAdapterPosition();
                            if (listener != null) {
                                for (MyEvent s : daysList) {
                                    s.isSelected = false;
                                }
                                MyEvent selectEvent =  daysList.get(h.getAdapterPosition());
                                selectEvent.isSelected = true;
                                listener.onEventSelected(h.getAdapterPosition(),selectEvent);
                            }
                            notifyDataSetChanged();
                        }else {
                            currentSelection = -1;
                            for (MyEvent s : daysList) {
                                s.isSelected = false;
                            }
                            listener.onEventSelected(h.getAdapterPosition(), null);
                            notifyDataSetChanged();
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }


                });
            }else {
                h.llEventFilter.setVisibility(View.GONE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return daysList.size();
    }

    private class EventViewHolder extends RecyclerView.ViewHolder {
        TextView tvEventCollection;
        LinearLayout llEventFilter;

        EventViewHolder(View itemView) {
            super(itemView);
            tvEventCollection = itemView.findViewById(R.id.tvEventCollection);
            llEventFilter = itemView.findViewById(R.id.llEventFilter);
        }
    }

    public void setOnEventSelectedListener(OnEventSelectedListener listener) {
        this.listener = listener;
    }

    public interface OnEventSelectedListener {
        void onEventSelected(int position, MyEvent selectEvent);
    }
}
