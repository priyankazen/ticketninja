package in.ticketninja.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.viewpager.widget.PagerAdapter;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import in.ticketninja.R;
import in.ticketninja.common.TouchImageView;
import in.ticketninja.common.ZoomableImageView;
import in.ticketninja.objects.EventGalleryList;

public class EventGalleryPagerAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<EventGalleryList> galleryList;

    public EventGalleryPagerAdapter(Context context, ArrayList<EventGalleryList> eventGalleryLists) {
        mContext = context;
        this.galleryList = eventGalleryLists;
    }

    @Override
    public int getCount() {
        return galleryList.size();
    }

    @NotNull
    @Override
    public Object instantiateItem(@NotNull ViewGroup collection, int position) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_vp_gallery_iv, collection, false);

       TouchImageView iv =  view.findViewById(R.id.ivGallery);

        EventGalleryList list = galleryList.get(position);
        try {
            Glide.with(mContext)
                    .load(list.getImage_url())
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(iv);
        }catch (Exception e){
            e.printStackTrace();
        }
        collection.addView(view);
        return view;
    }

    @Override
    public void destroyItem(@NotNull ViewGroup collection, int position, @NotNull Object view) {
        collection.removeView((View) view);
    }

    @Override
    public boolean isViewFromObject(@NotNull View view, @NotNull Object object) {
        return view == object;
    }

}