/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import in.ticketninja.R;
import in.ticketninja.common.TypefaceUtils;

/**
 * TicketNinja_Working(in.ticketninja.fragments) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 10/7/17.
 *
 * @author Suthar Rohit
 */
public class ExpandebleAdapter extends BaseExpandableListAdapter {
    private Context _context;
    private List<String> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<String, String> _listDataChild;

    public ExpandebleAdapter(Context context, List<String> listDataHeader,
                             HashMap<String, String> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition));
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        final String childText = (String) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater infalInflater = LayoutInflater.from(_context);
            convertView = infalInflater.inflate(R.layout.custom_expandeble_item, null);
        }
        //Typeface tfMedium = TypefaceUtils.HelveticaMedium(_context);
        Typeface tfReguler = TypefaceUtils.HelveticaRegular(_context);


        TextView txtListChild = convertView.findViewById(R.id.lblListItem);
        txtListChild.setTypeface(tfReguler);

        txtListChild.setText(childText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater infalInflater = LayoutInflater.from(_context);
            convertView = infalInflater.inflate(R.layout.custom_expandeble_group, null);
        }

        TextView lblListHeader = convertView.findViewById(R.id.lblListHeader);

        if (isExpanded) {
            lblListHeader.setSelected(true);
          /*  lblListHeader.animate().scaleY(-1f).start();
            lblListHeader.setRotation(1800);*/

        } else {
            lblListHeader.setSelected(false);
        /*    lblListHeader.animate().scaleY(1f).start();
            lblListHeader.setRotation(0);*/
        }


        lblListHeader.setText(headerTitle);
        Typeface tfMedium = TypefaceUtils.HelveticaMedium(_context);
        lblListHeader.setTypeface(tfMedium);

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
