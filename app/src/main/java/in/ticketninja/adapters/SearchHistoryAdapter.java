/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.objects.Search_db;

/**
 * TicketNinja_Working(in.ticketninja.objects) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 13/6/17.
 *
 * @author Suthar Rohit
 */
public class SearchHistoryAdapter extends RecyclerView.Adapter {
    private List<Search_db> dataList;
    private setOnItemClickListener clickListener;

    public SearchHistoryAdapter(List<Search_db> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_search_history, viewGroup, false);
        return new SearchViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder itemView, int position) {
        try {
            SearchViewHolder holder = (SearchViewHolder) itemView;

            Search_db item = dataList.get(position);
            holder.tv_keyword.setText(item.getKeyword());
            holder.itemView.setOnClickListener(v -> {
                if (clickListener != null)
                    clickListener.onItemClick(dataList.get(position).getKeyword());
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private static class SearchViewHolder extends RecyclerView.ViewHolder {
        TextView tv_keyword;

        SearchViewHolder(View itemView) {
            super(itemView);
            tv_keyword = itemView.findViewById(R.id.tv_keyword);
        }
    }

    public interface setOnItemClickListener {
        void onItemClick(String keyword);
    }

    public void setOnItemClickListener(setOnItemClickListener listener) {
        this.clickListener = listener;
    }

}
