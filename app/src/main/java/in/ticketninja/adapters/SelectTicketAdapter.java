/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.graphics.Paint;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.ticketninja.R;
import in.ticketninja.objects.EventPriceCategory;

/**
 * TicketNinja_Working(in.ticketninja.adapters) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 12/8/17.
 *
 * @author Suthar Rohit
 */
public class SelectTicketAdapter extends RecyclerView.Adapter {

    private static final int MAXIMUM_TICKET = 10;

    private List<EventPriceCategory> dataList;
    private ArrayList<EventPriceCategory> selectedDataList;
    private OnTicketValueChangeListener changeListener;
    private int totalCount;
    private int totalTicketCount = 0;

    public SelectTicketAdapter(List<EventPriceCategory> dataList,int totalCount) {
        this.dataList = dataList;
        selectedDataList = new ArrayList<>(dataList.size());
        this.totalCount = totalCount;
    }

    public ArrayList<EventPriceCategory> getSelectedList() {
        selectedDataList.clear();
        for (int i = 0; i < dataList.size(); i++) {
            if (dataList.get(i).noOfTicket() >= 1) {
                EventPriceCategory category = dataList.get(i);
                //category.setMrp(category.getMrp() * category.noOfTicket());
                selectedDataList.add(category);
            }
        }
        return selectedDataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_selectdate_and_time, viewGroup, false);
        return new EventViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        try {
            EventViewHolder h = (EventViewHolder) holder;
            EventPriceCategory item = dataList.get(i);

            if (item.noOfTicket() == 0) {
                h.ll_ticketno.setSelected(false);
                h.tv_ticket.setSelected(false);
            } else {
                h.ll_ticketno.setSelected(true);
                h.tv_ticket.setSelected(true);
            }

            h.tv_ticket.setText(String.valueOf(item.noOfTicket()));
            h.tv_category.setText(item.name());
            h.tv_dec.setText(item.description());

            if (item.stock()==0){
                h.tvStock.setVisibility(View.VISIBLE);
            }else {
                h.tvStock.setVisibility(View.GONE);
            }

            if (item.noOfTicket() <= 0) {
                if (item.rate() == 0) {
                    h.tv_price.setText(R.string.str_price_free);
                } else {
                    h.tv_price.setText(String.format(Locale.getDefault(), "  ₹ %.2f", item.rate()));
                }
                h.tv_total.setVisibility(View.GONE);

            } else {

                h.tv_total.setVisibility(View.VISIBLE);

                //h.tv_price.setText(String.format(Locale.getDefault(), " x ₹ %.2f = ₹ %.2f", item.rate(), item.noOfTicket() * item.rate()));
                if (item.rate() == 0) {
                    h.tv_price.setText(R.string.str_price_free);
                } else {
                    h.tv_price.setText(String.format(Locale.getDefault(), "  ₹ %.2f", item.rate()));
                }
                if ((item.noOfTicket() * item.rate()) == 0) {
                    h.tv_total.setText(String.format(" = %s", h.tv_total.getContext().getString(R.string.str_price_free)));
                } else {
                    h.tv_total.setText(String.format(Locale.getDefault(), " = ₹ %.2f", item.noOfTicket() * item.rate()));
                }
            }


            if (item.getMrp() <= item.rate()) {
                h.tvmrp.setVisibility(View.GONE);

            } else {
                if (item.getMrp() == 0) {
                    h.tvmrp.setText(R.string.str_price_free);
                } else {
                    h.tvmrp.setText(String.format(Locale.getDefault(), "₹ %.2f", item.getMrp()));
                }
                h.tvmrp.setPaintFlags(h.tvmrp.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            }


            h.iv_plus.setOnClickListener(v -> {

                Log.e("Tag","item.stock()="+item.stock());
                Log.e("Tag","item.noOfTicket()="+item.noOfTicket());
                Log.e("Tag","totalTicketCount="+ totalTicketCount);
                Log.e("Tag","apiTotalCount="+ totalCount);

                if (totalTicketCount == totalCount){
                    Toast.makeText(v.getContext(), R.string.stock_msg_error,Toast.LENGTH_LONG).show();
                    return;
                }

                if (item.stock() <= item.noOfTicket()){
                    Toast.makeText(v.getContext(), R.string.stock_msg_error,Toast.LENGTH_LONG).show();
                    return;
                }

                if (item.noOfTicket() >= 0 && item.noOfTicket() < MAXIMUM_TICKET) {
                    int noTicket = item.noOfTicket() + 1;
                    totalTicketCount += 1;
                    h.ll_ticketno.setSelected(true);
                    h.tv_ticket.setSelected(true);
                    item.noOfTicket(noTicket);
                    item.amount(noTicket * item.rate());
                    item.setTotalMRP(noTicket * item.getMrp());

                    notifyDataSetChanged();

                    if (changeListener != null)
                        changeListener.onTicketValueChange(h.getAdapterPosition());
                }
            });

            h.iv_minus.setOnClickListener(v -> {

                if (item.noOfTicket() > 0) {
                    int noTicket = item.noOfTicket() - 1;
                    totalTicketCount -= 1;
                    h.ll_ticketno.setSelected(true);
                    h.tv_ticket.setSelected(true);
                    item.noOfTicket(noTicket);
                    item.amount(item.noOfTicket() * item.rate());
                    item.setTotalMRP(noTicket * item.getMrp());
                    notifyDataSetChanged();

                    if (changeListener != null)
                        changeListener.onTicketValueChange(h.getAdapterPosition());
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    private class EventViewHolder extends RecyclerView.ViewHolder {

        TextView tv_ticket, tv_price, tv_category, tv_dec, tv_total, tvmrp,tvStock;
        LinearLayout ll_ticketno, ll_price;
        ImageView iv_plus, iv_minus;

        EventViewHolder(View itemView) {
            super(itemView);
            try {
                ll_ticketno = itemView.findViewById(R.id.ll_ticketno);
                tv_ticket = itemView.findViewById(R.id.tv_ticket);
                tv_price = itemView.findViewById(R.id.tv_price);
                tv_category = itemView.findViewById(R.id.tv_category);
                tv_dec = itemView.findViewById(R.id.tv_dec);
                iv_plus = itemView.findViewById(R.id.iv_plus);
                iv_minus = itemView.findViewById(R.id.iv_minus);
                tv_total = itemView.findViewById(R.id.tv_total);
                tvmrp = itemView.findViewById(R.id.tvmrp);
                ll_price = itemView.findViewById(R.id.ll_price);
                tvStock = itemView.findViewById(R.id.tvStock);
            }catch (Exception e){
                e.printStackTrace();
            }

        }
    }

    // LOAD MORE LISTENER
    public void setOnItemClickListener(OnTicketValueChangeListener listener) {
        this.changeListener = listener;
    }

    public interface OnTicketValueChangeListener {
        void onTicketValueChange(int position);
    }

}
