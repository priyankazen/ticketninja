/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import in.ticketninja.R;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.objects.UserCouponCode;

public class EventCouponCodeAdapter extends RecyclerView.Adapter {
    //private static final String TAG = EventCouponCodeAdapter.class.getSimpleName();
    private ArrayList<UserCouponCode> dataList;
    private Context context;

    public interface CouponCodeItemClickEvent {
        void couponCodeItemClickEvent(UserCouponCode userCouponCode, int position);
    }

    private CouponCodeItemClickEvent couponCodeItemClickEvent;

    public void setClickEvent(CouponCodeItemClickEvent clickEvent) {
        this.couponCodeItemClickEvent = clickEvent;
    }

    public EventCouponCodeAdapter(Context context, ArrayList<UserCouponCode> dataList) {
        this.dataList = dataList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_coupon_code, parent, false);
        return new MenuHolder(v);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        try {
            Log.e("TAG",""+dataList);
            MenuHolder h = (MenuHolder) viewHolder;
            UserCouponCode userCouponCode = dataList.get(position);
            try {
              h.txtCode.setText(String.format("%s", userCouponCode.getCoupon_code()));
              h.txtCodeDetail.setText(userCouponCode.getCoupon_code_detail());
              String date = DateTimeUtils.changeDateTimeFormat(userCouponCode.getExpiry_date(),DateTimeUtils.SERVER_FORMAT_DATE,DateTimeUtils.FORMAT_DATE);
              h.txtDate.setText(String.format("%s", date));
              h.txtCodeDes.setText(userCouponCode.getCoupon_code_label());
            }catch (Exception e){
                e.printStackTrace();
            }

            h.itemView.setOnClickListener(view -> {
                if (couponCodeItemClickEvent != null) {
                    couponCodeItemClickEvent.couponCodeItemClickEvent(dataList.get(h.getAdapterPosition()),h.getAdapterPosition());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class MenuHolder extends RecyclerView.ViewHolder {

        TextView txtCode,txtCodeDes,txtDate,txtCodeDetail;

        MenuHolder(@NonNull View itemView) {
            super(itemView);
            try {
                txtCode = itemView.findViewById(R.id.txtCode);
                txtCodeDes = itemView.findViewById(R.id.txtCodeDes);
                txtDate = itemView.findViewById(R.id.txtDate);
                txtCodeDetail = itemView.findViewById(R.id.txtCodeDetail);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
