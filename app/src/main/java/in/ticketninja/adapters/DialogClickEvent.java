package in.ticketninja.adapters;

public interface DialogClickEvent {

    void openImagesView(String type);

}
