/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.activity.EventDetailActivity;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.MyEvent;

import static android.view.View.GONE;

/**
 * TicketNinja_Working(in.ticketninja.objects) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 9/6/17.
 *
 * @author Suthar Rohit
 */
@SuppressWarnings("ALL")
public class EventsAdapter extends RecyclerView.Adapter {
    private final String TAG = EventsAdapter.class.getSimpleName();
    private CommonClass CC;
    private Context mContext;
    private List<MyEvent> eventList;
    private String type;

    OnBottomReachedListener onBottomReachedListener;

    public interface OnBottomReachedListener {
        void onBottomReached(int position);
    }

    public void setOnBottomReachedListener(OnBottomReachedListener onBottomReachedListener) {
        this.onBottomReachedListener = onBottomReachedListener;
    }

    public EventsAdapter(Context context, List<MyEvent> eventList, String _type) {
        this.mContext = context;
        this.eventList = eventList;
        this.CC = new CommonClass(context);
        this.type = _type;
    }

    public void addArrayItem(int posStart, List<MyEvent> list) {
        this.eventList.addAll(list);
        Log.e(TAG, "posStart: " + posStart + " eventList : " + eventList.size());
        notifyItemRangeChanged(posStart, this.eventList.size());
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_event_list, viewGroup, false);
        return new EventViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        try {
            EventViewHolder h = (EventViewHolder) holder;

            if (Constant.WALLET == type) {
                h.eventListRl.getLayoutParams().width = Utility.dpToPx(mContext,280);
            }

            MyEvent item = eventList.get(i);

            Glide.with(mContext)
                    .load(item.getMainImageUrl())
                    //.centerCrop()
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(h.iv_poster);

            h.tvTitle.setText(item.getEvent_name());

            if ((item.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY)
                    || item.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW))
                    && Validate.isDateNotNull(item.getEvent_date()) && !item.getEvent_date().equals(item.getDateTo())) {
                SpannableStringBuilder dateBuilder = new SpannableStringBuilder();
                if (Validate.isDateNotNull(item.getEvent_date())) {
                    String monthFrom = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "MMM");
                    String dateFrom = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "dd");
                    dateBuilder.append(monthFrom.toUpperCase()).append(" ").append(dateFrom).append("\nto\n");
                    dateBuilder.setSpan(new ForegroundColorSpan(Color.RED), 0, monthFrom.length(), 0);
                }
                if (Validate.isDateNotNull(item.getDateTo())) {
                    int toStartPos = dateBuilder.length();
                    String monthTo = DateTimeUtils.changeDateTimeFormat(item.getDateTo(), DateTimeUtils.SERVER_FORMAT_DATE, "MMM");
                    String dateTo = DateTimeUtils.changeDateTimeFormat(item.getDateTo(), DateTimeUtils.SERVER_FORMAT_DATE, "dd");
                    dateBuilder.append(monthTo.toUpperCase()).append(" ").append(dateTo);
                    dateBuilder.setSpan(new ForegroundColorSpan(Color.RED), toStartPos, toStartPos + monthTo.length(), 0);
                }
                h.tvDate.setText(dateBuilder);
                h.tvDate.setVisibility(View.VISIBLE);

            } else if (item.getDayType().equalsIgnoreCase(Constant.EventDayType.PERPETUAL)) {
                h.tvDate.setVisibility(View.GONE);

            } else if ((item.getDayType().equalsIgnoreCase(Constant.EventDayType.SINGLE_DAY) || item.getEvent_date().equals(item.getDateTo()))
                    && Validate.isDateNotNull(item.getEvent_date())) {
                String month = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "MMM").toUpperCase();
                String date = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "dd");
                String day = DateTimeUtils.changeDateTimeFormat(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, "EEE").toUpperCase();
                SpannableStringBuilder dateBuilder = new SpannableStringBuilder();
                dateBuilder.append(month).append("\n").append(date).append("\n").append(day);
                dateBuilder.setSpan(new ForegroundColorSpan(Color.RED), 0, month.length(), 0);
                dateBuilder.setSpan(new RelativeSizeSpan(1f), 0, month.length(), 0);
                dateBuilder.setSpan(new RelativeSizeSpan(0.9f), month.length(), month.length() + day.length() + 1, 0);
                dateBuilder.setSpan(new RelativeSizeSpan(0.85f), month.length() + day.length() + 1, dateBuilder.length(), 0);

                h.tvDate.setText(dateBuilder);
                h.tvDate.setVisibility(View.VISIBLE);

            } else {
                h.tvDate.setVisibility(View.GONE);
            }

            if (Validate.isNotNull(item.getEvent_time()))
                h.tvTime.setText(item.getDayType().equalsIgnoreCase(Constant.EventDayType.PERPETUAL)
                        ? item.getEvent_time() : String.format("%s %s", item.getEvent_time(), Constant.ONWARDS));

            h.tvCategory.setText(item.getCategory());
            h.tvCategory.setVisibility(Validate.isNotNull(item.getCategory()) ? View.VISIBLE : GONE);
            h.tvLanguage.setText(item.getLanguage());
            h.tvLanguage.setVisibility(Validate.isNotNull(item.getLanguage()) ? View.VISIBLE : GONE);

       /* if (Validate.isNotNull(item.getCity())) {
            h.tvLocation.setText(String.format("%s, %s", item.getLocation_name(), item.getCity()));
        }*/

            if (Validate.isNotNull(item.getCity()) && Validate.isNotNull(item.getLocation_name())) {
                h.tvLocation.setText(String.format("%s, %s", item.getLocation_name(), item.getCity()));
            }else if (Validate.isNotNull(item.getLocation_name()) && Validate.isNotNull(item.getState())) {
                h.tvLocation.setText(String.format("%s, %s", item.getLocation_name(), item.getState()));
            }else if (Validate.isNotNull(item.getLocation_name())) {
                //h.tvLocation.setText(String.format("%s, %s", item.getLocation_name(), item.getState()));
                h.tvLocation.setText(String.format(item.getLocation_name()));
            }

            h.tvLocation.setOnClickListener(v -> CommonClass.openMap(mContext, item.getEvent_name(), item.getLatitude(), item.getLongitude()));

        /*if (item.isInviteOnly() && item.isInviteUsed()) {
            h.tvInviteTag.setText(R.string.event_list_invite_used_label);
            h.tvInviteTag.setVisibility(View.VISIBLE);
            h.tvInviteTag.setSelected(true);

        } else if (item.isInviteOnly() && item.isInviteAvailable()) {
            h.tvInviteTag.setText(R.string.event_list_invite_only_label);
            h.tvInviteTag.setVisibility(View.VISIBLE);
            h.tvInviteTag.setSelected(false);

        } else if (item.isInviteOnly() && item.isInviteLeft()) {
            h.tvInviteTag.setText(mContext.getString(R.string.event_list_invite_remaining_label, item.getInviteStock()));
            h.tvInviteTag.setVisibility(View.VISIBLE);
            h.tvInviteTag.setSelected(false);

        } else {
            h.tvInviteTag.setText(R.string.event_list_invite_used_label);
            h.tvInviteTag.setVisibility(View.GONE);
        }*/

            if (Constant.Code.ONE == item.isInviteOnly() && Constant.InviteStatus.USED.equals(item.getInvite_status())) {
                h.tvInviteTag.setText(R.string.event_list_invite_used_label);
                h.tvInviteTag.setVisibility(View.VISIBLE);
                h.tvInviteTag.setSelected(true);

            } else if (Constant.Code.ONE == item.isInviteOnly() && Constant.InviteStatus.AVAILABLE.equals(item.getInvite_status())) {
                h.tvInviteTag.setText(R.string.event_list_invite_only_label);
                h.tvInviteTag.setVisibility(View.VISIBLE);
                h.tvInviteTag.setSelected(false);

            } else if (Constant.Code.ONE == item.isInviteOnly() && Constant.InviteStatus.LEFT.equals(item.getInvite_status())) {
                h.tvInviteTag.setText(mContext.getString(R.string.event_list_invite_remaining_label, item.getInviteStock()));
                h.tvInviteTag.setVisibility(View.VISIBLE);
                h.tvInviteTag.setSelected(false);

            } else {
                h.tvInviteTag.setText(R.string.event_list_invite_used_label);
                h.tvInviteTag.setVisibility(View.GONE);
            }


        /*if (item.isInviteOnly() && Constant.InviteStatus.USED.equals(item.getInvite_status())) {
            h.tvInviteTag.setText(R.string.event_list_invite_used_label);
            h.tvInviteTag.setVisibility(View.VISIBLE);
            h.tvInviteTag.setSelected(true);

        } else if (item.isInviteOnly() && Constant.InviteStatus.AVAILABLE.equals(item.getInvite_status())) {
            h.tvInviteTag.setText(R.string.event_list_invite_only_label);
            h.tvInviteTag.setVisibility(View.VISIBLE);
            h.tvInviteTag.setSelected(false);

        } else if (item.isInviteOnly() && Constant.InviteStatus.LEFT.equals(item.getInvite_status())) {
            h.tvInviteTag.setText(mContext.getString(R.string.event_list_invite_remaining_label, item.getInviteStock()));
            h.tvInviteTag.setVisibility(View.VISIBLE);
            h.tvInviteTag.setSelected(false);

        } else {
            h.tvInviteTag.setText(R.string.event_list_invite_used_label);
            h.tvInviteTag.setVisibility(View.GONE);
        }*/

            //h.btnBuyTicket.setText(Constant.Code.ONE == item.isRsvp() ||item.isInviteRSVP() ? R.string.event_btn_rsvp : R.string.event_btn_buy_ticket);
            h.btnBuyTicket.setText(Constant.Code.ONE == item.isRsvp() || Constant.Code.ONE == item.isInviteRSVP() ? R.string.event_btn_rsvp : R.string.event_btn_buy_ticket);
            h.btnBuyTicket.setClickable(false);
            h.itemView.setOnClickListener((View v) -> {
                if (CC.isOnline()) {
                    Intent intDetail = new Intent(v.getContext(), EventDetailActivity.class);
                    intDetail.putExtra(Constant.ScreenExtras.EVENT_DATA, item);
                    mContext.startActivity(intDetail);
                /*if (Build.VERSION_CODES.JELLY_BEAN <= Build.VERSION.SDK_INT) {
                    Pair<View, String> p1 = Pair.create(h.iv_poster, "eventPoster");
                    Pair<View, String> p2 = Pair.create(h.tvTitle, "eventTitle");
                    //Pair<View, String> p3 = Pair.create((View) h.btnBuyTicket, "eventBuyNow");
                    //Pair<View, String> p4 = Pair.create((View) h.tvInviteTag, "eventInviteOnly");
                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) mContext, p1, p2);
                    mContext.startActivity(intDetail, options.toBundle());
                } else {
                    mContext.startActivity(intDetail);
                }*/
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });


            //adding by priyanka
            if (Constant.Code.ONE == item.getIs_exclusive()) {
                h.txtExc.setVisibility(View.VISIBLE);
            } else {
                h.txtExc.setVisibility(GONE);
            }

       /* Log.e(TAG,"i:" + i +", size:" +eventList.size());
        if (i == eventList.size() - 1) {
            if (onBottomReachedListener != null) {
                onBottomReachedListener.onBottomReached(i);
            }
        }*/

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return eventList.size();
    }


    private class EventViewHolder extends RecyclerView.ViewHolder {
        ImageView iv_poster;
        TextView tvTitle, tvDate, tvCategory, tvLanguage, tvLocation, tvTime, txtExc;
        TextView tvInviteTag;
        Button btnBuyTicket;
        RelativeLayout eventListRl;

        EventViewHolder(View itemView) {
            super(itemView);
            try {
                iv_poster = itemView.findViewById(R.id.ivEventPoster);
                tvTitle = itemView.findViewById(R.id.tvEventTitle);
                tvDate = itemView.findViewById(R.id.tvEventDate);
                tvCategory = itemView.findViewById(R.id.tvEventCategory);
                tvLanguage = itemView.findViewById(R.id.tvEventLanguage);
                tvLocation = itemView.findViewById(R.id.tvEventLocation);
                tvTime = itemView.findViewById(R.id.tvTime);
                btnBuyTicket = itemView.findViewById(R.id.btnEventBuyTicket);
                tvInviteTag = itemView.findViewById(R.id.tvInviteTag);
                txtExc = itemView.findViewById(R.id.txtExc);
                eventListRl = itemView.findViewById(R.id.eventListRl);
            }catch (Exception e){
                e.printStackTrace();
            }


        }
    }
}
