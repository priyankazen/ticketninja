/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

import in.ticketninja.R;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.objects.MultipleDay;

/**
 * TicketNinja_Working(in.ticketninja.objects) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 9/6/17.
 *
 * @author Suthar Rohit
 */
public class EventPricingDateChooserAdapter extends RecyclerView.Adapter {

    private List<MultipleDay> daysList;
    private OnDateSelectedListener listener;
    private boolean isSeasonPass;

    private int currentSelection = 0;

    public EventPricingDateChooserAdapter(List<MultipleDay> daysList, boolean isSeasonPass) {
        this.daysList = daysList;
        this.isSeasonPass = isSeasonPass;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_event_pricing_date_chooser, viewGroup, false);
        return new EventViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int i) {
        try {
            EventViewHolder h = (EventViewHolder) holder;
            MultipleDay item = daysList.get(i);

            h.itemView.setVisibility(Constant.Code.ONE == item.isSeasonPass() ? View.GONE : View.VISIBLE);
            h.tvDateDayName.setVisibility(Constant.Code.ONE == item.isSeasonPass() ? View.GONE : View.VISIBLE);
            h.tvDateDay.setVisibility(Constant.Code.ONE == item.isSeasonPass() ? View.GONE : View.VISIBLE);
            h.tvDateYear.setVisibility(Constant.Code.ONE == item.isSeasonPass() ? View.GONE : View.VISIBLE);

            Date date = DateTimeUtils.stringToDate(item.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE);
            //h.tvDateDayName.setText(DateTimeUtils.dateToString(date, "EEE"));
            // h.tvDateDay.setText(DateTimeUtils.dateToString(date, "dd"));


            h.tvDateDayName.setText(DateTimeUtils.dateToString(date, "MMM"));
            h.tvDateDay.setText(DateTimeUtils.dateToString(date, "dd"));
            h.tvDateYear.setText(DateTimeUtils.dateToString(date, "EEE"));

            if (!isSeasonPass) {
                h.itemView.setSelected(currentSelection == h.getAdapterPosition());
                h.tvDateDayName.setSelected(currentSelection == h.getAdapterPosition());
                h.tvDateDay.setSelected(currentSelection == h.getAdapterPosition());
                h.tvDateYear.setSelected(currentSelection == h.getAdapterPosition());
            }

            h.itemView.setEnabled(!isSeasonPass);
            h.tvDateDayName.setEnabled(!isSeasonPass);
            h.tvDateDay.setEnabled(!isSeasonPass);
            h.tvDateYear.setEnabled(!isSeasonPass);

            h.itemView.setOnClickListener(view -> {
                //currentSelection = currentSelection == h.getAdapterPosition() ? -1 : h.getAdapterPosition();
                if (currentSelection != h.getAdapterPosition()) {
                    currentSelection = h.getAdapterPosition();
                    if (listener != null) {
                        listener.onDateSelected(h.getLayoutPosition(), item);
                    }
                    notifyDataSetChanged();
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return daysList.size();
    }

    private class EventViewHolder extends RecyclerView.ViewHolder {
        TextView tvDateDayName, tvDateDay,tvDateYear;

        EventViewHolder(View itemView) {
            super(itemView);
            tvDateDayName = itemView.findViewById(R.id.tvDateDayName);
            tvDateDay = itemView.findViewById(R.id.tvDateDay);
            tvDateYear = itemView.findViewById(R.id.tvDateYear);

        }
    }

    public void setSeasonPass(boolean seasonPass, int selection) {
        isSeasonPass = seasonPass;
        if (seasonPass) {
            currentSelection = -1;
        } else {
            currentSelection = selection;
        }
    }

    public void setOnDateSelectedListener(OnDateSelectedListener listener) {
        this.listener = listener;
    }

    public interface OnDateSelectedListener {

        void onDateSelected(int position, MultipleDay day);

    }
}
