/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.Notifications;

/**
 * TicketNinja_Working(in.ticketninja.objects) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 9/6/17.
 *
 * @author Suthar Rohit
 */
public class NotificationListAdapter extends RecyclerView.Adapter implements View.OnCreateContextMenuListener {

    private final int MENU_MARK_READ = 1001;
    private final int MENU_DELETE = 1002;

    private Context mContext;

    private List<Notifications> dataList;
    private OnItemClickListener listener;

    public NotificationListAdapter(Context context, List<Notifications> dataList) {
        this.mContext = context;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_notification_list, viewGroup, false);
        return new EventViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        try {
            EventViewHolder h = (EventViewHolder) holder;

            Notifications item = dataList.get(position);

            Glide.with(mContext)
                    .load(item.getPosterImageURL())
                    .skipMemoryCache(false)
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(h.iv_poster);

            h.tvTitle.setText(item.getMessage());

            if (Validate.isNotNull(item.getNotificationDate())) {
                String dateTime = DateTimeUtils.getRelativeTimeSpanString(item.getNotificationDate());
                h.tvTime.setText(dateTime);
            }

            //h.llNotificationLayout.setSelected(!item.isRead());
            h.llNotificationLayout.setSelected(Constant.Code.ONE!=item.isRead());
            h.itemView.setTag(h.getAdapterPosition());

            h.itemView.setOnClickListener(view -> {
                if (listener != null) {
                    listener.onItemClick(h.getLayoutPosition(), item);
                }
            });
            h.itemView.setOnCreateContextMenuListener(this);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
        int position = (int) view.getTag();
        MenuItem menuRead = contextMenu.add(position, MENU_MARK_READ, 0, "Mark as Read");
        MenuItem menuDelete = contextMenu.add(position, MENU_DELETE, 0, "Delete");
        menuRead.setOnMenuItemClickListener(onMenuClick);
        menuDelete.setOnMenuItemClickListener(onMenuClick);
    }

   /* public void addItem(Notifications data) {
        dataList.add(data);
        notifyItemInserted(dataList.size());
    }*/

    public void removeItem(int position) {
        dataList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, dataList.size());
    }

    private final MenuItem.OnMenuItemClickListener onMenuClick = new MenuItem.OnMenuItemClickListener() {
        @Override
        public boolean onMenuItemClick(MenuItem item) {
            int position = item.getGroupId();
            switch (item.getItemId()) {
                case MENU_MARK_READ:
                    if (listener != null) {
                        listener.onItemMarkAsRead(position, dataList.get(position));
                    }
                    return true;

                case MENU_DELETE:
                    if (listener != null) {
                        listener.onItemDelete(position, dataList.get(position));
                    }
                    return true;
            }
            return false;
        }
    };

    private class EventViewHolder extends RecyclerView.ViewHolder {
        LinearLayout llNotificationLayout;
        ImageView iv_poster;
        TextView tvTitle, tvTime;

        EventViewHolder(View itemView) {
            super(itemView);

            llNotificationLayout = itemView.findViewById(R.id.llNotificationLayout);
            iv_poster = itemView.findViewById(R.id.ivEventPoster);
            tvTitle = itemView.findViewById(R.id.tvEventTitle);
            tvTime = itemView.findViewById(R.id.tvTime);

        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.listener = listener;
    }

    public interface OnItemClickListener {

        void onItemClick(int position, Notifications notification);

        void onItemMarkAsRead(int position, Notifications notification);

        void onItemDelete(int position, Notifications notification);

    }
}
