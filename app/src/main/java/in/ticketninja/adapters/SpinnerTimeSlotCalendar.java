/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.objects.EventTimeSlot;

/**
 * TicketNinja_Working(in.ticketninja.adapters) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 12/8/17.
 *
 * @author Suthar Rohit
 */
public class SpinnerTimeSlotCalendar extends BaseAdapter {

    private LayoutInflater mInflater;

    private List<EventTimeSlot> dataList;

    public SpinnerTimeSlotCalendar(Context context, List<EventTimeSlot> dataList) {
        mInflater = LayoutInflater.from(context);
        this.dataList = dataList;
    }


    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private static class ViewHolder {
        TextView tvItemText;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder h;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.custom_sp_timeslot, parent, false);
            h = new ViewHolder();
            h.tvItemText = convertView.findViewById(R.id.tvItemText);
            h.tvItemText.setTextColor(Color.parseColor("#323232"));
            convertView.setTag(h);

        } else {
            h = (ViewHolder) convertView.getTag();
        }

        if (dataList != null && dataList.size() > 0) {
            if (dataList.size() > position) {
                h.tvItemText.setAlpha(position == 0 ? 0.5f : 1f);
                h.tvItemText.setText(dataList.get(position).timeSlot());
            }
        }
        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        ViewHolder h;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.custom_sp_dropdown, parent, false);
            h = new ViewHolder();
            h.tvItemText = convertView.findViewById(R.id.tvItemText);
            convertView.setTag(h);

        } else {
            h = (ViewHolder) convertView.getTag();
        }
        if (dataList != null && dataList.size() > 0) {
            if (dataList.size() > position) {
                h.tvItemText.setText(dataList.get(position).timeSlot());
            }
        }
        return convertView;
    }


}
