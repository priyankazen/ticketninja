/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.activity.EventDetailActivity;
import in.ticketninja.activity.EventListActivity;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.objects.MyEvent;
import in.ticketninja.objects.MySlider;

/**
 * Created by Shabbir on 26/10/16.
 */

public class DashboardPagerAdapter extends PagerAdapter {

    private CommonClass CC;
    private Context mContext;
    private List<MySlider> dataList;

    public DashboardPagerAdapter(Context mContext, List<MySlider> dataList) {
        this.mContext = mContext;
        this.dataList = dataList;
        this.CC = new CommonClass(mContext);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.custom_pager_image, container, false);

        ImageView imageView = itemView.findViewById(R.id.img_pager_item);

        String url = dataList.get(position).getMainImageURL();

        /*if (Constant.PROMO_SLIDER_LIST.PROMO.equals(dataList.get(position).getType())) {
            url = "https://ticketninja.in/Admin/Document/".concat(dataList.get(position).getMainImageId())
        }*/

        try {
            Glide.with(mContext)
                    .load(url)
                    //.centerCrop()
                    .skipMemoryCache(false)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(imageView);

           // imageView.setImageBitmap(getbmpfromURL(src));
        }catch (Exception e){
            e.printStackTrace();
            Log.e("DashboardPagerAdapter","getMessage: "+e.getMessage());
        }


        imageView.setOnClickListener(v -> {
            if (CC.isOnline()) {
                if (Constant.PROMO_SLIDER_LIST.PROMO.equals(dataList.get(position).getType())) {
                    if (!TextUtils.isEmpty(dataList.get(position).getUrl()) && !Constant.HASH.equals(dataList.get(position).getUrl())) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(dataList.get(position).getUrl()));
                        mContext.startActivity(browserIntent);
                    }
                }else if (Constant.PROMO_SLIDER_LIST.COLLECTION.equals(dataList.get(position).getType())) {
                    Intent intEvent = new Intent(mContext, EventListActivity.class);
                    intEvent.putExtra(Constant.ScreenExtras.EVENT_TYPE_NAME, dataList.get(position).getEvent_type());
                    intEvent.putExtra(Constant.ScreenExtras.EVENT_TYPE_KEY, dataList.get(position).getEvent_type());
                    intEvent.putExtra(Constant.ScreenExtras.EVENT_C_TYPE, dataList.get(position).getCollection());
                    mContext.startActivity(intEvent);
                } else {
                    MyEvent myEvent = new MyEvent();
                    myEvent.event_id = dataList.get(position).event_id;
                    myEvent.Poster_Image_Id = dataList.get(position).getMainImageId();
                    myEvent.poster_image_url = dataList.get(position).getMainImageURL();
                    myEvent.Main_Image_Id = dataList.get(position).getMainImageId();
                    myEvent.main_image_url = dataList.get(position).getMainImageURL();

                    Intent intDetail = new Intent(v.getContext(), EventDetailActivity.class);
                    intDetail.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                    mContext.startActivity(intDetail);

                    /*if (Build.VERSION_CODES.JELLY_BEAN <= Build.VERSION.SDK_INT) {
                        Pair<View, String> p1 = Pair.create(imageView, "eventPoster");
                        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation((Activity) mContext, p1);
                        mContext.startActivity(intDetail, options.toBundle());
                    } else {
                        mContext.startActivity(intDetail);
                    }*/
                }
            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        });

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((LinearLayout) object);
    }


    /*public Bitmap getbmpfromURL(String surl){
        try {
            Log.e("DashboardPagerAdapter","surl: "+surl);
            URL url = new URL(surl);
            Log.e("DashboardPagerAdapter","src: "+url);
            HttpURLConnection urlcon = (HttpURLConnection) url.openConnection();
            urlcon.setDoInput(true);
            urlcon.connect();
            InputStream in = urlcon.getInputStream();
            return BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
            return null;
        }
    }*/

}
