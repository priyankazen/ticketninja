/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import in.ticketninja.R;

public class EventPricingCountAdapter extends RecyclerView.Adapter {

    private List<String> dataList = new ArrayList<>();
    private OnItemClickListener clickListener;

    private int currentSelection = -1;

    EventPricingCountAdapter(int totalCount) {
        for (int i = 0; i < totalCount; i++) {
            dataList.add("" + i);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_event_pricing_count, parent, false);
        return new PricingViewHolder(v);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        try {
            PricingViewHolder h = (PricingViewHolder) viewHolder;
            h.rdTicketCount.setText(String.format(Locale.getDefault(), "%d", position + 1));
            h.rdTicketCount.setSelected(currentSelection == h.getAdapterPosition());
            h.rdTicketCount.setOnClickListener(v -> {
                currentSelection = currentSelection == h.getAdapterPosition() ? -1 : h.getAdapterPosition();
                if (clickListener != null)
                    clickListener.onItemClick(h.getAdapterPosition(), currentSelection == h.getAdapterPosition());
                notifyDataSetChanged();
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private static class PricingViewHolder extends RecyclerView.ViewHolder {

        TextView rdTicketCount;

        PricingViewHolder(@NonNull View itemView) {
            super(itemView);
            rdTicketCount = itemView.findViewById(R.id.rdTicketCount);
        }
    }

    // LOAD MORE LISTENER
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.clickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, boolean isChecked);
    }

}
