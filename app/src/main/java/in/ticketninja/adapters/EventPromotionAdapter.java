/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.objects.EventPromotion;

public class EventPromotionAdapter extends RecyclerView.Adapter {
    //private static final String TAG = EventPromotionAdapter.class.getSimpleName();
    private List<EventPromotion> dataList;
    private Context context;

    public interface EventPromotionClickEvent {
        void eventPromotionClickEvent(EventPromotion eventPromotion);
    }

    private EventPromotionClickEvent eventPromotionClickEvent;

    public void setClickEvent(EventPromotionClickEvent clickEvent) {
        this.eventPromotionClickEvent = clickEvent;
    }

    public EventPromotionAdapter(Context context, List<EventPromotion> dataList) {
        this.dataList = dataList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_event_promotion_offer, parent, false);
        return new MenuHolder(v);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        try {
            MenuHolder h = (MenuHolder) viewHolder;
            h.txtPromoCode.setText(String.format("%s %s", "Use code ", dataList.get(position).getPromo_code()));
            h.txtPromoDes.setText(String.format("%s", dataList.get(position).getPromo_code_label()));
            h.itemView.setOnClickListener(view -> {
                Log.e("TAG", "click");
                if (eventPromotionClickEvent != null) {
                    eventPromotionClickEvent.eventPromotionClickEvent(dataList.get(h.getAdapterPosition()));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class MenuHolder extends RecyclerView.ViewHolder {

        TextView txtPromoCode, txtPromoDes;

        MenuHolder(@NonNull View itemView) {
            super(itemView);
            try {
                txtPromoCode = itemView.findViewById(R.id.txtPromoCode);
                txtPromoDes = itemView.findViewById(R.id.txtPromoDes);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
