/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import in.ticketninja.R;
import in.ticketninja.objects.EventGalleryList;

public class EventGalleryAdapter extends RecyclerView.Adapter {
    //private static final String TAG = EventGalleryAdapter.class.getSimpleName();
    private List<EventGalleryList> dataList;
    private Context context;

    public interface EventGalleryClickEvent {
        void eventGalleryClickEvent(EventGalleryList eventGallery,int position);
    }

    private EventGalleryClickEvent eventGalleryClickEvent;

    public void setClickEvent(EventGalleryClickEvent clickEvent) {
        this.eventGalleryClickEvent = clickEvent;
    }

    public EventGalleryAdapter(Context context, List<EventGalleryList> dataList) {
        this.dataList = dataList;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(context).inflate(R.layout.item_event_gallery, parent, false);
        return new MenuHolder(v);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        try {
            Log.e("TAG",""+dataList);
            MenuHolder h = (MenuHolder) viewHolder;
            try {
                Glide.with(context)
                        .load(dataList.get(position).getImage_url())
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(h.ivEventGallery);
            }catch (Exception e){
                e.printStackTrace();
            }

            h.itemView.setOnClickListener(view -> {
                Log.e("TAG", "click");
                if (eventGalleryClickEvent != null) {
                    eventGalleryClickEvent.eventGalleryClickEvent(dataList.get(h.getAdapterPosition()),h.getAdapterPosition());
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class MenuHolder extends RecyclerView.ViewHolder {

        ImageView ivEventGallery;

        MenuHolder(@NonNull View itemView) {
            super(itemView);
            try {
                ivEventGallery = itemView.findViewById(R.id.ivEventGallery);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
