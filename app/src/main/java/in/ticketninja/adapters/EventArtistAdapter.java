/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import in.ticketninja.R;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.EventArtist;

public class EventArtistAdapter extends RecyclerView.Adapter {

    //private static final String TAG = EventArtistAdapter.class.getSimpleName();
    private List<EventArtist> dataList;
    private OnItemClickListener clickListener;
    private int currentSelection = -1;
    private int nameDefault, nameActivated;

    public EventArtistAdapter(Context context, List<EventArtist> dataList) {
        this.dataList = dataList;
        nameDefault = ContextCompat.getColor(context, R.color.event_detail_artist_item_name_textColor);
        nameActivated = ContextCompat.getColor(context, R.color.themeRed);
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_event_artist, parent, false);
        return new MenuHolder(v);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        try {
            MenuHolder h = (MenuHolder) viewHolder;

            h.tvItemName.setText(String.format("%s", dataList.get(position).name()));
            h.tvItemName.setTextColor(currentSelection == position ? nameActivated : nameDefault);
            h.itemView.setOnClickListener(v -> {
                currentSelection = currentSelection == h.getAdapterPosition() ? -1 : h.getAdapterPosition();
                if (clickListener != null)
                    clickListener.onItemClick(h.getAdapterPosition(),
                            currentSelection != -1,
                            h.getAdapterPosition() > -1 ? dataList.get(h.getAdapterPosition()) : null);
                notifyDataSetChanged();
            });

            if (Validate.isNotNull(dataList.get(position).photoURL())) {
                Glide.with(h.ivItemIcon.getContext())
                        .load(dataList.get(position).photoURL())
                        .placeholder(R.drawable.ic_placeholder)
                        .error(R.drawable.ic_placeholder)
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        //.transform(new UserCircleTransform(h.ivItemIcon.getContext()))
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                //notifyItemChanged(h.getAdapterPosition());
                                h.ivItemIcon.setImageDrawable(resource);
                                return false;
                            }
                        })
                        .into(h.ivItemIcon);
            } else {
                Glide.with(h.ivItemIcon.getContext())
                        .load(R.drawable.ic_placeholder)
                        .into(h.ivItemIcon);
            }


        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private class MenuHolder extends RecyclerView.ViewHolder {

        CircleImageView ivItemIcon;
        TextView tvItemName;

        MenuHolder(@NonNull View itemView) {
            super(itemView);
            ivItemIcon = itemView.findViewById(R.id.ivItemIcon);
            tvItemName = itemView.findViewById(R.id.tvItemName);
        }
    }

    // LOAD MORE LISTENER
    public void setOnItemClickListener(OnItemClickListener listener) {
        this.clickListener = listener;
    }

    public interface OnItemClickListener {
        void onItemClick(int position, boolean isChecked, EventArtist artist);
    }

}
