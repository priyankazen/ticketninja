/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.database;

import android.database.Cursor;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import in.ticketninja.objects.BookingHistory;

/**
 * Created by Rohit on 07-11-2017.
 * Updated by Shreya on 10-05-2018 (changed table booking history)
 */

public class DB_BookingHistory {

    public static final String TABLE_NAME = "tbl_booking_history";

    public static final String NAME = "Name";
    public static final String CATEGORY = "Category";
    public static final String LANGUAGE = "Language";
    public static final String IMAGE_ID = "Detail_Image_Id";
    public static final String IMAGE_URL = "Detail_Image_URL";
    public static final String EVENT_TYPE = "E_Type";
    public static final String EVENT_TYPE_ICON = "E_Type_Icon";
    public static final String EVENT_ID = "Event_Id";
    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
    public static final String LOCATION_NAME = "Location_Name";
    public static final String DATE = "Date";
    public static final String DATE_TO = "Date_to";
    public static final String DAY_NAME = "Day_Name";
    public static final String DAY_TYPE = "Day_Type";
    public static final String M_D_DATE = "M_D_Date";
    public static final String Y_DATE = "Y_Date";
    public static final String TIME = "Time";
    public static final String AMOUNT = "Amount";
    public static final String IS_M_TICKET = "IsmTicket";
    public static final String EMAIL = "Email";
    public static final String MOBILE_NO = "MobileNo";
    public static final String P_D_ADDRESS = "p_d_address";
    public static final String D_TIME_SLOT = "d_time_slot";
    public static final String PAYMENT_ID = "Payment_Id";
    public static final String PAYMENT_STATUS = "Payment_Status";
    public static final String PAYMENT_TYPE = "Payment_Type";
    public static final String QR_CODE = "QR_Code";
    public static final String QR_CODE_URL = "QR_Code_url";
    public static final String TOTAL_TICKET = "Total_Ticket";
    public static final String SEAT_NO = "Seat_No";
    public static final String SEAT_TYPE = "Seat_Type";
    public static final String SEASON_PASS = "Seasonpass";
    public static final String TRANS_ID = "Trans_Id";
    public static final String TRANS_NO = "Trans_No";
    public static final String TRANS_DATE = "Trans_Date";
    public static final String TRANS_TYPE = "Trans_Type";
    public static final String USER_ID = "user_id";

    //added by Shreya 10-5-2018
    public static final String NET_PAYBLE_AMOUNT= "Net_Payble_Amount";
    public static final String DISCOUNT = "Discount";
    public static final String COUPON_ID = "Coupon_Id";
    public static final String COUPON_CODE = "Coupon_Code";
    public static final String TOTAL_PROCESSING_FEE = "total_processing_fee";
    public static final String PROCESSING_FEE = "processing_fee";
    public static final String TAX = "tex";
    public static final String TAX_TYPE = "tex_type";
    //added by priyanka
    public static final String IS_TICKET_LIVE = "ticket_live";
    public static final String IS_APP_TICKET = "app_ticket";

    public static final String CREATE_TABLE_SYNTAX = "CREATE TABLE IF NOT EXISTS " + DB_BookingHistory.TABLE_NAME
            + "(" +
            DB_BookingHistory.NAME + " TEXT," +
            DB_BookingHistory.CATEGORY + " TEXT," +
            DB_BookingHistory.LANGUAGE + " TEXT," +
            DB_BookingHistory.IMAGE_ID + " TEXT," +
            DB_BookingHistory.IMAGE_URL + " TEXT," +
            DB_BookingHistory.EVENT_TYPE + " TEXT," +
            DB_BookingHistory.EVENT_TYPE_ICON + " TEXT," +
            DB_BookingHistory.EVENT_ID + " TEXT," +
            DB_BookingHistory.LATITUDE + " TEXT," +
            DB_BookingHistory.LONGITUDE + " TEXT," +
            DB_BookingHistory.LOCATION_NAME + " TEXT," +
            DB_BookingHistory.DATE + " TEXT," +
            DB_BookingHistory.DATE_TO + " TEXT," +
            DB_BookingHistory.DAY_NAME + " TEXT," +
            DB_BookingHistory.DAY_TYPE + " TEXT," +
            DB_BookingHistory.M_D_DATE + " TEXT," +
            DB_BookingHistory.Y_DATE + " TEXT," +
            DB_BookingHistory.TIME + " TEXT," +
            DB_BookingHistory.AMOUNT + " TEXT," +
            DB_BookingHistory.IS_M_TICKET + " TEXT," +
            DB_BookingHistory.EMAIL + " TEXT," +
            DB_BookingHistory.MOBILE_NO + " TEXT," +
            DB_BookingHistory.P_D_ADDRESS + " TEXT," +
            DB_BookingHistory.D_TIME_SLOT + " TEXT," +
            DB_BookingHistory.PAYMENT_ID + " TEXT," +
            DB_BookingHistory.PAYMENT_STATUS + " TEXT," +
            DB_BookingHistory.PAYMENT_TYPE + " TEXT," +
            DB_BookingHistory.QR_CODE + " TEXT," +
            DB_BookingHistory.QR_CODE_URL + " TEXT," +
            DB_BookingHistory.TOTAL_TICKET + " TEXT," +
            DB_BookingHistory.SEAT_NO + " TEXT," +
            DB_BookingHistory.SEAT_TYPE + " TEXT," +
            DB_BookingHistory.SEASON_PASS + " TEXT," +
            DB_BookingHistory.TRANS_ID + " TEXT," +
            DB_BookingHistory.TRANS_NO + " TEXT," +
            DB_BookingHistory.TRANS_DATE + " TEXT," +
            DB_BookingHistory.TRANS_TYPE + " TEXT," +
            DB_BookingHistory.USER_ID + " TEXT," +
            //added by Shreya 10-5-2018
            DB_BookingHistory.NET_PAYBLE_AMOUNT + " TEXT," +
            DB_BookingHistory.DISCOUNT + " TEXT," +
            DB_BookingHistory.COUPON_ID + " TEXT," +
            DB_BookingHistory.COUPON_CODE + " TEXT," +
            DB_BookingHistory.TOTAL_PROCESSING_FEE + " TEXT," +
            DB_BookingHistory.PROCESSING_FEE + " TEXT," +
            DB_BookingHistory.TAX + " TEXT," +
            DB_BookingHistory.TAX_TYPE + " TEXT," +
            DB_BookingHistory.IS_TICKET_LIVE + " TEXT," +
            DB_BookingHistory.IS_APP_TICKET + " TEXT" +
            ")";
    public static final String UPDATE_TABLE_ADD_COLUMN_NET_PAYBLE_SYNTAX = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_BookingHistory.NET_PAYBLE_AMOUNT+" TEXT";
    public static final String UPDATE_TABLE_ADD_COLUMN_DISCOUNT_SYNTAX = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_BookingHistory.DISCOUNT+" TEXT";
    public static final String UPDATE_TABLE_ADD_COLUMN_COUPON_ID_SYNTAX = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_BookingHistory.COUPON_ID+" TEXT";
    public static final String UPDATE_TABLE_ADD_COLUMN_COUPON_CODE_SYNTAX = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_BookingHistory.COUPON_CODE+" TEXT";


    //adding by priyanka
    public static final String UPDATE_TABLE_ADD_COLUMN_TOTAL_PROCESSING_FEE_SYNTAX = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_BookingHistory.TOTAL_PROCESSING_FEE+" TEXT";
    public static final String UPDATE_TABLE_ADD_COLUMN_PROCESSING_FEE_SYNTAX = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_BookingHistory.PROCESSING_FEE+" TEXT";
    public static final String UPDATE_TABLE_ADD_COLUMN_TAX_SYNTAX = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_BookingHistory.TAX+" TEXT";
    public static final String UPDATE_TABLE_ADD_COLUMN_TAX_TYPE_SYNTAX = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_BookingHistory.TAX_TYPE+" TEXT";
    public static final String UPDATE_TABLE_ADD_COLUMN_QR_CODE_URL = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_BookingHistory.QR_CODE_URL+" TEXT";
    public static final String UPDATE_TABLE_ADD_COLUMN_IS_LIVE_TICKET = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_BookingHistory.IS_TICKET_LIVE+" TEXT";
    public static final String UPDATE_TABLE_ADD_COLUMN_IS_APP_TICKET = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_BookingHistory.IS_APP_TICKET+" TEXT";


    public static BookingHistory getDataFields(@NonNull Cursor c) {
        BookingHistory history = new BookingHistory();

        history.trans_id = c.getInt(c.getColumnIndex(DB_BookingHistory.TRANS_ID));
        history.trans_no = c.getString(c.getColumnIndex(DB_BookingHistory.TRANS_NO));
        history.event_name = c.getString(c.getColumnIndex(DB_BookingHistory.NAME));
        history.category = c.getString(c.getColumnIndex(DB_BookingHistory.CATEGORY));
        history.language = c.getString(c.getColumnIndex(DB_BookingHistory.LANGUAGE));
        history.detail_image_id = c.getString(c.getColumnIndex(DB_BookingHistory.IMAGE_ID));
        history.detail_image_url = c.getString(c.getColumnIndex(DB_BookingHistory.IMAGE_URL));
        history.type = c.getString(c.getColumnIndex(DB_BookingHistory.EVENT_TYPE));
        history.e_type_icon = c.getString(c.getColumnIndex(DB_BookingHistory.EVENT_TYPE_ICON));
        history.event_id = c.getInt(c.getColumnIndex(DB_BookingHistory.EVENT_ID));
        history.latitude = c.getString(c.getColumnIndex(DB_BookingHistory.LATITUDE));
        history.longitude = c.getString(c.getColumnIndex(DB_BookingHistory.LONGITUDE));
        history.location_name = c.getString(c.getColumnIndex(DB_BookingHistory.LOCATION_NAME));
        history.event_date = c.getString(c.getColumnIndex(DB_BookingHistory.DATE));
        history.event_date_to = c.getString(c.getColumnIndex(DB_BookingHistory.DATE_TO));
        history.Day_Name = c.getString(c.getColumnIndex(DB_BookingHistory.DAY_NAME));
        history.day_type = c.getString(c.getColumnIndex(DB_BookingHistory.DAY_TYPE));
        history.M_D_Date = c.getString(c.getColumnIndex(DB_BookingHistory.M_D_DATE));
        history.Y_Date = c.getString(c.getColumnIndex(DB_BookingHistory.Y_DATE));
        history.event_time = c.getString(c.getColumnIndex(DB_BookingHistory.TIME));
        history.amount = c.getDouble(c.getColumnIndex(DB_BookingHistory.AMOUNT));
        history.ismticket = Integer.valueOf(c.getString(c.getColumnIndex(DB_BookingHistory.IS_M_TICKET)));
        history.email = c.getString(c.getColumnIndex(DB_BookingHistory.EMAIL));
        history.mobileno = c.getString(c.getColumnIndex(DB_BookingHistory.MOBILE_NO));
        history.p_d_address = c.getString(c.getColumnIndex(DB_BookingHistory.P_D_ADDRESS));
        history.d_time_slot = c.getString(c.getColumnIndex(DB_BookingHistory.D_TIME_SLOT));
        history.payment_id = c.getString(c.getColumnIndex(DB_BookingHistory.PAYMENT_ID));
        history.payment_status = c.getString(c.getColumnIndex(DB_BookingHistory.PAYMENT_STATUS));
        history.payment_type = c.getString(c.getColumnIndex(DB_BookingHistory.PAYMENT_TYPE));
        history.qr_code = c.getString(c.getColumnIndex(DB_BookingHistory.QR_CODE ));
        history.qr_code_url = c.getString(c.getColumnIndex(DB_BookingHistory.QR_CODE_URL ));
       // history.total_ticket = c.getInt(c.getColumnIndex(DB_BookingHistory.TOTAL_TICKET ));
        history.noofticket = c.getInt(c.getColumnIndex(DB_BookingHistory.TOTAL_TICKET ));
        history.Seat_No = c.getString(c.getColumnIndex(DB_BookingHistory.SEAT_NO));
        history.Seat_Type = c.getString(c.getColumnIndex(DB_BookingHistory.SEAT_TYPE));
        history.seasonpass = Integer.valueOf(c.getString(c.getColumnIndex(DB_BookingHistory.SEASON_PASS)));
        history.trans_date = c.getString(c.getColumnIndex(DB_BookingHistory.TRANS_DATE));
        history.trans_type = c.getString(c.getColumnIndex(DB_BookingHistory.TRANS_TYPE));
        history.USER_ID = c.getString(c.getColumnIndex(DB_BookingHistory.USER_ID));

        //added by Shreya on 10-5-2018
        history.netamount = c.getDouble(c.getColumnIndex(DB_BookingHistory.NET_PAYBLE_AMOUNT));
        history.discount = c.getDouble(c.getColumnIndex(DB_BookingHistory.DISCOUNT));
        history.coupon_id = Integer.valueOf(c.getString(c.getColumnIndex(DB_BookingHistory.COUPON_ID)));
        history.coupon_code = c.getString(c.getColumnIndex(DB_BookingHistory.COUPON_CODE));

        //added by priyanka
        history.total_processingfee = Float.valueOf(c.getString(c.getColumnIndex(DB_BookingHistory.TOTAL_PROCESSING_FEE)));
        history.processingfee = c.getDouble(c.getColumnIndex(DB_BookingHistory.PROCESSING_FEE));
        history.tax = c.getDouble(c.getColumnIndex(DB_BookingHistory.TAX));
        history.tax_type = c.getString(c.getColumnIndex(DB_BookingHistory.TAX_TYPE));
        if (c.getString(c.getColumnIndex(DB_BookingHistory.IS_TICKET_LIVE)) != null && !TextUtils.isEmpty(c.getString(c.getColumnIndex(DB_BookingHistory.IS_TICKET_LIVE)))) {
            history.is_live_ticket = Integer.valueOf(c.getString(c.getColumnIndex(DB_BookingHistory.IS_TICKET_LIVE)));
        } else {
            history.is_live_ticket = 0;
        }

        if (c.getString(c.getColumnIndex(DB_BookingHistory.IS_APP_TICKET)) != null && !TextUtils.isEmpty(c.getString(c.getColumnIndex(DB_BookingHistory.IS_APP_TICKET)))) {
            history.is_app_ticket = Integer.valueOf(c.getString(c.getColumnIndex(DB_BookingHistory.IS_APP_TICKET)));
        } else {
            history.is_app_ticket = 0;
        }

        return history;
    }



}
