/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.database;

/**
 * Created by Rohit on 07-11-2017.
 */

public class DB_MyTickets {

    public static final String TABLE_NAME = "tbl_my_tickets";

    public static final String EVENT_ID = "Event_Id";
    public static final String NAME = "Name";
    public static final String CATEGORY = "Category";
    public static final String LANGUAGE = "Language";
    public static final String IMAGE_ID = "Detail_Image_Id";
    public static final String IMAGE_URL = "Detail_Image_URL";
    public static final String DATE = "Date";
    public static final String DATE_TO = "Date_to";
    public static final String DAY_TYPE = "Day_Type";
    public static final String TIME = "Time";
    public static final String EVENT_TYPE = "E_Type";
    public static final String LOCATION_NAME = "Location_Name";
    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
    public static final String SEAT_NO = "Seat_No";
    public static final String SEAT_TYPE = "Seat_Type";
    public static final String USER_ID = "user_id";

    //added by priyanka
    public static final String IS_LIVE_TICKET = "ticket_live";
    public static final String IS_APP_TICKET = "app_ticket";

    public static final String CREATE_TABLE_SYNTAX = "CREATE TABLE IF NOT EXISTS " + DB_MyTickets.TABLE_NAME
            + "(" +
            DB_MyTickets.EVENT_ID + " TEXT," +
            DB_MyTickets.NAME + " TEXT," +
            DB_MyTickets.CATEGORY + " TEXT," +
            DB_MyTickets.IMAGE_ID + " TEXT," +
            DB_MyTickets.IMAGE_URL + " TEXT," +
            DB_MyTickets.DATE + " TEXT," +
            DB_MyTickets.DATE_TO + " TEXT," +
            DB_MyTickets.DAY_TYPE + " TEXT," +
            DB_MyTickets.TIME + " TEXT," +
            DB_MyTickets.EVENT_TYPE + " TEXT," +
            DB_MyTickets.LANGUAGE + " TEXT," +
            DB_MyTickets.LOCATION_NAME + " TEXT," +
            DB_MyTickets.LATITUDE + " TEXT," +
            DB_MyTickets.LONGITUDE + " TEXT," +
            DB_MyTickets.SEAT_NO + " TEXT," +
            DB_MyTickets.SEAT_TYPE + " TEXT," +
            DB_MyTickets.USER_ID + " TEXT," +
            DB_MyTickets.IS_LIVE_TICKET + " TEXT," +
            DB_MyTickets.IS_APP_TICKET + " TEXT" +
            ")";

    public static final String UPDATE_TABLE_ADD_COLUMN_IS_LIVE_TICKET = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_MyTickets.IS_LIVE_TICKET+" TEXT";
    public static final String UPDATE_TABLE_ADD_COLUMN_IS_APP_TICKET  = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_MyTickets.IS_APP_TICKET+" TEXT";

}
