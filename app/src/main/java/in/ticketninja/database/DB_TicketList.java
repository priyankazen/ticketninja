/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.database;

import android.database.Cursor;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import in.ticketninja.objects.BookingTicketList;

/**
 * Created by Rohit on 07-11-2017.
 */

public class DB_TicketList {

    public static final String TABLE_NAME = "tbl_booking_history_ticket_list";

    public static final String BOOKING_ID = "Booking_Id";
    public static final String NAME = "Name";
    public static final String CATEGORY = "Category_Name";
    public static final String LANGUAGE = "Language";
    public static final String IMAGE_ID = "Detail_Image_Id";
    public static final String IMAGE_URL = "Detail_Image_URL";
    public static final String DATE = "Date";
    public static final String DATE_TO = "Date_to";
    public static final String DAY_NAME = "Day_Name";
    public static final String DAY_TYPE = "Day_Type";
    public static final String D_M_DATE = "D_M_Date";
    public static final String TIME = "Time";
    public static final String EVENT_TYPE = "E_Type";
    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
    public static final String LOCATION_NAME = "Location_Name";
    public static final String NO_OF_TICKETS = "NoofTicket";
    public static final String QR_CODE = "QR_Code";
    public static final String QR_CODE_URL = "QR_Code_url";
    public static final String RATE = "Rate";
    public static final String SEAT_NO = "Seat_No";
    public static final String SEAT_TYPE = "Seat_Type";
    public static final String STATUS = "Status";
    public static final String SHARE_FROM = "Share_From";
    public static final String SHARE_TO = "Share_To";
    public static final String SEASON_PASS = "Seasonpass";
    public static final String TICKET_DESC = "Ticket_desc";
    public static final String TRANS_ID = "Trans_Id";
    public static final String TRANS_NO = "Trans_No";
    public static final String Y_DATE = "Y_Date";
    public static final String USER_ID = "user_id";

    //add by priyanka
    public static final String IS_TICKET_LIVE = "ticket_live";
    public static final String IS_APP_TICKET = "app_ticket";


    public static final String CREATE_TABLE_SYNTAX = "CREATE TABLE IF NOT EXISTS " + DB_TicketList.TABLE_NAME
            + "(" +
            DB_TicketList.BOOKING_ID + " TEXT," +
            DB_TicketList.NAME + " TEXT," +
            DB_TicketList.CATEGORY + " TEXT," +
            DB_TicketList.LANGUAGE + " TEXT," +
            DB_TicketList.IMAGE_ID + " TEXT," +
            DB_TicketList.IMAGE_URL + " TEXT," +
            DB_TicketList.DATE + " TEXT," +
            DB_TicketList.DATE_TO + " TEXT," +
            DB_TicketList.DAY_NAME + " TEXT," +
            DB_TicketList.DAY_TYPE + " TEXT," +
            DB_TicketList.D_M_DATE + " TEXT," +
            DB_TicketList.TIME + " TEXT," +
            DB_TicketList.EVENT_TYPE + " TEXT," +
            DB_TicketList.LATITUDE + " TEXT," +
            DB_TicketList.LONGITUDE + " TEXT," +
            DB_TicketList.LOCATION_NAME + " TEXT," +
            DB_TicketList.NO_OF_TICKETS + " TEXT," +
            DB_TicketList.QR_CODE + " TEXT," +
            DB_TicketList.QR_CODE_URL + " TEXT," +
            DB_TicketList.RATE + " TEXT," +
            DB_TicketList.SEAT_NO + " TEXT," +
            DB_TicketList.SEAT_TYPE + " TEXT," +
            DB_TicketList.STATUS + " TEXT," +
            DB_TicketList.SHARE_FROM + " TEXT," +
            DB_TicketList.SHARE_TO + " TEXT," +
            DB_TicketList.SEASON_PASS + " TEXT," +
            DB_TicketList.TICKET_DESC + " TEXT," +
            DB_TicketList.TRANS_ID + " TEXT," +
            DB_TicketList.TRANS_NO + " TEXT," +
            DB_TicketList.Y_DATE + " TEXT," +
            DB_TicketList.USER_ID + " TEXT," +
            DB_TicketList.IS_TICKET_LIVE + " TEXT," +
            DB_TicketList.IS_APP_TICKET + " TEXT" +
            ")";

    //adding by priyanka
    public static final String UPDATE_TABLE_ADD_COLUMN_QR_CODE_URL = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_TicketList.QR_CODE_URL+" TEXT";
    public static final String UPDATE_TABLE_ADD_COLUMN_IS_LIVE_TICKET = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_TicketList.IS_TICKET_LIVE+" TEXT";
    public static final String UPDATE_TABLE_ADD_COLUMN_IS_APP_TICKET = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_TicketList.IS_APP_TICKET+" TEXT";


    public static  BookingTicketList getDataFields(@NonNull Cursor c) {
        BookingTicketList history = new BookingTicketList();

        history.booking_id = c.getString(c.getColumnIndex(DB_TicketList.BOOKING_ID));
        history.event_name = c.getString(c.getColumnIndex(DB_TicketList.NAME));
        history.category_name = c.getString(c.getColumnIndex(DB_TicketList.CATEGORY));
        history.language = c.getString(c.getColumnIndex(DB_TicketList.LANGUAGE));
        history.detail_image_id = c.getString(c.getColumnIndex(DB_TicketList.IMAGE_ID));
        history.detail_image_url = c.getString(c.getColumnIndex(DB_TicketList.IMAGE_URL));
        history.event_date = c.getString(c.getColumnIndex(DB_TicketList.DATE));
        history.event_date_to = c.getString(c.getColumnIndex(DB_TicketList.DATE_TO));
        history.Day_Name = c.getString(c.getColumnIndex(DB_TicketList.DAY_NAME));
        history.day_type = c.getString(c.getColumnIndex(DB_TicketList.DAY_TYPE));
        history.D_M_Date = c.getString(c.getColumnIndex(DB_TicketList.D_M_DATE));
        history.event_time = c.getString(c.getColumnIndex(DB_TicketList.TIME));
        history.type = c.getString(c.getColumnIndex(DB_TicketList.EVENT_TYPE));
        history.latitude = c.getString(c.getColumnIndex(DB_TicketList.LATITUDE));
        history.longitude = c.getString(c.getColumnIndex(DB_TicketList.LONGITUDE));
        history.location_name = c.getString(c.getColumnIndex(DB_TicketList.LOCATION_NAME));
        history.noofticket = c.getInt(c.getColumnIndex(DB_TicketList.NO_OF_TICKETS));
        history.qr_code = c.getString(c.getColumnIndex(DB_TicketList.QR_CODE));
        history.qr_code_url = c.getString(c.getColumnIndex(DB_TicketList.QR_CODE_URL));
        history.rate = c.getDouble(c.getColumnIndex(DB_TicketList.RATE));
        history.seat_no = c.getString(c.getColumnIndex(DB_TicketList.SEAT_NO));
        history.seat_type = c.getString(c.getColumnIndex(DB_TicketList.SEAT_TYPE));
        history.status = c.getString(c.getColumnIndex(DB_TicketList.STATUS));
        history.share_from = c.getString(c.getColumnIndex(DB_TicketList.SHARE_FROM));
        history.share_to = c.getString(c.getColumnIndex(DB_TicketList.SHARE_TO));
        history.seasonpass = Integer.valueOf(c.getString(c.getColumnIndex(DB_TicketList.SEASON_PASS)));
        history.ticket_desc = c.getString(c.getColumnIndex(DB_TicketList.TICKET_DESC));
        history.trans_id = c.getInt(c.getColumnIndex(DB_TicketList.TRANS_ID));
        history.trans_no = c.getString(c.getColumnIndex(DB_TicketList.TRANS_NO));
        history.Y_Date = c.getString(c.getColumnIndex(DB_TicketList.Y_DATE));
        history.user_id = c.getInt(c.getColumnIndex(DB_TicketList.USER_ID));

        if (c.getString(c.getColumnIndex(DB_TicketList.IS_TICKET_LIVE)) != null && !TextUtils.isEmpty(c.getString(c.getColumnIndex(DB_TicketList.IS_TICKET_LIVE)))) {
            history.is_live_ticket = Integer.valueOf(c.getString(c.getColumnIndex(DB_TicketList.IS_TICKET_LIVE)));
        } else {
            history.is_live_ticket = 0;
        }

        if (c.getString(c.getColumnIndex(DB_TicketList.IS_APP_TICKET)) != null &&  !TextUtils.isEmpty(c.getString(c.getColumnIndex(DB_TicketList.IS_APP_TICKET)))) {
            history.is_app_ticket = Integer.valueOf(c.getString(c.getColumnIndex(DB_TicketList.IS_APP_TICKET)));

        } else {
            history.is_app_ticket = 0;
        }

        return history;
    }

}
