/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.database;

import android.database.Cursor;
import androidx.annotation.NonNull;
import android.text.TextUtils;

import in.ticketninja.objects.BookingTicketList;

/**
 * Created by Rohit on 07-11-2017.
 */

public class DB_MyTicketList {

    public static final String TABLE_NAME = "tbl_my_ticket_list";

    public static final String BOOKING_ID = "Booking_Id";
    public static final String EVENT_ID = "Event_Id";
    public static final String NAME = "Name";
    public static final String CATEGORY = "Category_Name";
    public static final String LANGUAGE = "Language";
    public static final String IMAGE_ID = "Detail_Image_Id";
    public static final String IMAGE_URL = "Detail_Image_URL";
    public static final String DATE = "Date";
    public static final String DATE_TO = "Date_to";
    public static final String DAY_NAME = "Day_Name";
    public static final String DAY_TYPE = "Day_Type";
    public static final String D_M_DATE = "D_M_Date";
    public static final String TIME = "Time";
    public static final String EVENT_TYPE = "E_Type";
    public static final String LOCATION_NAME = "Location_Name";
    public static final String LATITUDE = "Latitude";
    public static final String LONGITUDE = "Longitude";
    public static final String NO_OF_TICKETS = "NoofTicket";
    public static final String QR_CODE = "QR_Code";
    public static final String QR_CODE_URL = "QR_Code_url";
    public static final String RATE = "Rate";
    public static final String SEASON_PASS = "Seasonpass";
    public static final String SEAT_NO = "Seat_No";
    public static final String SEAT_TYPE = "Seat_Type";
    public static final String SHARE_TO = "Share_To";
    public static final String SHARE_FROM = "Share_From";
    public static final String STATUS = "Status";
    public static final String TICKET_DESC = "Ticket_desc";
    public static final String TRANS_ID = "Trans_Id";
    public static final String TRANS_NO = "Trans_No";
    public static final String Y_DATE = "Y_Date";
    public static final String USER_ID = "user_id";

    //added by priyanka
    public static final String IS_TICKET_LIVE = "ticket_live";
    public static final String IS_APP_TICKET = "app_ticket";

    public static final String CREATE_TABLE_SYNTAX = "CREATE TABLE IF NOT EXISTS " + DB_MyTicketList.TABLE_NAME
            + "(" +
            DB_MyTicketList.BOOKING_ID + " TEXT," +
            DB_MyTicketList.EVENT_ID + " TEXT," +
            DB_MyTicketList.NAME + " TEXT," +
            DB_MyTicketList.CATEGORY + " TEXT," +
            DB_MyTicketList.LANGUAGE + " TEXT," +
            DB_MyTicketList.IMAGE_ID + " TEXT," +
            DB_MyTicketList.IMAGE_URL + " TEXT," +
            DB_MyTicketList.DATE + " TEXT," +
            DB_MyTicketList.DATE_TO + " TEXT," +
            DB_MyTicketList.DAY_NAME + " TEXT," +
            DB_MyTicketList.DAY_TYPE + " TEXT," +
            DB_MyTicketList.D_M_DATE + " TEXT," +
            DB_MyTicketList.TIME + " TEXT," +
            DB_MyTicketList.EVENT_TYPE + " TEXT," +
            DB_MyTicketList.LOCATION_NAME + " TEXT," +
            DB_MyTicketList.LATITUDE + " TEXT," +
            DB_MyTicketList.LONGITUDE + " TEXT," +
            DB_MyTicketList.NO_OF_TICKETS + " TEXT," +
            DB_MyTicketList.QR_CODE + " TEXT," +
            DB_MyTicketList.QR_CODE_URL + " TEXT," +
            DB_MyTicketList.RATE + " TEXT," +
            DB_MyTicketList.SEASON_PASS + " TEXT," +
            DB_MyTicketList.SEAT_NO + " TEXT," +
            DB_MyTicketList.SEAT_TYPE + " TEXT," +
            DB_MyTicketList.SHARE_FROM + " TEXT," +
            DB_MyTicketList.SHARE_TO + " TEXT," +
            DB_MyTicketList.STATUS + " TEXT," +
            DB_MyTicketList.TICKET_DESC + " TEXT," +
            DB_MyTicketList.TRANS_ID + " TEXT," +
            DB_MyTicketList.TRANS_NO + " TEXT," +
            DB_MyTicketList.Y_DATE + " TEXT," +
            DB_MyTicketList.USER_ID + " TEXT," +
            DB_MyTicketList.IS_TICKET_LIVE + " TEXT," +
            DB_MyTicketList.IS_APP_TICKET + " TEXT" +
            ")";

    //adding by priyanka
    public static final String UPDATE_TABLE_ADD_COLUMN_QR_CODE_URL = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_MyTicketList.QR_CODE_URL+" TEXT";
    public static final String UPDATE_TABLE_ADD_COLUMN_IS_LIVE_TICKET = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_MyTicketList.IS_TICKET_LIVE+" TEXT";
    public static final String UPDATE_TABLE_ADD_COLUMN_IS_APP_TICKET = "ALTER TABLE "+TABLE_NAME+" ADD COLUMN "+DB_MyTicketList.IS_APP_TICKET+" TEXT";


    public static  BookingTicketList getDataFields(@NonNull Cursor c) {
        BookingTicketList history = new BookingTicketList();

        history.booking_id = c.getString(c.getColumnIndex(DB_MyTicketList.BOOKING_ID));
        history.event_id = c.getInt(c.getColumnIndex(DB_MyTicketList.EVENT_ID));
        history.event_name = c.getString(c.getColumnIndex(DB_MyTicketList.NAME));
        history.category_name = c.getString(c.getColumnIndex(DB_MyTicketList.CATEGORY));
        history.language = c.getString(c.getColumnIndex(DB_MyTicketList.LANGUAGE));
        history.detail_image_id = c.getString(c.getColumnIndex(DB_MyTicketList.IMAGE_ID));
        history.detail_image_url = c.getString(c.getColumnIndex(DB_MyTicketList.IMAGE_URL));
        history.event_date = c.getString(c.getColumnIndex(DB_MyTicketList.DATE));
        history.event_date_to = c.getString(c.getColumnIndex(DB_MyTicketList.DATE_TO));
        history.Day_Name = c.getString(c.getColumnIndex(DB_MyTicketList.DAY_NAME));
        history.day_type = c.getString(c.getColumnIndex(DB_MyTicketList.DAY_TYPE));
        history.D_M_Date = c.getString(c.getColumnIndex(DB_MyTicketList.D_M_DATE));
        history.event_time = c.getString(c.getColumnIndex(DB_MyTicketList.TIME));
        history.type = c.getString(c.getColumnIndex(DB_MyTicketList.EVENT_TYPE));
        history.latitude = c.getString(c.getColumnIndex(DB_MyTicketList.LATITUDE));
        history.longitude = c.getString(c.getColumnIndex(DB_MyTicketList.LONGITUDE));
        history.location_name = c.getString(c.getColumnIndex(DB_MyTicketList.LOCATION_NAME));
        history.qr_code = c.getString(c.getColumnIndex(DB_MyTicketList.QR_CODE));
        history.qr_code_url = c.getString(c.getColumnIndex(DB_MyTicketList.QR_CODE_URL));
        history.noofticket = c.getInt(c.getColumnIndex(DB_MyTicketList.NO_OF_TICKETS));
        history.rate = c.getDouble(c.getColumnIndex(DB_MyTicketList.RATE));
        history.seat_no = c.getString(c.getColumnIndex(DB_MyTicketList.SEAT_NO));
        history.seat_type = c.getString(c.getColumnIndex(DB_MyTicketList.SEAT_TYPE));
        history.status = c.getString(c.getColumnIndex(DB_MyTicketList.STATUS));
        history.share_to = c.getString(c.getColumnIndex(DB_MyTicketList.SHARE_TO));
        history.share_from = c.getString(c.getColumnIndex(DB_MyTicketList.SHARE_FROM));
        history.seasonpass = Integer.valueOf(c.getString(c.getColumnIndex(DB_MyTicketList.SEASON_PASS)));
        history.ticket_desc = c.getString(c.getColumnIndex(DB_MyTicketList.TICKET_DESC));
        history.trans_id = c.getInt(c.getColumnIndex(DB_MyTicketList.TRANS_ID));
        history.trans_no = c.getString(c.getColumnIndex(DB_MyTicketList.TRANS_NO));
        history.Y_Date = c.getString(c.getColumnIndex(DB_MyTicketList.Y_DATE));
        history.user_id = c.getInt(c.getColumnIndex(DB_MyTicketList.USER_ID));

        if (c.getString(c.getColumnIndex(DB_MyTicketList.IS_TICKET_LIVE)) != null && !TextUtils.isEmpty(c.getString(c.getColumnIndex(DB_MyTicketList.IS_TICKET_LIVE)))) {
            history.is_live_ticket = Integer.valueOf(c.getString(c.getColumnIndex(DB_MyTicketList.IS_TICKET_LIVE)));
        } else {
            history.is_live_ticket = 0;
        }

        if (c.getString(c.getColumnIndex(DB_MyTicketList.IS_APP_TICKET)) != null && !TextUtils.isEmpty(c.getString(c.getColumnIndex(DB_MyTicketList.IS_APP_TICKET)))) {
            history.is_app_ticket = Integer.valueOf(c.getString(c.getColumnIndex(DB_MyTicketList.IS_APP_TICKET)));
        } else {
            history.is_app_ticket = 0;
        }


        return history;
    }


    /*{
        "Booking_Id":3901,
            "Category_Name":"Mazza Club",
            "Date":"2017-11-19",
            "Date_to":"2017-11-19",
            "Day_Type":"Singleday",
            "Detail_Image_Id":"524984cb-2921-46b1-be33-f42d74127ec4.jpg",
            "Detail_Image_URL":
        "https:\/\/demo.ticketninja.in:4430\/Admin\/Document\/524984cb-2921-46b1-be33-f42d74127ec4.jpg",
                "E_Type":"Event",
            "Latitude":"23.03430230953805",
            "Location_Name":"Rajpath Club-Ahmedabad",
            "Longitude":"72.50887602111811",
            "Name":"Papon Live",
            "NoofTicket":"1",
            "QR_Code":"T-NEF3904197",
            "Rate":2000.00,
            "Seasonpass":false,
            "Seat_No":"",
            "Seat_Type":"Free Seating",
            "Share_From":"",
            "Status":"Open",
            "Ticket_desc":"ADMIT ONE PERSON",
            "Time":"7:00 AM",
            "Time_Type":null,
            "Trans_Id":3904,
            "Trans_No":"NEF3904197"
    }*/

}
