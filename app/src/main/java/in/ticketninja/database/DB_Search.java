/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.database;

import android.content.ContentValues;

/**
 * Created by Rohit on 07-11-2017.
 */

public class DB_Search {

    public static final String TABLE_NAME = "search";

    public static final String USER_ID = "id";
    public static final String SEARCH_KEY = "search_key";

    public static final String CREATE_TABLE_SYNTAX = "CREATE TABLE IF NOT EXISTS " + DB_Search.TABLE_NAME
            + "("
            + DB_Search.USER_ID + " INTEGER,"
            + DB_Search.SEARCH_KEY + " TEXT"
            + ")";

    public static  ContentValues getContentValue(String keyword, String userId) {
        ContentValues values = new ContentValues();
        values.put(SEARCH_KEY, keyword);
        values.put(USER_ID, userId);
        return values;
    }
}
