/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import in.ticketninja.common.Constant;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.BookingHistory;
import in.ticketninja.objects.BookingTicketList;
import in.ticketninja.objects.File;
import in.ticketninja.objects.Search_db;
import in.ticketninja.objects.User_Ticket_Event;
import in.ticketninja.service.DownloadingService;

import static in.ticketninja.objects.File.getFile;

/**
 * TicketNinja_Working(in.ticketninja.database) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 13/6/17.
 *
 * @author Suthar Rohit
 * <p>
 * 1)Updated by Shreya DB VERSION 3
 * - added new column (NET_PAYBLE_AMOUNT,DISCOUNT,COUPON_ID,COUPON_CODE)
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    private final static  String TAG = DatabaseHelper.class.getSimpleName();
    /*  private static final int DATABASE_VERSION = 5;
      private static final int DATABASE_VERSION_OLD = 4;*/
    private static final int DATABASE_VERSION = 6;
    private static final int DATABASE_VERSION_OLD = 5;
    public static final String DATABASE_NAME = "ticket-ninja_db.sqlite";
    private Context mContext;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.e(TAG, "onCreate SQLiteDatabase");

        db.execSQL(DB_Search.CREATE_TABLE_SYNTAX);
        db.execSQL(DB_BookingHistory.CREATE_TABLE_SYNTAX);
        db.execSQL(DB_TicketList.CREATE_TABLE_SYNTAX);
        db.execSQL(DB_MyTickets.CREATE_TABLE_SYNTAX);
        db.execSQL(DB_MyTicketList.CREATE_TABLE_SYNTAX);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        try {
            Log.e(TAG, "oldVersion:" + oldVersion);
            Log.e(TAG, "newVersion:" + newVersion);

            db.execSQL(DB_Search.CREATE_TABLE_SYNTAX);
            if (newVersion > oldVersion) {

                if (oldVersion <= 2) {
                    db.execSQL(DB_BookingHistory.UPDATE_TABLE_ADD_COLUMN_NET_PAYBLE_SYNTAX);
                    db.execSQL(DB_BookingHistory.UPDATE_TABLE_ADD_COLUMN_DISCOUNT_SYNTAX);
                    db.execSQL(DB_BookingHistory.UPDATE_TABLE_ADD_COLUMN_COUPON_ID_SYNTAX);
                    db.execSQL(DB_BookingHistory.UPDATE_TABLE_ADD_COLUMN_COUPON_CODE_SYNTAX);
                } else if (oldVersion <= 3) {
                    //adding by priyanka
                    db.execSQL(DB_BookingHistory.UPDATE_TABLE_ADD_COLUMN_TOTAL_PROCESSING_FEE_SYNTAX);
                    db.execSQL(DB_BookingHistory.UPDATE_TABLE_ADD_COLUMN_PROCESSING_FEE_SYNTAX);
                    db.execSQL(DB_BookingHistory.UPDATE_TABLE_ADD_COLUMN_TAX_SYNTAX);
                    db.execSQL(DB_BookingHistory.UPDATE_TABLE_ADD_COLUMN_TAX_TYPE_SYNTAX);
                } else if (oldVersion <= 4) {
                    db.execSQL(DB_BookingHistory.UPDATE_TABLE_ADD_COLUMN_QR_CODE_URL);
                    db.execSQL(DB_MyTicketList.UPDATE_TABLE_ADD_COLUMN_QR_CODE_URL);
                    db.execSQL(DB_TicketList.UPDATE_TABLE_ADD_COLUMN_QR_CODE_URL);
                } else if (oldVersion <= 5) {
                    db.execSQL(DB_BookingHistory.UPDATE_TABLE_ADD_COLUMN_IS_LIVE_TICKET);
                    db.execSQL(DB_TicketList.UPDATE_TABLE_ADD_COLUMN_IS_LIVE_TICKET);
                    db.execSQL(DB_MyTicketList.UPDATE_TABLE_ADD_COLUMN_IS_LIVE_TICKET);
                    db.execSQL(DB_MyTickets.UPDATE_TABLE_ADD_COLUMN_IS_LIVE_TICKET);
                    db.execSQL(DB_BookingHistory.UPDATE_TABLE_ADD_COLUMN_IS_APP_TICKET);
                    db.execSQL(DB_TicketList.UPDATE_TABLE_ADD_COLUMN_IS_APP_TICKET);
                    db.execSQL(DB_MyTicketList.UPDATE_TABLE_ADD_COLUMN_IS_APP_TICKET);
                    db.execSQL(DB_MyTickets.UPDATE_TABLE_ADD_COLUMN_IS_APP_TICKET);
                }
            } else {
                db.execSQL(DB_BookingHistory.CREATE_TABLE_SYNTAX);
            }
            db.execSQL(DB_TicketList.CREATE_TABLE_SYNTAX);
            db.execSQL(DB_MyTickets.CREATE_TABLE_SYNTAX);
            db.execSQL(DB_MyTicketList.CREATE_TABLE_SYNTAX);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // -----------------------   SEARCH HISTORY   -----------------------
    public void addSearchHistory(String keyword, String userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String table = DB_Search.TABLE_NAME;

        // Delete Duplicate Value
        if (keyword.contains("'")) {
            keyword = keyword.replace("'", "%s");
            Log.e(TAG, "keyword: " + keyword);
        }

        Log.e(TAG, "getSearchHistory(): " + getSearchHistory(userId));

        //DELETE FROM search WHERE search_key LIKE 'Test 1 Red' Play Single Day'
        //try {
        int delete = db.delete(table, DB_Search.SEARCH_KEY + " LIKE '" + keyword + "'", null);
        Log.e(TAG, "delete: " + delete);
        // }catch (Exception e){
        //     e.printStackTrace();
        //  }

        //.....Insert new Value....//
        Log.e(TAG, "getContentValue: " + DB_Search.getContentValue(keyword, userId));
        db.insert(table, null, DB_Search.getContentValue(keyword, userId));

        String selectQuery = "SELECT  * FROM " + table;

        Cursor c = db.rawQuery(selectQuery, null);

        if (c.getCount() > 10) {
            int delCnt = c.getCount() - 10;
            db.execSQL("DELETE FROM " + table + " WHERE " + DB_Search.SEARCH_KEY + " IN (SELECT " + DB_Search.SEARCH_KEY + " FROM " + table + " LIMIT " + delCnt + ")");
        }

        db.close();
    }

    public List<Search_db> getSearchHistory(String userId) {
        List<Search_db> SearchHistory = new ArrayList<>();
        try {
            String selectQuery = "SELECT  * FROM " + DB_Search.TABLE_NAME + " WHERE " + DB_Search.USER_ID + "='" + userId + "'";
            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            if (c != null && c.moveToLast()) {
                do {
                    Search_db td = new Search_db(c.getInt(c.getColumnIndex(DB_Search.USER_ID)), c.getString(c.getColumnIndex(DB_Search.SEARCH_KEY)).replace("%s", "'"));

                    SearchHistory.add(td);
                } while (c.moveToPrevious());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return SearchHistory;
    }

    // -----------------------   BookingHistory   -----------------------
    public void addBookingHistory(BookingHistory history, String userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String table = DB_BookingHistory.TABLE_NAME;

        db.delete(table, DB_BookingHistory.TRANS_ID + "=?", new String[]{String.valueOf(history.trans_id)});

        ContentValues values = new ContentValues();
        values.put(DB_BookingHistory.D_TIME_SLOT, history.d_time_slot);
        values.put(DB_BookingHistory.IMAGE_ID, history.detail_image_id);
        values.put(DB_BookingHistory.IMAGE_URL, history.detail_image_url);
        values.put(DB_BookingHistory.EVENT_TYPE, history.type);
        values.put(DB_BookingHistory.EVENT_TYPE_ICON, history.e_type_icon);
        values.put(DB_BookingHistory.EVENT_ID, history.event_id);
        values.put(DB_BookingHistory.LATITUDE, history.latitude);
        values.put(DB_BookingHistory.LONGITUDE, history.longitude);
        values.put(DB_BookingHistory.DATE, history.event_date);
        values.put(DB_BookingHistory.DATE_TO, history.event_date_to);
        values.put(DB_BookingHistory.DAY_NAME, history.getDay());
        values.put(DB_BookingHistory.DAY_TYPE, history.day_type);
        values.put(DB_BookingHistory.M_D_DATE, history.getMDDate());
        values.put(DB_BookingHistory.Y_DATE, history.getYear());
        values.put(DB_BookingHistory.LOCATION_NAME, history.location_name);
        values.put(DB_BookingHistory.NAME, history.event_name);
        values.put(DB_BookingHistory.TIME, history.event_time);
        values.put(DB_BookingHistory.CATEGORY, history.category);
        values.put(DB_BookingHistory.LANGUAGE, history.language);
        values.put(DB_BookingHistory.AMOUNT, history.amount);
        values.put(DB_BookingHistory.IS_M_TICKET, history.ismticket + "");
        values.put(DB_BookingHistory.EMAIL, history.email);
        values.put(DB_BookingHistory.MOBILE_NO, history.mobileno);
        values.put(DB_BookingHistory.P_D_ADDRESS, history.p_d_address);
        values.put(DB_BookingHistory.PAYMENT_ID, history.payment_id);
        values.put(DB_BookingHistory.PAYMENT_STATUS, history.payment_status);
        values.put(DB_BookingHistory.PAYMENT_TYPE, history.payment_type);
        values.put(DB_BookingHistory.QR_CODE, history.qr_code);
        values.put(DB_BookingHistory.QR_CODE_URL, history.qr_code_url);
        //values.put(DB_BookingHistory.TOTAL_TICKET, history.total_ticket);
        values.put(DB_BookingHistory.TOTAL_TICKET, history.noofticket);
        values.put(DB_BookingHistory.TRANS_DATE, history.trans_date);
        values.put(DB_BookingHistory.TRANS_ID, history.trans_id);
        values.put(DB_BookingHistory.TRANS_NO, history.trans_no);
        values.put(DB_BookingHistory.TRANS_TYPE, history.trans_type);
        values.put(DB_BookingHistory.SEASON_PASS, history.seasonpass + "");
        values.put(DB_BookingHistory.USER_ID, userId);

        //added by Shreya on 10-5-2018
        values.put(DB_BookingHistory.NET_PAYBLE_AMOUNT, history.netamount);
        values.put(DB_BookingHistory.DISCOUNT, history.discount);
        values.put(DB_BookingHistory.COUPON_ID, history.coupon_id);
        values.put(DB_BookingHistory.COUPON_CODE, history.coupon_code);

        //added by priyanka
        values.put(DB_BookingHistory.TOTAL_PROCESSING_FEE, history.total_processingfee);
        values.put(DB_BookingHistory.PROCESSING_FEE, history.processingfee);
        values.put(DB_BookingHistory.TAX, history.tax);
        values.put(DB_BookingHistory.TAX_TYPE, history.tax_type);
        values.put(DB_BookingHistory.IS_TICKET_LIVE, String.valueOf(history.is_live_ticket));
        values.put(DB_BookingHistory.IS_APP_TICKET, String.valueOf(history.is_app_ticket));

        List<String> seatNo = new ArrayList<>();
        String seatType = "";
        for (int i = 0; i < history.ticket_list.size(); i++) {
            addBookingTicketList(db, history.ticket_list.get(i), userId);
            if (Validate.isNotNull(history.ticket_list.get(i).seat_no))
                seatNo.add(history.ticket_list.get(i).seat_no);
            seatType = Validate.isNotNull(history.ticket_list.get(i).seat_type) ? history.ticket_list.get(i).seat_type : "";
        }

        history.Seat_No = TextUtils.join(", ", seatNo);
        history.Seat_Type = seatType;
        values.put(DB_BookingHistory.SEAT_NO, history.Seat_No);
        values.put(DB_BookingHistory.SEAT_TYPE, history.Seat_Type);

        //.....Insert new Value....//
        db.insert(table, null, values);
        db.close();
    }

    public ArrayList<BookingHistory> getBookingHistory(String UserId) {
        ArrayList<BookingHistory> bookingHistoryList = new ArrayList<>();
        try {

            String selectQuery = "SELECT  * FROM " + DB_BookingHistory.TABLE_NAME + " WHERE " + DB_BookingHistory.USER_ID + "='" + UserId + "'";
            // String selectQuery = "SELECT  * FROM " + DB_BookingHistory.TABLE_NAME;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            if (c != null && c.moveToLast()) {
                do {
                    BookingHistory history = DB_BookingHistory.getDataFields(c);
                    bookingHistoryList.add(history);
                } while (c.moveToPrevious());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return bookingHistoryList;
    }


    public void clearBookingHistoryTable(String UserId) {
        SQLiteDatabase db = this.getWritableDatabase();
        //db.delete(DB_BookingHistory.TABLE_NAME, DB_BookingHistory.USER_ID + "=" + UserId, null);
        db.delete(DB_BookingHistory.TABLE_NAME, DB_BookingHistory.USER_ID + "='" + UserId+"'", null);
    }

    public BookingHistory getBookingHistoryByTransNo(String trans_no, String userId) {

        String selectQuery = "SELECT  * FROM " + DB_BookingHistory.TABLE_NAME + " WHERE " + DB_BookingHistory.USER_ID + "='" + userId + "' AND " + DB_BookingHistory.TRANS_NO + "='" + trans_no + "'";
        // String selectQuery = "SELECT  * FROM " + DB_BookingHistory.TABLE_NAME;

        BookingHistory history = new BookingHistory();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null && c.moveToLast()) {
            do {
                history = DB_BookingHistory.getDataFields(c);
                history.ticket_list = getBookingTicketList(db, history.getBookingId(), userId);
            } while (c.moveToPrevious());
        }

        return history;
    }


    public BookingHistory getBookingHistoryById(long transId, String userId) {

        printBookingHistoryTickets(userId);
        String selectQuery = "SELECT  * FROM " + DB_BookingHistory.TABLE_NAME
                + " WHERE " + DB_BookingHistory.TRANS_ID + "=" + transId + " AND " + DB_BookingHistory.USER_ID + "='" + userId + "'";

        BookingHistory history = new BookingHistory();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null && c.moveToLast()) {
            do {
                history = DB_BookingHistory.getDataFields(c);
                history.ticket_list = getBookingTicketList(db, history.getBookingId(), userId);

            } while (c.moveToPrevious());
        }

        return history;
    }

    private void printBookingHistoryTickets(String userId) {
        ArrayList<BookingHistory> his = getBookingHistory(userId);
        for (int i = 0; i < his.size(); i++) {
            for (int j = 0; j < his.get(i).ticket_list.size(); j++) {

                Log.e(TAG, "-------------------------");
                Log.e("getBookingHistoryById ", "bookingId:" + his.get(i).ticket_list.get(j).bookingId());
                Log.e("getBookingHistoryById ", "ticketId:" + his.get(i).ticket_list.get(j).ticketId());
                Log.e("getBookingHistoryById ", "getTransId:" + his.get(i).ticket_list.get(j).getTransId());
            }
        }
    }

    // -----------------------   BookingTicketList   -----------------------
    private void addBookingTicketList(SQLiteDatabase db, BookingTicketList ticketList, String userId) {
        try {
            String table = DB_TicketList.TABLE_NAME;

            db.delete(table, DB_TicketList.TRANS_ID + "=?", new String[]{String.valueOf(ticketList.trans_id)});

            ContentValues values = new ContentValues();
            values.put(DB_TicketList.TRANS_ID, ticketList.trans_id);
            values.put(DB_TicketList.TRANS_NO, ticketList.trans_no);
            values.put(DB_TicketList.BOOKING_ID, ticketList.booking_id);
            values.put(DB_TicketList.NAME, ticketList.event_name);
            values.put(DB_TicketList.CATEGORY, ticketList.category_name);
            values.put(DB_TicketList.LANGUAGE, ticketList.language);
            values.put(DB_TicketList.IMAGE_ID, ticketList.detail_image_id);
            values.put(DB_TicketList.IMAGE_URL, ticketList.detail_image_url);
            values.put(DB_TicketList.DATE, ticketList.event_date);
            values.put(DB_TicketList.DATE_TO, ticketList.event_date_to);
            values.put(DB_TicketList.DAY_NAME, ticketList.getDayName());
            values.put(DB_TicketList.DAY_TYPE, ticketList.day_type);
            values.put(DB_TicketList.D_M_DATE, ticketList.getD_M_Date());
            values.put(DB_TicketList.TIME, ticketList.event_time);
            values.put(DB_TicketList.EVENT_TYPE, ticketList.type);
            values.put(DB_TicketList.LATITUDE, ticketList.latitude);
            values.put(DB_TicketList.LONGITUDE, ticketList.longitude);
            values.put(DB_TicketList.LOCATION_NAME, ticketList.location_name);
            values.put(DB_TicketList.NO_OF_TICKETS, ticketList.noofticket);
            values.put(DB_TicketList.QR_CODE, ticketList.qr_code);
            values.put(DB_TicketList.QR_CODE_URL, ticketList.qr_code_url);
            values.put(DB_TicketList.RATE, ticketList.rate);
            values.put(DB_TicketList.SEAT_NO, ticketList.seat_no);
            values.put(DB_TicketList.SEAT_TYPE, ticketList.seat_type);
            values.put(DB_TicketList.STATUS, ticketList.status);
            values.put(DB_TicketList.SHARE_TO, ticketList.share_to + "");
            values.put(DB_TicketList.SHARE_FROM, ticketList.share_from + "");
            values.put(DB_TicketList.SEASON_PASS, ticketList.seasonpass + "");
            values.put(DB_TicketList.TICKET_DESC, ticketList.ticket_desc);
            values.put(DB_TicketList.Y_DATE, ticketList.getYear());
            values.put(DB_TicketList.USER_ID, userId);
            values.put(DB_TicketList.IS_TICKET_LIVE, String.valueOf(ticketList.is_live_ticket));
            values.put(DB_TicketList.IS_APP_TICKET, String.valueOf(ticketList.is_app_ticket));
            //.....Insert new Value....//
            db.insert(table, null, values);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private ArrayList<BookingTicketList> getBookingTicketList(SQLiteDatabase db, String bookingId, String userId) {
        ArrayList<BookingTicketList> bookingTicketList = new ArrayList<>();
       String selectQuery = "SELECT  * FROM " + DB_TicketList.TABLE_NAME +
                " WHERE " + DB_TicketList.BOOKING_ID + "='" + bookingId + "' AND " + DB_TicketList.USER_ID + "='" + userId + "'";

      //  String selectQuery = "SELECT  * FROM " + DB_TicketList.TABLE_NAME +
        //        " WHERE " + DB_TicketList.TRANS_NO + "='" + bookingId + "' AND " + DB_TicketList.USER_ID + "='" + userId + "'";


        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null && c.moveToLast()) {
            do {
                BookingTicketList history = DB_TicketList.getDataFields(c);

                bookingTicketList.add(history);
            } while (c.moveToPrevious());
        }

        return bookingTicketList;
    }

    public BookingTicketList getBookingTicketListDataByTansId(SQLiteDatabase db, long transId, String userId) {
        if (db == null) db = this.getReadableDatabase();

        BookingTicketList bookingTicketList = new BookingTicketList();
        String selectQuery = "SELECT  * FROM " + DB_TicketList.TABLE_NAME +
                " WHERE " + DB_TicketList.TRANS_ID + "='" + transId + "'" +
                " AND " + DB_TicketList.USER_ID + "='" + userId + "'";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null && c.moveToLast()) {
            do {
                bookingTicketList = DB_TicketList.getDataFields(c);
            } while (c.moveToPrevious());
        }
        return bookingTicketList;
    }

    public void updateBookingTicketListStatus(long tranId, String userId, String status) {
        // Log.e(TAG, "updateBookingTicketListStatus = tranId=" + tranId + "  userId=" + userId + "  status=" + status);
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String table = DB_TicketList.TABLE_NAME;

            ContentValues values = new ContentValues();
            values.put(DB_TicketList.STATUS, status);
            values.put(DB_TicketList.USER_ID, userId);

            db.update(table, values, DB_TicketList.TRANS_ID + "=" + tranId, null);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void updateBookingTicketListStatusShared(long tranId, String userId, String status, String sharedWith) {
        // Log.e(TAG, "updateBookingTicketListStatusShared = tranId=" + tranId + "  userId=" + userId + "  status=" + status + "  sharedWith=" + sharedWith);
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String table = DB_TicketList.TABLE_NAME;

            ContentValues values = new ContentValues();
            values.put(DB_TicketList.STATUS, status);
            values.put(DB_TicketList.SHARE_TO, sharedWith);
            values.put(DB_TicketList.USER_ID, userId);

            db.update(table, values, DB_TicketList.TRANS_ID + "=" + tranId, null);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void clearBookingTicketListTable(String userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        //db.delete(DB_TicketList.TABLE_NAME, DB_TicketList.USER_ID + "=" + userId, null);
        db.delete(DB_TicketList.TABLE_NAME, DB_TicketList.USER_ID + "='" + userId+"'", null);
    }

    // -------------------   BookingHistory & BookingTicketList   -------------------
    public void startDownloadingBookingHistoryInBackground() {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String table = DB_BookingHistory.TABLE_NAME;

            String selectQuery = "SELECT  * FROM " + table;
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<File> files = new ArrayList<>();

            if (c != null && c.moveToLast()) {
                do {
                    files.add(getFile(files.size(), c.getString(c.getColumnIndex(DB_BookingHistory.IMAGE_URL)), c.getString(c.getColumnIndex(DB_BookingHistory.IMAGE_ID))));

                    if (Validate.isNotNull(c.getString(c.getColumnIndex(DB_BookingHistory.QR_CODE_URL)))) {
                        files.add(getFile(files.size(), Constant.generateQRCodeURL1(c.getString(c.getColumnIndex(DB_BookingHistory.QR_CODE_URL))), c.getString(c.getColumnIndex(DB_BookingHistory.QR_CODE)) + ".png"));
                    } else {
                        files.add(getFile(files.size(), Constant.generateQRCodeURL(c.getString(c.getColumnIndex(DB_BookingHistory.QR_CODE))), c.getString(c.getColumnIndex(DB_BookingHistory.QR_CODE)) + ".png"));
                    }
                } while (c.moveToPrevious());
            }

            table = DB_TicketList.TABLE_NAME;

            selectQuery = "SELECT  * FROM " + table;
            c = db.rawQuery(selectQuery, null);

            if (c != null && c.moveToLast()) {
                do {
                    if (Validate.isNotNull(c.getString(c.getColumnIndex(DB_BookingHistory.QR_CODE_URL)))) {
                        files.add(getFile(files.size(), Constant.generateQRCodeURL1(c.getString(c.getColumnIndex(DB_BookingHistory.QR_CODE_URL))), c.getString(c.getColumnIndex(DB_BookingHistory.QR_CODE)) + ".png"));
                    } else {
                        files.add(getFile(files.size(), Constant.generateQRCodeURL(c.getString(c.getColumnIndex(DB_TicketList.QR_CODE))), c.getString(c.getColumnIndex(DB_TicketList.QR_CODE)) + ".png"));
                    }
                } while (c.moveToPrevious());
            }

            Intent intent = new Intent(mContext, DownloadingService.class);
            intent.putParcelableArrayListExtra("files", new ArrayList<>(files));
            mContext.startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void startDownloadingMyTicketsInBackground() {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String table = DB_MyTickets.TABLE_NAME;

            String selectQuery = "SELECT  * FROM " + table;
            Cursor c = db.rawQuery(selectQuery, null);
            ArrayList<File> files = new ArrayList<>();

            if (c != null && c.moveToLast()) {
                do {
                    files.add(getFile(files.size(), c.getString(c.getColumnIndex(DB_MyTickets.IMAGE_URL)),
                            c.getString(c.getColumnIndex(DB_MyTickets.IMAGE_ID))));
                } while (c.moveToPrevious());
            }

            table = DB_MyTicketList.TABLE_NAME;

            selectQuery = "SELECT  * FROM " + table;
            c = db.rawQuery(selectQuery, null);

            if (c != null && c.moveToLast()) {
                do {
                    if (Validate.isNotNull(c.getString(c.getColumnIndex(DB_BookingHistory.QR_CODE_URL)))) {
                        files.add(getFile(files.size(), Constant.generateQRCodeURL1(c.getString(c.getColumnIndex(DB_BookingHistory.QR_CODE_URL))), c.getString(c.getColumnIndex(DB_BookingHistory.QR_CODE)) + ".png"));
                    } else {
                        files.add(getFile(files.size(), Constant.generateQRCodeURL(c.getString(c.getColumnIndex(DB_MyTicketList.QR_CODE))), c.getString(c.getColumnIndex(DB_MyTicketList.QR_CODE)) + ".png"));
                    }
                } while (c.moveToPrevious());
            }

            Intent intent = new Intent(mContext, DownloadingService.class);
            intent.putParcelableArrayListExtra("files", new ArrayList<>(files));
            mContext.startService(intent);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }


    public BookingHistory getMyTicketByEventId(long eventId, String userId) {
        String selectQuery = "SELECT  * FROM " + DB_MyTickets.TABLE_NAME
                + " WHERE " + DB_MyTickets.EVENT_ID + "=" + eventId + " AND " + DB_MyTickets.USER_ID + "='" + userId + "'";

        BookingHistory history = new BookingHistory();

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null && c.moveToLast()) {
            do {
                history.event_id = c.getInt(c.getColumnIndex(DB_MyTickets.EVENT_ID));
                history.event_name = c.getString(c.getColumnIndex(DB_MyTickets.NAME));
                history.category = c.getString(c.getColumnIndex(DB_MyTickets.CATEGORY));
                history.language = c.getString(c.getColumnIndex(DB_MyTickets.LANGUAGE));
                history.detail_image_id = c.getString(c.getColumnIndex(DB_MyTickets.IMAGE_ID));
                history.detail_image_url = c.getString(c.getColumnIndex(DB_MyTickets.IMAGE_URL));
                history.event_date = c.getString(c.getColumnIndex(DB_MyTickets.DATE));
                history.event_date_to = c.getString(c.getColumnIndex(DB_MyTickets.DATE_TO));
                history.day_type = c.getString(c.getColumnIndex(DB_MyTickets.DAY_TYPE));
                history.event_time = c.getString(c.getColumnIndex(DB_MyTickets.TIME));
                history.type = c.getString(c.getColumnIndex(DB_MyTickets.EVENT_TYPE));
                history.location_name = c.getString(c.getColumnIndex(DB_MyTickets.LOCATION_NAME));
                history.latitude = c.getString(c.getColumnIndex(DB_MyTickets.LATITUDE));
                history.longitude = c.getString(c.getColumnIndex(DB_MyTickets.LONGITUDE));
                history.Seat_No = c.getString(c.getColumnIndex(DB_MyTickets.SEAT_NO));
                history.Seat_Type = c.getString(c.getColumnIndex(DB_MyTickets.SEAT_TYPE));
                history.USER_ID = userId;

                if (c.getString(c.getColumnIndex(DB_MyTickets.IS_LIVE_TICKET)) != null && !TextUtils.isEmpty(c.getString(c.getColumnIndex(DB_MyTickets.IS_LIVE_TICKET)))) {
                    history.is_live_ticket = Integer.parseInt(c.getString(c.getColumnIndex(DB_MyTickets.IS_LIVE_TICKET)));
                } else {
                    history.is_live_ticket = 0;
                }

                if (c.getString(c.getColumnIndex(DB_MyTickets.IS_APP_TICKET)) != null && !TextUtils.isEmpty(c.getString(c.getColumnIndex(DB_MyTickets.IS_APP_TICKET)))) {
                    history.is_app_ticket = Integer.parseInt(c.getString(c.getColumnIndex(DB_MyTickets.IS_APP_TICKET)));
                } else {
                    history.is_app_ticket = 0;
                }


            } while (c.moveToPrevious());
        }

        history.ticket_list = getMyTicketList(db, history.getEventId(), userId);
        return history;
    }

    // -------------------   MY TICKETS   -------------------
    public void addMyTickets(User_Ticket_Event userTicketEvent, String userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String table = DB_MyTickets.TABLE_NAME;

            db.delete(table, DB_MyTickets.EVENT_ID + "=?", new String[]{String.valueOf(userTicketEvent.event_id)});

            ContentValues values = new ContentValues();
            values.put(DB_MyTickets.EVENT_ID, userTicketEvent.event_id);
            values.put(DB_MyTickets.NAME, userTicketEvent.event_name);
            values.put(DB_MyTickets.CATEGORY, userTicketEvent.category);
            values.put(DB_MyTickets.LANGUAGE, userTicketEvent.language);
            values.put(DB_MyTickets.IMAGE_ID, userTicketEvent.Detail_Image_Id);
            values.put(DB_MyTickets.IMAGE_URL, userTicketEvent.detail_image_url);
            values.put(DB_MyTickets.DATE, userTicketEvent.event_date);
            values.put(DB_MyTickets.DATE_TO, userTicketEvent.event_date_to);
            values.put(DB_MyTickets.DAY_TYPE, userTicketEvent.day_type);
            values.put(DB_MyTickets.TIME, userTicketEvent.event_time);
            values.put(DB_MyTickets.EVENT_TYPE, userTicketEvent.type);
            values.put(DB_MyTickets.LOCATION_NAME, userTicketEvent.location_name);
            values.put(DB_MyTickets.LATITUDE, userTicketEvent.latitude);
            values.put(DB_MyTickets.LONGITUDE, userTicketEvent.longitude);
            values.put(DB_MyTickets.USER_ID, userId);
            values.put(DB_MyTickets.IS_LIVE_TICKET, String.valueOf(userTicketEvent.is_live_ticket));
            values.put(DB_MyTickets.IS_APP_TICKET, String.valueOf(userTicketEvent.is_app_ticket));

            try {
                List<String> seatNo = new ArrayList<>();
                String seatType = "";
                if (userTicketEvent.getTicketList() != null) {

                    for (int i = 0; i < userTicketEvent.getTicketList().size(); i++) {
                        userTicketEvent.getTicketList().get(i).event_id = userTicketEvent.event_id;
                        addMyTicketList(db, userTicketEvent.getTicketList().get(i), userId);
                        if (Validate.isNotNull(userTicketEvent.getTicketList().get(i).seat_no))
                            seatNo.add(userTicketEvent.getTicketList().get(i).seat_no);
                        seatType = Validate.isNotNull(userTicketEvent.getTicketList().get(i).seat_type) ? userTicketEvent.getTicketList().get(i).seat_type : "";
                    }
                }
                userTicketEvent.Seat_No = TextUtils.join(", ", seatNo);
                userTicketEvent.Seat_Type = seatType;
            }catch (Exception e){
                e.printStackTrace();
            }

            values.put(DB_MyTickets.SEAT_NO, userTicketEvent.Seat_No);
            values.put(DB_MyTickets.SEAT_TYPE, userTicketEvent.Seat_Type);

            //.....Insert new Value....//
            long insert = db.insert(table, null, values);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public ArrayList<User_Ticket_Event> getMyTickets(String userId) {
        ArrayList<User_Ticket_Event> bookingHistoryList = new ArrayList<>();
        try {
            String selectQuery = "SELECT  * FROM " + DB_MyTickets.TABLE_NAME + " WHERE " + DB_MyTickets.USER_ID + "='" + userId + "'";
            // String selectQuery = "SELECT  * FROM " + DB_BookingHistory.TABLE_NAME;

            SQLiteDatabase db = this.getReadableDatabase();
            Cursor c = db.rawQuery(selectQuery, null);

            if (c != null && c.moveToLast()) {
                do {
                    User_Ticket_Event history = new User_Ticket_Event();

                    history.event_id = c.getInt(c.getColumnIndex(DB_MyTickets.EVENT_ID));
                    history.event_name = c.getString(c.getColumnIndex(DB_MyTickets.NAME));
                    history.category = c.getString(c.getColumnIndex(DB_MyTickets.CATEGORY));
                    history.language = c.getString(c.getColumnIndex(DB_MyTickets.LANGUAGE));
                    history.Detail_Image_Id = c.getString(c.getColumnIndex(DB_MyTickets.IMAGE_ID));
                    history.detail_image_url = c.getString(c.getColumnIndex(DB_MyTickets.IMAGE_URL));
                    history.event_date = c.getString(c.getColumnIndex(DB_MyTickets.DATE));
                    history.event_date_to = c.getString(c.getColumnIndex(DB_MyTickets.DATE_TO));
                    history.day_type = c.getString(c.getColumnIndex(DB_MyTickets.DAY_TYPE));
                    history.event_time = c.getString(c.getColumnIndex(DB_MyTickets.TIME));
                    history.type = c.getString(c.getColumnIndex(DB_MyTickets.EVENT_TYPE));
                    history.location_name = c.getString(c.getColumnIndex(DB_MyTickets.LOCATION_NAME));
                    history.latitude = c.getString(c.getColumnIndex(DB_MyTickets.LATITUDE));
                    history.longitude = c.getString(c.getColumnIndex(DB_MyTickets.LONGITUDE));
                    history.Seat_No = c.getString(c.getColumnIndex(DB_MyTickets.SEAT_NO));
                    history.Seat_Type = c.getString(c.getColumnIndex(DB_MyTickets.SEAT_TYPE));
                    history.USER_ID = userId;
                    history.ticket_list = getMyTicketList(db, history.event_id, userId);

                    if (c.getString(c.getColumnIndex(DB_MyTickets.IS_LIVE_TICKET)) != null && !TextUtils.isEmpty(c.getString(c.getColumnIndex(DB_MyTickets.IS_LIVE_TICKET)))) {
                        history.is_live_ticket = Integer.parseInt(c.getString(c.getColumnIndex(DB_MyTickets.IS_LIVE_TICKET)));
                    } else {
                        history.is_live_ticket = 0;
                    }

                    if (c.getString(c.getColumnIndex(DB_MyTickets.IS_APP_TICKET)) != null && !TextUtils.isEmpty(c.getString(c.getColumnIndex(DB_MyTickets.IS_APP_TICKET)))) {
                        history.is_app_ticket = Integer.parseInt(c.getString(c.getColumnIndex(DB_MyTickets.IS_APP_TICKET)));
                    } else {
                        history.is_app_ticket = 0;
                    }

                    bookingHistoryList.add(history);
                } while (c.moveToPrevious());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bookingHistoryList;
    }

    public void clearMyTicketTable(String UserId) {
        SQLiteDatabase db = this.getWritableDatabase();
        //db.delete(DB_MyTickets.TABLE_NAME, DB_MyTickets.USER_ID + "=" + UserId, null);
        db.delete(DB_MyTickets.TABLE_NAME, DB_MyTickets.USER_ID + "='" + UserId +"'", null);
    }

    // -----------------------   MY TICKETS - TicketList   -----------------------
    private void addMyTicketList(SQLiteDatabase db, BookingTicketList ticketList, String userId) {
        try {
            String table = DB_MyTicketList.TABLE_NAME;

            db.delete(table, DB_MyTicketList.TRANS_NO + "=?", new String[]{String.valueOf(ticketList.trans_no)});

            ContentValues values = new ContentValues();
            values.put(DB_MyTicketList.BOOKING_ID, ticketList.booking_id);
            values.put(DB_MyTicketList.EVENT_ID, ticketList.event_id);
            values.put(DB_MyTicketList.NAME, ticketList.event_name);
            values.put(DB_MyTicketList.CATEGORY, ticketList.category_name);
            values.put(DB_MyTicketList.LANGUAGE, ticketList.language);
            values.put(DB_MyTicketList.IMAGE_ID, ticketList.detail_image_id);
            values.put(DB_MyTicketList.IMAGE_URL, ticketList.detail_image_url);
            values.put(DB_MyTicketList.DATE, ticketList.event_date);
            values.put(DB_MyTicketList.DATE_TO, ticketList.event_date_to);
            values.put(DB_MyTicketList.DAY_NAME, ticketList.getDayName());
            values.put(DB_MyTicketList.DAY_TYPE, ticketList.day_type);
            values.put(DB_MyTicketList.D_M_DATE, ticketList.getD_M_Date());
            values.put(DB_MyTicketList.TIME, ticketList.event_time);
            values.put(DB_MyTicketList.EVENT_TYPE, ticketList.type);
            values.put(DB_MyTicketList.LATITUDE, ticketList.latitude);
            values.put(DB_MyTicketList.LONGITUDE, ticketList.longitude);
            values.put(DB_MyTicketList.LOCATION_NAME, ticketList.location_name);
            values.put(DB_MyTicketList.NO_OF_TICKETS, ticketList.noofticket);
            values.put(DB_MyTicketList.QR_CODE, ticketList.qr_code);
            values.put(DB_MyTicketList.QR_CODE_URL, ticketList.qr_code_url);
            values.put(DB_MyTicketList.RATE, ticketList.rate);
            values.put(DB_MyTicketList.SEAT_NO, ticketList.seat_no);
            values.put(DB_MyTicketList.SEAT_TYPE, ticketList.seat_type);
            values.put(DB_MyTicketList.STATUS, ticketList.status);
            values.put(DB_MyTicketList.SHARE_TO, ticketList.share_to + "");
            values.put(DB_MyTicketList.SHARE_FROM, ticketList.share_from + "");
            values.put(DB_MyTicketList.SEASON_PASS, ticketList.seasonpass + "");
            values.put(DB_MyTicketList.TICKET_DESC, ticketList.ticket_desc);
            values.put(DB_MyTicketList.TRANS_ID, ticketList.trans_id);
            values.put(DB_MyTicketList.TRANS_NO, ticketList.trans_no);
            values.put(DB_MyTicketList.Y_DATE, ticketList.getYear());
            values.put(DB_MyTicketList.USER_ID, userId);
            values.put(DB_MyTicketList.IS_TICKET_LIVE, String.valueOf(ticketList.is_live_ticket));
            values.put(DB_MyTicketList.IS_APP_TICKET, String.valueOf(ticketList.is_app_ticket));

            //.....Insert new Value....//
            db.insert(table, null, values);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<BookingTicketList> getMyTicketList(SQLiteDatabase db, long eventId, String userId) {
        if (db == null) db = this.getReadableDatabase();

        ArrayList<BookingTicketList> bookingTicketList = new ArrayList<>();
        try {
            String selectQuery = "SELECT  * FROM " + DB_MyTicketList.TABLE_NAME +
                    " WHERE " + DB_MyTicketList.EVENT_ID + "='" + eventId + "' AND " + DB_MyTicketList.USER_ID + "='" + userId + "'";

            Cursor c = db.rawQuery(selectQuery, null);
            if (c != null && c.moveToLast()) {
                do {
                    BookingTicketList history = DB_MyTicketList.getDataFields(c);

                    bookingTicketList.add(history);
                } while (c.moveToPrevious());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return bookingTicketList;
    }

    public BookingTicketList getMyTicketDataByTansId(@Nullable SQLiteDatabase db, long transId, String userId) {
        if (db == null) db = this.getReadableDatabase();

        BookingTicketList bookingTicketList = new BookingTicketList();
        String selectQuery = "SELECT  * FROM " + DB_MyTicketList.TABLE_NAME +
                " WHERE " + DB_MyTicketList.TRANS_ID + "='" + transId + "' AND " + DB_MyTicketList.USER_ID + "='" + userId + "'";

        Cursor c = db.rawQuery(selectQuery, null);
        if (c != null && c.moveToLast()) {
            do {
                bookingTicketList = DB_MyTicketList.getDataFields(c);
            } while (c.moveToPrevious());
        }
        return bookingTicketList;
    }

    public void updateMyTicketListStatus(long tranId, String userId, String status) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String table = DB_MyTicketList.TABLE_NAME;

            ContentValues values = new ContentValues();
            values.put(DB_MyTicketList.STATUS, status);
            values.put(DB_MyTicketList.USER_ID, userId);

            //.....Insert new Value....//
            db.update(table, values, DB_MyTicketList.TRANS_ID + "=" + tranId, null);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void updateMyTicketListStatusShared(long tranId, String userId, String status, String sharedWith) {
        // Log.e(TAG, "updateMyTicketListStatusShared = tranId=" + tranId + "  userId=" + userId + "  status=" + status + "  sharedWith=" + sharedWith);
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            String table = DB_MyTicketList.TABLE_NAME;

            ContentValues values = new ContentValues();
            values.put(DB_MyTicketList.STATUS, status);
            values.put(DB_MyTicketList.SHARE_TO, sharedWith);
            values.put(DB_MyTicketList.USER_ID, userId);

            //.....Insert new Value....//
            db.update(table, values, DB_MyTicketList.TRANS_ID + "=" + tranId, null);

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    public void clearMyTicketListTable(String userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        try {
            //db.delete(DB_MyTicketList.TABLE_NAME, DB_MyTicketList.USER_ID + "=" + userId, null);
            db.delete(DB_MyTicketList.TABLE_NAME, DB_MyTicketList.USER_ID + "='" + userId+"'", null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteDatabase1(Context context){
        boolean delete = context.deleteDatabase(DATABASE_NAME);
        Log.e(TAG,"delete:" +delete);
    }

    public boolean doesDatabaseExist(Context context) {
        SQLiteDatabase db = this.getWritableDatabase();
         java.io.File dbFile = context.getDatabasePath(DatabaseHelper.DATABASE_NAME);
        Log.e(TAG,"checkdb:"+dbFile.exists());
        return dbFile.exists();
    }

    public void reCreteDatabase(Context context) {
        SQLiteDatabase db = this.getWritableDatabase();
        if (!doesDatabaseExist(context)){
            onCreate(db);
        }
    }

}
