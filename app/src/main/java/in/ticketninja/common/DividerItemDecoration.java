/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.common;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import androidx.annotation.ColorInt;
import androidx.annotation.Dimension;
import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import java.util.Objects;

import in.ticketninja.R;


/**
 * ContactSlectable(in.nichetech.widget) <br />
 * Developed by <b><a href="http://RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 4/7/16.
 *
 * @author Suthar Rohit
 */
public class DividerItemDecoration extends RecyclerView.ItemDecoration {

    // private final String TAG = "DividerItemDecoration";
    private Drawable mDivider;
    private int leftMargin;
    private int rightMargin;


    public DividerItemDecoration(@NonNull Context context) {
        mDivider = ContextCompat.getDrawable(context, R.drawable.divider_horizontal);
        this.leftMargin = 0;
        this.rightMargin = 0;
    }

    public DividerItemDecoration(@NonNull Context context, @ColorInt int color, int leftMargin, int rightMargin) {
        mDivider = ContextCompat.getDrawable(context, R.drawable.divider_horizontal);
        Objects.requireNonNull(mDivider).setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        this.leftMargin = Utility.intToDP(context, leftMargin);
        this.rightMargin = Utility.intToDP(context, rightMargin);
    }

    public DividerItemDecoration(@NonNull Context context, @Dimension int leftMargin) {
        this(context, leftMargin, 0);
    }

    public DividerItemDecoration(@NonNull Context context, @Dimension int leftMargin, @Dimension int rightMargin) {
        mDivider = ContextCompat.getDrawable(context, R.drawable.divider_horizontal);
        this.leftMargin = Utility.intToDP(context, leftMargin);
        this.rightMargin = Utility.intToDP(context, rightMargin);
    }

    @Override
    public void getItemOffsets(@NonNull Rect outRect, @NonNull View view, @NonNull RecyclerView parent,@NonNull RecyclerView.State state) {
        super.getItemOffsets(outRect, view, parent, state);
        if (parent.getChildAdapterPosition(view) == 0) {
            return;
        }
        outRect.top = mDivider.getIntrinsicHeight();
    }


    @Override
    public void onDraw(@NonNull Canvas canvas, @NonNull RecyclerView parent,@NonNull RecyclerView.State state) {
        int dividerLeft = parent.getPaddingLeft() + leftMargin;
        int dividerRight = parent.getWidth() - parent.getPaddingRight() - rightMargin;

        int childCount = parent.getChildCount();
        for (int i = 0; i < childCount - 1; i++) {
            View child = parent.getChildAt(i);

            RecyclerView.LayoutParams params = (RecyclerView.LayoutParams) child.getLayoutParams();

            int dividerTop = child.getBottom() + params.bottomMargin;
            int dividerBottom = dividerTop + mDivider.getIntrinsicHeight();

            mDivider.setBounds(dividerLeft, dividerTop, dividerRight, dividerBottom);
            mDivider.draw(canvas);
        }
    }
}
