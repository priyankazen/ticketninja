/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.common;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import in.ticketninja.R;
import in.ticketninja.activity.SplashActivity;

/**
 * MB_Working(com.nichetech.common) <br />
 * Developed by <b><a href="http://RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 27/9/16.
 *
 * @author Suthar Rohit
 */
public class NotificationUtils {

    LoginUtils mLogin;


    /* -=-=-=-=-=-=-=-=-=-=-=-=-=- ADD NOTIFICATION INTO PREFERENCES -=-=-=-=-=-=-=-=-=-=-=- */
    private static final String TAG = NotificationUtils.class.getSimpleName();

    /* -=-=-=-=-=-=-=-=-=-=-=-=-=- SHOW NOTIFICATION -=-=-=-=-=-=-=-=-=-=-=- */
    public static void showNotification(Context context, String msg) {
        showNotification(context, msg, 0, null, "", 0);
    }

    public static void showNotification(Context context, String msg, PendingIntent intent) {
        showNotification(context, msg, 0, intent, "", 0);
    }


    public static void showNotification(Context context, String msg, int msgId) {
        showNotification(context, msg, msgId, null, "", 0);
    }

    public static void showNotification(Context context, String msg, int msgId, PendingIntent intent) {
        showNotification(context, msg, msgId, intent, "", 0);
    }

    public static void showNotification(Context context, String msg, String Nid, long Eid) {
        showNotification(context, msg, 0, null, Nid, Eid);
    }

    public static void showNotification(Context context, String msg, int msgId,
                                        PendingIntent intent, String Nid, long Eid) {
        try {
            if (intent == null) {
                Intent noteIntent = new Intent(context, SplashActivity.class);
                noteIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent = PendingIntent.getActivity(context, 0, noteIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            }


            Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, NotificationChannelUtils.CHANNEL_ID);
            mBuilder.setContentTitle(context.getString(R.string.app_name));
            mBuilder.setTicker(context.getString(R.string.app_name));
            mBuilder.setWhen(System.currentTimeMillis());
            mBuilder.setContentText(msg);
            mBuilder.setSmallIcon(R.drawable.ic_notification);
            mBuilder.setLargeIcon(icon);
            mBuilder.setDefaults(Notification.DEFAULT_ALL);
            mBuilder.setContentIntent(intent);
            mBuilder.setAutoCancel(true);
            mBuilder.setPriority(Notification.PRIORITY_HIGH);
            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
            //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            mBuilder.setColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
            //if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            //    mBuilder.setChannelId(NotificationChannelUtils.CHANNEL_ID);

            NotificationManagerCompat mNotificationManager = NotificationManagerCompat.from(context);
            if (msgId > 0) {
                mNotificationManager.notify(msgId, mBuilder.build());
            } else {
                int id = (int) System.currentTimeMillis();
                mNotificationManager.notify(id, mBuilder.build());
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void showImageNotification(Context context, String msg, int msgId, String imageURL,
                                             final PendingIntent intent1, String Nid, long Eid) {

        try {
            Glide.with(context)
                    .load(imageURL)
                    .skipMemoryCache(false)
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {

                            PendingIntent intent = intent1;

                            if (intent == null) {
                                Intent noteIntent = new Intent(context, SplashActivity.class);
                                noteIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                intent = PendingIntent.getActivity(context, 0, noteIntent, PendingIntent.FLAG_UPDATE_CURRENT);
                            }

                            Bitmap icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
                            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, NotificationChannelUtils.CHANNEL_ID);
                            mBuilder.setContentTitle(context.getString(R.string.app_name));
                            mBuilder.setTicker(context.getString(R.string.app_name));
                            mBuilder.setWhen(System.currentTimeMillis());
                            mBuilder.setContentText(msg);
                            mBuilder.setSmallIcon(R.drawable.ic_notification);
                            mBuilder.setLargeIcon(icon);
                            mBuilder.setDefaults(Notification.DEFAULT_ALL);
                            mBuilder.setContentIntent(intent);
                            mBuilder.setAutoCancel(true);
                            mBuilder.setPriority(Notification.PRIORITY_HIGH);
                            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(msg));
                            // if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                            mBuilder.setColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
                        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                            mBuilder.setChannelId(NotificationChannelUtils.CHANNEL_ID);*/

                            NotificationManagerCompat mNotificationManager = NotificationManagerCompat.from(context);
                            if (msgId > 0) {
                                mNotificationManager.notify(msgId, mBuilder.build());
                            } else {
                                int id = (int) System.currentTimeMillis();
                                mNotificationManager.notify(id, mBuilder.build());
                            }
                            return true;
                        }
                    })
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
