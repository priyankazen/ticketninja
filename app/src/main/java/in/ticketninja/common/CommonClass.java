/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.common;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import androidx.appcompat.app.AlertDialog;

//import com.crashlytics.android.answers.Answers;
//import com.crashlytics.android.answers.ShareEvent;

import java.util.ArrayList;
import java.util.List;

import in.ticketninja.R;
import in.ticketninja.objects.AppVersion;
import in.ticketninja.objects.BookingTicketList;

/**
 * Matrubharti(com.nichetech.common) <br />
 * Developed by <b><a href="http://RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 15/12/15.
 *
 * @author Suthar Rohit
 */
public class CommonClass {

    private static final String TAG = CommonClass.class.getSimpleName();
    private Context context;
    private Activity activity;

    public CommonClass(Context context) {
        this.context = context;
        this.activity = (Activity) context;
    }

    public CommonClass(Context context, Activity activity) {
        this.context = context;
        this.activity = activity;
    }

    public static void showForceUpdateDialog(Context context, AppVersion appVersionData) {
        try {
            if (appVersionData != null && appVersionData.getAppVersion() > Utility.getAppVersionCode(context) && appVersionData.isForcefully()) {

                Activity activity = (Activity) context;
                AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialog_TicketNinja);
                builder.setCancelable(false);
                builder.setTitle(R.string.msg_update_app_title);
                builder.setMessage(Validate.isNotNull(appVersionData.getMessage()) ? appVersionData.getMessage() : context.getString(R.string.msg_update_app_title));
                builder.setPositiveButton(R.string.btn_update_now, (dialog, which) -> {
                    dialog.dismiss();
                    try {
                        if (Utility.isOnline(context)) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("market://details?id=" + context.getPackageName()));
                            context.startActivity(intent);
                            System.exit(0);
                        } else {
                            MessageUtils.showToast(context, R.string.msg_no_internet);
                        }
                    } catch (ActivityNotFoundException e) {
                        MessageUtils.showToast(context, R.string.msg_play_store_not_found);
                    }
                });

                //builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> System.exit(0));
                if (!activity.isFinishing()) builder.create().show();

            }else {
                Activity activity = (Activity) context;
                AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialog_TicketNinja);
                builder.setCancelable(false);
                builder.setTitle(R.string.msg_update_app_title);
                builder.setMessage(context.getString(R.string.msg_update_app_title));
                builder.setPositiveButton(R.string.btn_update_now, (dialog, which) -> {
                    dialog.dismiss();
                    try {
                        if (Utility.isOnline(context)) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            intent.setData(Uri.parse("market://details?id=" + context.getPackageName()));
                            context.startActivity(intent);
                            System.exit(0);
                        } else {
                            MessageUtils.showToast(context, R.string.msg_no_internet);
                        }
                    } catch (ActivityNotFoundException e) {
                        MessageUtils.showToast(context, R.string.msg_play_store_not_found);
                    }
                });

                //builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> System.exit(0));
                if (!activity.isFinishing()) builder.create().show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showForceUpdateDialogOffline(Context context) {
        try {
            if (PreferencesUtils.getAPP_VERSION(context) != null && Integer.parseInt(PreferencesUtils.getAPP_VERSION(context)) > Utility.getAppVersionCode(context) && PreferencesUtils.isIS_FORCEFULLY(context)) {

                Activity activity = (Activity) context;
                AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AlertDialog_TicketNinja);
                builder.setCancelable(false);
                builder.setTitle(R.string.msg_update_app_title);
                builder.setMessage(Validate.isNotNull(PreferencesUtils.getAppMessage(context)) ? PreferencesUtils.getAppMessage(context) : context.getString(R.string.msg_update_app_title));
                builder.setPositiveButton(R.string.btn_update_now, (dialog, which) -> {
                    dialog.dismiss();
                    try {
                        // if (Utility.isOnline(context)) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("market://details?id=" + context.getPackageName()));
                        context.startActivity(intent);
                        System.exit(0);
                        /*} else {

                        }*/
                    } catch (ActivityNotFoundException e) {
                        MessageUtils.showToast(context, R.string.msg_play_store_not_found);
                    }
                });
                //builder.setNegativeButton(android.R.string.cancel, (dialog, which) -> System.exit(0));
                if (!activity.isFinishing()) builder.create().show();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void openMap(Context context, String name, String latitude, String longitude) {
        if (Validate.isNotNull(latitude) && Validate.isNotNull(longitude)) {
            String geoUri = "http://maps.google.com/maps?q=loc:" + latitude + "," + longitude;
            if (Validate.isNotNull(name))
                geoUri = "http://maps.google.com/maps?q=loc:" + latitude + "," + longitude + " (" + name + ")";
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(geoUri));
            context.startActivity(intent);
        }
    }

    public static void shareEvent(Context context, long eventId, String eventName, String data) {
        try {
            if (Validate.isNotNull(data)) {
               /* try {
                    Answers.getInstance().logShare(new ShareEvent()
                            .putMethod("eBookShare")
                            .putContentName("" + eventName)
                            .putContentType("eBook")
                            .putContentId("" + eventId));
                } catch (Exception e) {
                    e.printStackTrace();
                }*/

                Intent intent1 = new Intent();
                intent1.setAction(Intent.ACTION_SEND);
                intent1.setType("text/plain");
                // intent1.putExtra(Intent.EXTRA_SUBJECT, book1.getTitle());
                intent1.putExtra(Intent.EXTRA_TEXT, data);
                //intent1.putExtra("com.facebook.platform.extra.APPLICATION_ID", getString(R.string.fb_app_id));
                context.startActivity(Intent.createChooser(intent1, "Share eBook via"));

            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static List<BookingTicketList> groupTicketList(List<BookingTicketList> ticketList) {
        List<String> catNameList = new ArrayList<>();
        List<BookingTicketList> dataList = new ArrayList<>();
        for (int j = 0; j < ticketList.size(); j++) {
            BookingTicketList ticket = ticketList.get(j);
            if (catNameList.contains(ticket.categoryName())) {
                for (int i = 0; i < dataList.size(); i++) {
                    BookingTicketList newTicket = new BookingTicketList();
                    BookingTicketList book1 = dataList.get(i);
                    newTicket.booking_id = book1.booking_id;
                    newTicket.category_name = book1.category_name;
                    newTicket.D_M_Date = book1.getD_M_Date();
                    newTicket.event_date = book1.event_date;
                    newTicket.Day_Name = book1.getDayName();
                    newTicket.detail_image_id = book1.detail_image_id;
                    newTicket.detail_image_url = book1.detail_image_url;
                    newTicket.type = book1.type;
                    newTicket.latitude = book1.latitude;
                    newTicket.longitude = book1.longitude;
                    newTicket.location_name = book1.location_name;
                    newTicket.qr_code = book1.qr_code;
                    newTicket.noofticket = book1.noofticket;
                    newTicket.rate = book1.rate;
                    newTicket.seat_no = book1.seat_no;
                    newTicket.seat_type = book1.seat_type;
                    newTicket.status = book1.status;
                    newTicket.ticket_desc = book1.ticket_desc;
                    newTicket.event_time = book1.event_time;
                    newTicket.trans_id = book1.trans_id;
                    newTicket.trans_no = book1.trans_no;
                    newTicket.Y_Date = book1.getYear();
                    if (book1.categoryName().equals(ticket.categoryName())) {
                        newTicket.noOfTicket(book1.noOfTicket() + ticket.noOfTicket());
                        newTicket.rate(book1.rate() + ticket.rate());
                        newTicket.seatNo(Validate.isNotNull(book1.seatNo()) ? String.format("%s, %s", book1.seatNo(), ticket.seatNo()) : ticket.seatNo());
                        dataList.set(i, newTicket);
                    }
                }
            } else {
                dataList.add(ticket);
                catNameList.add(ticket.categoryName());
            }
        }
        return dataList;
    }

    public Context getContext() {
        return context;
    }

    public Activity getActivity() {
        return activity;
    }

    public void appsFlyerTracking() {

    }

    public boolean isOnline() {
        return Utility.isOnline(context);
    }

    public Intent getRateNowIntent() {
        PreferencesUtils.setRateUs(context, true);
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://details?id=" + context.getPackageName()));
        return intent;
    }

    public Intent getMoreAppsIntent() {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setData(Uri.parse("market://search?q=pub:Matrubharti+Technologies+Pvt.+Ltd."));
        return intent;
    }

    public String getAppInfo() {
        return getAppVersionName() + " (" + Utility.getAppVersionCode(context) + ")";
    }

    public String getAppVersionName() {
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return pInfo.versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    // ALERT
    public void showAlert(int messageId) {
        MessageUtils.showAlert(activity, messageId);
    }

    public void showAlert(String title, String message) {
        MessageUtils.showAlert(activity, title, message);
    }

    public void showAlert(String message) {
        MessageUtils.showAlert(activity, message);
    }

    public void showAlert(int messageId, MessageUtils.OnOkClickListener listener) {
        MessageUtils.showAlert(activity, messageId, listener);
    }

    public void showAlert(String message, MessageUtils.OnOkClickListener listener) {
        MessageUtils.showAlert(activity, message, listener);
    }

    public void showAlert(int message, int Title, int postive_Button, int negative_Button, MessageUtils.OnOkClickListener listener) {
        MessageUtils.showAlert(activity, message, Title, postive_Button, negative_Button, listener);
    }

    public void showAlert(int message, int postive_Button, int negative_Button, MessageUtils.OnOkClickListener listener) {
        MessageUtils.showAlert(activity, message, postive_Button, negative_Button, listener);
    }
    public void showAlert(String message, int postive_Button, int negative_Button, MessageUtils.OnOkClickListener listener) {
        MessageUtils.showAlert(activity, message, postive_Button, negative_Button, listener);
    }
    public void showAlert(String Title,String message, int postive_Button,  MessageUtils.OnOkClickListener listener) {
        MessageUtils.showAlert(activity, Title,message , postive_Button, listener);
    }

    public void showAlert(String message, int postive_Button,  MessageUtils.OnOkClickListener listener) {
        MessageUtils.showAlert(activity, message , postive_Button, listener);
    }

    // TOAST
    public void showToast(String message) {
        MessageUtils.showToast(context, message);
    }

    public void showToast(int resId) {
        MessageUtils.showToast(context, resId);
    }

    public void showComingSoon(String moduleName) {
        MessageUtils.showToast(context, moduleName + " - Coming Soon…");
    }
}