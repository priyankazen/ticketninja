package in.ticketninja.common;

public class UserIdentificationCode {

    public static final int CUSTOMER_PHOTO_CODE = 1000;
    public static final int FULL_NAME_CODE = 1001;
    public static final int GENDER_CODE = 1002;
    public static final int BIRTH_DATE_CODE = 1003;
    public static final int ADDRESS_CODE = 1004;
    public static final int CITY_CODE = 1005;
    public static final int ORGANIZATION_NAME_CODE = 1006;
    public static final int MOBILE_NO_CODE = 1007;
    public static final int EMAIL_CODE = 1008;
    public static final int ID_CARD_CODE = 1009;
    public static final int PROFESSION_CODE = 1010;
    public static final int ID_NUMBER_CODE = 111;


    public static final String CUSTOMER_PHOTO = "Customer Photo";
    public static final String FULL_NAME = "Full event_name";
    public static final String GENDER = "gender";
    public static final String BIRTH_DATE = "birthdate";
    public static final String ADDRESS = "Address";
    public static final String CITY = "city";
    public static final String ORGANIZATION_NAME = "Organization event_name";
    public static final String MOBILE_NO = "mobileno";
    public static final String EMAIL = "email";
    public static final String ID_CARD = "ID card";
    public static final String ID_NUMBER = "Id Number";
    public static final String PROFESSION = "Profession";


    public static final String AADHAR_CARD = "Aadhar Card";
    public static final String DRIVING_LICENSE = "Driving License";
    public static final String PASSPORT = "Passport";
    public static final String STUDENT_ID_CARD = "Student ID Card";
    public static final String VOTER_CARD = "Voter Card";
    public static final String ID_TYPE = "Id_Type";
    public static final String _ID_NUMBER = "Id_Number";
    public static final String _FULL_NAME = "Full_Name";
    public static final String _BIRTH_DATE = "Birth_Date";
    public static final String _ORGANIZATION_NAME = "Organization_Name";
    public static final String PHOTO_ID = "Photo_Id";

    public static final String SR_NO = "srno";
    public static final String PROOF_ID = "proof_id";
    public static final String PROOF_VALUE = "proof_value";
    public static final String PROOF_TYPE = "proof_type";

    public static final String FILE = "file";
    public static final String TEXT = "text";
    public static final String RADIO = "radio";
    public static final String BIRTHDAY = "date";
    public static final String MOBILE_NO_TYPE = "mobileno";
    public static final String DROP_DOWN = "dropdown";
    public static final String EMAIL_TYPE = "email";


    public static final String SPLIT = ",";

    public static final String STR_BIRTH_DATE = "1900-01-01";

}

