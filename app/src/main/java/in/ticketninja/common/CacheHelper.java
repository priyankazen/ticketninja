/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.common;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.net.URLEncoder;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/***
 * how to use it :

 String string = "cache me!";
 String key = "cache1";
 CacheHelper.save(context, key, string);
 String getCache = CacheHelper.retrieve(context, key); // will return 'cache me!'
 */

public class CacheHelper {

    public static final String KEY_DASHBOARD = "json_dashboard";
    private Context context;
    private static int cacheLifeHour = 7 * 24;

    public CacheHelper(Context context) {
        this.context = context;
    }

    private static String getCacheDirectory(Context context) {
        return context.getCacheDir().getPath();
    }

    private static void save(Context context, String key, String value) {
        try {
            key = URLEncoder.encode(key, "UTF-8");
            File cache = new File(getCacheDirectory(context) + "/" + key + ".srl");
            ObjectOutput out = new ObjectOutputStream(new FileOutputStream(cache));
            out.writeUTF(value);
            out.close();
        } catch (Exception e) {
           // e.printStackTrace();
        }
    }

    public static void save(Context context, String key, String value, String identifier) {
        save(context, key + identifier, value);
    }

    public void save(String key, String value) {
        save(context, key, value);
    }


    public static String retrieve(Context context, String key, String identifier) {
        return retrieve(context, key + identifier);
    }

    public String retrieve(String key) {
        return retrieve(context, key);
    }

    public String retrieve(String key, int cacheLifeHours) {
        cacheLifeHour = cacheLifeHours;
        return retrieve(context, key);
    }

    private static String retrieve(Context context, String key) {
        try {
            key = URLEncoder.encode(key, "UTF-8");

            File cache = new File(getCacheDirectory(context) + "/" + key + ".srl");

            if (cache.exists()) {

                Date lastModDate = new Date(cache.lastModified());
                Date now = new Date();

                long diffInMillisec = now.getTime() - lastModDate.getTime();
                long diffInSec = TimeUnit.MILLISECONDS.toSeconds(diffInMillisec);

                diffInSec /= 60;
                diffInSec /= 60;
                long hours = diffInSec % 24;

                if (hours > cacheLifeHour) {
                    cache.delete();
                    return "";
                }

                ObjectInputStream in = new ObjectInputStream(new FileInputStream(cache));
                String value = in.readUTF();
                in.close();

                return value;
            }

        } catch (Exception e) {
           // e.printStackTrace();
        }

        return "";
    }

    public void remove(String key) {
        try {
            key = URLEncoder.encode(key, "UTF-8");

            File cache = new File(getCacheDirectory(context) + "/" + key + ".srl");

            if (cache.exists()) {
                if (cache.delete()) Log.e("", "");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}