/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import android.annotation.SuppressLint;

import in.ticketninja.common.Constant;
import in.ticketninja.objects.UserAddress;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class RequestInsertPlaceOrder {

    //private PlaceOrder Place_Order = new PlaceOrder();
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;
    private String email ;
    @Constant.MerchantWSType
    private String merchant;    // Paytm or Razorpay
    private String mobileno;
    private String payment_id ;      // Razor Pay Payment Id
    @Constant.PaymentWSType
    private String payment_type;    // COD or ONLINE
    private String session_id ;
    private String user_id ;
    private int admin_user_id = 0;
    private String booking_remark = "";
    private String booking_type = "";
    private String customer_name = "";
    private String sale_type = "";


    public RequestInsertPlaceOrder(String user_Id, String session_Id, String orderId, @Constant.PaymentWSType String paymenttype,
                                   @Constant.MerchantWSType String merchant,
                                   String payment_Id, String countryCode, String mobileNo, String email) {
        this.session_id = session_Id;
        this.user_id = user_Id;
        this.payment_type = paymenttype;
        this.merchant = merchant;
        this.payment_id = payment_Id;
        this.mobileno = mobileNo;
        this.email = email;

    }

    public RequestInsertPlaceOrder(String user_Id, String session_Id, @Constant.PaymentWSType String paymenttype,
                                   String countryCode, String mobileNo, String email) {

        this.email = email;
        this.merchant = "";
        this.mobileno = mobileNo;
        this.payment_id = "";
        this.payment_type = paymenttype;
        this.session_id = session_Id;
        this.user_id = user_Id;

    }

    public RequestInsertPlaceOrder(String user_Id, String session_Id, @Constant.PaymentWSType String paymenttype,
                                   String payment_Id, long address_Id, String d_Time_Slot,
                                   String countryCode, String mobileNo, String email, String codOTP, UserAddress userAddress) {

        this.email = email;
        this.merchant = "";
        this.mobileno = mobileNo;
        this.payment_id = payment_Id;
        this.payment_type = paymenttype;
        this.session_id = session_Id;
        this.user_id = user_Id;
    }


   /* public RequestInsertPlaceOrder(long user_Id, String session_Id, String orderId, @Constant.PaymentWSType String payment_Type,
                                   @Constant.MerchantWSType String merchant,
                                   String payment_Id, String countryCode, String mobileNo, String email) {
        Place_Order = new PlaceOrder(user_Id, session_Id, orderId, payment_Type, merchant, payment_Id, countryCode, mobileNo, email);
    }

    public RequestInsertPlaceOrder(long user_Id, String session_Id, @Constant.PaymentWSType String payment_Type,
                                   String countryCode, String mobileNo, String email) {
        Place_Order = new PlaceOrder(user_Id, session_Id, payment_Type, countryCode, mobileNo, email);
    }

    public RequestInsertPlaceOrder(long user_Id, String session_Id, @Constant.PaymentWSType String payment_Type,
                                   String payment_Id, long address_Id, String d_Time_Slot,
                                   String countryCode, String mobileNo, String email, String codOTP, UserAddress userAddress) {
        Place_Order = new PlaceOrder(user_Id, session_Id, payment_Type, payment_Id, address_Id,
                d_Time_Slot, countryCode, mobileNo, email, codOTP, userAddress);
    }*/

    public class PlaceOrder {

        private long user_id = 0;
        private String session_id = "";
        private String order_id = "";
        @SuppressLint("WrongConstant")
        @Constant.PaymentWSType
        private String payment_type ;    // COD or ONLINE
        @SuppressLint("WrongConstant")
        @Constant.MerchantWSType
        private String merchant ;    // Paytm or Razorpay
        private String payment_id = "0";      // Razor Pay Payment Id
        private long address_id = 0;
        private String D_Time_Slot = "";
        private String mobileno = "";
        private String country_code = "";
        private String email = "";
        private String COD_OTP = "";
        private UserAddress user_address = new UserAddress();

        public PlaceOrder() {
        }

        public PlaceOrder(long user_Id, String session_Id, String orderId, @Constant.PaymentWSType String paymenttype,
                          @Constant.MerchantWSType String merchant, String payment_Id, String countryCode,
                          String mobileNo, String email) {
            this.country_code = countryCode;
            this.user_id = user_Id;
            this.session_id = session_Id;
            this.order_id = orderId;
            this.merchant = merchant;
            this.payment_type = paymenttype;
            this.payment_id = payment_Id;
            this.mobileno = mobileNo;
            this.email = email;
        }

        public PlaceOrder(long user_Id, String session_Id, @Constant.PaymentWSType String paymenttype,
                          String countryCode, String mobileNo, String email) {
            this.country_code = countryCode;
            this.user_id = user_Id;
            this.session_id = session_Id;
            this.payment_type = paymenttype;
            this.mobileno = mobileNo;
            this.email = email;
        }

        public PlaceOrder(long user_Id, String session_Id, @Constant.PaymentWSType String paymenttype,
                          String payment_Id, long address_Id, String d_Time_Slot, String countryCode, String mobileNo,
                          String email, String codOTP, UserAddress userAddress) {
            this.country_code = countryCode;
            this.user_id = user_Id;
            this.session_id = session_Id;
            this.payment_type = paymenttype;
            this.payment_id = payment_Id;
            this.address_id = address_Id;
            this.D_Time_Slot = d_Time_Slot;
            this.mobileno = mobileNo;
            this.email = email;
            this.COD_OTP = codOTP;
            this.user_address = userAddress;
        }
    }

}