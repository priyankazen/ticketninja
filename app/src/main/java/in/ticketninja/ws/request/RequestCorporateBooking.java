/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import in.ticketninja.common.Constant;

/**
 * TicketNinja_Working(in.ticketninja.ws.request) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 12/8/17.
 *
 * @author Suthar Rohit
 */
public class RequestCorporateBooking {
    public String event_id = "";
    public String name = "";
    public String mobileno = "";
    public String email = "";
    public String category_name = "";
    public String Tickets_Looking = "";
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;
}
