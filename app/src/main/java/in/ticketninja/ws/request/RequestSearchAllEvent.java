/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import androidx.annotation.NonNull;

import in.ticketninja.common.Constant;

/**
 * TicketNinja_Working(in.ticketninja.ws.request) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 13/6/17.
 *
 * @author Suthar Rohit
 */
public class RequestSearchAllEvent {

    private String user_id ;
    private String keyword ;
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;


    public RequestSearchAllEvent(@NonNull String userId, @NonNull String keywords) {
        this.user_id = userId;
        keyword = keywords;
    }
}
