/*
 * Copyright (c) 2018. Shreya Prajapati
 * Developed by Shreya Prajapati for NicheTech Computer Solutions Pvt. Ltd. use only.
 *
 */

package in.ticketninja.ws.request;

import in.ticketninja.common.Constant;

public class RequestRemovePromoCode {

    private String session_id;
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;

    public RequestRemovePromoCode(String session_Id) {
        session_id = session_Id;
    }
}
