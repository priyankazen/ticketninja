/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import in.ticketninja.common.Constant;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class RequestGeneratePaytmChecksum {


    //private GenerateChecksum Paytm_Checksum = new GenerateChecksum();

    public String user_id ;
    private String session_id ;
    private String netamount ;
    private String email ;
    private String mobileno;
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;

    public RequestGeneratePaytmChecksum(String userId, String sessionId, String txnAmount, String email, String mobileNo) {
        this.user_id = userId;
        this.session_id = sessionId + "";
        this.netamount = txnAmount + "";
        this.email = email + "";
        this.mobileno = mobileNo + "";
    }

    /*public RequestGeneratePaytmChecksum(String userId, String sessionId, String txnAmount, String email, String mobileNo) {
        Paytm_Checksum = new GenerateChecksum(userId, sessionId, txnAmount, email, mobileNo);
    }*/



    private class GenerateChecksum {
        public String User_Id = "";
        private String Session_Id = "";
        private String ORDER_ID = "";
        private String CUST_ID = "0";
        private String TXN_AMOUNT = "";
        private String EMAIL = "";
        private String MOBILE_NO = "";
        private String CHANNEL_ID = "WAP";
        private String WEBSITE = "APP_STAGING";
        private String INDUSTRY_TYPE_ID = "Retail";
        private String CALLBACK_URL = "";
        public String engine_type = Constant.ENGINE_TYPE;
        private String device_type = Constant.DEVICE_TYPE;
        public String device_info = Constant.DEVICE_INFO;

        public GenerateChecksum() {
        }

        public GenerateChecksum(String userId, String sessionId, String txnAmount, String email, String mobileNo) {
            this.User_Id = userId;
            this.Session_Id = sessionId + "";
            this.ORDER_ID = sessionId + "";
            this.CUST_ID = userId + "";
            this.TXN_AMOUNT = txnAmount + "";
            this.CALLBACK_URL = "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=" + sessionId;
            this.EMAIL = email + "";
            this.MOBILE_NO = mobileNo + "";
        }
    }

}
