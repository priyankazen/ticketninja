/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import android.annotation.SuppressLint;

import in.ticketninja.common.Constant;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class RequestRegister {

   // public RegisterClass User_In_Up = new RegisterClass();
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;

    public String email = "";
    public String mobileno = "";
    public String password = "";
    public String country_code = "";

    public String user_id = "";
    public String firstname = "";
    public String lastname = "";
    public String gender = "";
    public String birthdate = "";
    private String user_imageid = "";
    @SuppressLint("WrongConstant")
    @Constant.RegisterWSMode
    public String mode = "";
    public String device_token = "";

    public RequestRegister(RegisterClass user_In_Up) {
        //User_In_Up = user_In_Up;
    }

    public RequestRegister() {

    }

    public static class RegisterClass {
        public String email = "";
        public String mobileno = "";
        public String password = "";
        public String country_code = "";

        public String user_id = "0";
        public String firstname = "";
        public String lastname = "";
        public String gender = "";
        public String birthdate = "";
        public String user_imageid = "";
        @SuppressLint("WrongConstant")
        @Constant.RegisterWSMode
        public String mode = "";
        public String device_token = "";
        private String device_type = Constant.DEVICE_TYPE;
        public String engine_type = Constant.ENGINE_TYPE;
    }

}
