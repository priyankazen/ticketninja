/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import androidx.annotation.NonNull;

import in.ticketninja.common.Constant;

/**
 * TicketNinja_Working(in.ticketninja.ws.request) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 13/6/17.
 *
 * @author Suthar Rohit
 */
public class RequestTrendingEvent {

    public String user_id ;
    public String city ;
    private String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    private String device_info = Constant.DEVICE_INFO;

}
