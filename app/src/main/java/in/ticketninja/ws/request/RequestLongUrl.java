/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import in.ticketninja.common.Constant;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class RequestLongUrl {

    private String short_code ;
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;

    public RequestLongUrl(String short_code) {
        this.short_code = short_code;
    }

    @Override
    public String toString() {
        return "RequestLongUrl{" +
                "short_code='" + short_code + '\'' +
                ", engine_type='" + engine_type + '\'' +
                ", device_type='" + device_type + '\'' +
                ", device_info='" + device_info + '\'' +
                '}';
    }
}
