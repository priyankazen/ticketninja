package in.ticketninja.ws.request;

import in.ticketninja.common.Constant;

public class RequestUserIdentification {

     public String event_id;
     public String category_id;
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;

    @Override
    public String toString() {
        return "RequestUserIdentification{" +
                "event_id='" + event_id + '\'' +
                ", category_id='" + category_id + '\'' +
                '}';
    }
}
