/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import in.ticketninja.common.Constant;

/**
 * Copyright (c) 2018. shabbir
 * Developed by shabbir for NicheTech Computer Solutions Pvt. Ltd. use only.
 * on 23/10/17.

 * @author shabbir
 */

public class RequestUserTicket {

    public String user_id ;
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;

    public RequestUserTicket(String user_Id) {
        user_id = user_Id;
    }

}
