/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;

import java.util.ArrayList;
import java.util.List;

import in.ticketninja.common.Constant;
import in.ticketninja.objects.EventPriceCategory;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class RequestInsertTicketSession {

    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;
    public long event_id ;
    String event_date ;
    //String time_slot = "";
    String event_time;
    String currency ;
    public String user_id ;

    List<EventPriceCategory> category_list;
    private JsonArray user_info_data = new JsonArray();

    public RequestInsertTicketSession(long eventId, String eventDate, String timeSlot, String currency, String userId, List<EventPriceCategory> categoryList) {
        event_id = eventId;
        event_date = eventDate;
        event_time = timeSlot;
        this.currency = currency;
        user_id = userId;
        category_list = new ArrayList<>();
        category_list.addAll(categoryList);
    }

   // public RequestInsertTicketSession(long eventId, String eventDate, String timeSlot, String userId, EventPriceCategory categoryList, String userInfo) {
    public RequestInsertTicketSession(long eventId, String eventDate, String timeSlot, String userId, List<EventPriceCategory> categoryList, String userInfo) {
        JsonArray jsonArray = null;
        try{
            JsonParser jsonParser = new JsonParser();
            jsonArray = (JsonArray) jsonParser.parse(userInfo);
        }catch (Exception e){
            e.printStackTrace();
        }

        event_id = eventId;
        event_date = eventDate;
        event_time = timeSlot;
        this.currency = "";
        user_id = userId;
        category_list = new ArrayList<>();
        category_list.addAll(categoryList);
        user_info_data = jsonArray;
    }


    /*private SessionData Session_data;
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
   // private JSONArray User_Info_List = new JSONArray();

    public RequestInsertTicketSession(long eventId, String eventDate, String timeSlot, String currency, long userId, List<EventPriceCategory> categoryList) {
        Session_data = new SessionData(eventId, eventDate, timeSlot, currency, userId, categoryList);
    }

    public RequestInsertTicketSession(long eventId, String eventDate, String timeSlot, long userId, EventPriceCategory categoryList, String userInfo) {
       // User_Info_List = userInfo;
        JsonArray jsonArray = null;
        try{
            JsonParser jsonParser = new JsonParser();
            jsonArray = (JsonArray) jsonParser.parse(userInfo);
        }catch (Exception e){
            e.printStackTrace();
        }
      
        Session_data = new SessionData(eventId, eventDate, timeSlot, "", userId, categoryList,jsonArray);
    }

    public class SessionData {

        public long event_id = 0;
        String event_date = "";
        String time_slot = "";
        String currency = "";
        public long user_id = 0;
        public String engine_type = Constant.ENGINE_TYPE;
        private String device_type = Constant.DEVICE_TYPE;
        List<EventPriceCategory> category_list;
        private JsonArray user_info_data = new JsonArray();

        SessionData(long event_Id, String event_Date, String time_Slot, String currency, long user_Id, List<EventPriceCategory> category_List) {
            event_id = event_Id;
            event_date = event_Date;
            time_slot = time_Slot;
            this.currency = currency;
            user_id = user_Id;
            category_list = new ArrayList<>();
            category_list.addAll(category_List);
        }

        SessionData(long event_Id, String event_Date, String time_Slot, String currency, long user_Id, EventPriceCategory category_List, JsonArray userInfo) {
            event_id = event_Id;
            event_date = event_Date;
            time_slot = time_Slot;
            this.currency = currency;
            user_id = user_Id;
            category_list = new ArrayList<>();
            category_list.add(category_List);
            user_info_data = userInfo;
        }

    }*/

}

/*
{
    "session_data": {
        "category_list": [
            {
                "addons": 0,
                "amount": 1500,
                "c_desc": "Remark",
                "category_id": 1040,
                "category_name": "Enterance and Guide Bundle",
                "event_date": "0001-01-01",
                "identification": 0,
                "mrp": 500,
                "noofticket": 3,
                "rate": 500,
                "seasonpass": 0,
                "seat_no": "",
                "seat_type": "Bundle",
                "show_id": 0,
                "show_name": "",
                "time": "",
                "totalMRP": 1500,
                "total_seat": 5
            }
        ],
        "currency": "IN",
        "event_date": "2019-03-16",
        "event_id": 1009,
        "time_slot": "01:00 PM-03:00 PM",
        "user_id": 1000,
        "user_info_data": [],
        "device_type": "android",
        "engine_type": "App"
    },
    "device_type": "android",
    "engine_type": "App"
}
*/
