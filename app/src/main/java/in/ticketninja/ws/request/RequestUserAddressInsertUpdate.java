/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import in.ticketninja.common.Constant;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class RequestUserAddressInsertUpdate {

    private UserAddress User_Address = new UserAddress();
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;


    public RequestUserAddressInsertUpdate(long user_Id, long address_Id, String fullName, String aline1, String aline2, String city,
                                          String state, String country, String pinCode, String mobileNo, String add_Type, @Constant.RegisterWSMode String mode) {
        User_Address = new UserAddress(user_Id, address_Id, fullName, aline1, aline2, city,
                state, country, pinCode, mobileNo, add_Type, mode);
    }

    public class UserAddress {

        private long User_Id=0;
        private long Address_Id;
        private String Fullname;
        private String Aline1;
        private String Aline2;
        private String City;
        private String State;
        private String Country;
        private String Pincode;
        private String Mobileno;
        private String Add_Type;
        @Constant.RegisterWSMode
        private String Mode = "";

        public UserAddress() {
        }

        public UserAddress(long user_Id, long address_Id, String fullName, String aline1, String aline2, String city,
                           String state, String country, String pinCode, String mobileNo, String add_Type, @Constant.RegisterWSMode String mode) {
            User_Id = user_Id;
            Address_Id = address_Id;
            Fullname = fullName;
            Aline1 = aline1;
            Aline2 = aline2;
            City = city;
            State = state;
            Country = country;
            Pincode = pinCode;
            Mobileno = mobileNo;
            Add_Type = add_Type;
            Mode = mode;
        }
    }

}