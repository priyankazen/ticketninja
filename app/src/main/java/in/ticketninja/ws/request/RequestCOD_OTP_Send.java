/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import in.ticketninja.common.Constant;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class RequestCOD_OTP_Send {

    private String Mobile_No;
    private String email;
    private String session_id;
    private String currency;
    private String country_code;
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;

    public RequestCOD_OTP_Send(String countryCode, String mobileId, String email, String sessionId, String currency) {
        country_code = countryCode;
        Mobile_No = mobileId;
        this.email = email;
        session_id = sessionId;
        this.currency = currency;
    }
}