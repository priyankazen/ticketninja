package in.ticketninja.ws.request;

import in.ticketninja.common.Constant;

public class RequestPromotionCampaignDetails {
    public int m_id = 0;
    public String c_id = "";
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;
}
