/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import androidx.annotation.NonNull;

import in.ticketninja.common.Constant;

/**
 * TicketNinja_Working(in.ticketninja.ws.request) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 8/6/17.
 *
 * @author Suthar Rohit
 */
public class RequestDashboard {

    private String user_id;
    private String Type;
    private String city ;
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;

    public RequestDashboard(@NonNull String userId, @NonNull String type, @NonNull String city_name) {
        this.user_id = userId;
        this.Type = type;
        this.city = city_name;
    }

}
