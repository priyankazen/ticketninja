/*
 * Copyright (c) 2018. Shreya Prajapati
 * Developed by Shreya Prajapati for NicheTech Computer Solutions Pvt. Ltd. use only.
 *
 */
package in.ticketninja.ws.request;

import in.ticketninja.common.Constant;
import in.ticketninja.objects.OfferCode;

public class RequestApplyPromoCode {

    //private OfferCode offer_code_request;

    private String session_id;
    private String user_id;
    private String code;
    private String email;
    private String mobileno;
    public String engine_type = Constant.ENGINE_TYPE;
    public String device_info = Constant.DEVICE_INFO;
    private String device_type = Constant.DEVICE_TYPE;

    public RequestApplyPromoCode(OfferCode offer_code_request) {
        //this.offer_code_request = offer_code_request;
    }

    public RequestApplyPromoCode(String session_Id, String user_Id, String code, String email, String mobileno) {
        this.session_id = session_Id;
        this.user_id = user_Id;
        this.code = code;
        this.email = email;
        this.mobileno = mobileno;
    }

}
