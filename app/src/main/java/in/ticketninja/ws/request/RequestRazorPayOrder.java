/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import in.ticketninja.common.Constant;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class RequestRazorPayOrder {

    public String user_id = "";
    public String session_id = "";
    public String email = "";
    public String mobileno = "";
    private String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    private String device_info = Constant.DEVICE_INFO;

    @Override
    public String toString() {
        return "RequestRazorPayOrder{" +
                "session_id='" + session_id + '\'' +
                ", email='" + email + '\'' +
                ", mobileno='" + mobileno + '\'' +
                ", engine_type='" + engine_type + '\'' +
                ", device_type='" + device_type + '\'' +
                ", device_info='" + device_info + '\'' +
                '}';
    }
}
