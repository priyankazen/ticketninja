/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import androidx.annotation.NonNull;

import in.ticketninja.common.Constant;

/**
 * TicketNinja_Working(in.ticketninja.ws.request) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 8/6/17.
 *
 * @author Suthar Rohit
 */
public class RequestEventList {

    private String user_id ;
    private String type ;
    private String city;
    private String c_tag;
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;

    public RequestEventList(@NonNull String userId, @NonNull String type, @NonNull String cityName,String c_tag) {
        this.user_id = userId;
        this.type = type;
        this.city = cityName;
        this.c_tag = c_tag;
    }

}
