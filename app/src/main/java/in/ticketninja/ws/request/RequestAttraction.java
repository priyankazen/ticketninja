/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import in.ticketninja.common.Constant;

/**
 * TicketNinja_Working(in.ticketninja.ws.request) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 12/8/17.
 *
 * @author Suthar Rohit
 */
public class RequestAttraction {

    public long event_id = 0;
    public String select_date = "";
    public String country_code = "";
    public String engine_type = Constant.ENGINE_TYPE;
    public String device_info = Constant.DEVICE_INFO;
    private String device_type = Constant.DEVICE_TYPE;
}
