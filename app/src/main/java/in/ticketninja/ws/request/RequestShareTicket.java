/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.request;

import in.ticketninja.common.Constant;

/**
 * Created by shabbir on 29/9/17.
 */

public class RequestShareTicket {

    public String user_id;
    public long trans_id;
    public String email ;
    public String engine_type = Constant.ENGINE_TYPE;
    private String device_type = Constant.DEVICE_TYPE;
    public String device_info = Constant.DEVICE_INFO;


    public RequestShareTicket(String user_Id, long trans_Id, String email) {
        user_id = user_Id;
        trans_id = trans_Id;
        this.email = email;
    }

}
