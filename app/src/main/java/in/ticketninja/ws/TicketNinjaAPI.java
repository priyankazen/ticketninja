/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws;

import in.ticketninja.objects.RequestSocialLogin;
import in.ticketninja.ws.RestApi.METHODS;
import in.ticketninja.ws.request.RequestApplyPromoCode;
import in.ticketninja.ws.request.RequestAttraction;
import in.ticketninja.ws.request.RequestBasicInfo;
import in.ticketninja.ws.request.RequestBookingHistory;
import in.ticketninja.ws.request.RequestBrandAssociation;
import in.ticketninja.ws.request.RequestCOD_OTP_Send;
import in.ticketninja.ws.request.RequestChangePassword;
import in.ticketninja.ws.request.RequestCheckPaymentSession;
import in.ticketninja.ws.request.RequestCityList;
import in.ticketninja.ws.request.RequestContactUs;
import in.ticketninja.ws.request.RequestCorporateBooking;
import in.ticketninja.ws.request.RequestCustomerCare;
import in.ticketninja.ws.request.RequestDashboard;
import in.ticketninja.ws.request.RequestDownloadBooking;
import in.ticketninja.ws.request.RequestEventDetail;
import in.ticketninja.ws.request.RequestEventList;
import in.ticketninja.ws.request.RequestEventRegister;
import in.ticketninja.ws.request.RequestForceUpdate;
import in.ticketninja.ws.request.RequestForgotPassword;
import in.ticketninja.ws.request.RequestGeneratePaytmChecksum;
import in.ticketninja.ws.request.RequestGetBookingDetailForCOD;
import in.ticketninja.ws.request.RequestGetBookingDetailForOnline;
import in.ticketninja.ws.request.RequestGetTicketSessionData;
import in.ticketninja.ws.request.RequestInsertPlaceOrder;
import in.ticketninja.ws.request.RequestInsertTicketSession;
import in.ticketninja.ws.request.RequestLogin;
import in.ticketninja.ws.request.RequestLogout;
import in.ticketninja.ws.request.RequestLongUrl;
import in.ticketninja.ws.request.RequestNotificationDelete;
import in.ticketninja.ws.request.RequestNotificationList;
import in.ticketninja.ws.request.RequestNotificationRead;
import in.ticketninja.ws.request.RequestPaymentCODdate;
import in.ticketninja.ws.request.RequestPromotionCampaignDetails;
import in.ticketninja.ws.request.RequestRazorPayOrder;
import in.ticketninja.ws.request.RequestRegister;
import in.ticketninja.ws.request.RequestRemovePromoCode;
import in.ticketninja.ws.request.RequestRemoveSessionData;
import in.ticketninja.ws.request.RequestRevokeTicket;
import in.ticketninja.ws.request.RequestSearch;
import in.ticketninja.ws.request.RequestSearchAllEvent;
import in.ticketninja.ws.request.RequestShareTicket;
import in.ticketninja.ws.request.RequestShareTicketEmailSMS;
import in.ticketninja.ws.request.RequestTransNo;
import in.ticketninja.ws.request.RequestTrendingEvent;
import in.ticketninja.ws.request.RequestUploadImage;
import in.ticketninja.ws.request.RequestUploadProfileImage;
import in.ticketninja.ws.request.RequestUserIdentification;
import in.ticketninja.ws.request.RequestUserTicket;
import in.ticketninja.ws.response.CallbackApplyPromoCode;
import in.ticketninja.ws.response.CallbackBasicInfo;
import in.ticketninja.ws.response.CallbackBookingHistory;
import in.ticketninja.ws.response.CallbackCOD_OTP_Send;
import in.ticketninja.ws.response.CallbackChangePassword;
import in.ticketninja.ws.response.CallbackCityList;
import in.ticketninja.ws.response.CallbackContactUs;
import in.ticketninja.ws.response.CallbackCustomerCare;
import in.ticketninja.ws.response.CallbackDownloadBooking;
import in.ticketninja.ws.response.CallbackDownloadTicket;
import in.ticketninja.ws.response.CallbackEventDetail;
import in.ticketninja.ws.response.CallbackEventList;
import in.ticketninja.ws.response.CallbackForceUpdate;
import in.ticketninja.ws.response.CallbackForgotPassword;
import in.ticketninja.ws.response.CallbackGeneratePaytmChecksum;
import in.ticketninja.ws.response.CallbackGetAttraction;
import in.ticketninja.ws.response.CallbackGetBookingDetailForCOD;
import in.ticketninja.ws.response.CallbackGetBookingDetailForOnline;
import in.ticketninja.ws.response.CallbackGetTicketSessionData;
import in.ticketninja.ws.response.CallbackInsertPlaceOrder;
import in.ticketninja.ws.response.CallbackInsertTicketSession;
import in.ticketninja.ws.response.CallbackLogin;
import in.ticketninja.ws.response.CallbackLogout;
import in.ticketninja.ws.response.CallbackNotificationDelete;
import in.ticketninja.ws.response.CallbackNotificationList;
import in.ticketninja.ws.response.CallbackNotificationRead;
import in.ticketninja.ws.response.CallbackPaymentCODdate;
import in.ticketninja.ws.response.CallbackPromotionCampaignDetails;
import in.ticketninja.ws.response.CallbackRegister;
import in.ticketninja.ws.response.CallbackRemovePromoCode;
import in.ticketninja.ws.response.CallbackRemoveTicketSession;
import in.ticketninja.ws.response.CallbackRevokeTicket;
import in.ticketninja.ws.response.CallbackSearch;
import in.ticketninja.ws.response.CallbackSearchAllEvent;
import in.ticketninja.ws.response.CallbackShareTicket;
import in.ticketninja.ws.response.CallbackShareTicketEmailSMS;
import in.ticketninja.ws.response.CallbackSocialLogin;
import in.ticketninja.ws.response.CallbackTrendingEvent;
import in.ticketninja.ws.response.CallbackUploadImage;
import in.ticketninja.ws.response.CallbackUploadProfileImage;
import in.ticketninja.ws.response.CallbackUserTicket;
import in.ticketninja.ws.response.CbUserIdentification;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface TicketNinjaAPI {

    //@POST(METHODS.FORCE_UPDATE1)
    //Call<CallbackForceUpdate> forceUpdate1(@Body RequestForceUpdate request);

    // LOGIN
    @POST(METHODS.BASIC_INFO)
    Call<CallbackBasicInfo> basicInfo(@Body RequestBasicInfo requestBasicInfo);

    @POST(METHODS.LOGIN)
    Call<CallbackLogin> login(@Body RequestLogin requestLogin);

    @POST(METHODS.LOGOUT)
    Call<CallbackLogout> logout(@Body RequestLogout request);

    @POST(METHODS.REGISTER_EDIT_PROFILE)
    Call<CallbackRegister> registerEditProfile(@Body RequestRegister requestRegister);

    @POST(METHODS.FORGOT_PASSWORD)
    Call<CallbackForgotPassword> forgotPassword(@Body RequestForgotPassword requestForgotPassword);

    @POST(METHODS.CHANGE_PASSWORD)
    Call<CallbackChangePassword> changePassword(@Body RequestChangePassword requestChangePassword);

    @POST(METHODS.UPLOAD_PROFILE_IMAGE)
    Call<CallbackUploadProfileImage> uploadProfileImage(@Body RequestUploadProfileImage requestUploadProfileImage);

    // EVENT
    @POST(METHODS.DASHBOARD)
    Call<ResponseBody> GetDashboard(@Body RequestDashboard requestDashboard);

    @POST(METHODS.SEARCH)
    Call<CallbackSearch> GetSearch(@Body RequestSearch requestSearch);

    @POST(METHODS.SEARCH_SUGGESTION)
    Call<CallbackSearchAllEvent> GetSearchSuggestion(@Body RequestSearchAllEvent request);

    @POST(METHODS.EVENT_LIST)
    Call<CallbackEventList> eventList(@Body RequestEventList requestEventList);

    @POST(METHODS.EVENT_DETAIL)
    Call<CallbackEventDetail> eventDetail(@Body RequestEventDetail requestEventDetail);

    // GENERAL
    @POST(METHODS.CITY_LIST)
    Call<CallbackCityList> cityList(@Body RequestCityList requestCityList);

    // PAYMENT
    @POST(METHODS.TICKET_SESSION_INSERT)
    Call<CallbackInsertTicketSession> insertTicketSession(@Body RequestInsertTicketSession requestInsertTicketSession);

    @POST(METHODS.TICKET_SESSION_REMOVE)
    Call<CallbackRemoveTicketSession> removeTicketSession(@Body RequestRemoveSessionData request);

    @POST(METHODS.TICKET_SESSION_GET_DATA)
    Call<CallbackGetTicketSessionData> getTicketSessionData(@Body RequestGetTicketSessionData requestGetTicketSessionData);

    @POST(METHODS.PAYMENT_COD_DATA)
    Call<CallbackPaymentCODdate> getPaymentCODdate(@Body RequestPaymentCODdate requestPaymentCODdate);

    @POST(METHODS.BOOKING_DETAIL_FOR_ONLINE)
    Call<CallbackGetBookingDetailForOnline> getBookingDetailForOnline(@Body RequestGetBookingDetailForOnline request);

    @POST(METHODS.BOOKING_DETAIL_FOR_COD)
    Call<CallbackGetBookingDetailForCOD> getBookingDetailForCOD(@Body RequestGetBookingDetailForCOD request);

    @POST(METHODS.BOOKING_DOWNLOAD)
    Call<CallbackDownloadBooking> downloadBooking(@Body RequestDownloadBooking request);

    @POST(METHODS.TICKET_DOWNLOAD)
    Call<CallbackDownloadTicket> downloadTicket(@Body RequestDownloadBooking request);

    @POST(METHODS.COD_OTP_SEND)
    Call<CallbackCOD_OTP_Send> COD_OTP_Send(@Body RequestCOD_OTP_Send request);

    //@POST(METHODS.COD_OTP_CHECK)
    //Call<CallbackCOD_OTP_Check> COD_OTP_Check(@Body RequestCOD_OTP_Check request);

    @POST(METHODS.PLACE_ORDER)
    Call<CallbackInsertPlaceOrder> InsertPlaceOrder(@Body RequestInsertPlaceOrder request);

    @POST(METHODS.GENERATE_PAYTM_CHECKSUM)
    Call<CallbackGeneratePaytmChecksum> GeneratePaytmChecksum(@Body RequestGeneratePaytmChecksum request);

    //@POST(METHODS.USER_ADDRESS_ADD_UPDATE)
    //Call<CallbackUserAddressInsertUpdate> UserAddressInsertUpdate(@Body RequestUserAddressInsertUpdate request);

    @POST(METHODS.SHARE_EMAIL_SMS)
    Call<CallbackShareTicketEmailSMS> ShareTicketEmailSMS(@Body RequestShareTicketEmailSMS request);

    @POST(METHODS.SHARE_TICKET)
    Call<CallbackShareTicket> ShareTicket(@Body RequestShareTicket request);

    @POST(METHODS.USER_TICKET)
    Call<CallbackUserTicket> UserTicket(@Body RequestUserTicket userTicket);

    @POST(METHODS.REVOKE_TICKET)
    Call<CallbackRevokeTicket> RevokeTicket(@Body RequestRevokeTicket request);


    @POST(METHODS.CONTACT_US)
    Call<CallbackContactUs> ContactUs(@Body RequestContactUs requestContactUs);

    @POST(METHODS.CUSTOMER_CARE)
    Call<CallbackCustomerCare> Customercare(@Body RequestCustomerCare requestCustomerCare);

    @POST(METHODS.SELECT_TIME)
    Call<CallbackGetAttraction> getAttraction(@Body RequestAttraction requestAttraction);

    @POST(METHODS.BOOKING_HISTORTY)
    Call<CallbackBookingHistory> GetBookingHistory(@Body RequestBookingHistory requestBookingHistory);


    // NOTIFICATION LIST
    @POST(METHODS.NOTIFICATION_LIST)
    Call<CallbackNotificationList> notificationList(@Body RequestNotificationList request);

    // NOTIFICATION READ
    @POST(METHODS.NOTIFICATION_READ)
    Call<CallbackNotificationRead> notificationRead(@Body RequestNotificationRead request);


    // NOTIFICATION DELETE
    @POST(METHODS.NOTIFICATION_DELETE)
    Call<CallbackNotificationDelete> notificationDelete(@Body RequestNotificationDelete request);

    // FORCE UPDATE
    @POST(METHODS.FORCE_UPDATE)
    Call<CallbackForceUpdate> forceUpdate(@Body RequestForceUpdate request);

    // CHECK SOCIAL LOGIM
    @POST(METHODS.SOCIAL_LOGIN)
    Call<CallbackSocialLogin> checkSocialLogin(@Body RequestSocialLogin request);

    //APPLY PROMO CODE
    @POST(METHODS.APPLY_PROMO_CODE)
    Call<CallbackApplyPromoCode> applyPromoCode(@Body RequestApplyPromoCode request);

    //REMOVE PROMO CODE
    @POST(METHODS.REMOVE_PROMO_CODE)
    Call<CallbackRemovePromoCode> removePromoCode(@Body RequestRemovePromoCode request);

    @POST(METHODS.GET_TRANS_ID)
    Call<ResponseBody> getTransId(@Body RequestTransNo request);

    @POST(METHODS.COR)
    Call<ResponseBody> getCorporationBooking(@Body RequestCorporateBooking request);

    @POST(METHODS.BRAND)
    Call<ResponseBody> getBrandAssociate(@Body RequestBrandAssociation request);

    @POST(METHODS.USER_IDENTIFICATION)
    Call<CbUserIdentification> getUserIdentification(@Body RequestUserIdentification request);

    //@POST(METHODS.UPLOAD_IMAGE)
   // Call<CallbackUploadImage> uploadImage(@Body RequestUploadImage requestUploadProfileImage);

    @POST(METHODS.USER_INFO)
    Call<ResponseBody> userIdentificationData(@Body RequestBody requestUser);

    @POST(METHODS.CHECK_PAYMENT_SESSION)
    Call<ResponseBody> checkPaymentSession(@Body RequestCheckPaymentSession checkPaymentSession);


    @POST(METHODS.EVENT_REGISTER)
    Call<ResponseBody> getEventReg(@Body RequestEventRegister request);

    @POST(METHODS.GET_LONG_URL)
    Call<ResponseBody> getLongUrl(@Body RequestLongUrl request);

    @POST(METHODS.CREATE_RAZOR_PAY_ORDER)
    Call<ResponseBody> createRazorPayOrder(@Body RequestRazorPayOrder request);

    @POST(METHODS.REGISTER_EDIT_PROFILE)
    Call<CallbackRegister> registerEditProfile(@Body RequestRegister.RegisterClass registerClass);

    @POST(METHODS.GET_PROMOTION_CAMPAIGN_DETAIL)
    Call<CallbackPromotionCampaignDetails> getPromotionCampaignDetails(@Body RequestPromotionCampaignDetails registerClass);


    @POST(METHODS.GET_TRADING_EVENT)
    Call<CallbackTrendingEvent> getTradingEvent(@Body RequestTrendingEvent requestSearch);


}