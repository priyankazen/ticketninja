/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.response;

import in.ticketninja.ws.RestApi;

/**
 * TicketNinja_Working(in.ticketninja.ws.response) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 10/7/17.
 *
 * @author Suthar Rohit
 */
public class CallbackContactUs {

    public String error_code ="";
    private String error_description = "";

   public insertContactus data = new insertContactus();

    public insertContactus data() {
        return data;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public class insertContactus{

        private int ErrorCode;
        private String ErrorMessage;
        private String ServiceName;

        public int getErrorCode() {
            return ErrorCode;
        }

        public String getErrorMessage() {
            return ErrorMessage;
        }

        public String getServiceName() {
            return ServiceName;
        }

        public boolean isSuccess() {
            return ErrorCode == RestApi.ErrorCode.SUCCESS;
        }

        public String message() {
            return ErrorMessage;
        }
    }






}
