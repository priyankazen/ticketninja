/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.response;

import in.ticketninja.ws.RestApi;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class CallbackUploadProfileImage {

    public String error_code ="";
    private String error_description = "";

    private wsUploadProfileImage data;

    public wsUploadProfileImage data(){
        return data;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public class wsUploadProfileImage {
        private int ErrorCode;
        private String ErrorMessage;
        private String ServiceName;
        private String user_imageid;//User_Imageid
        private String user_imageid_url;//User_Image_URL

        private String getUserImageId() {
            return user_imageid;
        }

        public String getUserImageURL() {
            return user_imageid_url;
        }

        public boolean isSuccess() {
            return ErrorCode == RestApi.ErrorCode.SUCCESS;
        }

        public String message() {
            return ErrorMessage;
        }


    }
}

 //{"error_description":"Upload Successfully","error_code":"1",
// "data":{"user_imageid":"BCEYK9c1aHTJexT54KiSQMT0R5F7RvgQ.jpg",
// "user_imageid_url":"https://ticketninja.in:4430/User_Image/BCEYK9c1aHTJexT54KiSQMT0R5F7RvgQ.jpg"}}

//{"data":{"user_id":"b899461b7561077a1b88a9f9f9eb4b07","firstname":"Romit","lastname":"Sadaria","email":"romit@zengroup.co.in","country_code":"+91","mobileno":"9377433176","gender":"Male","birthdate":"1991-12-28","user_imageid":"BCEYK9c1aHTJexT54KiSQMT0R5F7RvgQ.jpg",
// "user_imageid_url":"https://ticketninja.in:4430/User_Image/BCEYK9c1aHTJexT54KiSQMT0R5F7RvgQ.jpg"},"error_code":"1","error_description":"Login Successfully"}
