/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.response;

import java.util.List;

import in.ticketninja.objects.MyEvent;
import in.ticketninja.ws.RestApi;

/**
 * TicketNinja_Working(in.ticketninja.ws.response) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 8/6/17.
 *
 * @author Suthar Rohit
 */
public class CallbackEventList {

    public String error_code ="";
    private String error_description = "";
    private EventData data;

    public String getError_code() {
        return error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public EventData data() {
        return data;
    }

    public class EventData {

        public int ErrorCode;
        public String ErrorMessage;
        public String ServiceName;
        public List<MyEvent> featur_event_list ; //Poster_List;
        public long Poster_count;

        public boolean isSuccess() {
            return ErrorCode == RestApi.ErrorCode.SUCCESS;
        }

        public String message() {
            return ErrorMessage;
        }

    }
}
