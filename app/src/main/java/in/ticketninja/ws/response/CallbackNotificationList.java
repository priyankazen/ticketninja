/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.response;
import java.util.List;
import in.ticketninja.objects.Notifications;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class CallbackNotificationList {
    private String error_code ="";
    private String error_description = "";
    private NotificationData data = null;

    public NotificationData data() {

        return data;
    }

   //public List<NotificationData> data;

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public class NotificationData {

        //public int ErrorCode;
        //public String ErrorMessage;
        //public String ServiceName;
        public List<Notifications> notification_list;
        //private long Poster_count;

        /*public long notification_id;
        public int n_type;
        public long reference_id;
        public long event_id;
        public String type;
        public String message;
        public String notification_date;
        public int is_read;
        public int status;
        public long user_id;
        public String poster_image_id;
        public String poster_image_url;*/

        /*public boolean isSuccess() {
            return ErrorCode == RestApi.ErrorCode.SUCCESS;
        }

        public String message() {
            return ErrorMessage;
        }*/

    }
}


//{"data":[{"notification_id":1536,"n_type":2,"reference_id":9156,"event_id":1007,"type":"Concert","message":"You have received Ticket of Farhan Akhtar - Live in Concert","notification_date":"2019-04-02 17:15:33","is_read":0,"status":1,"user_id":1000,"poster_image_id":"https://ticketninja.in:4430/Admin/Document/14d308cf-51e8-4c22-9432-e2c63d02e18d.jpg"}],"error_code":"1","error_description":"sucess"}
//{"data":{"notification_list":[{"notification_id":1535,"n_type":2,"reference_id":9142,"event_id":1028,"type":"Event","message":"You have received Ticket of Test RSVP Green","notification_date":"2019-03-06 11:10:13","is_read":0,"status":1,"poster_image_url":"https://ticketninja.in:4430/Admin/Document/0ae16ce0-742a-4d39-a608-b06318e4e091.jpg"}]},"error_code":"1","error_description":"sucess"}
