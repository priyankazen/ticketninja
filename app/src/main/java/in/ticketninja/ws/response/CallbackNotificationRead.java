/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.response;

import androidx.annotation.NonNull;

import in.ticketninja.ws.RestApi;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class CallbackNotificationRead {
    public String error_code ="";
    private String error_description = "";
    // {"data":{"error_code":1,"error_description":"sucess","ServiceName":"data","notification_unread_count":4}}

    @NonNull
    private NotificationData data = new NotificationData();

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    @NonNull
    public NotificationData data() {
        return data;
    }

    public class NotificationData {

        private int notification_unread_count;

        public int getNotificationUnreadCount() {
            return notification_unread_count;
        }

        private int ErrorCode;
        private String ErrorMessage;
        private String ServiceName;

        public boolean isSuccess() {
            return ErrorCode == RestApi.ErrorCode.SUCCESS;
        }

        public String message() {
            return ErrorMessage;
        }

    }
}

