/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.response;

import java.util.ArrayList;

import in.ticketninja.objects.UserCouponCode;
import in.ticketninja.objects.User_Ticket_Event;
import in.ticketninja.ws.RestApi;

/**
 * Created by shabbir on 23/10/17.
 */

public class CallbackUserTicket {

    public String error_code ="";
    private String error_description = "";
    public Get_User_Tickets data;

    public Get_User_Tickets data() {
        return data;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public class Get_User_Tickets {

        public int ErrorCode;
        public String ErrorMessage;
        public String ServiceName;
        public boolean Time_Slot;

        public ArrayList<User_Ticket_Event> user_ticket_event;
        public ArrayList<UserCouponCode> user_coupon_code;

        public ArrayList<User_Ticket_Event> userTicketEvents() {
            return user_ticket_event;
        }

        public boolean isSuccess() {
            return ErrorCode == RestApi.ErrorCode.SUCCESS;
        }

        public String message() {
            return ErrorMessage;
        }

        public ArrayList<UserCouponCode> getUser_coupon_code() {
            return user_coupon_code;
        }
    }

}
