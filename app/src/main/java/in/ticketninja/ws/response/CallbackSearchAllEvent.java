/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.response;

import java.util.List;

import in.ticketninja.objects.MyEvent;
import in.ticketninja.ws.RestApi;

/**
 * TicketNinja_Working(in.ticketninja.ws.response) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 13/6/17.
 *
 * @author Suthar Rohit
 */
public class CallbackSearchAllEvent {

    public String error_code ="";
    private String error_description = "";

    public getsearch data;

    public getsearch data() {
        return data;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public class getsearch {

        private int ErrorCode;
        private String ErrorMessage;
        private String ServiceName;
        public List<MyEvent> event_list;//Data_List

        public int getErrorCode() {
            return ErrorCode;
        }

        public boolean isSuccess() {
            return ErrorCode == RestApi.ErrorCode.SUCCESS;
        }

        public String message() {
            return ErrorMessage;
        }

        public List<MyEvent> getPoster_List() {
            return event_list;
        }

    }
}
