/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.response;

import androidx.annotation.NonNull;

import in.ticketninja.ws.RestApi;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class CallbackGeneratePaytmChecksum {

    public String error_code ="";
    private String error_description = "";
    public GenerateChecksum data;

    public GenerateChecksum data() {
        return data;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public class GenerateChecksum {

        @NonNull
        private String CALLBACK_URL = "";
        @NonNull
        private String MID = "";
        @NonNull
        private String CHANNEL_ID = "";
        @NonNull
        private String CUST_ID = "";
        @NonNull
        private String EMAIL = "";
        @NonNull
        private String INDUSTRY_TYPE_ID = "";
        @NonNull
        private String MOBILE_NO = "";
        @NonNull
        private String ORDER_ID = "";
        @NonNull
        private String TXN_AMOUNT = "";
        @NonNull
        private String WEBSITE = "";

        private String paytmChecksum = "";

        private boolean PAYTM_LIVE = false;


        private String CHECKSUMHASH = "";
        private String STAGING_URL = "";
        private String PAYMENT_MODE_ONLY = "";
        private String PAYMENT_TYPE_ID = "";

        @NonNull
        public String callbackUrl() {
            return CALLBACK_URL;
        }

        @NonNull
        public String merchantID() {
            return MID;
        }

        @NonNull
        public String channelID() {
            return CHANNEL_ID;
        }

        @NonNull
        public String custID() {
            return CUST_ID;
        }

        @NonNull
        public String email() {
            return EMAIL;
        }

        @NonNull
        public String industryTypeId() {
            return INDUSTRY_TYPE_ID;
        }

        @NonNull
        public String mobileNo() {
            return MOBILE_NO;
        }

        @NonNull
        public String orderId() {
            return ORDER_ID;
        }

        @NonNull
        public String txnAmount() {
            return TXN_AMOUNT;
        }

        @NonNull
        public String website() {
            return WEBSITE;
        }

        @NonNull
        private String paytmChecksum() {
            return paytmChecksum;
        }

        public boolean isPaytmLive() {
            return PAYTM_LIVE;
        }

        public int ErrorCode;

        private String ErrorMessage;

        private String ServiceName;

        public boolean isSuccess() {
            return ErrorCode == RestApi.ErrorCode.SUCCESS;
        }

        @NonNull
        public String message() {
            return ErrorMessage;
        }

        public String getCHECKSUMHASH() {
            return CHECKSUMHASH;
        }

        public String getSTAGING_URL() {
            return STAGING_URL;
        }

        public String getPAYMENT_MODE_ONLY() {
            return PAYMENT_MODE_ONLY;
        }

        public String getPAYMENT_TYPE_ID() {
            return PAYMENT_TYPE_ID;
        }
    }

}

