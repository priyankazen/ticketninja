package in.ticketninja.ws.response;

import in.ticketninja.objects.PromotionCampaignDetailsData;
import in.ticketninja.objects.UserInfo;

public class CallbackPromotionCampaignDetails {
    public String error_code ="";
    private String error_description = "";
    private PromotionCampaignDetailsData data;

    public String getError_code() {
        return error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public PromotionCampaignDetailsData getData() {
        return data;
    }

    @Override
    public String toString() {
        return "CallbackPromotionCampaignDetails{" +
                "error_code='" + error_code + '\'' +
                ", error_description='" + error_description + '\'' +
                ", data=" + data +
                '}';
    }
}

// {"error_code":"1","error_description":"Successfully",
//
// "data":{"partner_name":"Zen Agri Food LLP","coupon_code":"HX4309","expiry_date":"16-11-2019",
// "status":"Active","campaign_tandc_list":[{"description":"Test"}],

// "campaign_event_list":[{"event_id":1030,"event_name":"Test 2 Green Multi Day","type":"Concert","day_type":"Multipleday",
// "location_name":"Classic Convention Three","city":"Hyderabad","state":"Telangana","event_date":"2019-12-20",
// "event_date_to":"2019-12-20","event_time":"5:00 PM","category":"Multi Day","language":"Gujarati","
// main_image_url":"https://api.ticketninja.in:3002/e_image/2ae240df-b766-4539-ac58-29f927963db7.jpg",
// "poster_image_url":"https://api.ticketninja.in:3002/e_image/d0f792eb-6a4b-4fe4-b922-00d189ae3a01.jpg",
// "latitude":"17.271497","longitude":"78.394182","is_exclusive":0,"invite_only":0,"invite_status":"","invite_stock":0,"rsvp":0,"
// invite_rsvp":0,"general_sale":1,"discount":0},

// {"event_id":1004,"event_name":"Coconut Theater’s \"Last Over\"","type":"Event","day_type":"Singleday","location_name":"Rajpath Club","city":"Ahmedabad","state":"Gujarat","event_date":"2019-12-31","event_date_to":"2019-12-31","event_time":"7:00 AM","category":"BOLLYWOOD","language":"Hindi", "main_image_url":"https://api.ticketninja.in:3002/e_image/8613cef5-f842-4dfa-bc70-1e17ec7dfba4.JPG", "poster_image_url":"https://api.ticketninja.in:3002/e_image/da159c2b-a144-4e88-b561-2f105cf7e54d.JPG", "latitude":"23.03430230953805","longitude":"72.50887602111811","is_exclusive":0,"invite_only":0,"invite_status":"","invite_stock":0,"rsvp":0,"invite_rsvp":0,"general_sale":1,"discount":0},
// {"event_id":1007,"event_name":"Farhan Akhtar - Live in Concert","type":"Concert","day_type":"Singleday","location_name":"Race Course Ground","city":"Rajkot","state":"Gujarat","event_date":"2019-12-31", "event_date_to":"2019-12-31","event_time":"7:00 AM","category":"BOLLYWOOD","language":"Hindi", "main_image_url":"https://api.ticketninja.in:3002/e_image/925ac791-fd39-42be-b63a-109195a2da3a.jpg","poster_image_url":"https://api.ticketninja.in:3002/e_image/14d308cf-51e8-4c22-9432-e2c63d02e18d.jpg","latitude":"22.3062012","longitude":"70.7900754","is_exclusive":0,"invite_only":0,"invite_status":"","invite_stock":0,"rsvp":0,"invite_rsvp":0,"general_sale":1,"discount":0},
// {"event_id":1042,"event_name":"Test Tata Theater","type":"Play","day_type":"Singleday","location_name":"Tata Theatre","city":"Mumbai","state":"Maharashtra ","event_date":"2020-01-01","event_date_to":"2020-01-01","event_time":"8:00 PM","category":"","language":"Hindi","main_image_url":"https://api.ticketninja.in:3002/e_image/4345b3f5-8b1d-400e-8787-98076579c242.jpg","poster_image_url":"https://api.ticketninja.in:3002/e_image/e9daa149-c22e-46d0-bf3e-33e2ae29192f.jpg","latitude":"23.043662","longitude":"72.51496999999995","is_exclusive":0,"invite_only":0,"invite_status":"","invite_stock":0,"rsvp":0,"invite_rsvp":0,"general_sale":1,"discount":0},
// {"event_id":1034,"event_name":"Ahmedabad Food Fest Test","type":"Event","day_type":"Multipleday","location_name":"Rajpath Club","city":"Ahmedabad","state":"Gujarat","event_date":"2021-12-01","event_date_to":"2021-12-03","event_time":"4:00 PM","category":"Food Fest ","language":"Hindi,English","main_image_url":"https://api.ticketninja.in:3002/e_image/6d28383a-85f4-41da-bb10-69d30d0f7fad.jpg","poster_image_url":"https://api.ticketninja.in:3002/e_image/854074d9-2662-4616-a24a-d3993d7bb223.jpg","latitude":"23.03430230953805","longitude":"72.50887602111811","is_exclusive":0,"invite_only":0,"invite_status":"",
// "invite_stock":0,"rsvp":0,"invite_rsvp":0,"general_sale":1,"discount":0}]

// }}
