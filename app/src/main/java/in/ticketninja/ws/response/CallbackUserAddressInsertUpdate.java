/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.response;

import in.ticketninja.ws.RestApi;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class CallbackUserAddressInsertUpdate {

    private wsUserAddress IN_UP_User_Address = new wsUserAddress();

    public wsUserAddress data() {
        return IN_UP_User_Address;
    }

    // {"IN_UP_User_Address":{"error_code":1,"error_description":"Address Save Successfully","ServiceName":"IN_UP_User_Address_Return","Address_Id":1072}}
    public class wsUserAddress {

        private long Address_Id;

        public long getAddressId() {
            return Address_Id;
        }

        private int ErrorCode;
        private String ErrorMessage;
        private String ServiceName;

        public boolean isSuccess() {
            return ErrorCode == RestApi.ErrorCode.SUCCESS;
        }

        public String message() {
            return ErrorMessage;
        }


    }
}

