/*
 * Copyright (c) 2018. Shreya Prajapati
 * Developed by Shreya Prajapati for NicheTech Computer Solutions Pvt. Ltd. use only.
 *
 */

package in.ticketninja.ws.response;

import in.ticketninja.objects.ApplyOfferCode;

public class CallbackApplyPromoCode {

    public String error_code ="";
    private String error_description = "";
    private ApplyOfferCode data;

    public ApplyOfferCode data() {
        return data;
    }

    public String getError_code() {
        return error_code;
    }

    public String getError_description() {
        return error_description;
    }

}
