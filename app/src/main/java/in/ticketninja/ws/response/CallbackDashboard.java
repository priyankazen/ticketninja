/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.response;

import java.util.List;

import in.ticketninja.objects.MyEvent;
import in.ticketninja.objects.MySlider;
import in.ticketninja.objects.PromoSliderList;
import in.ticketninja.objects.SiteMenu;
import in.ticketninja.ws.RestApi;

/**
 * TicketNinja_Working(in.ticketninja.ws.response) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 8/6/17.
 *
 * @author Suthar Rohit
 */
public class CallbackDashboard {

    public String error_code ="";
    private String error_description = "";

    public Get_Dashboard_ListResult data;

    public CallbackDashboard.Get_Dashboard_ListResult data() {
        return data;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    @Override
    public String toString() {
        return "CallbackDashboard{" +
                "data=" + data +
                '}';
    }

    public class Get_Dashboard_ListResult {

       /* "error_code":1,
                "error_description":"sucess",
                "ServiceName":"data",*/

        public int error_code;
        public String error_description;
        public String ServiceName;
        private List<MySlider> slider_list;
        private List<SiteMenu> site_menu;
        private List<MyEvent> featur_event_list;
        private List<PromoSliderList> Promo_Slider_List;

        private int featured_events_count;
        private int slider_count;

        public boolean isSuccess() {
            return error_code == RestApi.ErrorCode.SUCCESS;
        }

        public String message() {
            return error_description;
        }

        public List<MySlider> getSlider_list() {
            return slider_list;
        }

        public List<SiteMenu> getSite_menu() {
            return site_menu;
        }

        public List<PromoSliderList> getPromo_Slider_List() {
            return Promo_Slider_List;
        }

        public List<MyEvent> getFeatur_event_list() {
            return featur_event_list;
        }

        public int getError_code() {
            return error_code;
        }

        public Get_Dashboard_ListResult(List<MyEvent> featured_Events_List, List<MySlider> slider_List) {
            featur_event_list = featured_Events_List;
            slider_list = slider_List;
        }


        @Override
        public String toString() {
            return "Get_Dashboard_ListResult{" +
                    "error_code=" + error_code +
                    ", error_description='" + error_description + '\'' +
                    ", ServiceName='" + ServiceName + '\'' +
                    ", slider_list=" + slider_list +
                    ", site_menu=" + site_menu +
                    ", featur_event_list=" + featur_event_list +
                    ", featured_events_count=" + featured_events_count +
                    ", slider_count=" + slider_count +
                    '}';
        }
    }


}





