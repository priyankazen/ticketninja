package in.ticketninja.ws.response;

import in.ticketninja.ws.RestApi;

/**
 * Created by Shreya on 3/15/18.
 */

public class CallbackSocialLogin {

    public String error_code ="";
    private String error_description = "";
    private UserSocialLogin data;

    public UserSocialLogin data(){
        return data;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public class UserSocialLogin {

        public int ErrorCode;
        public String ErrorMessage;
        public String ServiceName;
        public String birthdate;
        public String country_code ;
        public String email;
        public String firstname;
        public String gender;
        public String lastname;
        public String mobileno;
        public String user_id="0";
        public String user_image_url;
        public String user_imageid;
        public boolean is_email_required;
        public boolean is_alreadyregister;
        public int notification_unread_count;

        public boolean isSuccess() {
            return ErrorCode == RestApi.ErrorCode.SUCCESS;
        }

        public String message() {
            return ErrorMessage;
        }

        @Override
        public String toString() {
            return "UserSocialLogin{" +
                    ", ServiceName='" + ServiceName + '\'' +
                    ", birthdate='" + birthdate + '\'' +
                    ", country_code='" + country_code + '\'' +
                    ", email='" + email + '\'' +
                    ", firstname='" + firstname + '\'' +
                    ", gender='" + gender + '\'' +
                    ", lastname='" + lastname + '\'' +
                    ", mobileno='" + mobileno + '\'' +
                    ", user_id='" + user_id + '\'' +
                    ", user_image_url='" + user_image_url + '\'' +
                    ", user_imageid='" + user_imageid + '\'' +
                    ", is_email_required=" + is_email_required +
                    ", is_alreadyregister=" + is_alreadyregister +
                    ", notification_unread_count=" + notification_unread_count +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "CallbackSocialLogin{" +
                "error_code='" + error_code + '\'' +
                ", error_description='" + error_description + '\'' +
                ", data=" + data +
                '}';
    }
}
