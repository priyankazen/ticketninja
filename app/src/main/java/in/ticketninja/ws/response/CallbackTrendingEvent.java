/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.response;

import android.provider.ContactsContract;

import java.util.List;

import in.ticketninja.objects.MyEvent;
import in.ticketninja.ws.RestApi;

/**
 * TicketNinja_Working(in.ticketninja.ws.response) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 13/6/17.
 *
 * @author Suthar Rohit
 */
public class CallbackTrendingEvent {
    public String error_code ="";
    private String error_description = "";
    public Data data;

    public Data data() {
        return data;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public class Data {

        private List<MyEvent> trending_event_list;

        public List<MyEvent> getTrending_event_list() {
            return trending_event_list;
        }
    }
}