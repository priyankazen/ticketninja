/*
 * Copyright (c) 2018. Shreya Prajapati
 * Developed by Shreya Prajapati for NicheTech Computer Solutions Pvt. Ltd. use only.
 *
 */

package in.ticketninja.ws.response;

import in.ticketninja.objects.ApplyOfferCode;

public class CallbackRemovePromoCode {

    public String error_code ="";
    private String error_description = "";
    private ApplyOfferCode data;

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public ApplyOfferCode data() {
        return data;
    }

}
