/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.response;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import in.ticketninja.objects.EventPriceCategory;
import in.ticketninja.objects.EventTimeSlot;
import in.ticketninja.ws.RestApi;

/**
 * TicketNinja_Working(in.ticketninja.ws.response) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 11/8/17.
 *
 * @author Suthar Rohit
 */
public class CallbackGetAttraction implements Serializable {
    public String error_code ="";
    private String error_description = "";

    private Get_Attraction_Category data;

    public Get_Attraction_Category data() {
        return data;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public class Get_Attraction_Category {
        private int ErrorCode;
        private String ErrorMessage;
        private String ServiceName;
        private String event_id="";
        private String name="";
        public int is_time_slot;
        public ArrayList<EventPriceCategory> category_list;
        public ArrayList<EventTimeSlot> time_slot_list = new ArrayList<>();
        public String Discount_Label;
        public double Discount_Per;

        public String getDiscount_Label() {
            return Discount_Label;
        }

        public double getDiscount_Per() {
            return Discount_Per;
        }
/* "Discount_Label":"",
                "Discount_Per":0,*/

        public List<EventPriceCategory> getCategory_list() {
            return category_list;
        }

        public List<EventTimeSlot> getTime_slot_list() {
            return time_slot_list;
        }

        public boolean isSuccess() {
            return ErrorCode == RestApi.ErrorCode.SUCCESS;
        }

        public String message() {
            return ErrorMessage;
        }


    }

}
