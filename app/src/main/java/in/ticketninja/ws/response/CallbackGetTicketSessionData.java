/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.response;

import in.ticketninja.objects.TicketSessionData;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class CallbackGetTicketSessionData {

    public String error_code ="";
    private String error_description = "";
    private TicketSessionData data;

    public TicketSessionData data() {
        return data;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }
}




// https://ticketninja.in:3001/place_order_service/get_ticket_session_data
//{"error_code":"1","error_description":"sucess",
// "data":{"session_id":"fBfiq2UttQ3TOETQfDV00MljcAqLlpzr","event_id":1009,
// "detail_image_url":"https://ticketninja.in:4430/Admin/Document/b8ec9bfc-e5b4-4801-a12c-6bd65248d8bb.jpg",
// "day_type":"Perpetual","type":"Attraction","event_name":"Attraction Test 01",
// "location_name":"City Palace-Udaipur","event_date":"2019-03-18",
// "event_date_to":"2019-03-18","event_time":"undefined","discount_per":0,
// "coupon_id":0,"coupon_code":"","coupon_type":"","coupon_per":0,"coupon_amount":0,
// "is_discount":0,"session_date":"2019-03-18T05:26:18.000Z","
// category_list":[{"category_id":1040,"show_id":0,"event_date":"2019-03-18",
// "time":"undefined","category_name":"Enterance and Guide Bundle",
// "seat_type":"Bundle","mrp":500,"rate":500,"c_desc":"Remark","noofticket":1,
// "seat_no":"","amount":500,"addons":0,"seasonpass":0}],
// "total_ticket":1,"amount":500,
// "total_processingfee":0,"processingfee":0,"tax":0,"tax_type":"","discount":0,
// "netamount":500,
// "boxoffice_payment_type":[{"type_id":1000,"type_name":"Cash"},{"type_id":1001,"type_name":"Card"},{"type_id":1002,"type_name":"eWallet"},{"type_id":1003,"type_name":"Cheque"},{"type_id":1004,"type_name":"Complementary"},{"type_id":1005,"type_name":"COD"},{"type_id":1006,"type_name":"Online"}]}}
