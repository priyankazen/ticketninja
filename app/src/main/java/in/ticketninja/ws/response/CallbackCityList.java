/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.ws.response;

import androidx.annotation.NonNull;

import java.util.List;

import in.ticketninja.objects.CityList;
import in.ticketninja.ws.RestApi;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 07-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class CallbackCityList {
    public String error_code ="";
    private String error_description = "";
    private CityData data = new CityData();


    @NonNull
    public CityData data() {
        return data;
    }

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }

    public String getError_description() {
        return error_description;
    }

    public void setError_description(String error_description) {
        this.error_description = error_description;
    }

    public class CityData {
        private int ErrorCode;
        private String ErrorMessage;
        private String ServiceName;
        public List<CityList> city_list;
        public List<CityList> top_city_list;
        public List<CityList> other_city_list;

        public boolean isSuccess() {
            return ErrorCode == RestApi.ErrorCode.SUCCESS;
        }

        public String message() {
            return ErrorMessage;
        }

        @Override
        public String toString() {
            return "CityData{" +
                    "city_list=" + city_list +
                    ", top_city_list=" + top_city_list +
                    ", other_city_list=" + other_city_list +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "CallbackCityList{" +
                "error_code='" + error_code + '\'' +
                ", error_description='" + error_description + '\'' +
                ", data=" + data +
                '}';
    }
}

