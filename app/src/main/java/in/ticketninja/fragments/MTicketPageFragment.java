/*
 * Copyright (c) 2017. Aditi PArikh
 * Developed by Aditi Parikhfor NicheTech Computer Solutions Pvt. Ltd. use only.
 */

package in.ticketninja.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.util.Objects;

import in.ticketninja.activity.MTicketsActivity;
import in.ticketninja.R;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DateTimeUtils;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.database.DatabaseHelper;
import in.ticketninja.objects.BookingHistory;
import in.ticketninja.objects.BookingTicketList;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestRevokeTicket;
import in.ticketninja.ws.response.CallbackRevokeTicket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MTicketPageFragment extends Fragment {
    private final String TAG = MTicketPageFragment.class.getSimpleName();
    /**
     * The argument key for the page number this fragment represents.
     */
    private static final String ARG_POSITION = "argPosition";
    private static final String ARG_TICKET_ITEM = "argHistory";

    private CommonClass CC;
    private DatabaseHelper DB;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;

    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_POSITION}, {@link #ARG_TICKET_ITEM}.
     */
    private int mPageNumber;
    private BookingHistory bookingHistory = new BookingHistory();
    private TextView tv_event_title;
    private TextView tv_event_location;
    private TextView tv_ticket_id;
    private TextView tv_booking_id;
    private TextView tv_ticket_category;
    private TextView tv_add_person;
    private TextView tv_email;
    private TextView tv_phone;
    private TextView tvEventDate, tvEventTime;
    private TextView tvSeatNo, tvSharedLabel, tvSeatType, tv_ticketShared;
    private Button btnShareRevoke;

    private View llSeatNo, llSeatType, llShared;
    private ImageView ivEventPoster;
    private ImageView iv_barcode;
    private BookingTicketList ticket;
    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     */
    public static MTicketPageFragment create(Context context, int pageNumber, BookingHistory history) {
        MTicketPageFragment fragment = new MTicketPageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, pageNumber);
        args.putSerializable(ARG_TICKET_ITEM, history);
        fragment.setArguments(args);
        return fragment;
    }

    public MTicketPageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = Objects.requireNonNull(getArguments()).getInt(ARG_POSITION);
        bookingHistory = (BookingHistory) getArguments().getSerializable(ARG_TICKET_ITEM);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater
                .inflate(R.layout.fragment_my_ticket, container, false);

        CC = new CommonClass(getActivity());
        DB = new DatabaseHelper(getActivity());
        mLogin = new LoginUtils(getActivity());
        mLoader = new SRKLoaderDialog(getActivity());

        tv_event_title = rootView.findViewById(R.id.tv_event_title);
        tv_event_location = rootView.findViewById(R.id.tv_event_location);
        tv_ticket_id = rootView.findViewById(R.id.tv_ticket_id);
        tv_booking_id = rootView.findViewById(R.id.tv_booking_id);
        tv_ticket_category = rootView.findViewById(R.id.tv_ticket_category);
        tv_email = rootView.findViewById(R.id.tv_email);
        tv_phone = rootView.findViewById(R.id.tv_phone);
        tv_add_person = rootView.findViewById(R.id.tv_add_person);
        tvSeatNo = rootView.findViewById(R.id.tvSeatNo);
        tvSeatType = rootView.findViewById(R.id.tvSeatType);
        tvEventDate = rootView.findViewById(R.id.tvEventDate);
        tvEventTime = rootView.findViewById(R.id.tvEventTime);
        llSeatNo = rootView.findViewById(R.id.llSeatNo);
        llSeatType = rootView.findViewById(R.id.llSeatType);
        llShared = rootView.findViewById(R.id.llShared);
        tvSharedLabel = rootView.findViewById(R.id.tvSharedLabel);
        btnShareRevoke = rootView.findViewById(R.id.btnShareRevoke);
        tv_ticketShared = rootView.findViewById(R.id.tv_ticketShared);

        ivEventPoster = rootView.findViewById(R.id.ivEventPoster);
        iv_barcode = rootView.findViewById(R.id.iv_barcode);

        iv_barcode.setOnClickListener(v -> MTicketsActivity.openBarcode());
        invalidateData(mPageNumber, bookingHistory);
        return rootView;
    }

    private void invalidateData(int position, BookingHistory data) {
        ticket=new BookingTicketList();
        if (data.getTicketList().size()>0) {
             ticket = data.getTicketList().get(position);
            tv_event_title.setText(Validate.isNotNull(data.getName()) ? String.format("%s", data.getName()) : "");
            tv_event_location.setText(Validate.isNotNull(data.getVenueName()) ? String.format("%s", data.getVenueName()) : "");
            tv_event_location.setOnClickListener(v -> CommonClass.openMap(v.getContext(), data.getVenueName(), data.getLatitude(), data.getLongitude()));
            tv_ticket_id.setText(Validate.isNotNull(ticket.ticketId()) ? ticket.ticketId() : "");
            tv_booking_id.setText(Validate.isNotNull(data.getBookingId()) ? data.getBookingId() : "");
            tv_ticket_category.setText(Validate.isNotNull(ticket.categoryName()) ? ticket.categoryName() : "");
            tv_add_person.setText(Validate.isNotNull(ticket.ticketDesc()) ? ticket.ticketDesc() : "");
        }

        /*if (Validate.isNotNull(data.getMDDate())) {
            if (ticket.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY) && data.isSeasonPass()) {
                tvEventDate.setText(Validate.isNotNull(data.getMDDate()) ? String.format("%s, %s", data.getMDDate(), data.getYear()) : "");
            } else {
                tvEventDate.setText(Validate.isNotNull(data.getMDDate()) ? String.format("%s, %s\n%s", data.getMDDate(), data.getYear(), data.getDay()) : "");
            }
        } else {
            tvEventDate.setText("-");
        }*/
        if (Validate.isNotNull(ticket.getEvent_date()) && Validate.isDateNotNull(ticket.getEvent_date())) {
            if ((ticket.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY)
                    || ticket.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW))
                    && !ticket.getEvent_date().equalsIgnoreCase(ticket.getDateTo())
                    && Constant.Code.ONE == data.isSeasonPass()) {
                String date = DateTimeUtils.changeDateTimeFormat(ticket.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, DateTimeUtils.OUTPUT_FORMAT);
                String dateTo = DateTimeUtils.changeDateTimeFormat(ticket.getDateTo(), DateTimeUtils.SERVER_FORMAT_DATE, DateTimeUtils.OUTPUT_FORMAT);
                tvEventDate.setText(Validate.isNotNull(date) ? date + "-" + dateTo : "");
            } else {
                String date = DateTimeUtils.changeDateTimeFormat(ticket.getEvent_date(), DateTimeUtils.SERVER_FORMAT_DATE, DateTimeUtils.OUTPUT_FORMAT_WITH_DAY);
                tvEventDate.setText(Validate.isNotNull(date) ? date : "");
            }
        } else {
            tvEventDate.setText("-");
        }

        if (ticket.getDayType().equalsIgnoreCase(Constant.EventDayType.SINGLE_DAY)) {
            tvEventTime.setText(String.format("%s %s", ticket.getEvent_time(), Constant.ONWARDS));
        } else if (ticket.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTIPLE_DAY)
                || ticket.getDayType().equalsIgnoreCase(Constant.EventDayType.MULTI_DAY_MULTI_SHOW)) {
            tvEventTime.setText(String.format("%s %s", ticket.getEvent_time(), Constant.ONWARDS));
        } else if (ticket.getDayType().equalsIgnoreCase(Constant.EventDayType.PERPETUAL)) {
            tvEventTime.setText(String.format("%s", ticket.getEvent_time()));
        } else {
            tvEventTime.setText(String.format("%s", ticket.getEvent_time()));
        }

        if (ticket.isStatusReceived()) {
            iv_barcode.setAlpha(1f);
            tv_ticketShared.setVisibility(View.GONE);

            llShared.setVisibility(View.VISIBLE);
            tvSharedLabel.setText(getString(R.string.m_ticket_shared_from, ticket.sharedFrom()));
            btnShareRevoke.setVisibility(View.GONE);
            btnShareRevoke.setOnClickListener(null);

        } else if (ticket.isStatusShared()) {
            iv_barcode.setAlpha(0.2f);
            tv_ticketShared.setVisibility(View.VISIBLE);

            llShared.setVisibility(View.VISIBLE);
            tvSharedLabel.setText(getString(R.string.m_ticket_shared_to, ticket.sharedTo()));
            btnShareRevoke.setVisibility(View.VISIBLE);
            btnShareRevoke.setOnClickListener(view -> {
                if (CC.isOnline()) {
                    //wsRevokeTicket(ticket.trans_id);
                    revokeTicket(ticket.trans_id);
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });

        } else {
            iv_barcode.setAlpha(1f);
            tv_ticketShared.setVisibility(View.GONE);

            llShared.setVisibility(View.GONE);
            btnShareRevoke.setVisibility(View.GONE);
        }

        if (Validate.isNotNull(ticket.getEventType()) && ticket.getEventType().equalsIgnoreCase(Constant.EVENT_TYPE_ATTRACTION)) {
            llSeatNo.setVisibility(View.GONE);
            llSeatType.setVisibility(View.GONE);
        } else {
            if (Validate.isNotNull(ticket.seatNo()) && ticket.seatType().equalsIgnoreCase(Constant.SEAT_TYPE_ARRANGE_SEATING) || ticket.seatType().equalsIgnoreCase(Constant.SEAT_TYPE_BLOCK_ARRANGE_SEATING)) {
                llSeatNo.setVisibility(View.VISIBLE);
                llSeatType.setVisibility(View.GONE);

            } else if (Validate.isNotNull(ticket.seatType())) {
                llSeatNo.setVisibility(View.GONE);
                llSeatType.setVisibility(View.VISIBLE);
            } else {
                llSeatNo.setVisibility(View.GONE);
                llSeatType.setVisibility(View.GONE);
            }
        }

        if (Validate.isNotNull(ticket.seatNo())) {
            tvSeatNo.setText(ticket.seatNo());
            tvSeatType.setText("");
        } else if (Validate.isNotNull(ticket.seatType())) {
            tvSeatNo.setText("");
            tvSeatType.setText(ticket.seatType());
        } else {
            tvSeatNo.setText("");
            tvSeatType.setText("");
        }


        if (getActivity() != null && isAdded()) {
            if (Validate.isNotNull(data.getImageUrl())) {
               // File f = new File(Constant.IMAGE_DOWNLOAD_PATH + data.getImageId());
                File f = new File("");
                if (f.exists()) {
                    Glide.with(getActivity())
                            .load(f)
                            //.centerCrop()
                            .skipMemoryCache(false)
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .skipMemoryCache(false)
                            .into(ivEventPoster);
                } else {
                    Glide.with(getActivity())
                            .load(data.getImageUrl())
                            //.centerCrop()
                            .skipMemoryCache(false)
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .skipMemoryCache(false)
                            .into(ivEventPoster);

                }

                ivEventPoster.setVisibility(View.VISIBLE);
            } else {
                ivEventPoster.setVisibility(View.GONE);
            }

            //File f = new File(Constant.IMAGE_DOWNLOAD_PATH + ticket.getQRCode() + ".png");
            File f = new File("");
            if (f.exists()) {
                Glide.with(getActivity())
                        .load(f)
                        .centerCrop()
                        .skipMemoryCache(false)
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .skipMemoryCache(false)
                        .into(iv_barcode);
            } else {
                if (Validate.isNotNull(ticket.getQR_Code_URL())){
                    Glide.with(getActivity())
                            .load(Constant.generateQRCodeURL1(ticket.getQR_Code_URL()))
                            .centerCrop()
                            .skipMemoryCache(false)
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .skipMemoryCache(false)
                            .into(iv_barcode);
                }else {
                    Glide.with(getActivity())
                            .load(Constant.generateQRCodeURL(ticket.getQRCode()))
                            .centerCrop()
                            .skipMemoryCache(false)
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .skipMemoryCache(false)
                            .into(iv_barcode);
                }

            }
        }

        tv_email.setOnClickListener(view -> {
            Intent i = new Intent(Intent.ACTION_SENDTO);
            String email = tv_email.getText().toString().trim();
            i.setData(Uri.parse("mailto:" + email));

            try {
                startActivity(i);
            } catch (android.content.ActivityNotFoundException ex) {
                MessageUtils.showToast(getActivity(), R.string.msg_no_email_client_found);
            }
        });

        tv_phone.setOnClickListener(view -> {
            Utility.hideKeyboard(Objects.requireNonNull(getActivity()));
            String number = tv_phone.getText().toString().trim();
            Utility.callPhone(getActivity(), number);
        });

    }

    /**
     * Returns the page number represented by this fragment object.
     */
    public int getPageNumber() {
        return mPageNumber;
    }

    public void openBarcodeView(View rootView, String bookingId, String imageUrl) {
        View barcodeView = rootView.findViewById(R.id.barcodeView);
        TextView tvBookingId = rootView.findViewById(R.id.tvBookingId);

        ImageView ivLargeBarcode = rootView.findViewById(R.id.ivBarcode);
        ImageView ivCloseButton = rootView.findViewById(R.id.ivBarcodeClose);

        barcodeView.setVisibility(View.VISIBLE);

        ivCloseButton.setOnClickListener(v -> barcodeView.setVisibility(View.GONE));

        if (Validate.isNotNull(bookingId)) tvBookingId.setText(bookingId);

        if (Validate.isNotNull(imageUrl)) {
            // int widthDP = Utility.intToDP(getContext(), 250);
            Glide.with(getContext())
                    .load(imageUrl)
                    .fitCenter()
                    .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                    .into(ivLargeBarcode);

        } else {
            Glide.with(getContext())
                    .load(R.drawable.ic_placeholder)
                    .into(ivLargeBarcode);
        }
    }

    private void revokeTicket(long transId) {
        CC.showAlert(R.string.msg_confirm_revoke_ticket,
                R.string.msg_confirm_title_revoke_ticket,
                android.R.string.yes,
                android.R.string.no,
                () -> {
                    if (CC.isOnline()) {
                        wsRevokeTicket(transId);
                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                });
    }

    // WS - REVOKE TICKET
    private void wsRevokeTicket(long trans_id) {
        if (!mLoader.isShowing()) mLoader.show();
        RequestRevokeTicket request = new RequestRevokeTicket(trans_id);
        TicketNinjaAPI tnAPI = RestApi.createAPI();
        Call<CallbackRevokeTicket> call = tnAPI.RevokeTicket(request);
        call.enqueue(new Callback<CallbackRevokeTicket>() {
            @Override
            public void onResponse(@NonNull Call<CallbackRevokeTicket> call, @NonNull Response<CallbackRevokeTicket> response) {
                try {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    if (response.isSuccessful()) {

                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                            if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description()))
                                CC.showToast(Objects.requireNonNull(response.body()).getError_description());

                            DB.updateBookingTicketListStatus(trans_id, mLogin.getUserId(), Constant.TicketStatus.OPEN);

                            BookingTicketList bookingTicketList = MTicketsActivity.refreshCurrentTicket(DB);

                            bookingHistory.ticket_list.set(mPageNumber, bookingTicketList);
                            invalidateData(mPageNumber, bookingHistory);
                        } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                            CC.showToast(Objects.requireNonNull(response.body()).getError_description());

                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } else {
                        CC.showToast(R.string.msg_something_wrong);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    CC.showToast(R.string.msg_something_wrong);
                }
            }

            @Override
            public void onFailure(@NonNull Call<CallbackRevokeTicket> call, @NonNull Throwable t) {
                t.printStackTrace();
                if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                CC.showToast(R.string.msg_something_wrong);
            }
        });
    }

}
