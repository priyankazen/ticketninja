/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.fragments;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.File;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.common.Constant;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.BookingTicketList;

public class MTicketBarcodePageFragment extends Fragment {
    //private final String TAG = MTicketBarcodePageFragment.class.getSimpleName();
    /**
     * The argument key for the page number this fragment represents.
     */
    private static final String ARG_POSITION = "argPosition";
    private static final String ARG_TICKET_ITEM = "argHistory";

    private BookingTicketList ticket;

    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     */
    public static MTicketBarcodePageFragment newInstance(int pageNumber, BookingTicketList ticketList) {
        MTicketBarcodePageFragment fragment = new MTicketBarcodePageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_POSITION, pageNumber);
        args.putSerializable(ARG_TICKET_ITEM, ticketList);
        fragment.setArguments(args);
        return fragment;
    }

    public MTicketBarcodePageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*
      The fragment's page number, which is set to the argument value for {@link #ARG_POSITION}, {@link #ARG_TICKET_ITEM}.
     */
        //int mPageNumber = Objects.requireNonNull(getArguments()).getInt(ARG_POSITION);
        ticket = (BookingTicketList) Objects.requireNonNull(getArguments()).getSerializable(ARG_TICKET_ITEM);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout containing a title and body text.
        ViewGroup rootView = (ViewGroup) inflater
                .inflate(R.layout.layout_my_ticket_dialogitem_barcode, container, false);

        // Set the title view to show the page number.

        ImageView iv_barcode = rootView.findViewById(R.id.ivBarcode);
        TextView tv_ticketShared = rootView.findViewById(R.id.tv_ticketShared);

        //File f = new File(Constant.IMAGE_DOWNLOAD_PATH + ticket.getQRCode() + ".png");
        File f = new File("");
        if (f.exists()) {
            Glide.with(getActivity())
                    .load(f)
                    .skipMemoryCache(false)
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .skipMemoryCache(false)
                    .into(iv_barcode);
        } else {
            if (Validate.isNotNull(ticket.getQR_Code_URL())) {
                Glide.with(getActivity())
                        .load(Constant.generateQRCodeURL1(ticket.getQR_Code_URL()))
                        .skipMemoryCache(false)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .skipMemoryCache(false)
                        .into(iv_barcode);
            }else {
                Glide.with(getActivity())
                        .load(Constant.generateQRCodeURL(ticket.getQRCode()))
                        .skipMemoryCache(false)
                        .centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .skipMemoryCache(false)
                        .into(iv_barcode);
            }

        }

        if (ticket.isStatusReceived()) {
            iv_barcode.setAlpha(1f);
            tv_ticketShared.setVisibility(View.GONE);

        } else if (ticket.isStatusShared()) {
            iv_barcode.setAlpha(0.2f);
            tv_ticketShared.setVisibility(View.VISIBLE);

        } else {
            iv_barcode.setAlpha(1f);
            tv_ticketShared.setVisibility(View.GONE);
        }

        return rootView;
    }

    /*
     * Returns the page number represented by this fragment object.
     */
    /*public int getPageNumber() {
        return mPageNumber;
    }*/

}
