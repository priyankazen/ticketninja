/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.fragments;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import in.ticketninja.R;
import in.ticketninja.activity.AboutUsActivity;
import in.ticketninja.activity.CitySelectionActivity;
import in.ticketninja.activity.ContactUsActivity;
import in.ticketninja.activity.CustomerCareActivity;
import in.ticketninja.activity.EventListActivity;
import in.ticketninja.activity.LandingActivity;
import in.ticketninja.activity.TermsAndConditionActivity;
import in.ticketninja.adapters.DashboardPagerAdapter;
import in.ticketninja.adapters.EventsAdapter;
import in.ticketninja.adapters.SiteMenuAdapter;
import in.ticketninja.common.CacheHelper;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.MyEvent;
import in.ticketninja.objects.MySlider;
import in.ticketninja.objects.PromoSliderList;
import in.ticketninja.objects.SiteMenu;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestDashboard;
import in.ticketninja.ws.response.CallbackDashboard;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeFragment extends Fragment implements ViewPager.OnPageChangeListener, ViewPager.PageTransformer {

    private final String TAG = HomeFragment.class.getSimpleName();
    private static final String PARAM_UPDATE_FLAG = "param_update_flag";
    private CommonClass CC;
    private PreferencesUtils mPreferences;
    private SRKLoaderDialog mLoader;
    private LoginUtils mLogin;
    private CacheHelper mCacheHelper;
    private boolean isDisplayFromCache;
    private int dotsCount;
    private ImageView[] dots;
    private static final float MIN_SCALE_DEPTH = 0.75f;
    private LinearLayout pager_indicator;

    private NestedScrollView nestedScrollView;
    private LinearLayout llErrorLayout;
    private ImageView ivErrorImage;
    private TextView tvErrorMessage, tvErrorButton;
    private ViewPager vpSliderPager;
    private RecyclerView rvSiteMenuList;
    private RecyclerView rvFeaturedEventList;

    private List<MySlider> mSliderList = new ArrayList<>();

    private DashboardPagerAdapter mSliderAdapter;
    // private EventsAdapter mFeaturedEventsAdapter;
    //private int startIndex = 0, endIndex = 5;

    private int currentPage = 0;
    private boolean isViewTouched = false;


    public static HomeFragment newInstance(boolean isDisplayFromCache) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putBoolean(PARAM_UPDATE_FLAG, isDisplayFromCache);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            isDisplayFromCache = getArguments().getBoolean(PARAM_UPDATE_FLAG);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_home, container, false);

        try {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            initializeData();

            // setHasOptionsMenu(true);

            // ACTIONBAR
            setupActionbar(v);


            findViewById(v);

            ImageView ivActionLocation = v.findViewById(R.id.ivActionLocation);
            ivActionLocation.setOnClickListener(v1 -> {
                if (CC.isOnline()) {
                    Intent intLocation = new Intent(getActivity(), CitySelectionActivity.class);
                    startActivity(intLocation);
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            });

            String myData = mCacheHelper.retrieve(CacheHelper.KEY_DASHBOARD, 1);
            Log.e(TAG, "isDashboardDisplayFromCache=" + isDisplayFromCache);
            if (isDisplayFromCache && Validate.isNotNull(myData)) {
                onResponse(myData);
            } else {
                if (CC.isOnline()) {
                    wsDashboard(mPreferences.getCityId() == 0 ? "" : mPreferences.getCityName());
                } else {
                    showNoInternet();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return v;
    }

    private void findViewById(View v) {
        try {
            nestedScrollView = v.findViewById(R.id.nestedScrollView);
            nestedScrollView.setVisibility(View.GONE);
            llErrorLayout = v.findViewById(R.id.llErrorLayout);
            ivErrorImage = v.findViewById(R.id.ivErrorImage);
            tvErrorMessage = v.findViewById(R.id.tvErrorMessage);
            tvErrorButton = v.findViewById(R.id.tvErrorButton);

            vpSliderPager = v.findViewById(R.id.pager_introduction);

            pager_indicator = v.findViewById(R.id.viewPagerCountDots);

            rvSiteMenuList = v.findViewById(R.id.rv_sitemenu);

            rvFeaturedEventList = v.findViewById(R.id.rv_event_list);
            rvFeaturedEventList.setNestedScrollingEnabled(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            CC = new CommonClass(getActivity());
            mPreferences = new PreferencesUtils(getActivity());
            mLogin = new LoginUtils(getActivity());
            mLoader = new SRKLoaderDialog(getActivity());
            mCacheHelper = new CacheHelper(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupActionbar(View v) {
        Toolbar toolbar = v.findViewById(R.id.toolbar1);
        ((LandingActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        if (((LandingActivity) getActivity()).getSupportActionBar() != null) {
            ActionBar supportActionBar = ((LandingActivity) getActivity()).getSupportActionBar();
            Objects.requireNonNull(supportActionBar).setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            supportActionBar.setDisplayHomeAsUpEnabled(false);
            supportActionBar.setDisplayShowHomeEnabled(false);
            ViewCompat.setElevation(toolbar, 10);

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                v.findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
        }
    }

    public void showNoInternet() {
        try {
            Glide.with(getActivity())
                    .load(R.drawable.ic_connection_lost)
                    .placeholder(R.drawable.ic_connection_lost_static)
                    .into(ivErrorImage);
            tvErrorMessage.setText(R.string.msg_no_internet);
            nestedScrollView.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.VISIBLE);
            tvErrorButton.setText(R.string.btn_try_again);
            tvErrorButton.setOnClickListener(v -> {
                if (CC.isOnline()) {
                    if (mPreferences.getCityId() == 0) {
                        wsDashboard("");
                    } else {
                        wsDashboard(mPreferences.getCityName());
                    }
                } else {
                    showNoInternet();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showError() {
        try {
            Glide.with(getActivity())
                    .load(R.drawable.ic_no_result_found)
                    .placeholder(R.drawable.ic_no_result_found)
                    .into(ivErrorImage);
            nestedScrollView.setVisibility(View.GONE);
            llErrorLayout.setVisibility(View.VISIBLE);

            tvErrorButton.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void setUiPageViewController() {
        try {
            if (getActivity() != null) {
                dotsCount = mSliderAdapter.getCount();
                dots = new ImageView[dotsCount];

                if (dotsCount > 1) {
                    for (int i = 0; i < dotsCount; i++) {
                        dots[i] = new ImageView(getActivity());
                        dots[i].setImageResource(R.drawable.nonselecteditem);

                        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                LinearLayout.LayoutParams.WRAP_CONTENT
                        );

                        params.setMargins(10, 0, 10, 0);

                        pager_indicator.addView(dots[i], params);
                    }
                    dots[0].setImageResource(R.drawable.selecteditem_dot);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        try {
            if (dotsCount > 1) {
                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageResource(R.drawable.nonselecteditem);
                }
                dots[position].setImageResource(R.drawable.selecteditem_dot);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onPageScrollStateChanged(int i) {
    }

    @Override
    public void transformPage(@NonNull View page, float position) {
        try {
            final float alpha;
            final float scale;
            final float translationX;

            if (position > 0 && position < 1) {
                // moving to the right
                alpha = (1 - position);
                scale = MIN_SCALE_DEPTH + (1 - MIN_SCALE_DEPTH) * (1 - Math.abs(position));
                translationX = (page.getWidth() * -position);
            } else {
                // use default for all other cases
                alpha = 1;
                scale = 1;
                translationX = 0;
            }

            page.setAlpha(alpha);
            page.setTranslationX(translationX);
            page.setScaleX(scale);
            page.setScaleY(scale);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    //.........................WS FOR DASHBOARD ............//
    private void wsDashboard(String type) {
        try {
            if (!mLoader.isShowing() && !Objects.requireNonNull(getActivity()).isFinishing())
                mLoader.show();

            TicketNinjaAPI ticketNinjaAPI = RestApi.createAPI();
            RequestDashboard request = new RequestDashboard(String.valueOf(mLogin.getUserId()), type, mPreferences.getCityName());
            Call<ResponseBody> call = ticketNinjaAPI.GetDashboard(request);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            String myData = Objects.requireNonNull(response.body()).string();
                            // Log.e(TAG, "response_myData: " + myData);
                            HomeFragment.this.onResponse(myData);
                            Objects.requireNonNull(response.body()).close();
                            mCacheHelper.save(CacheHelper.KEY_DASHBOARD, myData);
                        } else {
                            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                            tvErrorMessage.setText(R.string.msg_no_response);
                            showError();
                        }
                    } catch (Exception e) {
                        if (getActivity() != null && !getActivity().isFinishing()) {
                            e.printStackTrace();
                            tvErrorMessage.setText(R.string.msg_something_wrong);
                            showError();
                        }
                        //if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                    if (getActivity() != null) {
                        t.printStackTrace();
                        if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                        tvErrorMessage.setText(R.string.msg_something_wrong);
                        showError();
                    }
                }
            });

        } catch (Exception e) {
            if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
            e.printStackTrace();
        }

    }

    @SuppressLint("ClickableViewAccessibility")
    private void onResponse(String myData) {

        try {
            Gson gson = new Gson();
            Type listType = new TypeToken<CallbackDashboard>() {
            }.getType();
            CallbackDashboard dashboard = gson.fromJson(myData, listType);
            // Log.e(TAG, "dashboard.data()= " + dashboard.data());

            //if (dashboard.data().isSuccess()) {
            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(dashboard.getError_code())) {
                // mSliderList = dashboard.data().getSlider_list();
                List<MyEvent> mFeaturedEventList = dashboard.data().getFeatur_event_list();
                List<SiteMenu> mSiteMenuList = dashboard.data().getSite_menu();

                List<PromoSliderList> mPromoSliderList = dashboard.data().getPromo_Slider_List();
                if (mPromoSliderList != null && mPromoSliderList.size() > 0) {
                    for (int i = 0; i < mPromoSliderList.size(); i++) {
                        PromoSliderList list = mPromoSliderList.get(i);
                        MySlider mySlider = new MySlider();
                        mySlider.setEvent_id(list.getPromo_Id());
                        mySlider.setMain_Image_Id(list.getPromo_Image_Id());
                        mySlider.setSrc(list.getPromo_Image_URL());
                        mySlider.setUrl(list.getPromo_URL());
                        mySlider.setType(Constant.PROMO_SLIDER_LIST.PROMO);
                        mSliderList.add(mySlider);
                    }
                }

                mSliderList.addAll(dashboard.data().getSlider_list());

                Log.e(TAG, "mSliderList.datastartActivity()= " + mSliderList.size());
                // SLIDER
                mSliderAdapter = new DashboardPagerAdapter(getActivity(), mSliderList);
                vpSliderPager.setAdapter(mSliderAdapter);
                autoSwipe();
                setUiPageViewController();

                vpSliderPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    @Override
                    public void onPageScrolled(int i, float v, int i1) {
                    }

                    @Override
                    public void onPageSelected(int position) {
                        currentPage = vpSliderPager.getCurrentItem();
                        if (dotsCount > 1) {
                            for (int i = 0; i < dotsCount; i++) {
                                dots[i].setImageResource(R.drawable.nonselecteditem);
                            }
                            dots[position].setImageResource(R.drawable.selecteditem_dot);
                        }
                    }

                    @Override
                    public void onPageScrollStateChanged(int i) {

                    }
                });


                vpSliderPager.setOnTouchListener((view, motionEvent) -> {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            isViewTouched = true;
                            break;

                        case MotionEvent.ACTION_UP:
                            isViewTouched = false;
                            break;

                    }
                    return false;
                });

                vpSliderPager.setPageTransformer(false, (page, position) -> {
                    final float alpha;
                    final float scale;
                    final float translationX;

                    if (position > 0 && position < 1) {
                        // moving to the right
                        alpha = (1 - position);
                        scale = MIN_SCALE_DEPTH + (1 - MIN_SCALE_DEPTH) * (1 - Math.abs(position));
                        translationX = (page.getWidth() * -position);
                    } else {
                        // use default for all other cases
                        alpha = 1;
                        scale = 1;
                        translationX = 0;
                    }

                    page.setAlpha(alpha);
                    page.setTranslationX(translationX);
                    page.setScaleX(scale);
                    page.setScaleY(scale);
                });

                // SITE MENU
                SiteMenuAdapter mSiteMenuAdapter = new SiteMenuAdapter(mSiteMenuList, getActivity());
                mSiteMenuAdapter.setOnItemClickListener((position, menu) -> {
                    if (CC.isOnline()) {
                        Intent intEvent = new Intent(getActivity(), EventListActivity.class);
                        intEvent.putExtra(Constant.ScreenExtras.EVENT_TYPE_NAME, menu.name());
                        intEvent.putExtra(Constant.ScreenExtras.EVENT_TYPE_KEY, menu.key());
                        startActivity(intEvent);
                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                });
                rvSiteMenuList.setAdapter(mSiteMenuAdapter);

                // FEATURED EVENTS
                LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_animation_fall_down);
                rvFeaturedEventList.setLayoutAnimation(controller);
                EventsAdapter mFeaturedEventsAdapter = new EventsAdapter(getActivity(), mFeaturedEventList,Constant.DASHBOARD);
                rvFeaturedEventList.setAdapter(mFeaturedEventsAdapter);
                rvFeaturedEventList.scheduleLayoutAnimation();
                nestedScrollView.setVisibility(View.VISIBLE);
                llErrorLayout.setVisibility(View.GONE);

            } else if (Validate.isNotNull(dashboard.data().message())) {
                tvErrorMessage.setText(dashboard.data().message());
                showError();
            } else {
                tvErrorMessage.setText(R.string.msg_something_wrong);
                showError();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /*@SuppressLint("ClickableViewAccessibility")
    private void onResponse1(String myData) {
        try {
            new Thread(() -> {

                Gson gson = new Gson();
                Type listType = new TypeToken<CallbackDashboard>() {
                }.getType();
                CallbackDashboard dashboard = gson.fromJson(myData, listType);
                // Log.e(TAG, "dashboard.data()= " + dashboard.data());

                if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(dashboard.getError_code())) {
                    // mSliderList = dashboard.data().getSlider_list();
                    List<MyEvent> mFeaturedEventList = dashboard.data().getFeatur_event_list();
                    List<SiteMenu> mSiteMenuList = dashboard.data().getSite_menu();

                    List<PromoSliderList> mPromoSliderList = dashboard.data().getPromo_Slider_List();
                    if (mPromoSliderList != null && mPromoSliderList.size() > 0) {
                        for (int i = 0; i < mPromoSliderList.size(); i++) {
                            PromoSliderList list = mPromoSliderList.get(i);
                            MySlider mySlider = new MySlider();
                            mySlider.setEvent_id(list.getPromo_Id());
                            mySlider.setMain_Image_Id(list.getPromo_Image_Id());
                            mySlider.setSrc(list.getPromo_Image_URL());
                            mySlider.setUrl(list.getPromo_URL());
                            mySlider.setType(Constant.PROMO_SLIDER_LIST.PROMO);
                            mSliderList.add(mySlider);
                        }
                    }

                    mSliderList.addAll(dashboard.data().getSlider_list());
                    //Log.e(TAG, "mSliderList.data()= " + mSliderList.size());
                    //Log.e(TAG, "mFeaturedEventList.data()= " + mFeaturedEventList.size());

                    //FOR SLIDER
                    if (!mSliderList.isEmpty()) {
                        mSliderAdapter = new DashboardPagerAdapter(getActivity(), mSliderList);

                        Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
                            vpSliderPager.setAdapter(mSliderAdapter);
                            autoSwipe();
                            setUiPageViewController();
                            nestedScrollView.setVisibility(View.VISIBLE);
                        });

                        vpSliderPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                            @Override
                            public void onPageScrolled(int i, float v, int i1) {
                            }

                            @Override
                            public void onPageSelected(int position) {
                                currentPage = vpSliderPager.getCurrentItem();
                                if (dotsCount > 1) {
                                    for (int i = 0; i < dotsCount; i++) {
                                        dots[i].setImageResource(R.drawable.nonselecteditem);
                                    }
                                    dots[position].setImageResource(R.drawable.selecteditem_dot);
                                }
                            }

                            @Override
                            public void onPageScrollStateChanged(int i) {

                            }
                        });


                        vpSliderPager.setOnTouchListener((view, motionEvent) -> {
                            switch (motionEvent.getAction()) {
                                case MotionEvent.ACTION_DOWN:
                                    isViewTouched = true;
                                    break;
                                case MotionEvent.ACTION_UP:
                                    isViewTouched = false;
                                    break;
                            }
                            return false;
                        });

                        vpSliderPager.setPageTransformer(false, (page, position) -> {
                            final float alpha;
                            final float scale;
                            final float translationX;

                            if (position > 0 && position < 1) {
                                // moving to the right
                                alpha = (1 - position);
                                scale = MIN_SCALE_DEPTH + (1 - MIN_SCALE_DEPTH) * (1 - Math.abs(position));
                                translationX = (page.getWidth() * -position);
                            } else {
                                // use default for all other cases
                                alpha = 1;
                                scale = 1;
                                translationX = 0;
                            }

                            page.setAlpha(alpha);
                            page.setTranslationX(translationX);
                            page.setScaleX(scale);
                            page.setScaleY(scale);
                        });
                    }

                    // FOR SITE MENU
                    if (!mSiteMenuList.isEmpty()) {
                        SiteMenuAdapter mSiteMenuAdapter = new SiteMenuAdapter(mSiteMenuList, getActivity());
                        mSiteMenuAdapter.setOnItemClickListener((position, menu) -> {
                            if (CC.isOnline()) {
                                Intent intEvent = new Intent(getActivity(), EventListActivity.class);
                                intEvent.putExtra(Constant.ScreenExtras.EVENT_TYPE_NAME, menu.name());
                                intEvent.putExtra(Constant.ScreenExtras.EVENT_TYPE_KEY, menu.key());
                                startActivity(intEvent);
                            } else {
                                CC.showToast(R.string.msg_no_internet);
                            }
                        });

                        Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
                            rvSiteMenuList.setAdapter(mSiteMenuAdapter);
                            nestedScrollView.setVisibility(View.VISIBLE);
                        });

                    }

                    // FEATURED EVENTS
                    LayoutAnimationController controller = AnimationUtils.loadLayoutAnimation(getContext(), R.anim.layout_animation_fall_down);
                    rvFeaturedEventList.setLayoutAnimation(controller);
                     List<MyEvent> newList = new ArrayList<>(mFeaturedEventList.subList(startIndex, endIndex));
                    if (newList.size() > 0) {
                        mFeaturedEventsAdapter = new EventsAdapter(getActivity(), newList);

                        Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
                            rvFeaturedEventList.setAdapter(mFeaturedEventsAdapter);
                            rvFeaturedEventList.scheduleLayoutAnimation();
                            nestedScrollView.setVisibility(View.VISIBLE);
                            llErrorLayout.setVisibility(View.GONE);
                        });

                    }

                    Log.e(TAG, "stop= ");

                    nestedScrollView.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {
                        View view = v.getChildAt(v.getChildCount() - 1);
                        int diff = (view.getBottom() - (v.getHeight() + v.getScrollY()));

                        // if diff is zero, then the bottom has been reached
                        if (diff == 0) {
                            // do stuff
                            Log.e(TAG, "calllllllllllllllllll");
                            updateIndex(mFeaturedEventList);
                        }
                    });

                } else if (Validate.isNotNull(dashboard.data().message())) {
                    Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
                        tvErrorMessage.setText(dashboard.data().message());
                        showError();
                    });
                } else {
                    Objects.requireNonNull(getActivity()).runOnUiThread(() -> {
                        tvErrorMessage.setText(R.string.msg_something_wrong);
                        showError();
                    });
                }

                if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();

            }).run();

        }catch (Exception e){
            e.printStackTrace();
        }

    }*/

    /*private void updateIndex(List<MyEvent> list) {
        startIndex = endIndex;
        endIndex = endIndex + 5;
        if (endIndex > list.size()) {
            endIndex = list.size();
        }else if (endIndex == list.size()){
            return;
        }
        Log.e(TAG, "startIndex: " + startIndex + " , endIndex : " + endIndex);
        List<MyEvent> newList = new ArrayList<>(list.subList(startIndex, endIndex));
        rvFeaturedEventList.post(() -> mFeaturedEventsAdapter.addArrayItem(startIndex, newList));
        rvFeaturedEventList.smoothScrollToPosition(list.size()-1);
    }*/

    private void autoSwipe() {
        try {
            final Handler handler = new Handler();
            final Runnable Update = () -> {

                //Log.i("currentPage == ", "" + currentPage);

                if (currentPage == mSliderAdapter.getCount()) {
                    currentPage = 0;
                }

                vpSliderPager.setCurrentItem(currentPage, true);
                currentPage = currentPage + 1;
            };

            Timer timer = new Timer();
            timer.schedule(new TimerTask() { // task to be scheduled

                @Override
                public void run() {
                    if (!isViewTouched)
                        handler.post(Update);
                }
            }, 500, 5000);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.more_menu, menu);  // Use filter.xml from step 1
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        //int id = item.getItemId();
        try {
            switch (item.getItemId()) {
                case R.id.customer_care:
                    Intent iCustomer = new Intent(getActivity(), CustomerCareActivity.class);
                    Objects.requireNonNull(getActivity()).startActivity(iCustomer);
                    return true;


                case R.id.aboutus:
                    Intent i = new Intent(getActivity(), AboutUsActivity.class);
                    Objects.requireNonNull(getActivity()).startActivity(i);
                    return true;

                case R.id.contactus:
                    Intent iContactUs = new Intent(getActivity(), ContactUsActivity.class);
                    Objects.requireNonNull(getActivity()).startActivity(iContactUs);
                    return true;

                case R.id.terms_n_condition:
                    Intent iTerms_condition = new Intent(getActivity(), TermsAndConditionActivity.class);
                    Objects.requireNonNull(getActivity()).startActivity(iTerms_condition);
                    return true;

                case R.id.privacy_policy:
                    Intent iPrivacy = new Intent(getActivity(), AboutUsActivity.class);
                    iPrivacy.putExtra(Constant.ScreenExtras.PRIVACY, Constant.ScreenExtras.PRIVACY);
                    Objects.requireNonNull(getActivity()).startActivity(iPrivacy);
                    return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return super.onOptionsItemSelected(item);
    }

}
