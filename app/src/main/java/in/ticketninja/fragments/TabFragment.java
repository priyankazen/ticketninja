package in.ticketninja.fragments;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.tabs.TabLayout;

import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.activity.LandingActivity;
import in.ticketninja.activity.MyTicketsActivity;
import in.ticketninja.adapters.TabViewPagerAdapter;
import in.ticketninja.common.CustomViewPager;
import in.ticketninja.common.Utility;


public class TabFragment extends Fragment {

    public static TabFragment newInstance() {
        TabFragment fragment = new TabFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    private CustomViewPager pager;
    private TabLayout tabLayout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_tab_pager, container, false);
        findViewById(v);
        setUpToolbar(v);
        setupViewPager(pager);
        setTabBar();
        return v;
    }

    private void findViewById(View v) {
        try {
            pager = v.findViewById(R.id.pager);
            tabLayout = v.findViewById(R.id.tab_layout);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpToolbar(View v) {
        try {
            AppCompatActivity activity;
            if (getActivity() instanceof LandingActivity) {
                activity = (LandingActivity) getActivity();
            } else if (getActivity() instanceof MyTicketsActivity) {
                activity = (MyTicketsActivity) getActivity();
            } else {
                activity = null;
            }
            Toolbar toolbar = v.findViewById(R.id.toolbar);
            if (activity != null) {
                activity.setSupportActionBar(toolbar);
                if (activity.getSupportActionBar() != null) {
                    ActionBar supportActionBar = activity.getSupportActionBar();
                    supportActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                    supportActionBar.setDisplayHomeAsUpEnabled(false);
                    supportActionBar.setDisplayShowHomeEnabled(false);
                    ViewCompat.setElevation(toolbar, 10);

                    ImageView ivActionBack = v.findViewById(R.id.ivActionBack);
                    ivActionBack.setOnClickListener(view -> getActivity().onBackPressed());
                    ivActionBack.setVisibility(View.GONE);

                    TextView tvTitle = v.findViewById(R.id.tvTitle);
                    tvTitle.setText(R.string.str_myticket);
                    Utility.convertToLowerCase(tvTitle);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        TabViewPagerAdapter adapter = new TabViewPagerAdapter(getChildFragmentManager());
        adapter.addFrag(new MyTicketFragment(), Objects.requireNonNull(getActivity()).getResources().getString(R.string.ticket));
        adapter.addFrag(new CouponCodesFragment(), getString(R.string.notes));
        viewPager.setAdapter(adapter);
    }

    private void setTabBar() {
        try {
            tabLayout.setupWithViewPager(pager);
            //tabLayout.setSelectedTabIndicatorColor(getResources().getColor(R.color.themeRed));
           // tabLayout.setTabTextColors(getResources().getColor(R.color.themeGrayDark), getResources().getColor(R.color.themeRed));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //set custom tab view text with icon
    private void setupCustomTabTextIcons() {
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(getTabView(i));
        }
    }

    private View getTabView(int position) {
        @SuppressLint("InflateParams") View view = LayoutInflater.from(getActivity()).inflate(R.layout.item_custom_tab, null);
        TextView tabTextView = view.findViewById(R.id.tabTextView);
        tabTextView.setText(String.format("Tab %s", position));
        //ImageView tabImageView = view.findViewById(R.id.tabImageView);
        return view;
    }

}
