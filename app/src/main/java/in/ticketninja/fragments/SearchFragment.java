/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.fragments;

import android.animation.Animator;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
//import com.crashlytics.android.answers.Answers;
//import com.crashlytics.android.answers.SearchEvent;
import com.google.android.material.bottomsheet.BottomSheetDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.activity.EventDetailActivity;
import in.ticketninja.activity.LandingActivity;
import in.ticketninja.adapters.EventsAdapter;
import in.ticketninja.adapters.SearchHistoryAdapter;
import in.ticketninja.adapters.SearchSuggestionAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DividerItemDecoration;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.MessageUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.database.DatabaseHelper;
import in.ticketninja.objects.MyEvent;
import in.ticketninja.objects.Search_db;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestSearch;
import in.ticketninja.ws.request.RequestSearchAllEvent;
import in.ticketninja.ws.request.RequestTrendingEvent;
import in.ticketninja.ws.response.CallbackSearch;
import in.ticketninja.ws.response.CallbackSearchAllEvent;
import in.ticketninja.ws.response.CallbackTrendingEvent;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;


public class SearchFragment extends Fragment {

    private final int REQ_CODE_SPEECH_INPUT = 100;

    private final String TAG = SearchFragment.class.getSimpleName();
    private CommonClass CC;
    private DatabaseHelper DB;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;

    private final int LAYOUT_NONE = 0;
    private final int LAYOUT_HISTORY = 1;
    private final int LAYOUT_SUGGESTION = 2;
    private final int LAYOUT_RESULT = 3;
    private final int LAYOUT_ERROR = 4;
    private int currentScreen = -1;

    private EditText et_search;

    private TextView tvCancel, tvHistoryTitle, tvResultTitle;

    private View llHistoryLayout;
    private View llTrendingEventsLayout;
    private View llResultLayout;
    private View llSuggestionLayout;
    private View llErrorLayout;
    private ImageView ivErrorImage,ivSpeak;
    private TextView tvErrorMessage, tvErrorButton;

    private RecyclerView rvSearchHistory;
    private RecyclerView rvTrendingEvents;
    private List<Search_db> historyList = new ArrayList<>();

    private RecyclerView rvSearchResult;
    private List<MyEvent> resultList = new ArrayList<>();
    private EventsAdapter mSearchResultAdapter;

    private RecyclerView rvSuggestionEvent;
    private List<MyEvent> suggestionList = new ArrayList<>();
    private SearchSuggestionAdapter searchSuggestionAdapter;

    private Call<CallbackSearchAllEvent> suggestionCall;

    private BottomSheetDialog mBottomSheetDialog;

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_search, container, false);
        initializeData();
        setUpActionBar(v);
        findViewById(v);
        switchLayout(LAYOUT_NONE);
        invalidateSearch();
        setOnClickListener();
        return v;
    }

    private void setUpActionBar(View v) {
        try {
            // ACTIONBAR
            Toolbar toolbar = v.findViewById(R.id.toolbar);
            assert getActivity() != null;
            ((LandingActivity) getActivity()).setSupportActionBar(toolbar);
            if (((LandingActivity) getActivity()).getSupportActionBar() != null) {
                ActionBar supportActionBar = ((LandingActivity) getActivity()).getSupportActionBar();
                assert supportActionBar != null;
                supportActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                supportActionBar.setDisplayHomeAsUpEnabled(false);
                supportActionBar.setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                    v.findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setOnClickListener() {

        try {
            rvSuggestionEvent.addOnScrollListener(new RecyclerView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {


                    if (newState == RecyclerView.SCROLL_STATE_IDLE) {
                        Utility.hideKeyboard(Objects.requireNonNull(getActivity()), et_search);
                    }

                    super.onScrollStateChanged(recyclerView, newState);
                }

                @Override
                public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                    if (dy != 0)
                        Utility.hideKeyboard(Objects.requireNonNull(getActivity()), et_search);
                }
            });


            et_search.setOnKeyListener((v12, keyCode, event) -> {
                if (event.getAction() != KeyEvent.ACTION_DOWN)
                    return false;

                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    performSearch("");
                    return true;
                }
                return false;
            });

            et_search.setOnEditorActionListener((v1, actionId, event) -> {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch("");
                    return true;
                }
                return false;
            });

            et_search.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    tvCancel.setVisibility(Validate.isNull(s.toString().trim()) ? View.GONE : View.VISIBLE);
                    llTrendingEventsLayout.setVisibility(Validate.isNull(s.toString().trim()) ? View.VISIBLE : View.GONE);
                    if (CC.isOnline() && Validate.isNotNull(s.toString().trim())) {
                        wsSearchSuggestion(s.toString().trim());
                    } else if (Validate.isNull(s.toString().trim())) {
                        invalidateSearch();
                    }
                }
            });

            tvCancel.setOnClickListener(v13 -> {
                Utility.hideKeyboard(Objects.requireNonNull(getActivity()));
                et_search.setText("");
                //switchLayout(Validate.isNotNullList(historyList) ? LAYOUT_HISTORY : LAYOUT_NONE);
                //invalidateSearch();
            });

            ivSpeak.setOnClickListener(view -> startListeningWithDialog());

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void findViewById(View v) {
        try {

            llHistoryLayout = v.findViewById(R.id.llHistoryLayout);
            llResultLayout = v.findViewById(R.id.llResultLayout);
            llSuggestionLayout = v.findViewById(R.id.llSuggestionLayout);
            llErrorLayout = v.findViewById(R.id.llErrorLayout);
            ivErrorImage = v.findViewById(R.id.ivErrorImage);
            tvErrorMessage = v.findViewById(R.id.tvErrorMessage);
            tvErrorButton = v.findViewById(R.id.tvErrorButton);

            et_search = v.findViewById(R.id.et_search);
            tvCancel = v.findViewById(R.id.tv_cancel);
            tvResultTitle = v.findViewById(R.id.tvResultTitle);
            tvHistoryTitle = v.findViewById(R.id.tvHistoryTitle);

        /*actv_search.setThreshold(1);//will start working from first character
        actv_search.setAdapter(adapter);*/

            rvSearchHistory = v.findViewById(R.id.rv_search);
            // rvSearchHistory.addItemDecoration(new DividerItemDecoration(getActivity(), 45));
            rvSearchHistory.setLayoutManager(new LinearLayoutManager(getActivity()));

            rvSearchResult = v.findViewById(R.id.rv_search_event);
            rvSearchResult.setLayoutManager(new LinearLayoutManager(getActivity()));

            rvSuggestionEvent = v.findViewById(R.id.rv_suggestion_event);
            rvSuggestionEvent.addItemDecoration(new DividerItemDecoration(Objects.requireNonNull(getActivity()), 14));
            rvSuggestionEvent.setLayoutManager(new LinearLayoutManager(getActivity()));


            llTrendingEventsLayout = v.findViewById(R.id.llTrendingEventsLayout);
            rvTrendingEvents = v.findViewById(R.id.rvTrendingEvents);
            rvTrendingEvents.setLayoutManager(new LinearLayoutManager(getActivity()));

            ivSpeak = v.findViewById(R.id.ivSpeak);
            //ivSpeak.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void initializeData() {
        try {
            CC = new CommonClass(getActivity());
            DB = new DatabaseHelper(getActivity());
            mLogin = new LoginUtils(getActivity());
            mLoader = new SRKLoaderDialog(getActivity());

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void switchLayout(int screen) {
        try {
            if (currentScreen != screen) {
                tvCancel.setVisibility(Validate.isNull(et_search) ? View.GONE : View.VISIBLE);

                if (screen == LAYOUT_HISTORY) {
                    if (suggestionCall != null && suggestionCall.isExecuted()) suggestionCall.cancel();
                    llHistoryLayout.setVisibility(View.VISIBLE);
                    llSuggestionLayout.setVisibility(View.GONE);
                    llResultLayout.setVisibility(View.GONE);
                    llErrorLayout.setVisibility(View.GONE);
                    //tvCancel.setVisibility(View.GONE);
                    llTrendingEventsLayout.setVisibility(View.VISIBLE);

                } else if (screen == LAYOUT_SUGGESTION) {
                    llHistoryLayout.setVisibility(View.GONE);
                    llSuggestionLayout.setVisibility(View.VISIBLE);
                    llResultLayout.setVisibility(View.GONE);
                    llErrorLayout.setVisibility(View.GONE);
                    llTrendingEventsLayout.setVisibility(View.GONE);

                } else if (screen == LAYOUT_RESULT) {
                    llHistoryLayout.setVisibility(View.GONE);
                    llSuggestionLayout.setVisibility(View.GONE);
                    llResultLayout.setVisibility(View.VISIBLE);
                    llErrorLayout.setVisibility(View.GONE);
                    llTrendingEventsLayout.setVisibility(View.GONE);

                } else if (screen == LAYOUT_ERROR) {
                    llHistoryLayout.setVisibility(View.GONE);
                    llSuggestionLayout.setVisibility(View.GONE);
                    llResultLayout.setVisibility(View.GONE);
                    llErrorLayout.setVisibility(View.VISIBLE);

                } else {
                    if (suggestionCall != null && suggestionCall.isExecuted()) suggestionCall.cancel();
                    llHistoryLayout.setVisibility(View.GONE);
                    llSuggestionLayout.setVisibility(View.GONE);
                    llResultLayout.setVisibility(View.GONE);
                    llErrorLayout.setVisibility(View.GONE);
                    //tvCancel.setVisibility(View.GONE);
                    llTrendingEventsLayout.setVisibility(View.GONE);
                }
            }
            currentScreen = screen;
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void switchToResult(boolean toSearch) {
        try {
            if (toSearch) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    circulevisible(llHistoryLayout, llResultLayout);
                }

                switchLayout(LAYOUT_RESULT);

                tvCancel.setVisibility(View.VISIBLE);
                tvHistoryTitle.setVisibility(View.GONE);

            } else {
                resultList = new ArrayList<>();
                if (mSearchResultAdapter != null) mSearchResultAdapter.notifyDataSetChanged();

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    circulevisible(llResultLayout, llHistoryLayout);
                }

                switchLayout(Validate.isNotNullList(historyList) ? LAYOUT_HISTORY : LAYOUT_NONE);

                tvCancel.setVisibility(View.GONE);
                et_search.setText("");
                tvHistoryTitle.setVisibility(View.VISIBLE);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void showNoInternet() {
        try {
            Glide.with(getActivity())
                    .load(R.drawable.ic_connection_lost)
                    .placeholder(R.drawable.ic_connection_lost_static)
                    .into(ivErrorImage);
            tvErrorMessage.setText(R.string.msg_no_internet);

            switchLayout(LAYOUT_ERROR);

        /*tvErrorButton.setVisibility(View.VISIBLE);
        tvErrorButton.setText(R.string.btn_try_again);
        tvErrorButton.setOnClickListener(v -> {
            if (CC.isOnline()) {
                *//*et_search.setText(keyword);
                wsSearch(keyword);*//*
            } else {
                showNoInternet();
            }
        });*/
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public void showError(boolean noResult) {
        try {
            if (getActivity() != null) {
                Glide.with(getActivity())
                        .load(noResult ? R.drawable.ic_no_matching_result : R.drawable.ic_no_result_found)
                        .placeholder(noResult ? R.drawable.ic_no_matching_result : R.drawable.ic_no_result_found)
                        .into(ivErrorImage);
            }

            switchLayout(LAYOUT_ERROR);

            tvErrorMessage.setVisibility(noResult ? View.GONE : View.VISIBLE);
            tvErrorButton.setVisibility(View.GONE);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void invalidateSearch() {
        try {
            if (suggestionCall != null && suggestionCall.isExecuted()) suggestionCall.cancel();
            historyList = DB.getSearchHistory(mLogin.getUserId());

            if (historyList != null && historyList.size() > 0) {
                SearchHistoryAdapter mHistoryAdapter = new SearchHistoryAdapter(historyList);
                mHistoryAdapter.setOnItemClickListener(keyword -> {
                    Utility.hideKeyboard(Objects.requireNonNull(getActivity()));
                    if (CC.isOnline()) {
                        et_search.setText(keyword.trim());
                        wsSearch(keyword.trim());
                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                });
                rvSearchHistory.setAdapter(mHistoryAdapter);

                switchLayout(LAYOUT_HISTORY);
            } else {
                switchLayout(LAYOUT_NONE);
            }

            wsTradingEvent();
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void performSearch(String key) {
        try {
            Utility.hideKeyboard(Objects.requireNonNull(getActivity()));
            String searchKey;
            if (CC.isOnline()) {
                if (Validate.isNotNull(key)) {
                    searchKey = key;
                } else {
                    searchKey = et_search.getText().toString().trim();
                }

                if (Validate.isNotNull(searchKey)) {
                    et_search.setText(searchKey);
                    et_search.setSelection(searchKey.length());
                    wsSearch(searchKey);
                } else {
                    MessageUtils.showAlert(getActivity(), "Please enter keyword to search.");
                }
            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    // WS - Search
    private void wsTradingEvent() {
        try {
            if (!mLoader.isShowing() && !Objects.requireNonNull(getActivity()).isFinishing()) mLoader.show();
            TicketNinjaAPI ticketNinjaAPI = RestApi.createAPI();
            RequestTrendingEvent requestSearch = new RequestTrendingEvent();
            requestSearch.city = "";
            requestSearch.user_id = String.valueOf(mLogin.getUserId());
            Call<CallbackTrendingEvent> call = ticketNinjaAPI.getTradingEvent(requestSearch);
            call.enqueue(new Callback<CallbackTrendingEvent>() {
                @Override
                public void onResponse(@NonNull Call<CallbackTrendingEvent> call,@NonNull  Response<CallbackTrendingEvent> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                List<MyEvent> arrayList = response.body().data().getTrending_event_list();
                                if (arrayList != null && arrayList.size() > 0) {
                                    llTrendingEventsLayout.setVisibility(View.VISIBLE);
                                    SearchSuggestionAdapter adapter = new SearchSuggestionAdapter(arrayList);
                                    rvTrendingEvents.setAdapter(adapter);
                                    adapter.setOnItemClickListener((name, item) -> {
                                        try {
                                            Log.e(TAG,"name: "+name);
                                            et_search.setText(name);
                                            DB.addSearchHistory(name, mLogin.getUserId());
                                            Intent intDetail = new Intent(getActivity(), EventDetailActivity.class);
                                            intDetail.putExtra(Constant.ScreenExtras.EVENT_DATA, item);
                                            startActivity(intDetail);
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }

                                    });
                                } else {
                                    llTrendingEventsLayout.setVisibility(View.GONE);
                                }
                            } else if (Validate.isNotNull(response.body().getError_description())) {
                                llTrendingEventsLayout.setVisibility(View.GONE);
                            } else {
                                llTrendingEventsLayout.setVisibility(View.GONE);
                            }
                        } else {
                            llTrendingEventsLayout.setVisibility(View.GONE);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        llTrendingEventsLayout.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackTrendingEvent> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    llTrendingEventsLayout.setVisibility(View.GONE);
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    // WS - Search
    private void wsSearch(@NonNull String keyword) {
       /* try {
            Answers.getInstance().logSearch(new SearchEvent().putCustomAttribute("suggestion", "false").putQuery(keyword));
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        if (!mLoader.isShowing() && !Objects.requireNonNull(getActivity()).isFinishing()) mLoader.show();
        if (suggestionCall != null && suggestionCall.isExecuted()) suggestionCall.cancel();
        //switchToResult(true);

        try {
            TicketNinjaAPI ticketNinjaAPI = RestApi.createAPI();
            RequestSearch requestSearch = new RequestSearch(String.valueOf(mLogin.getUserId()), keyword);
            Call<CallbackSearch> call = ticketNinjaAPI.GetSearch(requestSearch);
            call.enqueue(new Callback<CallbackSearch>() {
                @Override
                public void onResponse(@NonNull Call<CallbackSearch> call,@NonNull  Response<CallbackSearch> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                resultList = new ArrayList<>();
                                resultList = response.body().data().event_list;
                                if (resultList != null && resultList.size() > 0) {
                                    if (resultList.size() > 1) {
                                        tvResultTitle.setText(String.format(Locale.getDefault(),
                                                "%ss (%d)", getString(R.string.search_result_title), resultList.size()));
                                    } else {
                                        tvResultTitle.setText(String.format(Locale.getDefault(),
                                                "%s (%d)", getString(R.string.search_result_title), resultList.size()));
                                    }

                                    mSearchResultAdapter = new EventsAdapter(getActivity(), resultList, Constant.DASHBOARD);
                                    rvSearchResult.setAdapter(mSearchResultAdapter);

                                    DB.addSearchHistory(keyword, mLogin.getUserId());

                                    switchLayout(LAYOUT_RESULT);

                                } else {
                                    tvErrorMessage.setText(R.string.msg_no_search_records_found);
                                    showError(true);
                                }
                            } else if (Validate.isNotNull(response.body().getError_description())) {
                                tvErrorMessage.setText(response.body().getError_description());
                                showError(true);
                            } else {
                                tvErrorMessage.setText(R.string.msg_no_search_records_found);
                                showError(true);
                            }
                        } else {
                            tvErrorMessage.setText(R.string.msg_no_response);
                            showError(false);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        tvErrorMessage.setText(R.string.msg_something_wrong);
                        showError(false);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackSearch> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    // WS - Search Suggestion
    private void wsSearchSuggestion(String keyword) {
       /* try {
            Answers.getInstance().logSearch(new SearchEvent().putCustomAttribute("suggestion", "true").putQuery(keyword));
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        try {
            TicketNinjaAPI ticketNinjaAPI = RestApi.createAPI();
            RequestSearchAllEvent requestSearch = new RequestSearchAllEvent(mLogin.getUserId(), keyword);
            if (suggestionCall != null && suggestionCall.isExecuted()) suggestionCall.cancel();
            suggestionCall = ticketNinjaAPI.GetSearchSuggestion(requestSearch);
            suggestionCall.enqueue(new Callback<CallbackSearchAllEvent>() {
                @Override
                public void onResponse(@NonNull Call<CallbackSearchAllEvent> call, @NonNull Response<CallbackSearchAllEvent> response) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    try {
                        if (response.isSuccessful()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                suggestionList = new ArrayList<>();
                                suggestionList = response.body().data().event_list;
                                if (suggestionList != null && suggestionList.size() > 0) {

                                    searchSuggestionAdapter = new SearchSuggestionAdapter(suggestionList);
                                    rvSuggestionEvent.setAdapter(searchSuggestionAdapter);

                                    switchLayout(LAYOUT_SUGGESTION);

                                    searchSuggestionAdapter.setOnItemClickListener((name, item) -> {
                                        try {
                                            Log.e(TAG,"name: "+name);
                                            DB.addSearchHistory(name, mLogin.getUserId());
                                            Intent intDetail = new Intent(getActivity(), EventDetailActivity.class);
                                            intDetail.putExtra(Constant.ScreenExtras.EVENT_DATA, item);
                                            startActivity(intDetail);
                                        }catch (Exception e){
                                            e.printStackTrace();
                                        }

                                    });

                                } else {
                                    switchLayout(LAYOUT_NONE);

                                    //tvErrorMessage.setText(R.string.msg_no_search_records_found);
                                    //showError();
                                }
                            } else if (Validate.isNotNull(response.body().getError_description())) {
                                switchLayout(LAYOUT_NONE);
                                //tvErrorMessage.setText(response.body().data().message());
                                //showError();
                            } else {
                                switchLayout(LAYOUT_NONE);
                                //tvErrorMessage.setText(R.string.msg_no_search_records_found);
                                //showError();
                            }
                        } else {
                            switchLayout(LAYOUT_NONE);
                            //tvErrorMessage.setText(R.string.msg_no_response);
                            //showError();
                        }

                    } catch (Exception e) {
                        switchLayout(LAYOUT_NONE);
                        e.printStackTrace();
                        //tvErrorMessage.setText(R.string.msg_something_wrong);
                        //showError();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackSearchAllEvent> call, @NonNull Throwable t) {
                    if (!call.isCanceled()) {
                        switchLayout(LAYOUT_NONE);
                        t.printStackTrace();
                        //if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                        //CC.showToast(R.string.msg_something_wrong);
                    }
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void circulevisible(View view, View visibleView) {
        try {
            // previously invisible view
            view.setVisibility(View.GONE);

            // get the center for the clipping circle
            int centerX = (visibleView.getLeft() + visibleView.getRight()) / 2;
            int centerY = (visibleView.getTop() + visibleView.getBottom()) / 2;

            int startRadius = 0;
            // get the final radius for the clipping circle
            int endRadius = Math.max(view.getWidth(), view.getHeight());

            // create the indicator_animator for this view (the start radius is zero)
            Animator anim = ViewAnimationUtils.createCircularReveal(visibleView, centerX, centerY, startRadius, endRadius);

            // make the view visible and start the animation
            visibleView.setVisibility(View.VISIBLE);
            anim.start();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Receiving speech input
     * */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == REQ_CODE_SPEECH_INPUT) {
                if (resultCode == RESULT_OK && null != data) {
                    ArrayList<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    String spokenText = Objects.requireNonNull(results).get(0);
                    Log.e(TAG,"spokenText: "+spokenText);
                    et_search.setText(spokenText);
                    et_search.setSelection(spokenText.length());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Showing google speech input dialog
     * */
    private void startListeningWithDialog() {
        try {
            // Create an intent that can start the Speech Recognizer activity
            Intent recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.speech_prompt));
            try {
                // Start the activity, the intent will be populated with the speech text
                startActivityForResult(recognizerIntent, REQ_CODE_SPEECH_INPUT);
            } catch (ActivityNotFoundException a) {
                Toast.makeText(getApplicationContext(), getString(R.string.speech_not_supported), Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

}
