package in.ticketninja.fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.activity.CouponCodesActivity;
import in.ticketninja.activity.LandingActivity;
import in.ticketninja.adapters.EventCouponCodeAdapter;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.Utility;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.UserCouponCode;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestUserTicket;
import in.ticketninja.ws.response.CallbackUserTicket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CouponCodesFragment extends Fragment implements EventCouponCodeAdapter.CouponCodeItemClickEvent {
    private static final String TAG = CouponCodesFragment.class.getSimpleName();
    private SRKLoaderDialog mLoader;
    private LoginUtils mLogin;
    private CommonClass CC;
    private RecyclerView recyclerView;
    private ArrayList<UserCouponCode> arrayList = new ArrayList<>();
    private boolean type;

    public static CouponCodesFragment newInstance(boolean type) {
        CouponCodesFragment fragment = new CouponCodesFragment();
        Bundle args = new Bundle();
        args.putBoolean("type",type);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.e(TAG, "onCreateView");
        View v = inflater.inflate(R.layout.fragment_coupon_code, container, false);
        try {
            getIntent();
            if(type) {
                setUpToolbar(v);
            }else {
                View view = v.findViewById(R.id.tv_toolbar);
                view.setVisibility(View.GONE);
            }
            initializeData();
            findViewById(v);
            if (CC != null && CC.isOnline()) {
                wsUserTicket();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e(TAG, "onResume");
    }

    private void initializeData() {
        try {
            CC = new CommonClass(getActivity());
            mLogin = new LoginUtils(getActivity());
            mLoader = new SRKLoaderDialog(getActivity());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void findViewById(View v) {
        try {
            recyclerView = v.findViewById(R.id.rv);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getIntent() {
        try {
            Bundle bundle = this.getArguments();
            if (bundle != null) {
                if(bundle.containsKey("type")) {
                    type = bundle.getBoolean("type",false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setUpToolbar(View v) {
        try {
            AppCompatActivity activity;
            if (getActivity() instanceof LandingActivity) {
                activity = (LandingActivity) getActivity();
            } else if (getActivity() instanceof CouponCodesActivity) {
                activity = (CouponCodesActivity) getActivity();
            } else {
                activity = null;
            }
            Toolbar toolbar = v.findViewById(R.id.toolbar);
            if (activity != null) {
                activity.setSupportActionBar(toolbar);
                if (activity.getSupportActionBar() != null) {
                    ActionBar supportActionBar = activity.getSupportActionBar();
                    supportActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                    supportActionBar.setDisplayHomeAsUpEnabled(false);
                    supportActionBar.setDisplayShowHomeEnabled(false);
                    ViewCompat.setElevation(toolbar, 10);
                    if (activity instanceof CouponCodesActivity) {
                        ImageView ivActionBack = v.findViewById(R.id.ivActionBack);
                        ivActionBack.setOnClickListener(view -> getActivity().onBackPressed());
                    } else {
                        v.findViewById(R.id.ivActionBack).setVisibility(View.GONE);
                    }
                    TextView tvTitle = v.findViewById(R.id.tvTitle);
                    tvTitle.setText(R.string.str_myticket);
                    Utility.convertToLowerCase(tvTitle);

                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                        v.findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setAdapter() {
        try {
            EventCouponCodeAdapter codeAdapter = new EventCouponCodeAdapter(getActivity(), arrayList);
            codeAdapter.setClickEvent(this);
            recyclerView.setAdapter(codeAdapter);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void couponCodeItemClickEvent(UserCouponCode userCouponCode, int position) {
        try {
            Log.e("tag", "click");
            Intent intent = new Intent(getActivity(), CouponCodesActivity.class);
            intent.putExtra(Constant.ScreenExtras.C_ID,userCouponCode.getCoupon_code());
            intent.putExtra(Constant.ScreenExtras.M_ID,userCouponCode.getM_id());
            Objects.requireNonNull(getActivity()).startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void wsUserTicket() {
        try {
            if (!mLoader.isShowing() && !Objects.requireNonNull(getActivity()).isFinishing())
                mLoader.show();

            RequestUserTicket request = new RequestUserTicket(mLogin.getUserId());

            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackUserTicket> call = tnAPI.UserTicket(request);
            call.enqueue(new Callback<CallbackUserTicket>() {
                @Override
                public void onResponse(@NonNull Call<CallbackUserTicket> call, @NonNull Response<CallbackUserTicket> response) {
                    try {
                        if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                        if (response.isSuccessful()) {
                            //if (response.body().data().isSuccess()) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                                arrayList = Objects.requireNonNull(response.body()).data().getUser_coupon_code();
                                if (arrayList != null && arrayList.size() > 0) {
                                    setAdapter();
                                }
                            } else if (Validate.isNotNull(Objects.requireNonNull(response.body()).getError_description())) {
                                CC.showToast(Objects.requireNonNull(response.body()).getError_description());
                            } else {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        } else {
                            CC.showToast(R.string.msg_something_wrong);
                        }
                    } catch (Exception e) {
                        // e.printStackTrace();
                        if (getActivity() != null) {
                            if (!getActivity().isFinishing()) {
                                CC.showToast(R.string.msg_something_wrong);
                            }
                        }
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CallbackUserTicket> call, @NonNull Throwable t) {
                    t.printStackTrace();
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    CC.showToast(R.string.msg_something_wrong);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
