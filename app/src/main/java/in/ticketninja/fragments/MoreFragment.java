/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.fragments;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import in.ticketninja.activity.AboutUsActivity;
import in.ticketninja.activity.ContactUsActivity;
import in.ticketninja.activity.CustomerCareActivity;
import in.ticketninja.activity.LandingActivity;
import in.ticketninja.R;
import in.ticketninja.activity.TermsAndConditionActivity;
import in.ticketninja.adapters.MoreMenuAdapter;
import in.ticketninja.common.Constant;
import in.ticketninja.common.DividerItemDecoration;
import in.ticketninja.objects.MoreMenu;


public class MoreFragment extends Fragment {

    public static MoreFragment newInstance() {
        MoreFragment fragment = new MoreFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_more, container, false);

        // ACTIONBAR
        Toolbar toolbar = v.findViewById(R.id.toolbar);
        ((LandingActivity) Objects.requireNonNull(getActivity())).setSupportActionBar(toolbar);
        if (((LandingActivity) getActivity()).getSupportActionBar() != null) {
            ActionBar supportActionBar = ((LandingActivity) getActivity()).getSupportActionBar();
            Objects.requireNonNull(supportActionBar).setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            supportActionBar.setDisplayHomeAsUpEnabled(false);
            supportActionBar.setDisplayShowHomeEnabled(false);
            ViewCompat.setElevation(toolbar, 10);
            v.findViewById(R.id.ivActionBack).setVisibility(View.GONE);
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
                v.findViewById(R.id.toolbarBottomDivider).setVisibility(View.VISIBLE);
        }
        //toolbar.setTitle(R.string.title_fragment_more);
        TextView tvTitle = toolbar.findViewById(R.id.tvTitle);
        tvTitle.setText(R.string.title_fragment_more);

        RecyclerView recyclerView = v.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), 10, 10));

        List<MoreMenu> menuList = new ArrayList<>();
        menuList.add(new MoreMenu(R.drawable.ic_more_customer_care, R.string.more_menu_customer_care));
        menuList.add(new MoreMenu(R.drawable.ic_more_about_us, R.string.more_menu_about_us));
        menuList.add(new MoreMenu(R.drawable.ic_more_contact_us, R.string.more_menu_contact_us));
        menuList.add(new MoreMenu(R.drawable.ic_more_term_condition, R.string.more_menu_terms_conditions));
        menuList.add(new MoreMenu(R.drawable.ic_more_privacy, R.string.more_menu_privacy));

        MoreMenuAdapter mAdapter = new MoreMenuAdapter(menuList);
        mAdapter.setOnItemClickListener((position, menu) -> {

            switch (position) {

                case 0:
                    Intent icustomer = new Intent(getActivity(), CustomerCareActivity.class);
                    getActivity().startActivity(icustomer);
                    break;

                case 1:
                    // CC.showToast("1");
                    Intent i = new Intent(getActivity(), AboutUsActivity.class);
                    getActivity().startActivity(i);

                    break;

                case 2:
                    Intent icontactus = new Intent(getActivity(), ContactUsActivity.class);
                    getActivity().startActivity(icontactus);
                    break;

                case 3:
                    Intent iterms_condition = new Intent(getActivity(), TermsAndConditionActivity.class);
                    getActivity().startActivity(iterms_condition);
                    break;

                case 4:
                    Intent iprivacy = new Intent(getActivity(), AboutUsActivity.class);
                    iprivacy.putExtra(Constant.ScreenExtras.PRIVACY, Constant.ScreenExtras.PRIVACY);
                    getActivity().startActivity(iprivacy);
                    //  CC.showToast("4");
                    break;


            }


            //  CC.showComingSoon(getString(menu.getText()));
        });
        recyclerView.setAdapter(mAdapter);

        return v;
    }

}
