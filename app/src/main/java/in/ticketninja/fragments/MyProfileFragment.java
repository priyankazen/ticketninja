/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.fragments;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
//import com.crashlytics.android.Crashlytics;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.crashlytics.FirebaseCrashlytics;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import in.ticketninja.R;
import in.ticketninja.activity.AboutUsActivity;
import in.ticketninja.activity.BookingHistoryActivity;
import in.ticketninja.activity.ChangePasswordActivity;
import in.ticketninja.activity.ContactUsActivity;
import in.ticketninja.activity.CustomerCareActivity;
import in.ticketninja.activity.EditProfileActivity;
import in.ticketninja.activity.LandingActivity;
import in.ticketninja.activity.LoginActivity;
import in.ticketninja.activity.NotificationListActivity;
import in.ticketninja.activity.RegisterActivity;
import in.ticketninja.activity.TermsAndConditionActivity;
import in.ticketninja.common.CacheHelper;
import in.ticketninja.common.CommonClass;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.TextViewUtils;
import in.ticketninja.common.Validate;
import in.ticketninja.database.DatabaseHelper;
import in.ticketninja.objects.UserInfo;
import in.ticketninja.objects.User_Ticket_Event;
import in.ticketninja.widget.SRKLoaderDialog;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestBasicInfo;
import in.ticketninja.ws.request.RequestLogout;
import in.ticketninja.ws.request.RequestUserTicket;
import in.ticketninja.ws.response.CallbackBasicInfo;
import in.ticketninja.ws.response.CallbackLogout;
import in.ticketninja.ws.response.CallbackUserTicket;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MyProfileFragment extends Fragment {

    //private final String TAG = MyProfileFragment.class.getSimpleName();
    private CommonClass CC;
    private LoginUtils mLogin;
    private SRKLoaderDialog mLoader;

    private CircleImageView ivAccountPic;
    private TextView tvAccountUsername, tvAccountSubHeader;
    private TextView tvAccLogout,tvLogout;
    private TextView tvAccountNotificationsCount;
    //private PreferencesUtils mPref;
    FirebaseCrashlytics crashlytics;

    public static MyProfileFragment newInstance() {
        MyProfileFragment fragment = new MyProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_my_profile, container, false);
        //mPref = new PreferencesUtils(getActivity());
        CC = new CommonClass(getActivity());
        mLogin = new LoginUtils(getActivity());
        mLoader = new SRKLoaderDialog(getActivity());
        crashlytics = FirebaseCrashlytics.getInstance();

        // ACTIONBAR
        AppCompatActivity activity;
        if (getActivity() instanceof LandingActivity) {
            activity = (LandingActivity) getActivity();
        } else {
            activity = null;
        }
        Toolbar toolbar = v.findViewById(R.id.toolbar);
        if (activity != null) {
            activity.setSupportActionBar(toolbar);
            if (activity.getSupportActionBar() != null) {
                ActionBar supportActionBar = activity.getSupportActionBar();
                supportActionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
                supportActionBar.setDisplayHomeAsUpEnabled(false);
                supportActionBar.setDisplayShowHomeEnabled(false);
                ViewCompat.setElevation(toolbar, 10);
            }
        }
        ivAccountPic = v.findViewById(R.id.ivAccountPic);
        tvAccountUsername = v.findViewById(R.id.tvAccountUsername);
        tvAccountSubHeader = v.findViewById(R.id.tvAccountSubHeader);

        TextView tvBookingHistory = v.findViewById(R.id.tvAccountBookingHistory);
        RelativeLayout rlNotifications = v.findViewById(R.id.rlNotifications);
        TextView tvEditProfile = v.findViewById(R.id.tvAccountEditProfile);
        TextView tvChangePassword = v.findViewById(R.id.tvAccountChangePassword);
        tvAccLogout = v.findViewById(R.id.tvAccountLogout);
        tvAccountNotificationsCount = v.findViewById(R.id.tvAccountNotificationsCount);
        TextView tvVersionNumber = v.findViewById(R.id.tvVersionNumber);
        tvVersionNumber.setText(String.format("Ticket Ninja Version A - %s", CC.getAppVersionName()));

        findViewById(v);

        tvBookingHistory.setOnClickListener(v14 -> {
            if (mLogin.isLoggedIn()) {
                Intent i = new Intent(getActivity(), BookingHistoryActivity.class);
                Objects.requireNonNull(getActivity()).startActivity(i);
            } else {
                if (CC.isOnline()) {
                    openLogin("");
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            }
        });

        rlNotifications.setOnClickListener(v14 -> {
            if (mLogin.isLoggedIn()) {
                Intent i = new Intent(getActivity(), NotificationListActivity.class);
                Objects.requireNonNull(getActivity()).startActivityForResult(i, Constant.ActivityForResult.NOTIFICATION);
                //Utility.openBottomSheetMenu(getActivity(), 1,0,"");
            } else {
                if (CC.isOnline()) {
                    openLogin("");
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            }
        });

        tvEditProfile.setOnClickListener(v1 -> {
            if (CC.isOnline()) {
                if (mLogin.isLoggedIn()) {
                    openEditProfile();
                } else {
                    openLogin("");
                }
            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        });

        tvChangePassword.setOnClickListener(v12 -> {
            if (CC.isOnline()) {
                if (mLogin.isLoggedIn()) {
                    Intent i = new Intent(getActivity(), ChangePasswordActivity.class);
                    startActivity(i);
                } else {
                    openLogin("");
                }
            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        });

        tvAccLogout.setOnClickListener(v13 -> {
            if (mLogin.isLoggedIn()) {
                logout();
            } else {
                openLogin("");
            }
        });

        invalidate();

        return v;
    }

    private void findViewById(View v) {

        if (getActivity() == null) {
            return;
        }

        TextView tvProfile = v.findViewById(R.id.tvProfile);
        TextView tvNotifications = v.findViewById(R.id.tvNotification);
        TextView tvChangePassword = v.findViewById(R.id.tvCp);
        tvLogout = v.findViewById(R.id.tvLogout);
        TextView tvCc = v.findViewById(R.id.tvCc);
        TextView tvContactUs = v.findViewById(R.id.tvContactUs);
        TextView tvAboutUs = v.findViewById(R.id.tvAboutUs);
        TextView tvTc = v.findViewById(R.id.tvTc);
        TextView tvPP = v.findViewById(R.id.tvPP);
        TextView tvBh = v.findViewById(R.id.tvBookingHistory);

        tvProfile.setOnClickListener(v17 -> {
            if (CC.isOnline()) {
                if (mLogin.isLoggedIn()) {
                    openEditProfile();
                } else {
                    openLogin("");
                }
            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        });

        tvNotifications.setOnClickListener(v16 -> {
            if (mLogin.isLoggedIn()) {
                Intent i = new Intent(getActivity(), NotificationListActivity.class);
                Objects.requireNonNull(getActivity()).startActivityForResult(i, Constant.ActivityForResult.NOTIFICATION);
                //Utility.openBottomSheetMenu(getActivity(), 1,0,"");
            } else {
                if (CC.isOnline()) {
                    openLogin("");
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            }
        });

        tvChangePassword.setOnClickListener(v15 -> {
            if (CC.isOnline()) {
                if (mLogin.isLoggedIn()) {
                    Intent i = new Intent(getActivity(), ChangePasswordActivity.class);
                    startActivity(i);

                } else {
                    openLogin("");
                }
            } else {
                CC.showToast(R.string.msg_no_internet);
            }
        });

        tvLogout.setOnClickListener(v14 -> {
            if (mLogin.isLoggedIn()) {
                logout();
            } else {
                openLogin("");
            }
        });

        tvCc.setOnClickListener(v13 -> {
            Intent iCustomer = new Intent(getActivity(), CustomerCareActivity.class);
            getActivity().startActivity(iCustomer);
        });

        tvContactUs.setOnClickListener(v12 -> {
            Intent iContactUs = new Intent(getActivity(), ContactUsActivity.class);
            getActivity().startActivity(iContactUs);
        });

        tvAboutUs.setOnClickListener(v1 -> {
            Intent i = new Intent(getActivity(), AboutUsActivity.class);
            getActivity().startActivity(i);
        });

        tvTc.setOnClickListener(v19 -> {
            Intent iTerms_condition = new Intent(getActivity(), TermsAndConditionActivity.class);
            getActivity().startActivity(iTerms_condition);
        });


        tvPP.setOnClickListener(v18 -> {
            Intent iPrivacy = new Intent(getActivity(), AboutUsActivity.class);
            iPrivacy.putExtra(Constant.ScreenExtras.PRIVACY, Constant.ScreenExtras.PRIVACY);
            getActivity().startActivity(iPrivacy);
        });

        tvBh.setOnClickListener(v14 -> {
            if (mLogin.isLoggedIn()) {
                Intent i = new Intent(getActivity(), BookingHistoryActivity.class);
                Objects.requireNonNull(getActivity()).startActivity(i);
            } else {
                if (CC.isOnline()) {
                    openLogin("");
                } else {
                    CC.showToast(R.string.msg_no_internet);
                }
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void invalidate() {
        if (mLogin.isLoggedIn()) {
            if (Validate.isNotNull(mLogin.getName())) {
                tvAccountUsername.setText(String.format("%s", mLogin.getName()));
            } else {
                tvAccountUsername.setText(R.string.account_guest_no_name);
            }
            tvAccountSubHeader.setText(String.format("%s", mLogin.getEmail()));

            if (Validate.isNotNull(mLogin.getPicture())) {
                Glide.with(getActivity())
                        .load(mLogin.getPicture())
                        .skipMemoryCache(false)
                        .listener(new RequestListener<String, GlideDrawable>() {
                            @Override
                            public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                                return false;
                            }

                            @Override
                            public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                                ivAccountPic.setImageDrawable(resource);
                                return false;
                            }
                        })
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .centerCrop()
                        .placeholder(R.drawable.ic_palceholder_edit_profile)
                        .error(R.drawable.ic_palceholder_edit_profile)
                        .into(ivAccountPic);
            } else {
                Glide.with(getActivity())
                        .load(R.drawable.ic_palceholder_edit_profile)
                        .into(ivAccountPic);
            }

            if (mLogin.getNotifications() > 99) {
                tvAccountNotificationsCount.setText("99+");
            } else {
                tvAccountNotificationsCount.setText(String.format(Locale.getDefault(), "%d", mLogin.getNotifications()));
            }
            tvAccountNotificationsCount.setVisibility(mLogin.getNotifications() == 0 ? View.GONE : View.VISIBLE);

        } else {
            Glide.with(getActivity())
                    .load(R.drawable.ic_palceholder_edit_profile)
                    .into(ivAccountPic);
            tvAccountUsername.setText(R.string.account_guest_name);

            SpannableString styledString = new SpannableString(getString(R.string.account_guest_sub_header));
            styledString.setSpan(new TextViewUtils.MyClickableSpan() {
                @Override
                public void onClick(@NonNull View widget) {
                    if (CC.isOnline()) {
                        openLogin("");
                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                }
            }, 0, 7, 0);
            styledString.setSpan(new TextViewUtils.MyClickableSpan() {
                @Override
                public void onClick(@NonNull View widget) {
                    if (CC.isOnline()) {
                        openRegister();
                    } else {
                        CC.showToast(R.string.msg_no_internet);
                    }
                }
            }, 10, 17, 0);
            tvAccountSubHeader.setMovementMethod(LinkMovementMethod.getInstance());
            tvAccountSubHeader.setText(styledString);
            tvAccountNotificationsCount.setVisibility(View.GONE);

        }
        tvAccLogout.setVisibility(mLogin.isLoggedIn() ? View.VISIBLE : View.GONE);
        tvLogout.setVisibility(mLogin.isLoggedIn() ? View.VISIBLE : View.GONE);
    }

    private void openEditProfile() {
        Intent intLogin = new Intent(getActivity(), EditProfileActivity.class);
        startActivityForResult(intLogin, Constant.ActivityForResult.EDIT_PROFILE);
    }

    private void openLogin(String email) {
        Intent intLogin = new Intent(getActivity(), LoginActivity.class);
        intLogin.putExtra(Constant.ScreenExtras.EMAIL, email);
        //intLogin.putExtra(Constant.ScreenExtras.LOGIN_REQUIRED, mPref.getPrefLoginRequired());
        startActivityForResult(intLogin, Constant.ActivityForResult.LOGIN);
    }

    private void openRegister() {
        Intent intRegister = new Intent(getActivity(), RegisterActivity.class);
        intRegister.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_MY_PROFILE);
        startActivityForResult(intRegister, Constant.ActivityForResult.REGISTER);
    }

    private void logout() {
        CC.showAlert(R.string.msg_confirm_logout,
                R.string.msg_confirm_Title,
                android.R.string.yes,
                android.R.string.no,
                () -> {
                    String deviceToken = PreferencesUtils.getFCMPushKey(getActivity());
                    wsLogout(mLogin.getUserId(), deviceToken);

                    mLogin.logout();
                    invalidate();
                });
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constant.ActivityForResult.LOGIN &&
                resultCode == Activity.RESULT_OK) {
            int intExtra = data.getIntExtra(Constant.ScreenExtras.DATA, 0);
            if (intExtra == Constant.ScreenExtras.PRESS_SIGNUP) {
                openRegister();
            } else {
                mLogin = new LoginUtils(getActivity());
                syncBookingHistory();
                invalidate();
            }
        } else if (requestCode == Constant.ActivityForResult.REGISTER
                && resultCode == Activity.RESULT_OK) {
            int intExtra = data.getIntExtra(Constant.ScreenExtras.DATA, 0);
            if (intExtra == Constant.ScreenExtras.PRESS_LOGIN) {
                String mailExtra = "";
                if (data.hasExtra(Constant.ScreenExtras.EMAIL))
                    mailExtra = data.getStringExtra(Constant.ScreenExtras.EMAIL);
                openLogin(mailExtra);
            } else {
                mLogin = new LoginUtils(getActivity());
                invalidate();
            }
        } else if (requestCode == Constant.ActivityForResult.EDIT_PROFILE && resultCode == Activity.RESULT_OK) {
            wsBasicInfo(mLogin.getUserId());

        } else if (requestCode == Constant.ActivityForResult.NOTIFICATION) {
            if (mLogin.getNotifications() > 99) {
                tvAccountNotificationsCount.setText("99+");
            } else {
                tvAccountNotificationsCount.setText(String.format(Locale.getDefault(), "%d", mLogin.getNotifications()));
            }
        }
    }

    // WS - BASIC INFO
    private void wsBasicInfo(String userId) {
        if (!mLoader.isShowing() && !Objects.requireNonNull(getActivity()).isFinishing())
            mLoader.show();
        RequestBasicInfo request = new RequestBasicInfo(String.valueOf(userId));

        TicketNinjaAPI tnAPI = RestApi.createAPI();
        Call<CallbackBasicInfo> call = tnAPI.basicInfo(request);
        call.enqueue(new Callback<CallbackBasicInfo>() {
            @Override
            public void onResponse(@NonNull Call<CallbackBasicInfo> call, @NonNull Response<CallbackBasicInfo> response) {
                Log.e("tag", "getActivity = " + getActivity());
                try {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();

                    if (getActivity() != null) {

                        try {
                            if (response.isSuccessful()) {
                                if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {

                                    // REMOVE CACHED DASHBOARD DATA
                                    CacheHelper mCacheHelper = new CacheHelper(getActivity());
                                    mCacheHelper.remove(CacheHelper.KEY_DASHBOARD);

                                    UserInfo userInfo = Objects.requireNonNull(response.body()).data();
                                    mLogin.setUserId(userInfo.user_id);
                                    mLogin.setFirstName(userInfo.firstname);
                                    mLogin.setLastName(userInfo.lastname);
                                    mLogin.setEmail(userInfo.email);
                                    mLogin.setCountryCode(userInfo.country_code);
                                    mLogin.setMobile(userInfo.mobileno);
                                    mLogin.setGender(userInfo.gender);
                                    mLogin.setPicture(userInfo.user_imageid_url);
                                    mLogin.setUserDOBWithParse(userInfo.birthdate);
                                    mLogin.setNotifications(userInfo.notification_unread_count);

                                    invalidate();


                                    try {
                                        // FirebaseAnalytics
                                        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(getActivity());
                                        mFirebaseAnalytics.setUserProperty("tn_user_id", mLogin.getUserId() + "");
                                        mFirebaseAnalytics.setUserProperty("tn_user_name", mLogin.getName() + "");
                                        mFirebaseAnalytics.setUserId(mLogin.getUserId() + "");
                                        // Crashlytics
                                        crashlytics.setUserId("" + mLogin.getUserId());
                                        //Crashlytics.setUserIdentifier("" + mLogin.getUserId());
                                        //Crashlytics.setUserEmail("" + mLogin.getEmail());
                                        //Crashlytics.setUserName("" + mLogin.getName());
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(@NonNull Call<CallbackBasicInfo> call, @NonNull Throwable t) {
                if (getActivity() != null) {
                    if (mLoader != null && mLoader.isShowing()) mLoader.dismiss();
                    t.printStackTrace();
                }
            }
        });
    }

    // WS - LOGOUT
    private void wsLogout(String userId, String deviceToken) {
        RequestLogout request = new RequestLogout(String.valueOf(userId), deviceToken);


        TicketNinjaAPI tnAPI = RestApi.createAPI();
        Call<CallbackLogout> call = tnAPI.logout(request);
        call.enqueue(new Callback<CallbackLogout>() {
            @Override
            public void onResponse(@NonNull Call<CallbackLogout> call, @NonNull Response<CallbackLogout> response) {
            }

            @Override
            public void onFailure(@NonNull Call<CallbackLogout> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void syncBookingHistory() {
        if (CC.isOnline() && mLogin.isLoggedIn()) {
            if (ContextCompat.checkSelfPermission(Objects.requireNonNull(getActivity()),
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE, android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE_FOR_BGDWNLD);
            } else {
                wsGetBookingHistory(getActivity(), mLogin.getUserId());
            }
        }
    }

    //..............................WS CALL FOR GET BOOKING HISTORY..........//
    private void wsGetBookingHistory(Context context, String userId) {
        RequestUserTicket request = new RequestUserTicket(mLogin.getUserId());

        TicketNinjaAPI tnAPI = RestApi.createAPI();
        Call<CallbackUserTicket> call = tnAPI.UserTicket(request);
        call.enqueue(new Callback<CallbackUserTicket>() {
            @Override
            public void onResponse(@NotNull Call<CallbackUserTicket> call, @NonNull Response<CallbackUserTicket> response) {
                try {
                    if (response.isSuccessful()) {
                        if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                            DatabaseHelper DB = new DatabaseHelper(context);

                            try {
                                if (DB.doesDatabaseExist(Objects.requireNonNull(getActivity()))) {
                                    DB.clearMyTicketTable(mLogin.getUserId());
                                    DB.clearMyTicketListTable(mLogin.getUserId());
                                }
                            }catch (Exception e){
                                e.printStackTrace();
                            }

                            ArrayList<User_Ticket_Event> ticketList = Objects.requireNonNull(response.body()).data().userTicketEvents();
                            if (ticketList != null && ticketList.size() > 0) {

                                for (int i = 0; i < ticketList.size(); i++) {
                                    DB.addMyTickets(ticketList.get(i), userId);
                                }

                                DB.startDownloadingMyTicketsInBackground();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CallbackUserTicket> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }

    /*//..............................WS CALL FOR GET BOOKING HISTORY..........//
    private void wsGetBookingHistory(Context context, long userId) {
        RequestBookingHistory request = new RequestBookingHistory();
        request.user_id = userId;
        request.h_type = Constant.HistoryType.FUTURE;

        TicketNinjaAPI tnAPI = RestApi.createAPI();
        Call<CallbackBookingHistory> call = tnAPI.GetBookingHistory(request);
        call.enqueue(new Callback<CallbackBookingHistory>() {
            @Override
            public void onResponse(Call<CallbackBookingHistory> call, @NonNull Response<CallbackBookingHistory> response) {
                try {
                    if (response.isSuccessful()) {
                        if (response.body().data().isSuccess()) {
                            DatabaseHelper DB = new DatabaseHelper(context);
                            DB.clearBookingHistoryTable(userId);
                            DB.clearBookingTicketListTable(userId);

                            ArrayList<BookingHistory> bookingHistoryList = response.body().data().booking_history;
                            if (bookingHistoryList != null && bookingHistoryList.size() > 0) {
                                for (int i = 0; i < bookingHistoryList.size(); i++) {
                                    DB.addBookingHistory(bookingHistoryList.get(i), userId);
                                }
                                DB.startDownloadingBookingHistoryInBackground();
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<CallbackBookingHistory> call, @NonNull Throwable t) {
                t.printStackTrace();
            }
        });
    }*/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == Constant.RequestPermissions.WRITE_EXTERNAL_STORAGE_FOR_BGDWNLD) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                syncBookingHistory();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mLogin != null && mLogin.isLoggedIn()) {
            wsBasicInfo(mLogin.getUserId());
        }
    }


    @Override
    public void onStart() {
        super.onStart();
    }


   /* @SuppressLint("SetTextI18n")
    public void UpdateCount() {
        if (mLogin.isLoggedIn()) {
            if (mLogin.getNotifications() > 99) {
                tvAccountNotificationsCount.setText("99+");
            } else {
                tvAccountNotificationsCount.setText(String.format(Locale.getDefault(), "%d", mLogin.getNotifications()));
            }
            tvAccountNotificationsCount.setVisibility(View.VISIBLE);
        } else {
            tvAccountNotificationsCount.setVisibility(View.GONE);

        }

    }*/


}
