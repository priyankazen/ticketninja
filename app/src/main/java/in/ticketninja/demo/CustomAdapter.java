package in.ticketninja.demo;

import android.annotation.SuppressLint;
import android.content.Context;
import com.google.android.material.textfield.TextInputEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Objects;

import in.ticketninja.R;


/**
 * Created by Gourav on 08-03-2016.
 */
public class CustomAdapter extends BaseExpandableListAdapter {
    private static final String TAG = CustomAdapter.class.getSimpleName();

    private Context context;
    private ArrayList<GroupInfo> deptList;

    CustomAdapter(Context context, ArrayList<GroupInfo> deptList) {
        this.context = context;
        this.deptList = deptList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<ChildInfo> productList = deptList.get(groupPosition).getProductList();
        return productList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View view, ViewGroup parent) {

        ChildInfo detailInfo = (ChildInfo) getChild(groupPosition, childPosition);
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_ex_child_view, parent, false);
        }

        ExpandableListView.LayoutParams lp = new ExpandableListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);

       /* TextView sequence = (TextView) view.findViewById(R.id.sequence);
        sequence.setText(detailInfo.getSequence().trim() + ". ");

        TextView childItem = view.findViewById(R.id.childItem);
        childItem.setText(detailInfo.getEvent_name().trim());*/

        LinearLayout ll = view.findViewById(R.id.linearlayout);
        ll.removeAllViews();
        ll.addView(addEdt(parent,detailInfo, childPosition, groupPosition));
        return view;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        ArrayList<ChildInfo> productList = deptList.get(groupPosition).getProductList();
        return productList.size();

    }

    @Override
    public Object getGroup(int groupPosition) {
        return deptList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return deptList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {

        GroupInfo headerInfo = (GroupInfo) getGroup(groupPosition);
        if (view == null) {
            LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = Objects.requireNonNull(inf).inflate(R.layout.list_group, parent,false);
        }

        TextView heading =  view.findViewById(R.id.heading);
        heading.setText(headerInfo.getName().trim());

        return view;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    //
    private View addEdt(ViewGroup parent, ChildInfo details, int childPosition, int groupPosition) {
        LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View v = layoutInflater != null ? layoutInflater.inflate(R.layout.edittext_view, parent,false) : null;

        if (v != null) {

            //TextInputLayout textInputLayout = v.findViewById(R.id.inputL);
            //textInputLayout.setHint("name");

            TextInputEditText edt = v.findViewById(R.id.edt);
            edt.setText(details.getName());


            edt.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    Log.e(TAG, "i= " + i + " , i1: " + i1);
                    Log.e(TAG, "click_groupPosition= " + groupPosition + " , childPosition: " + childPosition);
                    deptList.get(groupPosition).getProductList().get(childPosition).setName(charSequence.toString());
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }

        return v;
    }
}