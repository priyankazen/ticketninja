package in.ticketninja.demo;

import android.annotation.SuppressLint;
import android.content.Context;
import com.google.android.material.textfield.TextInputEditText;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;
import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.common.UserIdentificationCode;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.UserIdentificationDetails;
import in.ticketninja.objects.UsrIdentificationData;
import in.ticketninja.widget.MyDatePickerDialog;


/**
 * Created by Gourav on 08-03-2016.
 */
public class CustomAdapter1 extends BaseExpandableListAdapter {
    private static final String TAG = CustomAdapter1.class.getSimpleName();

    private Context context;
    private ArrayList<UsrIdentificationData> deptList;

    CustomAdapter1(Context context, ArrayList<UsrIdentificationData> deptList) {
        this.context = context;
        this.deptList = deptList;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        ArrayList<UserIdentificationDetails> productList = deptList.get(groupPosition).getUserInfo();
        return productList.get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View view, ViewGroup parent) {

        UserIdentificationDetails child = (UserIdentificationDetails) getChild(groupPosition, childPosition);
        if (view == null) {
            view = LayoutInflater.from(context).inflate(R.layout.item_ex_child_view, parent, false);
        }

        LinearLayout linearLayout = view.findViewById(R.id.linearlayout);
        linearLayout.removeAllViews();
        if (UserIdentificationCode.GENDER_CODE == child.getProof_id()) {
            linearLayout.addView(addRadioButtons(child, groupPosition, childPosition));
        } else if (UserIdentificationCode.CUSTOMER_PHOTO_CODE == child.getProof_id()) {
            linearLayout.addView(addPhoto());
        } else if (UserIdentificationCode.ID_CARD_CODE == child.getProof_id()) {
            linearLayout.addView(addSpinner(groupPosition, childPosition));
        } else if (UserIdentificationCode.BIRTH_DATE_CODE == child.getProof_id()) {
            linearLayout.addView(getText(child, groupPosition, childPosition));
            linearLayout.addView(addLine());
        } else {
            linearLayout.addView(addEdt(child, childPosition, groupPosition));
        }
        return view;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        ArrayList<UserIdentificationDetails> productList = deptList.get(groupPosition).getUserInfo();
        return productList.size();

    }

    @Override
    public Object getGroup(int groupPosition) {
        return deptList.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return deptList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {

        UsrIdentificationData headerInfo = (UsrIdentificationData) getGroup(groupPosition);
        if (view == null) {
            LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = Objects.requireNonNull(inf).inflate(R.layout.item_ex_group_view, parent,false);
        }

        TextView heading =  view.findViewById(R.id.listTitle);
        heading.setText(headerInfo.getGroup_title().trim());

        return view;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }


    private Spinner addSpinner(int groupPosition, int childPosition) {
        Spinner spinner = new Spinner(context);
        ArrayList<String> spinArrayList = new ArrayList<>();
        spinArrayList.add("Aadhar Card");
        spinArrayList.add("Driving License");
        spinArrayList.add("Passport");
        spinArrayList.add("Student ID Card");
        spinArrayList.add("Voter Card");
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, 0, spinArrayList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setPadding(10, 10, 10, 10);
        deptList.get(groupPosition).getUserInfo().get(childPosition).setText(spinArrayList.get(0));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                deptList.get(groupPosition).getUserInfo().get(childPosition).setText(adapterView.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        //spinner.setBackground(context.getResources().getDrawable(R.drawable.red_border_line));
        return spinner;
    }

    private View addPhoto() {
        LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View v = layoutInflater != null ? layoutInflater.inflate(R.layout.item_ex_child_customer_photo, null) : null;
        if (v != null) {
            TextView txt = v.findViewById(R.id.txtPhotoUpload);
            txt.setOnClickListener(view -> {
                Log.e(TAG, "id = " + v.getId());
                Log.e(TAG, "id = " + v.getId());
            });
            //ImageView imageView = v.findViewById(R.id.imgCustomerPhoto);
        }


        return v;
    }

    private View addEdt(UserIdentificationDetails details, int childPosition, int groupPosition) {
        LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View v = layoutInflater != null ? layoutInflater.inflate(R.layout.edittext_view, null) : null;

        if (v != null) {

           // TextInputLayout textInputLayout = v.findViewById(R.id.inputL);
            //textInputLayout.setHint(details.getProof_name());

            TextInputEditText edt = v.findViewById(R.id.edt);
            edt.setText(details.getText());

            if (UserIdentificationCode.MOBILE_NO_CODE == details.getProof_id()) {
                edt.setInputType(InputType.TYPE_CLASS_PHONE);
                edt.setFilters(new InputFilter[]{new InputFilter.LengthFilter(10)});
            } else if (UserIdentificationCode.EMAIL_CODE == details.getProof_id()) {
                edt.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
            }

            edt.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    Log.e(TAG, "i= " + i + " , i1: " + i1);
                    Log.e(TAG, "click_groupPosition= " + groupPosition + " , childPosition: " + childPosition);
                    Log.e(TAG, " aaaaa= " + deptList.get(groupPosition).getUserInfo().get(childPosition));
                    deptList.get(groupPosition).getUserInfo().get(childPosition).setText(charSequence.toString());
                    //arrayList.get(groupPosition).setGroup_title("1");
                    Log.e(TAG, "arrayList = " + deptList);
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }

        return v;
    }

    private RadioGroup addRadioButtons(UserIdentificationDetails child, int groupPosition, int childPosition) {

        //for (int row = 0; row < 1; row++) {
        RadioGroup rg = new RadioGroup(context);
        rg.setOrientation(LinearLayout.HORIZONTAL);
        for (int i = 0; i < 3; i++) {
            RadioButton rdBtn = new RadioButton(context);
            rdBtn.setId(i);

            if (i == 0) {
                rdBtn.setText(R.string.male);
                if (Validate.isNull(child.getText())){
                    rdBtn.setChecked(true);
                    deptList.get(groupPosition).getUserInfo().get(childPosition).setText(context.getResources().getString(R.string.male));
                }
            } else if (i == 1) {
                rdBtn.setText(R.string.female);
            } else {
                rdBtn.setText(R.string.other);
            }

            if (rdBtn.getText().toString().equals(child.getText())) {
                // Log.e(TAG, "child.getText(): " + child.getText());
                rdBtn.setChecked(true);
            }

            rg.addView(rdBtn);

            rg.setOnCheckedChangeListener((radioGroup, i1) -> {
                // get selected radio button from radioGroup
                int selectedId = radioGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                RadioButton radioButton = rg.findViewById(selectedId);

                if (radioButton!=null) {
                    // Log.e(TAG, "radioText: " + radioButton.getText());
                    deptList.get(groupPosition).getUserInfo().get(childPosition).setText(radioButton.getText().toString());
                }
            });
        }


        return rg;
        // }
        // return null;
    }


    private TextView getText(UserIdentificationDetails details, int groupPosition, int childPosition) {
        TextView textView = new TextView(context);

        textView.setTextSize(16);
        textView.setTextColor(context.getResources().getColor(R.color.themeGrayDark));
        textView.setPadding(5, 30, 5, 5);
        if (Validate.isNotNull(details.getText())) {
            textView.setText(details.getText());
        } else {
            textView.setText(details.getProof_name());
        }

        MyDatePickerDialog DatePickerDialog = new MyDatePickerDialog(context, R.style.datepicker,
                (view, year, monthOfYear, dayOfMonth) -> {
                    Calendar newDate = Calendar.getInstance();
                    newDate.set(year, monthOfYear, dayOfMonth);
                    String selectedDate = String.format(Locale.getDefault(), "%d-%02d-%02d", year, monthOfYear + 1, dayOfMonth);
                    Log.e(TAG, "selectedDate: " + selectedDate);
                    textView.setText(selectedDate);
                    deptList.get(groupPosition).getUserInfo().get(childPosition).setText(selectedDate);
                }, Calendar.getInstance().get(Calendar.YEAR), Calendar.getInstance().get(Calendar.MONTH), Calendar.getInstance().get(Calendar.DAY_OF_MONTH));

        DatePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());

        textView.setOnClickListener(view -> {
            Log.e(TAG, "name: " + details.getProof_name());

            DatePickerDialog.show();
        });
        return textView;
    }

    private View addLine() {
        View v = new View(context);
        LinearLayout.LayoutParams linLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 3);
        v.setLayoutParams(linLayoutParams);
        v.setBackgroundColor(context.getResources().getColor(R.color.themeGrayDark));
        return v;
    }

}