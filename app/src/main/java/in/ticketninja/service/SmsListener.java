/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsMessage;

import java.util.Objects;

/**
 * MB_WORKING(com.nichetech.service) <br />
 * Developed by <b><a href="http://RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 25/5/16.
 *
 * @author Suthar Rohit
 */
public class SmsListener extends BroadcastReceiver {

    //private static final String TAG = "SmsListener";
    //private SharedPreferences preferences;

    @Override
    public void onReceive(Context context, Intent intent) {

        try {
            if (Objects.requireNonNull(intent.getAction()).equals(Telephony.Sms.Intents.SMS_RECEIVED_ACTION)) {
                Bundle bundle = intent.getExtras();
                //Log.e(TAG, "bundle=" + bundle.toString());
                SmsMessage[] msgs;
                //---retrieve the SMS message received---
                try {
                    Object[] pdus = (Object[]) Objects.requireNonNull(bundle).get("pdus");
                    msgs = new SmsMessage[Objects.requireNonNull(pdus).length];
                    for (int i = 0; i < msgs.length; i++) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            String format = bundle.getString("format");
                            //Log.e(TAG, "sms format=" + format);
                            msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i], format);
                        } else {
                            msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                        }
                        //Log.e(TAG, "sms[" + i + "] Add=" + msgs[i].getOriginatingAddress());
                        //Log.e(TAG, "sms[" + i + "] SMS=" + msgs[i].getMessageBody());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
//                            Log.d("Exception caught",e.getMessage());
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}