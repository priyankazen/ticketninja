/*
 * Copyright (c) 2017. Aditi PArikh
 * Developed by Aditi Parikh for NicheTech Computer Solutions Pvt. Ltd. use only.
 */

package in.ticketninja.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import in.ticketninja.common.Constant;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.File;

/**
 * Created by aditi on 8/21/17.
 */

public class DownloadingService extends IntentService {

    private boolean mIsAlreadyRunning;

    private ExecutorService mExec;
    private CompletionService<NoResultType> mEcs;
    private List<DownloadTask> mTasks;

    public DownloadingService() {
        super("DownloadingService");
        mExec = Executors.newFixedThreadPool( /* only 5 at a time */5);
        mEcs = new ExecutorCompletionService<NoResultType>(mExec);
        mTasks = new ArrayList<>();
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        try {
            if (mIsAlreadyRunning) {
                return;
            }
            mIsAlreadyRunning = true;

            ArrayList<File> files = intent.getParcelableArrayListExtra("files");
            final Collection<DownloadTask> tasks = mTasks;
            int index = 0;
            for (File file : Objects.requireNonNull(files)) {
                DownloadTask yt1 = new DownloadTask(index++, file);
                tasks.add(yt1);
            }

            for (DownloadTask t : tasks) {
                mEcs.submit(t);
            }
            // wait for finish
            int n = tasks.size();
            for (int i = 0; i < n; ++i) {
                NoResultType r;
                try {
                    r = mEcs.take().get();
                    // use you result here
                } catch (InterruptedException | ExecutionException e) {
                    e.printStackTrace();
                }
            }
            // send a last broadcast
            mExec.shutdown();
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    class DownloadTask implements Callable<NoResultType> {
        private int mPosition;
        private int mProgress;
        private boolean mCancelled;
        private final File mFile;
        private Random mRand = new Random();

        DownloadTask(int position, File file) {
            mPosition = position;
            mFile = file;
        }

        @Override
        public NoResultType call() throws Exception {

            String urlToDownload = mFile.getUrl();

            try {
              // Log.e("DownloadingService" ," urlToDownload:"+urlToDownload);
                URL url = new URL(urlToDownload);
                URLConnection connection = url.openConnection();
                connection.connect();
                // this will be useful so that you can show a typical 0-100% progress bar
                int fileLength = connection.getContentLength();
                java.io.File file = new java.io.File(Constant.IMAGE_DOWNLOAD_PATH);
                if (!file.exists()) {
                    file.mkdir();
                }
                // download the file
                InputStream input = new BufferedInputStream(connection.getInputStream());

                if (Validate.isNotNull(mFile.getName())) {
                    OutputStream output = new FileOutputStream(Constant.IMAGE_DOWNLOAD_PATH + mFile.getName());
                    byte[] data = new byte[1024];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        total += count;
                        // publishing the progress....
                        Bundle resultData = new Bundle();
                        resultData.putInt("progress", (int) (total * 100 / fileLength));
                        mProgress = (int) (total * 100 / fileLength);
//                        receiver.send(UPDATE_PROGRESS, resultData);
                        output.write(data, 0, count);
                    }

                    output.flush();
                    output.close();
                    input.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return new NoResultType();
        }

        public int getProgress() {
            return mProgress;
        }

        public int getPosition() {
            return mPosition;
        }

        public void cancel() {
            mCancelled = true;
        }
    }

    class NoResultType {
    }

}
