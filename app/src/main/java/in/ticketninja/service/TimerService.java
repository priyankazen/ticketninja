package in.ticketninja.service;

import android.app.ActivityManager;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.RequiresApi;

import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import in.ticketninja.R;
import in.ticketninja.activity.EventDetailActivity;
import in.ticketninja.activity.OrderSummaryActivity;
import in.ticketninja.activity.PurchaseTicketActivity;
import in.ticketninja.common.Constant;
import in.ticketninja.objects.MyEvent;

public class TimerService extends Service {

    private final String TAG = TimerService.class.getSimpleName();

    public static final String BROADCAST_ACTION = "in.ticketninja.service.timerevent";

    private Intent intent;

    private MyEvent myEvent;
    private CountDownTimer cdt = null;

    @Override
    public void onCreate() {
        try {
            Log.e(TAG, "onCreate");
            super.onCreate();
            intent = new Intent(BROADCAST_ACTION);
            long totalTimer = TimeUnit.MINUTES.toMillis(Constant.PAYMENT_SESSION_TIMEOUT);
            cdt = new CountDownTimer(totalTimer, TimeUnit.SECONDS.toMillis(1)) {
                @Override
                public void onTick(long millisUntilFinished) {
                    intent.putExtra("countdown", millisUntilFinished);
                    float percentage = (((int) millisUntilFinished / 1000 * 100) / (Constant.PAYMENT_SESSION_TIMEOUT * 60));
                    if (percentage > 0 && percentage < 1) percentage = 1;
                    intent.putExtra("percentage", (int) percentage);
                    intent.putExtra("in_progress", true);
                    intent.putExtra("string_to_show", String.format(Locale.getDefault(), Constant.TIMER_FORMAT,
                            TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) - TimeUnit.HOURS.toMinutes(
                                    TimeUnit.MILLISECONDS.toHours(millisUntilFinished)),
                            TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(
                                    TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

                    sendBroadcast(intent);
                }

                @Override
                public void onFinish() {
                    Log.e(TAG,"onFinish : TIMER ");
                    Log.e(TAG, "getCurrentClass()==" + getCurrentClass());
                    if (getCurrentClass().equalsIgnoreCase(OrderSummaryActivity.class.getName())
                            || getCurrentClass().equalsIgnoreCase(PurchaseTicketActivity.class.getName())) {
                        intent.putExtra("countdown", 0L);
                        intent.putExtra("percentage", 0);
                        intent.putExtra("in_progress", false);
                        intent.putExtra("string_to_show", getString(R.string.label_done));

                        sendBroadcast(intent);
                /*} else if (getCurrentClass().contains("com.razorpay.")) {
                    Log.e(TAG, "Contain  com.razorpay.");
                    MessageUtils.showAlert((Activity) getApplicationContext(), R.string.purchase_ticket_msg_session_timeout, () -> {

                        Intent i1 = new Intent(getApplication(), EventDetailActivity.class);
                        i1.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                        i1.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_PAYMENT);
                        i1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i1);

                    });*/

                    } else {
                        Intent i = new Intent(getApplication(), EventDetailActivity.class);
                        i.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                        i.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_SERVICE);
                        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                    }

                    stopSelf();
                }
            };
            cdt.start();
        }catch (Exception e){
            e.printStackTrace();
        }

    }


    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        cdt.cancel();
        super.onDestroy();
    }

    @Override
    public void onRebind(Intent intent) {
        Log.e(TAG, "onBind");
        super.onRebind(intent);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        if (intent != null) {
            if (intent.hasExtra(Constant.ScreenExtras.EVENT_DATA)) {
                myEvent = (MyEvent) intent.getSerializableExtra(Constant.ScreenExtras.EVENT_DATA);
            }
        }
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public IBinder onBind(Intent arg0) {
        Log.e(TAG, "onBind");
        return null;
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    public String getCurrentClass() {
        try {
            ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
            List<ActivityManager.RunningTaskInfo> runningTaskInfo = null;
            if (manager != null) {
                runningTaskInfo = manager.getRunningTasks(1);
            }

            ComponentName componentInfo = Objects.requireNonNull(runningTaskInfo).get(0).topActivity;
            return componentInfo.getClassName();
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    //public int counter = 0;
    //private Timer timer;
    //private TimerTask timerTask;
   // long oldTime = 0;

    /*public void startTimer() {
        //set a new Timer
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 1000, 1000); //
    }*/

    /*
     * it sets the timer to print the counter every x seconds
     */
  /*  public void initializeTimerTask() {
        timerTask = new TimerTask() {
            public void run() {
                Log.i("in timer", "in timer ++++  " + (counter++));
            }
        };
    }*/

    /*
     * not needed
     */
    /*public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }*/

}