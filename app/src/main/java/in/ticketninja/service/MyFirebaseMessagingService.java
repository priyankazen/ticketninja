/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.service;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import in.ticketninja.activity.SplashActivity;
import in.ticketninja.common.Constant;
import in.ticketninja.common.LoginUtils;
import in.ticketninja.common.NotificationUtils;
import in.ticketninja.common.PreferencesUtils;
import in.ticketninja.common.Validate;
import in.ticketninja.objects.MyEvent;
import in.ticketninja.ws.RestApi;
import in.ticketninja.ws.TicketNinjaAPI;
import in.ticketninja.ws.request.RequestNotificationRead;
import in.ticketninja.ws.response.CallbackNotificationRead;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Matrubharti(com.nichetech.service) <br />
 * Developed by <b><a href="http://RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 24/8/16.
 *
 * @author Suthar Rohit
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFireBaseMsgService";

    @SuppressWarnings("deprecation")
    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e(TAG, "Refreshed token: " + s + " \n refreshedToken:"+refreshedToken);
        sendRegistrationToServer(s);
    }

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ

        // Check if message contains a data payload.
        try {
            if (remoteMessage.getData().size() > 0) {
                //boolean notification = PreferencesUtils.getPrfEnabledNotification(this);
                //if (notification) sendNotification(remoteMessage.getData().get("message"));

                // message:{"aps":{"data":{"N_Type":3,"Reference_Id":0,"message":"Be alert ! 12 Angry Jurors at Pandit Dindayal Upadhyay Auditorium - Ahmedabad","Event_Id":1792},"alert":"Be alert ! 12 Angry Jurors at Pandit Dindayal Upadhyay Auditorium - Ahmedabad"}}
                //2019-07-23 10:54:29 remoteMessage.getData()= {message={"aps":{"data":{"N_Type":3,"Reference_Id":0,"message":"Be alert ! 12 Angry Jurors at Pandit Dindayal Upadhyay Auditorium - Ahmedabad","Event_Id":1792},"alert":"Be alert ! 12 Angry Jurors at Pandit Dindayal Upadhyay Auditorium - Ahmedabad"}}}

                Log.e(TAG, "remoteMessage.getData():  " + remoteMessage.getData());

                try {
                    Log.e(TAG, "remoteMessage.getCollapseKey(): " + remoteMessage.getCollapseKey());
                    Log.e(TAG, "remoteMessage.getFrom(): " + remoteMessage.getFrom());
                    Log.e(TAG, "remoteMessage.getMessageType(): " + remoteMessage.getMessageType());
                    Log.e(TAG, "remoteMessage.getMessageId(): " + remoteMessage.getMessageId());
                    Log.e(TAG, "remoteMessage.getMessage: " + remoteMessage.getData().get("message"));
                /*if (Validate.isNotNull(remoteMessage.getFrom()) && remoteMessage.getFrom().contains("Event_Detail")) {
                    String eventId = remoteMessage.getData().get("event_id");
                    String eventName = remoteMessage.getData().get("Event_Name");
                    String eventImage = remoteMessage.getData().get("Event_Image");
                    String msg = remoteMessage.getNotification().getBody();

                    long EventId = 0;
                    try {
                        EventId = Long.parseLong(eventId);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (EventId > 0 && Validate.isNotNull(msg)) {
                        MyEvent myEvent = new MyEvent();
                        myEvent.event_id = EventId;
                        Intent noteIntent = new Intent(this, EventDetailActivity.class);
                        noteIntent.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                        noteIntent.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_NOTIFICATION);
                        noteIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        PendingIntent intent = PendingIntent.getActivity(this, 0, noteIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                        // NotificationUtils.showNotification(this, msg, intent);
                        NotificationUtils.showImageNotification(this, msg, 0, eventImage, intent, "", 0);

                    }

                } else {*/
                    JSONObject msgObject = new JSONObject(Objects.requireNonNull(remoteMessage.getData().get("message")));
                    Log.e("Firebase_Message", msgObject.getString("aps"));
                    JSONObject apsObject = new JSONObject(msgObject.getString("aps"));
                    String msgData = apsObject.optString("data");
                    Log.e("Firebase_Message_data", apsObject.optString("data"));
                    String message = apsObject.getString("alert");
                    Log.e("Firebase_Message1", apsObject.optString("alert"));
                    if (Validate.isNotNull(msgData)) {
                        JSONObject data = new JSONObject(apsObject.getString("data"));

                        LoginUtils mLogin = new LoginUtils(this);
                        String LoginMasterID = data.optString("LoginMasterID", "");
                        int nType = data.optInt("N_Type", 0);
                        Log.e("Firebase_nType", ""+nType);
                        if (nType == Constant.NotificationType.BULK_EVENT_DETAIL) {
                            long EventId = data.optLong("Event_Id", 0);
                            MyEvent myEvent = new MyEvent();
                            myEvent.event_id = EventId;
                            Intent noteIntent = new Intent(this, SplashActivity.class);
                            noteIntent.putExtra(Constant.ScreenExtras.MESSAGE, remoteMessage.getData().get("message"));
                            noteIntent.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                            noteIntent.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_NOTIFICATION);
                            noteIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            PendingIntent intent = PendingIntent.getActivity(this, 0, noteIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                            NotificationUtils.showNotification(this, message, intent);

                        } else if (mLogin.isLoggedIn() && LoginMasterID.equals(mLogin.getUserId())) {
                            long EventId = data.optLong("Event_Id", 0);
                          //  long ReferenceId = data.optLong("Reference_Id", 0);
                            long NotificationID = data.optLong("NotificationID", 0);

                            if (NotificationID > 0) {
                                wsNotificationRead(this, mLogin.getUserId(), NotificationID);
                            }

                            if (nType == Constant.NotificationType.EVENT) {
                                MyEvent myEvent = new MyEvent();
                                myEvent.event_id = EventId;
                                Intent noteIntent = new Intent(this, SplashActivity.class);
                                noteIntent.putExtra(Constant.ScreenExtras.MESSAGE, remoteMessage.getData().get("message"));
                                noteIntent.putExtra(Constant.ScreenExtras.EVENT_DATA, myEvent);
                                noteIntent.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_NOTIFICATION);
                                noteIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                PendingIntent intent = PendingIntent.getActivity(this, 0, noteIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                                NotificationUtils.showNotification(this, message, intent);

                            } else if (nType == Constant.NotificationType.TICKET) {
                                Intent noteIntent = new Intent(this, SplashActivity.class);
                                noteIntent.putExtra(Constant.ScreenExtras.MESSAGE, remoteMessage.getData().get("message"));
                                noteIntent.putExtra(Constant.ScreenExtras.FROM_SCREEN, Constant.ScreenExtras.FROM_ACTIVITY_NOTIFICATION);
                                noteIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                PendingIntent intent = PendingIntent.getActivity(this, 0, noteIntent, PendingIntent.FLAG_UPDATE_CURRENT);

                                NotificationUtils.showNotification(this, message, intent);

                            } else if (Validate.isNotNull(message)) {
                                NotificationUtils.showNotification(this, message);
                            }
                         } else if (Validate.isNotNull(message)) {
                            NotificationUtils.showNotification(this, message);
                        }
                    } else if (Validate.isNotNull(message)) {
                        NotificationUtils.showNotification(this, message);
                    }
                    //}

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        try {
            // Check if message contains a notification payload.
            if (remoteMessage.getNotification() != null) {
                //sendNotification(remoteMessage.getNotification().getBody());
                Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    // WS - NOTIFICATION READ
    private void wsNotificationRead(Context context, String userId, long notificationId) {
        try {
            RequestNotificationRead request = new RequestNotificationRead(String.valueOf(userId), notificationId);
            TicketNinjaAPI tnAPI = RestApi.createAPI();
            Call<CallbackNotificationRead> call = tnAPI.notificationRead(request);
            call.enqueue(new Callback<CallbackNotificationRead>() {
                @Override
                public void onResponse(@NonNull Call<CallbackNotificationRead> call, @NonNull Response<CallbackNotificationRead> response) {
                    try {
                        if (response.isSuccessful() && response.body() != null) {
                            if (String.valueOf(RestApi.ErrorCode.SUCCESS).equals(Objects.requireNonNull(response.body()).getError_code())) {
                                LoginUtils mLogin = new LoginUtils(context);
                                mLogin.setNotifications(Objects.requireNonNull(response.body()).data().getNotificationUnreadCount());
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                @Override
                public void onFailure(@NonNull Call<CallbackNotificationRead> call, @NonNull Throwable t) {
                    t.printStackTrace();
                }
            });
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    private void sendRegistrationToServer(String token) {
        try {
            PreferencesUtils.setFCMPushKey(this, token);
            FirebaseMessaging.getInstance().subscribeToTopic("Event_Detail");
            PreferencesUtils.setFCMTopicRegister(this, true);
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
}


