/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialog;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.Target;

import java.util.Objects;

import in.ticketninja.R;
import in.ticketninja.common.Validate;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 23-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class FullImageDialog extends AppCompatDialog {

    private String imageUrl;

    private ImageView ivFullImage;

    public FullImageDialog(Context context) {
        super(context);
    }

    public FullImageDialog(Context context, int theme) {
        super(context, theme);
    }

    protected FullImageDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            Objects.requireNonNull(getWindow()).setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setContentView(R.layout.layout_dialog_plan_display);

            ivFullImage = findViewById(R.id.ivFullImage);
            ImageView ivCloseButton = findViewById(R.id.ivCloseButton);
            Objects.requireNonNull(ivCloseButton).setOnClickListener(v -> dismiss());

            if (Validate.isNotNull(imageUrl)) {
                // int widthDP = Utility.intToDP(getContext(), 250);
                Glide.with(getContext())
                        .load(imageUrl)
                        .fitCenter()
                        .placeholder(R.drawable.ic_placeholder)
                        .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        .into(ivFullImage);
            } else {
                Glide.with(getContext())
                        .load(R.drawable.ic_placeholder)
                        .into(ivFullImage);
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(@NonNull String imageUrl) {
        try {
            this.imageUrl = imageUrl;
            if (Validate.isNotNull(imageUrl) && ivFullImage!=null) {
                Glide.with(getContext())
                        .load(imageUrl)
                        .fitCenter()
                        .placeholder(R.drawable.ic_placeholder)
                        .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        .into(ivFullImage);
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
