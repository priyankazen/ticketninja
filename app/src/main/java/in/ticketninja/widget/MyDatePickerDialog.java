/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.widget;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StyleRes;
import androidx.appcompat.app.AlertDialog;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;

import in.ticketninja.R;
import in.ticketninja.common.DateTimeUtils;

/**
 * Created by aditi on 7/17/17.
 */

public class MyDatePickerDialog extends AlertDialog implements DialogInterface.OnClickListener,
        DatePicker.OnDateChangedListener {

    private static final String YEAR = "year";
    private static final String MONTH = "month";
    private static final String DAY = "day";

    private final DatePicker mDatePicker;

    private OnDateSetListener mDateSetListener;
    private ArrayList<String> disableDatesList = new ArrayList<>();
    private String date;
    /*public AppCompatDialog onCreateDialog(Bundle savedInstanceState) {
        View v = getActivity().getLayoutInflater().inflate(R.layout.layout_dialog_date_picker, null);
        return new AlertDialog().Builder(getActivity()).setView(v).create();
    }*/


    /**
     * Creates a new event_date picker dialog for the current event_date using the parent
     * context's default event_date picker dialog theme.
     *
     * @param context the parent context
     */
    public MyDatePickerDialog(@NonNull Context context) {
        this(context, 0, null, Calendar.getInstance(), -1, -1, -1);
    }

    /**
     * Creates a new event_date picker dialog for the current event_date.
     *
     * @param context    the parent context
     * @param themeResId the resource ID of the theme against which to inflate
     *                   this dialog, or {@code 0} to use the parent
     *                   {@code context}'s default alert dialog theme
     */
    public MyDatePickerDialog(@NonNull Context context, @StyleRes int themeResId) {
        this(context, themeResId, null, Calendar.getInstance(), -1, -1, -1);
    }

    /**
     * Creates a new event_date picker dialog for the specified event_date using the parent
     * context's default event_date picker dialog theme.
     *
     * @param context    the parent context
     * @param listener   the listener to call when the user sets the event_date
     * @param year       the initially selected year
     * @param month      the initially selected month (0-11 for compatibility with
     *                   {@link Calendar#MONTH})
     * @param dayOfMonth the initially selected day of month (1-31, depending
     *                   on month)
     */
    public MyDatePickerDialog(@NonNull Context context, @Nullable OnDateSetListener listener,
                              int year, int month, int dayOfMonth) {
        this(context, 0, listener, null, year, month, dayOfMonth);
    }

    /**
     * Creates a new event_date picker dialog for the specified event_date.
     *
     * @param context     the parent context
     * @param themeResId  the resource ID of the theme against which to inflate
     *                    this dialog, or {@code 0} to use the parent
     *                    {@code context}'s default alert dialog theme
     * @param listener    the listener to call when the user sets the event_date
     * @param year        the initially selected year
     * @param monthOfYear the initially selected month of the year (0-11 for
     *                    compatibility with {@link Calendar#MONTH})
     * @param dayOfMonth  the initially selected day of month (1-31, depending
     *                    on month)
     */
    public MyDatePickerDialog(@NonNull Context context, @StyleRes int themeResId,
                              @Nullable OnDateSetListener listener, int year, int monthOfYear, int dayOfMonth) {
        this(context, themeResId, listener, null, year, monthOfYear, dayOfMonth);
    }

    private MyDatePickerDialog(@NonNull Context context, @StyleRes int themeResId,
                               @Nullable OnDateSetListener listener, @Nullable Calendar calendar, int year,
                               int monthOfYear, int dayOfMonth) {
        super(context, resolveDialogTheme(context, themeResId));
        final Context themeContext = getContext();
        final LayoutInflater inflater = LayoutInflater.from(themeContext);
        final View view = inflater.inflate(R.layout.layout_dialog_date_picker, null);
        setView(view);
        setCancelable(true);

        setButton(BUTTON_POSITIVE, themeContext.getString(android.R.string.ok), this);
        setButton(BUTTON_NEGATIVE, themeContext.getString(android.R.string.cancel), this);
        //setButtonPanelLayoutHint(LAYOUT_HINT_SIDE);

        if (calendar != null) {
            year = calendar.get(Calendar.YEAR);
            monthOfYear = calendar.get(Calendar.MONTH);
            dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        }

        mDatePicker = view.findViewById(R.id.dialog_date_datePicker);
        mDatePicker.init(year, monthOfYear, dayOfMonth, this);
        //mDatePicker.setValidationCallback(mValidationCallback);

        mDateSetListener = listener;
    }

    static @StyleRes
    int resolveDialogTheme(@NonNull Context context, @StyleRes int themeResId) {
        if (themeResId == 0) {
            final TypedValue outValue = new TypedValue();
            context.getTheme().resolveAttribute(android.R.attr.datePickerDialogTheme, outValue, true);
            return outValue.resourceId;
        } else {
            return themeResId;
        }
    }

    @Override
    public void onDateChanged(@NonNull DatePicker view, int year, int month, int dayOfMonth) {
        mDatePicker.init(year, month, dayOfMonth, this);
        date = DateTimeUtils.dateToString(DateTimeUtils.getDate(year, month, dayOfMonth), DateTimeUtils.SERVER_FORMAT_DATE);
        Log.e("onDateChanged", "event_date: " + date);
        //  setDisableDate(event_date,year,month,dayOfMonth);
    }

    /**
     * Sets the listener to call when the user sets the event_date.
     *
     * @param listener the listener to call when the user sets the event_date
     */
    public void setOnDateSetListener(@Nullable OnDateSetListener listener) {
        mDateSetListener = listener;
    }

    @Override
    public void onClick(@NonNull DialogInterface dialog, int which) {
        try {
            switch (which) {
                case BUTTON_POSITIVE:

                    if (disableDatesList != null && disableDatesList.size() > 0) {
                        for (int i = 0; i < disableDatesList.size(); i++) {
                            if (disableDatesList.get(i).equals(date)) {
                                Toast.makeText(getContext(), R.string.ticket_not_available, Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                    }

                    if (mDateSetListener != null) {
                        // Clearing focus forces the dialog to commit any pending
                        // changes, e.g. typed text in a NumberPicker.
                        mDatePicker.clearFocus();
                        mDateSetListener.onDateSet(mDatePicker, mDatePicker.getYear(),
                                mDatePicker.getMonth(), mDatePicker.getDayOfMonth());
                    }
                    break;
                case BUTTON_NEGATIVE:
                    cancel();
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * Returns the {@link DatePicker} contained in this dialog.
     *
     * @return the event_date picker
     */
    @NonNull
    public DatePicker getDatePicker() {
        return mDatePicker;
    }

    /**
     * Sets the current event_date.
     *
     * @param year       the year
     * @param month      the month (0-11 for compatibility with
     *                   {@link Calendar#MONTH})
     * @param dayOfMonth the day of month (1-31, depending on month)
     */
    public void updateDate(int year, int month, int dayOfMonth) {
        mDatePicker.updateDate(year, month, dayOfMonth);
    }

    @NonNull
    @Override
    public Bundle onSaveInstanceState() {
        final Bundle state = super.onSaveInstanceState();
        state.putInt(YEAR, mDatePicker.getYear());
        state.putInt(MONTH, mDatePicker.getMonth());
        state.putInt(DAY, mDatePicker.getDayOfMonth());
        return state;
    }

    @Override
    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        final int year = savedInstanceState.getInt(YEAR);
        final int month = savedInstanceState.getInt(MONTH);
        final int day = savedInstanceState.getInt(DAY);
        mDatePicker.init(year, month, day, this);
    }

    /*private final ValidationCallback mValidationCallback = new ValidationCallback() {
        @Override
        public void onValidationChanged(boolean valid) {
            final Button positive = getButton(BUTTON_POSITIVE);
            if (positive != null) {
                positive.setEnabled(valid);
            }
        }
    };
*/

    /**
     * The listener used to indicate the user has finished selecting a event_date.
     */
    public interface OnDateSetListener {
        /**
         * @param view       the picker associated with the dialog
         * @param year       the selected year
         * @param month      the selected month (0-11 for compatibility with
         *                   {@link Calendar#MONTH})
         * @param dayOfMonth th selected day of the month (1-31, depending on
         *                   month)
         */
        void onDateSet(DatePicker view, int year, int month, int dayOfMonth);
    }

    public void setDisableDate(ArrayList<String> disableDateArray) {
        this.disableDatesList = disableDateArray;
    }

    private void setDisableDate(String date, int year, int month, int dayOfMonth) {
        try {
// Log.e("disableDatesList",""+disableDatesList);
            if (disableDatesList != null && disableDatesList.size() > 0) {
                for (int i = 0; i < disableDatesList.size(); i++) {
                    if (disableDatesList.get(i).equals(date)) {
                        mDatePicker.init(0, 0, 0, this);
                        Log.e("setDisableDate", "" + false + " , " + date);
                    } else {
                        mDatePicker.init(year, month, dayOfMonth, this);
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
