/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.widget;

import java.util.Arrays;
import java.util.List;

public final class ColorGenerator {

    public static ColorGenerator MATERIAL;

    static {
       /* ColorGenerator DEFAULT = create(Arrays.asList(
                0xfff16364,
                0xfff58559,
                0xfff9a43e,
                0xffe4c62e,
                0xff67bf74,
                0xff59a2be,
                0xff2093cd,
                0xffad62a7,
                0xff805781
        ));*/
        MATERIAL = create(Arrays.asList(
                0xff000000,
                0xfff44336,
                0xffd32f2f,
                0xffe91e63,
                0xffc2185b,
                0xff9c27b0,
                0xff7b1fa2,
                0xff673ab7,
                0xff512da8,
                0xff3f51b5,
                0xff303f9f,
                0xff2196f3,
                0xff1976d2,
                0xff00bcd4,
                0xff0097a7,
                0xff009688,
                0xff00796b,
                0xff4caf50,
                0xff388e3c,
                0xff8bc34a,
                0xff689f38,
                0xffcddc39,
                0xffafb42b,
                0xffffeb3b,
                0xfffbc02d,
                0xffffa000,
                0xffff9800,
                0xfff57c00,
                0xffff5722,
                0xffe64a19,
                0xff795548,
                0xff5d4037,
                0xff9e9e9e,
                0xff616161
        ));
    }

    private final List<Integer> mColors;
    //private final Random mRandom;

    public static ColorGenerator create(List<Integer> colorList) {
        return new ColorGenerator(colorList);
    }

    private ColorGenerator(List<Integer> colorList) {
        mColors = colorList;
        //mRandom = new Random(System.currentTimeMillis());
    }

    /*public int getRandomColor() {
        return mColors.get(mRandom.nextInt(mColors.size()));
    }*/

    public int getColor(Object key) {
        return mColors.get(Math.abs(key.hashCode()) % mColors.size());
    }
}
