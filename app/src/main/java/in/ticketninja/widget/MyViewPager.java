/*
 * Copyright (c) 2017. Aditi PArikh
 * Developed by Aditi Parikhfor NicheTech Computer Solutions Pvt. Ltd. use only.
 */

package in.ticketninja.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by aditi on 8/25/17.
 */
public class MyViewPager extends ViewPager {

    private boolean enabled;

    public MyViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.enabled = true;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return this.enabled && super.onTouchEvent(event);

    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return this.enabled && super.onInterceptTouchEvent(event);
       /* if (this.enabled) {
            return super.onInterceptTouchEvent(event);
        }

        return false;*/
    }

    public void setPagingEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
