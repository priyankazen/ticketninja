/*
 * Copyright (c) 2017. Suthar Rohit
 * Developed by Suthar Rohit for NicheTech Computer Solutions Pvt. Ltd. use only.
 * <a href="http://RohitSuthar.com/">Suthar Rohit</a>
 *
 * @author Suthar Rohit
 */

package in.ticketninja.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDialog;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.Target;
import java.io.File;
import java.util.Objects;
import in.ticketninja.R;
import in.ticketninja.common.TypefaceUtils;
import in.ticketninja.common.Validate;

/**
 * Matrubharti(com.nichetech.matrubharti) <br />
 * Developed by <b><a href="http://www.RohitSuthar.com/">Suthar Rohit</a></b>  <br />
 * on 23-Jun-2017.
 *
 * @author Suthar Rohit
 */
public class BarCodeDialog extends AppCompatDialog {

    private String imageUrl;
    private String bookingId;
    private String QRCode;

    private TextView tvBookingId;
    private ImageView ivBarcode;

    /*public BarCodeDialog(Context context) {
        super(context);
    }

    protected BarCodeDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }*/

    public BarCodeDialog(Context context, int theme) {
        super(context, theme);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try {
            //getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            setContentView(R.layout.layout_dialog_barcode_display);

            // TYPEFACE
            //Typeface tfLight = TypefaceUtils.HelveticaLight(getContext());
            Typeface tfRegular = TypefaceUtils.HelveticaRegular(getContext());
            Typeface tfMedium = TypefaceUtils.HelveticaMedium(getContext());

            TextView tvBookingIdLabel = findViewById(R.id.tvBookingIdLabel);
            Objects.requireNonNull(tvBookingIdLabel).setTypeface(tfRegular);
            tvBookingId = findViewById(R.id.tvBookingId);
            Objects.requireNonNull(tvBookingId).setTypeface(tfMedium);

            ivBarcode = findViewById(R.id.ivBarcode);
            ImageView ivCloseButton = findViewById(R.id.ivBarcodeClose);
            Objects.requireNonNull(ivCloseButton).setOnClickListener(v -> dismiss());

            if (Validate.isNotNull(bookingId)) tvBookingId.setText(bookingId);

            if(Validate.isNotNull(QRCode))
            {
                //File f = new File(Constant.IMAGE_DOWNLOAD_PATH+QRCode+".png");
                File f = new File("");
                if(f.exists())
                {
                    Glide.with(getContext())
                            .load(f)
                            .fitCenter()
                            .placeholder(R.drawable.ic_placeholder)
                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                            .into(ivBarcode);
                }
                else if (Validate.isNotNull(imageUrl))
                {
                    Glide.with(getContext())
                            .load(imageUrl)
                            .fitCenter()
                            .placeholder(R.drawable.ic_placeholder)
                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                            .into(ivBarcode);
                }
                else {
                    Glide.with(getContext())
                            .load(R.drawable.ic_placeholder)
                            .into(ivBarcode);
                }

            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

   /* @Nullable
    public String getBookingId() {
        return bookingId;
    }

    @Nullable
    public String getImageUrl() {
        return imageUrl;
    }*/

    public void setImageUrl(@NonNull String imageUrl,String QRCode) {
        try {
            this.imageUrl = imageUrl;
            this.QRCode = QRCode;

            //File f = new File(Constant.IMAGE_DOWNLOAD_PATH+QRCode+".png");
            File f = new File("");
            if(ivBarcode != null)
            {
                if(f.exists())
                {
                    Glide.with(getContext())
                            .load(f)
                            .fitCenter()
                            .placeholder(R.drawable.ic_placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .skipMemoryCache(false)
                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                            .into(ivBarcode);
                }
                else if (Validate.isNotNull(imageUrl))
                {
                    Glide.with(getContext())
                            .load(imageUrl)
                            .fitCenter()
                            .placeholder(R.drawable.ic_placeholder)
                            .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                            .skipMemoryCache(false)
                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                            .into(ivBarcode);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public void setBookingId(@NonNull String bookingId) {
        this.bookingId = bookingId;
        if (Validate.isNotNull(bookingId) && tvBookingId != null) {
            tvBookingId.setText(bookingId);
        }
    }
}
